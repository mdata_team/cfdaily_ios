//
//  Addbutton.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TekstIDLabel;
@interface Addbutton : UIView

@property (nonatomic, strong) IBOutlet UIView *content;
@property (nonatomic, strong) IBOutlet UIButton *AddButton;
@property (nonatomic, strong) IBOutlet UIButton *Start;
@property (nonatomic, strong) IBOutlet TekstIDLabel *EndingViewText;
-(void)setbuttonAction:(UIViewController*)sender;

@end
