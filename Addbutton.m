//
//  Addbutton.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "Addbutton.h"
#import "TekstIDLabel.h"
#import <QuartzCore/QuartzCore.h>

@implementation Addbutton
@synthesize AddButton;
@synthesize Start;
@synthesize EndingViewText;
@synthesize content;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
           self.transform = CGAffineTransformMakeScale(-1, 1);
            
        }
        
       content = [[UIView alloc] initWithFrame:CGRectMake(50, 0, 250, 40)];
        [content setBackgroundColor:[UIColor whiteColor]];
        [content.layer setCornerRadius:10];
        [content.layer setBorderWidth:1.5];
//        [content.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
        [content.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [content.layer setMasksToBounds:YES];
        [self addSubview:content];
             
//        AddButton = [[UIButton alloc]  initWithFrame:CGRectMake(6, 3.5, 60*0.48, 72*0.48)];
        AddButton = [[UIButton alloc]  initWithFrame:CGRectMake(6, 3.5, 34, 34)];
        [AddButton setImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
        [AddButton setTag:173];
        [self addSubview:AddButton];
        [AddButton setHidden:YES];
      
        
        
        
        Start = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3, 60/2, 72/2)];
        [Start setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
        [Start setTag:172];
       
        [self addSubview:Start];
        
        
        
        EndingViewText = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, content.frame.size.width-20, 40)];
        [EndingViewText setTextColor:[UIColor blackColor]];
        [EndingViewText setFont:[UIFont systemFontOfSize:14]];
        [EndingViewText setText:NSLocalizedString(@"add new time",nil)];
        [EndingViewText setTextAlignment:NSTextAlignmentLeft];
        [EndingViewText setTitle:@"Ending"];
        [EndingViewText setTag:120];
        [EndingViewText setBackgroundColor:[UIColor clearColor]];
        [EndingViewText setNumberOfLines:3];
        [content addSubview:EndingViewText];
        
        
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
         
          
            for (TekstIDLabel *label in content.subviews) {
                
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    label.transform = CGAffineTransformMakeScale(-1, 1);
                    [label setTextAlignment:NSTextAlignmentNatural];
                    
                }
                
            }
      
            
            for (UIButton *label in content.subviews) {
                
                
                if ([label isKindOfClass:[UIButton class]]) {
                    
                    label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                    [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                    
                }
                
            }
        }
        
        else
        {
            
        }

     
    }
    return self;
}

-(void)setbuttonAction:(UIViewController*)sender

{
   
     [AddButton addTarget:sender action:@selector(Addit) forControlEvents:UIControlEventTouchUpInside];
     [Start addTarget:sender action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

-(void)setButtonHide

{
    
}





@end
