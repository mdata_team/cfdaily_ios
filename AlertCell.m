//
//  HistoryCell.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "AlertCell.h"
#import "HistoryViewController.h"
#import "MedicineViewController.h"
#import "GetData.h"
#import "AppDelegate.h"
#import "colorButton.h"
#import <QuartzCore/QuartzCore.h>
#import "Notification.h"
#import "SaveData.h"
#import "ActionArrow.h"
#define MIN_CELL_HEIGHT 30

@implementation AlertCell
@synthesize lostView;
@synthesize Date;
@synthesize DateColor;
@synthesize Time;
@synthesize Name;
@synthesize headShot;
@synthesize Content;
@synthesize Taken;
@synthesize Take;
@synthesize Postpone;
@synthesize Skip;
@synthesize edit;
@synthesize NameButton;
@synthesize CurrentArray;
@synthesize label;
@synthesize background;
@synthesize Ammount;
@synthesize Drug_type;
@synthesize Drug_types;
@synthesize info;
@synthesize InfoLabel;
@synthesize quantityString;
@synthesize remainingString;
@synthesize strengthString;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
        UIView *ProfileView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320, 80)];
          [ProfileView setBackgroundColor:[UIColor colorWithRed:0.859 green:0.918 blue:0.949 alpha:1.000]];
        [ProfileView setBackgroundColor:[UIColor clearColor]];
        
        [self addSubview:ProfileView];
        
        
 
        
        DateColor = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0, 320, 80)];
        [DateColor setBackgroundColor:[UIColor clearColor]];
        [ProfileView addSubview:DateColor];
        
        
        
        headShot = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
//        [headShot setBackgroundColor:[UIColor grayColor]];
        [headShot setTag:130];
        [ProfileView addSubview:headShot];
        
           [headShot setImage:[UIImage imageNamed:@"Medhot.png"]];

        
        
        info = [[ActionArrow alloc]  initWithFrame:CGRectMake(285, 10, 60/2, 72/2)];
        [info setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
        [info setCenter:headShot.center];
        [info setTag:140];
        [ProfileView addSubview:info];
//        [ProfileView setAlpha:0.5]; //20-07-2017 comment line for dark color
        
        Name = [[UILabel alloc] initWithFrame:CGRectMake(60, 5, 190, 18)];
//        [Name setTextColor:[UIColor blackColor]]; //01-07-2017 changes
//        [Name setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        //20-07-2017
        [Name setTextColor:[UIColor colorWithRed:104.0f/255.0f green:128.0f/255.0f blue:226.0f/255.0f alpha:1.0f]];
        [Name setFont:[UIFont boldSystemFontOfSize:13]];
        [Name setText:NSLocalizedString(@"",nil)];
        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:2];
        
        [ProfileView addSubview:Name];
        
        
        InfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, 190, 20)];
//        [InfoLabel setTextColor:[UIColor blackColor]]; //01-07-2017 changes
//        [InfoLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        //20-07-2017
        [InfoLabel setTextColor:[UIColor colorWithRed:104.0f/255.0f green:128.0f/255.0f blue:226.0f/255.0f alpha:1.0f]];
        [InfoLabel setFont:[UIFont systemFontOfSize:12]];
        [InfoLabel setTag:120];
        [InfoLabel setBackgroundColor:[UIColor clearColor]];
        [InfoLabel setNumberOfLines:4];
        
        [ProfileView addSubview:InfoLabel];
        
        
        
        Content = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, 190, 18)];
       // [Content setTextColor:[UIColor blackColor]]; //01-07-2017 changes
//        [Content setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        //20-07-2017
        [Content setTextColor:[UIColor colorWithRed:104.0f/255.0f green:128.0f/255.0f blue:226.0f/255.0f alpha:1.0f]];
        [Content setFont:[UIFont boldSystemFontOfSize:12]];
        [Content setText:NSLocalizedString(@"",nil)];
        [Content setTag:120];
        [Content setBackgroundColor:[UIColor clearColor]];
        [Content setNumberOfLines:2];
        
        [ProfileView addSubview:Content];
        [Content setAlpha:0];
        
        
        Date = [[UILabel alloc] initWithFrame:CGRectMake(60, 35, 190, 18)];
//        [Date setTextColor:[UIColor blackColor]]; //01-07-2017 changes
//        [Date setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        //20-07-2017
        [Date setTextColor:[UIColor colorWithRed:104.0f/255.0f green:128.0f/255.0f blue:226.0f/255.0f alpha:1.0f]];
        [Date setFont:[UIFont systemFontOfSize:12]];
        [Date setText:NSLocalizedString(@" ",nil)];
        [Date setTag:120];
        [Date setBackgroundColor:[UIColor clearColor]];
        [Date setNumberOfLines:3];
        
        
        [ProfileView addSubview:Date];
        
        
        Take = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 240, 18)];
        [Take setTextColor:[UIColor blackColor]]; // 01-07-2017 changes
//        [Take setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        //20-07-2017
        [Take setTextColor:[UIColor colorWithRed:104.0f/255.0f green:128.0f/255.0f blue:226.0f/255.0f alpha:1.0f]];
        [Take setFont:[UIFont systemFontOfSize:13]];
        [Take setText:NSLocalizedString(@" ",nil)];
        [Take setTag:120];
        [Take setBackgroundColor:[UIColor clearColor]];
        [Take setNumberOfLines:20];
        
        
        
        
        [ProfileView addSubview:Take];
        
        
        background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Vink.png"]];
        [background setFrame:CGRectMake(self.frame.size.width-100,  30, 20, 20)];
        [background setBackgroundColor:[UIColor clearColor]];
        [self addSubview:background];
        
        CurrentArray = [[NSMutableDictionary alloc]init];
        
        
    
        
        
        
    }
    return self;
}


-(colorButton*)setColor:(colorButton*)sender

{
    
    CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
    shineLayer2.frame = sender.layer.bounds;
    shineLayer2.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          nil];
    shineLayer2.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];
    
    
    
    [sender.layer addSublayer:shineLayer2];
    [sender.layer setMasksToBounds:YES];
    
    
    sender.titleLabel.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    sender.titleLabel.layer.shadowOpacity = 1;
    sender.titleLabel.layer.shadowRadius = 1.0;
    [sender.layer setCornerRadius:4];
    [sender.layer setBorderColor:sender.backgroundColor.CGColor];
    [sender.layer setBorderWidth:0.8];
    
    return sender;
    
}



- (CGFloat)calculateTextHeight:(NSString*)text width:(CGFloat)nWidth fontSize:(CGFloat)nFontSize{
	CGSize constraint = CGSizeMake(nWidth, 20000.0f);
	CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:nFontSize] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
	CGFloat height = MAX(size.height, MIN_CELL_HEIGHT);
	return height;
}



-(void)changeSelect

{
    /*
     
     if ([[NSUserDefaults standardUserDefaults] valueForKey:@"chosenMusic"]) {
     
     [background setAlpha:1];
     
     }
     else
     {
     
     [background setAlpha:0];
     }
     
     */
}


-(void)history:(colorButton*)sender
{
    
    
    [lostView GotToHistory:sender];
    
    
    
}



-(void)book:(colorButton*)sender
{
    
    
  
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void) FillAllItems:(UILocalNotification*) sender index:(NSInteger)set
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray *ry = [GetData getDrugTypeStrength:[sender.userInfo valueForKey:@"Type"]] ;
    
    if (ry.count != 0) //add condition for handle crash 04-09-2017
    {
        
//        NSArray *typit = [[GetData getDrugTypeStrength:[sender.userInfo valueForKey:@"Type"]] objectAtIndex:0]; //05-09-2017
        NSArray *typit = [ry objectAtIndex:0];
       
        
        if ([[sender.userInfo valueForKey:@"Type"] isEqualToString: NSLocalizedString(@"Ointment",nil)])
        {
            
            quantityString =[NSString stringWithFormat:@"%@", [sender.userInfo valueForKey:@"Type"]];
        }
        else
        {
            if ([[sender.userInfo valueForKey:@"Quantity"] intValue]>1) {
                
                if ([[sender.userInfo valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                    
                    //                if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"]) { //04-09-2017
                    if ([[typit valueForKey:@"Drug_types"] isEqualToString:NSLocalizedString(@"Other",nil)]) {
                        quantityString =[NSString stringWithFormat:@"%@ %@", [sender.userInfo valueForKey:@"Quantity"], [sender.userInfo valueForKey:@"Type"]];
                        
                    }
                    else
                    {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", [sender.userInfo valueForKey:@"Quantity"], [typit valueForKey:@"Drug_types"]];
                    }
                    
                }
                
                else
                    
                {
                    //                if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"]) { //04-09-2017
                    //9-11-2017
                    if ([[typit valueForKey:@"Drug_types"] isEqualToString:NSLocalizedString(@"Other",nil)])
                    {
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [sender.userInfo valueForKey:@"Type"]];

                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            if ([[sender.userInfo valueForKey:@"Quantity"] intValue] > 1)
                            {
                                quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [sender.userInfo valueForKey:@"Type"]];

                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                                
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                        }
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            if ([[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Inhalator"] || [[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Dråber"] || [[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Plaster"] || [[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Spray"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                        
                    }
                    else
                    {
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [typit valueForKey:@"Drug_types"]]; //04-09-2017

                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            if ([[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Spray"] || [[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Tablet"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                        }
                        
//                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [typit valueForKey:@"Dose_unit"]];
                        
                    }
                }
                
            }
            else {
                
                if ([[sender.userInfo valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                    
                    
                    
                    
                    if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Other"]) {
                        
                        
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Type"]];
                        
                    }
                    
                    else
                        
                        
                    {
                        
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", [sender.userInfo valueForKey:@"Quantity"], [typit valueForKey:@"Drug_less"]];
                    }
                    
                    
                }
                
                else
                    
                {
                    
                    //                if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Other"]) { //04-09-2017
                    if ([[typit valueForKey:@"Drug_less"] isEqualToString:NSLocalizedString(@"Other",nil)]) {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [sender.userInfo valueForKey:@"Type"]];

                        //09-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            
                            if ([[typit valueForKey:@"Dose_name"]isEqualToString:@"Tabletter per dose"])
                            {
                                if ([[sender.userInfo valueForKey:@"Quantity"] intValue] > 1)
                                {
                                    quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [sender.userInfo valueForKey:@"Type"]];
                                    
                                    quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                                }
                            }
                        }
                        
                        //9-11-2017
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                        }
                        
                    }
                    else
                        
                    {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [typit valueForKey:@"Drug_less"]];

                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
//                            if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Tablet"])
//                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
//                            }
                        }
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                        }
                        
                    }
                }
            }
        }
            
        if ([[sender.userInfo valueForKey:@"Strength"] intValue]>0) {
            
            strengthString =[NSString stringWithFormat:@"(%@ %@)", [sender.userInfo valueForKey:@"Strength"] , [sender.userInfo valueForKey:@"Strength_unit"]];
            
        }
        else {
            
            strengthString =@" ";
            
        }
        
        
        
        
        
        
        
        
        
        if ([[sender.userInfo valueForKey:@"Type"] isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
            
            quantityString =[NSString stringWithFormat:@"%@", [sender.userInfo valueForKey:@"Type"]];
            
            if ([[sender.userInfo valueForKey:@"Remaining"] intValue]>1) {
                
                remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [typit valueForKey:@"Drug_types"], NSLocalizedString(@"left",nil)];
            }
            
            else
            {
                
                
                
                remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [typit valueForKey:@"Drug_less"], NSLocalizedString(@"left",nil)];
                
                //15-11
                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                {
                    remainingString = [remainingString stringByReplacingOccurrencesOfString:@"Annet" withString:@""];
                }
            }
        }
        else
            
        {
            
            if ([[sender.userInfo valueForKey:@"Remaining"] isEqualToString:@" "]) {
                
                remainingString =@" ";
            }
            else
                
            {
                if ([[sender.userInfo valueForKey:@"Remaining"] intValue]>1) {
                    
                    if ([[sender.userInfo valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                        
                        if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"]) {
                            
                            
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [sender.userInfo valueForKey:@"Type"], NSLocalizedString(@"left",nil)];
                        }
                        
                        else
                            
                        {
                            
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [typit valueForKey:@"Drug_types"], NSLocalizedString(@"left",nil)];
                        }
                    }
                    
                    else
                    {
                        remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"],[sender.userInfo valueForKey:@"Dose_unit"], NSLocalizedString(@"left",nil)];

                        //15-11
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            remainingString = [remainingString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                        }
                        
                    }
                    
                }
                else {
                    
                    if ([[sender.userInfo valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                        
                        if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Other"]) {
                            
                            
                            
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [typit valueForKey:@"Drug_less"], NSLocalizedString(@"left",nil)];
                        }
                        
                        else
                        {
                            
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [typit valueForKey:@"Drug_less"], NSLocalizedString(@"left",nil)];
                        }
                    }
                    
                    else
                        
                    {
                        
                        remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"],[sender.userInfo valueForKey:@"Dose_unit"], NSLocalizedString(@"left",nil)];
                    }
                    
                }
                
            }
            
            
        }
        
        
        
        
        
        [appDelegate.curentChoise addObject:sender];
        
        [Name setText:[NSString stringWithFormat:@"%@", [sender.userInfo valueForKey:@"Name"]]];
        
        
        [InfoLabel setText:[NSString stringWithFormat:@"%@ %@",quantityString, strengthString]];
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormat setDateFormat:[appDelegate convertString:@"hh:mm a EEEE dd/MM/yy"]];
        
        
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:sender.fireDate];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:sender.fireDate];
        
        NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        if ([NSLocalizedString(@"at",nil) length]>1) {
            
            [Date setText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"at",nil),[dateFormat stringFromDate:itemNext]]];
            
        }
        else
            
        {
            [Date setText:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:itemNext]]];
            
            
        }
        
        
        
        
        if ([[sender.userInfo valueForKey:@"Location"] isEqualToString:@" "]) {
            
            
            if ([[sender.userInfo valueForKey:@"Instructions"] isEqualToString:@" "]) {
                
                [Take setText:[sender.userInfo valueForKey:@"Instructions"]];
                [Take setFrame:CGRectMake(10, 55, 240, 60)];
                
                
                
            }
            else
            {
                
                [Take setText:[sender.userInfo valueForKey:@"Instructions"]];
                [Take setFrame:CGRectMake(10, 55, 240, [self calculateTextHeight:[sender.userInfo valueForKey:@"Instructions"] width:240 fontSize:13])];
                
                
            }
            
        }
        
        else
        {
            [Take setText:[NSString stringWithFormat:@"%@\n%@", [sender.userInfo valueForKey:@"Location"], [sender.userInfo valueForKey:@"Instructions"]]];
            [Take setFrame:CGRectMake(10, 55, 240, [self calculateTextHeight:[NSString stringWithFormat:@"%@\n%@", [sender.userInfo valueForKey:@"Location"], [sender.userInfo valueForKey:@"Instructions"]] width:240 fontSize:13])];
        }
        
        
        
        
        if ([[sender.userInfo valueForKey:@"Recording"] isEqualToString:@"444"]) {
            [info setAlpha:0];
        }
        else
        {
            
            [info setTitleit:[sender.userInfo valueForKey:@"Recording"]];
            [info addTarget:lostView action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [info setAlpha:0.5];
            
        }
        
        headShot = (UIImageView *)[self viewWithTag:(130)];
        
        if ([UIImage imageWithContentsOfFile:[sender.userInfo valueForKey:@"Image"]]) {
            
            [headShot setImage:[UIImage imageWithContentsOfFile:[sender.userInfo valueForKey:@"Image"]]];
        }
        else
        {
            
        }
        
        
        
        
        if ([appDelegate.selectedMeds containsObject:[sender.userInfo valueForKey:@"ID_Medicine"]]) {
            
            [background setAlpha:0];
            [[NSUserDefaults standardUserDefaults] setObject:[sender.userInfo valueForKey:@"ID_Medicine"] forKey: @"ID_Medicine"];
            
        }
        else
        {  [background setAlpha:0];
            
        }
        
    }
    
    
}

-(void)getparent:(MedicineViewController*)set

{
    lostView =set;
}

@end
