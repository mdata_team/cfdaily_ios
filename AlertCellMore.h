//
//  HistoryCell.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MedicineViewController, colorButton;
@interface AlertCellMore : UITableViewCell 
@property (nonatomic, retain) IBOutlet  MedicineViewController *lostView;
@property (nonatomic, retain) IBOutlet UILabel *Date;
@property (nonatomic, retain) IBOutlet UILabel *DateColor;
@property (nonatomic, retain) IBOutlet UILabel *Time;
@property (nonatomic, retain) IBOutlet UILabel *Name;
@property (nonatomic, retain) IBOutlet UILabel *Taken;
@property (nonatomic, retain) IBOutlet UILabel *Content;
@property (nonatomic, retain) IBOutlet UILabel *Take;
@property (nonatomic, retain) IBOutlet NSString *taketext;
@property (nonatomic, retain) IBOutlet NSString *datetext;
@property (nonatomic, retain) IBOutlet NSString *nametext;
@property (nonatomic, retain) IBOutlet NSString *infotext;
@property (nonatomic, retain) IBOutlet NSString *Ammount;
@property (nonatomic, retain) IBOutlet NSString *Drug_type;
@property (nonatomic, retain) IBOutlet NSString *Drug_types;
@property (nonatomic, retain) IBOutlet UIView *ProfileView;
@property (nonatomic, retain) IBOutlet UILabel *InfoLabel;
//Ammount

@property (nonatomic, retain) NSString *quantityString;
@property (nonatomic, retain) NSString *remainingString;
@property (nonatomic, retain) NSString *strengthString;


@property (nonatomic, retain) IBOutlet colorButton *Postpone;
@property (nonatomic, retain) IBOutlet colorButton *Skip;
@property (nonatomic, retain) IBOutlet colorButton *edit;
@property (nonatomic, retain) IBOutlet  colorButton *NameButton;
@property (nonatomic, retain) IBOutlet UIImageView *headShot;
@property (nonatomic, retain) IBOutlet UILabel *label;
@property (nonatomic, retain) IBOutlet NSDictionary *CurrentArray;
@property (nonatomic, retain) IBOutlet UIImageView *background;
-(void)getparent:(MedicineViewController*)set;
-(void) FillAllItems:(UILocalNotification*) sender index:(NSInteger)set;
-(colorButton*)setColor:(colorButton*)sender;
@end
