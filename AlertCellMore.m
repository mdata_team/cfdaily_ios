//
//  HistoryCell.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "AlertCellMore.h"
#import "HistoryViewController.h"
#import "MedicineViewController.h"
#import "GetData.h"
#import "AppDelegate.h"
#import "colorButton.h"
#import <QuartzCore/QuartzCore.h>
#import "Notification.h"
#import "SaveData.h"
#define MIN_CELL_HEIGHT 20.0f

@implementation AlertCellMore
@synthesize lostView;
@synthesize Date;
@synthesize DateColor;
@synthesize Time;
@synthesize Name;
@synthesize headShot;
@synthesize Content;
@synthesize Taken;
@synthesize Take;
@synthesize Postpone;
@synthesize Skip;
@synthesize edit;
@synthesize NameButton;
@synthesize CurrentArray;
@synthesize label;
@synthesize background;
@synthesize Ammount;
@synthesize Drug_type;
@synthesize Drug_types;
@synthesize ProfileView;
@synthesize InfoLabel;
@synthesize quantityString;
@synthesize remainingString;
@synthesize taketext;
@synthesize datetext;
@synthesize nametext;
@synthesize infotext;
@synthesize strengthString;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
        ProfileView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320, self.frame.size.height+14)];
            
            [self addSubview:ProfileView];
        
            [self setBackgroundColor:[UIColor colorWithRed:0.859 green:0.918 blue:0.949 alpha:1.000]];

        DateColor = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0, 320, 80)];
        [DateColor setBackgroundColor:[UIColor clearColor]];
        [ProfileView addSubview:DateColor];
        
        
        headShot = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
        [headShot setTag:130];
       [ProfileView addSubview:headShot];
        
        [headShot setImage:[UIImage imageNamed:@"Medhot.png"]];
        
        
        Name = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 190, 60)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont boldSystemFontOfSize:13]];
        [Name setText:NSLocalizedString(@"",nil)];
        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:0];
        
        [ProfileView addSubview:Name];
        
   

        [ProfileView addSubview:Take];
      
  
        background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Vink.png"]];
        [background setFrame:CGRectMake(self.frame.size.width-30,  10, 20, 20)];
        [background setBackgroundColor:[UIColor clearColor]];
        [self addSubview:background];
        
        
        CurrentArray = [[NSMutableDictionary alloc]init];
            
        }
        
        else
            
        {
            
            //NSLog(@"left");
            
            ProfileView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320, self.frame.size.height+14)];
            //218, 226, 239
//            [ProfileView setBackgroundColor:[UIColor colorWithRed:0.859 green:0.918 blue:0.949 alpha:1.000]]; //06-07-2017
//            [ProfileView setBackgroundColor:[UIColor colorWithRed:218.0f/255.0f green:226.0f/255.0f blue:239.0f/255.0f alpha:1.000]]; //18-07-2017
            
            [self addSubview:ProfileView];
            
            [self setBackgroundColor:[UIColor colorWithRed:0.859 green:0.918 blue:0.949 alpha:1.000]];
            
            DateColor = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0, 320, 80)];
            [DateColor setBackgroundColor:[UIColor clearColor]];
            [ProfileView addSubview:DateColor];
            
            
            headShot = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
//            [headShot setBackgroundColor:[UIColor grayColor]];
            [headShot setTag:130];
            [ProfileView addSubview:headShot];
            
            [headShot setImage:[UIImage imageNamed:@"Medhot.png"]];
            
            
            
            
            
            
            
            Name = [[UILabel alloc] initWithFrame:CGRectMake(60, 5, 190, 60)];
            [Name setTextColor:[UIColor blackColor]];
            [Name setFont:[UIFont boldSystemFontOfSize:13]];
            [Name setText:NSLocalizedString(@"",nil)];
            [Name setTag:120];
            [Name setBackgroundColor:[UIColor clearColor]];
            [Name setNumberOfLines:0];
            
            [ProfileView addSubview:Name];
            
            
            
            
            InfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, 190, 20)];
            [InfoLabel setTextColor:[UIColor blackColor]];
            [InfoLabel setFont:[UIFont systemFontOfSize:12]];
            [InfoLabel setTag:120];
            [InfoLabel setBackgroundColor:[UIColor clearColor]];
            [InfoLabel setNumberOfLines:4];
            
            [ProfileView addSubview:InfoLabel];
            
            
            
            
            Date = [[UILabel alloc] initWithFrame:CGRectMake(60, 40, 250, 18)];
            [Date setTextColor:[UIColor blackColor]];
            [Date setFont:[UIFont systemFontOfSize:13]];
            [Date setText:NSLocalizedString(@" ",nil)];
            [Date setTag:120];
            [Date setBackgroundColor:[UIColor clearColor]];
            [Date setNumberOfLines:3];
            
            
            [ProfileView addSubview:Date];
            
            
            Take = [[UILabel alloc] initWithFrame:CGRectMake(60, 50, 190, 18)];
            [Take setTextColor:[UIColor blackColor]];
            [Take setFont:[UIFont systemFontOfSize:13]];
            [Take setText:NSLocalizedString(@" ",nil)];
            [Take setTag:120];
            [Take setBackgroundColor:[UIColor clearColor]];
            [Take setNumberOfLines:3];
            
            
            
            
            [ProfileView addSubview:Take];
            
            
            background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Vink.png"]];
            [background setFrame:CGRectMake(self.frame.size.width-30,  10, 20, 20)];
            [background setBackgroundColor:[UIColor clearColor]];
            [self addSubview:background];
            
            
            CurrentArray = [[NSMutableDictionary alloc]init];
        }
     

        
    }
    return self;
}


-(colorButton*)setColor:(colorButton*)sender

{
    
    CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
    shineLayer2.frame = sender.layer.bounds;
    shineLayer2.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          nil];
    shineLayer2.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];
    
    
    
    [sender.layer addSublayer:shineLayer2];
    [sender.layer setMasksToBounds:YES];
    
    
    sender.titleLabel.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    sender.titleLabel.layer.shadowOpacity = 1;
    sender.titleLabel.layer.shadowRadius = 1.0;
    [sender.layer setCornerRadius:4];
    [sender.layer setBorderColor:sender.backgroundColor.CGColor];
    [sender.layer setBorderWidth:0.8];
    
    return sender;
    
}




-(void)changeSelect

{
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"chosenMusic"]) {
        
        [background setAlpha:1];
        
    }
    else
    {
        
        [background setAlpha:0];
    }
}


-(void)history:(colorButton*)sender
{
  
    
    [lostView GotToHistory:sender];
    
    
    
}



-(void)book:(colorButton*)sender
{


   [lostView renewMedicine:sender.chosenCourse];
    
   

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

-(void) FillAllItems:(UILocalNotification*) sender index:(NSInteger)set
{
    // add contion for handle crash issue //*************

    NSArray *ry = [GetData getDrugTypeStrength:[sender.userInfo valueForKey:@"Type"]];
    
    if (ry.count != 0)
    {
        
//        NSArray *typit = [[GetData getDrugTypeStrength:[sender.userInfo valueForKey:@"Type"]] objectAtIndex:0]; //05-09-2017
        NSArray *typit = [ry objectAtIndex:0];        
        
        if ([[sender.userInfo valueForKey:@"Type"] isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
            
            quantityString =[NSString stringWithFormat:@"%@", [sender.userInfo valueForKey:@"Type"]];
        }
        else
        {
            if ([[sender.userInfo valueForKey:@"Quantity"] intValue]>1) {
                
                if ([[sender.userInfo valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                    
                    if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"]) {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", [sender.userInfo valueForKey:@"Quantity"], [sender.userInfo valueForKey:@"Type"]];
                        
                    }
                    else
                    {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", [sender.userInfo valueForKey:@"Quantity"], [typit valueForKey:@"Drug_types"]];
                    }
                }
                
                else
                    
                {
                    if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"]) {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [sender.userInfo valueForKey:@"Type"]];
                        
                    }
                    else
                    {
                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [sender.userInfo valueForKey:@"Type"]];
                            
                            //15-11
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            if ([[sender.userInfo valueForKey:@"Quantity"] intValue] > 1)
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprayer"];

                            }
                            
                            
                            if ([[sender.userInfo valueForKey:@"Type"]isEqualToString:@"Tablett"])
                            {
                                if ([[sender.userInfo valueForKey:@"Quantity"] intValue] > 1)
                                {
                                    quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], @"Tabletter"];

                                }
                                else
                                {
                                    quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], @"Tablett"];

                                }
                                
                                //15-11
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                            }
                            
                        }
                        //9-11-2017
                        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [sender.userInfo valueForKey:@"Type"]];
                            
                            if ([[sender.userInfo valueForKey:@"Quantity"] intValue] > 1)
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprays"];
                            }
                            
                            if ([[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Inhalator"] || [[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Dråber"] || [[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Spray"] || [[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Plaster"] || [[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Tablet"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                        else
                        {
                            quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [typit valueForKey:@"Drug_types"]];
                        }
                    }
                }
                
            }
            else {
                
                if ([[sender.userInfo valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                    
                    
                    
                    if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Other"]) {
                        
                        
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Type"]];
                        
                    }
                    
                    else
                        
                        
                    {
                        
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", [sender.userInfo valueForKey:@"Quantity"], [typit valueForKey:@"Drug_less"]];
                    }
                    
                    
                }
                
                else
                    
                {
                    
                    if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Other"]) {
                        
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [sender.userInfo valueForKey:@"Type"]];
                    }
                    else
                    {
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [typit valueForKey:@"Drug_less"]]; //13-11-2017

                        //13-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [sender.userInfo valueForKey:@"Type"]]; //15-11-2017
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            
                            if ([[sender.userInfo valueForKey:@"Type"] isEqualToString:@"Tablett"])
                            {
                                if ([[sender.userInfo valueForKey:@"Strength"] intValue] > 1)
                                {
                                    quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], @"Tabletter"];
                                }
                                else
                                {
                                    quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], @"Tablett"];
                                }
                            }
                        }
                        
                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            
                            quantityString =[NSString stringWithFormat:@"%@ %@ %@", [sender.userInfo valueForKey:@"Quantity"],[sender.userInfo valueForKey:@"Dose_unit"], [sender.userInfo valueForKey:@"Type"]]; //13-11-2017

                            
//                            if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Tablet"])
//                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
//                            }
                            
                            
                        }
                        
                    }
                }
                
            }
            
        }
        
        
        
        if ([[sender.userInfo valueForKey:@"Strength"] intValue]>0) {
            
            strengthString =[NSString stringWithFormat:@"(%@ %@)", [sender.userInfo valueForKey:@"Strength"] , [sender.userInfo valueForKey:@"Strength_unit"]];
            
        }
        else {
            
            strengthString =@" ";
            
        }
        
        
        
        
        
        
        
        
        
        if ([[sender.userInfo valueForKey:@"Type"] isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
            
            quantityString =[NSString stringWithFormat:@"%@", [sender.userInfo valueForKey:@"Type"]];
            
            if ([[sender.userInfo valueForKey:@"Remaining"] intValue]>1) {
                
                remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [typit valueForKey:@"Drug_types"], NSLocalizedString(@"left",nil)];
            }
            
            else
            {
                
                
                
                remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [typit valueForKey:@"Drug_less"], NSLocalizedString(@"left",nil)];
            }
        }
        else
            
        {
            
            if ([[sender.userInfo valueForKey:@"Remaining"] isEqualToString:@" "]) {
                
                remainingString =@" ";
            }
            else
                
            {
                if ([[sender.userInfo valueForKey:@"Remaining"] intValue]>1) {
                    
                    if ([[sender.userInfo valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                        
                        if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"]) {
                            
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [sender.userInfo valueForKey:@"Type"], NSLocalizedString(@"left",nil)];
                        }
                        
                        else
                            
                        {
                            
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [typit valueForKey:@"Drug_types"], NSLocalizedString(@"left",nil)];
                        }
                    }
                    
                    else
                        
                    {
                        
                        remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"],[sender.userInfo valueForKey:@"Dose_unit"], NSLocalizedString(@"left",nil)];
                    }
                    
                }
                else {
                    
                    if ([[sender.userInfo valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                        
                        if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Other"]) {
                            
                            
                            //////////////////////////////////////////////////////////////NSLog(@"test 4");
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [typit valueForKey:@"Drug_less"],NSLocalizedString(@"left",nil)];
                        }
                        
                        else
                        {
                            
                            //////////////////////////////////////////////////////////////NSLog(@"test 5");
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"], [typit valueForKey:@"Drug_less"], NSLocalizedString(@"left",nil)];
                        }
                    }
                    
                    else
                        
                    {
                        
                        //////////////////////////////////////////////////////////////NSLog(@"test 6");
                        remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [sender.userInfo valueForKey:@"Remaining"],[sender.userInfo valueForKey:@"Dose_unit"], NSLocalizedString(@"left",nil)];
                    }
                    
                }
                
            }
            
            
        }
        
        
        ////////////////////////////////////////////////////////////NSLog(@"%@ %@", quantityString, strengthString);
        
        infotext = [NSString stringWithFormat:@"%@ %@",quantityString, strengthString];
        
        
        nametext = [NSString stringWithFormat:@"%@", [sender.userInfo valueForKey:@"Name"]];
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormat setDateFormat:[appDelegate convertString:@"hh:mm a EEEE dd/MM/yy"]];
        
        
        
        
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:sender.fireDate];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:sender.fireDate];
        
        NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:0];
        NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        NSString *str = NSLocalizedString(@"at", nil);
        
        NSLog(@"at localize : %@",str);
        
        
        //    if ([NSLocalizedString(@"at",nil) length]>=1) { //04-09-2017
        if ([str length] >= 1) {
            
            datetext =[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"at",nil),[dateFormat stringFromDate:itemNext]];
            
        }
        else
            
        {
            taketext =[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:itemNext]];
            
            
        }
        
        if ([[sender.userInfo valueForKey:@"Location"] isEqualToString:@" "]) {
            
            
            
            
            taketext =[sender.userInfo valueForKey:@"Instructions"];
            
            
        }
        
        else
        {
            taketext =[NSString stringWithFormat:@"%@\n%@", [sender.userInfo valueForKey:@"Location"], [sender.userInfo valueForKey:@"Instructions"]];
            //
        }
        
        headShot = (UIImageView *)[self viewWithTag:(130)];
        
        if ([UIImage imageWithContentsOfFile:[sender.userInfo valueForKey:@"Image"]]) {
            
            [headShot setImage:[UIImage imageWithContentsOfFile:[sender.userInfo valueForKey:@"Image"]]];
            
            
        }
        
        else
        {
            
        }
        
        
        
        if ([appDelegate.selectedMeds  containsObject:sender]) {
            
            
            [background setAlpha:1];
        }
        else
            
        {
            [background setAlpha:0];
            
        }
        
        NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] init];
        
        
        UIFont *arialBold = [UIFont boldSystemFontOfSize:13];
        UIFont *ariallight = [UIFont systemFontOfSize:11];
        
        NSDictionary *arialDict = [NSDictionary dictionaryWithObject: arialBold forKey:NSFontAttributeName];
        
        NSDictionary *lightDict = [NSDictionary dictionaryWithObject: ariallight forKey:NSFontAttributeName];
        
        
        NSMutableAttributedString *nameit = [[NSMutableAttributedString alloc] initWithString:nametext attributes: arialDict];
        [nameit addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000] range:(NSMakeRange(0, [nametext length]))];
        
        NSMutableAttributedString *infoit = [[NSMutableAttributedString alloc] initWithString:infotext attributes: lightDict];
        [infoit addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000] range:(NSMakeRange(0, [infotext length]))];
        
        
        NSMutableAttributedString *takeit = [[NSMutableAttributedString alloc] initWithString:taketext attributes: lightDict];
        [takeit addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000] range:(NSMakeRange(0, [takeit length]))];
        
        NSMutableAttributedString *dateit = [[NSMutableAttributedString alloc] initWithString:datetext attributes: lightDict];
        [dateit addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000] range:(NSMakeRange(0, [dateit length]))];
        
        
        NSMutableAttributedString *tussen = [[NSMutableAttributedString alloc] initWithString:@"\n" attributes: arialDict];
        
        [aAttrString appendAttributedString:nameit];
        [aAttrString appendAttributedString:tussen];
        [aAttrString appendAttributedString:infoit];
        [aAttrString appendAttributedString:tussen];
        [aAttrString appendAttributedString:dateit];
        [aAttrString appendAttributedString:tussen];
        [aAttrString appendAttributedString:takeit];
        
        [Name setAttributedText:aAttrString];
        
        
        
        //    [Name setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]]; // 03-07-2017 add textcolore
        //20-07-2017 change
        [Name setTextColor:[UIColor colorWithRed:104.0f/255.0f green:128.0f/255.0f blue:226.0f/255.0f alpha:1.0f]];
        //    Name.lineBreakMode = NSLineBreakByCharWrapping;
        //    [Name  setFrame:CGRectMake(Name.frame.origin.x, Name.frame.origin.y, Name.frame.size.width, 60)];
        [Name sizeToFit];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Data not display" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
  
}

- (CGFloat)calculateTextHeight:(NSString*)text width:(CGFloat)nWidth fontSize:(CGFloat)nFontSize{
    
    //05-07-2017 change all comment code and write new below code
    
    NSString *cellText = [text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    UIFont *cellFont = [UIFont boldSystemFontOfSize:nFontSize];
    CGSize constraintSize = CGSizeMake(nWidth, MAXFLOAT);
    CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:CGSizeMake(240, constraintSize.height) lineBreakMode:NSLineBreakByWordWrapping];
    return labelSize.height;
    
}

-(void)getparent:(MedicineViewController*)set

{
    lostView =set;
}

@end
