//
//  AlertCustimize.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 07-05-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertCustimize : UIAlertView


-(void) MakeDeleteIt;
-(void) RemindMeLater;
-(void) JustOk;


@end
