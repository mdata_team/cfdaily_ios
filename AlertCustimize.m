//
//  AlertCustimize.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 07-05-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "AlertCustimize.h"

@implementation AlertCustimize

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) MakeDeleteIt


{
    [self setTag:50];
    [self addButtonWithTitle:NSLocalizedString(@"Delete", nil)];
    [self addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
}


-(void) RemindMeLater


{
    [self setTag:51];
    [self addButtonWithTitle:NSLocalizedString(@"Ok",nil)];
    [self addButtonWithTitle:NSLocalizedString(@"Remind me Later",nil)];
}


-(void) JustOk


{
    [self setTag:52];
    [self addButtonWithTitle:NSLocalizedString(@"Ok",nil)];
}

@end
