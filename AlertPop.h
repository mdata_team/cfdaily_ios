//
//  AlertPop.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertPop : UIView <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

{
    
    NSMutableArray *NotificationList;
}


@property (nonatomic, retain) IBOutlet UIImageView *headShot;
@property (nonatomic, retain) IBOutlet UILabel *Date;
@property (nonatomic, retain) IBOutlet UILabel *Name;
@property (nonatomic, retain) IBOutlet UITextView *Taken;
@property (nonatomic, retain) UITableView *table;
@property (nonatomic, retain) NSMutableArray *NotificationList;
@property (nonatomic, retain) NSMutableArray *SectionList;
@property (nonatomic, retain) UILabel *title;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, assign, getter=isShouldBeginEditing) BOOL shouldBeginEditing;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerCopy;


-(void)fresh;
-(void)setMultiple;
-(void)setSingle;
-(void)Choice:(UIButton*)sender;
-(void)changeData:(NSMutableDictionary*)sender date:(NSDate*)fireDate;


@end
