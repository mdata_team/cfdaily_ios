//
//  AlertPop.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "AlertPop.h"
#import <QuartzCore/QuartzCore.h>
#import "GetData.h"
#import "AppDelegate.h"
#import "Notification.h"
#import "AlertCell.h"
#import "RecordingAct.h"
#import "TakeOrTookPop.h"
#import "PostponePop.h"
#import "SkipPop.h"
#import "SaveCore.h"
#define MIN_CELL_HEIGHT 20

@implementation AlertPop
@synthesize headShot;
@synthesize Date;
@synthesize Name;
@synthesize Taken;
@synthesize table;
@synthesize NotificationList;
@synthesize SectionList;
@synthesize title;




- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
          NotificationList =[[NSMutableArray alloc]init];
     
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0.0f, 280, 280)];
        [title setTag:233];
//        [title setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //30-06-2017 changes
//        [title setBackgroundColor:[UIColor colorWithRed:0.396 green:0.722 blue:0.533 alpha:1.000]];
        [title setBackgroundColor:[UIColor colorWithRed:157.0f/255.0f green:214.0f/255.0f blue:185.0f/255.0f alpha:1.0f]]; //03-07-2017 change
        [title.layer setCornerRadius:10];
        [title.layer setMasksToBounds:YES];
        [self addSubview:title];
        
      /*
        CAGradientLayer *shineLayer1 = [CAGradientLayer layer];
        shineLayer1.frame = title.layer.bounds;
        shineLayer1.colors = [NSArray arrayWithObjects:
                              (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                              (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                              (id)[UIColor colorWithWhite:0.75f alpha:0.4f].CGColor,
                              (id)[UIColor colorWithWhite:0.4f alpha:0.4f].CGColor,
                              (id)[UIColor colorWithWhite:0.2f alpha:0.4f].CGColor,
                              nil];
        shineLayer1.locations = [NSArray arrayWithObjects:
                                 [NSNumber numberWithFloat:1.0f],
                                 [NSNumber numberWithFloat:0.8f],
                                 [NSNumber numberWithFloat:0.6f],
                                 [NSNumber numberWithFloat:0.4f],
                                 [NSNumber numberWithFloat:0.2f],
                                 nil];
        [title.layer addSublayer:shineLayer1]; */
        
       //03-07-2017 remove layer add color without shadow.
        
        
        
        UILabel *Take = [[UILabel alloc] initWithFrame:CGRectMake(20, 2, 280, 40)];
        [Take setTextColor:[UIColor whiteColor]];
        [Take setTag:120];
        [Take setText:NSLocalizedString(@"CFDaily Reminder",nil)];
        [Take setFont:[UIFont boldSystemFontOfSize:18]];
        [Take setTextAlignment:NSTextAlignmentCenter];
        [Take setBackgroundColor:[UIColor clearColor]];
        [Take setNumberOfLines:3];
        [self addSubview:Take];
        
        
        Name = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 280, 40)];
        [Name setTextColor:[UIColor whiteColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setTag:120];
        [Name setText:NSLocalizedString(@"Time to take:",nil)];
        [Name setTextAlignment:NSTextAlignmentCenter];
        [Name setFont:[UIFont boldSystemFontOfSize:15]];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [self addSubview:Name];
        
        
        NSArray *profileItem =[NSArray arrayWithObjects:NSLocalizedString(@"Take",nil),NSLocalizedString(@"Skip",nil),NSLocalizedString(@"Postpone",nil), nil];
        
        
        for (NSInteger i=0 ; i<[profileItem count]; i++) {
            
            
            
            UIButton *Stelvraag = [[UIButton alloc] initWithFrame:CGRectMake(30+(100*i), 185, 90, 40)];
            
            
            [Stelvraag setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
            [Stelvraag setTitle:[profileItem objectAtIndex:i] forState:UIControlStateNormal];
            Stelvraag.titleLabel.layer.shadowOffset = CGSizeMake(1, 1);
            Stelvraag.titleLabel.layer.shadowOpacity = 1;
            Stelvraag.titleLabel.layer.shadowRadius = 1.0;
            [Stelvraag.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
            [Stelvraag.titleLabel setTextColor:[UIColor whiteColor]];
            [Stelvraag setShowsTouchWhenHighlighted:YES];
            [Stelvraag.layer setCornerRadius:8];
            [Stelvraag setTag:200+i];
            [Stelvraag.layer setBorderWidth:1.5];
       
            if (i ==0) {
                //101-184-136 background color
                [Stelvraag setFrame:CGRectMake(25, 275, 70, 40)];
//                [Stelvraag setBackgroundColor:[UIColor colorWithRed:0.067 green:0.467 blue:0.082 alpha:1.000]]; //29-06-2017 changes
                [Stelvraag setBackgroundColor:[UIColor colorWithRed:0.396 green:0.722 blue:0.533 alpha:1.000]];
                     [Stelvraag addTarget:self action:@selector(Take:) forControlEvents:UIControlEventTouchUpInside];
            }
            if (i ==1) {
                
                [Stelvraag setFrame:CGRectMake(100, 275, 70, 40)];
                [Stelvraag setBackgroundColor:[UIColor colorWithRed:1.000 green:0.502 blue:0.000 alpha:1.000]];
                     [Stelvraag addTarget:self action:@selector(Skip:) forControlEvents:UIControlEventTouchUpInside];
            }
            if (i ==2) {
                
                [Stelvraag setFrame:CGRectMake(175, 275, 100, 40)];
//                [Stelvraag setBackgroundColor:[UIColor colorWithRed:0.106 green:0.157 blue:0.612 alpha:1.000]]; //30-06-2017 changes
                [Stelvraag setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                     [Stelvraag addTarget:self action:@selector(Postpone:) forControlEvents:UIControlEventTouchUpInside];
                
                
            }
            
            
            
            [self addSubview:Stelvraag];
            
            CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
            shineLayer2.frame = Stelvraag.layer.bounds;
            shineLayer2.colors = [NSArray arrayWithObjects:
                                  (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                                  (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                                  (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                                  (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                                  (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                                  nil];
            shineLayer2.locations = [NSArray arrayWithObjects:
                                     [NSNumber numberWithFloat:0.0f],
                                     [NSNumber numberWithFloat:0.5f],
                                     [NSNumber numberWithFloat:0.5f],
                                     [NSNumber numberWithFloat:0.8f],
                                     [NSNumber numberWithFloat:1.0f],
                                     nil];
            
            
            
            [Stelvraag.layer addSublayer:shineLayer2];
            [Stelvraag.layer setMasksToBounds:YES];
            [Stelvraag.layer setCornerRadius:4];
            [Stelvraag.layer setBorderWidth:1.5];
            [Stelvraag.layer setBorderColor:Stelvraag.backgroundColor.CGColor];
            
        }
        
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(20, 50, 260, 220)];
//        table.separatorColor = [UIColor colorWithRed:0.216 green:0.627 blue:0.651 alpha:1.000]; //01-07-2017 changes
        table.separatorColor = [UIColor colorWithRed:0.396 green:0.722 blue:0.533 alpha:1.000];
//        table.backgroundColor =[UIColor colorWithRed:0.859 green:0.918 blue:0.949 alpha:1.000]; //18-07-2017
        table.backgroundColor = [UIColor colorWithRed:252.0f/255.0f green:241.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
        table.rowHeight =60;
        [table.layer setBorderWidth:1];
//        [table.layer setBorderColor:[UIColor colorWithRed:0.216 green:0.627 blue:0.651 alpha:1.000].CGColor]; //01-07-2017 change
        [table.layer setBorderColor:[UIColor clearColor].CGColor];
        [table.layer setCornerRadius:6];
        [self addSubview:table];
        
        
    }
    return self;
}




-(void)PresentView:(UIButton*)sender

{
    
    
    //Paracetamol.pcm
    
   
    if (sender) {
   
    NSURL *url = [[NSURL alloc] initWithString:[[[[GetData getRecording] valueForKey:@"url"]objectAtIndex:[[[GetData getRecording] valueForKey:@"ID"] indexOfObject:sender.titleLabel.text]]stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
        
        
    
    [RecordingAct playRecording:url];
        
    }
    
    
}

-(void)Postpone:(UIButton*)sender

{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.FromScreen=NO;
    
    [appDelegate Postpone:sender];
    
}

-(void)Take:(UIButton*)sender

{
    
   
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.FromScreen=NO;
    
    [appDelegate Take:sender];
  
    
}

-(void)Skip:(UIButton*)sender

{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.FromScreen=NO;
    [appDelegate Skip:sender];
    
}

-(void)fresh


{
    
    
    NSDate *now = [[NSDate alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(fireDate <= %@)", now];
    NSMutableArray *notificationArray = [[NSMutableArray alloc] initWithArray:[[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate]];
    
    

    
    NSArray *profileItem =[NSArray arrayWithObjects:@"Name",@"ID",@"Image",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Silence",@"Vibrate",nil];
    
 
    NSMutableArray *copy = [SaveCore getProfiles:profileItem];
  
    [NotificationList removeAllObjects];
      
   
    SectionList =[[NSMutableArray alloc]init];
    
    
   for (NSInteger i =([copy count]); i > 0; i--) {
        
        
        [SectionList addObject:[[NSMutableArray alloc]init]];
        
        
    }
    
    
     
        if ([SectionList count]==0) {
            
        }
    else
    {
    
    
     
        //////////////////////////////////////////////NSLog(@"notificationArray count %i", [notificationArray count]);
    
   for (NSInteger i =([notificationArray count]-1); i >= 0; i--) {
        
      
       UILocalNotification *notif = [notificationArray objectAtIndex:i];
       

        
       
        if (notif.repeatInterval ==0) {
            
        }
        else
            
        {
        [self SeeifEndingDateIsPast:notif];
        
      
            if ([[copy valueForKey:@"ID"] indexOfObject:[notif.userInfo valueForKey:@"ID"]]>100) {
                
            }
            else
                
            {
                [[SectionList objectAtIndex:[[copy valueForKey:@"ID"] indexOfObject:[notif.userInfo valueForKey:@"ID"]]] addObject:notif];
                
                
            }
            
        }
        
    }
    
   
    
   for (NSInteger i=0 ; i<[SectionList count]; i++) {
        
        NSDictionary *countriesLivedInDict = [NSDictionary dictionaryWithObject:[SectionList objectAtIndex:i] forKey:@"Name"];
        
        NSArray *array = [countriesLivedInDict objectForKey:@"Name"];
  
        
        if ([array count] ==0) {
            
        }
        else
        {
            NSDictionary *countriesLivedInDict = [NSDictionary dictionaryWithObject:[SectionList objectAtIndex:i] forKey:@"Name"];
            
            if ([NotificationList containsObject:NotificationList]) {
                
            }
            else
            {
            [NotificationList addObject:countriesLivedInDict];
                
            }
        }
        
        
        
        
    }
    
        
  
    
    if ([NotificationList count] ==0) {
        
    }
    else
    {
        table.delegate = self;
        table.dataSource = self;
        
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        if ([NotificationList count] <=1) {
            [self setSingle];
            
            
            [appDelegate.taketook setSingle];
            
            [appDelegate.postpone setSingle];
            
            [appDelegate.skiptake setSingle];
            
            
            
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            NSDictionary *dictionary = [NotificationList objectAtIndex:0];
            NSArray *array = [dictionary objectForKey:@"Name"];
            
            if ([array count] ==1) {
                
                   [self setSingle];
                
                [appDelegate.taketook setSingle];
                
                [appDelegate.postpone setSingle];
                
                [appDelegate.skiptake setSingle];
                
                
                UILocalNotification *notif = [array objectAtIndex:0];
                
                
                [appDelegate.CurrentReminder setDictionary:notif.userInfo];
                
           
                
                [appDelegate.taketook setSingle];
                [appDelegate.postpone setMultiple];
            }
            else
            {
                 [self setMultiple];
                
                UILocalNotification *notif = [array objectAtIndex:0];
                
                   [appDelegate.CurrentReminder setDictionary:notif.userInfo];
      
                
                
                [appDelegate.taketook setMultiple];
               
                [appDelegate.postpone setMultiple];
                
                 [appDelegate.skiptake setMultiple];
            }
            
            
           
        }
        else
        {
            
            [self setMultiple];
            
            [appDelegate.taketook setMultiple];
            
            [appDelegate.postpone setMultiple];
            
            [appDelegate.skiptake setMultiple];
        }
        
        
    }
          
          
       
    
    
    }
    

    

    
    
}




-(NSDate*)calculatenow

{
    
    NSDate *now = [[NSDate alloc] init];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:now];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:now];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]-1];
    [dateComps setMinute:[timeComponents minute]];
    [dateComps setSecond:0];
    
    
    return [calendar dateFromComponents:dateComps];
}


-(void)checkdate:(UILocalNotification*)notif

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    

   
    
    
    NSDate *currDate = [NSDate date];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     
    [dateFormatter setDateFormat:[appDelegate convertString:@"dd-MM-yyyy hh:mm:00"]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
   
    [dateFormat setDateFormat:[appDelegate convertString:@"dd-MM-yyyy hh:mm:00"]];
    
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:notif.fireDate];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:notif.fireDate];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    [dateCompsEarly setDay:[dateComponents day]];
    [dateCompsEarly setMonth:[dateComponents month]];
    [dateCompsEarly setYear:[dateComponents year]];
    [dateCompsEarly setHour:[timeComponents hour]];
    [dateCompsEarly setMinute:timeComponents.minute];
    [dateCompsEarly setSecond:0];
    NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
    
    
    NSString *prettyVersion1 = [dateFormatter stringFromDate:currDate];
    NSString *prettyVersion2 = [dateFormatter stringFromDate:itemNext];
    
    

    
    
    NSDate *dt1 = [dateFormat dateFromString:prettyVersion1];
    NSDate *dt3 = [dateFormat dateFromString:prettyVersion2];
    
    
    if  ([dt1 compare:dt3]==NSOrderedDescending)
        
    {
        
       
        
    }
    else if ([dt1 compare:dt3]==NSOrderedAscending)
        
        
    {
      
    }
    else if ([dt1 compare:dt3]==NSOrderedSame)
        
        
    {
    }
    

    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [NotificationList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    NSDictionary *dictionary = [NotificationList objectAtIndex:indexPath.section];
    NSArray *array = [dictionary objectForKey:@"Name"];
    
    
    UILocalNotification *notif = [array  objectAtIndex:indexPath.row];
    
    if ([[notif.userInfo valueForKey:@"Location"] isEqualToString:@" "]) {
        
        
        
        if ([[notif.userInfo valueForKey:@"Instructions"] isEqualToString:@" "]) {
            
 
          return 70;
            
        }
        else
        {
               return [self calculateTextHeight:[notif.userInfo valueForKey:@"Instructions"] width:240 fontSize:14]+60;
        }
        
    
        
    }
    
    else
    {
        
       return [self calculateTextHeight:[NSString stringWithFormat:@"%@\n%@",[notif.userInfo valueForKey:@"Location"], [notif.userInfo valueForKey:@"Instructions"]] width:240 fontSize:14]+60;
    }
    
    
}


- (CGFloat)calculateTextHeight:(NSString*)text width:(CGFloat)nWidth fontSize:(CGFloat)nFontSize{
	CGSize constraint = CGSizeMake(nWidth, 20000.0f);
	CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:nFontSize] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
	CGFloat height = MAX(size.height, MIN_CELL_HEIGHT);
	return height+10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    NSDictionary *dictionary = [NotificationList objectAtIndex:section];
    NSArray *array = [dictionary objectForKey:@"Name"];
    
    
    UILocalNotification *notif = [array objectAtIndex:0];
    
    NSString *result = [NSString stringWithFormat:@"   %@ %@", NSLocalizedString(@"For:",nil),[notif.userInfo valueForKey:@"Profile"]];
    
    
    UILabel *sectionHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 15)];
//    sectionHeader.backgroundColor = [UIColor colorWithRed:0.129 green:0.369 blue:0.463 alpha:1.000]; //01-07-2017 changes
    sectionHeader.backgroundColor = [UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.textAlignment =NSTextAlignmentLeft;
    sectionHeader.font = [UIFont boldSystemFontOfSize:12];
    sectionHeader.text =  result;
    
    return sectionHeader;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //Number of rows it should expect should be based on the section
    NSDictionary *dictionary = [NotificationList objectAtIndex:section];
    NSArray *array = [dictionary objectForKey:@"Name"];

    
    return [array count];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSDictionary *dictionary = [NotificationList objectAtIndex:section];
    NSArray *array = [dictionary objectForKey:@"Name"];
    
    UILocalNotification *notif = [array objectAtIndex:0];
    
    NSString *result = [notif.userInfo valueForKey:@"Profile"];
    
    
    return [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"For:",nil), result];
}


-(void)setMultiple

{
       ////////////////////////////////////////////////////NSLog(@"setMultiple");
    
    [table setFrame:CGRectMake(20, 50, 260, 200)];
    
    UILabel *Label = (UILabel *)[self viewWithTag:(233)];
    [Label setFrame:CGRectMake(Label.frame.origin.x, Label.frame.origin.y, 280, 320)];
    
    
    NSArray *profileItem =[NSArray arrayWithObjects:NSLocalizedString(@"Take",nil),NSLocalizedString(@"Skip",nil),NSLocalizedString(@"Postpone",nil), nil];
    
    
    
   for (NSInteger i=0 ; i<[profileItem count]; i++) {
        
        
        UIButton *button = (UIButton *)[self viewWithTag:(200+i)];
        [button setCenter:CGPointMake(button.center.x, 290)];
        
    }
    
}

-(void)setSingle

{
    
    

    
    [table setFrame:CGRectMake(20, 50, 260, 130)];
    
    UILabel *Label = (UILabel *)[self viewWithTag:(233)];
    [Label setFrame:CGRectMake(Label.frame.origin.x, Label.frame.origin.y, 280, 240)];
    
    NSArray *profileItem =[NSArray arrayWithObjects:NSLocalizedString(@"Take",nil),NSLocalizedString(@"Skip",nil),NSLocalizedString(@"Postpone",nil), nil];
    
   for (NSInteger i=0 ; i<[profileItem count]; i++) {
        
        
        UIButton *button = (UIButton *)[self viewWithTag:(200+i)];
        
        
        [button setCenter:CGPointMake(button.center.x, 210)];
        
    }
}




- (void)tableView:(UITableView *)tableView willDisplayCell:(AlertCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    NSDictionary *dictionary = [NotificationList objectAtIndex:indexPath.section];
    NSArray *array = [dictionary objectForKey:@"Name"];
    [cell FillAllItems:[array objectAtIndex:indexPath.row] index:indexPath.row];
    
    [cell.label setAlpha:1];
    
    
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{

}




- (AlertCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    NSString *identifier = @"CellIdentifier";
    AlertCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
	if (cell == nil) {
        
        cell = [[AlertCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        

        
     
        
    }
    
    else
        
    {
        
    }
    
    //05-07-2017  add new condition for change cell alertnative color change
    if (indexPath.row % 2 == 0)
    {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    else
    {
        //219, 234, 242
        [cell setBackgroundColor:[UIColor colorWithRed:251.0f/255.0f green:241.0f/255.0f blue:235.0f/255.0f alpha:1.0f]];
    }
    
    
    
    return cell;
    
}


-(void)Choice:(UIButton*)sender

{
    
    
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    
    
    
    [UIView setAnimationDuration:0.5f];
    
    
    
    if (self.frame.origin.y == 100) {
        
        [self setFrame:CGRectMake(10, 480, 300, 300)];
    }
    else
    {
        
        [self setFrame:CGRectMake(10, 100, 300, 300)];
        
    }
    
    
    
    [UIView commitAnimations];
    
    
  
    
}


-(void)changeData:(NSMutableDictionary*)sender date:(NSDate*)fireDate

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    

    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     
    [dateFormat2 setDateFormat:@"hh:mm"];
    
  
    
    
     NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
 
    [format setDateFormat:@"dd/MM/yyyy"];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:[appDelegate convertString:@"hh:mm a"]];
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:fireDate];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:fireDate];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    [dateCompsEarly setDay:[dateComponents day]];
    [dateCompsEarly setMonth:[dateComponents month]];
    [dateCompsEarly setYear:[dateComponents year]];
    [dateCompsEarly setHour:[timeComponents hour]];
    [dateCompsEarly setMinute:timeComponents.minute];
    [dateCompsEarly setSecond:0];
    NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
    
    

    [Date setText:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:itemNext]]];
    
    
    
    [headShot setImage:[UIImage imageWithContentsOfFile:[sender valueForKey:@"Image"]]];
    
    
    
    
    //[Name setText:[sender valueForKey:@"Name"]];
    
    
}


-(BOOL)SeeifEndingDateIsPast:(UILocalNotification *)notification {
    
    
    BOOL set;

    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd 00:00:01 PM +0000"];
    
    
    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    NSDate *let = [dateConvert dateFromString:[notification.userInfo valueForKey:@"Duration"]];
   
   
    
    if([[notification.userInfo valueForKey:@"Duration"] isEqualToString:@"Nooit"]||[[notification.userInfo valueForKey:@"Duration"] isEqualToString:@"Never"]||[[notification.userInfo valueForKey:@"Duration"]isEqualToString:@"Nie"]||[[notification.userInfo valueForKey:@"Duration"] isEqualToString:@"Jamais"]||[[notification.userInfo valueForKey:@"Duration"] isEqualToString:@"Mai"]||[[notification.userInfo valueForKey:@"Duration"] isEqualToString:@"Ποτέ"]
       ||[[notification.userInfo valueForKey:@"Duration"] isEqualToString:@"אף פעם"])
        
    {
    
        set = YES;
        
    }
    else
    {
        if  ([let compare:notification.fireDate]==NSOrderedSame)
            
        {
            
            set = NO;
         
            
        }
         else if  ([let compare:notification.fireDate]==NSOrderedDescending)
            
            
        {
         
            
            set = YES;
            
        }
        else {
            
            set = NO;
        }
    }
    
    return set;
}




@end
