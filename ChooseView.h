//
//  ChooseView.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FrequencyViewController, Daybutton;
@interface ChooseView : UIView
@property (nonatomic, retain) FrequencyViewController *parantIt;

-(void)getParant:(UIViewController*)parant;
-(void)cleanup;
-(void)changeToDate:(NSDate*)select;
-(void)changeToDateIt:(NSDate*)select;
-(void)change:(Daybutton*)sender;
@end
