//
//  ChooseView.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "ChooseView.h"
#import <QuartzCore/QuartzCore.h>
#import "FrequencyViewController.h"
#import "Daybutton.h"
#import "TimeSchedulare.h"
@implementation ChooseView
@synthesize parantIt;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        
        
        
        NSArray *names =[NSArray arrayWithObjects:@"Su,1",@"Mo,2",@"Tu,3",@"We,4",@"Th,5",@"Fr,6",@"Sa,7", nil];
       
       for (NSInteger i =0; i < [names count]; i++) {
            
       
            
             NSArray *numbers2 = [[names objectAtIndex:i] componentsSeparatedByString:@","];
          
            
            Daybutton *NameButton = [[Daybutton alloc]  initWithFrame:CGRectMake(((307/7)*i), 0, (307/7), 40)];
            [NameButton setTitle:NSLocalizedString([numbers2 objectAtIndex:0],nil) forState:UIControlStateNormal];
            [NameButton setNameDate:[names objectAtIndex:i]];
            [NameButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
            [NameButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [NameButton setTag:140+i];
//            [NameButton setTitleColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000] forState:UIControlStateNormal];//28-06-2017 changes
            [NameButton setTitleColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000] forState:UIControlStateNormal];
            [NameButton.layer setBorderWidth:1];
//            [NameButton.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; // 28-06-2017 changes
            [NameButton.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
           [NameButton addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
            [NameButton setBackgroundColor:[UIColor whiteColor]];
            [self addSubview:NameButton];
            
        }
        
  
        
        
        
    }
    return self;
}

-(void)change:(Daybutton*)sender

{
    
    
    
    
       NSArray *numbers2 = [sender.nameDate componentsSeparatedByString:@","];
    
    
//    if ([sender.backgroundColor isEqual:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]) { //28-06-2017 changes
    if ([sender.backgroundColor isEqual:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]]) {

//        [sender setTitleColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000] forState:UIControlStateNormal];; //28-06-2017 changes
        [sender setTitleColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000] forState:UIControlStateNormal];;

        [sender setBackgroundColor:[UIColor whiteColor]];
   
       
        
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
        [set setObject:NSLocalizedString([numbers2 objectAtIndex:0],nil) forKey:@"Day"];
        [set setObject:[numbers2 objectAtIndex:1]  forKey:@"Number"];
        
       [parantIt.DayRow removeObject:set];
     
        
      
    }
    else
    {
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [sender setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [sender setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
   
        
        
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
        [set setObject:NSLocalizedString([numbers2 objectAtIndex:0],nil) forKey:@"Day"];
        [set setObject:[numbers2 objectAtIndex:1]  forKey:@"Number"];
        
        if ([parantIt.DayRow containsObject:set]) {
            
        }
        else
        {
        
        [parantIt.DayRow addObject:set];
            
        }
    
    }
       
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Number" ascending:TRUE];
    [parantIt.DayRow  sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
  
    
    
}

-(void)changeToDate:(NSDate*)select

{
    
        for (NSInteger i =1; i < 8; i++) {
    
             Daybutton *NameButton = (Daybutton *)[self viewWithTag:(140+i)];
    
             if ([parantIt.DayRow containsObject:NameButton.titleLabel.text]) {
                 
                 [self change:NameButton];
                 
                 [parantIt.DayRow addObject:NameButton.titleLabel.text];
             }
             
          
    
             unsigned units = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
             NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
             NSDateComponents *components = [gregorian components:units fromDate:select];
             [components setDay:[components day]+i];
    
             NSDate *thisDate = [gregorian dateFromComponents:components];
    
    
             NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
             [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    
             [dateFormatter setDateFormat:@"EE"];
    
             [NameButton setTitle:[dateFormatter stringFromDate:thisDate] forState:UIControlStateNormal];
             [NameButton setTotalDate:thisDate];
             
             
           
          /*
             
             if ([parantIt.DayRow containsObject:NameButton.titleLabel.text]) {
                 
                 [self change:NameButton];
                 
                 if ([parantIt.NumberOfDays containsObject:[self getnumberOfday:[dateFormatter stringFromDate:[dateFormatter dateFromString:NameButton.titleLabel.text]]]]) {
                     
                 }
                 else
                 {
                   [parantIt.NumberOfDays addObject:[self getnumberOfday:[dateFormatter stringFromDate:[dateFormatter dateFromString:NameButton.titleLabel.text]]]];
                     
                       [parantIt.DayRow addObject:NameButton.titleLabel.text];
                     
                 }
                 
                 
               
             }
           
           */
            
             
             
         }

 
    
}



-(NSString*)getnumberOfday:(NSString*)day
{
    NSString *StringReturn;
    
    
    NSArray *names =[NSArray arrayWithObjects:@"Su",@"Mo",@"Tu",@"We",@"Th",@"Fr",@"Sa", nil];
    NSArray *number =[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7", nil];
    
    
   for (NSInteger i=0 ; i<[names count]; i++) {
        
        if ([day isEqualToString:NSLocalizedString([names objectAtIndex:i],nil)]) {
           
            StringReturn =[number objectAtIndex:i];
        }
        
        
    }

    return StringReturn;
    
}



-(void)cleanup

{
        for (NSInteger i =1; i < 8; i++) {
           
             
              Daybutton *NameButton = (Daybutton *)[self viewWithTag:(140+i)];
//             [NameButton setTitleColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000] forState:UIControlStateNormal];; //28-06-2017 chanegs
            [NameButton setTitleColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000] forState:UIControlStateNormal];;
             [NameButton setBackgroundColor:[UIColor whiteColor]];
             
         }
}


-(void)getParant:(UIViewController*)parant

{
    
    parantIt =(FrequencyViewController*)parant;
}



@end
