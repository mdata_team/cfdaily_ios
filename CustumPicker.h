//
//  CustumPicker.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 04-09-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrengthViewControllerScroll.h"
@interface CustumPicker :UIPickerView <UIPickerViewDelegate, UIPickerViewDataSource>
{
    
    NSMutableArray *placeArray;
     NSMutableArray *docelist;
    NSString *setLocation;
    NSInteger number;
    NSInteger countit;
    StrengthViewControllerScroll *Parantit;
}

@property (nonatomic, strong) NSMutableArray *placeArray;
@property (nonatomic, strong) NSMutableArray *docelist;
@property (nonatomic, strong) NSString *setLocation;
@property (nonatomic, assign) NSInteger number;
@property (nonatomic, assign) NSInteger countit;
@property (nonatomic, strong) StrengthViewControllerScroll *Parantit;

-(void) geloadContenst:(NSInteger)set numbers:(NSInteger)count getparant:(UIViewController*) send;

@end
