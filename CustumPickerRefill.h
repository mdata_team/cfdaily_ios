//
//  CustumPicker.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 04-09-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrengthViewControllerScroll.h"
@interface CustumPickerRefill :UIPickerView <UIPickerViewDelegate, UIPickerViewDataSource>
{
    
    NSMutableArray *placeArray;
    NSString *setLocation;
    NSInteger number;
    NSInteger countit;
    StrengthViewControllerScroll *Parantit;
}

@property (nonatomic, copy) NSString *setLocation;
@property (nonatomic, assign) NSInteger number;
@property (nonatomic, assign) NSInteger countit;
@property (nonatomic, strong) StrengthViewControllerScroll *Parantit;

-(void) geloadContenst:(NSInteger)set numbers:(NSInteger)count getparant:(UIViewController*) send;

@end
