//
//  PickerView.m
//  Vertex-Dental
//
//  Created by Jeffrey Snijder on 28-02-11.
//  Copyright 2011 Livecast. All rights reserved.
//

#import "CustumPickerRefill.h"
#import "StrengthViewControllerScroll.h"
#import "AppDelegate.h"
#import "GetData.h"


@implementation CustumPickerRefill
@synthesize setLocation;
@synthesize number;
@synthesize countit;
@synthesize Parantit;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
     
        placeArray =[[[[[GetData getDrugTypeStrength:appDelegate.TypeMed] valueForKey:@"Dose_unit"] objectAtIndex:0] componentsSeparatedByString:@","] mutableCopy];
    }
    return self;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    [Parantit chosenMonth:NSLocalizedString([placeArray objectAtIndex:row],nil)];

   
     
   
    
}

- (NSString *) floatToString:(NSString*) val {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
 
    
    NSArray *numbers2 = [val componentsSeparatedByString:@"."];
    
    if ([numbers2 count]==2)
        
    {
        NSString *ret = [NSString stringWithFormat:@"%@", [numbers2 objectAtIndex:1]];
        NSInteger index = (int)[ret length] - 1;
        BOOL trim = FALSE;
        while (
               ([ret characterAtIndex:index] == '0' ||
                [ret characterAtIndex:index] == '.')
               &&
               index > 0)
        {
            index--;
            trim = TRUE;
        }
        if (trim)
            ret = [ret substringToIndex: index +1];
        
        
        if ([ret intValue]>0)
            
            
            return [NSString stringWithFormat:@"%i.%@ %@",[[numbers2 objectAtIndex:0] intValue],ret,  appDelegate.strength_unit];
        
        else
            
            return [NSString stringWithFormat:@"%i %@",[[numbers2 objectAtIndex:0] intValue], appDelegate.strength_unit];
   
    }
    
    else
    {
        
        
        NSString *ret = [NSString stringWithFormat:@"%@", val];
        return [NSString stringWithFormat:@"%i",[ret intValue]];
    }
}



- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	CGFloat componentWidth;
	componentWidth = 60;	// first column size is wider to hold names
	return componentWidth;
}



-(void) viewDidDisappear:(BOOL)animated
{
}

- (NSString*)pickerView:(UIPickerView*)pv titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return NSLocalizedString([placeArray objectAtIndex:row],nil);
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
	UILabel *retval = (id)view;
	if (!retval) {
		retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
	}
    
 	retval.text = NSLocalizedString([placeArray objectAtIndex:row],nil);
	retval.font = [UIFont boldSystemFontOfSize:12];
     retval.adjustsFontSizeToFitWidth = YES;
	retval.textAlignment = NSTextAlignmentCenter;
	retval.backgroundColor = [UIColor clearColor];
	
	return retval;
}

-(void) geloadContenst:(NSInteger)set numbers:(NSInteger)count getparant:(UIViewController*) send
{
    self.delegate = nil;
    number = set;
    countit =count;
    
    

    Parantit = (StrengthViewControllerScroll*)send;
    
    number = set;
    countit =count;
	
   
    
    
}

#pragma mark UIPickerViewDataSource methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pv
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView*)pv numberOfRowsInComponent:(NSInteger)component
{
    return [placeArray count];
  
}







@end
