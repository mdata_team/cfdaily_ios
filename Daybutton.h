//
//  Daybutton.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 03-05-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Daybutton : UIButton
@property (nonatomic, strong) NSDate *totalDate;
@property (nonatomic, strong) NSString *nameDate;

@end
