//
//  Disclaimer.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 18-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalClass.h"
#import "MBProgressHUD.h"

@class StartViewController;
@interface Disclaimer : UIView<UIWebViewDelegate>
@property (nonatomic, retain) IBOutlet UITextView *DiscaimerText;
@property (nonatomic, retain) IBOutlet UIButton *Agree;
@property  (nonatomic, retain) StartViewController *Parantit;
-(void)setParant:(UIViewController *)set;


@end
