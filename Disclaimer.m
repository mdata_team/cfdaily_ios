//
//  Disclaimer.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 18-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "Disclaimer.h"
#import "Reachability.h"
#import "WebServicesClass.h"

@implementation Disclaimer
{
    UIWebView *webview;

}
@synthesize DiscaimerText;
@synthesize Agree;
@synthesize Parantit;

#define kReachabilityChangedNotification @"kNetworkReachabilityChangedNotification"

#define APIURL @"http://mdataapps.com/cfdaily/api.php?action=get_tc_version"

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code


        DiscaimerText = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-155)];
        [DiscaimerText setBackgroundColor:[UIColor clearColor]];
        //        [DiscaimerText setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]]; //29-06-2017 changes
        [DiscaimerText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];

        [DiscaimerText setTag:123+1];
        [DiscaimerText setFont:[UIFont fontWithName:@"Helvetica" size:16]];


      
        webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-155)];
        [webview setBackgroundColor:[UIColor clearColor]];


        [self addSubview:webview];

        //        [DiscaimerText setUserInteractionEnabled:YES];
        //        [DiscaimerText setEditable:NO];
        //        [DiscaimerText setScrollEnabled:YES];
        //        [self addSubview:DiscaimerText];


        Agree = [[UIButton alloc] initWithFrame:CGRectMake((self.frame.size.width-295)/2, self.frame.size.height-150, 295,  36)];

        // [Agree setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //27-06-2017 change
        [Agree setBackgroundColor:[UIColor colorWithRed:0.455 green:0.514 blue:0.733 alpha:1.000]];
        [Agree setTitle:NSLocalizedString(@"Accept to continue",nil) forState:UIControlStateNormal];
        Agree.titleLabel.adjustsFontSizeToFitWidth = YES;
        [Agree.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
        [Agree setShowsTouchWhenHighlighted:NO];
        [Agree setAutoresizesSubviews:YES];
        [self addSubview:[self setbutton:Agree]];


        [self initial_setup];

    }
    return self;
}

-(void)termAndConditions_in_Web{
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {

        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_en.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];


    }

    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
    {
        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_fr.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];


    }

    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
    {
        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_dt.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
    {

        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_ge.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
    {
        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_it.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }

    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
    {

        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_sp.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
    {

        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_dn.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
    {
        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_nw.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];
        
    }
}
-(void)removefile
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"htmlTandC.html"];
    [fileManager removeItemAtPath:filePath error:NULL];
}

- (void)SaveInfo :(NSString *)HTMLString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"htmlTandC.html"];
    NSString *helloStr = HTMLString;
    NSError *error;
    [helloStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
}



#pragma mark - Void Methods

-(BOOL)isNetworkReachable
{
        BOOL internet_reachable = false;
    
        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
        NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
        if (networkStatus == NotReachable)
        {
            NSLog(@"Please check your network connection");
            internet_reachable = false;
        }
        else
        {
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            [reachability startNotifier];
    
            NetworkStatus status = [reachability currentReachabilityStatus];
    
            if(status == NotReachable)
            {
                //No internet
                internet_reachable = false;

                NSLog(@"**** Not Reachable ****");
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"htmlTandC.html"];
                NSString* htmlString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];

                if (htmlString != NULL)
                {

                    [Agree setEnabled:YES];
                    [webview loadHTMLString:htmlString baseURL:nil];

                }else
                {
                    [Agree setEnabled:NO];
                    [GlobalClass ShowAlertwithTitle:@"Alert" Message:@"Please turn on internet connection" okLabelTitle:@"Ok"];
                }
                

            }
            else if (status == ReachableViaWiFi || status == ReachableViaWWAN)
            {
                //Connected to WiFi
                internet_reachable = true;

                [Agree setEnabled:YES];

                [self termAndConditions_in_Web];

            }

        }
    
        return internet_reachable;
}


-(void)initial_setup
{

     webview.delegate = self;

      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isNetworkReachable) name:kReachabilityChangedNotification object:nil];

   Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [Agree setEnabled:NO];

        NSLog(@"**** Not Reachable ****");
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"htmlTandC.html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];

        if (htmlString != NULL)
        {
              [Agree setEnabled:YES];
            [webview loadHTMLString:htmlString baseURL:nil];

        }
        else
        {
              [Agree setEnabled:NO];
            [GlobalClass ShowAlertwithTitle:@"Alert" Message:@"Please turn on internet connection" okLabelTitle:@"Ok"];
        }

    }

   else if
        (networkStatus == ReachableViaWiFi || networkStatus == ReachableViaWWAN){

            [self termAndConditions_in_Web];

        }
}

-(UIButton*)setbutton:(UIButton*)sender
{

    CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
    shineLayer2.frame = sender.layer.bounds;
    shineLayer2.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          nil];
    shineLayer2.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];


    [sender.layer addSublayer:shineLayer2];
    [sender.layer setMasksToBounds:YES];
    [sender.layer setCornerRadius:4];
    [sender.layer setBorderWidth:1.5];
    [sender.layer setBorderColor:sender.backgroundColor.CGColor];
    
    return sender;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {


    [MBProgressHUD hideAllHUDsForView:webview animated:YES];

}

- (void)webViewDidStartLoad:(UIWebView *)webView {

    [MBProgressHUD showHUDAddedTo:webView animated:YES];

}

-(void)setParant:(UIViewController *)set

{
    Parantit =(StartViewController*)set;

    NSLog(@"%@", Parantit);
    
    [Agree addTarget:Parantit action:@selector(removeDisclaimer) forControlEvents:UIControlEventTouchUpInside];
    
}


@end

