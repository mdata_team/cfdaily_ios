//
//  DownloadImages.h
//  VVS
//
//  Created by Jeffrey Snijder on 12-12-12.
//  Copyright (c) 2012 Kieran Lafferty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DownloadImages : NSObject

+(void) Download: (NSString*)urlsting;
+(UIImage*) Download2: (NSString*)urlsting;
+(NSString*) getURlFromTeks:(NSString*)tekst;
@end
