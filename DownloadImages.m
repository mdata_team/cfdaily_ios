//
//  DownloadImages.m
//  VVS
//
//  Created by Jeffrey Snijder on 12-12-12.
//  Copyright (c) 2012 Kieran Lafferty. All rights reserved.
//

#import "DownloadImages.h"

@implementation DownloadImages




+(void) Download: (NSString*)urlsting

{
    
    NSArray *listItems = [urlsting componentsSeparatedByString:@"/"];
    
    NSString *naamBeeld = [listItems objectAtIndex:[listItems count]-1];
    
    
    NSURL *url = [NSURL URLWithString:urlsting];
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    
    UIImage *image2 = [[UIImage alloc] initWithData:data];
    
     // en zegt waar het heen moet
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir,naamBeeld];
    NSData *data2 = [NSData dataWithData:UIImageJPEGRepresentation(image2, 1.0f)];
    [data2 writeToFile:jpegFilePath atomically:YES];
    
    
}


+(UIImage*) Download2: (NSString*)urlsting

{

    NSArray *listItems = [urlsting componentsSeparatedByString:@"/"];
    
    NSString *naamBeeld = [listItems objectAtIndex:[listItems count]-1];
    
    NSFileManager *FileManager = [NSFileManager defaultManager];
    
    //Get the complete users document directory path.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    //Get the first path in the array.
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //Create the complete path to the database file.
    NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/Caches/%@",naamBeeld]];
 
    
    //Check if the file exists or not.
    BOOL success = [FileManager fileExistsAtPath:databasePath];
    
    
    if (success) {
        
        
        
        NSString *documentsDirectory2 = [paths objectAtIndex:0];
        NSString *filePath2 = [documentsDirectory2 stringByAppendingPathComponent:naamBeeld];
        UIImage *image2 = [UIImage imageWithContentsOfFile:filePath2];
        
        return image2;
    }
    else
    {
  
        
        
        
        NSURL *url = [NSURL URLWithString:urlsting];
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        
        UIImage *image2 = [[UIImage alloc] initWithData:data];
        
        // en zegt waar het heen moet
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir,naamBeeld];
        NSData *data2 = [NSData dataWithData:UIImageJPEGRepresentation(image2, 1.0f)];
        [data2 writeToFile:jpegFilePath atomically:YES];
        
            return image2;

 
    }

    

    
    
}

+(NSString*) getURlFromTeks:(NSString*)tekst

{

    NSScanner *theScanner;
    NSString *result = nil;

    theScanner = [NSScanner scannerWithString:tekst];

    while ([theScanner isAtEnd] == NO) {

        [theScanner scanUpToString:@"src=" intoString:NULL] ;

        [theScanner scanUpToString:@">" intoString:&result] ;

       
        tekst = [result stringByReplacingOccurrencesOfString: @"src=" withString:@""];

        if (tekst == NULL) {
            tekst = @"-";
        }

    }

    
    return tekst;

}




@end
