//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "FrequencyViewController.h"
#import "TimersTableView.h"
#import "HoursViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "DurationViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "SchedularView.h"
#import "ChooseView.h"
#import "TimeSchedulare.h"
#import "Addbutton.h"
#import "TekstIDLabel.h"
#import "ScedulareView.h"
#import "Daybutton.h"


@interface FrequencyViewController ()

@end

@implementation FrequencyViewController
@synthesize parantIt;
@synthesize NumberOfDays;
@synthesize DayinaRow;
@synthesize Schedule;
@synthesize EveryAction;
@synthesize chosenFrequentcy;
@synthesize Frequency;
@synthesize add_button;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize Choicetimer;
@synthesize table;
@synthesize Title;
@synthesize IDset;
@synthesize timeContent;
@synthesize monthView;
@synthesize StartingTimeText;
@synthesize Every;
@synthesize EveryView;
@synthesize EveryViewText;
@synthesize DayRow;
@synthesize Starting;
@synthesize StartingView;
@synthesize Start;
@synthesize Name;
@synthesize Ending;
@synthesize EndingView;
@synthesize EndingViewText;
@synthesize End;
@synthesize Choose;
@synthesize Chooseview;
@synthesize times;
@synthesize EveryLabel;
@synthesize CopyLabel;

@synthesize StartingTime;
@synthesize StartingTimeView;
@synthesize StartTime;



-(void)loadView{


    [super loadView];
}

-(void)whatLabelFrequency:(TekstIDLabel*)set

{
    parantLabel =set;
    
}

-(void)whatLabel:(TekstIDLabel*)set

{
    parantLabel =set;
    
}



-(void)setfreguency:(NSString*)ID


{
    
    //////////NSLog(@"setfreguency");
    
    IDset =ID;
    
    
    
    
    DayinaRow=[[NSMutableArray alloc]init];
    
    
    
    
    //////////NSLog(@"DayinaRow %@", DayinaRow);
    
    
    
    
     
   
}

-(void)viewDidAppear:(BOOL)animated

{
    
 
    
    
    
    /*
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [format setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    if ([format dateFromString:Name.text]) {
        
        [Chooseview changeToDate:[format dateFromString:Name.text]];
        
    }
    else
    {
        
    }
    
    
    
    
    if ([DayinaRow count]>0) {
   
    
    if ([format dateFromString: [[DayinaRow valueForKey:@"Starting"] objectAtIndex:0]]) {
        
        [Chooseview changeToDateIt:[format dateFromString: [[DayinaRow valueForKey:@"Starting"] objectAtIndex:0]]];
     [Name setText:[[DayinaRow valueForKey:@"Starting"] objectAtIndex:0]];
      
        
        
    }
        
    }
    
     */
    
    //////NSLog(@"DayinaRow DayinaRow%@", DayinaRow);
    
    
    
    
    

}

- (void)viewDidLoad {


    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
 
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    timeContent =[[NSMutableDictionary alloc]init];
    
    DayRow =[[NSMutableArray alloc]init];
        NumberOfDays =[[NSMutableArray alloc]init];
  
   
    
    Starting = [[UILabel alloc] initWithFrame:CGRectMake(15, 58, self.view.frame.size.width-20, 40)];
    [Starting setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [Starting setFont:[UIFont boldSystemFontOfSize:14]];
    [Starting setText:@"Starting"];
    [Starting setBackgroundColor:[UIColor clearColor]];
    [Starting setNumberOfLines:3];
    [self.view addSubview:Starting];
      [Starting setAlpha:0];
    
    
    
    StartingView = [[UIView alloc] initWithFrame:CGRectMake(10, 90, self.view.frame.size.width-20, 40)];
    [StartingView setBackgroundColor:[UIColor whiteColor]];
    [StartingView.layer setCornerRadius:10];
    [StartingView.layer setBorderWidth:1.5];
    [StartingView.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [StartingView.layer setMasksToBounds:YES];
    [self.view addSubview:StartingView];
    [StartingView setAlpha:0];
    
     
    
    
    
    Start = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3, 60/2, 72/2)];
    [Start setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
    // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
    [Start setTag:170];
    [Start addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [StartingView addSubview:Start];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [format setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    
     Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, StartingView.frame.size.width, 40)];
    [Name setTextColor:[UIColor blackColor]];
    [Name setFont:[UIFont systemFontOfSize:14]];
    [Name setText:[format stringFromDate:now]];
    [Name setTextAlignment:NSTextAlignmentLeft];
    [Name setTitle:@"Starting"];
    [Name setTag:120];
    [Name setBackgroundColor:[UIColor clearColor]];
    [Name setNumberOfLines:3];
    [StartingView addSubview:Name];
    
    
    [timeContent setObject:[format stringFromDate:now] forKey:@"Starting"];
     [timeContent setObject:@" " forKey:@"Ending"];
     [timeContent setObject:@" " forKey:@"Time"];
     [timeContent setObject:@" " forKey:@"Date"];
     [timeContent setObject:@" " forKey:@"Scedulare"];
     [timeContent setObject:@" " forKey:@"Days"];
    
    
    Ending = [[UILabel alloc] initWithFrame:CGRectMake(15, 120, self.view.frame.size.width-20, 40)];
    [Ending setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [Ending setFont:[UIFont boldSystemFontOfSize:14]];
    [Ending setText:@"Ending"];
    [Ending setBackgroundColor:[UIColor clearColor]];
    [Ending setNumberOfLines:3];
    [self.view addSubview:Ending];
    [Ending setAlpha:0];
    
    
    EndingView = [[UIView alloc] initWithFrame:CGRectMake(10, 150, self.view.frame.size.width-20, 40)];
    [EndingView setBackgroundColor:[UIColor whiteColor]];
    [EndingView.layer setCornerRadius:10];
    [EndingView.layer setBorderWidth:1.5];
    [EndingView.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [EndingView.layer setMasksToBounds:YES];
    [self.view addSubview:EndingView];
    [EndingView setAlpha:0];
    
    
    EndingViewText = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, EndingView.frame.size.width, 40)];
    [EndingViewText setTextColor:[UIColor blackColor]];
    [EndingViewText setFont:[UIFont systemFontOfSize:14]];
    [EndingViewText setText:@"Never"];
    [EndingViewText setTextAlignment:NSTextAlignmentLeft];
    [EndingViewText setTitle:@"Ending Time"];
    [EndingViewText setTag:120];
    [EndingViewText setBackgroundColor:[UIColor clearColor]];
    [EndingViewText setNumberOfLines:3];
    [EndingView addSubview:EndingViewText];
    
    
    End = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3, 60/2, 72/2)];
    [End setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
    // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
    [End setTag:171];
    [End addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [EndingView addSubview:End];
    
    
    Every = [[UILabel alloc] initWithFrame:CGRectMake(15, 180, self.view.frame.size.width-20, 40)];
    [Every setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [Every setFont:[UIFont boldSystemFontOfSize:14]];
    [Every setText:@"Every"];
    [Every setBackgroundColor:[UIColor clearColor]];
    [Every setNumberOfLines:3];
    [self.view addSubview:Every];
    [Every setAlpha:0];
    
    
    EveryView = [[UIView alloc] initWithFrame:CGRectMake(10, 210, self.view.frame.size.width-20, 40)];
    [EveryView setBackgroundColor:[UIColor whiteColor]];
    [EveryView.layer setCornerRadius:10];
    [EveryView.layer setBorderWidth:1.5];
    [EveryView.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [EveryView.layer setMasksToBounds:YES];
    [self.view addSubview:EveryView];
    [EveryView setAlpha:0];
    
    EveryAction = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3, 60/2, 72/2)];
    [EveryAction setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
    // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
    [EveryAction setTag:177];
    [EveryAction addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [EveryView addSubview:EveryAction];
    
    
    
    EveryViewText = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, StartingView.frame.size.width, 40)];
    [EveryViewText setTextColor:[UIColor blackColor]];
    [EveryViewText setFont:[UIFont systemFontOfSize:14]];
    [EveryViewText setText:@""];
    [EveryViewText setTextAlignment:NSTextAlignmentLeft];
    [EveryViewText setTitle:@"Every"];
    [EveryViewText setTag:120];
    [EveryViewText setBackgroundColor:[UIColor clearColor]];
    [EveryViewText setNumberOfLines:3];
    [EveryView addSubview:EveryViewText];
    
    
    EveryLabel = [[UILabel alloc] initWithFrame:CGRectMake(215, 0, 50, 40)];
    [EveryLabel setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [EveryLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [EveryLabel setText:@"Days"];
    [EveryLabel setBackgroundColor:[UIColor clearColor]];
    [EveryLabel setNumberOfLines:3];
    [EveryView addSubview:EveryLabel];
    [EveryLabel setAlpha:1];
    
    
    End = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3, 60/2, 72/2)];
    [End setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
   // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
    [End setTag:171];
    [End addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [EndingView addSubview:End];
    
    
    
    
  
    StartingTime = [[UILabel alloc] initWithFrame:CGRectMake(15, 240, self.view.frame.size.width-20, 40)];
    [StartingTime setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [StartingTime setFont:[UIFont boldSystemFontOfSize:14]];
    [StartingTime setText:@"Starting Time"];
    [StartingTime setBackgroundColor:[UIColor clearColor]];
    [StartingTime setNumberOfLines:3];
    [self.view addSubview:StartingTime];
    [StartingTime setAlpha:0];
    
    
    
    StartingTimeView = [[UIView alloc] initWithFrame:CGRectMake(10, 270, self.view.frame.size.width-20, 40)];
    [StartingTimeView setBackgroundColor:[UIColor whiteColor]];
    [StartingTimeView.layer setCornerRadius:10];
    [StartingTimeView.layer setBorderWidth:1.5];
    [StartingTimeView.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [StartingTimeView.layer setMasksToBounds:YES];
    [self.view addSubview:StartingTimeView];
    [StartingTimeView setAlpha:0];
    
    
    
    NSDateFormatter *format2 = [[NSDateFormatter alloc] init];
    [format2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
     [format2 setDateFormat:@"hh:mm a"];
    
    
    StartingTimeText = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, StartingView.frame.size.width, 40)];
    [StartingTimeText setTextColor:[UIColor blackColor]];
    [StartingTimeText setFont:[UIFont systemFontOfSize:14]];
    [StartingTimeText setText:[format2 stringFromDate:now]];
    [StartingTimeText setTextAlignment:NSTextAlignmentLeft];
    [StartingTimeText setTitle:@"c"];
    [StartingTimeText setTag:120];
    [StartingTimeText setBackgroundColor:[UIColor clearColor]];
    [StartingTimeText setNumberOfLines:3];
    [StartingTimeView addSubview:StartingTimeText];
    
    
    
    StartTime = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3, 60/2, 72/2)];
    [StartTime setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
    // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
    [StartTime setTag:178];
    [StartTime addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [StartingTimeView addSubview:StartTime];
    
    Choose = [[UILabel alloc] initWithFrame:CGRectMake(15, 180, self.view.frame.size.width-20, 40)];
    [Choose setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [Choose setFont:[UIFont boldSystemFontOfSize:14]];
    [Choose setText:@"Choose days of the week"];
    [Choose setBackgroundColor:[UIColor clearColor]];
    [Choose setNumberOfLines:3];
    [self.view addSubview:Choose];
      [Choose setAlpha:0];
    
    
    
    Chooseview = [[ChooseView alloc] initWithFrame:CGRectMake(10, 210, self.view.frame.size.width-20, 40)];
    [Chooseview setBackgroundColor:[UIColor whiteColor]];
    [Chooseview.layer setCornerRadius:10];
    [Chooseview.layer setBorderWidth:1.5];
    [Chooseview.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Chooseview.layer setMasksToBounds:YES];
    [self.view addSubview:Chooseview];
      [Chooseview setAlpha:0];
    
    
    [Chooseview getParant:self];
    
    
    
    
    
    
   times = [[UILabel alloc] initWithFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
    [times setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [times setFont:[UIFont boldSystemFontOfSize:14]];
    [times setText:@"Time(s) per Day"];
    [times setBackgroundColor:[UIColor clearColor]];
    [times setNumberOfLines:3];
    [self.view addSubview:times];
      [times setAlpha:0];
    
    
    
    monthView = [[ScedulareView alloc] initWithFrame:CGRectMake(10, 30, 300, 30)];
    [monthView setBackgroundColor:[UIColor colorWithRed:0.941 green:0.941 blue:0.941 alpha:1.000]];
    [monthView.layer setBorderWidth:1];
    [monthView.layer setCornerRadius:10];
    [monthView setParant:self];
    [monthView.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [monthView.layer setMasksToBounds:YES];
    [self.view addSubview:monthView];

    
    
    Frequency = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, self.view.frame.size.width-20, 30)];
    [Frequency setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [Frequency setFont:[UIFont boldSystemFontOfSize:14]];
    [Frequency setText:@"Frequency"];
    [Frequency setBackgroundColor:[UIColor clearColor]];
    [Frequency setNumberOfLines:3];
    [self.view addSubview:Frequency];
    
    //273 × 39 pixels
    Schedule = [[UIButton alloc]  initWithFrame:CGRectMake(10, 30, 300, 39)];
    [Schedule setTitle:@"Daily" forState:UIControlStateNormal];
    [Schedule.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [Schedule setTitleColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000] forState:UIControlStateNormal];
    [Schedule setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Frequency.png"]]];
    [Schedule setTag:140];
    [Schedule addTarget:self action:@selector(viewanimate) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:Schedule];
    
    
    [Schedule.layer setCornerRadius:10];
    [Schedule.layer setBorderColor:[UIColor grayColor].CGColor];
    [Schedule.layer setBorderWidth:0.8];
    
    
    
    chosenFrequentcy=@"Weekly";
    //116
   if (self.view.frame.size.height ==524.000000) {
        
        table = [[TimeSchedulare alloc] initWithFrame:CGRectMake(10, 270, 300, 205)];
        
        
        table.separatorColor = [UIColor clearColor];
        table.backgroundColor = [UIColor clearColor];
        
        
        [self.view addSubview:table];
        
            [table getparant:self];
  
       
    
    }
    else
    {
        table = [[TimeSchedulare alloc] initWithFrame:CGRectMake(10, 270, 300, 116)];
        
        
        table.separatorColor = [UIColor clearColor];
        table.backgroundColor = [UIColor clearColor];
        
        
        [self.view addSubview:table];
        [table getparant:self];
        
        
    
        
  
    }
    
     [table setAlpha:0];
    
    add_button = [[Addbutton alloc]  initWithFrame:CGRectMake(0, 5, self.view.frame.size.width-20, 40)];
    [add_button setTag:170];
    [table addSubview:add_button];
   
    [add_button setbuttonAction:self];
 

 
}

-(void)chosenSchedule:(NSString*)sender

{
    
        //////NSLog(@"%@", sender);
    
    chosenFrequentcy =sender;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [format setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    [Chooseview changeToDate:[format dateFromString:Name.text]];
    
    
    
    if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
      
        
            [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
        
        if ([EveryViewText.text intValue] ==0) {
            
        }
        else
            
        {
            NSDateFormatter *format2 = [[NSDateFormatter alloc] init];
            [format2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            
            [format2 setDateFormat:@"hh:mm a"];
            
            [self calculateInterval:[format2 dateFromString:StartingTimeText.text] count:[EveryViewText.text intValue]];
            
           // [self setDayinaRowItHours:[format dateFromString:Name.text]];
            
        }
        
        
        
    }
    if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
        
        
            [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
        
        NSDateFormatter *format2 = [[NSDateFormatter alloc] init];
        [format2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        
        [format2 setDateFormat:@"hh:mm a"];
        
        
        [self setDayinaRowIt:[format dateFromString:Name.text]];
        
            //[self calculateIntervalDays:[format2 dateFromString:StartingTimeText.text] count:[EveryViewText.text intValue]];
        
        
    }
    if ([chosenFrequentcy isEqualToString:@"Daily"]) {
        
            [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
        
        [self setDayinaRowIt:[format dateFromString:Name.text]];
    }
    
    

  

    
    
    
    if (Starting.alpha ==0) {
        
        
        chosenFrequentcy =sender;
        
       
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.8];
        
        
        //10, 25, 300, 39
        
        if (monthView.frame.size.height == 30) {
            [monthView setFrame:CGRectMake(10, 40, 300,30*5)];
            [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22*13)];
            
        }
        else
        {
            [monthView setFrame:CGRectMake(10, 40,300,30)];
            [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22)];
            
        }
        
        [UIView commitAnimations];
        
        
        if ([chosenFrequentcy isEqualToString:@"Daily"]) {
            
                [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
            
            
              [EveryViewText setText:@"24"];
           [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
            [Chooseview cleanup];
            [EveryView setAlpha:0];
            [Every setAlpha:0];
            
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [times setAlpha:1];
            [table setAlpha:1];
            
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
            [table setFrame:CGRectMake(10, 210, 300, 266)];
            [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
            
            if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 210, 300, 266)];
                [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 210, 300, 166)];
                [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
            }
         
            
            
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
            
                [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
            
              [EveryViewText setText:@""];
               [EveryLabel setText:@"Days"];
             [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
               [Chooseview cleanup];
            [EveryView setAlpha:1];
            [Every setAlpha:1];
            [EveryViewText setTitle:@"Every X Days"];
            
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
            [times setAlpha:1];
            [table setAlpha:1];
            
            if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 205)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
        }
        if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
            
                [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
            
               [EveryViewText setText:@"168"];
            [EveryView setAlpha:0];
            [Every setAlpha:0];
             [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
               [Chooseview cleanup];
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [times setAlpha:1];
            [table setAlpha:1];
            
            [Choose setAlpha:1];
            [Chooseview setAlpha:1];
            
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
           if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 205)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
            
                [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
            
              [EveryViewText setText:@""];
             [EveryLabel setText:@"Hours"];
             [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
               [Chooseview cleanup];
            [EveryView setAlpha:1];
            [Every setAlpha:1];
            [EveryViewText setTitle:@"Every X Hours"];
            
            
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            
            
            [times setAlpha:0];
            [table setAlpha:0];
            
            [StartingTime setAlpha:1];
            [StartingTimeView setAlpha:1];
            
            
            if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 205)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
            
        }
        
        
        

    }
    else
    {
        
             
        
        if ([table.TimesSet count] ==0) {
            
          
            
            chosenFrequentcy =sender;
            
           
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:0.8];
            
            
            //10, 25, 300, 39
            
            if (monthView.frame.size.height == 30) {
                [monthView setFrame:CGRectMake(10, 40, 300,30*5)];
                [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22*13)];
                
            }
            else
            {
                [monthView setFrame:CGRectMake(10, 40,300,30)];
                [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22)];
                
            }
            
            [UIView commitAnimations];
            
            
            if ([chosenFrequentcy isEqualToString:@"Daily"]) {
                
                
                    [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
                
                [EveryViewText setText:@"24"];
                [EveryView setAlpha:0];
                [Every setAlpha:0];
                 [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
                   [Chooseview cleanup];
                [StartingView setAlpha:1];
                [Starting setAlpha:1];
                
                [EndingView setAlpha:1];
                [Ending setAlpha:1];
                
                [times setAlpha:1];
                [table setAlpha:1];
                
                [Choose setAlpha:0];
                [Chooseview setAlpha:0];
                
                [StartingTime setAlpha:0];
                [StartingTimeView setAlpha:0];
                
                [table setFrame:CGRectMake(10, 210, 300, 266)];
                [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
                
                if (self.view.frame.size.height ==524.000000) {
                    [table setFrame:CGRectMake(10, 210, 300, 266)];
                    [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
                    
                }
                
                else
                {
                    [table setFrame:CGRectMake(10, 210, 300, 166)];
                    [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
                }
                
                
                
            }
            if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
                
                
                    [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
                
                [EveryViewText setText:@""];

                [EveryLabel setText:@"Days"];
                 [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
                   [Chooseview cleanup];
                [EveryView setAlpha:1];
                [Every setAlpha:1];
                [EveryViewText setTitle:@"Every X Days"];
                
                [StartingView setAlpha:1];
                [Starting setAlpha:1];
                
                [EndingView setAlpha:1];
                [Ending setAlpha:1];
                
                [Choose setAlpha:0];
                [Chooseview setAlpha:0];
                
                [StartingTime setAlpha:0];
                [StartingTimeView setAlpha:0];
                
                [times setAlpha:1];
                [table setAlpha:1];
                
                if (self.view.frame.size.height ==524.000000) {
                    [table setFrame:CGRectMake(10, 270, 300, 205)];
                    
                }
                
                else
                {
                    [table setFrame:CGRectMake(10, 270, 300, 116)];
                }
                [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
                
            }
            if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
                
                
                    [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
               
                [EveryViewText setText:@"168"];
                [EveryView setAlpha:0];
                [Every setAlpha:0];
                 [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
                   [Chooseview cleanup];
                [StartingView setAlpha:1];
                [Starting setAlpha:1];
                
                [EndingView setAlpha:1];
                [Ending setAlpha:1];
                
                [times setAlpha:1];
                [table setAlpha:1];
                
                [Choose setAlpha:1];
                [Chooseview setAlpha:1];
                
                [StartingTime setAlpha:0];
                [StartingTimeView setAlpha:0];
                
                if (self.view.frame.size.height ==524.000000) {
                    [table setFrame:CGRectMake(10, 270, 300, 205)];
                    
                }
                
                else
                {
                    [table setFrame:CGRectMake(10, 270, 300, 116)];
                }
                [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
                
            }
            if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
                
                
                    [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
                
                [EveryViewText setText:@""];

                [EveryLabel setText:@"Hours"];
                 [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
                   [Chooseview cleanup];
                [EveryView setAlpha:1];
                [Every setAlpha:1];
                [EveryViewText setTitle:@"Every X Hours"];
                
                
                [StartingView setAlpha:1];
                [Starting setAlpha:1];
                
                [EndingView setAlpha:1];
                [Ending setAlpha:1];
                
                [Choose setAlpha:0];
                [Chooseview setAlpha:0];
                
                
                [times setAlpha:0];
                [table setAlpha:0];
                
                [StartingTime setAlpha:1];
                [StartingTimeView setAlpha:1];
                
                
                if (self.view.frame.size.height ==524.000000) {
                    [table setFrame:CGRectMake(10, 270, 300, 205)];
                    
                }
                
                else
                {
                    [table setFrame:CGRectMake(10, 270, 300, 116)];
                }
                [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
                
                
            }
            
        }
        else
        {
            
  
    
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Choosing another Frequency will delete all other Frequency settings"];
        [alert setMessage:@""];
        [alert setDelegate:self];
        [alert setTag:464749];
        [alert addButtonWithTitle:@"Cancel"];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
            
        }
        
        
    }
    
    
        //////////NSLog(@"DayinaRow %@", DayinaRow);
}



-(void)chosenScheduleSet:(NSString*)sender

{
    
    //////NSLog(@"%@", sender);
    
    chosenFrequentcy =sender;
    
    if ([sender isEqualToString:@" "]) {
     
        
        chosenFrequentcy=@"Daily";
        [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
    }
    else
    {
    
        chosenFrequentcy =sender;
         [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
        
        
        ////////NSLog(@"sender %@", sender);
        
        if ([chosenFrequentcy isEqualToString:@"Daily"]) {
            
            [EveryViewText setText:@"24"];
            [EveryView setAlpha:0];
            [Every setAlpha:0];
            
            [Chooseview cleanup];
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [times setAlpha:1];
            [table setAlpha:1];
            
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
            [table setFrame:CGRectMake(10, 210, 300, 266)];
            [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
            
            if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 210, 300, 266)];
                [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 210, 300, 166)];
                [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
            }
            
            
            
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
            
            [EveryViewText setText:@""];
            
            [EveryLabel setText:@"Days"];
            
            [Chooseview cleanup];
            [EveryView setAlpha:1];
            [Every setAlpha:1];
            [EveryViewText setTitle:@"Every X Days"];
            
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
            [times setAlpha:1];
            [table setAlpha:1];
            
            if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 205)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
        }
        if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
            
            [EveryViewText setText:@"168"];
            [EveryView setAlpha:0];
            [Every setAlpha:0];
            
            [Chooseview cleanup];
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [times setAlpha:1];
            [table setAlpha:1];
            
            [Choose setAlpha:1];
            [Chooseview setAlpha:1];
            
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
            if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 205)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
            
            ////////NSLog(@"%@", DayinaRow);
            [EveryViewText setText:@""];
            
            [EveryLabel setText:@"Hours"];
            
            [Chooseview cleanup];
            [EveryView setAlpha:1];
            [Every setAlpha:1];
            [EveryViewText setTitle:@"Every X Hours"];
            
            
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            
            
            [times setAlpha:0];
            [table setAlpha:0];
            
            [StartingTime setAlpha:1];
            [StartingTimeView setAlpha:1];
            
            
            if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 205)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
            
        }
        
        
        
        //////////NSLog(@"DayinaRow %@", DayinaRow);
        
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        
        [format setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        //NSLog(@"%@", [[DayinaRow valueForKey:@"Starting"] objectAtIndex:0]);
        
        if ([format dateFromString:Name.text]) {
            
           [Chooseview changeToDate:[format dateFromString:Name.text]];
            
        }
        else
        {
            
        }
        
       
        
        if ([format dateFromString: [[DayinaRow valueForKey:@"Starting"] objectAtIndex:0]]) {
            
               if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
                   
          
                   [Chooseview changeToDateIt:[format dateFromString:[[DayinaRow valueForKey:@"Starting"] objectAtIndex:0]]];
                   
               }
            [Name setText:[[DayinaRow valueForKey:@"Starting"] objectAtIndex:0]];
            
        }
        
        
        NSArray *numbers2 = [[[DayinaRow valueForKey:@"Times"] objectAtIndex:0] componentsSeparatedByString:@","];
        
        [table.TimesSet removeAllObjects];
        
        for (int i =0; i < [numbers2 count]; i++) {
            
            
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            
            [dateFormat setDateFormat:@"hh:mm a"];
            
            
            
            NSDateFormatter *dateFormat24 = [[NSDateFormatter alloc] init];
            [dateFormat24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            [dateFormat24 setDateFormat:@"HH:mm a"];
            
            
            NSDate *now =[dateFormat dateFromString:[numbers2 objectAtIndex:i]];
            
            
            
            NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
            [set setObject:[numbers2 objectAtIndex:i] forKey:@"Time"];
             [set setObject:[dateFormat24 stringFromDate:now]  forKey:@"Date"];
            
            
            if ([[numbers2 objectAtIndex:i] isEqualToString:@" "]) {
                
            }
            else
                
            {
                [table.TimesSet addObject:set];
                
            }
            
        }
        
        [table reloadData];
        

        
    }
    
    
   

}


-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{
    
    
    if (index==1) {
        
       
     
        [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.8];
        
        
        //10, 25, 300, 39
        
        if (monthView.frame.size.height == 30) {
            [monthView setFrame:CGRectMake(10, 40, 300,30*5)];
            [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22*13)];
            
        }
        else
        {
            [monthView setFrame:CGRectMake(10, 40,300,30)];
            [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22)];
            
        }
        
        [UIView commitAnimations];
        
        
        if ([chosenFrequentcy isEqualToString:@"Daily"]) {
            
            
             [EveryViewText setText:@"24"];
            [table.TimesSet removeAllObjects];
           [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
            [NumberOfDays removeAllObjects];
             [Chooseview cleanup];
          
            
            [EveryView setAlpha:0];
            [Every setAlpha:0];
            
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [times setAlpha:1];
            [table setAlpha:1];
            
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
            [table setFrame:CGRectMake(10, 210, 300, 266)];
            [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
               [table reloadData];
            
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
            
            
            [EveryViewText setText:@""];

              [EveryLabel setText:@"Days"];
            
            
            [table.TimesSet removeAllObjects];
           [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
            
             [Chooseview cleanup];
        
            
            [EveryView setAlpha:1];
            [Every setAlpha:1];
            [EveryViewText setTitle:@"Every X Days"];
            
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
            [times setAlpha:1];
            [table setAlpha:1];
               [table reloadData];
            
            if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 205)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
        }
        if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
            
            
              [EveryViewText setText:@"168"];
            [table.TimesSet removeAllObjects];
           [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
            [Chooseview cleanup];
            [table reloadData];
            
            [EveryView setAlpha:0];
            [Every setAlpha:0];
            
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [times setAlpha:1];
            [table setAlpha:1];
            [table reloadData];
            
            [Choose setAlpha:1];
            [Chooseview setAlpha:1];
            
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
            if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 205)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
            
            [EveryViewText setText:@""];

            [EveryLabel setText:@"Hours"];
            [table.TimesSet removeAllObjects];
           [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
            
            
            [EveryView setAlpha:1];
            [Every setAlpha:1];
            [EveryViewText setTitle:@"Every X Hours"];
            
            
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            
            
            [times setAlpha:0];
            [table setAlpha:0];
            
            [StartingTime setAlpha:1];
            [StartingTimeView setAlpha:1];
            
            
            if (self.view.frame.size.height ==524.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 205)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
            
        }
        
        

        
    }
    
    else
    {
        
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.8];
        
        
        //10, 25, 300, 39
        
        if (monthView.frame.size.height == 30) {
            [monthView setFrame:CGRectMake(10, 40, 300,30*5)];
            [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22*13)];
            
        }
        else
        {
            [monthView setFrame:CGRectMake(10, 40,300,30)];
            [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22)];
            
        }
        
        [UIView commitAnimations];
        
        
        chosenFrequentcy =Schedule.titleLabel.text;
        
        
        
        
       
    }
}



-(void)viewanimate

{
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];
    
    
    //10, 25, 300, 39
    
    if (monthView.frame.size.height == 30) {
        [monthView setFrame:CGRectMake(10, 40, 300,30*5)];
        [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22*13)];
        
    }
    else
    {
        [monthView setFrame:CGRectMake(10, 40,300,30)];
        [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22)];
        
    }
    
    [UIView commitAnimations];
    
}



-(UIButton*)setColor:(UIButton*)sender

{
    
    CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
    shineLayer2.frame = sender.layer.bounds;
    shineLayer2.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          nil];
    shineLayer2.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];
    
    
    
    [sender.layer addSublayer:shineLayer2];
    [sender.layer setMasksToBounds:YES];
    
    
    sender.titleLabel.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    sender.titleLabel.layer.shadowOpacity = 1;
    sender.titleLabel.layer.shadowRadius = 1.0;
    [sender.layer setCornerRadius:10];
    [sender.layer setBorderColor:sender.backgroundColor.CGColor];
    [sender.layer setBorderWidth:0.8];
    
    return sender;
    
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [Choicetimer count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(ProfileCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
   
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    return 50;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    
    
    
    //[tableView reloadData];
    
    
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    NSString *identifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
	if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    else
        
    {
        
    }
  
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@ %@", [[Choicetimer valueForKey:@"CurrentDate"] objectAtIndex:indexPath.row], [[Choicetimer valueForKey:@"CurrentTime"] objectAtIndex:indexPath.row], [[Choicetimer valueForKey:@"Moment"] objectAtIndex:indexPath.row]];
    [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
    [cell.textLabel setFont:[UIFont systemFontOfSize:14]];
    
    
    return cell;
    
    
}




-(void)Cancel:(UIBarButtonItem*) sender

{

}

-(void)Save:(UIBarButtonItem*) sender

{
    
}



-(void)Addit:(UIButton*)sender

{
    if ([table.TimesSet containsObject:timeContent]) {
       
      
    }
    else
    {
        
 
    [table.TimesSet addObject:timeContent];
        
        
    }
    
   
    
    
    
    
    [add_button.AddButton setHidden:YES];
    [add_button.EndingViewText setText:@"add new time"];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [format setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    [Chooseview changeToDate:[format dateFromString:Name.text]];
    
    
    if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
        
    }
    if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
        
        [self setDayinaRowItHours:[format dateFromString:Name.text]];
    }
    if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
        
        [self setDayinaRowIt:[format dateFromString:Name.text]];
    }
    if ([chosenFrequentcy isEqualToString:@"Daily"]) {
        
        [self setDayinaRowIt:[format dateFromString:Name.text]];
    }
   
    for (int i=0 ; i<[DayinaRow count]; i++) {
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        
        for (id key in [DayinaRow objectAtIndex:i]) {
            
            
            
            if ([key isEqualToString:@"Times"]) {
                
                NSString *uploadString = [[table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
                
                [set setObject:uploadString forKey:@"Times"];
            }
            
            else
            {
                
                [set setObject:[[DayinaRow objectAtIndex:i] objectForKey:key] forKey:key];
                
            }
            
        }
        
        [DayinaRow replaceObjectAtIndex:i withObject:set];
        
    }
    
    //NSLog(@"%@", table.TimesSet);
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
    [table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    
    [table reloadData];
    
    
 
  
    

////NSLog(@"%@", DayinaRow);


}



-(NSMutableArray*) setDaysinaRow:(NSMutableDictionary*)days

{
    

    NSMutableArray *getIt=[[NSMutableArray alloc]init];
    
    
    NSArray *numbers2 = [[days valueForKey:@"Dates"] componentsSeparatedByString:@"#"];
    
    
    NSLog(@"%@", numbers2);
    
    for (int i=0 ; i<[numbers2 count]; i++) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [dateFormatter2 setDateFormat:@"dd, MMMM yyyy"];
        
        
        //////////NSLog(@"%@", [dateFormatter2 stringFromDate:[dateFormatter dateFromString:[numbers2 objectAtIndex:i]]]);
        
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
        [set setObject:[numbers2 objectAtIndex:i] forKey:@"Date"];
         [set setObject:@" " forKey:@"Day_number"];
        [set setObject:[numbers2 objectAtIndex:i] forKey:@"DateAssent"];
        [set setObject:[days valueForKey:@"Duration"] forKey:@"Ending"];
        [set setObject:[days valueForKey:@"Frequency"] forKey:@"Interval"];
        [set setObject:[days valueForKey:@"Frequency_text"] forKey:@"Setting"];
        [set setObject:[days valueForKey:@"Starting"] forKey:@"Starting"];
        [set setObject:[days valueForKey:@"Times"] forKey:@"Times"];
        [getIt addObject:set];
        
        
    }
    
    NSLog(@"%@", getIt);
    
    return getIt;
}



-(void)findDayInaWeek

{
    
}


-(void)setDayinaRowIt:(NSDate*)sender

{
  
   [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    [dateFormatter1 setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    [dateFormatter2 setDateFormat:@"dd, MMMM yyyy"];
    
    
    NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
    
    NSString *uploadString = [[table.TimesSet valueForKey:@"Time"]  componentsJoinedByString:@","];
    [set setObject:[dateFormatter1 stringFromDate:sender] forKey:@"Date"];
    [set setObject:[dateFormatter2 stringFromDate:sender] forKey:@"DateAssent"];
    
      [set setObject:chosenFrequentcy forKey:@"Setting"];
    if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
      
         [set setObject:EveryViewText.text forKey:@"Interval"];
    }
    if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
       
         [set setObject:EveryViewText.text forKey:@"Interval"];
    }
    if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
        
        NSString *timeIt =[NSString stringWithFormat:@"%i", ([EveryViewText.text intValue]*24)];
        [set setObject:timeIt forKey:@"Interval"];
    }
    if ([chosenFrequentcy isEqualToString:@"Daily"]) {
        
        [set setObject:EveryViewText.text forKey:@"Interval"];
    }
      [set setObject:@"" forKey:@"Day_number"];
    [set setObject:uploadString forKey:@"Times"];
    [set setObject:Name.text forKey:@"Starting"];
    [set setObject:EndingViewText.text forKey:@"Ending"];
    
    [DayinaRow addObject:set];
    
    
    
    for (int i=0 ; i<[DayinaRow count]; i++) {
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        
        for (id key in [DayinaRow objectAtIndex:i]) {
            
            
            
            if ([key isEqualToString:@"Times"]) {
                
                NSString *uploadString = [[table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
                
                [set setObject:uploadString forKey:@"Times"];
            }
            
            else
            {
                
                [set setObject:[[DayinaRow objectAtIndex:i] objectForKey:key] forKey:key];
                
            }
            
        }
        
        [DayinaRow replaceObjectAtIndex:i withObject:set];
        
    }
    
 
    
    ////NSLog(@"%@", DayinaRow);
}


-(void)setDayinaRowItHours:(NSDate*)sender

{
    ////////NSLog(@"setDayinaRowItHours");
    
   [DayinaRow removeAllObjects];
              [NumberOfDays removeAllObjects];
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    [dateFormatter1 setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    [dateFormatter2 setDateFormat:@"dd, MMMM yyyy"];
    
    
    NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
    
    [set setObject:[dateFormatter1 stringFromDate:sender] forKey:@"Date"];
    [set setObject:[dateFormatter2 stringFromDate:sender] forKey:@"DateAssent"];
     NSString *uploadString = [[table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
    [set setObject:uploadString forKey:@"Times"];
       [set setObject:chosenFrequentcy forKey:@"Setting"];
    if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
        
        [set setObject:EveryViewText.text forKey:@"Interval"];
    }
    if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
        
        [set setObject:EveryViewText.text forKey:@"Interval"];
    }
    if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
        
        [set setObject:EveryViewText.text forKey:@"Interval"];
    }
    if ([chosenFrequentcy isEqualToString:@"Daily"]) {
        
        [set setObject:EveryViewText.text forKey:@"Interval"];
    }
      [set setObject:@"" forKey:@"Day_number"];
    [set setObject:Name.text forKey:@"Starting"];
    [set setObject:EndingViewText.text forKey:@"Ending"];
    
    [DayinaRow addObject:set];
    
    
 
}





-(void)changeDays:(Daybutton*)sender

{

    
    //////////////NSLog(@"%@", DayRow);
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    [dateFormatter1 setDateFormat:@"EEEE dd, MMMM yyyy"];
 

    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    [dateFormatter2 setDateFormat:@"dd, MMMM yyyy"];
    
  
    
    if ([[DayinaRow valueForKey:@"Date"]  containsObject:[dateFormatter1 stringFromDate:sender.totalDate]]) {
        
        
       NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        
        
        NSString *uploadString = [[table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];

        [set setObject:[dateFormatter1 stringFromDate:sender.totalDate] forKey:@"Date"];
        [set setObject:[dateFormatter2 stringFromDate:sender.totalDate] forKey:@"DateAssent"];
        [set setObject:uploadString forKey:@"Times"];
           [set setObject:chosenFrequentcy forKey:@"Setting"];
        if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
            
            [set setObject:EveryViewText.text forKey:@"Interval"];
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
            
            [set setObject:EveryViewText.text forKey:@"Interval"];
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
            
            [set setObject:EveryViewText.text forKey:@"Interval"];
        }
        if ([chosenFrequentcy isEqualToString:@"Daily"]) {
            
            [set setObject:EveryViewText.text forKey:@"Interval"];
        }
          [set setObject:@"" forKey:@"Day_number"];
        [set setObject:Name.text forKey:@"Starting"];
        [set setObject:EndingViewText.text forKey:@"Ending"];
        
        [DayinaRow  removeObjectAtIndex:[[DayinaRow valueForKey:@"Date"] indexOfObject:[dateFormatter1 stringFromDate:sender.totalDate]]];
    }
    else
        
    {
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        
        NSString *uploadString = [[table.TimesSet valueForKey:@"Time"]  componentsJoinedByString:@","];
        [set setObject:[dateFormatter1 stringFromDate:sender.totalDate] forKey:@"Date"];
        [set setObject:[dateFormatter2 stringFromDate:sender.totalDate] forKey:@"DateAssent"];
        [set setObject:uploadString forKey:@"Times"];
           [set setObject:chosenFrequentcy forKey:@"Setting"];
        if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
            
            [set setObject:EveryViewText.text forKey:@"Interval"];
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
            
            [set setObject:EveryViewText.text forKey:@"Interval"];
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
            
            [set setObject:EveryViewText.text forKey:@"Interval"];
        }
        if ([chosenFrequentcy isEqualToString:@"Daily"]) {
            
            [set setObject:EveryViewText.text forKey:@"Interval"];
        }
          [set setObject:@"" forKey:@"Day_number"];
        [set setObject:Name.text forKey:@"Starting"];
        [set setObject:EndingViewText.text forKey:@"Ending"];
        
        [DayinaRow addObject:set];
       
        
    }

    
    for (int i=0 ; i<[DayinaRow count]; i++) {
        
              NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        
        for (id key in [DayinaRow objectAtIndex:i]) {
            
      
            
                if ([key isEqualToString:@"Times"]) {
              
                    NSString *uploadString = [[table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
                
                    [set setObject:uploadString forKey:@"Times"];
                }
            
                else
                {
            
                    [set setObject:[[DayinaRow objectAtIndex:i] objectForKey:key] forKey:key];
                
                }
          
        }
        
          [DayinaRow replaceObjectAtIndex:i withObject:set];
        
    }

    
  
}


-(void)calculateInterval:(NSDate*)sender count:(NSInteger)interval

{
    
 
    [table.TimesSet removeAllObjects];
    
    
    
    
        for (int i=0 ; i<24/interval; i++) {
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    
  
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:sender];
    
   
            
        
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setHour:[timeComponents hour]+(interval*i)];
    [dateComps setMinute:[timeComponents minute]];
    [dateComps setSecond:[timeComponents second]];
    
     NSDate *itemDate  = [calendar dateFromComponents:dateComps];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            
            [dateFormat setDateFormat:@"hh:mm a"];
            
            
           
            
              NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
            [set setObject:[dateFormat stringFromDate:itemDate] forKey:@"Time"];
            
           
            if ([dateFormat stringFromDate:itemDate]) {
                
                    [table.TimesSet addObject:set];
            }
            else
                
            {
      
                
            }
            
            ////////////NSLog(@"%@",table.TimesSet);
            
            
            
        }
    
   
    
    
}


-(void)calculateIntervalDays:(NSDate*)sender count:(NSInteger)interval

{
    
    ////////NSLog(@"interval %i", interval);
  
    [table.TimesSet removeAllObjects];
    
    for (int i=0 ; i<24/interval; i++) {
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        
        
        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:sender];
        
        
        
        
        NSDateComponents *dateComps = [[NSDateComponents alloc] init];
        [dateComps setHour:[timeComponents hour]+(interval*i)];
        [dateComps setMinute:[timeComponents minute]];
        [dateComps setSecond:[timeComponents second]];
        
        NSDate *itemDate  = [calendar dateFromComponents:dateComps];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        
        [dateFormat setDateFormat:@"hh:mm a"];
     
        
        
        NSDateFormatter *dateFormat24 = [[NSDateFormatter alloc] init];
        [dateFormat24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [dateFormat24 setDateFormat:@"HH:mm a"];
        
        
        
        NSDate *now =[dateFormat dateFromString:[dateFormat stringFromDate:itemDate]];
        
        
        
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        [set setObject:[dateFormat stringFromDate:itemDate] forKey:@"Time"];
        [set setObject:[dateFormat24 stringFromDate:now]  forKey:@"Date"];
        
        if ([dateFormat stringFromDate:itemDate]) {
            
            [table.TimesSet addObject:set];
        }
        else
            
        {
            
            
        }
        
        ////////////NSLog(@"%@",table.TimesSet);
        
        
        
    }
   
    
}







-(void)change:(UIButton*)sender

{
    
    
    if (sender.tag ==170) {
        
        DurationViewController *controller = [[DurationViewController alloc]  init];
               [controller setTitle:@"Starting Date"];
        
        
        if ([controller.TitleTextOut.text isEqualToString:@"add new time"]) {
            
               [controller.TitleTextOut setText:@""];
        }
        else
            
        {
          
            
            
            
            
        }
        
      
        
        [self.navigationController pushViewController:controller animated:YES];
 
 controller.titleEdit=@"add";
        [controller getParant:self];
        
        
        for (UIView *view in self.view.subviews) {
            
            
            
            for (TekstIDLabel *label in view.subviews) {
                
                
                     if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                if ([label.title isEqualToString:@"Starting"]) {
                    
                  
                         [controller whatLabel:label];
                    CopyLabel =label;
                    
                }
                         
                }
                
            }
            
        }
        
    }
    
    if (sender.tag ==171) {
        
        DurationViewController *controller = [[DurationViewController alloc]  init];
        [controller setTitle:@"Ending Time"];
        [self.navigationController pushViewController:controller animated:YES];
        
        controller.titleEdit=@"add"; 
        [controller getParant:self];
        
        
        for (UIView *view in self.view.subviews) {
            
            
             for (UIView *view in self.view.subviews) {
           
            for (TekstIDLabel *label in view.subviews) {
                
                
                     if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                if ([label.title isEqualToString:@"Ending Time"]) {
                   
                    [controller whatLabel:label];
                    
                }
                }
                
            }
                
            }
            
        }
        
    }
    if (sender.tag ==177) {
        
        HoursViewController *controller = [[HoursViewController alloc]  init];
             [controller setTitle:chosenFrequentcy];
        [controller getParant:self];
        
        [self.navigationController pushViewController:controller animated:YES];
      
        
        
   
        [controller getParant:self];
        
        
        for (UIView *view in self.view.subviews) {
            
            
            for (UIView *view in self.view.subviews) {
                
                for (TekstIDLabel *label in view.subviews) {
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        if ([label.title isEqualToString:@"Every X Days"]) {
                            
                            ////////NSLog(@"%@", label.title);
                            [controller whatLabel:label];
                            
                        }
                        
                        if ([label.title isEqualToString:@"Every X Hours"]) {
                            
                             ////////NSLog(@"%@", label.title);
                            [controller whatLabel:label];
                            
                        }
                    }
                    
                }
                
            }
            
        }
        
    }

    if (sender.tag ==178) {
        
        DurationViewController *controller = [[DurationViewController alloc]  init];
        [controller setTitle:@"Starting Time"];
        [self.navigationController pushViewController:controller animated:YES];
        
        //controller.titleEdit=@"add";
        [controller getParant:self];
        
        
     [controller whatLabel:StartingTimeText];
        
    }
    
    if (sender.tag ==172) {
        
        DurationViewController *controller = [[DurationViewController alloc]  init];
        [controller setTitle:@"Reminder Time"];
        [self.navigationController pushViewController:controller animated:YES];
        
         controller.titleEdit=@"add";
        [controller getParant:self];
        
        
        
        
        for (UIView *label in table.subviews) {
         
              if ([label isKindOfClass:[Addbutton class]]) {
                  
                    for (UIView *label2 in label.subviews) {
                        
                           if ([label2 isKindOfClass:[UIView class]]) {
                               
                               
                                   for (TekstIDLabel *label4 in label2.subviews) {
                               
                                         if ([label4 isKindOfClass:[TekstIDLabel class]]) {
                                             
                                            
                                             [controller whatLabel:label4];
                                             
                                         }
                                       
                                   }
                               
                           }
                        
                        
               
                        
                    }
                  
              }
       
            
        }
       
        for (TekstIDLabel *label in add_button.subviews) {
  
                     if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                if ([label.title isEqualToString:@""]) {
                  
                         [controller whatLabel:label];
                    
              
                }
                
            }
            
        }
        
    }
 
   
}


-(void)gotoScreen:(TekstIDLabel*)sender count:(NSInteger)rowit;

{
    
    
    DurationViewController *controller = [[DurationViewController alloc]  init];
    [controller setTitle:@"Reminder Time"];
    [self.navigationController pushViewController:controller animated:YES];
    controller.titleEdit=@"Edit";
    controller.countDict =rowit;
    
    [controller getParant:self];
    [controller whatLabel:sender];
    
    for (int i=0 ; i<[DayinaRow count]; i++) {
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        
        for (id key in [DayinaRow objectAtIndex:i]) {
            
            
            
            if ([key isEqualToString:@"Times"]) {
                
                NSString *uploadString = [[table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
                
                [set setObject:uploadString forKey:@"Times"];
            }
            
            else
            {
                
                [set setObject:[[DayinaRow objectAtIndex:i] objectForKey:key] forKey:key];
                
            }
            
        }
        
        [DayinaRow replaceObjectAtIndex:i withObject:set];
        
    }
    
    
  

}


-(void)MakeDictionary:(NSString*)Data andName:(NSString*)Name

{
    
}



-(void)makeStartEnd:(NSString*)starting end:(NSString*)endig
{
    
 
    
    for (UIScrollView *scroll in parantIt.view.subviews) {
        
        
        for (SchedularView *view in scroll.subviews) {
            
           
            for (TekstIDLabel *label in view.subviews) {
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                
                    
                    if ([label.title isEqualToString:@"Starting"]) {
                       
                        [label setText:starting];
                        label.realText=starting;
                        
                    }
                    
                    if ([label.title isEqualToString:@"Duration"]) {
                        
                        [label setText:endig];
                          label.realText=endig;
                        
                    }
                    
                    if ([label.title isEqualToString:@"Frequency"]) {
                        
                        [label setText:TitleText.text];
                        label.realText=TitleText.text;
                        
                    }
                
                }}}}
    
    
  
}


-(void)makechoice
{
    
}

-(void)Action:(UIButton*)sender

{

  
}

-(void)changeChose:(NSString*)sender

{


     chosen = sender;
    
    
}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

       TitleText.selectedRange = NSMakeRange(0, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [a1 setTag:44];
        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
        [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            ////////////////////////////////////////////////////////////////////NSLog////////////////////////////////////////////////////////////////////NSLog(@"%@", text);
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{


}

-(void) getchoice:(NSMutableArray*)choice

{
    
        [Choicetimer removeAllObjects];
    

    for (int i=0 ; i<4; i++) {
        
        UIImageView *setimage2 = (UIImageView *)[self.view viewWithTag:440+i];
        [setimage2 setAlpha:0];
        
    }
    
    
    if ([choice count] >0) {
        
        if ([[[choice valueForKey:@"choice"] objectAtIndex:0] isEqualToString:@"Daily"]) {
           
              UIImageView *setimage2 = (UIImageView *)[self.view viewWithTag:441];
               [setimage2 setAlpha:1];
        }
        if ([[[choice valueForKey:@"choice"] objectAtIndex:0] isEqualToString:@"Hours"]) {
            
            UIImageView *setimage2 = (UIImageView *)[self.view viewWithTag:440];
              [setimage2 setAlpha:1];
            
        }
        if ([[[choice valueForKey:@"choice"] objectAtIndex:0] isEqualToString:@"Weekly"]) {
              UIImageView *setimage2 = (UIImageView *)[self.view viewWithTag:442];
              [setimage2 setAlpha:1];
            
        }
        if ([[[choice valueForKey:@"choice"] objectAtIndex:0] isEqualToString:@"Days"]) {
              UIImageView *setimage2 = (UIImageView *)[self.view viewWithTag:443];
              [setimage2 setAlpha:1];
            
        }
        
    }
    
   
 

    //////////////////////////////NSLog(@"%@", choice);


    Choicetimer =choice;
    
    
    if ([Choicetimer count]==0) {
        
        
        [TitleText setAlpha:0];
        //[table setAlpha:0];
    }
    else
        
    {
        
        [TitleText setAlpha:1];
        //[table setAlpha:1];
    }

   
    
    //[table.TimesSet valueForKey:@"Time"]
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Time" ascending:TRUE];
    [table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    
    [table reloadData];
    
    //////////////////////////////////////////////////NSLog(@"%@", Choicetimer);
    
  
}


-(void)getParant:(NewMedecineController*)parant

{

    parantIt =parant;




  
    
}


-(void)setChoice:(NSString*)gothere

{
  
    for (int i=0 ; i<4; i++) {

        UIImageView *setimage2 = (UIImageView *)[self.view viewWithTag:440+i];
        [setimage2 setAlpha:0];
        
    }
    


    NSArray *names1 =[NSArray arrayWithObjects:@"   Choose from Contacts", @"   Add New Contacts", nil];


   
    for (int i=0 ; i<[names1 count]; i++) {

        

        UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
        [setimage setAlpha:0];
        
       


        if ([gothere isEqualToString:[names1 objectAtIndex:i]]) {




        }
        else
            {
            
            chosen=gothere;
            
            }

           if (parantIt) {
               
        if ([ [parantIt.MedicineSpecs valueForKey:gothere] isEqualToString:[names1 objectAtIndex:i]]) {

            ////////////////////////////////////////////////////////NSLog////////////////////////////////////////////////////////////////////NSLog(@"lep");

              

                UILabel *setimage2 = (UILabel *)[self.view viewWithTag:180+i];

                chosen = setimage2.text;
            
        }
           }
        
    }
   

}

- (IBAction)doSomething:(UIButton *)sender {

   
    [[self navigationController] popViewControllerAnimated:YES];

double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){


    
    ////////NSLog(@"%@", DayinaRow);
    
    if ([DayinaRow count] ==0) {


    }
    else{
        
        
        ////////NSLog(@"%@", [[DayinaRow valueForKey:@"Interval"] objectAtIndex:0]);

        [parantIt setitemsDictionary:[[DayinaRow valueForKey:@"Interval"] objectAtIndex:0] name:@"Frequency"];
        [parantIt setitemsDictionary:[[DayinaRow valueForKey:@"Setting"] objectAtIndex:0] name:@"Frequency_text"];
        
        [parantIt setitemsDictionary:[[DayinaRow valueForKey:@"Starting"] objectAtIndex:0] name:@"Starting"];
         [parantIt setitemsDictionary:[[DayinaRow valueForKey:@"Ending"] objectAtIndex:0] name:@"Duration"];
        
        [parantIt setitemsDictionary:[[DayinaRow valueForKey:@"Day_number"] objectAtIndex:0] name:@"Day_number"];
       
        
        NSString *uploadString = [[DayRow valueForKey:@"Day"] componentsJoinedByString:@"#"];
        [parantIt setitemsDictionary:uploadString name:@"Dates"];
        
        
        
        
        NSString *uploadString2 = [[DayRow valueForKey:@"Number"] componentsJoinedByString:@","];
        [parantIt setitemsDictionary:uploadString2 name:@"Day_number"];
        
        [parantIt.Schdules.Frequent setText:[[DayinaRow valueForKey:@"Setting"] objectAtIndex:0]];
     
        
          [parantIt setitemsDictionary:[[DayinaRow valueForKey:@"Times"] objectAtIndex:0] name:@"Times"];
        
        if ([chosenFrequentcy isEqualToString:@"Weekly"]) {
            
            
            
            NSString *uploadString2 = [NumberOfDays componentsJoinedByString:@","];
            [parantIt setitemsDictionary:uploadString2 name:@"Day_number"];
        
            [parantIt.Schdules.Frequent setText:[[DayinaRow valueForKey:@"Setting"] objectAtIndex:0]];
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Hours"]) {
            
            
            
            NSString *uploadString2 = [NumberOfDays componentsJoinedByString:@","];
            [parantIt setitemsDictionary:uploadString2 name:@"Day_number"];
            
            [parantIt.Schdules.Frequent setText:[[DayinaRow valueForKey:@"Interval"] objectAtIndex:0]];
            
            //[parantIt.Schdules.Frequent setText:[[[DayinaRow valueForKey:@"Setting"] objectAtIndex:0] stringByReplacingOccurrencesOfString: @"X" withString:[[DayinaRow valueForKey:@"Interval"] objectAtIndex:0] ]];
            
             [parantIt.Schdules.Frequent setText:[[DayinaRow valueForKey:@"Setting"] objectAtIndex:0]];
            
            
        }
        if ([chosenFrequentcy isEqualToString:@"Every X Days"]) {
            
            
            NSString *uploadString2 = [NumberOfDays componentsJoinedByString:@","];
            [parantIt setitemsDictionary:uploadString2 name:@"Day_number"];
            
            [parantIt.Schdules.Frequent setText:[[DayinaRow valueForKey:@"Interval"] objectAtIndex:0]];
           // NSString *Calculate =[NSString stringWithFormat:@"%i", [[[DayinaRow valueForKey:@"Interval"] objectAtIndex:0] intValue]/24];
            
           //[parantIt.Schdules.Frequent setText:[[[DayinaRow valueForKey:@"Setting"] objectAtIndex:0] stringByReplacingOccurrencesOfString: @"X" withString:Calculate]];
            
                 [parantIt.Schdules.Frequent setText:[[DayinaRow valueForKey:@"Setting"] objectAtIndex:0]];
        }
        if ([chosenFrequentcy isEqualToString:@"Daily"]) {
            
            
            
            NSString *uploadString2 = [NumberOfDays componentsJoinedByString:@","];
            [parantIt setitemsDictionary:uploadString2 name:@"Day_number"];
            
            
            [parantIt.Schdules.Frequent setText:[[DayinaRow valueForKey:@"Setting"] objectAtIndex:0]];
        }
   
        
        [parantIt.Schdules.strating setText:Name.text];
        [parantIt setitemsDictionary:Name.text name:@"Starting"];
        [parantIt.Schdules.ending setText:[[DayinaRow valueForKey:@"Ending"] objectAtIndex:0]];
        [parantIt.Schdules.times setText:[[[DayinaRow valueForKey:@"Times"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@"," withString:@"\n"]];
        
        [parantIt.Schdules setTableview:DayinaRow];
        
        [parantIt.Schdules.related reloadData];
        
        
       

    }
});
    
    

}

- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
