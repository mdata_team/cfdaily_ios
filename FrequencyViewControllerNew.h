//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CodePopView, NewMedecineController, TekstIDLabel, Addbutton, TimeSchedulare, ScedulareView, ChooseView;
@interface FrequencyViewControllerNew : UIViewController<UITextViewDelegate> {

    Addbutton *add_button;
    UITextView *TitleText;
   NSString *Compaire;
    UIView *Combi;
     UIView *CombiOther;
    NSString *chosen;
      NSString *chosenID;
    NSMutableDictionary *timeContent;
}
@property (nonatomic, strong) IBOutlet TekstIDLabel *CopyLabel;
@property (nonatomic, strong) IBOutlet UILabel *EveryLabel;
@property (nonatomic, strong) IBOutlet UIButton *EveryAction;
@property (nonatomic, strong) IBOutlet UILabel *Every;
@property (nonatomic, strong) IBOutlet UIView *EveryView;
@property (nonatomic, strong) TekstIDLabel *EveryViewText;
@property (nonatomic, strong) IBOutlet NSString *chosenFrequentcy;
@property (nonatomic, strong) IBOutlet UILabel *StartingTime;
@property (nonatomic, strong) IBOutlet UIView *StartingTimeView;
@property (nonatomic, strong) IBOutlet UIButton *StartTime;
@property (nonatomic, strong) IBOutlet UIButton *Schedule;
@property (nonatomic, strong) IBOutlet UILabel *Frequency;
@property (nonatomic, strong) Addbutton *add_button;
@property (nonatomic, strong) NSMutableDictionary *timeContent;
@property (nonatomic, strong) NSMutableArray *DayinaRow;
@property (nonatomic, strong) NSMutableArray *DayRow;
@property (nonatomic, strong) NSMutableArray *NumberOfDays;
@property (nonatomic, retain) NSString *IDset;
@property (nonatomic, retain) NSString *Interval;
@property (nonatomic, retain) IBOutlet  NSMutableArray *Choicetimer;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;
@property (nonatomic, retain) TimeSchedulare *table;
@property (nonatomic, retain) UILabel *Title;
@property (nonatomic, strong) ScedulareView *monthView;
@property (nonatomic, assign) NSInteger CurrentDay;
@property (nonatomic, strong) TekstIDLabel *StartingTimeText;

@property (nonatomic, strong) IBOutlet UILabel *Starting;
@property (nonatomic, strong) IBOutlet UIView *StartingView;
@property (nonatomic, strong) IBOutlet UIButton *Start;
@property (nonatomic, strong) TekstIDLabel *Name;
@property (nonatomic, strong) IBOutlet UILabel *Ending;
@property (nonatomic, strong) IBOutlet UIView *EndingView;
@property (nonatomic, strong) TekstIDLabel *EndingViewText;
@property (nonatomic, strong) IBOutlet UIButton *End;
@property (nonatomic, strong) IBOutlet UILabel *Choose;
@property (nonatomic, strong) ChooseView *Chooseview;
@property (nonatomic, strong) IBOutlet UILabel *times;


-(void)chosenSchedule:(NSString*)sender;

-(void)change:(UIButton*)sender;

-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;

-(void)setfreguency:(NSString*)ID;

-(void)gotoScreen:(TekstIDLabel*)sender count:(NSInteger)rowit;
-(void)Addit;

@end

