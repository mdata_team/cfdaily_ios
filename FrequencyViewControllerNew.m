//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "FrequencyViewControllerNew.h"
#import "TimersTableView.h"
#import "HoursViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "DurationViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "SchedularView.h"
#import "ChooseView.h"
#import "TimeSchedulare.h"
#import "Addbutton.h"
#import "TekstIDLabel.h"
#import "ScedulareView.h"
#import "Daybutton.h"
#import "Addbutton.h"


@interface FrequencyViewControllerNew ()

@end

@implementation FrequencyViewControllerNew
@synthesize parantIt;
@synthesize CurrentDay;
@synthesize NumberOfDays;
@synthesize DayinaRow;
@synthesize Schedule;
@synthesize EveryAction;
@synthesize chosenFrequentcy;
@synthesize Frequency;
@synthesize add_button;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize Choicetimer;
@synthesize table;
@synthesize Title;
@synthesize IDset;
@synthesize timeContent;
@synthesize monthView;
@synthesize StartingTimeText;
@synthesize Every;
@synthesize EveryView;
@synthesize EveryViewText;
@synthesize DayRow;
@synthesize Starting;
@synthesize StartingView;
@synthesize Start;
@synthesize Name;
@synthesize Ending;
@synthesize EndingView;
@synthesize EndingViewText;
@synthesize End;
@synthesize Choose;
@synthesize Chooseview;
@synthesize times;
@synthesize EveryLabel;
@synthesize CopyLabel;
@synthesize StartingTime;
@synthesize StartingTimeView;
@synthesize StartTime;
@synthesize Interval;



-(void)loadView{


    [super loadView];
}

-(void)whatLabelFrequency:(TekstIDLabel*)set

{
    
  
    parantLabel =set;
    
    
}

-(void)whatLabel:(UILabel*)set

{
    
  
  
    parantLabel =(TekstIDLabel*)set;
    
}



-(void)setfreguency:(NSString*)ID


{
    
      
}

-(void)viewDidAppear:(BOOL)animated
{

//     [self.view.layer setMasksToBounds:YES];



    if (Interval) {
      
        
         [parantIt.ChosenMedicen setObject:Interval forKey:@"Frequency"];
        [EveryViewText setText:Interval];
    }
    else
    {
    
   
        
    }
    
    

  
}

- (void)viewDidLoad {

    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
 
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    timeContent =[[NSMutableDictionary alloc]init];
    
    DayRow =[[NSMutableArray alloc]init];
        NumberOfDays =[[NSMutableArray alloc]init];
  
   
    
    Starting = [[UILabel alloc] initWithFrame:CGRectMake(15, 58, self.view.frame.size.width-20, 40)];
//    [Starting setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [Starting setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [Starting setFont:[UIFont boldSystemFontOfSize:15]];
    [Starting setText:NSLocalizedString(@"Starting",nil)];
    [Starting setBackgroundColor:[UIColor clearColor]];
    [Starting setNumberOfLines:3];
    [self.view addSubview:Starting];
      [Starting setAlpha:0];
    
    
    
    StartingView = [[UIView alloc] initWithFrame:CGRectMake(10, 90, self.view.frame.size.width-20, 40)];
    [StartingView setBackgroundColor:[UIColor whiteColor]];
    [StartingView.layer setCornerRadius:10];
    [StartingView.layer setBorderWidth:1.5];
//    [StartingView.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 chanegs
    [StartingView.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [StartingView.layer setMasksToBounds:YES];
    [self.view addSubview:StartingView];
    [StartingView setAlpha:0];
    
     
    
    
    
    Start = [[UIButton alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3, 60/2, 72/2)];
    [Start setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
    // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
    [Start setTag:170];
    [Start addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [StartingView addSubview:Start];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [format setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:now];
    
    
   CurrentDay = [comp weekday];
    
    
    
    
    
     Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, StartingView.frame.size.width, 40)];
    [Name setTextColor:[UIColor blackColor]];
    [Name setFont:[UIFont systemFontOfSize:14]];
    [Name setText:[format stringFromDate:now]];
    
    NSString *strName = @"";
    if (strName != nil || ![strName isEqualToString:@""])
    {
        strName = [Name.text stringByReplacingOccurrencesOfString:@"," withString:@"."];
        Name.text = strName;
    }
    
    [Name setTextAlignment:NSTextAlignmentLeft];
    [Name setTitle:@"Starting"];
    [Name setTag:120];
    [Name setBackgroundColor:[UIColor clearColor]];
    [Name setNumberOfLines:3];
    [StartingView addSubview:Name];
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
       
        StartingView.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        for (UILabel *label in StartingView.subviews) {
            
            
            if ([label isKindOfClass:[UILabel class]]) {
                
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        for (TekstIDLabel *label in StartingView.subviews) {
            
            
            if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        for (UIButton *label in StartingView.subviews) {
            
            
            if ([label isKindOfClass:[UIButton class]]) {
                
                label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }

        
    }
    
    else
    {
        
    }

    
    
    Ending = [[UILabel alloc] initWithFrame:CGRectMake(15, 120, self.view.frame.size.width-20, 40)];
//    [Ending setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [Ending setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [Ending setFont:[UIFont boldSystemFontOfSize:15]];
    [Ending setText:NSLocalizedString(@"Ending",nil)];
    [Ending setBackgroundColor:[UIColor clearColor]];
    [Ending setNumberOfLines:3];
    [self.view addSubview:Ending];
    [Ending setAlpha:0];
    
    
    EndingView = [[UIView alloc] initWithFrame:CGRectMake(10, 150, self.view.frame.size.width-20, 40)];
    [EndingView setBackgroundColor:[UIColor whiteColor]];
    [EndingView.layer setCornerRadius:10];
    [EndingView.layer setBorderWidth:1.5];
//    [EndingView.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 chanegs
    [EndingView.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [EndingView.layer setMasksToBounds:YES];
    [self.view addSubview:EndingView];
    [EndingView setAlpha:0];
    
    
    EndingViewText = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, EndingView.frame.size.width, 40)];
    [EndingViewText setTextColor:[UIColor blackColor]];
    [EndingViewText setFont:[UIFont systemFontOfSize:14]];
    [EndingViewText setText:NSLocalizedString(@"Never",nil)];
    [EndingViewText setTextAlignment:NSTextAlignmentLeft];
    [EndingViewText setTitle:@"Ending Date"];
    [EndingViewText setTag:120];
    [EndingViewText setBackgroundColor:[UIColor whiteColor]];
    [EndingViewText setNumberOfLines:3];
    [EndingView addSubview:EndingViewText];
    
    
    End = [[UIButton alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3, 60/2, 72/2)];
    [End setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
    // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
    [End setTag:171];
    [End addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [EndingView addSubview:End];
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        EndingView.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        for (UILabel *label in EndingView.subviews) {
            
            
            if ([label isKindOfClass:[UILabel class]]) {
                
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        for (TekstIDLabel *label in EndingView.subviews) {
            
            
            if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        for (UIButton *label in EndingView.subviews) {
            
            
            if ([label isKindOfClass:[UIButton class]]) {
                
                label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }

    }
    
    else
    {
        
    }

    
    
    Every = [[UILabel alloc] initWithFrame:CGRectMake(15, 180, self.view.frame.size.width-20, 40)];
//    [Every setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [Every setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [Every setFont:[UIFont boldSystemFontOfSize:15]];
    [Every setText:NSLocalizedString(@"Every",nil)];
    [Every setBackgroundColor:[UIColor clearColor]];
    [Every setNumberOfLines:3];
    [self.view addSubview:Every];
    [Every setAlpha:0];
    
    
    EveryView = [[UIView alloc] initWithFrame:CGRectMake(10, 210, self.view.frame.size.width-20, 40)];
    [EveryView setBackgroundColor:[UIColor whiteColor]];
    [EveryView.layer setCornerRadius:10];
    [EveryView.layer setBorderWidth:1.5];
//    [EveryView.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [EveryView.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [EveryView.layer setMasksToBounds:YES];
    [self.view addSubview:EveryView];
    [EveryView setAlpha:0];
    
    EveryAction = [[UIButton alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3, 60/2, 72/2)];
    [EveryAction setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
    // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
    [EveryAction setTag:177];
    [EveryAction addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [EveryView addSubview:EveryAction];
    
    
    
    EveryViewText = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, StartingView.frame.size.width, 40)];
    [EveryViewText setTextColor:[UIColor blackColor]];
    [EveryViewText setFont:[UIFont systemFontOfSize:14]];
    [EveryViewText setText:@""];
    [EveryViewText setTextAlignment:NSTextAlignmentLeft];
    [EveryViewText setTitle:@"Every"];
    [EveryViewText setTag:120];
    [EveryViewText setBackgroundColor:[UIColor clearColor]];
    [EveryViewText setNumberOfLines:3];
    [EveryView addSubview:EveryViewText];
    
    
    EveryLabel = [[UILabel alloc] initWithFrame:CGRectMake(205, 0, 60, 40)];
//    [EveryLabel setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [EveryLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [EveryLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [EveryLabel setText:NSLocalizedString(@"Days",nil)];
    [EveryLabel setBackgroundColor:[UIColor clearColor]];
    [EveryLabel setNumberOfLines:3];
    [EveryView addSubview:EveryLabel];
    [EveryLabel setAlpha:1];
    
    
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        EveryView.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        for (UILabel *label in EveryView.subviews) {
            
            
            if ([label isKindOfClass:[UILabel class]]) {
                
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        for (TekstIDLabel *label in EveryView.subviews) {
            
            
            if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        for (UIButton *label in EveryView.subviews) {
            
            
            if ([label isKindOfClass:[UIButton class]]) {
                
                label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
    }
    
    else
    {
        
    }

    
    End = [[UIButton alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3, 60/2, 72/2)];
    [End setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
   // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
    [End setTag:171];
    [End addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [EndingView addSubview:End];
    
    
    
    
  
    StartingTime = [[UILabel alloc] initWithFrame:CGRectMake(15, 240, self.view.frame.size.width-20, 40)];
//    [StartingTime setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [StartingTime setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [StartingTime setFont:[UIFont boldSystemFontOfSize:15]];
    [StartingTime setText:NSLocalizedString(@"Starting Time",nil)];
    [StartingTime setBackgroundColor:[UIColor clearColor]];
    [StartingTime setNumberOfLines:3];
    [self.view addSubview:StartingTime];
    [StartingTime setAlpha:0];
    
    
    
    StartingTimeView = [[UIView alloc] initWithFrame:CGRectMake(10, 270, self.view.frame.size.width-20, 40)];
    [StartingTimeView setBackgroundColor:[UIColor whiteColor]];
    [StartingTimeView.layer setCornerRadius:10];
    [StartingTimeView.layer setBorderWidth:1.5];
//    [StartingTimeView.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [StartingTimeView.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [StartingTimeView.layer setMasksToBounds:YES];
    [self.view addSubview:StartingTimeView];
    [StartingTimeView setAlpha:0];
    
  
    
    NSDateFormatter *format2 = [[NSDateFormatter alloc] init];
    [format2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
     [format2 setDateFormat:[myApp convertString:@"hh:mm a"]];
    
    
    StartingTimeText = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, StartingView.frame.size.width, 40)];
    [StartingTimeText setTextColor:[UIColor blackColor]];
    [StartingTimeText setFont:[UIFont systemFontOfSize:14]];
    [StartingTimeText setText:[format2 stringFromDate:now]];
    [StartingTimeText setTextAlignment:NSTextAlignmentLeft];
    [StartingTimeText setTitle:@"c"];
    [StartingTimeText setTag:120];
    [StartingTimeText setBackgroundColor:[UIColor clearColor]];
    [StartingTimeText setNumberOfLines:3];
    [StartingTimeView addSubview:StartingTimeText];
    
    
    
    StartTime = [[UIButton alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3, 60/2, 72/2)];
    [StartTime setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
    // [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
    [StartTime setTag:178];
    [StartTime addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [StartingTimeView addSubview:StartTime];
    
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        StartingTimeView.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        for (UILabel *label in StartingTimeView.subviews) {
            
            
            if ([label isKindOfClass:[UILabel class]]) {
                
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        for (TekstIDLabel *label in StartingTimeView.subviews) {
            
            
            if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        for (UIButton *label in StartingTimeView.subviews) {
            
            
            if ([label isKindOfClass:[UIButton class]]) {
                
                label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
    }
    
    else
    {
        
    }

    
    Choose = [[UILabel alloc] initWithFrame:CGRectMake(15, 180, self.view.frame.size.width-20, 40)];
//    [Choose setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [Choose setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [Choose setFont:[UIFont boldSystemFontOfSize:15]];
    [Choose setText:NSLocalizedString(@"Choose days of the week",nil)];
    [Choose setBackgroundColor:[UIColor clearColor]];
    [Choose setNumberOfLines:3];
    [self.view addSubview:Choose];
      [Choose setAlpha:0];
    
    
    
    Chooseview = [[ChooseView alloc] initWithFrame:CGRectMake(10, 210, self.view.frame.size.width-20, 40)];
    [Chooseview setBackgroundColor:[UIColor clearColor]];
    [Chooseview.layer setCornerRadius:10];
    [Chooseview.layer setBorderWidth:1.5];
//    [Chooseview.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 chanegs
    [Chooseview.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Chooseview.layer setMasksToBounds:YES];
    [self.view addSubview:Chooseview];
      [Chooseview setAlpha:0];
    
    [Chooseview getParant:self];
    
    
    
   times = [[UILabel alloc] initWithFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
//    [times setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [times setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [times setFont:[UIFont boldSystemFontOfSize:15]];
    [times setText:NSLocalizedString(@"Time(s) per Day",nil)];
    [times setBackgroundColor:[UIColor clearColor]];
    [times setNumberOfLines:3];
    [self.view addSubview:times];
      [times setAlpha:0];
    
    
    
    monthView = [[ScedulareView alloc] initWithFrame:CGRectMake(10, 30, 300, 30)];
    [monthView setBackgroundColor:[UIColor colorWithRed:0.941 green:0.941 blue:0.941 alpha:1.000]];
    [monthView.layer setBorderWidth:1];
    [monthView.layer setCornerRadius:10];
    [monthView setParant:self];
//    [monthView.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 chanegs
    [monthView.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [monthView.layer setMasksToBounds:YES];
    [self.view addSubview:monthView];

    
    
    Frequency = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, self.view.frame.size.width-20, 30)];
//    [Frequency setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [Frequency setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [Frequency setFont:[UIFont boldSystemFontOfSize:15]];
    [Frequency setText: NSLocalizedString(@"Frequency",nil)];
    [Frequency setBackgroundColor:[UIColor clearColor]];
    [Frequency setNumberOfLines:3];
    [self.view addSubview:Frequency];
    
    //273 × 39 pixels
    Schedule = [[UIButton alloc]  initWithFrame:CGRectMake(10, 30, 300, 39)];
    [Schedule setTitle:NSLocalizedString(@"Daily",nil) forState:UIControlStateNormal];
    [Schedule.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
//    [Schedule setTitleColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000] forState:UIControlStateNormal]; //28-06-2017 changes
     [Schedule setTitleColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000] forState:UIControlStateNormal];
    [Schedule setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Frequency.png"]]];
    [Schedule setTag:140];
    [Schedule addTarget:self action:@selector(viewanimate) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:Schedule];
    
    
    [Schedule.layer setCornerRadius:10];
    [Schedule.layer setBorderColor:[UIColor grayColor].CGColor];
    [Schedule.layer setBorderWidth:0.8];
    
  
 
    
    chosenFrequentcy=NSLocalizedString(@"Weekly",nil);
    //116
   if (self.view.frame.size.height == 548.000000) {
       
       
       
       
        table = [[TimeSchedulare alloc] initWithFrame:CGRectMake(10, 270, 300, 245)];
       [table setBackgroundColor:[UIColor clearColor]];
        table.separatorColor = [UIColor clearColor];
        table.backgroundColor = [UIColor clearColor];
        
        
        [table setEditing:NO];
    [self.view addSubview:table];
        
            [table getparant:self];
  
       
    
    }
    else
    {
        table = [[TimeSchedulare alloc] initWithFrame:CGRectMake(10, 270, 300, 116)];
         [table setBackgroundColor:[UIColor clearColor]];
        
        table.separatorColor = [UIColor clearColor];
        table.backgroundColor = [UIColor clearColor];
        
        
        [table setEditing:NO];
        [self.view addSubview:table];
        [table getparant:self];
        
        
    
        
  
    }
    
     [table setAlpha:0];
    
    add_button = [[Addbutton alloc]  initWithFrame:CGRectMake(0, 5, self.view.frame.size.width-20, 40)];
    [add_button setTag:170];
    [table addSubview:add_button];
   
    [add_button setbuttonAction:self];
 
    
    NewMedecineController *oldscreen =(NewMedecineController *)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
    
    ////////////////////////////////////////////////////////////////////NSLog(@"%@", oldscreen.ChosenMedicen);
    
    self.Interval =[oldscreen.ChosenMedicen valueForKey:@"Frequency"];
   
    [self chosenSchedule:[oldscreen.ChosenMedicen valueForKey:@"Frequency_text"]];
    [self getParant:oldscreen];
    
    
    BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
       if (strsetting) {
        
         //NSLog(@"YES");
    }
    else
        
    {
          //NSLog(@"NO");
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
        [table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
        
        NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"am", @"a.m.", @"vorm.", @"μ.μ.", @"AM"];
        
        NSArray *notificationArray= [table.TimesSet filteredArrayUsingPredicate:predicate];
        
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"pm", @"p.m.", @"nachm.", @"π.μ.", @"PM"];
        
        NSArray *notificationArray2= [table.TimesSet filteredArrayUsingPredicate:predicate2];
        
        [table.TimesSet removeAllObjects];
        
        [table.TimesSet addObjectsFromArray:notificationArray];
        [table.TimesSet addObjectsFromArray:notificationArray2];
        
        
        
        NSArray *copy = [table.TimesSet copy];
        NSInteger index = [copy count] - 1;
        for (id object in [copy reverseObjectEnumerator]) {
            if ([table.TimesSet indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                [table.TimesSet removeObjectAtIndex:index];
            }
            index--;
        }
      
        
        //NSLog(@"%@", table.TimesSet);
    }
    
    [table reloadData];

  
}



-(void)viewanimate

{
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];
    
    
    //10, 25, 300, 39
    
    if (monthView.frame.size.height == 30) {
        [monthView setFrame:CGRectMake(10, 40, 300,30*4)];
        [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22*12)];
        
    }
    else
    {
        [monthView setFrame:CGRectMake(10, 40,300,30)];
        [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22)];
        
    }
    
    [UIView commitAnimations];
    
  
    
}


-(void)chosenSchedule:(NSString*)sender

{
   
    ////////////////////////////////////////////////////NSLog(@"%f", self.view.frame.size.height);
    

    chosenFrequentcy =sender;
        
        if ([sender isEqualToString:NSLocalizedString(@"Daily",nil)]) {
            
            [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
      
            
            [EveryViewText setText:@"24"];
            [DayinaRow removeAllObjects];
            //[table.TimesSet removeAllObjects];
            [DayRow removeAllObjects];
            [NumberOfDays removeAllObjects];
            [Chooseview cleanup];
           
            [EveryView setAlpha:0];
            [Every setAlpha:0];
            
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            
            [times setAlpha:1];
            [table setAlpha:1];
            
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
            [table setFrame:CGRectMake(10, 210, 300, 266)];
            [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
            
            if (self.view.frame.size.height >=504.000000) {
                [table setFrame:CGRectMake(10, 210, 300, 266)];
                [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 210, 300, 166)];
                [times setFrame:CGRectMake(15, 183, self.view.frame.size.width-20, 40)];
            }
            
              [self setDayNumber];
            
        }
        if ([sender isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
           
            [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
            
            [EveryViewText setText:@""];
            [EveryLabel setText:NSLocalizedString(@"Days",nil)];
            //[table.TimesSet removeAllObjects];
            [DayinaRow removeAllObjects];
              [DayRow removeAllObjects];
            [NumberOfDays removeAllObjects];
           
            [Chooseview cleanup];
            [EveryView setAlpha:1];
            [Every setAlpha:1];
            [EveryViewText setTitle:NSLocalizedString(@"Every X Days",nil)];
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            [Choose setAlpha:0];
            [Chooseview setAlpha:0];
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            [times setAlpha:1];
            [table setAlpha:1];
            
            
            
         
            
            if (self.view.frame.size.height >=504.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 205)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
            
            
            [self setDayNumber];
            
        }
        if ([sender isEqualToString:NSLocalizedString(@"Weekly",nil)]) {
            
          
            Interval =@"168";
            [Schedule setTitle:chosenFrequentcy forState:UIControlStateNormal];
            
            [EveryViewText setText:@"168"];
            [EveryView setAlpha:0];
            [Every setAlpha:0];
            [DayinaRow removeAllObjects];
              [DayRow removeAllObjects];
            //[table.TimesSet removeAllObjects];
            [NumberOfDays removeAllObjects];
           
            [Chooseview cleanup];
            [StartingView setAlpha:1];
            [Starting setAlpha:1];
            [EndingView setAlpha:1];
            [Ending setAlpha:1];
            [times setAlpha:1];
            [table setAlpha:1];
            [Choose setAlpha:1];
            [Chooseview setAlpha:1];
            [StartingTime setAlpha:0];
            [StartingTimeView setAlpha:0];
            
            
            if (self.view.frame.size.height >=504.000000) {
                [table setFrame:CGRectMake(10, 270, 300, 245)];
                
            }
            
            else
            {
                [table setFrame:CGRectMake(10, 270, 300, 116)];
            }
            [times setFrame:CGRectMake(15, 245, self.view.frame.size.width-20, 40)];
            
            
        }
  

 

   
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];
    [monthView setFrame:CGRectMake(10, 40,300,30)];
    [monthView.table setFrame:CGRectMake(0.0, 0, 320, 22)];
    
    [UIView commitAnimations];
    
    
    
    
}

-(void)plaese:(NSString*)set

{
    
    
   
    
    /* //27-06-2017 change
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:[NSString stringWithFormat:NSLocalizedString(@"Fill in a %@ first to continue",nil), set]
                                                   delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
    [alert show];
    */
    
    UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"",nil) message:[NSString stringWithFormat:NSLocalizedString(@"Fill in a %@ first to continue",nil), set] preferredStyle:UIAlertControllerStyleAlert];
    [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
        
    });

    
}


- (IBAction)back:(UIButton *)sender {

    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
    
    NSLog(@"%@",NSLocalizedString(@"en_US",nil));
    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateConvert setDateFormat:@"EEEE d, MMMM yyyy"];


    [parantIt.ChosenMedicen setObject:[dateConvert dateFromString:Name.text] forKey:@"Starting"];

    [parantIt.Schdules.strating setText:Name.text];
    

    
    NSString *uploadString = [[DayRow valueForKey:@"Day"] componentsJoinedByString:@","];
    [parantIt setitemsDictionary:uploadString name:@"Dates"];
    
    if ([chosenFrequentcy isEqualToString:NSLocalizedString(@"Weekly",nil)])
    {
    [parantIt.Schdules.Dates setText:uploadString];
        [parantIt.Schdules.Dates sizeToFit];
        

        
    }
    else
    {
         [parantIt.Schdules.Dates setText:@""];
    }
    
    
    NSString *uploadString2 = [[DayRow valueForKey:@"Number"] componentsJoinedByString:@","];
    [parantIt setitemsDictionary:uploadString2 name:@"Day_number"];
    
    
    [parantIt.ChosenMedicen setObject:EndingViewText.text forKey:@"Duration"];
    [parantIt.Schdules.ending setText:EndingViewText.text];

    
    
    [parantIt.ChosenMedicen setObject:chosenFrequentcy forKey:@"Frequency_text"];
    //[parantIt.Schdules.Frequent setText:chosenFrequentcy];
    
    


      if ([chosenFrequentcy isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
            
            
            NSMutableArray *timesIt = [[NSLocalizedString(@"Every X Days",nil) componentsSeparatedByString:@" "] mutableCopy];
            
            [timesIt replaceObjectAtIndex:1 withObject:Interval];
        
     
              [parantIt.Schdules.FrequentNumber  setText:[NSString stringWithFormat:@"%@ %@ %@", [timesIt objectAtIndex:0],[timesIt objectAtIndex:1],[timesIt objectAtIndex:2]]];
            
        }
        else  if ([chosenFrequentcy isEqualToString:NSLocalizedString(@"Daily",nil)]) {
            
            [parantIt.Schdules.FrequentNumber  setText:chosenFrequentcy];
           
        }
        else  if ([chosenFrequentcy isEqualToString:NSLocalizedString(@"Weekly",nil)]) {
            
            
            [parantIt.Schdules.FrequentNumber  setText:chosenFrequentcy];
        }

    
    BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
        if (strsetting) {
         //NSLog(@"YES");
    }
    else
        
    {
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
        [table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
        
         NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"am", @"a.m.", @"vorm.", @"μ.μ.", @"AM"];
        
        NSArray *notificationArray= [table.TimesSet filteredArrayUsingPredicate:predicate];
        
       NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"pm", @"p.m.", @"nachm.", @"π.μ.", @"PM"];
        
        NSArray *notificationArray2= [table.TimesSet filteredArrayUsingPredicate:predicate2];
        
        [table.TimesSet removeAllObjects];
        
        [table.TimesSet addObjectsFromArray:notificationArray];
        [table.TimesSet addObjectsFromArray:notificationArray2];
     
        NSArray *copy = [table.TimesSet copy];
        NSInteger index = [copy count] - 1;
        for (id object in [copy reverseObjectEnumerator]) {
            if ([table.TimesSet indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                [table.TimesSet removeObjectAtIndex:index];
            }
            index--;
        }
    }

    

     NSString *uploadString3 = [[table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
    [parantIt.ChosenMedicen setObject:uploadString3 forKey:@"Times"];
    [parantIt.Schdules.times setText:[uploadString3 stringByReplacingOccurrencesOfString:@"," withString:@"\n"]];
   
    if ([table.TimesSet count] ==1||[table.TimesSet count] ==0) {
        
        [parantIt DynamicCell:34+39+39+39];
    }
    else
    {
        [parantIt DynamicCell:(34+39+39+(39*[table.TimesSet count])) ];
        
    }

    
    [parantIt.Schdules.times setNumberOfLines:[table.TimesSet count]];

   
    if ([[parantIt.ChosenMedicen valueForKey:@"Times"] isEqualToString:@""]) {
        
        if ([add_button.EndingViewText.text isEqualToString:NSLocalizedString(@"add new time",nil)]) {
            
  
          
              [[self navigationController] popViewControllerAnimated:YES];
            
        }
        
        else
        {
       
        [self Addit];
     
            
        }
        
        
    }
    
    else
    {
        
        if ([self SeeifEndingDateIsPast:parantIt.ChosenMedicen]) {
          
           
            [[self navigationController] popViewControllerAnimated:YES];
        }
        else
        {
        
            /* //27-06-2017 change
            UIAlertView *alertit = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Ending date is in the past or the same!",nil)
                                                             delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
            [alertit show];
   */
             
            UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Ending date is in the past or the same!",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                
            });

            
            
                 [parantIt.ChosenMedicen setObject:NSLocalizedString(@"Never",nil) forKey:@"Duration"];
              [EndingViewText setText:NSLocalizedString(@"Never",nil)];
            
        }
        
    }
}

-(BOOL)SeeifEndingDateIsPast:(NSDictionary *)notification {
   
   
    
    BOOL set;
    


    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd 00:00:01 PM +0000"];
    

    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc]init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd hh:mm:ss a +0000"];
    
    
    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    NSDate *let =[dateConvert dateFromString:[notification valueForKey:@"Duration"]];
    
    
    NSDate *get =[notification valueForKey:@"Starting"];
    

    
    
    if ([[notification valueForKey:@"Duration"] isEqualToString:NSLocalizedString(@"Never",nil)])
    {
        set = YES;
        
    }
    else
    {
        if  ([let compare:get]==NSOrderedSame)
            
        {
            
            set = YES;
          
            
        }
        else if  ([let compare:get]==NSOrderedDescending)
            
            
        {
            
       
            
            set = YES;
            
        }
        else
            
        {
            
           
            
            set = NO;
        }
    }
    
    return set;
}


-(void)change:(UIButton*)sender

{
    ////NSLog(@"%i", sender.tag);
    
   
    
    if (sender.tag ==170) {
        
        DurationViewController *controller = [[DurationViewController alloc]  init];
        [controller setTitle:NSLocalizedString(@"Starting Date",nil)];
        
        
        
      
        
        if ([controller.TitleTextOut.text isEqualToString:NSLocalizedString(@"add new time",nil)]) {
            
            [controller.TitleTextOut setText:@""];
        }
        else
            
        {
            
            
            
            
            
        }
        
        
        
        [self.navigationController pushViewController:controller animated:YES];
        
        controller.titleEdit=@"add";
        [controller getParant:self];
        
        
        for (UIView *view in self.view.subviews) {
            
            
            
            for (TekstIDLabel *label in view.subviews) {
                
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    if ([label.title isEqualToString:@"Starting"]) {
                        
                        
                        [controller whatLabel:label];
                        CopyLabel =label;
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    if (sender.tag ==171) {
        
        DurationViewController *controller = [[DurationViewController alloc]  init];
        [controller setTitle:NSLocalizedString(@"Ending Date",nil)];
        [self.navigationController pushViewController:controller animated:YES];
        
        controller.titleEdit=@"add";
        [controller getParant:self];
        

            
            for (UIView *view in self.view.subviews) {
                
                for (TekstIDLabel *label in view.subviews) {
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        if ([label.title isEqualToString:@"Ending Date"]) {
                            
                            [controller whatLabel:label];
                            
                        }
                    }
                    
                }
                
            }
            
        }
        
    if (sender.tag ==177) {
        
        HoursViewController *controller = [[HoursViewController alloc]  init];
        [controller setTitle:chosenFrequentcy];
        [controller getParant:self];
        
        [self.navigationController pushViewController:controller animated:YES];
        
        
        
        
        [controller getParant:self];
        
       
            
            
            for (UIView *view in self.view.subviews) {
                
                for (TekstIDLabel *label in view.subviews) {
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        if ([label.title isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
                          
                            [controller whatLabel:label];
                            
                        }
                        
                       
                    }
                    
                }
                
            }
            
        }
    
    
    if (sender.tag ==178) {
        
        DurationViewController *controller = [[DurationViewController alloc]  init];
        [controller setTitle:NSLocalizedString(@"Starting Time",nil)];
        [self.navigationController pushViewController:controller animated:YES];
        
        //controller.titleEdit=@"add";
        [controller getParant:self];
        
        
        [controller whatLabel:StartingTimeText];
        
    }
    
    if (sender.tag ==172) {
        
        DurationViewController *controller = [[DurationViewController alloc]  init];
        [controller setTitle:NSLocalizedString(@"Reminder Time",nil)];
        [self.navigationController pushViewController:controller animated:YES];
        
        controller.titleEdit=@"add";
        [controller getParant:self];
        
        
        
        
        for (UIView *label in table.subviews) {
            
            if ([label isKindOfClass:[Addbutton class]]) {
                
                for (UIView *label2 in label.subviews) {
                    
                    if ([label2 isKindOfClass:[UIView class]]) {
                        
                        
                        for (TekstIDLabel *label4 in label2.subviews) {
                            
                            if ([label4 isKindOfClass:[TekstIDLabel class]]) {
                                
                                
                                [controller whatLabel:label4];
                                
                            }
                            
                        }
                        
                    }
                    
                    
                    
                    
                }
                
            }
            
            
        }
        
        for (TekstIDLabel *label in add_button.subviews) {
            
            if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                if ([label.title isEqualToString:@""]) {
                    
                    [controller whatLabel:label];
                    
                    
                }
                
            }
            
        }
        
    }
    
    
}



-(void)Addit

{
 
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [dateFormat setDateFormat:[myApp convertString:@"hh:mm a"]];
    
    
    
    NSDateFormatter *dateFormat24 = [[NSDateFormatter alloc] init];
    [dateFormat24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat24 setDateFormat:[myApp convertString:@"hh:mm a"]];


  
    NSDate *now =[dateFormat dateFromString:[myApp changesDone:add_button.EndingViewText.text]];
    //


    NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
   
   
   
    [set setObject:[dateFormat24 stringFromDate:now]  forKey:@"Date"];
    [set setObject:add_button.EndingViewText.text forKey:@"Time"];
   
    if ([[table.TimesSet valueForKey:@"Time" ] containsObject:set]) {
        
    }
    else
    {
           [table.TimesSet addObject:set];
    }
 
    
   
    
    
    [add_button.AddButton setHidden:YES];
    [add_button.EndingViewText setText:NSLocalizedString(@"add new time",nil)];
    
  
    
    [table reloadData];
    
    // NSString *uploadString = [[table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
    
    
    
 
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
    [table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    
    
}



-(void)setDayNumber
{

    [DayRow removeAllObjects];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy"];
   
   
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:[dateFormatter dateFromString:Name.text]];
    
    
    CurrentDay = [comp weekday];
    
    
    NSArray *names =[NSArray arrayWithObjects:@"Su,1",@"Mo,2",@"Tu,3",@"We,4",@"Th,5",@"Fr,6",@"Sa,7", nil];
    
   for (NSInteger i =0; i < [names count]; i++) {
        
        
        NSArray *numbers2 = [[names objectAtIndex:i] componentsSeparatedByString:@","];
        
        
        if (CurrentDay == [[numbers2 objectAtIndex:1] intValue]) {
            
            
            NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
            [set setObject:NSLocalizedString([numbers2 objectAtIndex:0],nil) forKey:@"Day"];
            [set setObject:[numbers2 objectAtIndex:1]  forKey:@"Number"];
            
            
            [DayRow addObject:set];
            
            
        }
        
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Number" ascending:TRUE];
    [DayRow sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    
}







-(void)gotoScreen:(TekstIDLabel*)sender count:(NSInteger)rowit;

{
    
    
    DurationViewController *controller = [[DurationViewController alloc]  init];
    [controller setTitle:NSLocalizedString(@"Reminder Time",nil)];
    [self.navigationController pushViewController:controller animated:YES];
    controller.titleEdit= @"Edit";
    controller.countDict =rowit;
    
    [controller getParant:self];
    [controller whatLabel:sender];
    
   for (NSInteger i=0 ; i<[DayinaRow count]; i++) {
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        
        for (id key in [DayinaRow objectAtIndex:i]) {
            
            
            
            if ([key isEqualToString:@"Times"]) {
                
           
                NSString *uploadString = [[table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
                
                [set setObject:uploadString forKey:@"Times"];
            }
            
            else
            {
                
                [set setObject:[[DayinaRow objectAtIndex:i] objectForKey:key] forKey:key];
                
            }
            
        }
        
        [DayinaRow replaceObjectAtIndex:i withObject:set];
        
    }
    
    
    
    
}



-(void)setDayinaRowIt:(NSDate*)sender

{
    

    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    
    [dateFormatter1 setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    
    [dateFormatter2 setDateFormat:@"dd, MMMM yyyy"];
    

    
   
}


-(void)getParant:(UIViewController*)parant

{
  
    
    
    parantIt =(NewMedecineController*)parant;
    
    
    [EveryViewText setText:[parantIt.ChosenMedicen valueForKey:@"Frequency"]];
    if ([[parantIt.ChosenMedicen valueForKey:@"Frequency_text"] isEqualToString:@" "]) {
        
    }
    
    else
    {
    
   
        
        NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
        [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];

    
    if ([dateConvert stringFromDate:[parantIt.ChosenMedicen valueForKey:@"Starting"]]) {

        
      [Name setText:[dateConvert stringFromDate:[parantIt.ChosenMedicen valueForKey:@"Starting"]]];
        
        NSString  *name = Name.text;

        Name.text = [name stringByReplacingOccurrencesOfString:@"," withString:@"."];
           
    }
    else
    {
       
    }
    if ([parantIt.ChosenMedicen valueForKey:@"Duration"]) {
        
          [EndingViewText setText:[parantIt.ChosenMedicen valueForKey:@"Duration"]];
    }
    else
    {
       
    }
    
  
  

    if ([chosenFrequentcy isEqualToString:NSLocalizedString(@"Every X Days",nil)]||[chosenFrequentcy isEqualToString:NSLocalizedString(@"Daily",nil)]) {
        

    [self setDayNumber];
        
    }
        
        
        

    
    NSArray *Days = [[parantIt.ChosenMedicen valueForKey:@"Day_number"] componentsSeparatedByString:@","];
   for (NSInteger k=0 ; k<[Days count]; k++) {
        
        
        NSArray *names =[NSArray arrayWithObjects:@"Sun",@"Mon",@"Tue",@"Wen",@"",@"Fri",@"Sat", nil];
        NSArray *number =[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7", nil];
        
        
       
       for (NSInteger i=0 ; i<[names count]; i++) {
            
            if ([[number objectAtIndex:i] isEqualToString:[Days objectAtIndex:k]]) {
                
                NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                [set setObject:NSLocalizedString([names objectAtIndex:i],nil) forKey:@"Day"];
                [set setObject:[number objectAtIndex:i]  forKey:@"Number"];
                
                if ([DayRow containsObject:set]) {
                    
                }
                else
                {
                //[DayRow addObject:set];
                    
                }
                
                 Addbutton *addbutton = (Addbutton *)[Chooseview viewWithTag:(140+i)];
                
                [Chooseview change:addbutton];
              
            }
            
            
        }
 
        
        NSArray *Timeit = [[parantIt.ChosenMedicen valueForKey:@"Times"] componentsSeparatedByString:@","];
        
        
      
          for (NSInteger k =0; k < [Timeit count]; k++) {
               
               if ([[Timeit objectAtIndex:k] isEqualToString:@""]) {
                   
               }
               else
               {
                   
                   
                   
              
                   
                   NSString *time=[Timeit objectAtIndex:k];
           
                   
                   NSDateFormatter *format24 = [[NSDateFormatter alloc] init];
                   [format24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                   [format24 setDateFormat:@"HH:mm"];
                   
                   
                   NSDateFormatter *format = [[NSDateFormatter alloc] init];
                   [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                   [format setDateFormat:@"hh:mm a"];
                   
                   
                   
                   if ([format24 dateFromString:time]) {
                       
                       
                        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
                       
                        if (!strsetting)
                       {
                           // time = [format stringFromDate:[format24 dateFromString:time]];
                           
                           ////////////////NSLog(@"NO format24 %@", [format stringFromDate:[format24 dateFromString:time]]);
                           
                           time= [format stringFromDate:[format24 dateFromString:time]];
                       }
                       
                       else
                       {
                           
                           ////////////////NSLog(@"YES format24 %@", time);
                           // time = [format stringFromDate:[format24 dateFromString:time]];
                       }
                       
                       
                   }
                   else
                       
                   {
                        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
                       
                        if (!strsetting)
                       {
                           
                          ////////////////NSLog(@"YES format %@", time);
                       }
                       
                       else
                       {
                           ////////////////NSLog(@"NO format %@", [format24 stringFromDate:[format dateFromString:time]]);
                           // time = [format stringFromDate:[format24 dateFromString:time]];
                           
                           time= [format24 stringFromDate:[format dateFromString:time]];
                           
                        
                           // time = [format stringFromDate:[format24 dateFromString:time]];
                       }
                       
                       
                       
                       
                   }
                   
                   

                   
                   
                   AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];

               
               NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
               [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
               
               [dateFormat setDateFormat:[myApp convertString:@"hh:mm a"]];
               

               NSDateFormatter *dateFormat24 = [[NSDateFormatter alloc] init];
               [dateFormat24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
               [dateFormat24 setDateFormat:[myApp convertString:@"hh:mm a"]];

                   

                
                   NSDate *now =[dateFormat dateFromString:[myApp changesDone:time]];



                   

               
              
               
               NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
               
               
               [set setObject:[dateFormat24 stringFromDate:now]  forKey:@"Date"];
               [set setObject:time forKey:@"Time"];
              
              
               
              
              
               if ([table.TimesSet containsObject:set]) {
                   
               }
               else
               {
                   [table.TimesSet addObject:set];
                   
               }
                   
               }
               
            
           }
        
        
        
         
      

        
        if ([table.TimesSet count] >0) {
           
            [StartingTimeText setText:[[table.TimesSet valueForKey:@"Time"] objectAtIndex:0]];
            
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
        [table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
        
        
        [table reloadData];
        
    }
        
    }
    
    
    
}



@end
