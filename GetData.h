//
//  GetData.h
//  VVS_new
//
//  Created by Jeffrey Snijder on 14-01-13.
//  Copyright (c) 2013 Kieran Lafferty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetData : NSObject


+(NSMutableArray*) getHistory:(NSArray*)itemsnamen OnID:(NSString*)ID;
+(NSMutableArray*) getMonth:(NSArray*)itemsnamen;
+(NSMutableArray*) getContents:(NSArray*)itemsnamen;
+(NSMutableArray*) getSetting;
+(NSMutableDictionary*) getNotifictionwithID:(NSString*)ID;
+(NSMutableArray*) getMusic;
+(NSMutableArray*) getRecording;
+(NSMutableArray*) getRecordingWithEmpty;
+(NSMutableArray*) getDrugTypeStrength:(NSString*)Type;
+(NSMutableArray*) getMedicine;
+(NSMutableArray*) getMedicinewithProfileID:(NSString*)ID;
+(NSMutableArray*)getInstellingen;

+(NSString*)getHistorySQL:(NSDictionary*)items;
+(NSMutableArray*) getHistoryTotal;
+(NSMutableDictionary*) getMedicinewithIDCopy:(NSString*)ID;
+(NSMutableDictionary*) getAktiveMeds:(NSString*)ID;
+(NSMutableArray*) getHistory:(NSArray*)itemsnamen OnMedID:(NSString*)ID;
@end
