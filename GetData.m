//
//  GetData.m
//  VVS_new
//
//  Created by Jeffrey Snijder on 14-01-13.
//  Copyright (c) 2013 Kieran Lafferty. All rights reserved.
//

#import "GetData.h"
#import "sqlite3.h"
#import "AppDelegate.h"
#import "Profiles.h"

@implementation GetData

#pragma mark - Core Data stack



#pragma mark - Core Data old


+(NSMutableArray*) getSetting


{
    
  
    
    NSArray *names =[NSArray arrayWithObjects:@"Second_alert",@"Disturb_in",@"Disturb_out",@"Late",@"Early",@"History",@"ID",@"Disturb_out_ad",@"Disturb_in_ad", nil];
    
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    NSString* searchQuery =@"SELECT Second_alert,Disturb_in,Disturb_out,Late,Early,History,ID,Disturb_out_ad, Disturb_in_ad from Settings";
    
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            
           for (NSInteger i =0; i < [names count]; i++) {
                
                
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[names objectAtIndex:i]];
            }
            
            
            [Take addObject:item];
            
            
            
            
        }
        
    }
    
    
    //05-09-2017
    sqlite3_finalize(selectStatement);
    sqlite3_close(database);
    
    
    return Take;
    
}


+(NSMutableArray*) getMusic


{
    
    
    
    NSArray *names =[NSArray arrayWithObjects:@"ID",@"title",@"url", nil];
    
    
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    NSString* searchQuery =@"SELECT ID,title,url from Music";
    
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            
           for (NSInteger i =0; i < [names count]; i++) {
                
                
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[names objectAtIndex:i]];
                
                
            }
            
            
            [Take addObject:item];
            
            
            
            
        }
        
    }
    
    //05-09-2017
    sqlite3_finalize(selectStatement);
    sqlite3_close(database);
    
    
    return Take;
    
}



+(NSString*)getHistorySQL:(NSDictionary*)items
{
    
  
    NSArray *names =[NSArray arrayWithObjects:@"ID",@"Date",@"Time",@"Name",@"Dose",@"Taken",@"ID_medicine",@"Count",@"Time_Action",@"Action",@"Quantity",@"Type",@"Time_Action_24",@"Original_Time",@"Action_Date", nil];
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    NSString *exists = @"-";
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "SELECT * from History WHERE (ID_medicine) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [[items objectForKey:@"ID_Medicine"] UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            
            
           for (NSInteger i =0; i < [names count]; i++) {
                
                
                if ([[names objectAtIndex:i] isEqualToString:@"Taken"]) {
                    
                    
                    if ([[NSString stringWithUTF8String:(char *)sqlite3_column_text(syncStatementCheck, i)] isEqualToString:@"Taken earlier"]) {
                        
                        
                        exists = @"Taken earlier";
                        
                        
                    }
                    else if ([[NSString stringWithUTF8String:(char *)sqlite3_column_text(syncStatementCheck, i)] isEqualToString:@"Taken"]) {
                        
                        exists = @"Taken";
                        
                        
                    }
                    else if ([[NSString stringWithUTF8String:(char *)sqlite3_column_text(syncStatementCheck, i)] isEqualToString:@"Skipped"]) {
                        
                        exists = @"Skipped";
                        
                        
                    }
                    else if ([[NSString stringWithUTF8String:(char *)sqlite3_column_text(syncStatementCheck, i)] isEqualToString:@"Postponed"]) {
                        
                        exists = @"Postponed";
                        
                        
                    }
                    
                    
                }
                
                
            }
            
            
            //[item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[names objectAtIndex:i]];
        }
    }
    
//    sqlite3_finalize(syncStatementCheck); 05-09-2017 comment this and add below two line
    
    
    sqlite3_finalize(syncStatementCheck);
    sqlite3_close(database);
    
    
   
    
    return exists;
    
}

+(NSMutableArray*) getRecording


{
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    
    NSArray *names =[NSArray arrayWithObjects:@"ID",@"title",@"url", nil];
    
    
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    NSString* searchQuery =@"SELECT ID,title,url from Recordings";
    
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            
           for (NSInteger i =0; i < [names count]; i++) {
                
                
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[names objectAtIndex:i]];
                
                
                
            }
            
            
            NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
            [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [dateFormatLevel setDateFormat:[myApp convertString:@"dd/MM/yyyy hh:mm a"]];
            
            
            [item setObject:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]] forKey:@"Date_time"];
            
            
            
            
            if ([[item valueForKey:@"Action_Date"]length]>0) {
                
                
                [item setObject:[dateFormatLevel dateFromString:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]]] forKey:@"Action_Date"];
            }
            else
            {
                
                NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
                [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateFormatLevel setDateFormat:[myApp convertString:@"dd/MM/yyyy hh:mm a"]];
                
                
                [item setObject:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]] forKey:@"Action_Date"];
                [Take addObject:item];
                
            }
            
            
            if ([[item valueForKey:@"title"] isEqualToString:@"Leeg"]) {
                
                [Take removeObject:item];
                
            }
            else
            {
                
            }
            
            
            
        }
        
    }
    
    //05-09-2017
    sqlite3_finalize(selectStatement);
    sqlite3_close(database);
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date_time" ascending:TRUE];
    [Take sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    
    
    
    
    return Take;
    
}




+(NSMutableArray*) getHistoryTotal


{
  

    
    NSArray *names =[NSArray arrayWithObjects:@"ID",@"Date",@"Time",@"Name",@"Dose",@"Taken",@"ID_medicine",@"Count",@"Time_Action",@"Action",@"Quantity",@"Type",@"Time_Action_24",@"Original_Time",@"Action_Date", nil];
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    
    NSString* searchQuery =@"SELECT * from History";
    
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            
           for (NSInteger i =0; i < [names count]; i++) {
                
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[names objectAtIndex:i]];
                
                
                
                
            }
            
            AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            
            
            NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
            [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [dateFormatLevel setDateFormat:[myApp convertString:@"dd/MM/yyyy hh:mm a"]];
            
            
            [item setObject:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]] forKey:@"Date_time"];
            
            
            
            
            
            if ([[item valueForKey:@"Action_Date"]length]>0) {
                
                [item setObject:[dateFormatLevel dateFromString:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]]] forKey:@"Action_Date"];
            }
            else
            {
                
                NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
                [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateFormatLevel setDateFormat:[myApp convertString:@"dd/MM/yyyy hh:mm a"]];
                
                
                [item setObject:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]] forKey:@"Action_Date"];
                
                
            }
            
            
            [Take addObject:item];
            
            
            if ([[item valueForKey:@"title"] isEqualToString:@"Leeg"]) {
                
                [Take removeObject:item];
                
            }
            else
            {
                
            }
            
            
            
            
            
        }
        
        
        
    }
    
    //05-09-2017
    sqlite3_finalize(selectStatement);
    sqlite3_close(database);
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date_time" ascending:FALSE];
    [Take sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    
    
  
    return Take;
    
}


+(NSMutableArray*) getRecordingWithEmpty


{
    
    
    
    NSArray *names =[NSArray arrayWithObjects:@"ID",@"title",@"url", nil];
    
    
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    NSString* searchQuery =@"SELECT ID,title,url from Recordings";
    
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            
           for (int i =0; i < [names count]; i++) {
                
                
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[names objectAtIndex:i]];
                
                
                
            }
            
            
            [Take addObject:item];
            
            
            
            
            
            
        }
        
    }
    
    //05-09-2017
    sqlite3_finalize(selectStatement);
    sqlite3_close(database);
    
    return Take;
    
}



+(NSMutableArray*) getDrugTypeStrength:(NSString*)Type
{
    Type = NSLocalizedString(Type,nil);

    
    NSLog(@" Type: %@",Type);
    
    NSArray *names1 = [NSArray arrayWithObjects:@"Drop",@"Inhaler",@"Injection",@"Liquid", @"Ointment",@"Patch",@"Tablet",@"Spray", nil];
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    if ([names1 containsObject:Type]) {
        
        //@"Quantity",@"Duration",@"Instructions"hist
        
        NSArray *itemsnamen =[NSArray arrayWithObjects:@"Drug_type",@"Dose_unit",@"Strength_unit",@"Dose",@"Strength",@"Location",@"Format_dose",@"Format_Strength",@"Remaining_Qty_unit",@"Refill_Qty_unit",@"Refills_Remaining_unit",@"Format_Rem_Qty",@"Format_Refill_Qty",@"Format_Refills_Rem",@"Dose_name",@"Strength_name",@"Location_name",@"Locations",@"Remaining_name",@"Refill_name",@"Drug_types",@"Drug_less",@"Minimume_name", nil];
        
        
        sqlite3 *database = [AppDelegate getNewDBconnection];
        
        sqlite3_stmt *selectStatement;
        
        
        
        
        NSString* searchQuery =@"SELECT Drug_type,Dose_unit,Strength_unit,Dose,Strength,Location,Format_dose,Format_Strength,Remaining_Qty_unit,Refill_Qty_unit,Refills_Remaining_unit,Format_Rem_Qty,Format_Refill_Qty,Format_Refills_Rem,Dose_name,Strength_name,Location_name,Locations,Remaining_name,Refill_name,Drug_types, Drug_less,Minimume_name from Contents WHERE (Drug_type) = (?)";
        const char *sql =   [searchQuery UTF8String];
        
        
        NSLog(@"searchQuery :  %@",searchQuery);
        
        
        if(sqlite3_prepare_v2(database, sql, -2, &selectStatement, NULL) == SQLITE_OK)
        {
            
            NSLog(@"sqlite3_prepare_v2 : ");

            sqlite3_bind_text(selectStatement, 1, [Type UTF8String], -1, SQLITE_TRANSIENT);
            
            
            while(sqlite3_step(selectStatement) == SQLITE_ROW)
            {
                
                NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
                
                
               for (int i =0; i < [itemsnamen count]; i++) {
                    
                    if ([[itemsnamen objectAtIndex:i] isEqualToString:@"Refill_name"]||[[itemsnamen objectAtIndex:i] isEqualToString:@"Remaining_name"]||[[itemsnamen objectAtIndex:i] isEqualToString:@"Dose_name"]) {
                        
                     
                     
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
                            
                        {
                            
                            NSString *thestring = NSLocalizedString([NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)],nil);
                            [item  setObject:thestring forKey:[itemsnamen objectAtIndex:i]];
                            
                        }
                        
                        else
                        {
                           
                            if ([[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] isEqualToString:@"Ointment"]) {
                                
                                [item  setObject:NSLocalizedString(@"Application",nil) forKey:[itemsnamen objectAtIndex:i]];
                                
                            }
                            else if ([[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] isEqualToString:@"Ointments"]) {
                                
                                [item  setObject:NSLocalizedString(@"Application",nil) forKey:[itemsnamen objectAtIndex:i]];
                                
                            }
                            else
                                
                            {
                                [item  setObject:NSLocalizedString([NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)],nil) forKey:[itemsnamen objectAtIndex:i]];
                                
                            }
                            
                            
                        }
                        
                    }
                    
                    else
                    {
                        
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
                            
                        {
                            
                            NSString *thestring = NSLocalizedString([NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)],nil);
                            [item  setObject:thestring forKey:[itemsnamen objectAtIndex:i]];
                            
                        }
                        
                        else
                        {
                            if ([[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] isEqualToString:@"Ointment"]) {
                                
                                 [item  setObject:NSLocalizedString(@"Application",nil) forKey:[itemsnamen objectAtIndex:i]];
                                
                            }
                            else if ([[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] isEqualToString:@"Ointments"]) {
                                
                                [item  setObject:NSLocalizedString(@"Application",nil) forKey:[itemsnamen objectAtIndex:i]];
                                
                            }
                            else
                            {
                            
                            [item  setObject:NSLocalizedString([NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)],nil) forKey:[itemsnamen objectAtIndex:i]];
                                
                            }
                            
                            
                        }
                    }
                    
                    
                }
                
              
                [Take addObject:item];
                
                
                
            }
            
        }
        
        //05-09-2017 add new line for close database.
        sqlite3_finalize(selectStatement);
        sqlite3_close(database);

        
    }
    else
    {
        
        Type = NSLocalizedString(Type,nil);

        //@"Quantity",@"Duration",@"Instructions"
        
        NSArray *itemsnamen =[NSArray arrayWithObjects:@"Drug_type",@"Dose_unit",@"Strength_unit",@"Dose",@"Strength",@"Location",@"Format_dose",@"Format_Strength",@"Remaining_Qty_unit",@"Refill_Qty_unit",@"Refills_Remaining_unit",@"Format_Rem_Qty",@"Format_Refill_Qty",@"Format_Refills_Rem",@"Dose_name",@"Strength_name",@"Location_name",@"Locations",@"Remaining_name",@"Refill_name",@"Drug_types",@"Drug_less",@"Minimume_name",nil];
        
        
        
        
        sqlite3 *database = [AppDelegate getNewDBconnection];
        
        sqlite3_stmt *selectStatement;
        
        
        
        
        NSString* searchQuery =@"SELECT Drug_type,Dose_unit,Strength_unit,Dose,Strength,Location,Format_dose,Format_Strength,Remaining_Qty_unit,Refill_Qty_unit,Refills_Remaining_unit,Format_Rem_Qty,Format_Refill_Qty,Format_Refills_Rem,Dose_name,Strength_name,Location_name,Locations,Remaining_name,Refill_name,Drug_types,Drug_less,Minimume_name from Contents WHERE (Drug_type) = (?)";
        const char *sql =   [searchQuery UTF8String];
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
        {
            
            sqlite3_bind_text(selectStatement, 1, [@"Other" UTF8String], -1, SQLITE_TRANSIENT);
                        
            while(sqlite3_step(selectStatement) == SQLITE_ROW)
            {
                
                NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
                
                
               for (NSInteger i =0; i < [itemsnamen count]; i++) {
                    
                    
                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
                        
                    {
                        
                        NSString *thestring = NSLocalizedString([NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)],nil);
                        [item  setObject:thestring forKey:[itemsnamen objectAtIndex:i]];
                        
                    }
                    
                    else
                    {
                        
                        
                        [item  setObject:NSLocalizedString([NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)],nil) forKey:[itemsnamen objectAtIndex:i]];
                        
                    }
                    
                }
                
              
               //NSLog(@"%@", item);
                
                [Take addObject:item];
                
                
                
            }
            
        }
        
        //05-09-2017 add new line for close database.
        sqlite3_finalize(selectStatement);
        sqlite3_close(database);

        
    }


  //////NSLog(@"%@", Take);

    return Take;
    
    
    
    
}


+(NSMutableDictionary*) getNotifictionwithID:(NSString*)ID


{
    NSArray *itemsnamen =[NSArray arrayWithObjects:@"ID",@"CurrentDate",@"CurrentDay",@"CurrentTime",@"Day",@"Diverence",@"ID_Timer",@"Moment",@"Time",@"choice",@"intervalDays",@"intervalOurs",@"ID_Medicine", nil];
    
    
    NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    NSString* searchQuery =@"SELECT ID,CurrentDate,CurrentDay,CurrentTime,Day,Diverence,ID_Timer,Moment,Time,choice,intervalDays,intervalOurs,ID_Medicine from Notifiction WHERE (ID_Medicine) = (?)";
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(selectStatement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            
            
           for (NSInteger i =0; i < [itemsnamen count]; i++) {
                
                
                
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[itemsnamen objectAtIndex:i]];
                
                
                
            }
            
            
            
            
            
        }
        
    }
   
    
    
    sqlite3_finalize(selectStatement);
	sqlite3_close(database);
    
    return item;
    
}


+(NSMutableDictionary*) getAktiveMeds:(NSString*)ID


{
    NSArray *names =[NSArray arrayWithObjects:@"Name",@"Firetime",@"Date",@"Time",@"ID_Medicine",nil];
    
    NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    NSString* searchQuery =@"SELECT Name,Firetime,Date,Time,ID_Medicine from AtiveMeds WHERE (ID_Medicine) = (?)";
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(selectStatement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            
            
           for (NSInteger i =0; i < [names count]; i++) {
                
                
                
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[names objectAtIndex:i]];
                
                
                
            }
            
            
            
            
            
        }
        
    }
    
    sqlite3_finalize(selectStatement);
	sqlite3_close(database);
    
    return item;
    
}


+(NSMutableDictionary*) getMedicinewithIDCopy:(NSString*)ID


{
    
    
    
    
    NSArray *itemsnamen =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",@"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit",@"Location",@"active", nil];
    
    
    NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    NSString* searchQuery =@"SELECT ID_Medicine,Name,Strength,Type,Profile,Schedule,Frequency,Remaining,Filling,Refilling,Doctor,Prescription,Notes,Image,ID,ColorRed,ColorGreen,Colorblue,Coloralpha,Audio,Quantity,Duration,Instructions,PerscriptionImage, Starting, Recording, Frequency_text, Times, Dates, Message, Day_number,Phone_Doctor,Phone_Pharmacy,eMail_Doctor,eMail_Pharmacy,Pharmacy, Dose_unit, Strength_unit, Location, active from Medicine_copy WHERE (ID_Medicine) = (?)";
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(selectStatement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            
            
           for (NSInteger i =0; i < [itemsnamen count]; i++) {
                
                
                [item  setObject:NSLocalizedString([NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)],nil) forKey:[itemsnamen objectAtIndex:i]];
                
                
                
                
            }
            
            
            
        }
        
    }
    
    
    sqlite3_finalize(selectStatement);
	sqlite3_close(database);
    return item;
    
}



+(NSMutableArray*) getMedicinewithProfileID:(NSString*)ID


{
    
    
    
    
    NSArray *itemsnamen =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",@"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit",@"Location",@"active", nil];
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    NSString* searchQuery =@"SELECT ID,Name,Strength,Type,Profile,Schedule,Frequency,Remaining,Filling,Refilling,Doctor,Prescription,Notes,Image,ID_Medicine,ColorRed,ColorGreen,Colorblue,Coloralpha,Audio,Quantity,Duration,Instructions,PerscriptionImage, Starting, Recording, Frequency_text, Times, Dates, Message, Day_number,Phone_Doctor,Phone_Pharmacy,eMail_Doctor,eMail_Pharmacy,Pharmacy,Location, active from Medicine  WHERE (ID) = (?)";
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(selectStatement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            
           for (NSInteger i =0; i < [itemsnamen count]; i++) {
                
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[itemsnamen objectAtIndex:i]];
                
                
                
                
            }
            
            
            [Take addObject:item];
            
            
            
        }
        
    }
    
    
    sqlite3_finalize(selectStatement);
	sqlite3_close(database);
    return Take;
    
}





+(NSMutableArray*) getMedicine


{
    
    
 
    
    NSArray *itemsnamen =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",nil];
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    //openen de database
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    //maken een statement
    
    sqlite3_stmt *selectStatement;
    
    
    
    NSString* searchQuery =@"SELECT * from Medicine";
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_int(selectStatement, 1, 0);
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
           for (NSInteger i =0; i < [itemsnamen count]; i++) {
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[itemsnamen objectAtIndex:i]];
                
            }
            
            
            [Take addObject:item];
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.dose_unit = NSLocalizedString([item valueForKey:@"Dose_unit"],nil);
            appDelegate.strength_unit = NSLocalizedString([item valueForKey:@"Strength_unit"],nil);
            
        }
        
    }
    
 

    
    sqlite3_finalize(selectStatement);
	sqlite3_close(database);
    return Take;
}




+(NSMutableArray*) getMonth:(NSArray*)itemsnamen


{
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    //openen de database
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    //maken een statement
    
    sqlite3_stmt *selectStatement;
    
    
    
    NSString* searchQuery =@"SELECT Month,Number from Months";
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_int(selectStatement, 1, 0);
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
           for (NSInteger i =0; i < [itemsnamen count]; i++) {
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[itemsnamen objectAtIndex:i]];
                
                
            }
            
            
            [Take addObject:item];
            
        }
        
    }
    
    sqlite3_finalize(selectStatement);
	sqlite3_close(database);
    
    return Take;
}

+(NSMutableArray*) getContents:(NSArray*)itemsnamen


{
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    //openen de database
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    //maken een statement
    
    sqlite3_stmt *selectStatement;
    
    
    
    NSString* searchQuery =@"SELECT Content,Number from Contents";
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_int(selectStatement, 1, 0);
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
           for (NSInteger i =0; i < [itemsnamen count]; i++) {
                
                [item  setObject:NSLocalizedString([NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)],nil) forKey:[itemsnamen objectAtIndex:i]];
                
                
            }
            
            
            [Take addObject:item];
            
        }
        
    }
    
    
    sqlite3_finalize(selectStatement);
	sqlite3_close(database);
    return Take;
}



+(NSMutableArray*) getHistory:(NSArray*)itemsnamen OnID:(NSString*)ID


{
    
  
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    //ORDER BY value DESC
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    //order by Name AND Date AND Time_Action_24 desc
    NSString* searchQuery =@"SELECT ID,Date,Time,Name,Dose,Taken,ID_medicine,Count,Time_Action,Action,Quantity,Type,Time_Action_24,Original_Time,Action_Date from History WHERE (ID) = (?)";
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(selectStatement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            
           for (NSInteger i =0; i < [itemsnamen count]; i++) {
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[itemsnamen objectAtIndex:i]];
                
                
            }
            
            AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            
            NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
            [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [dateFormatLevel setDateFormat:[myApp convertString:@"dd/MM/yyyy hh:mm a"]];
            
            
            [item setObject:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]] forKey:@"Date_time"];
            
            
            
            
            
            if ([[item valueForKey:@"Action_Date"] length]>1) {
                
                [item setObject:[dateFormatLevel dateFromString:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]]] forKey:@"Action_Date"];
            }
            else
            {
                
                NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
                [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateFormatLevel setDateFormat:[myApp convertString:@"dd/MM/yyyy hh:mm a"]];
                
                
                [item setObject:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]] forKey:@"Action_Date"];
                
                
            }
            
            [Take addObject:item];
          
            
        }
        
    }
    
    
    sqlite3_finalize(selectStatement);
    sqlite3_close(database);
    
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date_time" ascending:TRUE];
    [Take sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    
    
    
    return Take;
    
}





+(NSMutableArray*) getHistory:(NSArray*)itemsnamen OnMedID:(NSString*)ID


{
    
    
    NSArray *names =[NSArray arrayWithObjects:@"ID",@"Date",@"Time",@"Name",@"Dose",@"Taken",@"ID_medicine",@"Count",@"Time_Action",@"Action",@"Quantity",@"Type",@"Time_Action_24",@"Original_Time",@"Action_Date",nil];
    
    
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    
    //Time_Action_24 DESC
    
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    NSString* searchQuery =@"SELECT ID,Date,Time,Name,Dose,Taken,ID_medicine,Count,Time_Action,Action,Quantity,Type,Time_Action_24,Original_Time,Action_Date from History WHERE (ID_medicine) = (?)";
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(selectStatement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            
           for (NSInteger i =0; i < [names count]; i++) {
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[names objectAtIndex:i]];
                
                
            }
            AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            
            
            
            NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
            [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [dateFormatLevel setDateFormat:[myApp convertString:@"dd/MM/yyyy hh:mm a"]];
            
            
            [item setObject:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]] forKey:@"Date_time"];
            
            
            
            
            if ([[item valueForKey:@"Action_Date"]length]>0) {
                
                [item setObject:[dateFormatLevel dateFromString:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]]] forKey:@"Action_Date"];
            }
            else
            {
                
                NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
                [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateFormatLevel setDateFormat:[myApp convertString:@"dd/MM/yyyy hh:mm a"]];
                
                
                [item setObject:[NSString stringWithFormat:@"%@ %@", [item valueForKey:@"Date"], [item valueForKey:@"Time_Action_24"]] forKey:@"Action_Date"];
              
                
            }
            
              [Take addObject:item];
            
            
        }
        
    }
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date_time" ascending:FALSE];
    [Take sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    
    sqlite3_finalize(selectStatement);
	sqlite3_close(database);
    
    
  
    
    return Take;
    
}




+(NSArray*)getInstellingen;

{
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    //openen de database
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    //maken een statement
    
    sqlite3_stmt *selectStatement;
    
    
    NSString* searchQuery =@"SELECT * from Instelling";
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_int(selectStatement, 1, 0);
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            
            [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, 0)] forKey:@"Soort"];
            
            
            [Take addObject:item];
            
        }
        
    }
    
    
    sqlite3_finalize(selectStatement);
	sqlite3_close(database);
    return Take;
}






+(NSString *) change: (NSString *) dumb
{
    if ([dumb length] ==0) {
        
        dumb=@"-";
        
        return dumb;
        
    }
    
    else
    {
        
        return dumb;
        
    }
    
   
}




@end
