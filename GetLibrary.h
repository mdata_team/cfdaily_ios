//
//  GetLibrary.h
//  Tell
//
//  Created by Jeffrey Snijder on 28-01-13.
//  Copyright (c) 2013 Livecast. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "NewProfileController.h"
@interface GetLibrary : NSObject


+(void)loadPhotosdelegate:(NewProfileController*)delegate;
+(void)loadPhotos:(NewProfileController*)parant;
+(void)addThumb:(ALAsset *)asset;;
@end
