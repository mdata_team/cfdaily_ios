//
//  GetLibrary.m
//  Tell
//
//  Created by Jeffrey Snijder on 28-01-13.
//  Copyright (c) 2013 Livecast. All rights reserved.
//

#import "GetLibrary.h"
#import "AppDelegate.h"
#import "NewProfileController.h"

@implementation GetLibrary


+(void)loadPhotosdelegate:(NewProfileController*)delegate;
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


        // Enumerate just the photos and videos group by using ALAssetsGroupSavedPhotos.
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone|| UIUserInterfaceIdiomPad)
        {
 [appDelegate.library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop)
         {

             

             // Within the group enumeration block, filter if necessary
         [group setAssetsFilter:[ALAssetsFilter allPhotos]];
         [group enumerateAssetsUsingBlock:^(ALAsset *alAsset, NSUInteger index, BOOL *innerStop)
          {
                         // The end of the enumeration is signaled by asset == nil.
          if (alAsset)
              {
            
              [self addThumb: alAsset];



              }
          else
              {

              [appDelegate finischedit];


              }
          }];
         }
                                         failureBlock: ^(NSError *error) {
                                             
                                         }];
        }
    
    
    
}


+(void)loadPhotos:(NewProfileController*)parant
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


        // Enumerate just the photos and videos group by using ALAssetsGroupSavedPhotos.
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone|| UIUserInterfaceIdiomPad)
        {

        [appDelegate.library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop)
         {

             

             // Within the group enumeration block, filter if necessary
         [group setAssetsFilter:[ALAssetsFilter allPhotos]];
         [group enumerateAssetsUsingBlock:^(ALAsset *alAsset, NSUInteger index, BOOL *innerStop)
          {
            
              // The end of the enumeration is signaled by asset == nil.
          if (alAsset)
              {
                 

              [self addThumb: alAsset];



              }
          else
              {

              [appDelegate finisched:parant];


              }
          }];
         }
                             failureBlock: ^(NSError *error) {
                                     
                             }];
        }
    
    
    
}

+(void)addThumb:(ALAsset *)asset
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    NSMutableDictionary *workingDictionary = [[NSMutableDictionary alloc] init];

    [workingDictionary setObject:[UIImage imageWithCGImage:[asset thumbnail]]  forKey:@"Thumb"];
    [workingDictionary setObject:[asset valueForProperty:ALAssetPropertyURLs] forKey:@"Location"];
    [appDelegate.photos addObject:workingDictionary];


}






@end
