//
//  MedSyncManager.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//


/*
 
 
 <br />
 <b>Fatal error</b>:  Uncaught exception 'PDOException' with message 'SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens' in /home/content/80/5381280/html/cfmedcaretest.fr/json/loginIpad.php:8
 Stack trace:
 #0 /home/content/80/5381280/html/cfmedcaretest.fr/json/loginIpad.php(8): PDOStatement-&gt;execute(Array)
 #1 {main}
 thrown in <b>/home/content/80/5381280/html/cfmedcaretest.fr/json/loginIpad.php</b> on line <b>8</b><br />
 
 
 Hello Jeffrey,
 
 As we discussed yesterday, I have created a db for the Ipad login of the French version.
 
 I am going to send you the data with json.
 
 From the Ipad, you can send me the pass variable (i have called it "passe"). From my side,
 i will check it in the database and I’ll return to you the following variables:
 1. success: "true" or "false"
 2. pseudo: the pseudo, if it exists, of the user.
 
 The url you will use is: http://www.mdata.gr/cfmedcaretest.fr/json/loginIpad.php
 
 I am using post requests. If you need to do it with get, please let me know.
 
 For the tests, I have created the following record in the db:
 
 pseudo: Jeffrey Snijder
 passe: jeffrey
 
 Please, let me know when you test it to continue with the Forgot Password check.
 
 Best Regards,
 
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:ValidTicket>
 <!--Optional:-->
 <tem:Ticket>?</tem:Ticket>
 </tem:ValidTicket>
 </soapenv:Body>
 </soapenv:Envelope>
 
 
 
 
 Setcontent = [[NSMutableArray alloc] init];
 citys = [[NSMutableArray alloc] init];
 setName=@"ValidTicketResponse";
 
 setNames = [[NSMutableArray alloc] initWithObjects:@"ValidTicketResult", nil];
 
 
 //http://mdmedcare.com/dosingws/dosingws.svc?wsdl
 
 NSString *soapMessage = [NSString stringWithFormat:
 
 @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
 "<soapenv:Header/>"
 "<soapenv:Body>"
 "<tem:ValidTicket>"
 "<tem:Ticket>%@</tem:Ticket>"
 "</tem:ValidTicket>"
 "</soapenv:Body>"
 "</soapenv:Envelope>", [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"]];
 
 //To suppress the leak in NSXMLParser
 NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
 NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
 NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
 
 [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
 [theRequest addValue: @"http://tempuri.org/IDosingWS/ValidTicket" forHTTPHeaderField:@"SOAPAction"];
 [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
 [theRequest setHTTPMethod:@"POST"];
 [theRequest setHTTPBody:
 [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
 
 
 theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
 webData = [[NSMutableData data] retain];
 
 
 
 
 
 
 Nice!
 
 Ok, now for “forgot password”, the process will be:
 
 Step1: The user enters his e-mail address to the ipad.
 Step2: You sent me his e-mail address through the below link:
 www.mdata.gr/cfmedcaretest.fr/json/ForgotIpad.php?email=kkalpakloglou@mdata.gr
 
 Instead of my e-mail, you should enter the user’s email.
 
 kkalpakloglou@mdata.gr will now give success “true” for testing purposes. Any other value
 will give success “false”.
 
 Step3: If you receive success “true”, then you display a message “an e-mail is sent to your
 e-mail address”. If you receive success “false”, then you display a message “no user is found
 with that e-mail address”. (The exact messages will be sent to you by Mina).
 
 As we said on skype, the e-mail to the user with the password will be sent from us, so you don’t have
 to do anything.
 
 Please test it and let me know if it works.
 
 
 */



#import "GetpaswordSyncManager.h"
#import "AppDelegate.h"
//#import "JSONKit.h"//sn
//#import "SBJson.h"//sn

@implementation GetpaswordSyncManager
@synthesize Setcontent;
@synthesize setNames;
@synthesize citys;
@synthesize Result;
@synthesize theConnection;
@synthesize currentElement;
@synthesize item;
@synthesize count;
@synthesize setName;
@synthesize term;
@synthesize webData;
@synthesize xmlParser;
@synthesize Parantit;


-(GetpaswordSyncManager*)init{
	self = [super init];
	if(self){
        
    }
    return self;
}

-(void)GetID:(NSString*)pasword setparent:(UIViewController*)parent
{
    Parantit =parent;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.cfmedcaretest.fr/json/loginIpad.php?passe=%@", pasword]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    webData = [[NSMutableData data] retain];
}


-(void)GetIDask:(NSString*)mail setparent:(UIViewController*)parent
{
        Parantit =parent;
    
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.cfmedcaretest.fr/json/ForgotIpad.php?email=%@", mail]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
        
        theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        webData = [[NSMutableData data] retain];
}



-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [webData setLength: 0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[webData appendData:data];
    
    unsigned char byteBuffer[[webData length]];
    [webData getBytes:byteBuffer];
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSURL *myURL = [[connection currentRequest] URL];
    
    //////////////NSLog(@"%@", myURL);
    
    NSString *responseString = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
    
    NSString *myString = [myURL absoluteString];
    
    if ([myString rangeOfString:@"ForgotIpad"].location==NSNotFound)
        
    {
        NSDictionary *set = [responseString objectFromJSONString];
        
        //////////////NSLog(@"%@", [set valueForKey:@"success"]);
        if ([[set  valueForKey:@"success"] rangeOfString:@"true"].location==NSNotFound)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"Not Argree" forKey:@"Loggedin"];
            
            //26-06-2017 change
//            UIAlertView *alertit = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Password not registered!",nil)
//                                                             delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
//            [alertit show];
            
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Password not registered!",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alert animated:YES completion:nil];
                
            });
        }
        else
            
            
        {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"Argree" forKey:@"Loggedin"];
            
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            
            [appDelegate.toolbarDown setAlpha:1];

            [Parantit dismissViewControllerAnimated:YES completion:NULL];
            
            
            
            
        }
        
        
    }
    
    else
    {
        NSDictionary *set = [responseString objectFromJSONString];
        
        //////////////NSLog(@"%@", [set valueForKey:@"success"]);
        
        
        
        if ([[set  valueForKey:@"success"] rangeOfString:@"true"].location==NSNotFound)
            
        {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"Not Argree" forKey:@"Loggedin"];
            
            //26-06-2017 change
//            UIAlertView *alertit = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil)
//                                                              message:NSLocalizedString(@"E-mail not registered!",nil)
//                                                             delegate:nil
//                                                    cancelButtonTitle:NSLocalizedString(@"OK",nil)
//                                                    otherButtonTitles:nil];
//            [alertit show];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"E-mail not registered!",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alert animated:Yellow completion:nil];
                
            });
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"Not Argree" forKey:@"Loggedin"];

            //26-06-2017 change
//            UIAlertView *alertit = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil)
//                                                              message:NSLocalizedString(@"Veuillez vérifier votre messagerie, vous allez recevoir un message contenant votre mot de passe.",nil)
//                                                             delegate:nil
//                                                    cancelButtonTitle:NSLocalizedString(@"OK",nil)
//                                                    otherButtonTitles:nil];
//            [alertit show];
//            
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Veuillez vérifier votre messagerie, vous allez recevoir un message contenant votre mot de passe.",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alert animated:Yellow completion:nil];
                
            });

        }
    }
}
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
}

-(void) parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
{
    NSString *temp = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];
    
   for (NSInteger i =0; i < [setNames count]; i++) {
        
        if ([currentElement isEqualToString:[setNames objectAtIndex:i]]) {
            
            [[Setcontent objectAtIndex:i] appendString:temp];
        }
    }
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    currentElement = [elementName copy];
    if ([elementName isEqualToString:setName]) {
        
        item = [[NSMutableDictionary alloc] init];
        [Setcontent removeAllObjects];
        
       for (NSInteger i =0; i < [setNames count]; i++) {
            
            NSMutableString *Setit= [[NSMutableString alloc] init];
            [Setcontent addObject:Setit];
            
        }
        
    }
    
    
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
	
    
    
    if ([elementName isEqualToString:setName]) {
        
        
       for (NSInteger i =0; i < [setNames count]; i++) {
            
            [item setObject:[Setcontent objectAtIndex:i] forKey:[setNames objectAtIndex:i]];
            
        }
        
        [citys addObject:item];
        
        
        //////////////////NSLog(@"%@", citys);
        
        
        
        
        [item release];
		
    }
	
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    
    
    string = [string stringByReplacingOccurrencesOfString: @"\"" withString:@""];
    string = [string stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    string = [string stringByReplacingOccurrencesOfString: @"\t" withString:@""];
    
    
    
   for (NSInteger i =0; i < [setNames count]; i++) {
        
        if ([currentElement isEqualToString:[setNames objectAtIndex:i]]) {
            
            [[Setcontent objectAtIndex:i] appendString:string];
            
            
        }
        
        
        
        
    }
    
}

-(void)parser:(NSXMLParser*)myParser {
    
    NSAutoreleasePool *schedulePool = [[NSAutoreleasePool alloc] init];
    
    BOOL success = [myParser parse];
    
    if(success) {
        
        
    } else {
        
        
        
    }
    
    [schedulePool drain];
    
    [myParser release];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    
    
    if ([citys count] >0) {
        
        if ([[[citys valueForKey:@"ValidTicketResult"]objectAtIndex:0] isEqualToString:@"Success"]) {
            
        }
        else
            
            
        {
            [delegate getticket];
        }
    }
    
    
    
    
}




-(NSString *) change: (NSString *) dumb
{
    if ([dumb isEqualToString:@""]) {
        
        dumb=@"-";
        
        return dumb;
        
    }
    
    else
    {
        
        return dumb;
        
    }
    
}





@end
