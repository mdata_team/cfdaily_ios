//
//  GlobalClass.h
//  TimoView
//
//  Created by TechnoMac-13 on 07/06/17.
//  Copyright © 2017 TechnoMac-13. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface GlobalClass : NSObject

//Alertview
+(void)ShowAlertwithTitle:(NSString *)strTitle Message:(NSString *)strMessage okLabelTitle:(NSString* )okLabel;

@end
