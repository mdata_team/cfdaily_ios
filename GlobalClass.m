//  GlobalClass.m
//  TimoView
//  Created by TechnoMac-13 on 07/06/17.
//  Copyright © 2017 TechnoMac-13. All rights reserved.

#import "GlobalClass.h"
#import "AppDelegate.h"

@implementation GlobalClass

#pragma mark - ALERT

+(void)ShowAlertwithTitle:(NSString *)strTitle Message:(NSString *)strMessage okLabelTitle:(NSString* )okLabel{

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:strTitle message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertOK = [UIAlertAction actionWithTitle:okLabel style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

    }];
    [alert addAction:alertOK];
    AppDelegate *appDel= (AppDelegate *)[[UIApplication sharedApplication] delegate];
    dispatch_async(dispatch_get_main_queue(), ^{

        [[appDel window].rootViewController presentViewController:alert animated:YES completion:nil];

    });

}

@end
