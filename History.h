//
//  Course.h
//  CDCourses
//
//  Created by Jeffrey Snijder on 26-07-13.
//  Copyright (c) 2013 Jeffrey Snijder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface History : NSManagedObject


@property (nonatomic, retain) NSString * action;
@property (nonatomic, retain) NSDate * actiondate;
@property (nonatomic, retain) NSString * count;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * dose;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * idmedicine;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * originaltime;
@property (nonatomic, retain) NSString * quantity;
@property (nonatomic, retain) NSString * taken;
@property (nonatomic, retain) NSDate * time;
@property (nonatomic, retain) NSDate * timeacaction24;
@property (nonatomic, retain) NSDate * timeaction;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * know;
@property (nonatomic, retain) NSString * send;
@property (nonatomic, retain) NSString *sendid;

@end
