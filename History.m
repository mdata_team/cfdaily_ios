//
//  Course.m
//  CDCourses
//
//  Created by Jeffrey Snijder on 26-07-13.
//  Copyright (c) 2013 Jeffrey Snijder. All rights reserved.
//

#import "History.h"


@implementation History


@dynamic action;
@dynamic actiondate;
@dynamic count;
@dynamic date;
@dynamic dose;
@dynamic id;
@dynamic idmedicine;
@dynamic name;
@dynamic originaltime;
@dynamic quantity;
@dynamic taken;
@dynamic time;
@dynamic timeacaction24;
@dynamic timeaction;
@dynamic type;
@dynamic know;
@dynamic send;
@dynamic sendid;


-(void)awakeFromInsert
{
    
    [super awakeFromInsert];

    
    
}
@end
