//
//  HistoryCell.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "History.h"
#import "MomentButton.h"

@class HistoryViewController;
@interface HistoryCell : UITableViewCell 
@property (nonatomic, retain) IBOutlet  HistoryViewController *lostView;
@property (nonatomic, retain) IBOutlet UILabel *Date;
@property (nonatomic, retain) IBOutlet UILabel *DateColor;
@property (nonatomic, retain) IBOutlet UITextView *Time;
@property (nonatomic, retain) IBOutlet UITextView *Name;
@property (nonatomic, retain) IBOutlet UITextView *Taken;
@property (nonatomic, retain) NSString *quantityString;
@property (nonatomic, retain) NSString *remainingString;
@property (nonatomic, retain) NSString *strengthString;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, retain) MomentButton *Moment;
-(void)getparent:(HistoryViewController*)set;
-(void) FillAllItems:(History*) sender;

@end
