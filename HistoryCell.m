//
//  HistoryCell.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "HistoryCell.h"
#import "HistoryViewController.h"
#import "GetData.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@implementation HistoryCell
@synthesize lostView;
@synthesize Date;
@synthesize DateColor;
@synthesize Time;
@synthesize Name;
@synthesize Taken;
@synthesize Moment;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //Initialization code
        
        UIView *ProfileView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320, 80)];
        
        [ProfileView setBackgroundColor:[UIColor clearColor]];
        
        [self addSubview:ProfileView];
        
        DateColor = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320, 20)];
        [DateColor setBackgroundColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
        DateColor.layer.shadowOffset = CGSizeMake(1, 1);
        DateColor.layer.shadowOpacity = 1;
        DateColor.layer.shadowRadius = 1.0;
        [ProfileView addSubview:DateColor];
        
        Date = [[UILabel alloc] initWithFrame:CGRectMake(10, 0.0f, 300, 20)];
        [Date setTextColor:[UIColor whiteColor]];
        [Date setFont:[UIFont boldSystemFontOfSize:10]];
        [Date setTag:120];
        [Date setBackgroundColor:[UIColor clearColor]];
        
        [ProfileView addSubview:Date];
        
        Time = [[UITextView alloc] initWithFrame:CGRectMake(10, 20, 80, 80)];
        [Time setTextColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
        [Time setFont:[UIFont systemFontOfSize:10]];
        [Time setTag:120];
        [Time setScrollEnabled:NO];
        [Time setBackgroundColor:[UIColor clearColor]];
        [Time setEditable:NO];
        
        [ProfileView addSubview:Time];
        
        Name = [[UITextView alloc] initWithFrame:CGRectMake(90, 20, 120, 80)];
        [Name setTextColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
        [Name setFont:[UIFont systemFontOfSize:10]];
        [Name setTag:120];
        [Name setScrollEnabled:NO];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setEditable:NO];
        [ProfileView addSubview:Name];
        
        Taken = [[UITextView alloc] initWithFrame:CGRectMake(210, 20, 110, 80)];
        [Taken setTextColor:[UIColor blackColor]];
        [Taken setFont:[UIFont boldSystemFontOfSize:10]];
        [Taken setTag:120];
        [Taken setScrollEnabled:NO];
        [Taken setBackgroundColor:[UIColor clearColor]];
        [Taken setEditable:NO];
        
        [ProfileView addSubview:Taken];
        
        Moment = [[MomentButton alloc] initWithFrame:CGRectMake(300, 25, 15, 15)];
        [Moment setTitle:@"!" forState:UIControlStateNormal];
        [Moment.titleLabel setFont:[UIFont boldSystemFontOfSize:10]];
        [Moment.layer setCornerRadius:7.5];
        
//        [Moment setBackgroundColor:[UIColor colorWithRed:0.090 green:0.071 blue:0.541 alpha:1.000]]; //05-07-2017 change
        [Moment setBackgroundColor:[UIColor colorWithRed:0.388 green:0.482 blue:0.718 alpha:1.000]];
        [ProfileView addSubview:Moment];
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft)
        {
            ProfileView.transform = CGAffineTransformMakeScale(-1, 1);
            
            for (UIView *content in ProfileView.subviews) {
                
                for (UILabel *label in content.subviews) {
                    
                    if ([label isKindOfClass:[UILabel class]]) {
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentNatural];
                    }
                }
                for (UITextView *label in content.subviews)
                {
                    if ([label isKindOfClass:[UITextView class]]) {
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentNatural];
                    }
                }
                for (UIButton *label in content.subviews) {
                    if ([label isKindOfClass:[UIButton class]]) {
                        
                        label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                        [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                    }
                }
            }
        }
        else
        {
            
        }
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    //Configure the view for the selected state
}

-(void)action:(MomentButton*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [format setDateFormat:[appDelegate convertString:@"hh:mm a"]];
/* //27-06-2017 change
    UIAlertView *alertit = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:[NSString stringWithFormat:
                                                                                    NSLocalizedString(@"This medicine has been postoned from %@ to %@!",nil), [format stringFromDate: sender.currenCourse.originaltime],sender.currenCourse.know]
                                                     delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
    [alertit show];
    */
    
    
    UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:[NSString stringWithFormat:
                                                                                                                                   NSLocalizedString(@"This medicine has been postoned from %@ to %@!",nil), [format stringFromDate: sender.currenCourse.originaltime],sender.currenCourse.know] preferredStyle:UIAlertControllerStyleAlert];
    [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
        
    });
    
}
-(void) FillAllItems:(History*) sender;
{
    //16-11-2017
    
//    NSLog(@"array_AllMedicine :: %@",[SHARED_APPDELEGATE array_AllMedicine]);
        
    for (int i=0; i<[SHARED_APPDELEGATE array_AllMedicine].count; i++)
    {
        Medicine *cours1 = [[SHARED_APPDELEGATE array_AllMedicine] objectAtIndex:i];
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        
        if ([cours1.name isEqualToString:sender.name])
        {
            [dic setValue:cours1.type forKey:@"med_type"];
            [dic setValue:cours1.quantity forKey:@"med_count"];
            [dic setValue:[NSString stringWithFormat:@"(%@ %@)",cours1.strength,cours1.strength_unit] forKey:@"dose"];
            [SHARED_APPDELEGATE setDicMedicine:dic];
        }
    }
    
//    NSLog(@"Temp : %@",[SHARED_APPDELEGATE dicMedicine]);
    
    
    [Moment setAlpha:0];
    [Moment addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchUpInside];
    Moment.currenCourse =sender;
    
    if ([[GetData getDrugTypeStrength:sender.type] count] != 0) // add condition for handle crash  04-09-2017
    {
        
        NSArray *typit = [[GetData getDrugTypeStrength:sender.type] objectAtIndex:0];
        
        ////////////////////////////////////////////////////////////////////////NSLog(@"%@", sender);
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        NSDateFormatter *dateFormatLevelConvert = [[NSDateFormatter alloc] init];
        [dateFormatLevelConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevelConvert setDateFormat:[appDelegate convertString:@"dd/MM/yyyy\nhh:mm a"]];
        
        NSDateFormatter *formatstring = [[NSDateFormatter alloc] init];
        [formatstring setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [formatstring setDateFormat:@"dd/MM/yyyy"];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [format setDateFormat:@"dd MMMM yyyy"];
        
        [Date setText:[NSString stringWithFormat:@"%@\n",[format stringFromDate:sender.date]]];
        [Time setText:[NSString stringWithFormat:@"%@\n%@",NSLocalizedString(@"Set at:",nil),[dateFormatLevel stringFromDate:sender.originaltime]]];
        
//        if ([sender.taken isEqualToString:@"Taken#"])
//        {
//            //***************** 02-09-2017 change range color TAKE LATER ********************
//            
//            NSArray *ary = [Time.text componentsSeparatedByString:@" "];
//            NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:Time.text];
//            for (NSString *word in ary)
//            {
//                if ([word isEqualToString:[ary lastObject]])
//                {
//                    NSRange range = [Time.text rangeOfString:[ary lastObject]];
//                    
//                    [string addAttribute:NSForegroundColorAttributeName
//                                   value:[UIColor colorWithRed:0.95 green:0.251 blue:0.133 alpha:1.000]
//                                   range:NSMakeRange(range.location, 5)];
//                }
//                else
//                {
//                    NSRange range1 = [Time.text rangeOfString:[ary lastObject]];
//                    
//                    NSRange range = [Time.text rangeOfString:Time.text];
//                    [string addAttribute:NSForegroundColorAttributeName
//                                   value:[UIColor colorWithRed:0.180 green:0.541 blue:0.6 alpha:1.000]
//                                   range:NSMakeRange(range.location, range1.location)];
//                }
//            }
//            
//            
//            
//            [Time setAttributedText: string];
//            [Time setFont:[UIFont systemFontOfSize:10]];
//            [Time setTag:120];
//            [Time setScrollEnabled:NO];
//            
//            NSLog(@"str : %@",string);
//            
//            //***************** 02-09-2017 change range color TAKE LATER ********************
//            
//        }
        
        
        
        
        if ([sender.taken isEqualToString:@"Taken earlier"]) {
            
            
            [Taken setTextColor:[UIColor colorWithRed:0.000 green:0.502 blue:0.502 alpha:1.000]];
            //        [DateColor setBackgroundColor:[UIColor colorWithRed:0.000 green:0.502 blue:0.502 alpha:1.000]]; //01-07-2017 changes
            //(29, 147, 141)
            [DateColor setBackgroundColor:[UIColor colorWithRed:0.114 green:0.576 blue:0.553 alpha:1.000]];
            
            [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString(sender.taken,nil), NSLocalizedString(@"at",nil) ,[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
            
        }
        else if ([sender.taken isEqualToString:@"Taken late"]) {
            
//            [Taken setTextColor:[UIColor colorWithRed:0.949 green:0.208 blue:0.102 alpha:1.000]]; //07-09-2017
            [Taken setTextColor:[UIColor colorWithRed:0.95 green:0.251 blue:0.133 alpha:1.000]];

//            [DateColor setBackgroundColor:[UIColor colorWithRed:0.949 green:0.208 blue:0.102 alpha:1.000]]; //07-09-2017
            [DateColor setBackgroundColor:[UIColor colorWithRed:0.95 green:0.251 blue:0.133 alpha:1.000]];

            [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString(sender.taken,nil),NSLocalizedString(@"at",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
            
            
        }
        else if ([sender.taken isEqualToString:@"Missed"]) {
            [Taken setTextColor:[UIColor colorWithRed:0.000 green:0.502 blue:0.502 alpha:1.000]];
            //        [DateColor setBackgroundColor:[UIColor colorWithRed:0.949 green:0.208 blue:0.102 alpha:1.000]];
            [DateColor setBackgroundColor:[UIColor yellowColor]];
            [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString(sender.taken,nil),NSLocalizedString(@"at",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
            
        }
        
        else if ([sender.taken isEqualToString:@"Taken"]) {
            
            //        [Taken setTextColor:[UIColor colorWithRed:0.067 green:0.467 blue:0.082 alpha:1.000]]; //02-09-2017
            [Taken setTextColor:[UIColor colorWithRed:0.180 green:0.541 blue:0.6 alpha:1.000]];
            //        [DateColor setBackgroundColor:[UIColor colorWithRed:0.067 green:0.467 blue:0.082 alpha:1.000]]; //01-07-2017 changes
            //(119, 197, 152)
            [DateColor setBackgroundColor:[UIColor colorWithRed:0.467 green:0.772 blue:0.596 alpha:1.000]];
//            [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString(sender.taken,nil),NSLocalizedString(@"at",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]]; //9-11-2017

            [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString(sender.taken,nil),NSLocalizedString(@"",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
            
        }
        else if ([sender.taken isEqualToString:@"Skipped"]) {
            
            
            [Taken setTextColor:[UIColor colorWithRed:1.000 green:0.502 blue:0.000 alpha:1.000]];
            //        [DateColor setBackgroundColor:[UIColor colorWithRed:1.000 green:0.502 blue:0.000 alpha:1.000]]; //01-07-2017 changes
            //(249, 156, 72)
            [DateColor setBackgroundColor:[UIColor colorWithRed:0.976 green:0.612 blue:0.282 alpha:1.000]];
//            [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString(sender.taken,nil),NSLocalizedString(@"at",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]]; //9-11-2017
            
            [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString(sender.taken,nil),NSLocalizedString(@"",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]];

            
        }
        else if ([sender.taken isEqualToString:@"Postponed"]) {
            
            [Taken setTextColor:[UIColor colorWithRed:0.090 green:0.071 blue:0.541 alpha:1.000]];
            //        [DateColor setBackgroundColor:[UIColor colorWithRed:0.090 green:0.071 blue:0.541 alpha:1.000]]; //01-07-2017 chanegs
            //(99, 123, 183)
            [DateColor setBackgroundColor:[UIColor colorWithRed:0.388 green:0.482 blue:0.718 alpha:1.000]];
            
            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
            {
                [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString(sender.taken,nil),NSLocalizedString(@"tot",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
            }
            else
            {
                [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString(sender.taken,nil),NSLocalizedString(@"to",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
            }
            
        }
        else {
            
            [Moment setAlpha:1];
            
            if ([[sender.taken stringByReplacingOccurrencesOfString:@"#" withString:@""] isEqualToString:@"Taken"]) {
                
                [Taken setTextColor:[UIColor colorWithRed:0.067 green:0.467 blue:0.082 alpha:1.000]];
                //            [DateColor setBackgroundColor:[UIColor colorWithRed:0.067 green:0.467 blue:0.082 alpha:1.000]]; //01-07-2017
                [DateColor setBackgroundColor:[UIColor colorWithRed:0.467 green:0.772 blue:0.596 alpha:1.000]];
                [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString([sender.taken stringByReplacingOccurrencesOfString:@"#" withString:@""],nil),NSLocalizedString(@"at",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
                
//                //***************** 02-09-2017 change range color for TAKE LATE ********************
                
//                NSArray *ary = [Taken.text componentsSeparatedByString:@"\n"];
//                NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:Taken.text];
//                for (NSString *word in ary)
//                {
//                    if ([word isEqualToString:[ary lastObject]])
//                    {
//                        NSRange range = [Taken.text rangeOfString:[ary lastObject]];
//                        
//                        [string addAttribute:NSForegroundColorAttributeName
//                                       value:[UIColor colorWithRed:0.95 green:0.251 blue:0.133 alpha:1.000]
//                                       range:NSMakeRange(range.location, 5)];
//                    }
//                    else
//                    {
//                        
//                        NSRange range1 = [Taken.text rangeOfString:[ary lastObject]];
//                        
//                        NSRange range = [Taken.text rangeOfString:Taken.text];
//                        [string addAttribute:NSForegroundColorAttributeName
//                                       value:[UIColor colorWithRed:0.180 green:0.541 blue:0.6 alpha:1.000]
//                                       range:NSMakeRange(range.location, range1.location)];
//                    }
//                }
//                
//                [Taken setAttributedText: string];
//                [Taken setFont:[UIFont boldSystemFontOfSize:10]];
//                [Taken setTag:120];
//                [Taken setScrollEnabled:NO];
//                
//                NSLog(@"str : %@",string);
//                
//                //***************** 02-09-2017 change range color TAKE LATE ********************
            }
            
            if ([[sender.taken stringByReplacingOccurrencesOfString:@"#" withString:@""] isEqualToString:@"Skipped"]) {
                
                [Taken setTextColor:[UIColor colorWithRed:1.000 green:0.502 blue:0.000 alpha:1.000]];
                //            [DateColor setBackgroundColor:[UIColor colorWithRed:1.000 green:0.502 blue:0.000 alpha:1.000]]; //01-07-2017 changes
                //(249, 156, 72)
                [DateColor setBackgroundColor:[UIColor colorWithRed:0.976 green:0.612 blue:0.282 alpha:1.000]];
                
                [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString([sender.taken stringByReplacingOccurrencesOfString:@"#" withString:@""],nil),NSLocalizedString(@"at",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
            }
            
            if ([[sender.taken stringByReplacingOccurrencesOfString:@"#" withString:@""] isEqualToString:@"Taken earlier"]) {
                
                [Taken setTextColor:[UIColor colorWithRed:0.000 green:0.502 blue:0.502 alpha:1.000]];
                //            [DateColor setBackgroundColor:[UIColor colorWithRed:0.000 green:0.502 blue:0.502 alpha:1.000]]; //01-07-2017 chanegs
                [DateColor setBackgroundColor:[UIColor colorWithRed:0.114 green:0.576 blue:0.553 alpha:1.000]];
                
                [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString([sender.taken stringByReplacingOccurrencesOfString:@"#" withString:@""],nil), NSLocalizedString(@"at",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
                
            }
            if ([[sender.taken stringByReplacingOccurrencesOfString:@"#" withString:@""] isEqualToString:@"Taken late"]) {
                
                [Taken setTextColor:[UIColor colorWithRed:0.949 green:0.208 blue:0.102 alpha:1.000]];
//                [DateColor setBackgroundColor:[UIColor colorWithRed:0.949 green:0.208 blue:0.102 alpha:1.000]]; //07-09-2017
//                [DateColor setBackgroundColor:[UIColor colorWithRed:0.95 green:0.251 blue:0.133 alpha:1.000]];
                [DateColor setBackgroundColor:[UIColor colorWithRed:0.95 green:0.251 blue:0.133 alpha:1.000]];
                
                [Taken setText:[NSString stringWithFormat:@"%@ %@\n%@",NSLocalizedString([sender.taken stringByReplacingOccurrencesOfString:@"#" withString:@""],nil),NSLocalizedString(@"at",nil),[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
                
            }
            //[Taken setText:[NSString stringWithFormat:@"%@ at\n%@",[sender.taken stringByReplacingOccurrencesOfString:@"#" withString:@""],[dateFormatLevelConvert stringFromDate:sender.timeaction]]];
        }
        
        if ([sender.type isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
            if (sender.quantity) {
                
                if ([sender.dose isEqualToString:@"piece"]) {
                    
                    if ([sender.quantity intValue] >1) {
                        
                        [Name setText:[NSString stringWithFormat:@"%@\n%@\n%@",sender.name, sender.type, [typit valueForKey:@"Drug_types"]]];
                    }
                    else
                    {
                        [Name setText:[NSString stringWithFormat:@"%@\n%@\n%@",sender.name, sender.type, [typit valueForKey:@"Drug_less"]]];
                    }
                }
                else
                {
                    [Name setText:[NSString stringWithFormat:@"%@\n%@\n%@",sender.name, sender.type, sender.dose]];
                }
            }
            else
            {
                if ([sender.dose isEqualToString:@"piece"]) {
                    
                    if ([sender.quantity intValue] >1) {
                        [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, @"", sender.type,[typit valueForKey:@"Drug_types"]]];
                    }
                    else
                    {
                        [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, @"", sender.type,[typit valueForKey:@"Drug_less"]]];
                    }
                }
                else
                {
                    [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, @"", sender.type,sender.dose]];
                }
            }
        }
        else
        {
            if (sender.quantity) {
                
                if ([sender.dose isEqualToString:@"piece"])
                {
                    
                    if ([sender.quantity intValue] >1)
                    {
                        
                        [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, sender.quantity, sender.type, [typit valueForKey:@"Drug_types"]]];
                    }
                    else
                    {
                        [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, sender.quantity, sender.type, [typit valueForKey:@"Drug_less"]]];
                    }
                }
                else
                {
                    [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, sender.quantity, sender.type, sender.dose]];
                    
                    //16-11
                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                    {
                        if ([[[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_type"]isEqualToString:@"Tablett"])
                        {
                            if ([[[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_count"] intValue] > 1)
                            {
                                [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_count"], sender.type, [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"dose"]]];

                                
                                Name.text = [Name.text stringByReplacingOccurrencesOfString:@"Annet" withString:@"Tabletter"];
                            }
                            else
                            {
                                [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_count"], [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_type"], [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"dose"]]];

                                Name.text = [Name.text stringByReplacingOccurrencesOfString:@"Annet" withString:[[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_type"]];
                            }
                        }
                        
                        else if ([[[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_type"]isEqualToString:@"Spray"])
                        {
                            if ([[[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_count"] intValue] > 1)
                            {
                                [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_count"], sender.type, [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"dose"]]];
                                
                            }
                            else
                            {
                                [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_count"], [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_type"], [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"dose"]]];
                                
                            }
                        }
                        else
                        {
                            
                            [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_count"], [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_type"], [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"dose"]]];
                        }
                        
                        Name.text = [Name.text stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                    }
                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                    {
                        if ([[[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_type"] isEqualToString:@"Tablet"])
                        {
                            if ([[[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_count"] intValue] > 1)
                            {
                                [[SHARED_APPDELEGATE dicMedicine] setValue:@"Tabletter" forKey:@"med_type"];
                            }
                        }
                       else if ([[[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_type"] isEqualToString:@"Spray"])
                        {
                            if ([[[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_count"] intValue] > 1)
                            {
                                [[SHARED_APPDELEGATE dicMedicine] setValue:@"Sprays" forKey:@"med_type"];
                            }
                        }

                        
                        [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_count"], [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"med_type"], [[SHARED_APPDELEGATE dicMedicine] valueForKey:@"dose"]]];

                        Name.text = [Name.text stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                    }
                }
            }
            else
            {
                if ([sender.dose isEqualToString:@"piece"])
                {
                    if ([sender.quantity intValue] >1) {
                        [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, @"", sender.type,[typit valueForKey:@"Drug_types"]]];
                    }
                    else
                    {
                        [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, @"", sender.type,[typit valueForKey:@"Drug_less"]]];
                    }
                }
                else
                {
                    [Name setText:[NSString stringWithFormat:@"%@\n%@ %@\n%@",sender.name, @"", sender.type,sender.dose]];
                }
            }
        }

    }
    
}

-(void)getparent:(HistoryViewController*)set
{
    lostView =set;
}

@end
