//
//  ImageViewController.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 23-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UINavigationController
@property (nonatomic, retain) IBOutlet UIToolbar *toolbarUp;
-(void) getStamp: (UIImage*) image tegit:(NSInteger) tagNow;
@end
