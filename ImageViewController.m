//
//  ImageViewController.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 23-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "ImageViewController.h"
#import "Item.h"

@interface ImageViewController ()

@end

@implementation ImageViewController
@synthesize toolbarUp;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];

    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Cancel:)];

    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Cancel setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }



    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];


    UIView *textit  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 160, 20)];
    [title setTextColor:[UIColor whiteColor]];
    [title setText:NSLocalizedString(@"Taken Image",nil)];
    [title setFont:[UIFont boldSystemFontOfSize:15]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];
    [textit addSubview:title];


    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:textit];







    toolbarUp = [[UIToolbar alloc] init];
    toolbarUp.frame = CGRectMake(0, -2, 320, 54);
    [toolbarUp setBarTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    
    [toolbarUp setTranslucent:NO];
    [self.view addSubview:toolbarUp];


    NSArray *itemsUp = [NSArray arrayWithObjects:Cancel,flexItem,random,flexItem,nil];
    toolbarUp.items = itemsUp;




    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];


    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void) getStamp: (UIImage*) image tegit:(NSInteger) tagNow
{


  
        Item *Testview = [[Item alloc] initWithFrame:CGRectMake(0, 100, 320,  320)];
        [Testview setBackgroundColor:[UIColor clearColor]];
        [Testview setUserInteractionEnabled:YES];
   
        [Testview setImage:image];
       [Testview getParent:self.view];


    [Testview setCenter:self.view.center];


   

       [Testview getParentController:self];


  [self.view insertSubview:Testview belowSubview:toolbarUp];


    
    
    
}


-(void)Save:(UIBarButtonItem*) sender

{



    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)Cancel:(UIBarButtonItem*) sender

{



    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
