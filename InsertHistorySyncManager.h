//
//  MedSyncManager.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncManager.h"
#import "History.h"

@class RssParser;
@interface InsertHistorySyncManager : NSObject <NSXMLParserDelegate, NSStreamDelegate>
{
    
    NSMutableArray *Setcontent;
    NSMutableArray *setNames;
    NSMutableArray *citys;
    NSMutableArray *Result;
    NSURLConnection *theConnection;
    NSMutableString *currentElement;
    NSMutableDictionary *item;
    NSInteger count;
    NSString *setName;
    NSString *term;
    NSMutableData *webData;
}
@property (nonatomic, strong) NSXMLParser *xmlParser;
@property (nonatomic, strong) NSMutableData *webData;
@property (nonatomic, strong) NSMutableArray *Setcontent;
@property (nonatomic, strong) NSMutableArray *setNames;
@property (nonatomic, strong) NSMutableArray *citys;
@property (nonatomic, strong) NSMutableArray *Result;
@property (nonatomic, strong) NSURLConnection *theConnection;
@property (nonatomic, strong) NSMutableString *currentElement;
@property (nonatomic, strong) NSMutableDictionary *item;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, strong) NSString *setName;
@property (nonatomic, strong) NSString *term;
-(void)InsertEventwith:(NSMutableDictionary*)send;
-(void)insertHistoryMissing:(NSString*)termit with:(History*)send;


-(NSDate*)convertDate:(NSDate*)Letit;
-(void)InsertEventwhatmissing:(History*)send;
@end
