//
//  MedSyncManager.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "InsertHistorySyncManager.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "History.h"

@implementation InsertHistorySyncManager
@synthesize Setcontent;
@synthesize setNames;
@synthesize citys;
@synthesize Result;
@synthesize theConnection;
@synthesize currentElement;
@synthesize item;
@synthesize count;
@synthesize setName;
@synthesize term;
@synthesize webData;
@synthesize xmlParser;


-(InsertHistorySyncManager*)init{
	self = [super init];
	if(self){
        
        
    }
    
    return self;
    
}


-(NSInteger)seeIflate:(NSDictionary*)items

{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSDate *now = [[NSDate alloc] init];
    
    
    
    
    NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
    [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
    
    
    NSDateFormatter *dateFormatLevelDays = [[NSDateFormatter alloc] init];
    [dateFormatLevelDays setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevelDays setDateFormat:@"dd/MM/yyyy"];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:[appDelegate convertString:@"hh:mm a"]];
    
    
    NSDateFormatter *dateFormat24 = [[NSDateFormatter alloc] init];
    [dateFormat24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat24 setDateFormat:@"HH:mm"];
    
    
    
    
    NSDate *Action =[dateFormat24 dateFromString:[dateFormat24 stringFromDate:[dateFormat dateFromString:[items valueForKey:@"Original_Time"]]]];
    
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:Action];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    [dateCompsEarly setHour:[timeComponents hour]];
    if ([[[[GetData getSetting] valueForKey:@"Early"] objectAtIndex:0] isEqualToString:@"0"] ) {
        
        [dateCompsEarly setMinute:timeComponents.minute-[[[[GetData getSetting] valueForKey:@"Early"] objectAtIndex:0] intValue]-1];
    }
    else
        
    {
        [dateCompsEarly setMinute:timeComponents.minute-[[[[GetData getSetting] valueForKey:@"Early"] objectAtIndex:0] intValue]];
    }
    
    [dateCompsEarly setSecond:0];
    
    NSDateComponents *timeComponents2 = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:Action];
    
    NSDateComponents *dateCompsEarly2 = [[NSDateComponents alloc] init];
    
    
    [dateCompsEarly2 setHour:[timeComponents2 hour]];
    if ([[[[GetData getSetting] valueForKey:@"Late"] objectAtIndex:0] isEqualToString:@"0"] ) {
        
        [dateCompsEarly2 setMinute:timeComponents2.minute+[[[[GetData getSetting] valueForKey:@"Late"] objectAtIndex:0] intValue]+1];
    }
    else
        
    {
        [dateCompsEarly2 setMinute:timeComponents2.minute+[[[[GetData getSetting] valueForKey:@"Late"] objectAtIndex:0] intValue]];
    }
    
    [dateCompsEarly2 setSecond:[timeComponents2 second]];
    NSDate *Timelate = [calendar dateFromComponents:dateCompsEarly2];
    
    
    
    NSDate *TimeFront = [dateFormatLevel dateFromString:[NSString stringWithFormat:@"%@ %@", [items valueForKey:@"Date"],[dateFormat stringFromDate:Timelate]]];
    
    NSDate *TimeAction= [dateFormatLevel dateFromString:[NSString stringWithFormat:@"%@ %@", [dateFormatLevelDays stringFromDate:now],[items valueForKey:@"Time_Action"]]];
    
    
    
    
    
    
    NSTimeInterval interval2 = [TimeFront timeIntervalSinceDate:TimeAction];
    int hoursLate = (int)interval2 / 3600;             // integer division to get the hours part
    int minutesLate = (interval2 - (hoursLate*3600)) / 60; // interval minus hours part (in seconds) divided by 60 yields minutes
    
    
    NSInteger Taken =minutesLate+(hoursLate*60);
    
    
    
    
    
    return Taken;
}

-(void)InsertEventwhatmissing:(History*)send

{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
  
      NSDate *Schedule =send.originaltime;
    
    
    NSDate *Event =send.timeaction;
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    
    
    NSString *EventString = [[dateFormatter2 stringFromDate:Event] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    NSString *ScheduleString = [[dateFormatter2 stringFromDate:Schedule] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    
    
    
    
    if ([[[self switchAmount:send.type]valueForKey:@"Strength_unit"] isEqualToString:@"-"]) {
     
        //03-08-2017
        /*
        NSString *soapMessage = [NSString stringWithFormat:
                                 
                                 @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                                 "<soapenv:Header/>"
                                 "<soapenv:Body>"
                                 "<tem:insertEvent>"
                                 "<tem:myEvent>"
                                 
                                 "<dos:DoseAmount>%f</dos:DoseAmount>"
                                 "<dos:DoseStrength>%f</dos:DoseStrength>"
                                 "<dos:DoseStrengthUnit>%@</dos:DoseStrengthUnit>"
                                 
                                 "<dos:DrugID>%@</dos:DrugID>"
                                 "<dos:EventDateTime>%@</dos:EventDateTime>"
                                 
                                 
                                 "<dos:EventDescription>%@</dos:EventDescription>"
                                 "<dos:EventID>%@</dos:EventID>"
                                 
                                 "<dos:HisDrugName>%@</dos:HisDrugName>"
                                 "<dos:HisDrugTypeID>%i</dos:HisDrugTypeID>"
                                 
                                 "<dos:HisScheduleFrequencyID>%i</dos:HisScheduleFrequencyID>"
                                 
                                 
                                 "<dos:OperationTypeID>%i</dos:OperationTypeID>"
                                 "<dos:PatientID>%@</dos:PatientID>"
                                 "<dos:ProfileID>%@</dos:ProfileID>"
                                 "<dos:ScheduleDateTime>%@</dos:ScheduleDateTime>"
                                 "<dos:Ticket>%@</dos:Ticket>"
                                 "</tem:myEvent>"
                                 "</tem:insertEvent>"
                                 "</soapenv:Body>"
                                 "</soapenv:Envelope>",
                                 
                                 
                                 [send.quantity floatValue],
                                 [[[self switchDose:send.dose] valueForKey:@"Dose"] floatValue],
                                 [[self switchDose:send.dose]valueForKey:@"Dose_unit"],
                                 
                                 
                                 send.idmedicine,
                                 EventString,
                                 send.taken,
                                  [send valueForKey:@"sendid"],
                                 
                                 send.name,
                                 [[self changeIDMed:send.type]intValue],
                                 1,
                                 [self getIDTaken:send.taken],
                                 
                                 
                                 appDelegate.strApplicationUUID,
                                 send.id,
                                 ScheduleString,
                                 [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"],nil];
        
        
        
        //To suppress the leak in NSXMLParser
        NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/IDosingWS/insertEvent" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:
         [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
         //////////
        
    ////NSLog(@"%@", soapMessage);
        
        
        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        webData = [[NSMutableData data] retain];
        */
    }
    else
    {
        
     //03-08-2017
        /*
        NSString *soapMessage = [NSString stringWithFormat:
                                 
                                 @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                                 "<soapenv:Header/>"
                                 "<soapenv:Body>"
                                 "<tem:insertEvent>"
                                 "<tem:myEvent>"
                                 
                                 "<dos:DoseAmount>%f</dos:DoseAmount>"
                                 "<dos:DoseAmountUnit>%@</dos:DoseAmountUnit>"
                                 "<dos:DoseStrength>%f</dos:DoseStrength>"
                                 "<dos:DoseStrengthUnit>%@</dos:DoseStrengthUnit>"
                                 
                                 
                                 "<dos:DrugID>%@</dos:DrugID>"
                                 "<dos:EventDateTime>%@</dos:EventDateTime>"
                                 
                                 
                                 "<dos:EventDescription>%@</dos:EventDescription>"
                                 "<dos:EventID>%@</dos:EventID>"
                                 
                                 "<dos:HisDrugName>%@</dos:HisDrugName>"
                                 "<dos:HisDrugTypeID>%i</dos:HisDrugTypeID>"
                                 
                                 "<dos:HisScheduleFrequencyID>%i</dos:HisScheduleFrequencyID>"
                                 
                                 
                                 "<dos:OperationTypeID>%i</dos:OperationTypeID>"
                                 "<dos:PatientID>%@</dos:PatientID>"
                                 "<dos:ProfileID>%@</dos:ProfileID>"
                                 "<dos:ScheduleDateTime>%@</dos:ScheduleDateTime>"
                                 "<dos:Ticket>%@</dos:Ticket>"
                                 "</tem:myEvent>"
                                 "</tem:insertEvent>"
                                 "</soapenv:Body>"
                                 "</soapenv:Envelope>",
                                 
                                 
                                 [send.quantity floatValue],
                                 [[self switchAmount:send.type]valueForKey:@"Strength_unit"],
                                 [[[self switchDose:send.dose] valueForKey:@"Dose"] floatValue],
                                 [[self switchDose:send.dose]valueForKey:@"Dose_unit"],
                                 
                                 send.idmedicine,
                                 EventString,
                                 send.taken,
                                  [send valueForKey:@"sendid"],
                                 
                                 send.name,
                                 [[self changeIDMed:send.type]intValue],
                                 1,
                                 [self getIDTaken:send.taken],
                                 
                                 
                                 appDelegate.strApplicationUUID,
                                 send.id,
                                 
                                 ScheduleString,
                                 [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"],nil];
        
        
        
        
        
        ////////////NSLog(@"%@", soapMessage);
        
        //To suppress the leak in NSXMLParser
        NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/IDosingWS/insertEvent" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:
         [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        ////////////////NSLog(@"%@", soapMessage);
        
        
        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        webData = [[NSMutableData data] retain];
        
        */
        
    }
    
    
}

-(void)insertHistoryMissing:(NSString*)termit with:(History*)send



{
 ////NSLog(@"%@", send);
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    
    NSDateFormatter *dateFrom2 = [[NSDateFormatter alloc] init];
    [dateFrom2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFrom2 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
    
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSDate *Schedule =send.originaltime;
    
    
    NSDate *Event =send.timeaction;
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    
    
    NSString *EventString = [[dateFormatter2 stringFromDate:Event] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    NSString *ScheduleString = [[dateFormatter2 stringFromDate:Schedule] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    
    
    
    
    if ([[[self switchAmount:[send valueForKey:@"Type"]]valueForKey:@"Strength_unit"] isEqualToString:@"-"]) {
        
        ////////////////NSLog(@"DoseAmount %@", [[self switchDose:[send valueForKey:@"Dose"]] valueForKey:@"Dose"]);
        ////////////////NSLog(@"DoseStrength %@", [[self switchDose:[send valueForKey:@"Dose"]] valueForKey:@"Dose_unit"]);
        
        ////////////////NSLog(@"HisDrugTypeID %@", [[self switchAmount:[send valueForKey:@"Type"]]valueForKey:@"Strength_unit"]);
        
        //03-08-2017
        
        /*
        NSString *soapMessage = [NSString stringWithFormat:
                                 
                                 @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                                 "<soapenv:Header/>"
                                 "<soapenv:Body>"
                                 "<tem:insertEvent>"
                                 "<tem:myEvent>"
                                 
                                 "<dos:DoseAmount>%f</dos:DoseAmount>"
                                 "<dos:DoseStrength>%f</dos:DoseStrength>"
                                 "<dos:DoseStrengthUnit>%@</dos:DoseStrengthUnit>"
                                 
                                 "<dos:DrugID>%@</dos:DrugID>"
                                 "<dos:EventDateTime>%@</dos:EventDateTime>"
                                 
                                 
                                 "<dos:EventDescription>%@</dos:EventDescription>"
                                 "<dos:EventID>%@</dos:EventID>"
                                 
                                 "<dos:HisDrugName>%@</dos:HisDrugName>"
                                 "<dos:HisDrugTypeID>%i</dos:HisDrugTypeID>"
                                 
                                 "<dos:HisScheduleFrequencyID>%i</dos:HisScheduleFrequencyID>"
                                 
                                 
                                 "<dos:OperationTypeID>%i</dos:OperationTypeID>"
                                 "<dos:PatientID>%@</dos:PatientID>"
                                 "<dos:ProfileID>%@</dos:ProfileID>"
                                 "<dos:ScheduleDateTime>%@</dos:ScheduleDateTime>"
                                 "<dos:Ticket>%@</dos:Ticket>"
                                 "</tem:myEvent>"
                                 "</tem:insertEvent>"
                                 "</soapenv:Body>"
                                 "</soapenv:Envelope>",
                                 
                                 
                                 [send.quantity floatValue],
                                 [[[self switchDose:send.dose] valueForKey:@"Dose"] floatValue],
                                 [[self switchDose:send.dose]valueForKey:@"Dose_unit"],
                                 
                                 
                                 send.idmedicine,
                                 EventString,
                                 send.taken,
                                  [send valueForKey:@"sendid"],
                                 
                                 send.name,
                                 [[self changeIDMed:send.type]intValue],
                                 1,
                                 [self getIDTaken:send.taken],
                                 
                                 
                                 appDelegate.strApplicationUUID,
                                 send.id,
                                 ScheduleString,
                                 [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"],nil];
        
        
        
        ////////////NSLog(@"%@", soapMessage);
        
        //To suppress the leak in NSXMLParser
        NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/IDosingWS/insertEvent" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:
         [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        //////////////NSLog(@"soapMessage %@", soapMessage);
        
        
        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        webData = [[NSMutableData data] retain];
        
         */
         
    }
    else
    {
        
        ////////////////NSLog(@"DoseAmount %@", [[self switchDose:[send valueForKey:@"Dose"]] valueForKey:@"Dose"]);
        ////////////////NSLog(@"DoseStrength %@", [[self switchDose:[send valueForKey:@"Dose"]] valueForKey:@"Dose_unit"]);
        
        ////////////////NSLog(@"HisDrugTypeID %@", [[self switchAmount:[send valueForKey:@"Type"]]valueForKey:@"Type"]);
        ////////////////NSLog(@"DoseStrengthUnit %@", [[self switchAmount:[send valueForKey:@"Type"]]valueForKey:@"Strength_unit"]);
        
        //03-08-2017
        
        /*
        NSString *soapMessage = [NSString stringWithFormat:
                                 
                                 @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                                 "<soapenv:Header/>"
                                 "<soapenv:Body>"
                                 "<tem:insertEvent>"
                                 "<tem:myEvent>"
                                 
                                 "<dos:DoseAmount>%f</dos:DoseAmount>"
                                 "<dos:DoseAmountUnit>%@</dos:DoseAmountUnit>"
                                 "<dos:DoseStrength>%f</dos:DoseStrength>"
                                 "<dos:DoseStrengthUnit>%@</dos:DoseStrengthUnit>"
                                 
                                 
                                 "<dos:DrugID>%@</dos:DrugID>"
                                 "<dos:EventDateTime>%@</dos:EventDateTime>"
                                 
                                 
                                 "<dos:EventDescription>%@</dos:EventDescription>"
                                 "<dos:EventID>%@</dos:EventID>"
                                 
                                 "<dos:HisDrugName>%@</dos:HisDrugName>"
                                 "<dos:HisDrugTypeID>%i</dos:HisDrugTypeID>"
                                 
                                 "<dos:HisScheduleFrequencyID>%i</dos:HisScheduleFrequencyID>"
                                 
                                 
                                 "<dos:OperationTypeID>%i</dos:OperationTypeID>"
                                 "<dos:PatientID>%@</dos:PatientID>"
                                 "<dos:ProfileID>%@</dos:ProfileID>"
                                 "<dos:ScheduleDateTime>%@</dos:ScheduleDateTime>"
                                 "<dos:Ticket>%@</dos:Ticket>"
                                 "</tem:myEvent>"
                                 "</tem:insertEvent>"
                                 "</soapenv:Body>"
                                 "</soapenv:Envelope>",
                                 
                                 
                                 [send.quantity floatValue],
                                 [[self switchAmount:send.type]valueForKey:@"Strength_unit"],
                                 [[[self switchDose:send.dose] valueForKey:@"Dose"] floatValue],
                                 [[self switchDose:send.dose]valueForKey:@"Dose_unit"],
                                 
                                 send.idmedicine,
                                 EventString,
                                 send.taken,
                                  [send valueForKey:@"sendid"],
                                 
                                 send.name,
                                 [[self changeIDMed:send.type]intValue],
                                 2,
                                 [self getIDTaken:send.taken],
                                 
                                 
                                 appDelegate.strApplicationUUID,
                                 send.id,
                                 
                                 ScheduleString,
                                 [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"],nil];
        
        
        
        
        
        ////////////NSLog(@"%@", soapMessage);
        
        //To suppress the leak in NSXMLParser
        NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/IDosingWS/insertEvent" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:
         [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        //////////////NSLog(@"soapMessage %@", soapMessage);
        
        
        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        webData = [[NSMutableData data] retain];
        
        */
        
        
        
    }
    
}


-(void)InsertEventwith:(NSMutableDictionary*)send

{
    
    
    
   ////NSLog(@"%@", send);
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    

    
    
    NSDateFormatter *dateFrom2 = [[NSDateFormatter alloc] init];
    [dateFrom2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFrom2 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
    
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSDate *Schedule =[dateFrom2 dateFromString:[send valueForKey:@"Original_Time"]];
    
    
    NSDate *Event =[dateFrom2 dateFromString:[send valueForKey:@"Time_Action"]];
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    
    
    NSString *EventString = [[dateFormatter2 stringFromDate:Event] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    NSString *ScheduleString = [[dateFormatter2 stringFromDate:Schedule] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    
    
    
    
    if ([[[self switchAmount:[send valueForKey:@"Type"]]valueForKey:@"Strength_unit"] isEqualToString:@"-"]) {
        
        ////////////////NSLog(@"DoseAmount %@", [[self switchDose:[send valueForKey:@"Dose"]] valueForKey:@"Dose"]);
        ////////////////NSLog(@"DoseStrength %@", [[self switchDose:[send valueForKey:@"Dose"]] valueForKey:@"Dose_unit"]);
        
        ////////////////NSLog(@"HisDrugTypeID %@", [[self switchAmount:[send valueForKey:@"Type"]]valueForKey:@"Strength_unit"]);
        
        //03-08-2017
        /*
        NSString *soapMessage = [NSString stringWithFormat:
                                 
                                 @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                                 "<soapenv:Header/>"
                                 "<soapenv:Body>"
                                 "<tem:insertEvent>"
                                 "<tem:myEvent>"
                                 
                                 "<dos:DoseAmount>%f</dos:DoseAmount>"
                                 "<dos:DoseStrength>%f</dos:DoseStrength>"
                                  "<dos:DoseStrengthUnit>%@</dos:DoseStrengthUnit>"
                                 
                                 "<dos:DrugID>%@</dos:DrugID>"
                                 "<dos:EventDateTime>%@</dos:EventDateTime>"
                                 
                                 
                                 "<dos:EventDescription>%@</dos:EventDescription>"
                                 "<dos:EventID>%@</dos:EventID>"
                                 
                                 "<dos:HisDrugName>%@</dos:HisDrugName>"
                                 "<dos:HisDrugTypeID>%i</dos:HisDrugTypeID>"
                                 
                                 "<dos:HisScheduleFrequencyID>%i</dos:HisScheduleFrequencyID>"
                                 
                                 
                                 "<dos:OperationTypeID>%i</dos:OperationTypeID>"
                                 "<dos:PatientID>%@</dos:PatientID>"
                                 "<dos:ProfileID>%@</dos:ProfileID>"
                                 "<dos:ScheduleDateTime>%@</dos:ScheduleDateTime>"
                                 "<dos:Ticket>%@</dos:Ticket>"
                                 "</tem:myEvent>"
                                 "</tem:insertEvent>"
                                 "</soapenv:Body>"
                                 "</soapenv:Envelope>",
                                 
                                 
                                 [[send valueForKey:@"Quantity"] floatValue],
                                 [[[self switchDose:[send valueForKey:@"Dose"]] valueForKey:@"Dose"] floatValue],
                                 [[self switchDose:[send valueForKey:@"Dose"]]valueForKey:@"Dose_unit"],
                                 
                                 
                                 [send valueForKey:@"ID_medicine"],
                                 EventString,
                                 [send valueForKey:@"Taken"],
                                 [send valueForKey:@"sendid"],
                                 
                                 [send valueForKey:@"Name"],
                                 [[self changeIDMed:[send valueForKey:@"Type"]]intValue],
                                 1,
                                 [self getIDTaken:[send valueForKey:@"Taken"]],
                                 
                                 
                                   appDelegate.strApplicationUUID,
                                 [send valueForKey:@"ID"],
                                 ScheduleString,
                                 [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"],nil];
        
        
        
         //////////NSLog(@"%@", soapMessage);
        
        //To suppress the leak in NSXMLParser
        NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/IDosingWS/insertEvent" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:
         [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        //////////////NSLog(@"soapMessage %@", soapMessage);
        
        
        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        webData = [[NSMutableData data] retain];
        
         */
         
    }
    else
    {
        
        ////////////////NSLog(@"DoseAmount %@", [[self switchDose:[send valueForKey:@"Dose"]] valueForKey:@"Dose"]);
        ////////////////NSLog(@"DoseStrength %@", [[self switchDose:[send valueForKey:@"Dose"]] valueForKey:@"Dose_unit"]);
        
        ////////////////NSLog(@"HisDrugTypeID %@", [[self switchAmount:[send valueForKey:@"Type"]]valueForKey:@"Type"]);
        ////////////////NSLog(@"DoseStrengthUnit %@", [[self switchAmount:[send valueForKey:@"Type"]]valueForKey:@"Strength_unit"]);
        
        //03-08-2017
        
        /*
        NSString *soapMessage = [NSString stringWithFormat:
                                 
                                 @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                                 "<soapenv:Header/>"
                                 "<soapenv:Body>"
                                 "<tem:insertEvent>"
                                 "<tem:myEvent>"
                                 
                                 "<dos:DoseAmount>%f</dos:DoseAmount>"
                                 "<dos:DoseAmountUnit>%@</dos:DoseAmountUnit>"
                                 "<dos:DoseStrength>%f</dos:DoseStrength>"
                                 "<dos:DoseStrengthUnit>%@</dos:DoseStrengthUnit>"
                                 
                                 
                                 "<dos:DrugID>%@</dos:DrugID>"
                                 "<dos:EventDateTime>%@</dos:EventDateTime>"
                                 
                                 
                                 "<dos:EventDescription>%@</dos:EventDescription>"
                                 "<dos:EventID>%@</dos:EventID>"
                                 
                                 "<dos:HisDrugName>%@</dos:HisDrugName>"
                                 "<dos:HisDrugTypeID>%i</dos:HisDrugTypeID>"
                                 
                                 "<dos:HisScheduleFrequencyID>%i</dos:HisScheduleFrequencyID>"
                                 
                                 
                                 "<dos:OperationTypeID>%i</dos:OperationTypeID>"
                                 "<dos:PatientID>%@</dos:PatientID>"
                                 "<dos:ProfileID>%@</dos:ProfileID>"
                                 "<dos:ScheduleDateTime>%@</dos:ScheduleDateTime>"
                                 "<dos:Ticket>%@</dos:Ticket>"
                                 "</tem:myEvent>"
                                 "</tem:insertEvent>"
                                 "</soapenv:Body>"
                                 "</soapenv:Envelope>",
                                 
                                 
                                 [[send valueForKey:@"Quantity"] floatValue],
                                  [[self switchAmount:[send valueForKey:@"Type"]]valueForKey:@"Strength_unit"],
                                 [[[self switchDose:[send valueForKey:@"Dose"]] valueForKey:@"Dose"] floatValue],
                                 [[self switchDose:[send valueForKey:@"Dose"]]valueForKey:@"Dose_unit"],
                                 
                                 [send valueForKey:@"ID_medicine"],
                                 EventString,
                                 [send valueForKey:@"Taken"],
                                  [send valueForKey:@"sendid"],
                                 
                                 [send valueForKey:@"Name"],
                                 [[self changeIDMed:[send valueForKey:@"Type"]]intValue],
                                 2,
                                 [self getIDTaken:[send valueForKey:@"Taken"]],
                                 
                                 
                                  appDelegate.strApplicationUUID,
                                 [send valueForKey:@"ID"],
                                
                                 ScheduleString,
                                 [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"],nil];
        
        
        
        
        
         ////////////NSLog(@"%@", soapMessage);
        
        //To suppress the leak in NSXMLParser
        NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/IDosingWS/insertEvent" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:
         [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        //////////////NSLog(@"soapMessage %@", soapMessage);
        
        
        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        webData = [[NSMutableData data] retain];
        
        
        */
        
        
    }
    
    
}





-(NSString*)changeIDMed:(NSString*)send

{
    
    
    
    
    NSString * strippedNumber=@"7";
    
    
    NSArray *numbers1 = [send componentsSeparatedByString:@" "];
    
    if ([numbers1 count] >1) {
        
        
        NSArray *names1 =[NSArray arrayWithObjects:@"Drop,1",@"Drops,1",@"Inhaler,2",@"Inhalations,2", @"Injection,3",@"Injections,3",@"Liquid,4",@"Ointment,5",@"Patches,6",@"Patch,6",@"Tablet,7",@"Tablets,7",@"Sprays,8",@"Spray,8", @"Other,9",nil];
        
       for (NSInteger i=0 ; i<[names1 count]; i++) {
            
            
            NSArray *numbers2 = [[names1 objectAtIndex:i] componentsSeparatedByString:@","];
            
            
            if ([NSLocalizedString([numbers2 objectAtIndex:0],nil) isEqualToString:[numbers1 objectAtIndex:1]]) {
                
                
                strippedNumber = [numbers2 objectAtIndex:1];
                
                
                
                
            }
            
            
            
            
        }
        
    }
    
    else
    {
        
        
        NSArray *names1 =[NSArray arrayWithObjects:@"Drop,1",@"Drops,1",@"Inhaler,2",@"Inhalations,2", @"Injection,3",@"Injections,3",@"Liquid,4",@"Ointment,5",@"Patches,6",@"Patch,6",@"Tablet,7",@"Tablets,7",@"Sprays,8",@"Spray,8", @"Other,9",nil];
        
       for (NSInteger i=0 ; i<[names1 count]; i++) {
            
            
            NSArray *numbers2 = [[names1 objectAtIndex:i] componentsSeparatedByString:@","];
            
            
            if ([NSLocalizedString([numbers2 objectAtIndex:0],nil) isEqualToString:send]) {
                
                
                strippedNumber = [numbers2 objectAtIndex:1];
                
                
                
                
            }
            
            
            
            
        }
        
    }
    
    
    
    return strippedNumber;
    
}



-(NSMutableDictionary*)switchDose:(NSString*)send

{

    
    
    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
    
    
    
    NSArray *numbers = [send componentsSeparatedByString:@" "];
    
    
    
    
    [set setObject:[numbers objectAtIndex:0] forKey:@"Dose"];
    
    
    send = [send stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@ ", [numbers objectAtIndex:0]] withString:@""];
    
    
        //////////NSLog(@"%@", send);
    
    if ([numbers count]>1) {
        
        
        
        NSArray *names1 =[NSArray arrayWithObjects:@"%,%,1",@"IU,IU,2",@"mcg,mcg,3",@"mg/mL,mg/mL,4",@"g/mL,g/mL,5",@"mg,mg,6",@"mg/L,mg/L,7",@"units/mL,units/mL,8",@"IU/mL,IU/mL,9",@"mg/cc,mg/cc,10",@"units/cc,units/cc,11",@"IU/cc,IU/cc,12",@"mg/tsp,mg/tsp,13",@"mg/tbsp,mg/tbsp,14",@"mg/oz,mg/oz,15",@"mg/hr,mg/hr,16",@"mcg/hr,mcg/hr,17",@"mg/24hr,mg/24hr,18",@"g,g,19",@"units,units,20",@"meq,meq,21",@"mL,mL,22",@"cc,cc,23",@"tsp,tsp,24",@"tbsp,tbsp,25",@"oz,oz,26", nil];
    
        
       for (NSInteger i=0 ; i<[names1 count]; i++) {
            
            
            NSArray *numbers2 = [[names1 objectAtIndex:i] componentsSeparatedByString:@","];
            
            
            
            if ([NSLocalizedString([numbers2 objectAtIndex:0],nil)  isEqualToString:[send lowercaseString]]) {
                
                
                [set setObject:[numbers2 objectAtIndex:0] forKey:@"Dose_unit"];
                
                
            }
        }
        
    }
    else
    {
        [set setObject:@"-" forKey:@"Dose_unit"];
    }
    
    //////////NSLog(@"set %@", set);
    
    
    return set;
    
    
    
}


-(NSMutableDictionary*)switchAmount:(NSString*)send

{
    //////////NSLog(@"%@", send);
    
    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
    
      [set setObject:@"-" forKey:@"Strength_unit"];
    
    NSArray *numbers = [send componentsSeparatedByString:@" "];
    
    
    if ([numbers count]>1) {
        
        [set setObject:[numbers objectAtIndex:1] forKey:@"Type"];
        
        send = [send stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@ ", [numbers objectAtIndex:0]] withString:@""];
        
        
        
        NSArray *names1 =[NSArray arrayWithObjects:@"%,%,1",@"IU,IU,2",@"mcg,mcg,3",@"mg/mL,mg/mL,4",@"g/mL,g/mL,5",@"mg,mg,6",@"mg/L,mg/L,7",@"units/mL,units/mL,8",@"IU/mL,IU/mL,9",@"mg/cc,mg/cc,10",@"units/cc,units/cc,11",@"IU/cc,IU/cc,12",@"mg/tsp,mg/tsp,13",@"mg/tbsp,mg/tbsp,14",@"mg/oz,mg/oz,15",@"mg/hr,mg/hr,16",@"mcg/hr,mcg/hr,17",@"mg/24hr,mg/24hr,18",@"g,g,19",@"units,units,20",@"meq,meq,21",@"mL,mL,22",@"cc,cc,23",@"tsp,tsp,24",@"tbsp,tbsp,25",@"oz,oz,26", nil];
        
       for (NSInteger i=0 ; i<[names1 count]; i++) {
            
            
            NSArray *numbers2 = [[names1 objectAtIndex:i] componentsSeparatedByString:@","];
            
            
            
            
            if ([[NSLocalizedString([numbers2 objectAtIndex:0],nil)lowercaseString]  isEqualToString:[send lowercaseString]]) {
                
                
                [set setObject:[numbers2 objectAtIndex:0] forKey:@"Strength_unit"];
                
                
            }
        }
        
    }
    else
    {
        
      
    }
    
      //////////NSLog(@"%@", set);
    
    return set;
    
    
    
}


-(NSDate*)convertDate:(NSDate*)Letit

{
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:Letit];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:Letit];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    
    [dateCompsEarly setDay:[dateComponents day]];
    [dateCompsEarly setMonth:[dateComponents month]];
    [dateCompsEarly setYear:[dateComponents year]];
    [dateCompsEarly setHour:[timeComponents hour]];
    [dateCompsEarly setMinute:timeComponents.minute];
    [dateCompsEarly setSecond:0];
    
    
    return [calendar dateFromComponents:dateCompsEarly];
    
    
}

-(int)getIDTaken:(NSString*)set


{
    
    ////////////NSLog(@"%@", set);
    
 
    
    if ([set isEqualToString:@"Taken"]) {
        
         return 1;
    }
    else  if ([set isEqualToString:@"Skipped"])
    {
         return 3;
    }
    else  if ([set isEqualToString:@"Taken earlier"])
    {
         return 1;
        
    }
    else  if ([set isEqualToString:@"Postponed"])
    {
        return 4;
    }
    else  if ([set isEqualToString:@"Taken late"])
    {
          return 1;
        
    }
    else
        
    {
          return 1;
    }
    
    
  
}

-(void)GetID:(NSString*)termit with:(NSDictionary*)send

{
    
    
}


-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	
    [webData setLength: 0];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[webData appendData:data];
    
    unsigned char byteBuffer[[webData length]];
    [webData getBytes:byteBuffer];
    
    
	
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    

    
    
    xmlParser = [[NSXMLParser alloc] initWithData: webData];
    [xmlParser setDelegate: self];
    [xmlParser setShouldResolveExternalEntities: YES];
    [xmlParser parse];//start parsing
    
    
    
    
    
    
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
   
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    myApp.currentHistory.send =@"NO";
    
}

-(void) parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
{
    
    
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    
    
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
	
    
	
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
   ////NSLog(@"string %@", string);
    
    
    if ([string rangeOfString:[[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"]].location != NSNotFound) {
        
       myApp.currentHistory.send =@"NO";
        
    }
    else
        
    {
        
       myApp.currentHistory.send =@"YES";
        
       ////NSLog(@"hold on");
        
    }


    
    
    
}

-(void)parser:(NSXMLParser*)myParser {
    
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
         appDelegate.currentHistory.send =@"YES";

    
}




-(NSString *) change: (NSString *) dumb
{
    if ([dumb isEqualToString:@""]) {
        
        dumb=@"-";
        
        return dumb;
        
    }
    
    else
    {
        
        return dumb;
        
    }
    
}





@end
