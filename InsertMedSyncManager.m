//
//  MedSyncManager.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "InsertMedSyncManager.h"
#import "GetData.h"
#import "AppDelegate.h"

@implementation InsertMedSyncManager
@synthesize Setcontent;
@synthesize setNames;
@synthesize citys;
@synthesize Result;
@synthesize theConnection;
@synthesize currentElement;
@synthesize item;
@synthesize count;
@synthesize setName;
@synthesize term;
@synthesize webData;
@synthesize xmlParser;


-(InsertMedSyncManager*)init{
	self = [super init];
	if(self){
        
        
    }
    
    return self;
}

-(void)insertMedicineMissing:(NSString*)termit with:(Medicine*)send;

{
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    Setcontent = [[NSMutableArray alloc] init];
    citys = [[NSMutableArray alloc] init];
    setName=@"insertDrugResult";
    
    
    
   //NSLog(@"%@", send);
    
    
    
    setNames = [[NSMutableArray alloc] initWithObjects:@"insertDrugResponse", nil];
    
    
    
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:[myApp convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
    
    
    NSArray *numbers3 = [send.times componentsSeparatedByString:@","];
    
    
    NSString *time=[numbers3 objectAtIndex:0];
    
    
    NSDateFormatter *format24 = [[NSDateFormatter alloc] init];
    [format24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [format24 setDateFormat:@"HH:mm"];
    
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [format setDateFormat:@"hh:mm a"];
    
    
    
    if ([format24 dateFromString:time]) {
        
        
        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
        
        if (!strsetting)
        {
            // time = [format stringFromDate:[format24 dateFromString:time]];
            
            ////////////////NSLog(@"NO format24 %@", [format stringFromDate:[format24 dateFromString:time]]);
            
            time= [format stringFromDate:[format24 dateFromString:time]];
        }
        
        else
        {
            
            ////////////////NSLog(@"YES format24 %@", time);
            // time = [format stringFromDate:[format24 dateFromString:time]];
        }
        
        
    }
    else
        
    {
        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
        
        if (!strsetting)
        {
            
            ////////////////NSLog(@"NO format %@", [format24 stringFromDate:[format dateFromString:time]]);
            // time = [format stringFromDate:[format24 dateFromString:time]];
            
            time= [format24 stringFromDate:[format dateFromString:time]];
        }
        
        else
        {
            
            ////////////////NSLog(@"YES format %@", time);
            // time = [format stringFromDate:[format24 dateFromString:time]];
        }
        
        
        
        
    }
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    
    NSDateFormatter *localTimeZoneFormatter = [NSDateFormatter new];
    localTimeZoneFormatter.timeZone = [NSTimeZone localTimeZone];
    localTimeZoneFormatter.dateFormat = @"Z";
    
    
    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    NSString *When;
    
    if (send.starting1) {
        
        When =[[dateFormat stringFromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:send.starting1], time]]] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    }
    
    else
        
    {
        
        NSDate *now = [dateConvert dateFromString:send.starting];
        
        When =[[dateFormat stringFromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now], time]]] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    }
    
    
    
    
    NSString *End;
    
    
    if([send.duration isEqualToString:@"Nooit"]||[send.duration isEqualToString:NSLocalizedString(@"Never",nil)]||[send.duration isEqualToString:@"Nie"]||[send.duration isEqualToString:@"Jamais"]||[send.duration isEqualToString:@"Mai"]||[send.duration isEqualToString:@"Ποτέ"])
    {
        
        End =@"1970-01-01T00:00:00";
        
          //NSLog(@"%@", End);
    }
    else
        
    {
        End =[[dateFormat stringFromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", send.duration,time]]] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
        
       //NSLog(@"%@", End);
        
    }
    //03-08-2017
    
    if ([[self changeNumbers:send.dose_unit] isEqualToString:@"-"]) {
    
        NSString *soapMessage = [NSString stringWithFormat:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                                 "<soapenv:Header/>"
                                 "<soapenv:Body>"
                                 "<tem:insertDrug>"
                                 "<tem:myDrug>"
                                 "<dos:Directions>%@</dos:Directions>"
                                 //Directions
                                 "<dos:DoseAmount>%f</dos:DoseAmount>"
                                 "<dos:DoseStrength>%f</dos:DoseStrength>"
                                 "<dos:DoseStrengthUnitType>%i</dos:DoseStrengthUnitType>"
                                 "<dos:DrugID>%@</dos:DrugID>"
                                 "<dos:DrugName>%@</dos:DrugName>"
                                 "<dos:DrugTakeTypeID>1</dos:DrugTakeTypeID>"
                                 "<dos:DrugTypeID>%i</dos:DrugTypeID>"
                                 "<dos:EndDate>%@</dos:EndDate>"
                                 "<dos:FrequencyText>%@</dos:FrequencyText>"
                                 
                                 "<dos:PatientID>%@</dos:PatientID>"
                                 "<dos:ProfileID>%@</dos:ProfileID>"
                                 "<dos:ScheduleFrequencyID>1</dos:ScheduleFrequencyID>"
                                 "<dos:StartDate>%@</dos:StartDate>"
                                 "<dos:Ticket>%@</dos:Ticket>"
                                 "<dos:TimesText>%@</dos:TimesText>"
                                 "</tem:myDrug>"
                                 "</tem:insertDrug>"
                                 "</soapenv:Body>"
                                 "</soapenv:Envelope>",
                                 send.instructions,
                                 [send.quantity floatValue],
                                 [send.strength floatValue],
                                 [[self changeNumbers:send.strength_unit] intValue],
                                 
                                 send.id_medicine,
                                 send.name,
                                 [[self changeIDMed:send.type]intValue],
                                 End,
                                 [NSString stringWithFormat:@"%@ %@", [[self changeType:NSLocalizedString(send.frequency_text,nil)] stringByReplacingOccurrencesOfString:@"X" withString:send.frequency], [self changeDays:send.day_number]],
                                 myApp.strApplicationUUID,
                                 send.id,
                                 
                                 When,
                                 [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"], [self changeTimes:send.times]];
        
        //To suppress the leak in NSXMLParser
        NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/IDosingWS/insertDrug" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:
         [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
            //NSLog(@"%@", soapMessage);
        
        
        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        webData = [[NSMutableData data] retain];
     
    }
    else
    {
        //03-08-2017
        
        NSString *soapMessage = [NSString stringWithFormat:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                                 "<soapenv:Header/>"
                                 "<soapenv:Body>"
                                 "<tem:insertDrug>"
                                 "<tem:myDrug>"
                                 "<dos:Directions>%@</dos:Directions>"
                                 "<dos:DoseAmount>%f</dos:DoseAmount>"
                                 "<dos:DoseAmountUnitType>%i</dos:DoseAmountUnitType>"
                                 "<dos:DoseStrength>%f</dos:DoseStrength>"
                                 "<dos:DoseStrengthUnitType>%i</dos:DoseStrengthUnitType>"
                                 "<dos:DrugID>%@</dos:DrugID>"
                                 "<dos:DrugName>%@</dos:DrugName>"
                                 "<dos:DrugTakeTypeID>1</dos:DrugTakeTypeID>"
                                 "<dos:DrugTypeID>%i</dos:DrugTypeID>"
                                 "<dos:EndDate>%@</dos:EndDate>"
                                 "<dos:FrequencyText>%@</dos:FrequencyText>"
                                 "<dos:PatientID>%@</dos:PatientID>"
                                 "<dos:ProfileID>%@</dos:ProfileID>"
                                 "<dos:ScheduleFrequencyID>1</dos:ScheduleFrequencyID>"
                                 "<dos:StartDate>%@</dos:StartDate>"
                                 "<dos:Ticket>%@</dos:Ticket>"
                                 "<dos:TimesText>%@</dos:TimesText>"
                                 "</tem:myDrug>"
                                 "</tem:insertDrug>"
                                 "</soapenv:Body>"
                                 "</soapenv:Envelope>",
                                 send.instructions,
                                 [send.quantity floatValue],
                                 [[self changeNumbers:send.dose_unit] intValue],
                                 [send.strength floatValue],
                                 [[self changeNumbers:send.strength_unit] intValue],
                                 
                                 send.id_medicine,
                                 send.name,
                                 [[self changeIDMed:send.type]intValue],
                                 End,
                                 [NSString stringWithFormat:@"%@ %@", [[self changeType:NSLocalizedString(send.frequency_text,nil)] stringByReplacingOccurrencesOfString:@"X" withString:send.frequency], [self changeDays:send.day_number]],
                                 myApp.strApplicationUUID,
                                 send.id,
                                 
                                 When,
                                 [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"],[self changeTimes:send.times]];
        
        //To suppress the leak in NSXMLParser
        NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/IDosingWS/insertDrug" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:
         [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        
        //NSLog(@"%@", soapMessage);
        
        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        webData = [[NSMutableData data] retain];
        
    }
    
    
}

-(void)insertMedicineNew:(NSString*)termit with:(NSMutableDictionary*)send;

{
    
       //NSLog(@"%@", send);
    
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    Setcontent = [[NSMutableArray alloc] init];
    citys = [[NSMutableArray alloc] init];
    setName=@"insertDrugResult";
    
    
    
    setNames = [[NSMutableArray alloc] initWithObjects:@"insertDrugResponse", nil];
    
    
    
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:[myApp convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
    
    
    NSArray *numbers2 = [[send valueForKey:@"Times"] componentsSeparatedByString:@","];
    
    NSString *time=[numbers2 objectAtIndex:0];
    
    
    NSDateFormatter *format24 = [[NSDateFormatter alloc] init];
    [format24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [format24 setDateFormat:@"HH:mm"];
    
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [format setDateFormat:@"hh:mm a"];
    
    
    
    if ([format24 dateFromString:time]) {
        
        
        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
        
        if (!strsetting)
        {
            // time = [format stringFromDate:[format24 dateFromString:time]];
            
            ////////////////NSLog(@"NO format24 %@", [format stringFromDate:[format24 dateFromString:time]]);
            
            time= [format stringFromDate:[format24 dateFromString:time]];
        }
        
        else
        {
            
            ////////////////NSLog(@"YES format24 %@", time);
            // time = [format stringFromDate:[format24 dateFromString:time]];
        }
        
        
    }
    else
        
    {
        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
        
        if (!strsetting)
        {
            
            ////////////////NSLog(@"YES format %@", time);
        }
        
        else
        {
            
            ////////////////NSLog(@"NO format %@", [format24 stringFromDate:[format dateFromString:time]]);
            // time = [format stringFromDate:[format24 dateFromString:time]];
            
            time= [format24 stringFromDate:[format dateFromString:time]];
            
            
            // time = [format stringFromDate:[format24 dateFromString:time]];
        }
        
        
        
        
    }
    
    
    
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    
    NSDateFormatter *localTimeZoneFormatter = [NSDateFormatter new];
    localTimeZoneFormatter.timeZone = [NSTimeZone localTimeZone];
    localTimeZoneFormatter.dateFormat = @"Z";
    
    
    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    
    NSString *When =[[dateFormat stringFromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:[send valueForKey:@"Starting"]], time]]] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    
    
    
    NSString *End;
    
    if([[send valueForKey:@"Duration"]isEqualToString:@"Nooit"]||[[send valueForKey:@"Duration"] isEqualToString:@"Never"]||[[send valueForKey:@"Duration"]isEqualToString:@"Nie"]||[[send valueForKey:@"Duration"] isEqualToString:@"Jamais"]||[[send valueForKey:@"Duration"]isEqualToString:@"Ποτέ"])
        
    {
        
        End =@"1970-01-01T00:00:00";
    }
    else
        
    {
        End =[[dateFormat stringFromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [send valueForKey:@"Duration"], time]]] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    }
    
    //----<
    if ([[self changeNumbers:[send valueForKey:@"Dose_unit"]] isEqualToString:@"-"]) {
        
        //03-8-2017
        
        NSString *soapMessage = [NSString stringWithFormat:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                                 "<soapenv:Header/>"
                                 "<soapenv:Body>"
                                 "<tem:insertDrug>"
                                 "<tem:myDrug>"
                                 "<dos:Directions>%@</dos:Directions>"
                                 //Directions
                                 "<dos:DoseAmount>%f</dos:DoseAmount>"
                                 "<dos:DoseStrength>%f</dos:DoseStrength>"
                                 "<dos:DoseStrengthUnitType>%i</dos:DoseStrengthUnitType>"
                                 "<dos:DrugID>%@</dos:DrugID>"
                                 "<dos:DrugName>%@</dos:DrugName>"
                                 "<dos:DrugTakeTypeID>1</dos:DrugTakeTypeID>"
                                 "<dos:DrugTypeID>%i</dos:DrugTypeID>"
                                 "<dos:EndDate>%@</dos:EndDate>"
                                 "<dos:FrequencyText>%@</dos:FrequencyText>"
                                 
                                 "<dos:PatientID>%@</dos:PatientID>"
                                 "<dos:ProfileID>%@</dos:ProfileID>"
                                 "<dos:ScheduleFrequencyID>1</dos:ScheduleFrequencyID>"
                                 "<dos:StartDate>%@</dos:StartDate>"
                                 "<dos:Ticket>%@</dos:Ticket>"
                                 "<dos:TimesText>%@</dos:TimesText>"
                                 "</tem:myDrug>"
                                 "</tem:insertDrug>"
                                 "</soapenv:Body>"
                                 "</soapenv:Envelope>",
                                 [send valueForKey:@"Instructions"],
                                 [[send valueForKey:@"Quantity"] floatValue],
                                 [[send valueForKey:@"Strength"] floatValue],
                                 [[self changeNumbers:[send valueForKey:@"Strength_unit"]] intValue],
                                 
                                 [send valueForKey:@"ID_Medicine"],
                                 [send valueForKey:@"Name"],
                                 [[self changeIDMed:[send valueForKey:@"Type"]]intValue],
                                 End,
                                 [NSString stringWithFormat:@"%@ %@", [[self changeType:NSLocalizedString([send valueForKey:@"Frequency_text"],nil)] stringByReplacingOccurrencesOfString:@"X" withString:[send valueForKey:@"Frequency"]], [self changeDays:[send valueForKey:@"Day_number"]]],
                                 myApp.strApplicationUUID,
                                 [send valueForKey:@"ID"],
                                 
                                 When,
                                 [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"], [self changeTimes:[send valueForKey:@"Times"]]];
        
        //To suppress the leak in NSXMLParser
        NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/IDosingWS/insertDrug" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:
         [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        
       //NSLog(@"%@", soapMessage);
        
//        NSURLSession *session = [NSURLSession sharedSession];
//        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:theRequest
//                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
//                                          {
//                                              // do something with the data
//                                          }];
//        [dataTask resume];

        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        webData = [[NSMutableData data] retain];
         
        
    }
    else
    {
        //03-08-2017
        
        NSString *soapMessage = [NSString stringWithFormat:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                                 "<soapenv:Header/>"
                                 "<soapenv:Body>"
                                 "<tem:insertDrug>"
                                 "<tem:myDrug>"
                                 "<dos:Directions>%@</dos:Directions>"
                                 "<dos:DoseAmount>%f</dos:DoseAmount>"
                                 "<dos:DoseAmountUnitType>%i</dos:DoseAmountUnitType>"
                                 "<dos:DoseStrength>%f</dos:DoseStrength>"
                                 "<dos:DoseStrengthUnitType>%i</dos:DoseStrengthUnitType>"
                                 "<dos:DrugID>%@</dos:DrugID>"
                                 "<dos:DrugName>%@</dos:DrugName>"
                                 "<dos:DrugTakeTypeID>1</dos:DrugTakeTypeID>"
                                 "<dos:DrugTypeID>%i</dos:DrugTypeID>"
                                 "<dos:EndDate>%@</dos:EndDate>"
                                 "<dos:FrequencyText>%@</dos:FrequencyText>"
                                 "<dos:PatientID>%@</dos:PatientID>"
                                 "<dos:ProfileID>%@</dos:ProfileID>"
                                 "<dos:ScheduleFrequencyID>1</dos:ScheduleFrequencyID>"
                                 "<dos:StartDate>%@</dos:StartDate>"
                                 "<dos:Ticket>%@</dos:Ticket>"
                                 "<dos:TimesText>%@</dos:TimesText>"
                                 "</tem:myDrug>"
                                 "</tem:insertDrug>"
                                 "</soapenv:Body>"
                                 "</soapenv:Envelope>",
                                 [send valueForKey:@"Instructions"],
                                 [[send valueForKey:@"Quantity"] floatValue],
                                 [[self changeNumbers:[send valueForKey:@"Dose_unit"]] intValue],
                                 [[send valueForKey:@"Strength"] floatValue],
                                 [[self changeNumbers:[send valueForKey:@"Strength_unit"]] intValue],
                                 
                                 [send valueForKey:@"ID_Medicine"],
                                 [send valueForKey:@"Name"],
                                 [[self changeIDMed:[send valueForKey:@"Type"]]intValue],
                                 End,
                                 [NSString stringWithFormat:@"%@ %@", [[self changeType:NSLocalizedString([send valueForKey:@"Frequency_text"],nil)] stringByReplacingOccurrencesOfString:@"X" withString:[send valueForKey:@"Frequency"]], [self changeDays:[send valueForKey:@"Day_number"]]],
                                 myApp.strApplicationUUID,
                                 [send valueForKey:@"ID"],
                                 
                                 When,
                                 [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"], [self changeTimes:[send valueForKey:@"Times"]]];
        
        //To suppress the leak in NSXMLParser
        NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/IDosingWS/insertDrug" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:
         [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
          //NSLog(@"%@", soapMessage);
        
        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        webData = [[NSMutableData data] retain];
        
    }
    
    
}




-(NSString*)changeText:(NSString*)send

{
    NSString * strippedNumber = [send stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [send length])];
    
    
    
    
    return strippedNumber;
    
}

-(NSString*)changeNumbers:(NSString*)send

{
    
    
    
    
    
    NSString * goOn =@"-";
    
    
    
    
    NSArray *names1 =[NSArray arrayWithObjects:@"%,%,1",@"IU,IU,2",@"mcg,mcg,3",@"mg/mL,mg/mL,4",@"g/mL,g/mL,5",@"mg,mg,6",@"mg/L,mg/L,7",@"units/mL,units/mL,8",@"IU/mL,IU/mL,9",@"mg/cc,mg/cc,10",@"units/cc,units/cc,11",@"IU/cc,IU/cc,12",@"mg/tsp,mg/tsp,13",@"mg/tbsp,mg/tbsp,14",@"mg/oz,mg/oz,15",@"mg/hr,mg/hr,16",@"mcg/hr,mcg/hr,17",@"mg/24hr,mg/24hr,18",@"g,g,19",@"units,units,20",@"meq,meq,21",@"mL,mL,22",@"cc,cc,23",@"tsp,tsp,24",@"tbsp,tbsp,25",@"oz,oz,26", nil];
    
    for (NSInteger i=0 ; i<[names1 count]; i++) {
        
        
        NSArray *numbers2 = [[names1 objectAtIndex:i] componentsSeparatedByString:@","];
        
        
        
        if ([[NSLocalizedString([numbers2 objectAtIndex:0],nil)lowercaseString]  isEqualToString:[send lowercaseString]]) {
            
            
            goOn = [numbers2 objectAtIndex:2];
            
            
        }
    }
    
    
    ////////////////NSLog(@"changeNumbers %@", goOn);
    
    return goOn;
    
    
    
}


-(NSString*)changeTimes:(NSString*)send

{
    ////NSLog(@"send %@", send);
    
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSArray *numbers2 = [send componentsSeparatedByString:@","];
    
    
    NSMutableString *mainString=[[NSMutableString alloc]initWithString:@""];
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    [dateFormat1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat1 setDateFormat:[appDelegate convertString:@"hh:mm a"]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:[appDelegate convertString:@"HH:mm"]];
    
    

    for (NSInteger i=0 ; i<[numbers2 count]; i++) {
        
       
        
        if ([[numbers2 objectAtIndex:i] rangeOfString:@"pm"].location != NSNotFound||[[numbers2 objectAtIndex:i] rangeOfString:@"p.m."].location != NSNotFound) {
            
            
            
            if ([[numbers2 objectAtIndex:i] rangeOfString:@"am"].location != NSNotFound||[[numbers2 objectAtIndex:i] rangeOfString:@"a.m."].location != NSNotFound) {
                
                if (i==0) {
                    
                    [mainString appendFormat:[NSString stringWithFormat:@"%@", [numbers2 objectAtIndex:i]]];
                    
                    
                     // ////NSLog(@"send %@", mainString);
                    
                }
                else
                    
                {
                    [mainString appendFormat:[NSString stringWithFormat:@",%@", [numbers2 objectAtIndex:i]]];
                    
                    
                     //////NSLog(@"send %@", mainString);
                    
                }
                
            }
            else
                
            {
                if (i==0) {
                    
                    [mainString appendFormat:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:[dateFormat1 dateFromString:[numbers2 objectAtIndex:i]]]]];
                    
                    
                    
                    ////NSLog(@"send %@", mainString);
                    
                }
                else
                    
                {
                    [mainString appendFormat:[NSString stringWithFormat:@",%@", [dateFormat stringFromDate:[dateFormat1 dateFromString:[numbers2 objectAtIndex:i]]]]];
                    
                    
                    ////NSLog(@"send %@", mainString);
                    
                }
                
                
            }
            
        }
        else
            
        {
            
            if (i==0) {
                
                [mainString appendFormat:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:[dateFormat1 dateFromString:[numbers2 objectAtIndex:i]]]]];
                
            }
            else
                
            {
                [mainString appendFormat:[NSString stringWithFormat:@",%@", [dateFormat stringFromDate:[dateFormat1 dateFromString:[numbers2 objectAtIndex:i]]]]];
                
            }
            
        }
        
        
    }
    

    
    return mainString;
    
}




-(NSString*)changeIDMed:(NSString*)send

{
    
    NSString * strippedNumber=@"7";
    
       NSArray *names1 =[NSArray arrayWithObjects:@"Drop,1",@"Drops,1",@"Inhaler,2",@"Inhalations,2", @"Injection,3",@"Injections,3",@"Liquid,4",@"Ointment,5",@"Patches,6",@"Patch,6",@"Tablet,7",@"Tablets,7",@"Sprays,8",@"Spray,8", @"Other,9",nil];
    
    for (NSInteger i=0 ; i<[names1 count]; i++) {
        
        
        NSArray *numbers2 = [[names1 objectAtIndex:i] componentsSeparatedByString:@","];
        
        if ([NSLocalizedString([numbers2 objectAtIndex:0],nil) isEqualToString:send]) {
            
            
            strippedNumber = [numbers2 objectAtIndex:1];
            
            
        }
        
        
        
        
    }
    
    
    
    return strippedNumber;
    
}


-(NSString*)changeType:(NSString*)send

{
    
    
    NSString * strippedNumber=@"";
    
    NSArray *names1 =[NSArray arrayWithObjects:
                      @"Daily,Daily",
                      @"Weekly,Weekly",
                      @"Every X Days,Every X Days",
                      nil];
    
    
    for (NSInteger i=0 ; i<[names1 count]; i++) {
        
        NSArray *timesIt = [[names1 objectAtIndex:i] componentsSeparatedByString:@","];
        
        if ([NSLocalizedString([timesIt objectAtIndex:0],nil) isEqualToString:send]) {
            strippedNumber =[timesIt objectAtIndex:1];
            
            
            
        }
        
        
        
    }
    
    
    
    
    return strippedNumber;
    
}


-(NSString*)changeDays:(NSString*)send

{
    
    
    NSMutableString *mainString=[[NSMutableString alloc]initWithString:@""];
    
    
    
    NSArray *timesDays = [send componentsSeparatedByString:@","];
    
    for (NSInteger i=0 ; i<[timesDays count]; i++) {
        
        
        NSArray *names1 =[NSArray arrayWithObjects:@"Su,1",@"Mo,2",@"Tu,3",@"We,4",@"Th,5",@"Fr,6",@"Sa,7", nil];
        
        
        for (NSInteger k=0 ; k<[names1 count]; k++) {
            
            
            NSArray *Days = [[names1 objectAtIndex:k] componentsSeparatedByString:@","];
            
            if ([[timesDays objectAtIndex:i] isEqualToString:NSLocalizedString([Days objectAtIndex:1], nil)]) {
                
          
                
                if (i ==0) {
                    
                    [mainString appendFormat:[NSString stringWithFormat:@"%@",[Days objectAtIndex:0]]];
                }
                else
                    
                {
                    
                    [mainString appendFormat:[NSString stringWithFormat:@",%@",[Days objectAtIndex:0]]];
                }
                
                
                
            }
            
        }
        
        
    }
    
    
    
    
    
    return mainString;
    
}







-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [webData setLength: 0];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [webData appendData:data];
    
    unsigned char byteBuffer[[webData length]];
    [webData getBytes:byteBuffer length:[webData length]];
	
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{

    xmlParser = [[NSXMLParser alloc] initWithData: webData];
    [xmlParser setDelegate: self];
    [xmlParser setShouldResolveExternalEntities: YES];
    [xmlParser parse];//start parsing
    
    
    
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
            myApp.chosenCourse.send =@"NO";
    
}

-(void) parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
{
    NSString *temp = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];
    
    
    for (NSInteger i =0; i < [setNames count]; i++) {
        
        if ([currentElement isEqualToString:[setNames objectAtIndex:i]]) {
            
            
            [[Setcontent objectAtIndex:i] appendString:temp];
        }
    }
    
    
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    
    currentElement = [elementName copy];
    if ([elementName isEqualToString:setName]) {
        
        item = [[NSMutableDictionary alloc] init];
        [Setcontent removeAllObjects];
        
        for (NSInteger i =0; i < [setNames count]; i++) {
            
            NSMutableString *Setit= [[NSMutableString alloc] init];
            [Setcontent addObject:Setit];
            
        }
        
    }
    
    
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
	
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if ([elementName isEqualToString:setName]) {
        
        
        for (NSInteger i =0; i < [setNames count]; i++) {
            
            [item setObject:[Setcontent objectAtIndex:i] forKey:[setNames objectAtIndex:i]];
            
            
        }
        
        [citys addObject:item];
        
       
        myApp.chosenCourse.send =@"YES";
        
        
        
        [item release];
		
    }
	
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
   //NSLog(@"%@", string);
    
    string = [string stringByReplacingOccurrencesOfString: @"\"" withString:@""];
    string = [string stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    string = [string stringByReplacingOccurrencesOfString: @"\t" withString:@""];
    
    
    
    for (NSInteger i =0; i < [setNames count]; i++) {
        
        if ([currentElement isEqualToString:[setNames objectAtIndex:i]]) {
            
            [[Setcontent objectAtIndex:i] appendString:string];
            
            
            AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            
            appDelegate.currentHistory.send =@"YES";
            
        }
        
        
        
        
    }
    
}



-(void)parser:(NSXMLParser*)myParser {
    
    NSAutoreleasePool *schedulePool = [[NSAutoreleasePool alloc] init];
    
    BOOL success = [myParser parse];
    
    if(success) {
        
        
        
    } else {
        
        
        
    }
    
    [schedulePool drain];
    
    [myParser release];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    
    
}




-(NSString *) change: (NSString *) dumb
{
    if ([dumb isEqualToString:@""]) {
        
        dumb=@"-";
        
        return dumb;
        
    }
    
    else
    {
        
        return dumb;
        
    }
    
}





@end
