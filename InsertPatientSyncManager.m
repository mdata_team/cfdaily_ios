//
//  MedSyncManager.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//




#import "InsertPatientSyncManager.h"
#import "AppDelegate.h"
#import "Profiles.h"

@implementation InsertPatientSyncManager 
@synthesize Setcontent;
@synthesize setNames;
@synthesize citys;
@synthesize Result;
@synthesize theConnection;
@synthesize currentElement;
@synthesize item;
@synthesize count;
@synthesize setName;
@synthesize term;
@synthesize webData;
@synthesize xmlParser;


-(InsertPatientSyncManager*)init{
	self = [super init];
	if(self){
     
        
    }
    
        return self;
}

-(void)insertPatientNew:(NSString*)termit with:(Profiles*)send;

{
    
 
  
    Setcontent = [[NSMutableArray alloc] init];
    citys = [[NSMutableArray alloc] init];
    setName=@"insertPatientResponse";
    
    setNames = [[NSMutableArray alloc] initWithObjects:@"insertPatientResult", nil];
    
    
    UIDevice *device = [UIDevice currentDevice];
    
      NSDate *now = [[NSDate alloc] init];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    
   
    NSDateFormatter *localTimeZoneFormatter = [NSDateFormatter new];
    localTimeZoneFormatter.timeZone = [NSTimeZone localTimeZone];
    localTimeZoneFormatter.dateFormat = @"Z";
    NSString *localTimeZoneOffset = [localTimeZoneFormatter stringFromDate:now];
    


    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    ////////////////NSLog(@"%@",  [[NSUserDefaults standardUserDefaults] valueForKey: @"countryCode"]);
    ////////////////NSLog(@"%@",  [[NSUserDefaults standardUserDefaults] valueForKey: @"Language"]);
    ////////////////NSLog(@"%@", [[NSUserDefaults standardUserDefaults] valueForKey: @"Continent"]);
  
    //03-08-2017
 /*
    NSString *soapMessage = [NSString stringWithFormat:
 
                             @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dos=\"http://schemas.datacontract.org/2004/07/DosingService\">"
                             "<soapenv:Header/>"
                             "<soapenv:Body>"
                             "<tem:insertPatient>"
                             "<tem:myPatient>"
                             "<dos:Country>%@</dos:Country>"
                             "<dos:CreateDate>%@</dos:CreateDate>"
                             "<dos:DeviceName>%@</dos:DeviceName>"
                             "<dos:Language>%@</dos:Language>"
                             "<dos:LastSeenDate>%@</dos:LastSeenDate>"
                             "<dos:PatientID>%@</dos:PatientID>"
                             "<dos:ProfileID>%@</dos:ProfileID>"
                             "<dos:Region>%@</dos:Region>"
                             "<dos:Ticket>%@</dos:Ticket>"
                             "<dos:TimeZone>%@</dos:TimeZone>"
                             "<dos:TimeZoneOffsetGMT>%@</dos:TimeZoneOffsetGMT>"
                             "</tem:myPatient>"
                             "</tem:insertPatient>"
                             "</soapenv:Body>"
                             "</soapenv:Envelope>", [[NSUserDefaults standardUserDefaults] valueForKey: @"countryCode"], [[dateFormat stringFromDate:now] stringByReplacingOccurrencesOfString:@" " withString:@"T"], [device model], [[NSUserDefaults standardUserDefaults] valueForKey:@"Language"], [[dateFormat stringFromDate:now] stringByReplacingOccurrencesOfString:@" " withString:@"T"], myApp.strApplicationUUID,send.id, [[NSUserDefaults standardUserDefaults] valueForKey: @"Continent"], [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"],localTimeZoneOffset, localTimeZoneOffset];
    
    
    //////NSLog(@"%@", soapMessage);
    
    //To suppress the leak in NSXMLParser
    NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://tempuri.org/IDosingWS/insertPatient" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:
     [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    

    
    
    theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
   webData = [[NSMutableData data] retain];
    
   */
}


-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	
    [webData setLength: 0];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [webData appendData:data];
    
    unsigned char byteBuffer[[webData length]];
    [webData getBytes:byteBuffer];
	
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
 
  
    xmlParser = [[NSXMLParser alloc] initWithData: webData];
    [xmlParser setDelegate: self];
    [xmlParser setShouldResolveExternalEntities: YES];
    [xmlParser parse];//start parsing
    

   
    
    
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
     myApp.currentProfile.send =@"NO";
 
}

-(void) parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
{
    NSString *temp = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];

    
  
   for (NSInteger i =0; i < [setNames count]; i++) {
        
        if ([currentElement isEqualToString:[setNames objectAtIndex:i]]) {
            
            
            [[Setcontent objectAtIndex:i] appendString:temp];
            
            
        }
    }
    
    
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    
    currentElement = [elementName copy];
    if ([elementName isEqualToString:setName]) {
        
        item = [[NSMutableDictionary alloc] init];
        [Setcontent removeAllObjects];
        
       for (NSInteger i =0; i < [setNames count]; i++) {
            
            NSMutableString *Setit= [[NSMutableString alloc] init];
            [Setcontent addObject:Setit];
            
        }
        
    }
    
    
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
	
    
    
    if ([elementName isEqualToString:setName]) {
        
        
       for (NSInteger i =0; i < [setNames count]; i++) {
            
            [item setObject:[Setcontent objectAtIndex:i] forKey:[setNames objectAtIndex:i]];
            
            
        }
        
        [citys addObject:item];
        
        
        ////////////////NSLog(@"citys %@",citys);
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        if ([[[citys objectAtIndex:0] valueForKey:@"insertPatientResult"] rangeOfString:@"succes"].location != NSNotFound) {
           
            
            myApp.currentProfile.send =@"NO";
            
            
        }
        else
            
        {
        
        myApp.currentProfile.send =@"YES";
            
        }
        
        
        ////////////NSLog(@"currentProfile %@", myApp.currentProfile.send);
        
        [item release];
		
    }
	
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    
    
    string = [string stringByReplacingOccurrencesOfString: @"\"" withString:@""];
    string = [string stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    string = [string stringByReplacingOccurrencesOfString: @"\t" withString:@""];
    
    
    
   for (NSInteger i =0; i < [setNames count]; i++) {
        
        if ([currentElement isEqualToString:[setNames objectAtIndex:i]]) {
            
            [[Setcontent objectAtIndex:i] appendString:string];
            
            
        }
        
        
        
        
    }
    
}

-(void)parser:(NSXMLParser*)myParser {
    
    NSAutoreleasePool *schedulePool = [[NSAutoreleasePool alloc] init];
    
    BOOL success = [myParser parse];
    
    if(success) {
        
        
        
    } else {
        
        
        
    }
    
    [schedulePool drain];
    
    [myParser release];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    

    
  
    
}




-(NSString *) change: (NSString *) dumb
{
    if ([dumb isEqualToString:@""]) {
        
        dumb=@"-";
        
        return dumb;
        
    }
    
    else
    {
        
        return dumb;
        
    }
    
}





@end
