//
//  PickerView.m
//  Vertex-Dental
//
//  Created by Jeffrey Snijder on 28-02-11.
//  Copyright 2011 Livecast. All rights reserved.
//

#import "IntegerPicker.h"
#import "StrengthViewControllerScroll.h"
#import "GetData.h"
#import "AppDelegate.h"


@implementation IntegerPicker
@synthesize setLocation;
@synthesize number;
@synthesize countit;
@synthesize docelist;
@synthesize placeArray;
@synthesize Parantit;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
  
        
        
        placeArray = [[NSMutableArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
       
  
        
    }
    return self;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
   
 
    NSMutableString *mainString=[[NSMutableString alloc]initWithString:@""];
    
    BOOL Tekst =NO;
    
   for (NSInteger i=0 ; i<countit; i++) {
        
        NSInteger newRow = [pickerView selectedRowInComponent:i];
        
          NSString *string= [placeArray objectAtIndex:newRow];
        
        
       Tekst =YES;
        
        if (i ==number) {
          
            [mainString appendFormat:@"%@",@"."];
        }
      
        else
            
        {
            if (Tekst) {
               
                   [mainString appendFormat:@"%@",string];
            }
      
     
            
        }
       
        
    }
    
    
    [Parantit chosenValue:[self floatToString:mainString]];
   
   
 
    
}


- (NSString *) floatToString:(NSString*) val {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    

    
    NSArray *numbers2 = [val componentsSeparatedByString:@"."];
    
    if ([numbers2 count]==2)
        
    {
        NSString *ret = [NSString stringWithFormat:@"%@", [numbers2 objectAtIndex:1]];
        NSInteger index = (int)[ret length] - 1;
        BOOL trim = FALSE;
        while (
               ([ret characterAtIndex:index] == '0' ||
                [ret characterAtIndex:index] == '.')
               &&
               index > 0)
        {
            index--;
            trim = TRUE;
        }
        if (trim)
            ret = [ret substringToIndex: index +1];
        
        
        if ([ret intValue]>0)
            
            
            return [NSString stringWithFormat:@"%i.%@ %@",[[numbers2 objectAtIndex:0] intValue],ret,  appDelegate.strength_unit];
        
        else
            
            return [NSString stringWithFormat:@"%i %@",[[numbers2 objectAtIndex:0] intValue], appDelegate.strength_unit];
        
    }
    
    else
    {       NSString *ret = [NSString stringWithFormat:@"%@", val];
        return [NSString stringWithFormat:@"%i",[ret intValue]];
    }
}



- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	CGFloat componentWidth;
	componentWidth = 30;	// first column size is wider to hold names
	return componentWidth;
}



-(void) viewDidDisappear:(BOOL)animated
{
}

- (NSString*)pickerView:(UIPickerView*)pv titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [placeArray objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
	UILabel *retval = (id)view;
	if (!retval) {
		retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
	}
    
    if (component ==number) {
        retval.text = @".";
        retval.font = [UIFont boldSystemFontOfSize:15];
        retval.textAlignment = NSTextAlignmentCenter;
        retval.backgroundColor = [UIColor clearColor];
    }
    else
    {
    
	retval.text = [placeArray objectAtIndex:row];
	retval.font = [UIFont boldSystemFontOfSize:15];
	retval.textAlignment = NSTextAlignmentCenter;
	retval.backgroundColor = [UIColor clearColor];
        
    }
	
	return retval;
}

-(void) geloadContenst:(NSInteger)set numbers:(NSInteger)count getparant:(StrengthViewControllerScroll*) send
{
    self.delegate = nil;
    number = set;
    countit =count;
    
    

    Parantit = (StrengthViewControllerScroll*)send;
    
    number = set;
    countit =count;

    
    
}

#pragma mark UIPickerViewDataSource methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pv
{
    return countit;
}

- (NSInteger)pickerView:(UIPickerView*)pv numberOfRowsInComponent:(NSInteger)component
{
    
    if (component ==number) {
        return 1;
    }
    else
    {
         return [placeArray count];
    }
  
}







@end
