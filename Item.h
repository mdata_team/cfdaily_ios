//
//  Item.h
//  Monster
//
//  Created by Jeffrey Snijder on 20-04-12.
//  Copyright (c) 2012 Livecast.nl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface Item : UIImageView <UIGestureRecognizerDelegate>
{
    CGFloat lastScale;
	CGFloat lastRotation;  
	CGFloat firstX;
	CGFloat firstY;
    NSInteger tagThis;
    NSMutableArray *colors;
    NSInteger count;
    NSMutableArray *integerArray;
    NSInteger countBack;
    CAShapeLayer *marque;
    UIImageView *photoImage;
    BOOL isDrawing;
    NSInteger countIt;
    NSInteger lijndikte;
    BOOL dragging;
    NSInteger setMovement;
    CGPoint lastPoint;
    NSInteger lastMovement;
    NSMutableArray *DropsinArray;
    NSMutableArray *points;
    NSInteger Colorinteger;
    UIButton *Messure;
    UIView *Parantit;
     UIView *parantExtra;
    UIViewController *parantViewController;
    UIPinchGestureRecognizer *pinchRecognizer;
    UIRotationGestureRecognizer *rotationRecognizer;
    UIPanGestureRecognizer *panRecognizer;
    UITapGestureRecognizer *tapProfileImageRecognizer;
    UIButton *SaveButton;
    UIButton *CloseButton;
    BOOL Lock;
    
    NSString *mask;
    NSString *imageName;
}

@property (nonatomic, retain) NSMutableArray *DropsinArray;
@property (nonatomic, assign, getter=isDragging) BOOL dragging;
@property (nonatomic, assign) CGPoint lastPoint;
@property (nonatomic, assign) NSInteger lijndikte;
@property (nonatomic, retain) NSMutableArray *points;
@property (nonatomic, retain) UIImage *Setimage;
@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic, assign) NSInteger countIt;
@property (nonatomic, assign) NSInteger setMovement;
@property (nonatomic, assign) NSInteger lastMovement;
@property (nonatomic, assign) unsigned char *rawData;
@property (nonatomic, retain) IBOutlet UIImageView * tekening;
@property (nonatomic, assign) NSUInteger bytesPerPixel;
@property (nonatomic, assign) NSUInteger bytesPerPixelBall;
@property (nonatomic, assign) NSUInteger bytesPerRow;
@property (nonatomic, retain) NSString *mask;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, assign) CGFloat lastScale;
@property (nonatomic, assign) CGFloat lastRotation;
@property (nonatomic, assign) CGFloat firstX;
@property (nonatomic, assign) CGFloat firstY;
@property (nonatomic, assign) NSInteger tagThis;
@property (nonatomic, retain) NSMutableArray *colors;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, retain) NSMutableArray *integerArray;
@property (nonatomic, assign) NSInteger countBack;
@property (nonatomic, retain) CAShapeLayer *marque;
@property (nonatomic, retain) IBOutlet UIImageView *photoImage;
@property (nonatomic, assign) NSInteger Colorinteger;
@property (nonatomic, retain) IBOutlet UIButton *Messure;
@property (nonatomic, retain) IBOutlet UIView *Parantit;
@property (nonatomic, retain) IBOutlet UIView *parantExtra;
@property (nonatomic, retain) UIViewController *parantViewController;
@property (nonatomic, retain) UIPinchGestureRecognizer *pinchRecognizer;
@property (nonatomic, retain) UIRotationGestureRecognizer *rotationRecognizer;
@property (nonatomic, retain) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic, retain) UITapGestureRecognizer *tapProfileImageRecognizer;
@property (nonatomic, assign, getter=isLock) BOOL Lock;
-(void) tagImage:(NSInteger)tagNow imagIt:(UIImage*) set;
-(void) getParent:(UIView*) sender;
-(void) getParentExtra:(UIView*) sender;
    //getParentExtra
-(void) getParentController:(UIViewController*) sender;
@end
