//
//  Item.m
//  Monster
//
//  Created by Jeffrey Snijder on 20-04-12.
//  Copyright (c) 2012 Livecast.nl. All rights reserved.
//

#import "Item.h"

@implementation Item
@synthesize lastScale;
@synthesize lastRotation;
@synthesize firstX;
@synthesize firstY;
@synthesize tagThis;
@synthesize colors;
@synthesize count;
@synthesize integerArray;
@synthesize countBack;
@synthesize marque;
@synthesize photoImage;
@synthesize countIt;
@synthesize lijndikte;
@synthesize dragging;
@synthesize setMovement;
@synthesize lastPoint;
@synthesize lastMovement;
@synthesize DropsinArray;
@synthesize points;
@synthesize Colorinteger;
@synthesize Messure;
@synthesize Parantit;
@synthesize parantViewController;
@synthesize pinchRecognizer;
@synthesize rotationRecognizer;
@synthesize panRecognizer;
@synthesize tapProfileImageRecognizer;
@synthesize Lock;
@synthesize mask;
@synthesize imageName;
@synthesize parantExtra;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    #define IPAD_IT 0
       
        Lock= NO;



        DropsinArray = [[NSMutableArray alloc] initWithObjects:[UIColor colorWithRed:0.875 green:0.161 blue:0.529 alpha:1.000], [UIColor colorWithRed:0.875 green:0.161 blue:0.529 alpha:1.000], nil];




        lijndikte = 5;
        lijndikte = 5;

        lastMovement=0;
        countIt =0;
   

        pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scale:)];
        [pinchRecognizer setDelegate:self];
        [self addGestureRecognizer:pinchRecognizer];
        
        rotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotate:)];
        [rotationRecognizer setDelegate:self];
        [self addGestureRecognizer:rotationRecognizer];
        
        panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
        [panRecognizer setMinimumNumberOfTouches:1];
        [panRecognizer setMaximumNumberOfTouches:1];
        [panRecognizer setDelegate:self];
        [self addGestureRecognizer:panRecognizer];
        
        tapProfileImageRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [tapProfileImageRecognizer setNumberOfTapsRequired:1];
        [tapProfileImageRecognizer setDelegate:self];
        [self addGestureRecognizer:tapProfileImageRecognizer];
        
        
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] 
                                              initWithTarget:self 
                                              action:@selector(userDidDoubleTap:)];
        recognizer.numberOfTapsRequired = 2;
        [self addGestureRecognizer:recognizer];


        
     
     
        
        
    }
    return self;
}



-(void) delete:(UIButton*)sender
{
    
  
    
    [parantViewController delete:sender];
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
	[super touchesBegan:touches withEvent:event];
    
  
   
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event];
   

    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
   
      
}

- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
	[super touchesEnded:touches withEvent:event];
 
}

-(void) getParent:(UIView*) sender
{
    Parantit =sender; 
    
}

-(void) getParentController:(UIViewController*) sender
{
    parantViewController =sender; 
}

-(void) tagImage:(NSInteger)tagNow imagIt:(UIImage*) set
{

    [photoImage setImage:set];
    [photoImage setTag:tagNow];
    

    
}


-(void)showOverlayWithFrame:(CGRect)frame {
    
    
    if (![marque actionForKey:@"linePhase"]) {
        CABasicAnimation *dashAnimation;
        dashAnimation = [CABasicAnimation animationWithKeyPath:@"lineDashPhase"];
        [dashAnimation setFromValue:[NSNumber numberWithFloat:0.0f]];
        [dashAnimation setToValue:[NSNumber numberWithFloat:15.0f]];
        [dashAnimation setDuration:0.5f];
        [dashAnimation setRepeatCount:HUGE_VALF];
        [marque addAnimation:dashAnimation forKey:@"linePhase"];
    }
   
    marque.bounds = CGRectMake(frame.origin.x, frame.origin.y, 0, 0);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, frame);
    [marque setPath:path];
    CGPathRelease(path);
    
    marque.hidden = NO;
    
    
    
}

-(void)scale:(id)sender {
    
    if (Lock) {
        
    }
    else {
    
 
    if([(UIPinchGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        lastScale = 1.0;
       
      
    }
    else {
       
    }
    
    CGFloat scale = 1.0 - (lastScale - [(UIPinchGestureRecognizer*)sender scale]);

    CGAffineTransform currentTransform = self.transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, scale, scale);
    [self setCenter:CGPointMake(self.center.x, self.center.y)];
    [self setTransform:newTransform];
    
    lastScale = [(UIPinchGestureRecognizer*)sender scale];
    //[self showOverlayWithFrame:self.frame];
    if([(UIPinchGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        

        
         self.layer.shadowOffset = CGSizeMake(320/self.frame.size.height, 0);
    }
        
    }
      
}

-(void) getParentExtra:(UIView*) sender

{
    parantExtra =sender;

}





-(void)rotate:(id)sender {
    
    if (Lock) {
        
    }
    else {
        
    if([(UIRotationGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        
      
        lastRotation = 0.0;
        return;
    }
    else {
        
    }
    
    CGFloat rotation = 0.0 - (lastRotation - [(UIRotationGestureRecognizer*)sender rotation]);
    
    CGAffineTransform currentTransform = self.transform;
    CGAffineTransform newTransform = CGAffineTransformRotate(currentTransform,rotation);
    
    [self setTransform:newTransform];
     [self setCenter:CGPointMake(self.center.x, self.center.y)];
    lastRotation = [(UIRotationGestureRecognizer*)sender rotation];
    //[self showOverlayWithFrame:self.frame];
    if([(UIRotationGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        

         self.layer.shadowOffset = CGSizeMake(320/self.frame.size.height, 0);
    }
        
    }
   
}


-(void)move:(UIPanGestureRecognizer *)sender {
    
    if (Lock) {



       
        
        if([sender state] == UIGestureRecognizerStateBegan) {


           

            
        }
        if([sender state] == UIGestureRecognizerStateEnded) {

            
        }



        
    }
    else {
        
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:Parantit];
   if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        firstX = [self center].x;
        firstY = [self center].y;

       
    }
   else {
     
   }
    
    translatedPoint = CGPointMake(firstX+translatedPoint.x, firstY+translatedPoint.y);
     [self setCenter:CGPointMake(self.center.x, self.center.y)];
    [self setCenter:translatedPoint];
    //[self showOverlayWithFrame:self.frame];
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
    
        
           self.layer.shadowOffset = CGSizeMake(320/self.frame.size.height, 0);
        
        
        
    }

    }
}


- (IBAction)userDidDoubleTap:(UITapGestureRecognizer *)recognizer 
{

 
    
}
-(void)tapped:(id)sender {
    marque.hidden = YES;
    
 
      
    
}



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return ![gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && ![gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
