//
//  LeveyPopListView.h
//  LeveyPopListViewDemo
//
//  Created by Levey on 2/21/12.
//  Copyright (c) 2012 Levey. All rights reserved.
//

#import "GzColors.h"

@protocol LeveyPopColorViewDelegate;
@interface LeveyPopColorView : UIView <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *MytableView;
    NSMutableArray *options;
    NSInteger setit;
    BOOL _isModal;
    UIViewController *parent;
    NSInteger Order;
    NSMutableArray *RemoveOradd;
    UIButton *Removebutton;
    NSMutableArray *selectTitle;
    UILabel *Infotext;
}
@property (nonatomic, retain) NSMutableArray *RemoveOradd;
-(void)getParent:(UIViewController*)sender get:(NSInteger)set;
-(void) removeImage:(NSInteger)sender;
-(void)chosenImage:(UIButton*)set;
@property (nonatomic, assign) id<LeveyPopColorViewDelegate> delegate;
@property (assign) BOOL isModal;
- (void)fadeOut;
// The options is a NSArray, contain some NSDictionaries, the NSDictionary contain 2 keys, one is "img", another is "text".
- (id)initWithTitle:(NSString *)aTitle options:(NSMutableArray *)aOptions  setIt:(NSInteger)set;
// If animated is YES, PopListView will be appeared with FadeIn effect.
- (void)showInView:(UIView *)aView animated:(BOOL)animated;
@end

@protocol LeveyPopColorViewDelegate <NSObject>
- (void)leveyPopListView:(LeveyPopColorView *)popListView didSelectedIndex:(NSInteger)anIndex;
- (void)leveyPopListViewDidCancel;
@end