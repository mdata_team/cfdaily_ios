//
//  LeveyPopListView.m
//  LeveyPopListViewDemo
//
//  Created by Levey on 2/21/12.
//  Copyright (c) 2012 Levey. All rights reserved.
//

#import "LeveyPopColorView.h"
#import "LeveyPopColorViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "GzColors.h"

@interface LeveyPopColorView (private)
- (void)fadeIn;
- (void)fadeOut;
@end

@implementation LeveyPopColorView
@synthesize delegate;
@synthesize isModal = _isModal;
@synthesize RemoveOradd;

#pragma mark - initialization & cleaning up
- (id)initWithTitle:(NSString *)aTitle options:(NSMutableArray *)aOptions setIt:(NSInteger)set
{
    CGRect rect = [[UIScreen mainScreen] applicationFrame];
    if (self = [super initWithFrame:rect])
    {
        
        for (NSInteger i=0 ; i<[[GzColors allColorsToneOrdered] count]; i++) {
        if (fmod(i,3)==0) {
            Order++;
            
        } else {
            // odd num
        }
         }
        
        
        setit =set;
        
        UIButton *CloseButton = [[UIButton alloc] initWithFrame:CGRectMake(250, 40, 30, 30)];
        [CloseButton setShowsTouchWhenHighlighted:YES];
        [CloseButton setBackgroundImage:[[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"close.png" ofType:@""]] stretchableImageWithLeftCapWidth:0.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [CloseButton setTag:6790];
        [CloseButton addTarget:self action:@selector(fadeOut) forControlEvents:UIControlEventTouchUpInside];
        
        CloseButton.alpha =1;
        [self addSubview:CloseButton];
        [CloseButton release];
        
        
        UIButton *info = [[UIButton alloc] initWithFrame:CGRectMake(280, 10, 30, 30)];
        [info setShowsTouchWhenHighlighted:YES];
        [info setBackgroundImage:[[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"info.png" ofType:@""]] stretchableImageWithLeftCapWidth:0.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [info setTag:6790];
        [info addTarget:self action:@selector(Showinfo) forControlEvents:UIControlEventTouchUpInside];
        
        info.alpha =1;
        [self addSubview:info];
        [info release];
        
        RemoveOradd = [[NSMutableArray alloc] init];
        
        self.backgroundColor = [UIColor clearColor];
        options =  [[GzColors allColorsToneOrdered] retain];

        
        MytableView = [[UITableView alloc] initWithFrame:CGRectMake(40,90, 240, 280)];
        MytableView.separatorColor = [UIColor colorWithWhite:0 alpha:.2];
        MytableView.backgroundColor = [UIColor clearColor];
        MytableView.dataSource = self;
        MytableView.delegate = self;
        MytableView.rowHeight =70;
         [MytableView setEditing:NO];
        [self addSubview:MytableView];
        [MytableView release];
        self.isModal = YES;
        
        
        
        Infotext =[[UILabel alloc] initWithFrame:CGRectMake(40,50, 220, 280)];
        Infotext.numberOfLines =99;
        [Infotext setTextColor:[UIColor whiteColor]];
        Infotext.textAlignment =NSTextAlignmentCenter;
        Infotext.backgroundColor = [UIColor colorWithRed:0.498 green:0.498 blue:0.498 alpha:0.5];
        [self addSubview:Infotext];
        [Infotext release];
        [Infotext setAlpha:0];
        
        
        
        

        
   
        
    }
    return self;    
}


-(void)Showinfo
{
    if (Infotext.alpha ==0) {
        
        [Infotext setAlpha:1];
    }
    else
    {
        [Infotext setAlpha:0];
    }
}

-(void) Setremovemodes:(UIButton*)sender
{
      [MytableView reloadData];
}



-(void)AddImage:(UIButton*)sender
{
    
 
}


-(void) removeImage:(NSInteger)sender
{
    

    
  

}


- (void)tableView:(UITableView *)tableView willDisplayCell:(LeveyPopColorViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [cell witindex:indexPath.row items:options];
    [cell getparent:self];
    
    
}



- (void)dealloc
{

    [options release];
    [MytableView release];
    [super dealloc];
}

#pragma mark - Private Methods
- (void)fadeIn
{
    
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];

}
- (void)fadeOut
{


    
    
    [UIView animateWithDuration:.35 animations:^{
        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

#pragma mark - Instance Methods
- (void)showInView:(UIView *)aView animated:(BOOL)animated
{
    [aView addSubview:self];
    if (animated) {
        [self fadeIn];
    }
}

#pragma mark - Tableview datasource & delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Order;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentity = @"PopListViewCell";
    
    LeveyPopColorViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentity];
    if (cell ==  nil) {
        cell = [[[LeveyPopColorViewCell alloc] initWithStyle:UITableViewCellSelectionStyleGray reuseIdentifier:cellIdentity] autorelease];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setTag:[indexPath row]+1];

    return cell;
}


#pragma mark - TouchTouchTouch


-(void)getParent:(UIViewController*)sender get:(NSInteger)set
{
    parent=sender;
    
    
    setit=set;

}


-(void)chosenImage:(UIButton*)set
{
    
 
  
 
    [parent setSelectedColor:set.backgroundColor];
    [self fadeOut];
    
   }

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
 
}

#pragma mark - DrawDrawDraw
- (void)drawRect:(CGRect)rect
{
   CGRect separatorRect = CGRectMake(40, 40 + 10 - 2, 320 - 2 * 40, 2);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // Draw the background with shadow
    CGContextSetShadowWithColor(ctx, CGSizeZero, 6., [UIColor colorWithWhite:0 alpha:.75].CGColor);
   [[UIColor colorWithRed:1.000 green:0.976 blue:0.965 alpha:1.000] setFill];
    
    
    float x = 40;
    float y = 40;
    float width = 240;
    float height = 340;
    CGMutablePathRef path = CGPathCreateMutable();
	CGPathMoveToPoint(path, NULL, x, y + 5);
	CGPathAddArcToPoint(path, NULL, x, y, x + 5, y, 5);
	CGPathAddArcToPoint(path, NULL, x + width, y, x + width, y + 5, 5);
	CGPathAddArcToPoint(path, NULL, x + width, y + height, x + width - 5, y + height, 5);
	CGPathAddArcToPoint(path, NULL, x, y + height, x, y + height - 5, 5);
	CGPathCloseSubpath(path);
	CGContextAddPath(ctx, path);
    CGContextFillPath(ctx);
    CGPathRelease(path);
    
    // Draw the title and the separator with shadow
    CGContextSetShadowWithColor(ctx, CGSizeMake(0, 1), 0.5f, [UIColor whiteColor].CGColor);
    [[UIColor colorWithRed:0.020 green:0.549 blue:0.961 alpha:0] setFill];
    CGContextFillRect(ctx, separatorRect);
}

@end
