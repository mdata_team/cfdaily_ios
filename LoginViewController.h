//
//  LoginViewController.h
//  CFMedcare
//
//  Created by Livecast02 on 11-06-14.
//  Copyright (c) 2014 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, retain) IBOutlet UITextField *Inlogveld;
@property (nonatomic, retain) IBOutlet UIView *Emailme;
@property (nonatomic, retain) IBOutlet UITextField *Emailveld;
@property (nonatomic, retain) IBOutlet UIButton *askMail;
@property (nonatomic, retain) IBOutlet UIButton *Mailitnow;


@end
