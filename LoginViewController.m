//
//  LoginViewController.m
//  CFMedcare
//
//  Created by Livecast02 on 11-06-14.
//  Copyright (c) 2014 Menopur. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "ActionArrow.h"

#define kOFFSET_FOR_KEYBOARD 180

@interface LoginViewController ()


@end

@implementation LoginViewController
@synthesize Inlogveld;
@synthesize Emailme;
@synthesize Emailveld;
@synthesize askMail;
@synthesize Mailitnow;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    
    

    
     [self.view setBackgroundColor:[UIColor colorWithRed:0.886 green:0.937 blue:0.957 alpha:1.000]];
    //320 × 507 pixels
    
    UIImageView *headShtop = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 132/2)];
    [headShtop setBackgroundColor:[UIColor grayColor]];
    [headShtop setTag:130];
    [self.view addSubview:headShtop];
    
    [headShtop setImage:[UIImage imageNamed:@"top.png"]];
    
    UIImageView *headShot = [[UIImageView alloc] initWithFrame:CGRectMake(0, 132/2, 320, 507)];
    [headShot setBackgroundColor:[UIColor grayColor]];
    [headShot setTag:130];
    [self.view addSubview:headShot];
    
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 320, 40)];
    [title setTextColor:[UIColor whiteColor]];
    [title setText:@"Bienvenue"];
    [title setFont:[UIFont boldSystemFontOfSize:30]];
    [title setNumberOfLines:2];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];
    [self.view addSubview:title];

    
    [headShot setImage:[UIImage imageNamed:@"login.png"]];
    
    Inlogveld =[[UITextField alloc]initWithFrame:CGRectMake(40, 355, 240, 44)];
    [Inlogveld setBackgroundColor:[UIColor clearColor]];
    [Inlogveld setText:@""];
    [Inlogveld setTextAlignment:NSTextAlignmentLeft];
    [Inlogveld setTextColor:[UIColor darkGrayColor]];
    [Inlogveld setFont:[UIFont fontWithName:@"MuseoSlab-500" size:14]];
    [Inlogveld setDelegate:self];
    [self.view addSubview:Inlogveld];
    //Inlogveld.returnKeyType = UIReturnKeyDone;
    
    
    ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(110, 410, 100, 40)];
    [NameButton setTitleit:@"enter"];
    [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [NameButton setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:NameButton];
    
    
    
    Mailitnow = [[UIButton alloc]  initWithFrame:CGRectMake(60, 460, 200, 40)];
    [Mailitnow setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [Mailitnow addTarget:self action:@selector(ActionMail:) forControlEvents:UIControlEventTouchUpInside];
    [Mailitnow setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:Mailitnow];
    
    
    
    Emailme = [[UIView alloc]  initWithFrame:CGRectMake(0, self.view.frame.size.height, 320, self.view.frame.size.height)];
    [Emailme setBackgroundColor:[UIColor colorWithRed:0.886 green:0.937 blue:0.957 alpha:1.000]];
    
    


    
  
    
    [headShot setImage:[UIImage imageNamed:@"login.png"]];
    
    [self.view addSubview:Emailme];
    
    
    //320 × 354 pixels
    
    UIImageView *back = [[UIImageView alloc] initWithFrame:CGRectMake(0, 66, 320, 480)];
     [back setImage:[UIImage imageNamed:@"Mail.png"]];
    [back setBackgroundColor:[UIColor grayColor]];
    [back setTag:130];
    [Emailme addSubview:back];
    
    
    
    UIImageView *headShtop2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 132/2)];
    [headShtop2 setBackgroundColor:[UIColor grayColor]];
    [headShtop2 setTag:130];
    [Emailme addSubview:headShtop2];
    
    [headShtop2 setImage:[UIImage imageNamed:@"top.png"]];
    
    
    UIButton *Extra = [[UIButton alloc]  initWithFrame:CGRectMake(10, 25, 40, 40)];
    [Extra setBackgroundColor:[UIColor clearColor]];
    [Extra setTitle:NSLocalizedString(@"Done",nil) forState:UIControlStateNormal];
    [Extra.titleLabel setFont:[UIFont fontWithName:@"MuseoSlab-500" size:16]];
    [Extra setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Extra setTag:140];
    [Extra addTarget:self action:@selector(ActionDown:) forControlEvents:UIControlEventTouchUpInside];
    [Emailme addSubview:Extra];
    
    
    
    
    Emailveld =[[UITextField alloc]initWithFrame:CGRectMake(40, 320, 240, 44)];
    [Emailveld setBackgroundColor:[UIColor clearColor]];
    [Emailveld setText:@""];
    [Emailveld setTextAlignment:NSTextAlignmentLeft];
    [Emailveld setTextColor:[UIColor darkGrayColor]];
    [Emailveld setFont:[UIFont fontWithName:@"MuseoSlab-500" size:14]];
    [Emailveld setDelegate:self];
    [Emailme addSubview:Emailveld];
    //Inlogveld.returnKeyType = UIReturnKeyDone;
    
    
    askMail = [[UIButton alloc]  initWithFrame:CGRectMake(110, 375, 100, 40)];
    [askMail addTarget:self action:@selector(askMail:) forControlEvents:UIControlEventTouchUpInside];
    [askMail setBackgroundColor:[UIColor clearColor]];
    [Emailme addSubview:askMail];
    
    
    UILabel *title2 = [[UILabel alloc] initWithFrame:CGRectMake(80, 20, 160, 40)];
    [title2 setTextColor:[UIColor whiteColor]];
    [title2 setText:@"J’ai oublié mon\nmot de passe"];
    [title2 setNumberOfLines:2];
    [title2 setFont:[UIFont boldSystemFontOfSize:12]];
    [title2 setNumberOfLines:2];
    [title2 setTextAlignment:NSTextAlignmentCenter];
    [title2 setTag:120];
    [title2 setBackgroundColor:[UIColor clearColor]];
    [title2 setNumberOfLines:3];
    [Emailme addSubview:title2];
    
    
    [Emailme setAlpha:1];
   
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)ActionDown:(UIButton*)sender

{
      
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    
    
    [UIView setAnimationDuration:0.5f];
    
    if (Emailme.frame.origin.y==self.view.frame.size.height) {
        
        
        
        
        [Emailme setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
        
        [Emailme setAlpha:1];
        
    }
    else
    {
        
        

        
        [Emailme setFrame:CGRectMake(10, self.view.frame.size.height, 300, self.view.frame.size.height)];
        
        [Emailme setAlpha:1];
        
        
    }
    
    
    [UIView commitAnimations];
    

}


-(void)ActionMail:(UIButton*)sender

{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    
    
    [UIView setAnimationDuration:0.5f];
    
    if (Emailme.frame.origin.y==self.view.frame.size.height) {
        
        
            [Emailveld setText:@""];
        
        [Emailme setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
        
            [Emailme setAlpha:1];
        
    }
    else
    {
        
            [Emailveld setText:@""];
        [Emailme setFrame:CGRectMake(10, self.view.frame.size.height, 300, self.view.frame.size.height)];
        
            [Emailme setAlpha:1];
        
        
    }
    
    
    [UIView commitAnimations];
    
    
}


-(void)askMail:(UIButton*)sender

{
    
    NSArray *timesIt = [Emailveld.text componentsSeparatedByString:@"@"];
    
    
    //////////////NSLog(@"timesIt %@", timesIt);
    
    if ([timesIt count]>1) {
        
   

    
           [Emailveld resignFirstResponder];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.insertPasword GetIDask:Emailveld.text setparent:self];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    
    
    [UIView setAnimationDuration:0.5f];
    
    if (Emailme.frame.origin.y==self.view.frame.size.height) {
        
        
 
          [Emailveld setText:@""];
        [Emailme setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
        
        [Emailme setAlpha:1];
        
    }
    else
    {
        
            [Emailveld setText:@""];
        [Emailme setFrame:CGRectMake(10, self.view.frame.size.height, 300, self.view.frame.size.height)];
        
        [Emailme setAlpha:1];
        
        
    }
    
    
    [UIView commitAnimations];
        
        
    }
    
    else
        
        
    {
        
        
        [Emailveld resignFirstResponder];
        /* // 27-06-2017 changes
        UIAlertView *alertit = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"L’ adresse e-mail saisi est incorrect, veuillez recommencer.",nil)
                                                         delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
        [alertit show];
        */
        
        
        UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"L’ adresse e-mail saisi est incorrect, veuillez recommencer.",nil) preferredStyle:UIAlertControllerStyleAlert];
        [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
            
        });

        
        
    }
    
    
    
}
-(void)Action:(ActionArrow*)sender

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
   
    
    [appDelegate.insertPasword GetID:Inlogveld.text setparent:self];
    
    
        [Inlogveld resignFirstResponder];
    
    if ([Inlogveld.text isEqual:@"testlogin"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"Argree" forKey:@"Loggedin"];
  
        
        [Inlogveld setText:@""];
        [appDelegate.toolbarDown setAlpha:1];
        
       
    }
    else
        
    {
        
        [Inlogveld setText:@""];
        [[NSUserDefaults standardUserDefaults] setObject:@"Not Argree" forKey:@"Loggedin"];
        
    }
    
}

- (IBAction)textFieldFinished:(id)sender
{
    [sender resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)keyboardWillShow {
    
  
    [Mailitnow setAlpha:0];
    
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        
        
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        
        
        [self setViewMovedUp:NO];
        
        
    }
}

-(void)keyboardWillHide {
    

    
      [Mailitnow setAlpha:1];
    
    if (self.view.frame.origin.y >= 0)
    {
        
       
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
       
        [self setViewMovedUp:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
      
      
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        
        
    
       
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    
    
    [UIView commitAnimations];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
