//
//  HistoryCell.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedicineViewController.h"

@class MedicineViewController, colorButton;
@interface MedCell : UITableViewCell 
@property (nonatomic, retain) IBOutlet  MedicineViewController *lostView;
@property (nonatomic, retain) IBOutlet UILabel *Date;
@property (nonatomic, retain) IBOutlet UILabel *Datedone;
@property (nonatomic, retain) IBOutlet NSString *Filling;
@property (nonatomic, retain) IBOutlet UILabel *DateColor;
@property (nonatomic, retain) IBOutlet UILabel *Time;
@property (nonatomic, retain) IBOutlet UILabel *Name;
@property (nonatomic, retain) IBOutlet UILabel *Taken;
@property (nonatomic, retain) IBOutlet colorButton *Book;
@property (nonatomic, retain) IBOutlet colorButton *Take;
@property (nonatomic, retain) IBOutlet colorButton *Postpone;
@property (nonatomic, retain) IBOutlet colorButton *Skip;
@property (nonatomic, assign) IBOutlet NSInteger countthem;
@property (nonatomic, retain) IBOutlet colorButton *edit;
@property (nonatomic, retain) IBOutlet  colorButton *NameButton;
@property (nonatomic, retain) UILocalNotification *curentChoise;

@property (nonatomic, retain) IBOutlet UIImageView *headShot;
@property (nonatomic, retain) IBOutlet NSMutableDictionary *CurrentArray;
-(void)getparent:(UIViewController*)set;
-(void) FillAllItems:(NSMutableDictionary*) sender index:(NSInteger)set;
-(colorButton*)setColor:(colorButton*)sender;
@end
