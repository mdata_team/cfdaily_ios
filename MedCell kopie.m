//
//  HistoryCell.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "MedCell.h"
#import "HistoryViewController.h"
#import "MedicineViewController.h"
#import "GetData.h"
#import "AppDelegate.h"
#import "colorButton.h"
#import <QuartzCore/QuartzCore.h>
#import "Notification.h"
#import "SaveData.h"

@implementation MedCell
@synthesize lostView;
@synthesize Date;
@synthesize DateColor;
@synthesize Time;
@synthesize Name;
@synthesize headShot;
@synthesize Taken;
@synthesize Book;
@synthesize Take;
@synthesize Postpone;
@synthesize Skip;
@synthesize edit;
@synthesize NameButton;
@synthesize CurrentArray;
@synthesize Filling;
@synthesize countthem;
@synthesize curentChoise;
@synthesize Datedone;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code


      
       
        UIView *ProfileView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320, 120)];

        [ProfileView setBackgroundColor:[UIColor clearColor]];

        [self addSubview:ProfileView];


        DateColor = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320, 130)];
        [DateColor setBackgroundColor:[UIColor colorWithRed:0.733 green:0.851 blue:0.902 alpha:1.000]];
        [ProfileView addSubview:DateColor];



        headShot = [[UIImageView alloc] initWithFrame:CGRectMake(8, 10, 80, 80)];
        [headShot setBackgroundColor:[UIColor grayColor]];
        [headShot setTag:130];
       [ProfileView addSubview:headShot];

        
        Datedone = [[UILabel alloc] initWithFrame:CGRectMake(95, 10, 200, 20)];
        [Datedone setTextColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
        [Datedone setFont:[UIFont systemFontOfSize:13]];
        [Datedone setTag:120];
        [Datedone setBackgroundColor:[UIColor clearColor]];
        [Datedone setNumberOfLines:3];
        [Datedone setAlpha:0];
        
        
               [ProfileView addSubview:Datedone];
        

        Date = [[UILabel alloc] initWithFrame:CGRectMake(95, 10, 200, 20)];
        [Date setTextColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
        [Date setFont:[UIFont systemFontOfSize:13]];
        [Date setTag:120];
        [Date setBackgroundColor:[UIColor clearColor]];
        [Date setNumberOfLines:3];
        
        
        [ProfileView addSubview:Date];
        
        
        
       NameButton = [[colorButton alloc]  initWithFrame:CGRectMake(285, 45, 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:@"" forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(book:) forControlEvents:UIControlEventTouchUpInside];
        [ProfileView addSubview:NameButton];


        Name = [[UILabel alloc] initWithFrame:CGRectMake(95, 14, 130, 80)];
        [Name setTextColor:[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000]];
        [Name setFont:[UIFont boldSystemFontOfSize:13]];
        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];

        [ProfileView addSubview:Name];

        
          //143 × 45 pixels

        Take = [[colorButton alloc] initWithFrame:CGRectMake(8,  98 ,143/2, 46/2)];
        [Take setBackgroundColor:[UIColor colorWithRed:0.078 green:0.408 blue:0.067 alpha:1.000]];
        [Take setShowsTouchWhenHighlighted:NO];
         [Take setTitle:@"Take" forState:UIControlStateNormal];
        //[Take setImage:[UIImage imageNamed:@"Take.png"] forState:UIControlStateNormal];
        [Take setAutoresizesSubviews:YES];
        [Take setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [Take.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
        [Take addTarget:self action:@selector(Take:) forControlEvents:UIControlEventTouchUpInside];
            [Take setShowsTouchWhenHighlighted:YES];
        [Take setTag:31];
        [ProfileView addSubview:[self setColor:Take]];

        
        
        //286 × 92 pixels


        Postpone = [[colorButton alloc] initWithFrame:CGRectMake(85,  98 ,143/2, 46/2)];
        [Postpone setBackgroundColor:[UIColor colorWithRed:0.090 green:0.071 blue:0.541 alpha:1.000]];
        [Postpone setShowsTouchWhenHighlighted:NO];
        [Postpone setTitle:@"Postpone" forState:UIControlStateNormal];
        //[Postpone setImage:[UIImage imageNamed:@"Postpone.png"] forState:UIControlStateNormal];
        [Postpone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [Postpone setAutoresizesSubviews:YES];
         [Postpone.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
        [Postpone addTarget:self action:@selector(Postpone:) forControlEvents:UIControlEventTouchUpInside];
        [Postpone setTag:32];
          [Postpone setShowsTouchWhenHighlighted:YES];
        [ProfileView addSubview:[self setColor:Postpone]];
     


        Skip = [[colorButton alloc] initWithFrame:CGRectMake(162,  98 ,143/2, 46/2)];
        [Skip setBackgroundColor:[UIColor colorWithRed:1.000 green:0.502 blue:0.000 alpha:1.000]];
        [Skip setShowsTouchWhenHighlighted:NO];
        [Skip setTitle:@"Skip" forState:UIControlStateNormal];
         [Skip setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [Skip setAutoresizesSubviews:YES];
        [Skip.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
        [Skip addTarget:self action:@selector(Skip:) forControlEvents:UIControlEventTouchUpInside];
        [Skip setTag:33];
        [Skip setShowsTouchWhenHighlighted:YES];
         [ProfileView addSubview:[self setColor:Skip]];

        
        
        edit = [[colorButton alloc] initWithFrame:CGRectMake(240, 98 ,143/2, 46/2)];
         [edit setBackgroundColor:[UIColor colorWithRed:0.212 green:0.635 blue:0.675 alpha:1.000]];
        [edit setShowsTouchWhenHighlighted:NO];
         [edit setTitle:@"Undo" forState:UIControlStateNormal];
        [edit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         [edit.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
        [edit setAutoresizesSubviews:YES];
        [edit addTarget:self action:@selector(Undo:) forControlEvents:UIControlEventTouchUpInside];
        [edit setTag:33];
         [edit setShowsTouchWhenHighlighted:YES];
       [ProfileView addSubview:[self setColor:edit]];

        
        //80 × 22 pixels

        Book = [[colorButton alloc] initWithFrame:CGRectMake(285, 10,27, 23)];
        [Book setBackgroundColor:[UIColor clearColor]];
        [Book setShowsTouchWhenHighlighted:NO];
        [Book setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        [Book setImage:[UIImage imageNamed:@"Book.png"] forState:UIControlStateNormal];
        [Book setAutoresizesSubviews:YES];
        [Book addTarget:self action:@selector(history:) forControlEvents:UIControlEventTouchUpInside];
        [Book setTag:35];
        [ProfileView addSubview:Book];

        [Postpone setEnabled:NO];
        [Take setEnabled:NO];
        //[edit setEnabled:NO];
        [Skip setEnabled:NO];
        
        
        
   
      

        
        CurrentArray = [[NSMutableDictionary alloc]init];
     

        
    }
    return self;
}


-(colorButton*)setColor:(colorButton*)sender

{
    
    CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
    shineLayer2.frame = sender.layer.bounds;
    shineLayer2.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          nil];
    shineLayer2.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];
    
    
    
    [sender.layer addSublayer:shineLayer2];
    [sender.layer setMasksToBounds:YES];
    
    
    sender.titleLabel.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    sender.titleLabel.layer.shadowOpacity = 1;
    sender.titleLabel.layer.shadowRadius = 1.0;
    [sender.layer setCornerRadius:4];
    [sender.layer setBorderColor:sender.backgroundColor.CGColor];
    [sender.layer setBorderWidth:0.8];
    
    return sender;
    
}


-(void)Undo:(colorButton*)sender
{
    
    
    NSLog(@"sender %@", sender.currentNotification.fireDate);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.CurrentReminder =CurrentArray;
    appDelegate.FromScreen=YES;
    [appDelegate.curentChoise removeAllObjects];
    
    if (curentChoise) {
          [appDelegate.curentChoise addObject:curentChoise];
  
 

  
    
    if ([[GetData getHistoryTotal] count] ==0) {
        
    }
    else
    {
   
    [SaveData DeleteHistory: [[[GetData getHistoryTotal] objectAtIndex:[[GetData getHistoryTotal] count]-1] valueForKey:@"Original_Time"]];
   
    [Notification undowhatYoudid:CurrentArray];
    
    [edit setEnabled:NO];
        
    }
   
     appDelegate.FromScreen=YES;
        
    }
    
    [appDelegate loadingviewOff:0.3];
    [appDelegate reloadTableview];
    
    
    
    
    
}


-(void)Take:(colorButton*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   
    
    appDelegate.CurrentReminder =CurrentArray;
    appDelegate.FromScreen=YES;
    [appDelegate.curentChoise removeAllObjects];
    [appDelegate.curentChoise addObject:curentChoise];
  [lostView Take:sender.titleLabel.text];
    
    
    [appDelegate loadingviewOff:0.3];
    [appDelegate reloadTableview];
    
    
}
-(void)Postpone:(colorButton*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.FromScreen=YES;
    appDelegate.CurrentReminder =CurrentArray;
    [appDelegate.curentChoise removeAllObjects];
  [appDelegate.curentChoise addObject:curentChoise];
    [lostView Postpone:sender.title];
    
    [appDelegate loadingviewOff:0.3];
    [appDelegate reloadTableview];
}

-(void)Skip:(colorButton*)sender
{
    
    ////NSLog(@"Skip");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
      appDelegate.CurrentReminder =CurrentArray;
    [appDelegate.curentChoise removeAllObjects];
  [appDelegate.curentChoise addObject:curentChoise];
     appDelegate.FromScreen=YES;
    [lostView Skip:sender.title];
    
    [appDelegate loadingviewOff:0.3];
    [appDelegate reloadTableview];
}

-(void)photo:(UIButton*)sender
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     appDelegate.FromScreen=YES;
      appDelegate.CurrentReminder =CurrentArray;
}
-(void)history:(colorButton*)sender
{
 
    [lostView GotToHistory:sender];
    
}



-(void)book:(colorButton*)sender
{

  
    [lostView renewMedicine:sender.title];
  

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void) FillAllItems:(NSMutableDictionary*) sender index:(NSInteger)set

{
    
    
    if ([[[UIApplication sharedApplication] scheduledLocalNotifications] count]==0) {
       
   
    }
    else
    {
    
    
    Filling=@" ";
    for (int i =([[[UIApplication sharedApplication] scheduledLocalNotifications] count]); i > 0; i--) {
        
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        if ([[notif.userInfo valueForKey:@"Duration"] isEqualToString:@"Never"]) {
            
        }
        else
        {
            
            NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
            [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            [setdata setDateFormat:@"EEEE dd, MMMM yyyy"];
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:[setdata dateFromString:[notif.userInfo valueForKey:@"Duration"]]];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[setdata dateFromString:[notif.userInfo valueForKey:@"Duration"]]];
            
            
            
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:23];
            // Notification will fire in one minute
            [dateComps setMinute:59];
            [dateComps setSecond:59];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            
            if  ([itemDate compare:notif.fireDate]==NSOrderedSame)
                
            {
                
                ////NSLog(@"NSOrderedSame %@ %@", itemDate,notif.fireDate );
                
            }
            else if  ([itemDate compare:notif.fireDate]==NSOrderedDescending)
                
                
            {
                ////NSLog(@"NSOrderedDescending %@ %@", itemDate,notif.fireDate );
            }
            else if  ([itemDate compare:notif.fireDate]==NSOrderedAscending)
                
                
            {
                ////NSLog(@"NSOrderedAscending %@ %@", itemDate,notif.fireDate );
                
                [[UIApplication sharedApplication] cancelLocalNotification:notif];
                
                
                
            }
        }

        
        
        if (notif.repeatInterval==0) {
            
        }
        else
        {
        
            
            
            
            if ([[notif.userInfo valueForKey:@"ID_Medicine" ] isEqualToString:[sender valueForKey:@"ID_Medicine"]])
            {
                
                
                
                
                
                NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
                [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                [dateFormat2 setDateFormat:@"hh:mm"];
                
                
                Filling = [[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]]valueForKey:@"Remaining"];
                
                
                
                NSDate *now = [self calculatenow];
                
                
                
                NSTimeInterval interval = [notif.fireDate timeIntervalSinceNow];
                int hours = (int)interval / 3600;             // integer division to get the hours part
                int minutes = (interval - (hours*3600)) / 60; // interval minus hours part (in seconds) divided by 60 yields minutes
                
                
                ////NSLog(@"%i, %i", minutes, hours);
                
                
                
                if (hours==0)
                    
                {
                    
                    
                    if (minutes<[[[[GetData getSetting] valueForKey:@"History"] objectAtIndex:0] intValue] && minutes>-[[[[GetData getSetting] valueForKey:@"History"] objectAtIndex:0] intValue]) {
                        
                        
                        
                        edit.currentNotification =notif;
                        CurrentArray =notif.userInfo;
                        curentChoise=notif;
                        
                        
                        [Postpone setAlpha:1];
                        [Take setAlpha:1];
                        [Skip setAlpha:1];
                        //[edit setAlpha:1];
                        
                        [Postpone setEnabled:YES];
                        [Take setEnabled:YES];
                        [edit setEnabled:YES];
                        [Skip setEnabled:YES];
                    }
                    
                    else
                    {
                         edit.currentNotification =notif;
                        CurrentArray =notif.userInfo;
                        curentChoise=notif;
                        
                        [Postpone setAlpha:0.5];
                        [Take setAlpha:0.5];
                        [Skip setAlpha:0.5];
                        //[edit setAlpha:0.5];
                        
                        [Postpone setEnabled:NO];
                        [Take setEnabled:NO];
                        //[edit setEnabled:NO];
                        [Skip setEnabled:NO];
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                
                else
                    
                {
                    
                    
                     edit.currentNotification =notif;
                    CurrentArray =notif.userInfo;
                    curentChoise=notif;
                    
                    [Postpone setAlpha:0.5];
                    [Take setAlpha:0.5];
                    [Skip setAlpha:0.5];
                    //[edit setAlpha:0.5];
                    
                    [Postpone setEnabled:NO];
                    [Take setEnabled:NO];
                    //[edit setEnabled:NO];
                    [Skip setEnabled:NO];
                    
                    
                    
                }
                
                
                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                
                [format setDateFormat:@"dd/MM/yyyy"];
                
                
                if ([[format stringFromDate:notif.fireDate] isEqualToString:[format stringFromDate:now]]) {
                    
                    
                    
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                    [dateFormat setDateFormat:@"hh:mm a EEEE dd/MM/yy"];
                    
                    
                    
                    
                    
                    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                    
                    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:notif.fireDate];
                    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:notif.fireDate];
                    
                    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
                    [dateCompsEarly setDay:[dateComponents day]];
                    [dateCompsEarly setMonth:[dateComponents month]];
                    [dateCompsEarly setYear:[dateComponents year]];
                    [dateCompsEarly setHour:[timeComponents hour]];
                    [dateCompsEarly setMinute:timeComponents.minute];
                    [dateCompsEarly setSecond:0];
                    NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
                    
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                    [formatter setDateFormat:@"EEEE"];
                    
                  
                    
                    if ([now compare: itemNext]==NSOrderedAscending) {
                        
                        
                        [Date setText:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:itemNext]]];
                         [Date setTextColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
                           [Datedone setAlpha:0];
                        
                        
                    }
                    else if  ([now compare: itemNext]==NSOrderedDescending) {
                        
                        
                        
                        [Date setText:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:itemNext]]];
                         [Date setTextColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
                           [Datedone setAlpha:0];
                        
                        
                    }
                    else if  ([now compare: itemNext]==NSOrderedSame) {
                        
                        
                        Filling = [[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]]valueForKey:@"Remaining"];
                        [Date setText:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:itemNext]]];
                         [Date setTextColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
                           [Datedone setAlpha:0];
                        
                        
                    }
                    
                    
                }
                else
                {
                    
                    
                    
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                    [dateFormat setDateFormat:@"hh:mm a EEEE  dd/MM/yy"];
                    
                    
                    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                    
                    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:notif.fireDate];
                    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:notif.fireDate];
                    
                    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
                    [dateCompsEarly setDay:[dateComponents day]];
                    [dateCompsEarly setMonth:[dateComponents month]];
                    [dateCompsEarly setYear:[dateComponents year]];
                    [dateCompsEarly setHour:[timeComponents hour]];
                    [dateCompsEarly setMinute:timeComponents.minute];
                    [dateCompsEarly setSecond:0];
                    NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
                    
                    
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                    [formatter setDateFormat:@"EEEE"];
                    
                    [Date setText:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:itemNext]]];
                         [Date setTextColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
                       [Datedone setAlpha:0];
                    
                }
                
                
                
                
                
            }
        
               
        
        
    }




}
        
    }





if (fmod(set,2)==0) {
    [DateColor setBackgroundColor:[UIColor colorWithRed:0.882 green:0.937 blue:0.957 alpha:1.000]];    // even num
} else {
    [DateColor setBackgroundColor:[UIColor colorWithRed:0.733 green:0.851 blue:0.902 alpha:1.000]];
}



//[Date setText:[sender valueForKey:@"Date"]];
[Time setText:[NSString stringWithFormat:@"%@\n\n",[sender valueForKey:@"Time"]]];
[Taken setText:[sender valueForKey:@"Taken"]];

////NSLog(@"Filling |%@|", Filling);

if ([Filling isEqualToString:@" "]) {
    
    if ([[sender valueForKey:@"Quantity"] intValue]>1) {
        
        [Name setText:[NSString stringWithFormat:@"%@ \n%@ \n%@ %@", [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Quantity"],[[[GetData getDrugTypeStrength:[sender valueForKey:@"Type"]] valueForKey:@"Drug_types"] objectAtIndex:0]]];
    }
    else
    {
        [Name setText:[NSString stringWithFormat:@"%@ \n%@ \n%@ %@", [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Quantity"],[sender valueForKey:@"Type"]]];
        
    }
    
      ////NSLog(@"%@", Name.text);
    
}
else
{
    if ([[sender valueForKey:@"Quantity"] intValue]>1) {
        
        
        
        [Name setText:[NSString stringWithFormat:@"%@ \n%@ \n%@ %@ (%@ left)", [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Quantity"], [[[GetData getDrugTypeStrength:[sender valueForKey:@"Type"]] valueForKey:@"Drug_types"] objectAtIndex:0], Filling]];
        
        
        
          ////NSLog(@"%@", Name.text);
        
        
        
    }
    else
    {
        [Name setText:[NSString stringWithFormat:@"%@ \n%@ \n%@ %@ (%@ left)", [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Quantity"],[sender valueForKey:@"Type"], Filling]];
        
          ////NSLog(@"%@", Name.text);
        
    
        
        
    }
    
    
}


Book.titleLabel.text=[sender valueForKey:@"ID_Medicine"];
NameButton.title =[sender valueForKey:@"ID_Medicine"];
Take.title=[sender valueForKey:@"ID_Medicine"];
Postpone.title=[sender valueForKey:@"ID_Medicine"];
Skip.title=[sender valueForKey:@"ID_Medicine"];
Book.title=[sender valueForKey:@"ID"];

headShot = (UIImageView *)[self viewWithTag:(130)];
[headShot setImage:[UIImage imageWithContentsOfFile:[sender valueForKey:@"Image"]]];

  if([[sender valueForKey:@"Duration"] isEqualToString:@"Never"])
        
    {
    }
    else
    {
        
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
        [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [setdata setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComps = [[NSDateComponents alloc] init];
        
        NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:[setdata dateFromString:[sender valueForKey:@"Duration"]]];
        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[setdata dateFromString:[sender valueForKey:@"Duration"]]];
        
        
        
        [dateComps setDay:[dateComponents day]];
        [dateComps setMonth:[dateComponents month]];
        [dateComps setYear:[dateComponents year]];
        [dateComps setHour:23];
        // Notification will fire in one minute
        [dateComps setMinute:59];
        [dateComps setSecond:59];
        
        
        NSDate *itemDate = [calendar dateFromComponents:dateComps];
        
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        
        [format setDateFormat:@"dd/MM/yyyy"];
        
        
    
        
        if (Date.text) {
                   
        }
        
        else
        {
         
            [Datedone setText:[NSString stringWithFormat:@"Ended on %@", [format stringFromDate:itemDate]]];
            [Datedone setTextColor:[UIColor redColor]];
            [Datedone setAlpha:1];
            
            [Postpone setAlpha:0.1];
            [Take setAlpha:0.1];
            [Skip setAlpha:0.1];
            [edit setAlpha:0.1];
            
            [Postpone setEnabled:NO];
            [Take setEnabled:NO];
            //[edit setEnabled:NO];
            [Skip setEnabled:NO];
             [edit setEnabled:NO];
            
            
        }
       
    }


}


-(BOOL)SeeifEndingDateIsPast:(UILocalNotification *)notification {
    
    
    
    BOOL set;
    
    
    
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd 00:00:01 PM +0000"];
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc]init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd hh:mm:ss a +0000"];
    
    
    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    NSDate *let =[dateConvert dateFromString:[notification.userInfo valueForKey:@"Duration"]];
    
    
    NSDate *get =[dateConvert dateFromString:[notification.userInfo valueForKey:@"Starting"]];
    
    ////NSLog(@"let op %@ %@", [dateFormatter stringFromDate:let],  [dateFormatter stringFromDate:get]);
    
    
    if ([[notification.userInfo valueForKey:@"Duration"] isEqualToString:@"Never"])
    {
        set = YES;
        
    }
    else
    {
        if  ([let compare:get]==NSOrderedSame)
            
        {
            
            set = NO;
            ////NSLog(@"NSOrderedSame");
            
                   //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:[[UIApplication sharedApplication] applicationIconBadgeNumber]-1];
              [[UIApplication sharedApplication] cancelLocalNotification:notification];
            
            
        }
        if  ([let compare:get]==NSOrderedDescending)
            
            
        {
            
            ////NSLog(@"Toekomst");
            
            set = YES;
            
        }
        if ([let compare:get]==NSOrderedAscending)
            
            
        {
            
            ////NSLog(@"Verleden");
                   //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:[[UIApplication sharedApplication] applicationIconBadgeNumber]-1];
              [[UIApplication sharedApplication] cancelLocalNotification:notification];
            
            set = NO;
        }
    }
    
    return set;
}




-(NSDate*)calculatenow

{
    
       NSDate *now = [[NSDate alloc] init];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]+1];
    [dateComps setMinute:[timeComponents minute]];
    [dateComps setSecond:0];
    
    
    return [calendar dateFromComponents:dateComps];
}



-(void)getparent:(MedicineViewController*)set

{
    lostView =set;
}

@end
