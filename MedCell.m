//
//  HistoryCell.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "MedCell.h"
#import "HistoryViewController.h"
#import "MedicineViewController.h"
#import "GetData.h"
#import "AppDelegate.h"
#import "colorButton.h"
#import <QuartzCore/QuartzCore.h>
#import "Notification.h"
#import "SaveData.h"
#import "SaveCore.h"
#import "Notification.h"
#import "Notifications.h"
#import "ActionArrow.h"

@implementation MedCell
@synthesize lostView;
@synthesize Date;
@synthesize DateColor;
@synthesize Time;
@synthesize Name;
@synthesize headShot;
@synthesize Taken;
@synthesize Book;
@synthesize Take;
@synthesize Postpone;
@synthesize Skip;
@synthesize edit;
@synthesize NameButton;
@synthesize CurrentArray;
@synthesize Filling;
@synthesize countthem;
@synthesize curentChoise;
@synthesize Datedone;
@synthesize Info;
@synthesize RanOut;
@synthesize quantityString;
@synthesize remainingString;
@synthesize strengthString;
@synthesize Day;
@synthesize ProfileView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
        
        ProfileView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320, 120)];
        
        [ProfileView setBackgroundColor:[UIColor clearColor]];
        
        [self addSubview:ProfileView];
        
        
        DateColor = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320, 130)];
//        [DateColor setBackgroundColor:[UIColor colorWithRed:0.733 green:0.851 blue:0.902 alpha:1.000]];
        //218, 226, 239 //06-07-2017
        [DateColor setBackgroundColor:[UIColor colorWithRed:218.0f/255.0f green:226.0f/255.0f blue:239.0f/255.0f alpha:1.000]];
        [ProfileView addSubview:DateColor];
        
        
        
        headShot = [[UIImageView alloc] initWithFrame:CGRectMake(8, 10, 80, 80)];
        [headShot setBackgroundColor:[UIColor grayColor]];
        [headShot setTag:130];
        [ProfileView addSubview:headShot];
        
        
        Datedone = [[UILabel alloc] initWithFrame:CGRectMake(95, 8, 200, 20)];
        [Datedone setTextColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
        [Datedone setFont:[UIFont systemFontOfSize:12]];
        [Datedone setTag:120];
        [Datedone setBackgroundColor:[UIColor clearColor]];
        [Datedone setNumberOfLines:3];
        [Datedone setAlpha:0];
        
        [ProfileView addSubview:Datedone];
        
        colorButton *NameButton3 = [[colorButton alloc]  initWithFrame:CGRectMake(285, 45, 60/2, 72/2)];
        [NameButton3 setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [ProfileView addSubview:NameButton3];
        
//        NameButton = [[colorButton alloc]  initWithFrame:CGRectMake(0, 0, 320, 100)]; //25-10-2017
        NameButton = [[colorButton alloc]  initWithFrame:CGRectMake(285, 45, 60/2, 72/2)];
        [NameButton setBackgroundColor:[UIColor clearColor]];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(book:) forControlEvents:UIControlEventTouchUpInside];
        [ProfileView addSubview:NameButton];
        
//        Name = [[UITextView alloc] initWithFrame:CGRectMake(95, 23, self.frame.size.width-140, 20)]; //05-09-2017
        Name = [[UITextView alloc] initWithFrame:CGRectMake(95, 23, self.frame.size.width-120, 20)];
//        [Name setTextColor:[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000]]; //06-07-2017 comment line and add new to bottom for change color
        [Name setFont:[UIFont boldSystemFontOfSize:12]];
        [Name setTag:120];
        Name.contentInset = UIEdgeInsetsMake(-4,-4,0,0);
        [Name setBackgroundColor:[UIColor clearColor]];
        //[Name setNumberOfLines:1];
        
        [ProfileView addSubview:Name];
        
        RanOut = [[UILabel alloc] initWithFrame:CGRectMake(95, 68, self.frame.size.width-140, 30)];
        [RanOut setTextColor:[UIColor clearColor]];
        [RanOut setFont:[UIFont systemFontOfSize:12]];
        [RanOut setText:NSLocalizedString(@"Empty",nl)];
        [RanOut setTag:120];
        [RanOut setBackgroundColor:[UIColor clearColor]];
        [RanOut setNumberOfLines:4];
        
        [ProfileView addSubview:RanOut];
        
        //bottom - 119-197-152 //top - 101-184-136
        //143 × 45 pixels
        Take = [[colorButton alloc] initWithFrame:CGRectMake(8,  98 ,143/2, 46/2)];
//        [Take setBackgroundColor:[UIColor colorWithRed:0.078 green:0.408 blue:0.067 alpha:1.000]];
        [Take setBackgroundColor:[UIColor colorWithRed:0.396 green:0.722 blue:0.533 alpha:1.000]];
        [Take setTitle:NSLocalizedString(@"Take",nil) forState:UIControlStateNormal];
        //[Take setImage:[UIImage imageNamed:@"Take.png"] forState:UIControlStateNormal];
        [Take setAutoresizesSubviews:YES];
        [Take setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [Take.titleLabel setFont:[UIFont boldSystemFontOfSize:11]];
        [Take addTarget:self action:@selector(Take:) forControlEvents:UIControlEventTouchUpInside];
        [Take setShowsTouchWhenHighlighted:YES];
        [Take setTag:31];
        [ProfileView addSubview:[self setColor:Take]];
        
        
        
        //286 × 92 pixels
        
        
        Postpone = [[colorButton alloc] initWithFrame:CGRectMake(85,  98 ,143/2, 46/2)];
        [Postpone setBackgroundColor:[UIColor colorWithRed:0.090 green:0.071 blue:0.541 alpha:1.000]];
        [Postpone setTitle:NSLocalizedString(@"Postpone",nil) forState:UIControlStateNormal];
        //[Postpone setImage:[UIImage imageNamed:@"Postpone.png"] forState:UIControlStateNormal];
        [Postpone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [Postpone setAutoresizesSubviews:YES];
        [Postpone.titleLabel setFont:[UIFont boldSystemFontOfSize:11]];
        [Postpone addTarget:self action:@selector(Postpone:) forControlEvents:UIControlEventTouchUpInside];
        [Postpone setTag:32];
        [Postpone setShowsTouchWhenHighlighted:YES];
        [ProfileView addSubview:[self setColor:Postpone]];
        
        
        
        Skip = [[colorButton alloc] initWithFrame:CGRectMake(162,  98 ,143/2, 46/2)];
        [Skip setBackgroundColor:[UIColor colorWithRed:1.000 green:0.502 blue:0.000 alpha:1.000]];
        [Skip setTitle:NSLocalizedString(@"Skip",nil) forState:UIControlStateNormal];
        [Skip setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [Skip setAutoresizesSubviews:YES];
        [Skip.titleLabel setFont:[UIFont boldSystemFontOfSize:11]];
        [Skip addTarget:self action:@selector(Skip:) forControlEvents:UIControlEventTouchUpInside];
        [Skip setTag:33];
        [Skip setShowsTouchWhenHighlighted:YES];
        [ProfileView addSubview:[self setColor:Skip]];
        
        
        //bottom - 129-144-200 //top - 142-155-206
        edit = [[colorButton alloc] initWithFrame:CGRectMake(240, 98 ,143/2, 46/2)];
//        [edit setBackgroundColor:[UIColor colorWithRed:0.212 green:0.635 blue:0.675 alpha:1.000]]; //29-06-2017 changes
        [edit setBackgroundColor:[UIColor colorWithRed:0.557 green:0.608 blue:0.808 alpha:1.000]];
        [edit setTitle:NSLocalizedString(@"Undo",nil) forState:UIControlStateNormal];
        [edit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [edit.titleLabel setFont:[UIFont boldSystemFontOfSize:11]];
        [edit setAutoresizesSubviews:YES];
        [edit addTarget:self action:@selector(Undo:) forControlEvents:UIControlEventTouchUpInside];
        [edit setTag:33];
        [edit setShowsTouchWhenHighlighted:YES];
        [ProfileView addSubview:[self setColor:edit]];
        
        
        //80 × 22 pixels
        
        colorButton *Book2 = [[colorButton alloc] initWithFrame:CGRectMake(285, 7,27, 26)];
       // [Book2 setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        [Book2 setShowsTouchWhenHighlighted:NO];
        [Book2 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        [Book2 setImage:[UIImage imageNamed:@"Book.png"] forState:UIControlStateNormal];
        [Book2 setAutoresizesSubviews:YES];
        [ProfileView addSubview:Book2];
        [Book2.layer setCornerRadius:4];
        
        
        
        Book = [[colorButton alloc]  initWithFrame:CGRectMake(285, 0, self.frame.size.width-285, 40)];
        [Book setBackgroundColor:[UIColor clearColor]];
        [Book setTag:35];
        [Book addTarget:self action:@selector(history:) forControlEvents:UIControlEventTouchUpInside];
        [ProfileView addSubview:Book];
        
        [Postpone setEnabled:NO];
        [Take setEnabled:NO];
        //[edit setEnabled:NO];
        [Skip setEnabled:NO];
        
        
        
        CurrentArray = [[NSMutableDictionary alloc]init];
        
        
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
            
            //NSLog(@"left");
            
            self.transform = CGAffineTransformMakeScale(-1, 1);
            
            
            for (UIView *content in self.subviews) {
                
                
                for (UILabel *label in content.subviews) {
                    
                    
                    if ([label isKindOfClass:[UILabel class]]) {
                        
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentLeft];
                        
                    }
                    
                }
                for (UITextView *label in content.subviews) {
                    
                    
                    if ([label isKindOfClass:[UITextView class]]) {
                        
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentLeft];
                        
                    }
                    
                }
                
                for (UIButton *label in content.subviews) {
                    
                    
                    if ([label isKindOfClass:[UIButton class]]) {
                        
                        label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                        [label.titleLabel setTextAlignment:NSTextAlignmentLeft];
                        
                    }
                    
                }
                
                
                
                
            }
            
            
        }
        
        else
        {
            
        }
        
        
        
        
        
        
    }
    return self;
}


-(colorButton*)setColor:(colorButton*)sender
{
    
    CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
    shineLayer2.frame = sender.layer.bounds;
    shineLayer2.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          nil];
    shineLayer2.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];
    
    
    
    [sender.layer addSublayer:shineLayer2];
    [sender.layer setMasksToBounds:YES];
    
    
    sender.titleLabel.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    sender.titleLabel.layer.shadowOpacity = 1;
    sender.titleLabel.layer.shadowRadius = 1.0;
    [sender.layer setCornerRadius:4];
    [sender.layer setBorderColor:sender.backgroundColor.CGColor];
    [sender.layer setBorderWidth:0.8];
    
    return sender;
    
}


-(void)Undo:(colorButton*)sender
{
    [self addActionForGoogleAnalytics:@"Undo Actions"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]]) {
        
        ////////////////////////////////////////////////////NSLog(@"1");
        NSData *dataExtra= [[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]];
        
        if (dataExtra ==NULL) {
            
        }
        else
        {
            UILocalNotification *localNotif = [NSKeyedUnarchiver unarchiveObjectWithData:dataExtra];
            
            
            
            [[UIApplication sharedApplication] cancelLocalNotification:localNotif];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]];
            
        }
        
    }
    
    
    appDelegate.CurrentReminder =CurrentArray;
    appDelegate.FromScreen=NO;
    [appDelegate.curentChoise removeAllObjects];
    appDelegate.chosenCourse =sender.chosenCourse;
    
    
    
    
    
    Notifications *undoset =[appDelegate GetnotificationID:sender.chosenCourse.id_medicine];
    UILocalNotification *undoNote = (UILocalNotification*)[NSKeyedUnarchiver unarchiveObjectWithData:undoset.notifications];
    
    if (undoNote != nil) //26-06-2017 condition for not nil (application crash)
    {
        [appDelegate.curentChoise addObject:undoNote];
    }
    
    
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((userInfo.Coloralpha = %@) AND (userInfo.ID_Medicine = %@)",@"Postponed", sender.chosenCourse.id_medicine];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(userInfo.Coloralpha = %@) AND (userInfo.ID_Medicine = %@)",@"Postponed", sender.chosenCourse.id_medicine];
    
    NSArray *notificationArray = [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        
        
        
        [[UIApplication sharedApplication] cancelLocalNotification:notif];
        
        
    }
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", sender.chosenCourse.id_medicine];
    
    
    
    NSArray *notificationArray2= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate2];
    
    if ([notificationArray2 count]>0) {
        
        
        ////////////////////////////////////////////////////NSLog(@"5");
        
        UILocalNotification *notif = [notificationArray2 objectAtIndex:([notificationArray2 count]-1)];
        
        
        
        NSMutableDictionary* CopyDictonary = [NSMutableDictionary dictionaryWithDictionary:notif.userInfo];
        
        
        if ([[CopyDictonary valueForKey:@"Remaining"] integerValue]>1)
        {
            if (undoNote != nil) {
                
                [CopyDictonary setObject:[undoNote.userInfo valueForKey:@"Remaining"]  forKey:@"Remaining"];
                sender.chosenCourse.remaining = [undoNote.userInfo valueForKey:@"Remaining"] ;
            }
           
        }
        else
        {
            
            
        }
        
        NSDictionary* firstDictionary = [NSMutableDictionary dictionaryWithDictionary:CopyDictonary];
        
        [undoNote setUserInfo:firstDictionary];
        
        [[UIApplication sharedApplication] cancelLocalNotification:notif];
        [[UIApplication sharedApplication] scheduleLocalNotification:undoNote];
        
    }
    
    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"Undo" afterDelay:0.1];
    
    appDelegate.FromScreen=NO;
    [appDelegate canPerformAction:@selector(PostNotfication:)  withSender:@"NO"];
    
    [appDelegate performSelector:@selector(PostNotfication:) withObject:@"NO"];
    
    [appDelegate removeHistory:sender.chosenCourse.id_medicine];
    
    
}

-(void)Take:(colorButton*)sender
{
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]]) {
        
        
        NSData *dataExtra= [[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]];
        
        if (dataExtra ==NULL) {
            
        }
        else
        {
            UILocalNotification *localNotif = [NSKeyedUnarchiver unarchiveObjectWithData:dataExtra];
            //////////////////////////////////////////////////////NSLog(@"Remove localnotification  are %@", localNotif);
            
            
            [[UIApplication sharedApplication] cancelLocalNotification:localNotif];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]];
            
        }
        
    }
    
    
    
    
    
    appDelegate.CurrentReminder =CurrentArray;
    appDelegate.FromScreen=NO;
    [appDelegate.curentChoise removeAllObjects];
    [appDelegate.curentChoise addObject:curentChoise];
    appDelegate.chosenCourse =sender.chosenCourse;
    [lostView Take:sender.titleLabel.text];
    
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(userInfo.Coloralpha = %@) AND (userInfo.ID_Medicine = %@)",@"Postponed", sender.chosenCourse.id_medicine];
    
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        
        [[UIApplication sharedApplication] cancelLocalNotification:notif];
    
    }
    
    [appDelegate performSelector:@selector(reloadTableview:) withObject:NSLocalizedString(@"Take",nil) afterDelay:0.1];
    
    
    
}
-(void)Postpone:(colorButton*)sender
{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]]) {
        
        
        NSData *dataExtra= [[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]];
        
        if (dataExtra ==NULL) {
            
        }
        else
        {
            UILocalNotification *localNotif = [NSKeyedUnarchiver unarchiveObjectWithData:dataExtra];
            
            
            
            [[UIApplication sharedApplication] cancelLocalNotification:localNotif];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]];
            
        }
        
    }
    
    
    
    
    
    appDelegate.FromScreen=NO;
    appDelegate.CurrentReminder =CurrentArray;
    [appDelegate.curentChoise removeAllObjects];
    [appDelegate.curentChoise addObject:curentChoise];
    appDelegate.chosenCourse =sender.chosenCourse;
    [lostView Postpone:sender.title];
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(userInfo.Coloralpha = %@) AND (userInfo.ID_Medicine = %@)",@"Postponed", sender.chosenCourse.id_medicine];
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        [[UIApplication sharedApplication] cancelLocalNotification:notif];
        
        
    }
    
    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"Postpone" afterDelay:0.1];
    
}

-(void)Skip:(colorButton*)sender
{
    
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]]) {
        
        
        NSData *dataExtra= [[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]];
        
        if (dataExtra ==NULL) {
            
        }
        else
        {
            UILocalNotification *localNotif = [NSKeyedUnarchiver unarchiveObjectWithData:dataExtra];
            //////////////////////////////////////////////////////NSLog(@"Remove localnotification  are %@", localNotif);
            
            
            [[UIApplication sharedApplication] cancelLocalNotification:localNotif];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString  stringWithFormat:@"Extra_%@",sender.chosenCourse.id_medicine]];
            
        }
        
    }
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.CurrentReminder =CurrentArray;
    [appDelegate.curentChoise removeAllObjects];
    [appDelegate.curentChoise addObject:curentChoise];
    appDelegate.FromScreen=NO;
    appDelegate.chosenCourse =sender.chosenCourse;
    
    
    
    
    [lostView Skip:sender.title];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(userInfo.Coloralpha = %@) AND (userInfo.ID_Medicine = %@)",@"Postponed", sender.chosenCourse.id_medicine];
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        
        
        [[UIApplication sharedApplication] cancelLocalNotification:notif];
        
        
    }
    
    
    //[appDelegate reloadTableview:@"Skip"];
    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"Skip" afterDelay:0.1];
    
}

-(void)photo:(UIButton*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.FromScreen=YES;
    appDelegate.CurrentReminder =CurrentArray;
}
-(void)history:(colorButton*)sender
{
    
    [lostView GotToHistory:sender];
    
}


-(void)book:(colorButton*)sender
{
    [lostView renewMedicine:sender.chosenCourse];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void) FillAllItems:(Medicine*) sender index:(NSInteger)set
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    quantityString =@"";
    strengthString =@"";
    remainingString =@"";
    
    
    [Datedone setAlpha:0];
    [Datedone  setText:@" "];
    [Date setAlpha:1];
    
    [RanOut setTextColor:[UIColor clearColor]];
    
    
    [Date setAlpha:1];
    
    NSArray *ry = [GetData getDrugTypeStrength:sender.type];
    
    if (ry.count != 0)  // add condition for handle crash 05-09-2017
    {
        
//        NSArray *typit = [[GetData getDrugTypeStrength:sender.type] objectAtIndex:0]; //05-09-2017
               NSArray *typit = [ry objectAtIndex:0];
        
        //NSLog(@"%@", [typit valueForKey:@"Drug_types"]);
        
        if ([sender.type isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
            
            quantityString =[NSString stringWithFormat:@"%@", sender.type];
        }
        else
            
        {
            
            
            if ([sender.quantity intValue]>1) {
                
                if ([sender.dose_unit isEqualToString:@"piece"]) {
                    
                    if ([[typit valueForKey:@"Drug_types"] isEqualToString:NSLocalizedString(@"Other",nil)]) {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", sender.quantity, sender.type];
                        
                    }
                    else
                    {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", sender.quantity, [typit valueForKey:@"Drug_types"]];
                    }
                    
                }
                
                else
                    
                {
                    if ([[typit valueForKey:@"Drug_types"] isEqualToString:NSLocalizedString(@"Other",nil)]) {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", sender.quantity,sender.dose_unit, sender.type];

                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            if ([sender.quantity intValue] > 1)
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }

                        
                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            
                            if ([sender.quantity intValue] > 1)
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                            }
                            
                        }

                        
                        
                    }
                    else
                    {
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", sender.quantity,sender.dose_unit, [typit valueForKey:@"Drug_types"]]; //16-10-2017
                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"unidad" withString:@""];
                        
                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                            if ([sender.quantity intValue] > 1)
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                            }
                        }
                        
                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            if ([sender.type isEqualToString:@"Spray"] || [sender.type isEqualToString:@"Tablet"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                    }
                }
            }
            else {
                
                if ([sender.dose_unit isEqualToString:@"piece"]) {
                    
                    //////////////////////////////////////////////////////////NSLog(@"piece");
                    
                    
                    if ([[typit valueForKey:@"Drug_less"] isEqualToString:NSLocalizedString(@"Other",nil)]) {
                        
                        
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", sender.quantity,sender.type];
                        
                    }
                    
                    else
                    {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", sender.quantity, [typit valueForKey:@"Drug_less"]];
                    }
                    
                    
                }
                
                else
                    
                {
                    
                    if ([[typit valueForKey:@"Drug_less"] isEqualToString:NSLocalizedString(@"Other",nil)]) {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", sender.quantity,sender.dose_unit, sender.type];
                        
                        //9-11-2017
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            
                            if ([sender.quantity intValue] > 1)
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                            }
                        }
                        
                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
//                            if ([sender.type isEqualToString:@"Plaster"] || [sender.type isEqualToString:@"Dråber"])
//                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
//                            }
                        }
                    }
                    else
                    {
                        
//                        quantityString =[NSString stringWithFormat:@"%@ %@ %@", sender.quantity,sender.dose_unit, [typit valueForKey:@"Drug_less"]]; //02-10-2017
                        quantityString = [NSString stringWithFormat:@"%@ %@ %@", sender.quantity,sender.dose_unit, [typit valueForKey:@"Drug_less"]];
                        
                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"unidad" withString:@""];
                        
                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            
                            if ([sender.quantity intValue] > 1)
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                            }
                        }
                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            if ([sender.type isEqualToString:@"Spray"] || [sender.type isEqualToString:@"Tablet"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                    }
                }
                
            }
            
        }
        
        
        
        if ([sender.strength intValue]>0) {
            
            strengthString =[NSString stringWithFormat:@"(%@ %@)", sender.strength, sender.strength_unit];
            
        }
        else {
            
            strengthString =@" ";
        }
        
        if ([sender.type isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
            
            quantityString =[NSString stringWithFormat:@"%@", sender.type];
            
            if ([sender.remaining intValue]>1) {
                
                remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", sender.remaining, [typit valueForKey:@"Drug_types"], NSLocalizedString(@"left",nil)];

                //9-11-2017
                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                {
                    if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Andet"])
                    {
                        remainingString = [remainingString stringByReplacingOccurrencesOfString:@"Andet" withString:@""];
                    }
                }
                
                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                {
                    remainingString = [remainingString stringByReplacingOccurrencesOfString:@"Annet" withString:@""];
                }
            }
            
            else
            {
                
                remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", sender.remaining, [typit valueForKey:@"Drug_less"],NSLocalizedString(@"left",nil)];

                //9-11-2017
                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                {
                    if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Andet"])
                    {
                        remainingString = [remainingString stringByReplacingOccurrencesOfString:@"Andet" withString:@""];
                    }
                }
                
                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                {
                    remainingString = [remainingString stringByReplacingOccurrencesOfString:@"Annet" withString:@""];
                }
            }
        }
        else
            
        {
            
            if ([sender.remaining isEqualToString:@" "]) {
                
                remainingString =@" ";
            }
            else
                
            {
                if ([sender.remaining intValue]>1) {
                    
                    
                    
                    
                    if ([sender.dose_unit isEqualToString:@"piece"]) {
                        
                        if ([[typit valueForKey:@"Drug_types"] isEqualToString:NSLocalizedString(@"Other",nil)]) {
                            
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", sender.remaining, sender.type, NSLocalizedString(@"left",nil)];
                        }
                        
                        else
                            
                        {
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", sender.remaining, [typit valueForKey:@"Drug_types"], NSLocalizedString(@"left",nil)];
                        }
                    }
                    
                    else
                    {
                        
                        remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", sender.remaining,sender.dose_unit, NSLocalizedString(@"left",nil)];
                        remainingString = [remainingString stringByReplacingOccurrencesOfString:@"unidad" withString:@""]; //02-010-2017
                        
                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            if ([sender.type isEqualToString:@"Tablet"] || [sender.type isEqualToString:@"Inhalator"] || [sender.type isEqualToString:@"Spray"] || [sender.type isEqualToString:@"Dråber"] || [sender.type isEqualToString:@"Plaster"])
                            {
                                remainingString = [remainingString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            remainingString = [remainingString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }
                    }
                    
                }
                else {
                    
                    if ([sender.dose_unit isEqualToString:@"piece"]) {
                        
                        if ([[typit valueForKey:@"Drug_less"] isEqualToString:NSLocalizedString(@"Other",nil)]) {
                            
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", sender.remaining, [typit valueForKey:@"Drug_less"], NSLocalizedString(@"left",nil)];
                        }
                        
                        else
                        {
                            
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", sender.remaining, [typit valueForKey:@"Drug_less"], NSLocalizedString(@"left",nil)];
                        }
                    }
                    
                    else
                        
                    {
                        remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", sender.remaining,sender.dose_unit, NSLocalizedString(@"left",nil)];
                        
                        //9-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
//                            if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Tablet"] || [[typit valueForKey:@"Drug_less"] isEqualToString:@"Spray"] || [[typit valueForKey:@"Drug_less"] isEqualToString:@"Inhalator"])
//                            {
                                remainingString = [remainingString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
//                            }
                        }
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            remainingString = [remainingString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                        }
                    }
                }
            }
        }
        
        
        
        //    NSString *str = [[NSString alloc] initWithData:sender.notifications encoding:NSASCIIStringEncoding];
        
        
        
        
        UILocalNotification *notif = (UILocalNotification*)[NSKeyedUnarchiver unarchiveObjectWithData:sender.notifications];
        
        
        edit.currentNotification = notif;
        
        [CurrentArray setDictionary:notif.userInfo];
        
        
        
        curentChoise=notif;
        
        
        
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
        [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [setdata setDateFormat:@"EE dd, MMMM yyyy"];
        
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormat setDateFormat:[appDelegate convertString:@"hh:mm a EEEE dd/MM/yy"]];
        
        
        
        NSTimeInterval interval = [sender.firedate timeIntervalSinceNow]; //07-07-2017
        //    NSTimeInterval interval = [strdate timeIntervalSinceNow];
        int hours = (int)interval / 3600;             // integer division to get the hours part
        int minutes = (interval - (hours*3600)) / 60; // interval minus hours part (in seconds) divided by 60 yields minutes
        
        if (hours<=0)
        {
            NSArray *ry = [[GetData getSetting] valueForKey:@"History"];
            
            if (ry.count != 0) //add condition for handle crash 04-09-2017
            {
//                if (minutes<[[[[GetData getSetting] valueForKey:@"History"] objectAtIndex:0] intValue] && minutes>-[[[[GetData getSetting] valueForKey:@"History"] objectAtIndex:0] intValue]) { //05-09-2017
                if (minutes<[[ry objectAtIndex:0] intValue] && minutes>-[[ry objectAtIndex:0] intValue]) {

                    [Postpone setAlpha:1];
                    [Take setAlpha:1];
                    [Skip setAlpha:1];
                    [Postpone setEnabled:YES];
                    [Take setEnabled:YES];
                    [edit setEnabled:YES];
                    [Skip setEnabled:YES];
                }
                
                else
                {
                    [Postpone setAlpha:0.5];
                    [Take setAlpha:0.5];
                    [Skip setAlpha:0.5];
                    [Postpone setEnabled:NO];
                    [Take setEnabled:NO];
                    [Skip setEnabled:NO];
                    
                    
                }
            }
            
            
            
            
            
        }
        
        else
            
        {
            
            [Postpone setAlpha:0.5];
            [Take setAlpha:0.5];
            [Skip setAlpha:0.5];
            [Postpone setEnabled:NO];
            [Take setEnabled:NO];
            [Skip setEnabled:NO];
            
            
            
        }
        
        NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] init];
        
        
        UIFont *arialBold = [UIFont boldSystemFontOfSize:14];
        UIFont *ariallight = [UIFont systemFontOfSize:12];
        
        NSDictionary *arialDict = [NSDictionary dictionaryWithObject: arialBold forKey:NSFontAttributeName];
        
        NSDictionary *lightDict = [NSDictionary dictionaryWithObject: ariallight forKey:NSFontAttributeName];
        
        
        NSMutableAttributedString *dateit = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:sender.firedate]] attributes: lightDict];
        //007-07-2017 "sender.firedate" to  "strdate"
        
        NSMutableAttributedString *nameit = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", sender.name] attributes: arialDict];
        
        NSMutableAttributedString *tussen = [[NSMutableAttributedString alloc] initWithString:@"\n" attributes: arialDict];
        
        NSString *infotext;
        
        
        if ([sender.remaining intValue]==0 && [sender.refilling intValue]>0&&[sender.filling intValue]>0) {
            
            if ([sender.type isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
            }
            else
            {
                infotext =[NSString stringWithFormat:@"%@ %@\n", quantityString, strengthString];
                
            }
        }
        else if ([sender.remaining intValue]==0 && [sender.refilling isEqualToString:@" "] && [sender.refilling isEqualToString:@" "]) {
            
            infotext =[NSString stringWithFormat:@"%@ %@\n", quantityString, strengthString];
        }
        else
            
        {
            infotext =[NSString stringWithFormat:@"%@ %@\n%@", quantityString, strengthString, remainingString];
            
            
            
        }
        
        [dateit addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000] range:(NSMakeRange(0, [dateit length]))];
        
        
        [nameit addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000] range:(NSMakeRange(0, [[NSString stringWithFormat:@"%@", sender.name] length]))];
        
        NSMutableAttributedString *infoit = [[NSMutableAttributedString alloc] initWithString:infotext attributes: lightDict];
        
        
        [infoit addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000] range:(NSMakeRange(0, [infotext length]))];
        
        
        //[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000]
        
        
        [aAttrString appendAttributedString:dateit];
        [aAttrString appendAttributedString:tussen];
        [aAttrString appendAttributedString:nameit];
        [aAttrString appendAttributedString:tussen];
        [aAttrString appendAttributedString:infoit];
        
        
//        NSMutableAttributedString *str =  [[NSMutableAttributedString alloc]initWithString:@"01:05 p.m. miercoles \n30/08/2017 \n Medicamento 2 \n1 unidad Comprimindo (80 \nmg) (10 unidad izquierdo(a))"];
        
        [Name setAttributedText:aAttrString];
//                [Name setAttributedText:str];
//        [Name  setFrame:CGRectMake(95, 23, self.frame.size.width-140, 20)]; //05-09-2017
//        [Name  setFrame:CGRectMake(95, 23, self.frame.size.width-100, 20)];

        [Name sizeToFit];
        
        
//        [Name setFrame:CGRectMake(95, 10, Name.frame.size.width, Name.frame.size.height)]; //06-09-2017
        [Name setFrame:CGRectMake(95, 10, self.frame.size.width - (95 + 45), headShot.frame.size.height)];
        [Name setUserInteractionEnabled:false]; //add new line for stop scrolling.. 06-09-2017
        //    [Name setTextColor:[UIColor colorWithRed:126.0f/255.0f green:141.0f/255.0f blue:198.0f/255.0f alpha:1.0f]]; //06-07-2017 add new line for changes color
        
        //20-07-2017
        [Name setTextColor:[UIColor colorWithRed:104.0f/255.0f green:128.0f/255.0f blue:226.0f/255.0f alpha:1.0f]];
        
        //[Date setText:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:sender.firedate]]];
        
        ////////////////////////////////NSLog(@"Date %@", Date.text);
        
        [Date setTextColor:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000]];
        
        if (fmod(set,2)==0) {
            //        [DateColor setBackgroundColor:[UIColor colorWithRed:0.882 green:0.937 blue:0.957 alpha:1.000]];    // even num //06-07-2017
            
            //        [DateColor setBackgroundColor:[UIColor colorWithRed:218.0f/255.0f green:226.0f/255.0f blue:239.0f/255.0f alpha:1.000]]; //18-07-2017
            
            [DateColor setBackgroundColor:[UIColor whiteColor]];
            
        } else {
            //        [DateColor setBackgroundColor:[UIColor colorWithRed:0.733 green:0.851 blue:0.902 alpha:1.000]];
            //06-07-2017
            //        [DateColor setBackgroundColor:[UIColor colorWithRed:218.0f/255.0f green:226.0f/255.0f blue:239.0f/255.0f alpha:1.000]];
            //18-07-2017
            [DateColor setBackgroundColor:[UIColor colorWithRed:251.0f/255.0f green:241.0f/255.0f blue:235.0f/255.0f alpha:1.000]];
            
            //251, 241, 235
        }
        
        
        
        [Book.titleLabel setText:sender.id_medicine];
        [NameButton setTitle:sender.id_medicine];
        [Take setTitle:sender.id_medicine];
        [Postpone setTitle:sender.id_medicine];
        [Skip setTitle:sender.id_medicine];
        [Book setTitle:sender.id];
        
        headShot = (UIImageView *)[self viewWithTag:(130)];
        
        [headShot setImage:[UIImage imageNamed:@"Medhot.png"]];
        
        if ([UIImage imageWithContentsOfFile:sender.image])
        {
            
            [headShot setImage:[UIImage imageWithContentsOfFile:sender.image]];
        }
        
        
        if([sender.duration isEqualToString:@"Nooit"]||[sender.duration isEqualToString:NSLocalizedString(@"Never",nil)]||[sender.duration isEqualToString:@"Nie"]||[sender.duration isEqualToString:@"Jamais"]||[sender.duration isEqualToString:@"Mai"]||[sender.duration isEqualToString:@"Ποτέ"]||[sender.duration isEqualToString:@"Mai"]||[sender.duration isEqualToString:@"אף פעם"])
        {
            
        }
        else
        {
            
            NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
            [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [setdata setDateFormat:@"EE dd, MMMM yyyy 23:59:59 +0000"];
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            
            //        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[setdata dateFromString:sender.duration]]; //26-06-2017 change
            NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[setdata dateFromString:sender.duration]];
            
            
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:23];
            // Notification will fire in one minute
            [dateComps setMinute:59];
            [dateComps setSecond:59];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            
            
            ////////////////////////////////NSLog(@"%@", notif.userInfo);
            
            if  ([notif.fireDate compare:itemDate]==NSOrderedSame)
                
            {
                [Date setAlpha:1];
                [Datedone setAlpha:0];
                [Datedone setText: @""];
                
            }
            else if ([notif.fireDate compare:itemDate]==NSOrderedAscending)
                
                
            {
                [Date setAlpha:1];
                [Datedone setAlpha:0];
                [Datedone setText: @""];
                
            }
            else if ([notif.fireDate compare:itemDate]==NSOrderedDescending)
                
            {
                
                
                
                if ([sender.active isEqualToString:@"NO"]) {
                    
                    
                    [[UIApplication sharedApplication] cancelLocalNotification:notif];
                    
                    
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    
                    
                    NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
                    [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                    [dateFormatLevel setDateFormat:[appDelegate convertString:@"hh:mm a"]];
                    
                    
                    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                    [dateConvert setDateFormat:@"EE dd, MMMM yyyy"];
                    
                    
                    NSDateFormatter *format = [[NSDateFormatter alloc] init];
                    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                    
                    [format setDateFormat:@"dd/MM/yyyy"];
                    
                    
                    
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                    [dateFormat setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm:a"]];
                    
                    
                    
                    ////NSLog(@"duration %@", sender.duration);
                    
                    
                    
                    
                    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] init];
                    
                    
                    UIFont *arialBold = [UIFont boldSystemFontOfSize:14];
                    UIFont *ariallight = [UIFont systemFontOfSize:12];
                    
                    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: arialBold forKey:NSFontAttributeName];
                    
                    NSDictionary *lightDict = [NSDictionary dictionaryWithObject: ariallight forKey:NSFontAttributeName];
                    
                    
                    NSMutableAttributedString *dateit = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"Ended on",nil), [format stringFromDate:[dateConvert dateFromString:sender.duration]], [dateFormatLevel stringFromDate:sender.firedate]] stringByReplacingOccurrencesOfString:@"(null) " withString:@""] attributes: lightDict];
                    
                    
                    
                    
                    
                    NSMutableAttributedString *nameit = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", sender.name] attributes: arialDict];
                    
                    NSMutableAttributedString *tussen = [[NSMutableAttributedString alloc] initWithString:@"\n" attributes: arialDict];
                    
                    NSString *infotext;
                    
                    
                    if ([sender.remaining intValue]==0 && [sender.refilling intValue]>0&&[sender.filling intValue]>0) {
                        
                        if ([sender.type isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
                        }
                        else
                        {
                            infotext =[NSString stringWithFormat:@"%@ %@\n", quantityString, strengthString];
                            
                        }
                    }
                    else if ([sender.remaining intValue]==0 && [sender.refilling isEqualToString:@" "] && [sender.refilling isEqualToString:@" "]) {
                        
                        infotext =[NSString stringWithFormat:@"%@ %@\n", quantityString, strengthString];
                    }
                    else
                        
                    {
                        infotext =[NSString stringWithFormat:@"%@ %@\n%@", quantityString, strengthString, remainingString];
                        
                        
                        
                    }
                    
                    [dateit addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:(NSMakeRange(0, [dateit length]))];
                    
                    
                    [nameit addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.145 green:0.498 blue:0.557 alpha:1.000] range:(NSMakeRange(0, [[NSString stringWithFormat:@"%@", sender.name] length]))];
                    
                    NSMutableAttributedString *infoit = [[NSMutableAttributedString alloc] initWithString:infotext attributes: lightDict];
                    
                    
                    [infoit addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000] range:(NSMakeRange(0, [infotext length]))];
                    
                    
                    //[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000]
                    
                    
                    
                    [aAttrString appendAttributedString:dateit];
                    [aAttrString appendAttributedString:tussen];
                    [aAttrString appendAttributedString:nameit];
                    [aAttrString appendAttributedString:tussen];
                    [aAttrString appendAttributedString:infoit];
                    
                    
                    
                    [Name setAttributedText:aAttrString];
                    
                    
                    [Postpone setAlpha:0.1];
                    [Take setAlpha:0.1];
                    [Skip setAlpha:0.1];
                    [edit setAlpha:0.1];
                    
                    [Postpone setEnabled:NO];
                    [Take setEnabled:NO];
                    [Skip setEnabled:NO];
                    [edit setEnabled:NO];
                    
                    
                    [Date setAlpha:0];
                    [Datedone setAlpha:1];
                    
                    
                }
                
            }
            
            
            
            
        }
        
        
        if ([sender.remaining isEqualToString:@" "]) {
            
            [RanOut setTextColor:[UIColor clearColor]];
        }
        else
        {
            if ([sender.remaining integerValue]==0) {
                
                [RanOut setTextColor:[UIColor redColor]];
            }
            else
            {
                [RanOut setTextColor:[UIColor clearColor]];
                
            }
        }
        
        Book.chosenCourse=sender;
        
        
        NameButton.chosenCourse=sender;
        
        
        
        edit.chosenCourse=sender;
        
        
        
        Take.chosenCourse =sender;
        
        
        
        
        Postpone.chosenCourse =sender;
        
        
        
        
        Skip.chosenCourse  =sender;
        
    }
    
    
    
}
-(BOOL)SeeifEndingDateIsPast:(UILocalNotification *)notification {
    
    
    
    BOOL set;
    
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd 00:00:01 PM +0000"];
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc]init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd hh:mm:ss a +0000"];
    
    
    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateConvert setDateFormat:@"EE dd, MMMM yyyy"];
    
    
    NSDate *let =[dateConvert dateFromString:[notification.userInfo valueForKey:@"Duration"]];
    
    
    
    NSDate *get =[dateConvert dateFromString:[notification.userInfo valueForKey:@"Starting"]];
    
    if ([NSLocalizedString([notification.userInfo valueForKey:@"Duration"],nil) isEqualToString:NSLocalizedString(@"Never",nil)])
    {
        set = YES;
        
    }
    else
    {
        if  ([let compare:get]==NSOrderedSame)
            
        {
            
            set = NO;
            
            
            
            
            
            
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
            
            
        }
        else if  ([let compare:get]==NSOrderedDescending)
            
            
        {
            
            
            set = YES;
            
        }
        else {
            
            
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
            
            set = NO;
        }
    }
    
    return set;
}




-(NSDate*)calculatenow

{
    
    NSDate *now = [[NSDate alloc] init];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:now];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:now];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]+1];
    [dateComps setMinute:[timeComponents minute]];
    [dateComps setSecond:0];
    
    
    return [calendar dateFromComponents:dateComps];
}



-(void)getparent:(MedicineViewController*)set

{
    lostView =set;
}

#pragma mark - Google Analytics - Action

-(void)addActionForGoogleAnalytics:(NSString *)actionName
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"button_press"
                                                           label:actionName
                                                           value:nil] build]];
}


@end
