//
//  MedChoiceView.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 11-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMedecineController.h"

@interface MedChoiceView : UIView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) NewMedecineController *parantit;
@property (nonatomic, retain) NSMutableArray *ListOfMedicine;
@property (nonatomic, retain) UITableView *table;
@property (nonatomic, retain) NSString *check;


-(void)getparant:(UIViewController*)sender;
@end
