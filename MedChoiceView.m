//
//  MedChoiceView.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 11-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "MedChoiceView.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "SaveData.h"
#import "SaveCore.h"

@implementation MedChoiceView
@synthesize parantit;
@synthesize ListOfMedicine;
@synthesize table;
@synthesize check;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        ListOfMedicine =[[NSMutableArray alloc]initWithArray:[GetData getMedicine]];
        
        [self setBackgroundColor:[UIColor whiteColor]];
        
        
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 40, 300, self.bounds.size.height-44)];
        table.separatorColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
        table.backgroundColor = [UIColor clearColor];
        table.rowHeight =260;
        table.delegate = self;
        table.dataSource = self;
        [self addSubview:table];
    
        
        
        NSArray *Cell1 =[NSArray arrayWithObjects:@"Edit#83",@"Select#88",@"New#82",nil];
        
       for (NSInteger i=0 ; i<[Cell1 count]; i++) {
            
            
            NSArray *numbers = [[Cell1 objectAtIndex:i] componentsSeparatedByString:@"#"];
            
            
            UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(99*i, 0,  102, 40)];
            [NameButton setTitle:[numbers objectAtIndex:0] forState:UIControlStateNormal];
            [NameButton setTitleColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000] forState:UIControlStateNormal];
            [NameButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
            [NameButton setTag:140+i];
            [NameButton setBackgroundColor:[UIColor whiteColor]];
            [NameButton.layer setBorderWidth:1.5];
            [NameButton.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
            [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:NameButton];
            
            
        
            
            double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    
    [table reloadData];
});
            
        }
        
        
    }
    return self;
}



- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        
         //////////////////////////////////////////////////////////NSLog(@"editingStyle");
        
        [SaveData RemovetMedice:[[ListOfMedicine valueForKey:@"ID_Medicine"] objectAtIndex:indexPath.row]];
     
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", [[ListOfMedicine valueForKey:@"ID_Medicine"] objectAtIndex:indexPath.row]];
        
        
              NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
        
       for (NSInteger i =0; i < ([notificationArray count]); i++) {
      
            UILocalNotification *notif = [notificationArray objectAtIndex:i];
            
          
            [[UIApplication sharedApplication] cancelLocalNotification:notif];
            
            break;
            
            
        }
        
        
     
        
        
        [ListOfMedicine removeObjectAtIndex:indexPath.row];
        
        
        [table reloadData];
        
        [table setEditing:NO animated:YES];
        [table setEditing:NO];
        
        
        
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        
        
        
    }
    
    
}



-(void) Action:(UIButton*)sender

{
    
   for (NSInteger i=0 ; i<=3; i++) {
        
        UIButton *setimage2 = (UIButton *)[self viewWithTag:140+i];
        [setimage2 setBackgroundColor:[UIColor whiteColor]];
        
    }
    
     [sender setBackgroundColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:0.4]];
    
    if ([sender.titleLabel.text isEqualToString:@"Edit"]) {
        
        if (table.editing ==YES) {
            [table setEditing:NO animated:YES];
            [table setEditing:NO];
            
               [sender setBackgroundColor:[UIColor whiteColor]];
        }
        else
        {
        [table setEditing:YES animated:YES];
        [table setEditing:YES];
            
               [sender setBackgroundColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:0.4]];
            
        }
        
    }
    if ([sender.titleLabel.text isEqualToString:@"Select"]) {
       
        
        
        [parantit.scrollViewSpread setAlpha:1];
        [parantit setItemsChangeID:[SaveCore getMedicinewithID:check]];
        [parantit editOrShow:@"Edit"];
        [self setAlpha:0];
        
    }
    
    if ([sender.titleLabel.text isEqualToString:@"New"]) {
        
          [parantit.scrollViewSpread setAlpha:1];
         [self setAlpha:0];
        
    }
    
    
}

-(void)getparant:(UIViewController*)sender

{
    
    parantit=(NewMedecineController*)sender;
    
    double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){


    
     [table reloadData];
    
    
});
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [ListOfMedicine count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 40;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    check = [[ListOfMedicine valueForKey:@"ID_Medicine"] objectAtIndex:indexPath.row];

     [table reloadData];
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString *identifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    
    //cell.accessoryType=UITableViewCellAccessoryNone;
    
      [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    
	if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
      
        
    }
    
    else
        
    {
        
        [cell.textLabel setText:[[ListOfMedicine valueForKey:@"Name"] objectAtIndex:indexPath.row]];
        
        if ([check isEqualToString:[[ListOfMedicine valueForKey:@"ID_Medicine"] objectAtIndex:indexPath.row]]) {
          
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
            
        {
            
        }
      
        
    }

    
    return cell;
    
    
}


@end
