//
//  MedSyncManager.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "MedTicketSyncManager.h"
#import "AppDelegate.h"

@implementation MedTicketSyncManager 
@synthesize Setcontent;
@synthesize setNames;
@synthesize citys;
@synthesize Result;
@synthesize theConnection;
@synthesize currentElement;
@synthesize item;
@synthesize count;
@synthesize setName;
@synthesize term;
@synthesize webData;
@synthesize xmlParser;


-(MedTicketSyncManager*)init{
	self = [super init];
	if(self){
        
        
        
       [self GetID:@""];
        
    }
    
    return self;
}

-(void)GetID:(NSString*)termit

{
    
    
    
    Setcontent = [[NSMutableArray alloc] init];
    citys = [[NSMutableArray alloc] init];
    setName=@"AuthenticateUserResult";
    
    setNames = [[NSMutableArray alloc] initWithObjects:@"AuthenticateUserResult", nil];
    
  //03-08-20107
    /*
    NSString *soapMessage = [NSString stringWithFormat:
                             
                             @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
                             "<soapenv:Header/>"
                             "<soapenv:Body>"
                             "<tem:AuthenticateUser>"
                             "<!--Optional:-->"
                             "<tem:Username>marco</tem:Username>"
                             "<!--Optional:-->"
                             "<tem:Password>marcodm</tem:Password>"
                             "</tem:AuthenticateUser>"
                             "</soapenv:Body>"
                             "</soapenv:Envelope>"];
    
    //To suppress the leak in NSXMLParser
    NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://tempuri.org/IDosingWS/AuthenticateUser" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:
     [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    //http://mdmedcare.com
    
    theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    webData = [[NSMutableData data] retain];
 */
     
}


-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	
    [webData setLength: 0];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[webData appendData:data];
    
    unsigned char byteBuffer[[webData length]];
    [webData getBytes:byteBuffer];
    
    
	
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{

  
    xmlParser = [[NSXMLParser alloc] initWithData: webData];
    [xmlParser setDelegate: self];
    [xmlParser setShouldResolveExternalEntities: YES];
    [xmlParser parse];//start parsing
    [connection release];
   
   
    
    
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
}

-(void) parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
{
    NSString *temp = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];

    
   for (NSInteger i =0; i < [setNames count]; i++) {
        
        if ([currentElement isEqualToString:[setNames objectAtIndex:i]]) {
            
            
            [[Setcontent objectAtIndex:i] appendString:temp];
            
            
        }
    }
    
    
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    
    currentElement = [elementName copy];
    if ([elementName isEqualToString:setName]) {
        
        item = [[NSMutableDictionary alloc] init];
        [Setcontent removeAllObjects];
        
       for (NSInteger i =0; i < [setNames count]; i++) {
            
            NSMutableString *Setit= [[NSMutableString alloc] init];
            [Setcontent addObject:Setit];
            
        }
        
    }
    
    
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
	
    
    
    if ([elementName isEqualToString:setName]) {
        
        
       for (NSInteger i =0; i < [setNames count]; i++) {
            
            [item setObject:[Setcontent objectAtIndex:i] forKey:[setNames objectAtIndex:i]];
            
            
        }
        
        [citys addObject:item];
        
        
    
        
        [item release];
		
    }
	
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    
    
    string = [string stringByReplacingOccurrencesOfString: @"\"" withString:@""];
    string = [string stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    string = [string stringByReplacingOccurrencesOfString: @"\t" withString:@""];
    
    
    
   for (NSInteger i =0; i < [setNames count]; i++) {
        
        if ([currentElement isEqualToString:[setNames objectAtIndex:i]]) {
            
            [[Setcontent objectAtIndex:i] appendString:string];
            
            
        }
        
        
        
        
    }
    
}

-(void)parser:(NSXMLParser*)myParser {
    
    NSAutoreleasePool *schedulePool = [[NSAutoreleasePool alloc] init];
    
    BOOL success = [myParser parse];
    
    if(success) {
        
        
        
    } else {
        
        
        
    }
    
    [schedulePool drain];
    
    [myParser release];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    
 
    
    
    if ([citys count] >0) {
        
        
        
         [[NSUserDefaults standardUserDefaults] setObject:[[citys valueForKey:@"AuthenticateUserResult"]objectAtIndex:0] forKey: @"AuthenticateUserResult"];
        
        ////////////NSLog(@"AuthenticateUserResult %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"]);
        
        
     
    
    }
    else
    {
        
        ////////////NSLog(@"AuthenticateUserResult %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"]);
        
        
        
    }
    
    
    

    
    
      }




-(NSString *) change: (NSString *) dumb
{
    if ([dumb isEqualToString:@""]) {
        
        dumb=@"-";
        
        return dumb;
        
    }
    
    else
    {
        
        return dumb;
        
    }
    
}





@end
