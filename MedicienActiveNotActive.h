//
//  TimeScedulare.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "MedicineViewController.h"

@interface MedicienActiveNotActive : UITableView < UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) MedicineViewController *parantIt;
@property (nonatomic, retain) NSArray *CombineActive;
@property (nonatomic, retain) NSArray *CombineNonActive;
@property (nonatomic, retain) NSMutableArray *Total;
-(void)setactive:(MedicineViewController*)set;

@end
