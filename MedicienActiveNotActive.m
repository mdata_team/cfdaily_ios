//
//  TimeScedulare.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "MedicienActiveNotActive.h"
#import "SchedulareCell.h"
#import "SchedulareCellAdd.h"
#import <QuartzCore/QuartzCore.h>
#import "FrequencyViewControllerNew.h"
#import "AppDelegate.h"
#import "MedCell.h"
#import "Notification.h"

@implementation MedicienActiveNotActive
@synthesize fetchedResultsController =_fetchedResultsController;
@synthesize parantIt;
@synthesize CombineNonActive;
@synthesize CombineActive;
@synthesize Total;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        

        
    
    }
    return self;
}


-(void)setactive:(MedicineViewController*)set

{

  
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   
    parantIt =set;
    

    self.managedObjectContext=nil;
  
    
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    
    
        
    [Total removeAllObjects];
    
    
    
    
    [self reloadData];
    [self fetchedResultsControllers];
   
 
    
    
    NSMutableDictionary *GoActive = [[NSMutableDictionary alloc] init];
    [GoActive setObject:CombineActive forKey:@"active"];
    
    NSMutableDictionary *GoNonActive = [[NSMutableDictionary alloc] init];
    [GoNonActive setObject:CombineNonActive forKey:@"active"];
    
  
    [Total addObject:GoActive];
    
    [Total addObject:GoNonActive];
  

    [self setDelegate:self];
    [self setDataSource:self];


    
    
    ////NSLog(@"Total %@", Total);
    


    [self reloadData];
   
}


-(void) fetchedResultsControllers
{
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
   
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Medicine"
                                        inManagedObjectContext:[myApp managedObjectContext]]];
    
    
    
    NSError *error = nil;
 
    
    
    NSArray *fetchedObjects = [myApp.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
   for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        
        Medicine *cours =[fetchedObjects objectAtIndex:i];
        
     
        if ([cours.active isEqualToString:@"YES"]) {
            
          
            
            NSArray *sortDescriptors = nil;
            
            NSMutableArray *parr = [NSMutableArray array];
            
            
            [parr addObject:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(id contains [cd]'%@')", myApp.CurrentID]]];
            
            
            [parr addObject:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(active contains [cd]'%@')", @"YES"]]];
            
           
            
            NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
            
            
            
            
            [fetchRequest setPredicate:predicate];
            
            NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"active"
                                                                            ascending:NO];
            
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firedate" ascending:YES];
            
            
            sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor2,sortDescriptor, nil];
            
            
            
            [fetchRequest setSortDescriptors:sortDescriptors];
            
            
            
           CombineActive = [myApp.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
       
         
            
            
        }
        else
        {
            
   

            
            NSArray *sortDescriptors = nil;
            
            NSMutableArray *parr = [NSMutableArray array];
            
            
            [parr addObject:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(id contains [cd]'%@')", myApp.CurrentID]]];
            
            
           [parr addObject:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(active contains [cd]'%@')", @"NO"]]];
            
            
            NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
            
            
            
            
            [fetchRequest setPredicate:predicate];
            
            NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"active"
                                                                            ascending:NO];
            
            
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firedate" ascending:NO];
            
            
            sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor2,sortDescriptor, nil];
            
            
            [fetchRequest setSortDescriptors:sortDescriptors];
            
            
           CombineNonActive = [myApp.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
     
            
         
            
            
            
           
            
        }
        
        
    }
    
    
    
    
    
 
    
}

/*

-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller


{
    
    ////////////////////////////NSLog(@"controllerWillChangeContent");
    
    [self beginUpdates];
    
    
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller


{
    ////////////////////////////NSLog(@"controllerDidChangeContent");
    
    [self endUpdates];
    
    
}

-(void) controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    

    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
            
              _fetchedResultsController.delegate = nil;
            
            [self deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
    }
    
}
 
 */



#pragma mark - Table view delegate



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section

{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    return [Total count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *dictionary = [Total objectAtIndex:section];
    NSArray *array = [dictionary objectForKey:@"active"];
    
    return [array count];
}

- (MedCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MedCell *cell = (MedCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[MedCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    NSDictionary *dictionary = [Total objectAtIndex:indexPath.section];
    NSArray *array = [dictionary objectForKey:@"active"];
        
    Medicine *cours =[array objectAtIndex:indexPath.row];
   
    NSMutableArray *ary = [NSMutableArray array];
    
    [[SHARED_APPDELEGATE array_AllMedicine] removeAllObjects];
    
    for (int i=0; i<array.count; i++)
    {
        Medicine *cours1 =[array objectAtIndex:i];
        [ary addObject:cours1];
    }

    [SHARED_APPDELEGATE setArray_AllMedicine:ary];
    
//    NSLog(@"array_AllMedicine :: %@",[SHARED_APPDELEGATE array_AllMedicine]);
    
    [cell FillAllItems:cours index:indexPath.row];
    
    [cell getparent:parantIt];
        
    cell.Info.editable=NO;
    cell.Name.editable=NO;
    
    return cell;
}




- (NSString *) reuseIdentifier {
    
    ////////////////////////////////////////////NSLog(@"what");
    
    return @"CheckListTableView";
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   UILabel *sectionHeader2;
    
   sectionHeader2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 1)];
    sectionHeader2.backgroundColor = [UIColor colorWithRed:0.631 green:0.663 blue:0.675 alpha:1.000];
    sectionHeader2.textColor = [UIColor clearColor];
    sectionHeader2.font = [UIFont boldSystemFontOfSize:16];
    //sectionHeader2.text = [[[self.fetchedResultsController sections] objectAtIndex:section]name];
    sectionHeader2.textAlignment=NSTextAlignmentCenter;
    
    [sectionHeader2 setHidden:NO];
    
    return sectionHeader2;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] name];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return NO;
}

    
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSManagedObjectContext *contex =self.managedObjectContext;
        Medicine *cours =[self.fetchedResultsController objectAtIndexPath:indexPath];
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        [myApp removeHistory:cours.id_medicine];
        [myApp removeNotification:cours.id_medicine];
        
        [contex deleteObject:cours];
        
        NSError *error = nil;
        if (![contex save:&error]) {
            
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


@end
