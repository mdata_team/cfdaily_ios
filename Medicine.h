//
//  Medicine.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 09-10-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Medicine : NSManagedObject

@property (nonatomic, retain) NSString * audio;
@property (nonatomic, retain) NSString * changeDate;
@property (nonatomic, retain) NSString * coloralpha;
@property (nonatomic, retain) NSString * colorblue;
@property (nonatomic, retain) NSString * colorGreen;
@property (nonatomic, retain) NSString * colorRed;
@property (nonatomic, retain) NSString * dates;
@property (nonatomic, retain) NSString * day_number;
@property (nonatomic, retain) NSString * doctor;
@property (nonatomic, retain) NSString * dose_unit;
@property (nonatomic, retain) NSString * duration;
@property (nonatomic, retain) NSString * email_doctor;
@property (nonatomic, retain) NSString * email_pharmacy;
@property (nonatomic, retain) NSString * filling;
@property (nonatomic, retain) NSDate * firedate;
@property (nonatomic, retain) NSString * frequency;
@property (nonatomic, retain) NSString * frequency_text;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * id_medicine;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * instructions;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSData * notifications;
@property (nonatomic, retain) NSString * perscriptionImage;
@property (nonatomic, retain) NSString * pharmacy;
@property (nonatomic, retain) NSString * phone_doctor;
@property (nonatomic, retain) NSString * phone_pharmacy;
@property (nonatomic, retain) NSString * prescription;
@property (nonatomic, retain) NSString * profile;
@property (nonatomic, retain) NSString * quantity;
@property (nonatomic, retain) NSString * recording;
@property (nonatomic, retain) NSString * refilling;
@property (nonatomic, retain) NSString * remaining;
@property (nonatomic, retain) NSString * schedule;
@property (nonatomic, retain) NSString * starting;
@property (nonatomic, retain) NSDate * starting1;
@property (nonatomic, retain) NSString * strength;
@property (nonatomic, retain) NSString * strength_unit;
@property (nonatomic, retain) NSString * times;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * active;
@property (nonatomic, retain) NSString * send;




-(NSArray*)AllKeys;
@end
