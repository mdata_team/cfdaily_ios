//
//  Medicine.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 09-10-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "Medicine.h"


@implementation Medicine

@dynamic audio;
@dynamic changeDate;
@dynamic coloralpha;
@dynamic colorblue;
@dynamic colorGreen;
@dynamic colorRed;
@dynamic dates;
@dynamic day_number;
@dynamic doctor;
@dynamic dose_unit;
@dynamic duration;
@dynamic email_doctor;
@dynamic email_pharmacy;
@dynamic filling;
@dynamic firedate;
@dynamic frequency;
@dynamic frequency_text;
@dynamic id;
@dynamic id_medicine;
@dynamic image;
@dynamic instructions;
@dynamic location;
@dynamic message;
@dynamic name;
@dynamic notes;
@dynamic notifications;
@dynamic perscriptionImage;
@dynamic pharmacy;
@dynamic phone_doctor;
@dynamic phone_pharmacy;
@dynamic prescription;
@dynamic profile;
@dynamic quantity;
@dynamic recording;
@dynamic refilling;
@dynamic remaining;
@dynamic schedule;
@dynamic starting;
@dynamic starting1;
@dynamic strength;
@dynamic strength_unit;
@dynamic times;
@dynamic type;
@dynamic active;
@dynamic send;



-(NSArray*)AllKeys

{
    return [NSArray arrayWithObjects:@"audio",@"changeDate",@"coloralpha",@"colorblue",@"colorGreen",@"colorRed",@"dates",@"day_number",@"doctor",@"dose_unit",@"duration",@"email_doctor",@"email_pharmacy",@"filling",@"firedate",@"frequency",@"frequency_text",@"id",@"id_medicine",@"image",@"instructions",@"location",@"message",@"name",@"notes",@"notifications",@"perscriptionImage",@"pharmacy",@"phone_doctor",@"phone_pharmacy",@"prescription",@"profile",@"quantity",@"recording",@"refilling",@"remaining",@"schedule",@"starting",@"strength",@"strength_unit",@"times",@"type",@"active",@"send",nil];
}


@end
