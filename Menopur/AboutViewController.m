//
//  InstructiefilmViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "AboutViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "InstructiefilmViewController.h"
#import "VragenViewController.h"
#import "BijsluiterViewController.h"
#import "AppDelegate.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
@synthesize toolbarDown;
@synthesize setbackground;
@synthesize Film_logo;
@synthesize Logo_pdf;
@synthesize info;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];


//    /87 × 59 pixels
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 87/1.9, 59/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



    UIImage *image = [UIImage imageNamed:@"Logo_bar.png"];
	UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0, 0,136, 34);

    self.navigationItem.titleView = imageView;

    
    setbackground =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [setbackground setBackgroundColor:[UIColor colorWithRed:0.627 green:0.851 blue:0.961 alpha:1.000]];

    [self.view addSubview:setbackground];

    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = setbackground.layer.bounds;
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                         nil];
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.1f],
                            [NSNumber numberWithFloat:0.2f],
                            [NSNumber numberWithFloat:0.3f],
                            [NSNumber numberWithFloat:0.4f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.6f],
                            nil];



    [setbackground.layer addSublayer:shineLayer];

    toolbarDown = [[UIToolbar alloc] init];
    toolbarDown.frame = CGRectMake(0, self.view.frame.size.height-88, self.view.frame.size.width, 44);
    toolbarDown.tintColor = [UIColor colorWithRed:0.204 green:0.714 blue:0.816 alpha:1.000];




    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];



    info = [UIButton buttonWithType:UIButtonTypeCustom];
    [info setTag:44];
    [info setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [info addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    info.layer.cornerRadius = 2;
    [info setTag:22];
    [info setImage:[UIImage imageNamed:@"Info_Logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *info_button = [[UIBarButtonItem alloc] initWithCustomView:info];




    Logo_pdf = [UIButton buttonWithType:UIButtonTypeCustom];
    [Logo_pdf setTag:44];
    [Logo_pdf setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [Logo_pdf addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
      Logo_pdf.layer.cornerRadius = 2;
    [Logo_pdf setTag:23];
    [Logo_pdf setImage:[UIImage imageNamed:@"Logo_pdf.png"] forState:UIControlStateNormal];
    UIBarButtonItem *logo_button = [[UIBarButtonItem alloc] initWithCustomView:Logo_pdf];



        //Film_Logo.png


    Film_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    [Film_logo setTag:44];
    [Film_logo setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [Film_logo addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    Film_logo.layer.cornerRadius = 2;
    [Film_logo setTag:24];
    [Film_logo setImage:[UIImage imageNamed:@"Film_Logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *film_button = [[UIBarButtonItem alloc] initWithCustomView:Film_logo];


    UIScrollView *scrollViewSpread = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
    [scrollViewSpread setCanCancelContentTouches:NO];
    scrollViewSpread.showsHorizontalScrollIndicator = YES;
    [scrollViewSpread setBackgroundColor:[UIColor whiteColor]];
    scrollViewSpread.delaysContentTouches=YES;
    scrollViewSpread.clipsToBounds = NO;
    scrollViewSpread.scrollEnabled = YES;
    scrollViewSpread.pagingEnabled = YES;
    [scrollViewSpread setDelegate:self];
    scrollViewSpread.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.view  addSubview:scrollViewSpread];


   


    NSArray *items2 = [NSArray arrayWithObjects:
                       film_button, flexItem,logo_button,flexItem,info_button,
                       nil];
    toolbarDown.items = items2;
    
    
    [self.view addSubview:toolbarDown];


    UILabel *MerkenKopLabel= [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-290)/2, 10, 290, 20)];
    MerkenKopLabel.backgroundColor = [UIColor clearColor];
    [MerkenKopLabel setText: @"Over Ferring"];
    MerkenKopLabel.font = [UIFont boldSystemFontOfSize:15];
    [MerkenKopLabel setNumberOfLines:30];
    [MerkenKopLabel setTextAlignment:NSTextAlignmentLeft];
    MerkenKopLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
    [MerkenKopLabel setBackgroundColor:[UIColor clearColor]];
    [scrollViewSpread   addSubview:MerkenKopLabel];


    UILabel *MerkenStaartLabel= [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-290)/2, 315, 290, 20)];
    MerkenStaartLabel.backgroundColor = [UIColor clearColor];
    [MerkenStaartLabel setText: @"Ferring B.V."];
    MerkenStaartLabel.font = [UIFont boldSystemFontOfSize:15];
    [MerkenStaartLabel setNumberOfLines:30];
    [MerkenStaartLabel setTextAlignment:NSTextAlignmentLeft];
    MerkenStaartLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
    [MerkenStaartLabel setBackgroundColor:[UIColor clearColor]];
    [scrollViewSpread   addSubview:MerkenStaartLabel];


    UILabel *MerkenLabel= [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-290)/2, 10, 290, 420)];
    MerkenLabel.backgroundColor = [UIColor clearColor];
    [MerkenLabel setText: @"\n\nFerring is een biofarmaceutische organisatie gericht op onderzoek, ontwikkeling en innovatie van geneesmiddelen.\nOnze deskundigheid richt zich op het ontdekken, ontwikkelen en op de markt brengen van geneesmiddelen voor de volgende aandachtsgebieden:\n\n• Ongewilde kinderloosheid\n• Veilig bevallen\n• Gastrointestinale aandoeningen\n• Groeistoornissen\n• Bedplassen\n• Prostaatkanker\n\n\nPostbus 184\n2130 AD Hoofddorp\nTel. 023 – 5680300\ninfoNL@ferring.com\nwww.ferring.nl"];
    MerkenLabel.font = [UIFont systemFontOfSize:14];
    [MerkenLabel setNumberOfLines:30];
    [MerkenLabel setTextAlignment:NSTextAlignmentLeft];
    MerkenLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
    [MerkenLabel setBackgroundColor:[UIColor clearColor]];
    [scrollViewSpread   addSubview:MerkenLabel];



    
    

    [MerkenLabel setAlpha:1];


    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod"] || [deviceType isEqualToString:@"iPod touch"] || [deviceType isEqualToString:@"iPhone Simulator"]) {


    UIImageView *background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Ferring.png"]];
    [background setFrame:CGRectMake(200, 315, 205/2, 93/2)];
    [background setBackgroundColor:[UIColor clearColor]];
    [scrollViewSpread addSubview:background];

    }

    else
        {

        UIImageView *background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Ferring.png"]];
        [background setFrame:CGRectMake(400, 315, 205/2, 93/2)];
        [background setBackgroundColor:[UIColor clearColor]];
        [scrollViewSpread addSubview:background];
        
        }

     [scrollViewSpread setContentSize:CGSizeMake(320, 524)];

        //Ferring.png


    
	// Do any additional setup after loading the view.
}

-(void)PresentView:(UIButton*)sender

{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    for (int i =1; i < [[[self navigationController] viewControllers] count]; i++) {

        UIViewController *controller = [[[self navigationController] viewControllers] objectAtIndex:i];
        [[self navigationController] popToViewController:controller animated:NO];


    }




    int64_t delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

        if (sender.tag ==22) {



            VragenViewController *controller = [[VragenViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];

            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];






        }
        if (sender.tag ==23) {


            BijsluiterViewController *controller = [[BijsluiterViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];

            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];

        }
        if (sender.tag ==24) {



            InstructiefilmViewController *controller = [[InstructiefilmViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];
            
            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];
            
            
        }
        
        
    });
    
    
}

-(void) turnbuttons:(NSString*) setter

{
 NSLog(@"turnbuttons");
}

-(void) backAction
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
