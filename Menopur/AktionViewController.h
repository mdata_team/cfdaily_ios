//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CodePopView, SettingsViewcontroller, TekstIDLabel;
@interface AktionViewController : UIViewController<UITextViewDelegate> {
    
    
    UITextView *TitleText;
    NSString *Compaire;
    UIView *Combi;
    UIView *CombiOther;
    NSString *chosen;
    NSString *chosenID;
    NSString *theTime;
    NSString *theDay;
    NSString *theDate;
}
@property (nonatomic, retain) NSString *theDate;
@property (nonatomic, retain) IBOutlet UITextView *underTitleText;
@property (nonatomic, retain) IBOutlet NSString *timeframe;
@property (nonatomic, retain) IBOutlet  NSMutableArray *TimeChoses;
@property (nonatomic, retain) NSString *theTime;
@property (nonatomic, retain) NSString *theDay;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) SettingsViewcontroller *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;


-(void)setChoice:(NSString*)gothere;
-(void)change:(UIButton*)sender;
-(void) setTextview: (NSString *) text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;

@end
