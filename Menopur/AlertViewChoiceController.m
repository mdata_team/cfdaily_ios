//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "AlertViewChoiceController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewProfileController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
@interface AlertViewChoiceController ()

@end

@implementation AlertViewChoiceController

@synthesize Vraagantwoord;
@synthesize table;

//
- (void)viewDidLoad
{
    [super viewDidLoad];

       [self.view.layer setMasksToBounds:YES];

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];

    


//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
           if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
               [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
               a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



    UIBarButtonItem *edit = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                             target:self
                             action:@selector(CreateAccount)];
//    edit.style = UIBarButtonItemStyleBordered; //26-06-2017
    edit.style = UIBarButtonItemStylePlain;
   
    
  //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
      if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [edit setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }
    


    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = edit;


    table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0, 320, self.view.bounds.size.height-44)];
    table.separatorColor = [UIColor clearColor];
    table.backgroundColor = [UIColor clearColor];
    table.rowHeight =260;
    table.delegate = self;
    table.dataSource = self;
    [table setEditing:NO];
    [table setEditing:NO];
    [self.view addSubview:table];




}

-(void)switchValueReminder:(UISwitch*)sender

{

}






-(void) backAction
{

    
    //popViewControllerAnimated

    [[self navigationController] popViewControllerAnimated:YES];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)index{

    if (index==1) {


        NewProfileController *controller = [[NewProfileController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];

    }

    else
        {



        
        }
        }

-(void) CreateAccount

{
    NewProfileController *controller = [[NewProfileController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];

}
-(void)PresentView:(UIButton*)sender

{
    
}

- (IBAction)instructie:(UIButton *)sender {


}

- (IBAction)Vragen:(UIButton *)sender {


    

        //VragenViewController
    
}


- (IBAction)bijsluiter:(UIButton *)sender {


    

    
}

- (IBAction)about:(UIButton *)sender {

 
    
}


- (IBAction)Cancel:(UIButton *)sender {


}

- (IBAction)OK:(UIButton *)sender {

 
   
 
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 2;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(ProfileCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {


    


}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

  

    return 50;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{


    

    [tableView reloadData];




}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *identifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

	if (cell == nil) {

        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    else
        
        {
        
        }
    
    
    return cell;
    
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{

}

@end
