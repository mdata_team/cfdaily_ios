//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CodePopView,TekstIDLabel, Profiles;
@interface AlertViewController : UIViewController<UITextViewDelegate, UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) IBOutlet UITableView *table;
@property (nonatomic, retain) IBOutlet UILabel *extra;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) TekstIDLabel *parantLabelCOPY;

- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)whatLabel:(UILabel*)set;
-(void)setItems:(Profiles*)set;
@end
