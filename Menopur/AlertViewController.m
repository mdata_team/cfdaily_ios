//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "AlertViewController.h"
#import "AlertViewChoiceController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewProfileController.h"
#import "MusicViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "GetData.h"
#import "ActionArrow.h"
#import "Notification.h"
#import <AudioToolbox/AudioServices.h>
@interface AlertViewController ()

@end

@implementation AlertViewController

@synthesize Vraagantwoord;
@synthesize table;
@synthesize extra;
@synthesize parantLabel;
@synthesize parantLabelCOPY;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //     [self.view.layer setMasksToBounds:YES];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationItem.title =  NSLocalizedString(@"Alert Sound:", nil);
    


    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
          if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:20.0];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            
            [label sizeToFit];

        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;


    
    
    
    NSArray *names3 =[NSArray arrayWithObjects:@"Silent",@"Alert Sound:", nil];
    NSArray *Cell3 =[NSArray arrayWithObjects:@"Silence#100",@"Alert#100", nil];

   for (NSInteger i=0 ; i<[names3 count]; i++) {


        UIView *Schdules = [[UIView alloc] initWithFrame:CGRectMake(10, 10+(50*i), self.view.frame.size.width-20, 40)];
        [Schdules setBackgroundColor:[UIColor whiteColor]];
        [Schdules.layer setCornerRadius:10];
        [Schdules.layer setBorderWidth:1.5];
        [Schdules setTag:333+i];
        
   
        
//        [Schdules.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //27-06-2017 changes
       [Schdules.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];

        [Schdules.layer setMasksToBounds:YES];
        [self.view addSubview:Schdules];
        
        //////////////////////////////////////////////NSLog(@"%@", [NSString stringWithFormat:@"   %@", NSLocalizedString([names3 objectAtIndex:i], nil)]);

        NSArray *numbers = [[Cell3 objectAtIndex:i] componentsSeparatedByString:@"#"];



        UILabel *MedName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-20, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //27-06-2017 changes
       [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:15]];
        [MedName setText:[NSString stringWithFormat:@"   %@", NSLocalizedString([names3 objectAtIndex:i], nil)]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
//        [MedName.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //27-06-2017 changes
       [MedName.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];

        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Schdules addSubview:MedName];

        
    
        if (i>=0 && i<1) {
    
            if ([appDelegate.currentProfile.silence isEqualToString:@"NO"]) {
                
                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                ////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
                
                  if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
                    
                    
                UISwitch *swActive = [[UISwitch alloc] initWithFrame:CGRectMake(245, 5, 100,50)];
                [swActive setTag:100+i];
             
//                [swActive setOnTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes   //101, 184, 136
//                      [swActive setOnTintColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                      //11-07-2017
                      [swActive setOnTintColor:[UIColor colorWithRed:101.0f/255.0f green:184.0f/255.0f blue:136.0f/255.0f alpha:1.000]];

                [swActive setOn:FALSE animated:FALSE];
                [swActive addTarget:self action:@selector(switchValueReminder:) forControlEvents:UIControlEventValueChanged];
                [Schdules addSubview:swActive];
                }
                
                else
                {
                    
                    UISwitch *swActive = [[UISwitch alloc] initWithFrame:CGRectMake(215, 8, 100,50)];
                    [swActive setTag:100+i];
                    
//                    [swActive setOnTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];//18-07-2017
                    [swActive setOnTintColor:[UIColor colorWithRed:101.0f/255.0f green:184.0f/255.0f blue:136.0f/255.0f alpha:1.000]];
                    
                    [swActive setOn:FALSE animated:FALSE];
                    [swActive addTarget:self action:@selector(switchValueReminder:) forControlEvents:UIControlEventValueChanged];
                    [Schdules addSubview:swActive];
                    
                }
               
            }
            else
                
            {
                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                ////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
                
                 if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
                    
                
                UISwitch *swActive = [[UISwitch alloc] initWithFrame:CGRectMake(245, 5, 100,50)];
                [swActive setTag:100+i];
//                [swActive setOnTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];//18-07-2017
                     [swActive setOnTintColor:[UIColor colorWithRed:101.0f/255.0f green:184.0f/255.0f blue:136.0f/255.0f alpha:1.000]];

                [swActive setOn:TRUE animated:FALSE];
                [swActive addTarget:self action:@selector(switchValueReminder:) forControlEvents:UIControlEventValueChanged];
                [Schdules addSubview:swActive];
                }
                
                else
                    
                {
                    
                    
                    UISwitch *swActive = [[UISwitch alloc] initWithFrame:CGRectMake(215, 8, 100,50)];
                    [swActive setTag:100+i];
//                    [swActive setOnTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];//18-07-2017
                    [swActive setOnTintColor:[UIColor colorWithRed:101.0f/255.0f green:184.0f/255.0f blue:136.0f/255.0f alpha:1.000]];

                    [swActive setOn:TRUE animated:FALSE];
                    [swActive addTarget:self action:@selector(switchValueReminder:) forControlEvents:UIControlEventValueChanged];
                    [Schdules addSubview:swActive];
                    
                }
              
            }
      
            
           

        }
        else
            {
                
                parantLabel = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 0, 260, 40)];
                [parantLabel setTextColor:[UIColor blackColor]];
                [parantLabel setTextAlignment:NSTextAlignmentRight];
                [parantLabel setFont:[UIFont systemFontOfSize:14]];
                [parantLabel setText:NSLocalizedString(@"",nil)];
                [parantLabel setTitle:[numbers objectAtIndex:0]];
                [parantLabel setTextColor:[UIColor blackColor]];
                [parantLabel setTag:120];
                [parantLabel setBackgroundColor:[UIColor clearColor]];
                [parantLabel setNumberOfLines:3];
                [Schdules addSubview:parantLabel];


            ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Schdules.frame.size.width-35, 3, 60/2, 72/2)];
            [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
            [NameButton setTitleit:[numbers objectAtIndex:0]];
            [NameButton setTag:222];
            [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
            [Schdules addSubview:NameButton];
         
            
            
                extra= parantLabel;
            
            }
       
       if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
           
           
           //NSLog(@"left");
           Schdules.transform = CGAffineTransformMakeScale(-1, 1);
           
           for (UIView *view in Schdules.subviews) {
               
               MedName.transform = CGAffineTransformMakeScale(-1, 1);
               [MedName setTextAlignment:NSTextAlignmentRight];
               
               parantLabel.transform = CGAffineTransformMakeScale(-1, 1);
               [parantLabel setTextAlignment:NSTextAlignmentLeft];
              
               for (UIButton *label in view.subviews) {
                   
                   
                   if ([label isKindOfClass:[UIButton class]]) {
                       
                       label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                       [label.titleLabel setTextAlignment:NSTextAlignmentLeft];
                       
                   }
                   
               }
               
           }
           
           //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
           
           
       } else {
           
           //NSLog(@"right");
           //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
       }

      
    }
  
    
    

}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];



   
    

}


-(void)setItems:(Profiles*)set



{
    
    //////////////////////////////////////////////NSLog(@"%@", set);
    
   double delayInSeconds = 0.2;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    [extra setText:set.audio];
    
    
    if ([set.silence isEqualToString:@"NO"]) {
        
        
        UISwitch *headShot = (UISwitch *)[self.view viewWithTag:(100)];
        [headShot setOn:FALSE animated:FALSE];
        
        
        UIView *doel = (UIView *)[self.view viewWithTag:(334)];
        [doel setAlpha:1];
       
       
    }
    else if ([set.silence isEqualToString:@"YES"]) {
        
        UISwitch *headShot = (UISwitch *)[self.view viewWithTag:(100)];
        
        [headShot setOn:TRUE animated:FALSE];
     
        UIView *doel = (UIView *)[self.view viewWithTag:(334)];
        [doel setAlpha:0];
        
    }
    
    
});
    
  
}

-(void)Save:(UIBarButtonItem*) sender

{
    
    
    
}

-(void)switchValueReminder:(UISwitch*)sender
{
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (sender.isOn) {
        
        
        if (sender.tag ==100) {
            
         appDelegate.currentProfile.silence = @"YES";
            
               UIView *headShot = (UIView *)[self.view viewWithTag:(334)];
             [headShot setAlpha:0];
            
          
        }
        else  if (sender.tag ==101) {
            
            
            appDelegate.currentProfile.vibrate = @"YES";
         
        }
          
    }
    
    else
    {
        if (sender.tag ==100) {
            
         
            
            appDelegate.currentProfile.silence = @"NO";
            
            
            
            UIView *headShot = (UIView *)[self.view viewWithTag:(334)];
            [headShot setAlpha:1];
            
            
            
        }
        else  if (sender.tag ==101) {
            
            
               appDelegate.currentProfile.vibrate = @"NO";
            
            
           
             
        }
        
 
      
    }
 
   
}

-(void)Action:(ActionArrow*)sender

{
    

    MusicViewController *controller = [[MusicViewController alloc]init];
    [controller whatLabel:parantLabel];
    [self.navigationController pushViewController:controller animated:YES];
}





-(void) backAction
{
   
    [parantLabelCOPY setText:parantLabel.text];
    [[self navigationController] popViewControllerAnimated:YES];
    
    
  
 
}



-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{


    if (index==1) {


        NewProfileController *controller = [[NewProfileController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];

    }

    else
        {



        
        }
        }

-(void) CreateAccount

{
    NewProfileController *controller = [[NewProfileController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];

}
-(void)PresentView:(UIButton*)sender

{
    
}

- (IBAction)instructie:(UIButton *)sender {


}

- (IBAction)Vragen:(UIButton *)sender {


    

        //VragenViewController
    
}


- (IBAction)bijsluiter:(UIButton *)sender {


    

    
}

- (IBAction)about:(UIButton *)sender {

 
    
}


- (IBAction)Cancel:(UIButton *)sender {


}

- (IBAction)OK:(UIButton *)sender {

 
   
 
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 2;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(ProfileCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {



}

-(void)whatLabel:(UILabel*)set

{
    
    
    parantLabelCOPY =(TekstIDLabel*)set;;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

  

    return 50;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{


    



    [tableView reloadData];




}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {




    NSString *identifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

	if (cell == nil) {

        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    else
        
        {
        
        }
    
    
    return cell;
    
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{
}

@end
