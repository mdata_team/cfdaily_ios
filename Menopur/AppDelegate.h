//
//  AppDelegate.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
// bundle iD : com.dasquire.app
//New Bundle id : com.mdata.cfdailydev
// new bundle id :: com.mdata.cfdaily - when use appstore certificate
#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <sqlite3.h>
#import "LeveyPopColorView.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "NHMailActivity.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
//#import "LoadingView.h"
#import "MDataViewController.h"
#import "Notifications.h"
#import "Profiles.h"
#import "Medicine.h"
#import "History.h"
#import <CoreLocation/CoreLocation.h>
#import "GetpaswordSyncManager.h"

#import <GoogleAnalytics/GAI.h> // for Google Analytics
#import "GAIFields.h"

#import "DBManager.h"
#import "WebServicesClass.h"

#define SHARED_APPDELEGATE (AppDelegate *)[[UIApplication sharedApplication] delegate]

//#define GOOGLEANALYTIC_TRACKING_ID @"UA-107515212-1" // old account
#define GOOGLEANALYTIC_TRACKING_ID @"UA-111031451-1" // new account
#define APIURL @"http://mdata.gr/CFDaily_Database/add_device.php"
#define term_and_condition_version_APIURL @"http://mdataapps.com/cfdaily/api.php?action=get_tc_version"

@class StartViewController;
@class BufferedNavigationController, AlertPop, TakeOrTookPop, SkipPop, MultipleViewController, PostponePop, MedSyncManager, MedTicketSyncManager, InsertPatientSyncManager, InsertHistorySyncManager, InsertMedSyncManager,ValidTicketSyncManager,ProcesIt;
@interface AppDelegate : UIResponder <UIPickerViewDelegate,UIApplicationDelegate,UIAlertViewDelegate, UIDocumentInteractionControllerDelegate, NSStreamDelegate, MFMailComposeViewControllerDelegate, NHCalendarActivityDelegate, AVAudioPlayerDelegate, AVAudioRecorderDelegate, AVAudioSessionDelegate,NSFetchedResultsControllerDelegate, CLLocationManagerDelegate, UIPickerViewAccessibilityDelegate>

{
    	UIBackgroundTaskIdentifier bgTask;
    Reachability *hostReach;

    enum
    {
    ENC_AAC = 1,
    ENC_ALAC = 2,
    ENC_IMA4 = 3,
    ENC_ILBC = 4,
    ENC_ULAW = 5,
    ENC_PCM = 6,
    } encodingTypes;
}

-(BOOL)isNetworkReachable;
- (void)sendHitsInBackground;

@property(nonatomic, copy) void (^dispatchHandler)(GAIDispatchResult result);

//new define 9-11-2017
@property (strong ,nonatomic) NSString *strMedtype;
@property (strong ,nonatomic) NSMutableDictionary *dicMedicine;
@property (strong ,nonatomic) NSMutableArray *array_AllMedicine;

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, assign) CLLocationCoordinate2D myLoction;
@property (nonatomic, assign) CLLocationCoordinate2D chosenLocation;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) float sizeWidth;
@property (nonatomic, assign) float sizeHeight;
@property (nonatomic, retain) CLLocation *locationNow;

//strength_unit
@property (nonatomic, retain) History *currentHistory;
@property (nonatomic, retain) Profiles *currentProfile;
@property (nonatomic, retain) NSString *chosenID;
@property (nonatomic, retain) NSString *strength_unit;
@property (nonatomic, retain) NSString *dose_unit;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) Medicine *chosenCourse;
@property (nonatomic, retain) ProcesIt *progressBar;
//@property (nonatomic, retain) LoadingView *loadingview;
@property (nonatomic, retain) MedTicketSyncManager *MedTicket;
@property (nonatomic, retain) InsertHistorySyncManager *insertHistory;
@property (nonatomic, retain) InsertPatientSyncManager *insertPatient;
@property (nonatomic, retain) ValidTicketSyncManager *validTicket;
@property (nonatomic, assign) NSInteger classit;
@property (nonatomic, retain) UIToolbar *toolbarDown2;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *PostponeButton;
@property (nonatomic, retain) NSString *strApplicationUUID;
@property (nonatomic, retain) GetpaswordSyncManager *insertPasword;



@property (nonatomic, retain) InsertMedSyncManager *insertMeds;
@property (nonatomic, retain) UINavigationController *Copynavigaton;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) MedSyncManager *medView;
@property (nonatomic, retain) NSMutableArray *SelectedCells;
@property (nonatomic, retain) NSMutableArray *selectedMeds;
@property (nonatomic, retain) SkipPop *skiptake;
@property (nonatomic, retain) PostponePop *postpone;
@property (nonatomic, retain) MDataViewController *mdataViewController;
@property (nonatomic, retain) MultipleViewController *Multicontroller;
@property (nonatomic, retain) TakeOrTookPop *taketook;
@property (nonatomic, retain) AlertPop *alert;
@property (nonatomic, retain) NSString *chosenURL;
@property (nonatomic, retain) NSString *chosenMusic;
@property (nonatomic, retain) NSString *Notevariable;
@property (nonatomic, retain) NSString *recordingURL;
@property (nonatomic, retain) NSString *recordingMusic;
@property (nonatomic, retain) IBOutlet UIView *Postponeview;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbarDown;
@property (nonatomic, retain) NSMutableDictionary *CurrentReminder;
@property (nonatomic, retain) NSTimer *onTimer;
@property (nonatomic, assign, getter=isPlayingNow) BOOL Editing;
@property (nonatomic, assign, getter=isPlayingNow) BOOL SavingDef;
@property (nonatomic, assign, getter=isPlayingNow) BOOL playingNow;
@property (nonatomic, assign, getter=isNotify) BOOL Notify;
@property (nonatomic, assign) int recordEncoding;
@property (nonatomic, retain) NSString *kRemindMeNotificationDataKey;
@property (nonatomic, retain) NSString *CurrentID;
@property (nonatomic, retain) NSString *PerscriptionNotes;
@property (nonatomic, retain) NSString *Notes;
@property (nonatomic, assign, getter=isVol) BOOL Vol;
@property (nonatomic, assign, getter=isActive) BOOL Active;
@property (nonatomic, assign, getter=isFromPop) BOOL FromPop;
@property (nonatomic, assign, getter=isFromScreen) BOOL FromScreen;
@property (nonatomic, retain) NSString *TitleID;
@property (nonatomic, retain) NSString *TypeMed;
@property (nonatomic, retain) NSString *timeString;
@property (nonatomic, retain) NSString *DateString;
@property (nonatomic, retain) ALAssetsLibrary *library;
@property (assign, nonatomic) NSUInteger Selected;
@property (nonatomic, assign, getter=isInternet) BOOL Internet;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) BufferedNavigationController *navigationController;
@property (strong, nonatomic) StartViewController *viewController;
@property (nonatomic, retain) Reachability *curReach;
@property (nonatomic, retain) IBOutlet UIProgressView *myProgressIndicator;
@property (nonatomic, retain) NSMutableArray *Navigaioncopy;
@property (nonatomic, retain) NSMutableArray *Rememberdates;
@property (nonatomic, assign) float currentProgress;
@property (nonatomic, retain) NSMutableArray *photos;
@property (nonatomic, retain) NSMutableDictionary *Item;
@property (nonatomic, retain) NSMutableArray *Profile;
@property (nonatomic, retain) UIButton *button;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer;
@property (nonatomic, assign, getter=isRecoringNow) BOOL recoringNow;
@property (nonatomic, assign, getter=isTakeall) BOOL takeall;
@property (nonatomic, retain) AVAudioRecorder *audioRecorder;
@property (nonatomic, retain) UIDatePicker *pickerTaal;
@property (nonatomic, retain) NSDate *Constrant;
@property (nonatomic, retain) NSString *Choisestring;
@property (nonatomic, retain) UILabel *title;
@property (nonatomic, assign) NSInteger Countnote;
@property (nonatomic, retain) NSMutableArray *curentChoise;
@property (nonatomic, retain) NSString *postponedtime;
@property (nonatomic, retain) NSString *quantityString;
@property (nonatomic, retain) NSString *remainingString;
@property (nonatomic, retain) NSString *strengthString;
@property (nonatomic, retain) NSString *Country;
@property (nonatomic, assign) BOOL akertactive;

@property (nonatomic, strong) DBManager *dbManager; 



-(void)undoHistory:(UILocalNotification*)set;
-(void)ShowAlert:(NSString*)Titel Messageit:(NSString*)Message Custimize:(NSString*)How parant:(UIViewController*)set;
-(void)seeifiRunOut:(NSDictionary*)sender;
-(void)deleteAllalerts;
-(void)seeIfNotification;
-(void)TakenOrtook;
-(void)reloadTableview:(NSString*)set;
-(void)Next:(UIButton*)sender;
+(sqlite3*)getNewDBconnection;
+(sqlite3*)getNewDBconnectionWith:(NSString*)set;
-(void)createDatabaseIfNeeded;
-(void)createDatabaseIfNeededWith:(NSString*)set;
-(void)save:(NSString*)url setName:(NSString*)name;
-(void)finischedit;
-(void)finisched:(UIViewController*)parant;
- (NSString *)newUUID;
-(void)getticket;
-(void)TicketValid;
-(void)goSeeIfHistorySend;
- (void)SendtheMail;
- (void)SendtheMailtoAdress:(NSString*)mail subject:(NSString*)subjectit with:(UINavigationController*)parant;
- (void)SendtheMailtoAdress:(NSString*)mail;
-(void)Play;
-(void)SkipIt:(UIButton*)sender;
-(void)SkipMoreAction:(UIBarButtonItem*)sender;
-(void)SkipMore:(UIButton*)sender;
-(void)setitemsDictionary:(NSString*)set name:(NSString*)name;
-(void)Postponeit:(UIBarButtonItem*)sender;
-(void)Postpone:(UIButton*)sender;
-(void)TakenPostoneUp:(BOOL)UpDown;
-(void)TakenSkipUp:(BOOL)UpDown;
-(void)TakenOrtookUp:(BOOL)UpDown;
-(void)PostNotfication:(BOOL)UpDown;
-(void)Take:(UIButton*)sender;
-(void)Skipped:(UIButton*)sender;
-(void)PostponeMore:(UIButton*)sender;

-(void)SetProfile:(NSString*)ID;
-(NSString*)getProfileID;
-(void)MultiPop;
-(void)SeeBadges;
-(void)doPolicy:(UINavigationController*)parant;
-(void)doMData:(UINavigationController*)parant;
-(void)Skip:(UIButton*)sender;
-(NSString*)getListMial:(NSMutableArray*)HistSectionList;
-(void)PostponeMoreAction:(UIBarButtonItem*)sender;
-(void)doWebview:(UINavigationController*)parant;
-(void)InsertEventwith:(NSMutableDictionary*)send;
-(BOOL)SeeifEndingDateIsPast:(UILocalNotification *)notification;
-(void)insertSetting;
-(void)NextMed:(UIButton*)sender;
-(BOOL)SeePostpone:(Medicine*)set;
-(void)checkifLanguageschanges;
-(void)insertPatientNew:(NSString*)termit with:(Profiles*)send;
-(void)getlocationsNow;
-(NSArray*)GetProfiles;
-(void)goSeeIfProfileSend;
-(NSString*)changesDone:(NSString*)text;
-(void)removeHistory:(NSString*)set;
-(void)removeNotification:(NSString*)set;
-(void)removeMedicine:(NSString*)set;
-(void)removeMedicinePerAccount:(NSString*)set;
-(void)removeProfile:(NSString*)set;
-(BOOL)insertProfile:(NSMutableDictionary*)set;
-(void)insertMedicine:(NSMutableDictionary*)set;
-(void)insertMedicineNew:(NSString*)termit with:(NSMutableDictionary*)send;
-(void)insertNotifications:(NSMutableDictionary*)set withdata:(NSData*)datajobs;
-(void)insertHistory:(NSMutableDictionary*)set;
-(Notifications*)GetnotificationID:(NSString*)ID;

-(Profiles*)GetProfilesWithID:(NSString*)ID;

- (void)saveContext;
-(NSString*)convertString:(NSString*)sender;
-(NSString*)convertList:(NSString*)sender;
-(void)TakenOrtook:(UIButton*)sender;


-(UIImageView *)setNavigationbarIconImage:(UINavigationController *)navigation setimge:(BOOL)isset;

@end
