//
//  AppDelegate.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#import "AppDelegate.h"
#import "BufferedNavigationController.h"
#import "HistViewController.h"
#import "HistoryViewController.h"
#import "MedsViewController.h"
#import "MedicineViewController.h"
#import "SettingsViewcontroller.h"
#import "MultipleViewController.h"
#import "GetData.h"
#import "MedicineViewController.h"
#import "RecordingAct.h"
#import "SaveData.h"
#import "AlertPop.h"
#import "TakeOrTookPop.h"
#import "Notification.h"
#import "SkipPop.h"
#import "GetData.h"
//#import "TestFlight.h"
#import "AlertCustimize.h"
#import "PostponePop.h"
#import "MedSyncManager.h"
#import "MedTicketSyncManager.h"
#import "InsertHistorySyncManager.h"
#import "InsertPatientSyncManager.h"
#import "InsertMedSyncManager.h"
#import "ThermsViewController.h"
#import "InfoViewController.h"
#import "ProcesIt.h"
#import "ValidTicketSyncManager.h"
#import "History.h"
#import "Profiles.h"
#import "Medicine.h"
#import "Notifications.h"
#import "Settings.h"
#import "History.h"
//#import "SSKeychain.h"//sn
#import "SaveCore.h"
#import "GetpaswordSyncManager.h"
#import "LoginViewController.h"

#import "StartViewController.h"



@implementation AppDelegate

@synthesize Countnote;
@synthesize Vraagantwoord;
@synthesize medView;
//@synthesize loadingview;
@synthesize navigationController;
@synthesize Internet;
@synthesize Multicontroller;
@synthesize Notevariable;
@synthesize curReach;
@synthesize pickerTaal;
@synthesize progressBar;
@synthesize window;
@synthesize myProgressIndicator;
@synthesize currentProgress;
@synthesize Selected;
@synthesize Navigaioncopy;
@synthesize library;
@synthesize timeString;
@synthesize DateString;
@synthesize TitleID;
@synthesize Vol;
@synthesize akertactive;
@synthesize photos;
@synthesize SelectedCells;
@synthesize Item;
@synthesize TypeMed;
@synthesize Notes;
@synthesize CurrentID;
@synthesize button;
@synthesize alert;
@synthesize kRemindMeNotificationDataKey;
@synthesize audioPlayer;
@synthesize recoringNow;
@synthesize audioRecorder;
@synthesize playingNow;
@synthesize recordEncoding;
@synthesize onTimer;
@synthesize CurrentReminder;
@synthesize toolbarDown;
@synthesize Active;
@synthesize chosenMusic;
@synthesize chosenURL;
@synthesize recordingMusic;
@synthesize recordingURL;
@synthesize Profile;
@synthesize Postponeview;
@synthesize taketook;
@synthesize Notify;
@synthesize skiptake;
@synthesize FromPop;
@synthesize FromScreen;
@synthesize Constrant;
@synthesize Choisestring;
@synthesize selectedMeds;
@synthesize title;
@synthesize Editing;
@synthesize curentChoise;
@synthesize postpone;
@synthesize MedTicket;
@synthesize SavingDef;
@synthesize insertHistory;
@synthesize insertPatient;
@synthesize insertMeds;
@synthesize Copynavigaton;
@synthesize validTicket;
@synthesize PostponeButton;
@synthesize toolbarDown2;
@synthesize strengthString;
@synthesize quantityString;
@synthesize remainingString;
@synthesize postponedtime;
@synthesize chosenID;
@synthesize currentProfile;
@synthesize strApplicationUUID;
@synthesize locationManager;
@synthesize myLoction;
@synthesize chosenLocation;
@synthesize coordinate;
@synthesize Country;
@synthesize locationNow;
@synthesize chosenCourse;
@synthesize currentHistory;
@synthesize insertPasword;
@synthesize sizeWidth;
@synthesize sizeHeight;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;



static NSBundle *bundle = nil;

+(void)initialize {
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString *current = [languages objectAtIndex:0];
    [self setLanguage:current];
    
}


+(void)setLanguage:(NSString *)l {
    ////////////////////NSLog(@"preferredLang: %@", l);
    NSString *path = [[ NSBundle mainBundle ] pathForResource:l ofType:@"lproj"];
    bundle = [NSBundle bundleWithPath:path];
}

+(NSString *)get:(NSString *)key alter:(NSString *)alternate {
    return [bundle localizedStringForKey:key value:alternate table:nil];
}

-(UIImageView *)setNavigationbarIconImage:(UINavigationController *)navigation setimge:(BOOL)isset
{
    UIImageView *imageView_headlogo;
    if (isset) {
        
        for (UIView *view in navigation.navigationBar.subviews) {
            
            if ([view isKindOfClass:[UIImageView class]])
            {
                UIImageView *imgview = (UIImageView *)view;
                
                NSData *data1 = UIImagePNGRepresentation(imgview.image);
                NSData *data = UIImagePNGRepresentation([UIImage imageNamed:@"headlogo"]);
                
                if ([data1 isEqual:data])
                {
                    [imgview removeFromSuperview];
                }
            }
        }

         imageView_headlogo= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"headlogo"]];
            CGSize imageSize = CGSizeMake(80, 22);
        CGFloat marginX = (navigation.navigationBar.frame.size.width / 2) - (imageSize.width / 2);
        
        imageView_headlogo.frame = CGRectMake(marginX, 12, imageSize.width, imageSize.height);
        [navigation.navigationBar addSubview:imageView_headlogo];
    
    }
    else
    {
        for (UIView *view in navigation.navigationBar.subviews) {
            
            if ([view isKindOfClass:[UIImageView class]])
            {
                UIImageView *imgview = (UIImageView *)view;
                
                NSData *data1 = UIImagePNGRepresentation(imgview.image);
                NSData *data = UIImagePNGRepresentation([UIImage imageNamed:@"headlogo"]);
                
                if ([data1 isEqual:data])
                {
                    [imgview removeFromSuperview];
                }
            }
        }
    }
    return imageView_headlogo;
}

-(void)check_term_and_condtion_version
{

    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];

    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == ReachableViaWiFi || networkStatus == ReachableViaWWAN)
    {

        [WebServicesClass.sharedWebServiceClass JsonCallGET:term_and_condition_version_APIURL WitCompilation:^(NSMutableDictionary *Dictionary) {

            NSLog(@"%@",Dictionary);

            if ([Dictionary count] != 0){

                NSString *version = [Dictionary valueForKey:@"current_version"];

                [NSUserDefaults.standardUserDefaults setObject:version forKey:@"current_version"];
            }

        }];
    }
}

-(void)apicall
{
    
//    CFUUIDRef udid = CFUUIDCreate(NULL);
//    NSString *udidString = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, udid));
    NSString *udidString = [[UIDevice currentDevice] identifierForVendor].UUIDString;
    
    NSString *strParam = [NSString stringWithFormat:@"id=%@&device=iOS&version=1.0",udidString];

    [[WebServicesClass sharedWebServiceClass]JsonCall:strParam url:APIURL WitCompilation:^(NSString *strResponse, NSError *error) {
        if (!error)
        {
            NSLog(@"strResponse : %@",strResponse);
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"APIURL_DONE"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            NSLog(@"Errorr : %@",[error localizedDescription]);
        }
    }];
    
}

#pragma mark - Appdelegate Methods

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Api call


    [self check_term_and_condtion_version];


    [self apicall];
//    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"APIURL_DONE"] == NO)
//    {
//        
//    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isNetworkReachable) name:kReachabilityChangedNotification object:nil];
    
    hostReach = [Reachability reachabilityWithHostName:@"com.google.com"];
    [hostReach startNotifier];
    
    //********** Google Analytics ****************
    
    GAI *gai = [GAI sharedInstance];
    [gai trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
    
    // Optional: automatically report uncaught exceptions.
    gai.trackUncaughtExceptions = YES;
    
    // Optional: set Logger to VERBOSE for debug information.
    // Remove before app release.
    gai.logger.logLevel = kGAILogLevelVerbose;
    
//    [[GAI sharedInstance] dispatch];
    
//    [GAI sharedInstance].dispatchInterval = 20;
    
    //********** Google Analytics ****************
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCurrencySymbol) name:NSCurrentLocaleDidChangeNotification object:nil];
    
    sizeWidth =window.frame.size.width;
    sizeHeight =window.frame.size.height;
    
    //[Fabric with:@[[Crashlytics class]]];
    
    akertactive =YES;
    
    [self registerDefaultsFromSettingsBundle];
    
    Class userNotification = NSClassFromString(@"UIUserNotificationSettings");
    
    if (userNotification)
    {
        UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    }
    
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             |UIRemoteNotificationTypeSound
                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
    
    
    Country = [[NSLocale currentLocale] localeIdentifier];
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"countryCode"])
    {
        
    }
    else
        
    {
        [[NSUserDefaults standardUserDefaults] setObject:[[NSLocale currentLocale] localeIdentifier] forKey: @"countryCode"];
        

        [[NSUserDefaults standardUserDefaults] setObject:Country forKey: @"countryCode"];
        
        
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey: @"Language"];
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"fr" forKey: @"Language"];
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
            
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"de" forKey: @"Language"];
        }
        
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
            
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"it" forKey: @"Language"];
        }
       else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"es" forKey: @"Language"];
        }

//        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
//            
//        {
//            [[NSUserDefaults standardUserDefaults] setObject:@"el" forKey: @"Language"];
//        }
//        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
//            
//        {
//            [[NSUserDefaults standardUserDefaults] setObject:@"he" forKey: @"Language"];
//        }
//        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
//            
//        {
//            [[NSUserDefaults standardUserDefaults] setObject:@"ar" forKey: @"Language"];
//        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"nl" forKey: @"Language"];
        }
        else
        {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey: @"Language"];
        }
        
        
        ////////////////NSLog(@"%@",  [[NSUserDefaults standardUserDefaults] valueForKey: @"Language"]);
        
        [self getlocationsNow];
    }
    
    
    
    
    
    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    
    NSRange range = NSMakeRange (0, 1);
    ////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"])
    {
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:[self newUUID] forKey: @"AuthenticateUserResult"];
    }
    
    ////////////////////////////////////////NSLog(@"%@", NSLocalizedString(@"Silent", nil));
    
    
    UILocalNotification *localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotification) {
    }
    else {
        
    }
    
    //[TestFlight takeOff:@"5d1b4c8f-cfd0-49c1-b461-431af9bba2bb"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@" " forKey: @"ID_Medicine"];
    
    kRemindMeNotificationDataKey = @"kRemindMeNotificationDataKey";
    
    UILocalNotification *launchNote = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (launchNote){
        
    }
    
    Notify = NO;
    
    [self createDatabaseIfNeeded];
    
    Item =[[NSMutableDictionary alloc]init];
    Profile =[[NSMutableArray alloc]init];
    CurrentReminder  =[[NSMutableDictionary alloc]init];
    
    [self TicketValid];
    
    insertPatient= [[InsertPatientSyncManager alloc]init];
    insertHistory= [[InsertHistorySyncManager alloc]init];
    insertPasword = [[GetpaswordSyncManager alloc] init];
    
    
    medView =[[MedSyncManager alloc]init];
    
    insertMeds= [[InsertMedSyncManager alloc]init];
    
    SelectedCells =[[NSMutableArray alloc]init];
    curentChoise =[[NSMutableArray alloc] init];
    selectedMeds =[[NSMutableArray alloc]init];
    
    Vraagantwoord =[[NSMutableArray alloc]init];
    
    StartViewController *rootViewController = [[StartViewController alloc] init];
    
    Navigaioncopy =[[NSMutableArray alloc]init];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        BufferedNavigationController *navController = [[BufferedNavigationController alloc] initWithRootViewController:rootViewController];
        [navController.navigationBar setTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        self.navigationController = navController;
        [navController.navigationBar setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forBarMetrics:20];
        [navController.navigationBar setBarStyle:UIBarStyleDefault];
        [navController.navigationBar setTranslucent:NO];
        self.navigationController = navController;
    }
    else
    {
        BufferedNavigationController *navController = [[BufferedNavigationController alloc] initWithRootViewController:rootViewController];
        [navController.navigationBar setTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        [navController.navigationBar setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forBarMetrics:0];
        [navController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
        self.navigationController = navController;
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = rootViewController;
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    
    photos= [[NSMutableArray alloc] init];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        toolbarDown = [[UIToolbar alloc] init];
        toolbarDown.frame = CGRectMake(-5,  self.window.frame.size.height-42,  self.viewController.view.frame.size.width+10, 44);
        toolbarDown.backgroundColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
        toolbarDown.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
        [toolbarDown setBackgroundImage:[UIImage imageNamed:@"balk_toolbar.png"] forToolbarPosition:0 barMetrics:0];
    }
    else
    {
        toolbarDown = [[UIToolbar alloc] init];
        toolbarDown.frame = CGRectMake(-5,  self.window.frame.size.height-42,  self.viewController.view.frame.size.width+10, 44);
        toolbarDown.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
        [toolbarDown setBackgroundImage:[UIImage imageNamed:@"balk_toolbar.png"] forToolbarPosition:0 barMetrics:0];
    }
    
    toolbarDown.layer.shadowOffset = CGSizeMake(3, 0);
    toolbarDown.layer.shadowOpacity = 1;
    toolbarDown.layer.shadowRadius = 1.0;
    
    NSMutableArray *listTotal=[[NSMutableArray alloc] initWithObjects:@"Icons_smile",@"Icons_send",@"Icons_Alert",@"Icons_email",@"Icons_info",@"Icons_settings",nil];
    
    NSMutableArray *arrayViews = [NSMutableArray array];
    for(NSInteger i=0; i<=[listTotal count]-1; i++)
    {
        UIButton *btnTwo = [[UIButton alloc] initWithFrame:CGRectMake(25+(50*i), 7, 30,  30)];
        [btnTwo setBackgroundColor:[UIColor clearColor]];
        [btnTwo setShowsTouchWhenHighlighted:NO];
        [btnTwo setImage:[UIImage imageNamed:[listTotal objectAtIndex:i]] forState:UIControlStateNormal];
        [btnTwo setAutoresizesSubviews:YES];
        [btnTwo addTarget:self action:@selector(Next:) forControlEvents:UIControlEventTouchUpInside];
        [btnTwo setTag:i+31];
//        UIBarButtonItem *infoButtonItem=[[UIBarButtonItem alloc] initWithCustomView:btnTwo];
//        [arrayViews addObject:infoButtonItem];
        [toolbarDown addSubview:btnTwo];
    }
    
//    [toolbarDown setItems:arrayViews];
    
    if ([[NSUserDefaults standardUserDefaults]  objectForKey:@"chosenMusic"]) {
        // chosenMusic =[[NSUserDefaults standardUserDefaults]  objectForKey:@"chosenMusic"];
        chosenURL =[[NSUserDefaults standardUserDefaults]  objectForKey:@"chosenURL"];
        recordingMusic =[[NSUserDefaults standardUserDefaults]  objectForKey:@"recordingMusic"];
        recordingURL =[[NSUserDefaults standardUserDefaults]  objectForKey:@"recordingURL"];
    }
    else
    {
        
    }

    [window addSubview:toolbarDown];
    [toolbarDown layoutSubviews];
    
    for (id view in toolbarDown.subviews)
    {
        if ([view isKindOfClass:(NSClassFromString(@"_UIToolbarContentView"))]) {
            UIView *testView = view;
            testView.userInteractionEnabled = NO;
//            [testView removeFromSuperview];
        }
    }
    
    
    if ([MFMailComposeViewController canSendMail])
        button.enabled = YES;
    
    alert =[[AlertPop alloc] initWithFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
    [alert setBackgroundColor:[UIColor clearColor]];
    alert.layer.shadowOffset = CGSizeMake(1, 1);
    alert.layer.shadowOpacity = 1;
    alert.layer.shadowRadius = 1.0;
    [window insertSubview:alert aboveSubview:window];
    [alert setMultiple];
    
    postpone =[[PostponePop alloc] initWithFrame:CGRectMake(10, window.frame.size.height, 300, 140)];
    [postpone setBackgroundColor:[UIColor clearColor]];
    postpone.layer.shadowOffset = CGSizeMake(1, 1);
    postpone.layer.shadowOpacity = 1;
    postpone.layer.shadowRadius = 1.0;
    [window insertSubview:postpone aboveSubview:window];
    [postpone.layer setMasksToBounds:YES];
    
    skiptake =[[SkipPop alloc] initWithFrame:CGRectMake(10, window.frame.size.height, 300, 140)];
    [skiptake setBackgroundColor:[UIColor clearColor]];
    skiptake.layer.shadowOffset = CGSizeMake(1, 1);
    skiptake.layer.shadowOpacity = 1;
    skiptake.layer.shadowRadius = 1.0;
    [window insertSubview:skiptake aboveSubview:window];
    
    
    taketook =[[TakeOrTookPop alloc] initWithFrame:CGRectMake(10, window.frame.size.height, 300, 150)];
    [taketook setBackgroundColor:[UIColor clearColor]];
    taketook.layer.shadowOffset = CGSizeMake(1, 1);
    taketook.layer.shadowOpacity = 1;
    taketook.layer.shadowRadius = 1.0;
    [window insertSubview:taketook aboveSubview:window];
    [taketook setMultiple];
    
    Postponeview =[[UIView alloc] initWithFrame:CGRectMake(0, window.frame.size.height-240, 320, 340)];
    //250, 241, 235
//    [Postponeview setBackgroundColor:[UIColor whiteColor]]; //04-07-2017 change
    [Postponeview setBackgroundColor:[UIColor colorWithRed:250.0f/255.0f green:241.0f/255.0f blue:235.0f/255.0f alpha:1.0f]];
    [window addSubview:Postponeview];
    
    
    
    toolbarDown2 = [[UIToolbar alloc] init];
    toolbarDown2.frame = CGRectMake(0,  0,  self.viewController.view.frame.size.width, 44);
    toolbarDown2.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
    [toolbarDown2 setBackgroundImage:[UIImage imageNamed:@"balk_toolbar.png"] forToolbarPosition:0 barMetrics:0];
    
    [Postponeview addSubview:toolbarDown2];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [toolbarDown2 setBackgroundImage:[UIImage imageNamed:@"balk_toolbar.png"] forToolbarPosition:0 barMetrics:0];
    }
    else
    {
        [toolbarDown2 setBackgroundImage:[UIImage imageNamed:@"balk_toolbar.png"] forToolbarPosition:0 barMetrics:0];
    }
    
    toolbarDown2.layer.shadowOffset = CGSizeMake(3, 0);
    toolbarDown2.layer.shadowOpacity = 1;
    toolbarDown2.layer.shadowRadius = 1.0;
    
    [Postponeview addSubview:toolbarDown2];
    
    //8-11-2017
    PostponeButton = [[UIBarButtonItem alloc]
                      initWithTitle:NSLocalizedString(@"Done ",nil) style:UIBarButtonSystemItemCancel target:self
                      action:@selector(Postponeit:)];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [PostponeButton setTintColor:[UIColor whiteColor]];
    }
    
    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:NSLocalizedString(@" Cancel", nil) style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Postponeit:)];
    
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Cancel setTintColor:[UIColor whiteColor]];
    }
    
    UIView *textit  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];
    
    title = [[UILabel alloc] initWithFrame:CGRectMake(0, 4, 160, 20)];
    
    [title setTextColor:[UIColor whiteColor]];
    [title setFont:[UIFont boldSystemFontOfSize:15]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];
    [textit addSubview:title];
    
    
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:textit];
    
    
    
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    NSDate *itemDate = [[NSDate alloc] init];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:itemDate];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:itemDate];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
    [dateComps setMinute:[timeComponents minute]+1];
    [dateComps setSecond:0];
    
    Constrant = [calendar dateFromComponents:dateComps];
    
    NSArray *itemsUp = [NSArray arrayWithObjects:Cancel,flexItem,random, flexItem,PostponeButton,nil];
    toolbarDown2.items = itemsUp;
    
    BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
    pickerTaal = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 44, 320, 300)];
    
    if (!strsetting)
    {
        ////////////NSLog(@"let op");
        
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US_POSIX",nil)];
        pickerTaal.locale = locale;
    }
    else
    {
        //NSLocalizedString(@"en_US",nil)
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)];
        pickerTaal.locale = locale;
    }
    
    pickerTaal.datePickerMode = UIDatePickerModeTime;
    [pickerTaal setDate:[calendar dateFromComponents:dateComps]];
    
    for (UIView *view in pickerTaal.subviews) {
        
        for (UILabel *label in view.subviews) {
            
            if ([label isKindOfClass:[UILabel class]])
            {
                ////////////NSLog(@"%@", label.text);
            }
            else
            {
                ////////////NSLog(@"%@", label);
            }
        }
    }
    
    [pickerTaal setTag:679];
    [pickerTaal addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [Postponeview addSubview:pickerTaal];
    
    [Postponeview setFrame:CGRectMake(0, window.frame.size.height+5, 320, 340)];
    
    [SaveData setUpDatabase:@"Action_Date" onTable:@"History"];
    
    if ([[[[GetData getSetting] valueForKey:@"History"] objectAtIndex:0] intValue]==0) {
        
        [SaveData insertSetting:@"History" setnames:@"5"];
    }
    else
    {
        
    }
    
    for (UIBarButtonItem *item in self.toolbarDown2.items)
    {
        item.enabled = NO;
    }
    
    [self insertSetting];
    [self checkifLanguageschanges];
    
    [self goSeeIfProfileSend];
    
    [self goSeeIfMedicineisSend];
    
    [self goSeeIfHistorySend];
    
    //////////////////NSLog(@"AuthenticateUserResult %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"]);
    
    
    //05-09-2017
    
//    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"Profiles.sqlite"];
    
    
    return YES;
}
-(void)infoButtonClicked {
    NSLog(@"Event called");
}
-(void) refreshCurrencySymbol
{
    NSLocale *theLocale = [NSLocale currentLocale];
    NSString *symbol = [theLocale objectForKey:NSLocaleCurrencySymbol];
    NSLog(@"Symbol : %@",symbol);
    NSString *code = [theLocale objectForKey:NSLocaleCurrencyCode];
    NSLog(@"Code : %@",code);
}

- (void)registerDefaultsFromSettingsBundle
{
    NSObject * object = [[NSUserDefaults standardUserDefaults] objectForKey:@"preferred_TIME"];
    if(object != nil){
        
        //object is there
        //NSLog(@"voll preferred_TIME %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"]);
    }
    else
    {
        BOOL set =YES;
        [[NSUserDefaults standardUserDefaults] setBool:set forKey:@"preferred_TIME"];
        //NSLog(@"null preferred_TIME %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"]);
    }
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}

-(NSString*)convertList:(NSString*)sender
{
    //sn put below line to prevent crash
    if ([sender isEqualToString:@" "]||[sender isEqualToString:@""]) {
        
        
//    if ([sender isEqualToString:@" "]) {
        
    }
    else
        
    {
        
        NSArray *timesIt = [sender componentsSeparatedByString:@","];
        
        for (NSInteger i=0 ; i<[timesIt count]; i++) {
            
            NSString *time=[timesIt objectAtIndex:i];
            
            NSDateFormatter *format24 = [[NSDateFormatter alloc] init];
            [format24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [format24 setDateFormat:@"HH:mm"];
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [format setDateFormat:@"hh:mm a"];
            
            if ([format24 dateFromString:time]) {
                
                BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
                
                if (!strsetting)
                {
                    // time = [format stringFromDate:[format24 dateFromString:time]];
                    
                    //////////////NSLog(@"NO format24 %@", [format stringFromDate:[format24 dateFromString:time]]);
                    
                    sender = [sender stringByReplacingOccurrencesOfString:time withString:[format stringFromDate:[format24 dateFromString:time]]];
                }
                else
                {
                    //////////////NSLog(@"YES format24 %@", time);
                    // time = [format stringFromDate:[format24 dateFromString:time]];
                }
                
                
            }
            else
                
            {
                BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
                
                if (!strsetting)
                {
                    
                    //////////////NSLog(@"YES format %@", time);
                }
                
                else
                {
                    
                    sender = [sender stringByReplacingOccurrencesOfString:time withString:[format24 stringFromDate:[format dateFromString:time]]];
                    
                }
                
                
            }
            
            
            
        }
        //////////////NSLog(@"sender %@", sender);
        
    }
    
    return sender;
}

-(NSString*)convertString:(NSString*)sender

{
    NSString *datestring =sender;
    
    BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
    if (!strsetting)
        
        
        return datestring;
    
    else
        datestring = [sender stringByReplacingOccurrencesOfString:@"hh:mm a" withString:@"HH:mm"];
    
    
    return datestring;
}


-(void)getlocationsNow

{
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.locationNow = [locations lastObject];
    
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:self.locationNow completionHandler:^(NSArray *placemarks, NSError *error) {
        for (CLPlacemark * placemark in placemarks) {
            
            Country =[placemark ISOcountryCode];
            
            [[NSUserDefaults standardUserDefaults] setObject:Country forKey: @"countryCode"];
            
            
            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey: @"Language"];
            }
            else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"fr" forKey: @"Language"];
            }
            else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
                
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:@"el" forKey: @"Language"];
            }
            
            else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"de" forKey: @"Language"];
            }
            else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"es" forKey: @"Language"];
            }

//            else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
//                
//            {
//                [[NSUserDefaults standardUserDefaults] setObject:@"he" forKey: @"Language"];
//            }
//            else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
//                
//            {
//                  [[NSUserDefaults standardUserDefaults] setObject:@"ar" forKey: @"Language"];
//            }
            else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
                
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"nl" forKey: @"Language"];
            }
            else
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey: @"Language"];
            }
            
            
            ////////////////NSLog(@"%@",  [[NSUserDefaults standardUserDefaults] valueForKey: @"Language"]);
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:[self getcontinent:[[NSUserDefaults standardUserDefaults] valueForKey: @"countryCode"]] forKey: @"Continent"];
            
            
            
            
            [self.locationManager stopUpdatingLocation];
            
            
            
            
            
        }
    }];
    
    
    
    
    
    
    
    
}


-(NSString*) getcontinent:(NSString*)country

{
    
    
    
    NSString * rturnstring=@"Europe";
    
    
    
    
    
    NSArray *names1 =[NSArray arrayWithObjects:@"AF,ASIA",@"AL,EUROPE",@"DE,EUROPE",@"DZ,EUROPE",@"AD,EUROPE",@"AQ,ANTARTIDA",@"AG,SOUTH_AMERICA",@"SA,ASIA",@"AR,SOUTH_AMERICA",@"AM,ASIA",@"AW,SOUTH_AMERICA",@"AU,OCEANIA",@"AZ,ASIA",@"AT,EUROPE",@"BS,SOUTH_AMERICA",@"BH,ASIA",@"BD,ASIA",@"BB,SOUTH_AMERICA",@"BZ,SOUTH_AMERICA",@"BJ,AFRICA",@"BM,SOUTH_AMERICA",@"BE,EUROPE",@"BT,ASIA",@"BE,EUROPE",@"BO,SOUTH_AMERICA",@"BW,AFRICA",@"BA,EUROPE",@"BR,SOUTH_AMERICA",@"BN,ASIA",@"BG,EUROPE",@"BF,AFRICA",@"BI,AFRICA",@"KH,ASIA",@"CM,AFRICA",@"CA,NORTH_AMERICA",@"CV,AFRICA",@"BQ,SOUTH_AMERICA",@"AF,ASIA",@"CO,SOUTH_AMERICA",@"KM,AFRICA",@"CG,AFRICA",@"CD,AFRICA",@"KP,ASIA",@"KR,ASIA",@"CR,SOUTH_AMERICA",@"CI,AFRICA",@"HR,EUROPE",@"CU,SOUTH_AMERICA",@"CW,SOUTH_AMERICA",@"DK,EUROPE",@"DJ,AFRICA",@"DM,SOUTH_AMERICA",@"EG,AFRICA",@"SV,SOUTH_AMERICA",@"EC,SOUTH_AMERICA",@"ER,AFRICA",@"SK,EUROPE",@"SI,EUROPE",@"ES,EUROPE",@"US,NORTH_AMERICA",@"EE,EUROPE",@"ET,AFRICA",@"FJ,OCEANIA",@"PH,ASIA",@"FI,EUROPE",@"FR,EUROPE",@"GA,AFRICA",@"GM,AFRICA",@"GE,EUROPE",@"GH,AFRICA",@"GI,EUROPE",@"GD,SOUTH_AMERICA",@"GL,NORTH_AMERICA",@"GP,SOUTH_AMERICA",@"GF,SOUTH_AMERICA",@"GU,AFRICA",@"GT,SOUTH_AMERICA",@"GG,EUROPE",@"GN,AFRICA",@"GW,AFRICA",@"GQ,AFRICA",@"GY,SOUTH_AMERICA",@"HT,SOUTH_AMERICA",@"HN,SOUTH_AMERICA",@"HN,EUROPE",@"IE,ASIA",@"BV,ANTARTIDA",@"CX,OCEANIA",@"HM,ANTARTIDA",@"AF,OCEANIA",@"IM,EUROPE",@"RE,AFRICA",@"KY,SOUTH_AMERICA",@"CC,OCEANIA",@"CK,OCEANIA",@"FO,EUROPE",@"GS,ANTARTIDA",@"FK,SOUTH_AMERICA",@"MP,OCEANIA",@"MH,OCEANIA",@"UM,OCEANIA",@"PN,SOUTH_AMERICA",@"SB,OCEANIA",@"TC,SOUTH_AMERICA",@"VG,SOUTH_AMERICA",@"VI,SOUTH_AMERICA",@"ID,ASIA",@"IR,ASIA",@"IQ,ASIA",@"IE,EUROPE",@"IS,EUROPE",@"IL,ASIA",@"IT,EUROPE",@"IN,ASIA",@"JM,SOUTH_AMERICA",@"JP,ASIA",@"JE,EUROPE",@"JO,ASIA",@"KZ,ASIA",@"KE,AFRICA",@"KG,ASIA",@"KI,OCEANIA",@"KW,ASIA",@"LA,ASIA",@"LV,EUROPE",@"LR,AFRICA",@"LI,EUROPE",@"LT,EUROPE",@"LB,ASIA",@"LY,AFRICA",@"LU,EUROPE",@"MK,EUROPE",@"MG,AFRICA",@"MW,AFRICA",@"MV,ASIA",@"ML,AFRICA",@"MT,EUROPE",@"MA,AFRICA",@"MQ,SOUTH_AMERICA",@"MU,AFRICA",@"MR,AFRICA",@"YT,AFRICA",@"MX,NORTH_AMERICA",@"FM,OCEANIA",@"MZ,AFRICA",@"MD,EUROPE",@"MN,ASIA",@"ME,EUROPE",@"MS,SOUTH_AMERICA",@"MC,EUROPE",@"MM,ASIA",@"NA,AFRICA",@"NR,OCEANIA",@"NP,ASIA",@"NI,SOUTH_AMERICA",@"NG,AFRICA",@"NU,OCEANIA",@"NE,AFRICA",@"NO,EUROPE",@"NC,OCEANIA",@"NZ,OCEANIA",@"OM,ASIA",@"NL,EUROPE",@"PK,ASIA",@"PW,OCEANIA",@"PS,ASIA",@"PA,SOUTH_AMERICA",@"PG,OCEANIA",@"PY,SOUTH_AMERICA",@"PE,SOUTH_AMERICA",@"PF,OCEANIA",@"PL,EUROPE",@"PT,EUROPE",@"PR,SOUTH_AMERICA",@"QA,ASIA",@"HK,ASIA",@"MO,ASIA",@"GB,EUROPE",@"CF,EUROPE",@"DO,SOUTH_AMERICA",@"CZ,EUROPE",@"ZA,AFRICA",@"RO,EUROPE",@"RU,EUROPE",@"RW,AFRICA",@"UM,SOUTH_AMERICA",@"KN,SOUTH_AMERICA",@"SH,AFRICA",@"LC,SOUTH_AMERICA",@"MF,SOUTH_AMERICA",@"VC,SOUTH_AMERICA",@"PM,NORTH_AMERICA",@"WS,OCEANIA",@"AS,OCEANIA",@"SM,EUROPE",@"EH,AFRICA",@"ST,AFRICA",@"SN,AFRICA",@"SC,AFRICA",@"RS,EUROPE",@"SL,AFRICA",@"SG,AS", @"SR,SOUTH_AMERICA",@"SJ,EUROPE",@"SZ,AFRICA",@"TJ,ASIA",@"TH,ASIA",@"TW,ASIA",@"TZ,AFRICA",@"IO,OCEANIA",@"TF,OCEANIA",@"TL,ASIA",@"TG,AFRICA",@"TK,OCEANIA",@"TO,OCEANIA",@"TT,SOUTH_AMERICA",@"TN,AFRICA",@"TM,ASIA",@"TR,ASIA",@"TV,OCEANIA",@"TD,AFRICA",@"UA,EUROPE",@"UG,AFRICA",@"AE,ASIA",@"UY,SOUTH_AMERICA",@"UZ,ASIA",@"VZ,OCEANIA",@"VA,EUROPE",@"VE,SOUTH_AMERICA",@"VN,ASIA",@"WF,OCEANIA",@"CL,SOUTH_AMERICA",@"CN,ASIA",@"CY,EUROPE",@"ZM,AFRICA",@"ZW,AFRICA",nil];
    
    for (NSInteger i=0 ; i<[names1 count]; i++) {
        
        
        NSArray *numbers2 = [[names1 objectAtIndex:i] componentsSeparatedByString:@","];
        
        
        if ([country isEqualToString:[numbers2 objectAtIndex:0]]) {
            
            
            rturnstring = [numbers2 objectAtIndex:1];
            
            
        }
        
    }
    
    
    
    
    return rturnstring;
    
}
-(void)checkifLanguageschanges
{
    
    //AppleLanguages
    
    NSArray *go = [[NSUserDefaults standardUserDefaults] valueForKey:@"AppleLanguages"];
    
    NSString *strLang=[go objectAtIndex:0];
    
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"chosenLanguges"]) {
        
        
        
        
    }
    else
    {
        
        
        [[NSUserDefaults standardUserDefaults] setObject:strLang forKey: @"chosenLanguges"];
    }
    
    
    
}

-(void)InsertEventwith:(NSMutableDictionary*)send;

{
    //03-08-2017
//    [insertHistory InsertEventwith:send];
}

-(void)TicketValid

{
    
    validTicket =[[ValidTicketSyncManager alloc]init];
    
}

-(void)getticket

{
    
    MedTicket =[[MedTicketSyncManager alloc]init];
}





-(void)insertPatientNew:(NSString*)termit with:(Profiles*)send;

{
    
    [insertPatient insertPatientNew:[[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"] with:send];
    
}

- (void)methodCalled :(NSNotification*)aNotification{
    
    
}


-(void) loadingviewOff: (float) fix
{
    
    /*
     
     [loadingview
     performSelector:@selector(removeView)
     withObject:nil
     afterDelay:fix];
     
     */
    
    
    
    
    
}
-(void) loadingviewOn
{
    /*
     
     [loadingview
     performSelector:@selector(removeViewIn)
     withObject:nil
     afterDelay:0.0];
     
     */

    
}
-(void)doWebview:(UINavigationController*)parant
{
//    // ****************** googleAnalyticsGroupping ********************
//    
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:4]value:@"Visit website"];
//    
//    // ****************** googleAnalyticsGroupping ********************
    
    
    //NSLocalizedString(@"http://www.cfmedcare.eu",nil)
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"http://www.cfmedcare.eu",nil)]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"https://www.cfdaily.eu",nil)]];

    //
}

-(void)doMData:(UINavigationController*)parant
{
    
//    // ************************* googleAnalyticsGroupping ************************
//    
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:4]value:@"About Developer"];
//    
//    // ************************* googleAnalyticsGroupping ************************

    
    
    [toolbarDown setAlpha:0];
    MDataViewController *controller = [[MDataViewController alloc]init];
    [parant pushViewController:controller animated:YES];
}

-(void)doPolicy:(UINavigationController*)parant
{
    [toolbarDown setAlpha:0];
    
//    // ********************* googleAnalyticsGroupping ********************
//    
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:4]value:@"Terms and Privacy Policy"];
//    
//    // ********************* googleAnalyticsGroupping ********************
    
    ThermsViewController *controller = [[ThermsViewController alloc]init];
    [parant pushViewController:controller animated:YES];
    
}

-(void)deleteAllalerts

{
    
    ////////////////////////////////////////////////////NSLog(@"deleteAllalerts");
    
    for (NSInteger i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
        
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        [[UIApplication sharedApplication] cancelLocalNotification:notif];
        
        
    }
    
    
}



#pragma mark - getListMial

-(NSString*)getListMial:(NSMutableArray*)HistSectionList

{
    
    
    
    
    
    ////////////////////////////////////////////////NSLog(@"%@", HistSectionList);
    
    
    
    NSString *increments = @"";
    
    
    
    if ([HistSectionList count] ==0) {
        
    }
    else
        
    {
        
        
        for (NSInteger i=0; i < [HistSectionList count]; i++) {
            
            
            
            NSArray *typit = [[GetData getDrugTypeStrength:[[HistSectionList objectAtIndex:i] valueForKey:@"Type"]] objectAtIndex:0];
            
            
            if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Type"] isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
                
                quantityString =[NSString stringWithFormat:@"%@", [[HistSectionList objectAtIndex:i] valueForKey:@"Drug_types"]];
                
                //20-11-2017
                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] ||                         [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                {
                    quantityString =[NSString stringWithFormat:@"%@", [[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
                }
            }
            else
                
            {
                
                
                if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"] intValue]>1) {
                    
                    if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                        
                        if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"]) {
                            
                            quantityString =[NSString stringWithFormat:@"%@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            
                        }
                        else
                        {
                            
                            quantityString =[NSString stringWithFormat:@"%@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"], [typit valueForKey:@"Drug_types"]];
                            //20-11-2017
                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                            {
                                quantityString =[NSString stringWithFormat:@"%@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
                                
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                            }

                        }
                        
                        
                    }
                    
                    else
                        
                    {
                        if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"]) {
                            
                            quantityString =[NSString stringWithFormat:@"%@ %@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"],[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
                            
                            //20-11-2017
                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                        else
                            
                            
                        {
                            quantityString =[NSString stringWithFormat:@"%@ %@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"],[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"], [typit valueForKey:@"Drug_types"]];
                            
                            //20-11-2017
                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                            {
                                quantityString =[NSString stringWithFormat:@"%@ %@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"],[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];

                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                                
                                if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"] intValue] > 1)
                                {
                                    if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Type"] isEqualToString:@"Tablet"])
                                    {
                                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablet" withString:@"Tabletter"];
                                    }
                                    
                                    if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Type"] isEqualToString:@"Spray"])
                                    {
                                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprays"];
                                    }
                                }
                            }
                            
                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                            {
                                quantityString =[NSString stringWithFormat:@"%@ %@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"],[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
                                
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                                
                                if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"] intValue] > 1)
                                {
                                    if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Type"] isEqualToString:@"Tablett"])
                                    {
                                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                                    }
                                    
                                    if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Type"] isEqualToString:@"Spray"])
                                    {
                                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprayer"];
                                    }
                                }
                            }
                        }
                    }
                    
                }
                else {
                    
                    if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                        
                        ////////////////////////////////////////////////////////NSLog(@"piece");
                        
                        
                        if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Other"]) {
                            
                            quantityString =[NSString stringWithFormat:@"%@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"],[[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
                            
                            //20-11-2017
                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                            
                        }
                        
                        else
                            
                            
                        {
                            
                            
                            quantityString =[NSString stringWithFormat:@"%@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"], [typit valueForKey:@"Drug_less"]];
                            
                            //20-11-2017
                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                            {
                                quantityString =[NSString stringWithFormat:@"%@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];

                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                    }
                    
                    else
                        
                    {
                        
                        if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Other"]) {
                            
                            
                            quantityString =[NSString stringWithFormat:@"%@ %@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"],[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
                            
                            
                            //20-11-2017
                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                        else
                            
                        {
                            
                            quantityString =[NSString stringWithFormat:@"%@ %@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"],[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"], [typit valueForKey:@"Drug_less"]];
                            
                            //20-11-2017
                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                            {
                                quantityString =[NSString stringWithFormat:@"%@ %@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"],[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];

                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                    }
                    
                }
                
            }
            
            
            
            if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Strength"] intValue]>0) {
                
                strengthString =[NSString stringWithFormat:@"%@ %@", [[HistSectionList objectAtIndex:i] valueForKey:@"Strength"] , [[HistSectionList objectAtIndex:i] valueForKey:@"Strength_unit"]];
                
            }
            else {
                
                strengthString =@" ";
                
            }
            
            
            
            
            
            
            
            
            
            if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Type"] isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
                
                quantityString =[NSString stringWithFormat:@"%@", [[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
                
                if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"] intValue]>1) {
                    
                    remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [typit valueForKey:@"Drug_types"], NSLocalizedString(@"left",nil)];
                    
                    //20-11-2017
                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                    {
                        remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"], NSLocalizedString(@"left",nil)];
                        
                    }

                }
                
                else
                {
                                    
                    remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [typit valueForKey:@"Drug_less"], NSLocalizedString(@"left",nil)];
                    
                    //20-11-2017
                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                    {
                        remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"], NSLocalizedString(@"left",nil)];
                    }

                }
            }
            else
                
            {
                
                if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"] isEqualToString:@" "]) {
                    
                    remainingString =@" ";
                }
                else
                    
                {
                    if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"] intValue]>1) {
                        
                        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                            
                            if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"]) {
                                
                                ////////////////////////////////////////////////////////NSLog(@"test 1");
                                remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"], NSLocalizedString(@"left",nil)];
                                
                                remainingString = [remainingString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                            
                            else
                                
                            {
                                ////////////////////////////////////////////////////////NSLog(@"test 2");
                                remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [typit valueForKey:@"Drug_types"], NSLocalizedString(@"left",nil)];
                                
                                //20-11-2017
                                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                                {
                                    remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"], NSLocalizedString(@"left",nil)];
                                }
                            }
                        }
                        
                        else
                            
                        {
                            ////////////////////////////////////////////////////////NSLog(@"test 3");
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"],[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"], NSLocalizedString(@"left",nil)];
                            
                            //20-11-2017
                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                            {
                                remainingString = [remainingString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                            
                        }
                        
                    }
                    else {
                        
                        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                            
                            if ([[typit valueForKey:@"Drug_less"] isEqualToString:@"Other"]) {
                                
                                
                                ////////////////////////////////////////////////////////NSLog(@"test 4");
                                remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [typit valueForKey:@"Drug_less"], NSLocalizedString(@"left",nil)];
                                
                                //20-11-2017
                                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                                {
                                    remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"], NSLocalizedString(@"left",nil)];
                                    
                                }

                            }
                            
                            else
                            {
                                
                                ////////////////////////////////////////////////////////NSLog(@"test 5");
                                remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [typit valueForKey:@"Drug_less"], NSLocalizedString(@"left",nil)];
                                
                                //20-11-2017
                                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                                {
                                    remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"], [[HistSectionList objectAtIndex:i] valueForKey:@"Type"], NSLocalizedString(@"left",nil)];

                                }
                            }
                        }
                        
                        else
                            
                        {
                            
                            ////////////////////////////////////////////////////////NSLog(@"test 6");
                            remainingString =[NSString stringWithFormat:@"(%@ %@ %@)", [[HistSectionList objectAtIndex:i] valueForKey:@"Remaining"],[[HistSectionList objectAtIndex:i] valueForKey:@"Dose_unit"], NSLocalizedString(@"left",nil)];
                            
                            //20-11-2017
                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                            {
                                remainingString = [remainingString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                            }
                        }
                        
                    }
                    
                }
                
                
            }
            
            
            
            if (i ==0) {
                
                
                NSString *recipeTitle = [NSString  stringWithFormat:@"<h5><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@ ", NSLocalizedString(@"Profile name:",nil)];
                
                recipeTitle = [recipeTitle stringByAppendingString:[[HistSectionList valueForKey:@"Profile"] objectAtIndex:i]];
                
                
                
                
                
                recipeTitle = [recipeTitle stringByAppendingString:@"</h5>"];
                
                
                
                
                NSString *tableFirstLine = [NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@ </th>  </tr>", NSLocalizedString(@"Medicine Name",nil), NSLocalizedString(@"Type",nil), NSLocalizedString(@"Strength",nil), NSLocalizedString(@"Dose Quantity",nil), NSLocalizedString(@"Starting Date",nil), NSLocalizedString(@"Ending Date",nil), NSLocalizedString(@"Instructions",nil)];
                
                
                
                
                increments = [increments stringByAppendingString:tableFirstLine];
                
                increments = [increments stringByAppendingString:recipeTitle];
                
                
            }
            
            
            NSDateFormatter *set = [[NSDateFormatter alloc] init];
            [set setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            
            [set setDateFormat:@"dd/MM/yyyy"];
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            
            [format setDateFormat:@"dd/MM/yyyy"];
            
            increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Name"]];
            increments = [increments stringByAppendingString:@"</th>"];
            
            
            increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
            increments = [increments stringByAppendingString:@"</th>"];
            
            
            
            increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:strengthString];
            increments = [increments stringByAppendingString:@"</th>"];
            
            
            
            increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:quantityString];
            increments = [increments stringByAppendingString:@"</th>"];
            
            
            NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
            [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
            
            increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:[dateConvert stringFromDate:[[HistSectionList objectAtIndex:i] valueForKey:@"Starting"]]];
            increments = [increments stringByAppendingString:@"</th>"];
            
            
            increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Duration"]];
            increments = [increments stringByAppendingString:@"</th>"];
            
            
            
            if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Instructions"] isEqualToString:@" "]) {
                
                
                increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >"];
                increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Instructions"]];
                increments = [increments stringByAppendingString:@"</th>"];
                
            }
            
            else
            {
                
                increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >"];
                increments = [increments stringByAppendingString:[NSString stringWithFormat:@"%@\n%@", [[HistSectionList objectAtIndex:i] valueForKey:@"Location"], [[HistSectionList objectAtIndex:i] valueForKey:@"Instructions"]]];
                increments = [increments stringByAppendingString:@"</th>"];
                
                
            }
            
            
            
            
            increments = [increments stringByAppendingString:@"<tr>"];
            
            
            
        }
        
        
        
        
        
    }
    
    increments = [increments stringByAppendingString:@"</table><tr><td width=\"120\" align=\"center\"><font face=\"Arial\"  color=\"000000\">"];
    
    
    
    return increments;
}

-(void)SetProfile:(NSString*)ID;

{
    [[NSUserDefaults standardUserDefaults] setObject:ID forKey: @"ID_Profile"];
}


-(NSString*)getProfileID

{
    
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"ID_Profile"];
}


#pragma mark - reloadTableview




-(void)reloadTableview:(NSString*)set

{
    
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Medicine"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        Medicine *newCourse = [fetchedObjects objectAtIndex:i];
        
        
        if([newCourse.duration isEqualToString:@"Nooit"]||[newCourse.duration isEqualToString:NSLocalizedString(@"Never",nil)]||[newCourse.duration isEqualToString:@"Nie"]||[newCourse.duration isEqualToString:@"Jamais"]||[newCourse.duration isEqualToString:@"Mai"]||[newCourse.duration isEqualToString:@"Ποτέ"]||[newCourse.duration isEqualToString:@"אף פעם"] || [newCourse.duration isEqualToString:@"Nunca"])
        {
            
        }
        else
        {
            UILocalNotification *notif = (UILocalNotification*)[NSKeyedUnarchiver unarchiveObjectWithData:newCourse.notifications];
            
            
            
            NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
            [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [setdata setDateFormat:@"EEEE dd, MMMM yyyy 23:59:59 +0000"];
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            
            NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[setdata dateFromString:newCourse.duration]];
            
            
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:23];
            // Notification will fire in one minute
            [dateComps setMinute:59];
            [dateComps setSecond:59];
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            
            
            if  ([notif.fireDate compare:itemDate]==NSOrderedSame)
                
            {
                
                [Notification ActivateNotification:[notif.userInfo valueForKey:@"ID_Medicine"]  active:@"YES"];
                
                
            }
            else if ([notif.fireDate compare:itemDate]==NSOrderedAscending)
                
                
            {
                [Notification ActivateNotification:[notif.userInfo valueForKey:@"ID_Medicine"]  active:@"YES"];
                
                
                
            }
            else if ([notif.fireDate compare:itemDate]==NSOrderedDescending)
                
            {
                
                [[UIApplication sharedApplication] cancelLocalNotification:notif];
                
                
                NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
                [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateFormatLevel setDateFormat:[appDelegate convertString:@"hh:mm a"]];
                
                
                NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
                
                
                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                
                [format setDateFormat:@"dd/MM/yyyy"];
                
                
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateFormat setDateFormat:@"dd/MM/yyyy hh:mm:a"];
                
                
                
                [Notification ActivateNotification:[notif.userInfo valueForKey:@"ID_Medicine"]  active:@"NO"];
                
                
                
                
                
                
            }
            
            
        }
        
        // //////////////////////////NSLog(@"callFunction %@", [fetchedObjects objectAtIndex:i]);
        
        if (![context save:&error]) {
            
        }
        
        
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", newCourse.id_medicine];
        
        NSArray *notificationArray = [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
        
        if ([notificationArray count]>0)
        {
            
            UILocalNotification *notif = [notificationArray objectAtIndex:0];
         
            //27-07-2017 add New Code for Resolve DateTime Issue
            NSDate *prevDate;
            for (NSInteger i =0; i < ([notificationArray count]); i++) {
                
                UILocalNotification *notif1 = [notificationArray objectAtIndex:i];
                if (i<1) {
                    prevDate = notif1.fireDate;
                }
                if( [prevDate timeIntervalSinceDate:notif1.fireDate] > 0 )
                {
                    prevDate = notif1.fireDate;
                    notif = notif1;
                }
            }
            //27-07-2017 add New Code for Resolve DateTime Issue

            
            if ([[notif.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
                
                newCourse.firedate =notif.fireDate;
                if (![context save:&error]) {
                    
                }
                else
                    
                {
                    
                    
                }
                
                
            }
            else
            {
                // //////////////////////////////////////////////////NSLog(@"%@", notif.fireDate);
                
                
                //***************** 12-07-2017 ********************
                /*
                NSMutableArray *arrayTemp = [NSMutableArray array];
                NSMutableArray *arrayTemp1 = [NSMutableArray array];
                
                NSDate *currentDate = [NSDate date];
                
                NSLog(@"Notificaation : %@",notif.fireDate);
                
                for (int i=0; i<[notificationArray count]; i++)
                {
                    UILocalNotification *notif = [notificationArray objectAtIndex:i];
                    
                    NSTimeInterval secondsBetween = [notif.fireDate timeIntervalSinceDate:currentDate];
                    if (secondsBetween > 0)
                    {
                        [arrayTemp addObject:@(secondsBetween)];
                    }
                }
                
                
                arrayTemp1 = [arrayTemp mutableCopy];
                
                NSArray *sortedArray = [arrayTemp1 sortedArrayUsingSelector:@selector(compare:)];
                
                NSLog(@"sortedArray : %@",sortedArray);
                
                for (int i=0; i<arrayTemp.count; i++)
                {
                    if ([arrayTemp objectAtIndex:i] == [sortedArray objectAtIndex:0])
                    {
                        
                        UILocalNotification *notif = [notificationArray objectAtIndex:i];
                        newCourse.firedate = notif.fireDate;
                        
                        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notif];
                        newCourse.notifications = data;
                        break;
                    }
                }
                        */
                
                //***************** 12-07-2017 ********************
                
                
                //12-07-2017 comment below code and write new above code for display proper time in Medicine List 
                
                 newCourse.firedate = notif.fireDate;
                 
                 NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notif];
                 newCourse.notifications = data;
                
                if (![context save:&error]) {
                    
                }
                else
                    
                {
                    
                    
                }
                
            }
            
        }
        
        
    }
        
    for (NSInteger i=0 ; i<[self.navigationController.viewControllers count]; i++) {
        
        if ([[self.navigationController.viewControllers objectAtIndex:i] isKindOfClass:[MedicineViewController class]]) {
            
            
            MedicineViewController *oldscreen =[self.navigationController.viewControllers objectAtIndex:i];
            
            //////////////////////////NSLog(@"%i", i);
            [oldscreen performSelector:@selector(callFunction:) withObject:[NSDate date] afterDelay:0.4];
            
            
        }
        else
        {
        }
        
        
    }
    
    [self seeIfNotification];
    
    
    
}


#pragma mark - reloadTableviewend

-(NSString*)makelist:(NSMutableArray*)HistSectionList

{
    
    NSString *tableFirstLine = [NSString stringWithFormat:@"<TABLE border=\"1\" valign=\"top\" width=\"1200\" bordercolor=\"#e6e6e6\" cellpadding=\"5\" cellspacing=\"1\"><tr><td width=\"100\" align=\"center\" bgcolor=\"#000000\"><font face=\"Arial\" color=\"ffffff\"><p style=\"margin-left:125px\">%@</p></font></td><td width=\"100\" align=\"center\" bgcolor=\"#000000\"><font face=\"Arial\" color=\"ffffff\"><p style=\"margin-left:125px\">%@</p></font></td><td width=\"100\" align=\"center\" bgcolor=\"#000000\"><font face=\"Arial\" color=\"ffffff\"><p style=\"margin-left:125px\">%@</p></font></td><td width=\"100\" align=\"center\" bgcolor=\"#000000\"><font face=\"Arial\" color=\"ffffff\"><p style=\"margin-left:125px\">%@</p></font></td><td width=\"100\" align=\"center\" bgcolor=\"#000000\"><font face=\"Arial\" color=\"ffffff\"><p style=\"margin-left:125px\">%@</p></font><td width=\"100\" align=\"center\" bgcolor=\"#000000\"><font face=\"Arial\" color=\"ffffff\"><p style=\"margin-left:125px\">%@</p></font><td width=\"100\" align=\"center\" bgcolor=\"#000000\"><font face=\"Arial\" color=\"ffffff\"><p style=\"margin-left:125px\">%@</p></font><td width=\"200\" align=\"center\" bgcolor=\"#000000\"><font face=\"Arial\" color=\"ffffff\"><p style=\"margin-left:125px\">%@</p></font><td width=\"100\" align=\"center\" bgcolor=\"#000000\"><font face=\"Arial\" color=\"ffffff\"><p style=\"margin-left:125px\">%@</p></font></td></tr></td>", NSLocalizedString(@"ScheduledDate",nil), NSLocalizedString(@"Scheduled Time",nil), NSLocalizedString(@"Medicine Name",nil), NSLocalizedString(@"Strength",nil), NSLocalizedString(@"Dose",nil), NSLocalizedString(@"Instructions",nil), NSLocalizedString(@"Action Date",nil), NSLocalizedString(@"Action Time",nil), NSLocalizedString(@"Action",nil)];
    
    
    
    
    
    
    
    NSString *increments = @"";
    
    
    increments = [increments stringByAppendingString:tableFirstLine];
    
    
    for (NSInteger i=0; i < [HistSectionList count]; i++) {
        
        
        NSDateFormatter *set = [[NSDateFormatter alloc] init];
        [set setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [set setDateFormat:@"dd/MM/yyyy"];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [format setDateFormat:@"dd/MM/yyyy"];
        
        
        
        increments = [increments stringByAppendingString:@"<tr><td width=\"200\" align=\"center\"><font face=\"Arial\"  font size=\"1pt\" color=\"000000\">"];
        increments = [increments stringByAppendingString:[format stringFromDate:[set dateFromString:[[HistSectionList objectAtIndex:i] valueForKey:@"Date"]]]];
        
        
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.50\" color=\"#000000\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Original_Time"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Name"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Dose"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\%\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"]];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Action"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Date"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Time_Action"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        
        
        
        
        
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]isEqualToString:NSLocalizedString(@"Taken",nil)]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#125c0f\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]isEqualToString:NSLocalizedString(@"Taken earlier",nil)]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#126362\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]isEqualToString:NSLocalizedString(@"Taken late",nil)]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#f2351a\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]isEqualToString:NSLocalizedString(@"Missed",nil)]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#f2351a\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]isEqualToString:NSLocalizedString(@"Skipped",nil)]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#d45a19\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"] isEqualToString:NSLocalizedString(@"Postponed",nil)]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#0f006d\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        
        
        
        
        
        increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        
    }
    
    return increments;
}



-(void)valueChanged:(UIDatePicker*)sender
{
    
    
    
    
    NSDate *now = [[NSDate alloc] init];
    
    
    for (NSInteger i=-1; i < 1; i++) {
        
        
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
        [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [setdata setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComps = [[NSDateComponents alloc] init];
        
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:now];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:now];
        
        
        
        [dateComps setDay:[dateComponents day]];
        [dateComps setMonth:[dateComponents month]];
        [dateComps setYear:[dateComponents year]];
        [dateComps setHour:[timeComponents hour]];
        // Notification will fire in one minute
        [dateComps setMinute:[timeComponents minute]+i];
        [dateComps setSecond:0];
        
        
        NSDate *itemDate = [calendar dateFromComponents:dateComps];
        
        
        
        
        for (UIBarButtonItem *item in self.toolbarDown2.items)
        {
            if ([item.title isEqualToString:NSLocalizedString(@"Done",nil)]) {
                item.enabled = YES;
            }
        }
        
        
        if  ([itemDate compare:sender.date]==NSOrderedSame)
            
        {
            
            double delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                for (UIBarButtonItem *item in self.toolbarDown2.items)
                {
                    if ([item.title isEqualToString:NSLocalizedString(@"Done",nil)]) {
                        item.enabled = NO;
                    }
                    else
                    {
                        item.enabled = YES;
                    }
                    
                }
                Constrant = sender.date;
            });
            
            break;
        }
        else if  ([itemDate compare:sender.date]==NSOrderedDescending)
            
            
        {
            
            double delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                for (UIBarButtonItem *item in self.toolbarDown2.items)
                {
                    if ([item.title isEqualToString:NSLocalizedString(@"Done",nil)]) {
                        item.enabled = YES;
                    }
                    else
                    {
                        item.enabled = YES;
                    }
                }
                Constrant = sender.date;
            });
            
        }
        else if ([itemDate compare:sender.date]==NSOrderedAscending)
            
            
        {
            double delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                for (UIBarButtonItem *item in self.toolbarDown2.items)
                {
                    if ([item.title isEqualToString:NSLocalizedString(@"Done",nil)]) {
                        item.enabled = YES;
                    }
                    else
                    {
                        item.enabled = YES;
                    }
                }
                Constrant = sender.date;
            });
        }
        
    }
    
    
    
}

-(void)Play

{
    
    
    
    
}

-(void)NextMed:(UIButton*)sender

{
    ////////////////////////NSLog(@"NextMed");
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    if ([fetchedObjects count] ==1) {
        
        ////////////////////////NSLog(@"2 %@", self.currentProfile);
        
        MedicineViewController *controller2 = [[MedicineViewController alloc]init];
        
        
        
        [Navigaioncopy removeAllObjects];
        [Navigaioncopy addObject:controller2];
        
        [self.navigationController setViewControllers:Navigaioncopy animated:NO];
        
        
        [controller2 CreateAccount:self.currentProfile];
        
        [toolbarDown setAlpha:1];
        
        
    }
    
    else
    {
        
        ////////////////////////NSLog(@"3 %@", self.currentProfile);
        
        MedicineViewController *controller2 = [[MedicineViewController alloc]init];
        
        MedsViewController *controller3 = [[MedsViewController alloc]init];
        
        [Navigaioncopy removeAllObjects];
        [Navigaioncopy addObject:controller3];
        [Navigaioncopy addObject:controller2];
        
        
        
        [self.navigationController setViewControllers:Navigaioncopy animated:NO];
        
        
        
        
        [controller2 CreateAccount:self.currentProfile];
        
        [toolbarDown setAlpha:1];
        
    }
}



#pragma mark - Next
//sn
-(void)Next:(UIButton*)sender
{
    
//    [self googleAnalyticsGroupping];
    
    ////////////////////////NSLog(@"Next");
    
    //////////////////////////NSLog(@"%i", sender.tag);
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count] ==1) {
        
        NSMutableArray *listTotal=[[NSMutableArray alloc] initWithObjects:@"Icons_smile",@"Icons_send",@"Icons_Alert",@"Icons_email",@"Icons_info",@"Icons_settings",nil];
        
        
        for(NSInteger i=0; i<=[listTotal count]-1; i++)
        {
            
            
            UIButton *setimage = (UIButton *)[window viewWithTag:i+31];
            
            [setimage setEnabled:YES];
            
            
        }
        
        
        UIButton *setimage = (UIButton *)[window viewWithTag:3+31];
        UIButton *setimage2 = (UIButton *)[window viewWithTag:4+31];
        
        if (setimage == sender||setimage2 == sender) {
            
        }
        else
        {
            
            
        }
        
        if (sender.tag ==31) {
            StartViewController *controller3 = [[StartViewController alloc]init];
            [self.navigationController pushViewController:controller3 animated:NO];
            [self setNavigationbarIconImage:self.navigationController setimge:NO];
        }
        if (sender.tag ==32) {
            
            
           
            
            HistoryViewController *controller2 = [[HistoryViewController alloc]init];
            [self.navigationController pushViewController:controller2 animated:NO];
            
            [self setNavigationbarIconImage:self.navigationController setimge:NO];
            
            int64_t delayInSeconds = 0.1;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                
                controller2.SetOrder = @"Date";
                
                
                [Navigaioncopy removeAllObjects];
                [Navigaioncopy addObject:controller2];
                
                [self.navigationController setViewControllers:Navigaioncopy animated:NO];
                
                NSError *error = nil;
                NSManagedObjectContext *context = self.managedObjectContext;
                
                
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                                          inManagedObjectContext:context];
                [fetchRequest setEntity:entity];
                
                NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
                
                
                for (NSInteger i =0; i < [fetchedObjects count]; i++) {
                    
                    
                    Profiles *newCourse = [fetchedObjects objectAtIndex:i];
                    CurrentID=newCourse.id;
                    
                    [controller2 setItems:newCourse];
                    
                }
                
                
                
            });
            
            
            
            
            
        }
        if (sender.tag ==33) {
            
            
            [Navigaioncopy removeAllObjects];
            
            
            
            
            for (NSInteger i =0; i < [fetchedObjects count]; i++) {
                
                
                Profiles *newCourse = [fetchedObjects objectAtIndex:i];
                CurrentID=newCourse.id;
                
                currentProfile =newCourse;
                
                
                
                
                
                
                
            }
            
            
            
            MedicineViewController *controller2 = [[MedicineViewController alloc]init];
            [Navigaioncopy addObject:controller2];
            
            [self.navigationController setViewControllers:Navigaioncopy animated:NO];
            [self setNavigationbarIconImage:self.navigationController setimge:YES];
            
            
            
            
            
            
            
            
        }
        
        if (sender.tag ==34) {
            
            int64_t delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [self SendtheMail];
            });
            
        }
        if (sender.tag ==35) {
            
            
            int64_t delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [self popview];
            });
            
        }
        
        if (sender.tag ==36) {
            
            SettingsViewcontroller *controller4 = [[SettingsViewcontroller alloc]init];
            [self.navigationController pushViewController:controller4 animated:NO];
            [self setNavigationbarIconImage:self.navigationController setimge:NO];
        }
    }
    
    else if ([fetchedObjects count] >1) {
        
        NSMutableArray *listTotal=[[NSMutableArray alloc] initWithObjects:@"Icons_smile",@"Icons_send",@"Icons_Alert",@"Icons_email",@"Icons_info",@"Icons_settings",nil];
        
        
        for(NSInteger i=0; i<=[listTotal count]-1; i++)
        {
            
            
            UIButton *setimage = (UIButton *)[window viewWithTag:i+31];
            
            [setimage setEnabled:YES];
            
            
        }
        
        
        UIButton *setimage = (UIButton *)[window viewWithTag:3+31];
        UIButton *setimage2 = (UIButton *)[window viewWithTag:4+31];
        
        if (setimage == sender||setimage2 == sender) {
            
        }
        else
        {
            
        }
        
        if (sender.tag ==31) {
            
            StartViewController *controller3 = [[StartViewController alloc]init];
            [self.navigationController pushViewController:controller3 animated:NO];
            [self setNavigationbarIconImage:self.navigationController setimge:NO];
        }
        if (sender.tag ==32) {
            
            
            HistViewController *controller2 = [[HistViewController alloc]init];
            [self.navigationController pushViewController:controller2 animated:NO];
            [self setNavigationbarIconImage:self.navigationController setimge:NO];
            
        }
        if (sender.tag ==33) {
            
            
            
            MedicineViewController *controller2 = [[MedicineViewController alloc]init];
            
            MedsViewController *controller3 = [[MedsViewController alloc]init];
            
            [Navigaioncopy removeAllObjects];
            [Navigaioncopy addObject:controller3];
            [Navigaioncopy addObject:controller2];
            
            
            [self.navigationController pushViewController:controller3 animated:NO];
            [self setNavigationbarIconImage:self.navigationController setimge:YES];
            [self setNavigationbarIconImage:self.navigationController setimge:YES];
        }
        
        if (sender.tag ==34) {
            
            int64_t delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [self SendtheMail];
            });
            
        }
        if (sender.tag ==35) {
            
            
            int64_t delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [self popview];
            });
            
        }
        
        if (sender.tag ==36) {
            
            
            SettingsViewcontroller *controller4 = [[SettingsViewcontroller alloc]init];
            [self.navigationController pushViewController:controller4 animated:NO];
            [self setNavigationbarIconImage:self.navigationController setimge:NO];
        }
        
    }
    
}


-(void)popview
{
    
    [toolbarDown setAlpha:0];
    
//    [self googleAnalyticsGroupping];
    
    InfoViewController *controller = [[InfoViewController alloc]init];
    
    BufferedNavigationController *navController = [[BufferedNavigationController alloc] initWithRootViewController:controller];
    [navController.navigationBar setTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    [navController.navigationBar setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forBarMetrics:0];
    [navController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    controller.navController = navController;
    
    
    
    [self.navigationController presentViewController:controller.navController animated:YES completion:Nil];
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    
    [self.window becomeFirstResponder];
    [Copynavigaton  dismissViewControllerAnimated:YES completion:NULL];
    
    if (Copynavigaton==self.navigationController) {
        
        [toolbarDown setAlpha:1];
    }
    else
    {
        
    }
    
}

- (void)SendtheMailtoAdress:(NSString*)mail subject:(NSString*)subjectit with:(UINavigationController*)parant

{
    
    Copynavigaton=parant;
    
    [toolbarDown setAlpha:0];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        
        
        NSArray *toRecipients2 = [[NSArray alloc] initWithObjects:mail,nil];
        
        
        MFMailComposeViewController *controller2 = [[MFMailComposeViewController alloc] init];
        
        
        
        
        controller2.mailComposeDelegate = self;
        
        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
       if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            
            
            
        }
        
        else
        {
            controller2.navigationBar.tintColor =[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
        }
        

        [controller2 setToRecipients: toRecipients2];
        [controller2 setSubject:subjectit];
        
        if ([subjectit isEqualToString:@"CFDaily"])
        {
//            [controller2 setMessageBody:[NSString stringWithFormat:@"%@\nhttps://itunes.apple.com/us/app/cf-medcare-cystic-fibrosis/id658766860?l=nl&ls=1&mt=8",NSLocalizedString(@"Check out this App!",nil)] isHTML:YES]; //20-11
            [controller2 setMessageBody:[NSString stringWithFormat:@"%@\nhttps://itunes.apple.com/us/app/cfdaily-eu/id1314615718?ls=1&mt=8",NSLocalizedString(@"Check out this App!",nil)] isHTML:YES];

        }
        else
        {
            [controller2 setMessageBody:@"" isHTML:NO];
        }
        
        [parant presentViewController:controller2 animated:YES completion:NULL];
        
    }
    else
    {
        [toolbarDown setAlpha:1];
        
        //27-06-2017 change
      /*  UIAlertView *alertit = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"there is no mail active!",nil)
                                                         delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
        [alertit show];
       */
        
        UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"there is no mail active!",nil) preferredStyle:UIAlertControllerStyleAlert];
        [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
            
        });
        
    }
    
    
    
    
}

- (void)SendtheMailtoAdress:(NSString*)mail

{
    
    Copynavigaton=self.navigationController;
    
    [toolbarDown setAlpha:0];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        
        
        NSArray *toRecipients2 = [[NSArray alloc] initWithObjects:mail,nil];
        
        
        MFMailComposeViewController *controller2 = [[MFMailComposeViewController alloc] init];
        
        
        
        
        controller2.mailComposeDelegate = self;
        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            
            
        }
        
        else
        {
            controller2.navigationBar.tintColor =[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
        }
        
        
        
        [controller2 setToRecipients: toRecipients2];
        [controller2 setSubject:@""];
        
        
        
        [controller2 setMessageBody:@"" isHTML:NO];
        [self.navigationController presentViewController:controller2 animated:YES completion:NULL];
        
    }
    else
    {
        [toolbarDown setAlpha:1];
        
        /* // 27-06-2017 change
        UIAlertView *alertit = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"there is no mail active!",nil)
                                                         delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
        [alertit show];
        */
        
        
        UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"there is no mail active!",nil) preferredStyle:UIAlertControllerStyleAlert];
        [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
            
        });

        
        
    }
    
    
    
}



-(void)SendtheMail

{
    
    
    
    Copynavigaton=self.navigationController;
    [toolbarDown setAlpha:0];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        
        
        NSArray *toRecipients2 = [[NSArray alloc] initWithObjects:@"",nil];
        
        
        MFMailComposeViewController *controller2 = [[MFMailComposeViewController alloc] init];
        
        
        
        
        controller2.mailComposeDelegate = self;
        
        
        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        
      if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            
            
            
        }
        
        else
        {
            controller2.navigationBar.tintColor =[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
        }
        
        
        
        [controller2 setToRecipients: toRecipients2];
//        [controller2 setSubject:NSLocalizedString(@"List of Medicines",nil)]; //18-07-2017
        [controller2 setSubject:NSLocalizedString(@"CFDaily List of Medicines",nil)];
        
        
        NSArray *profileItem =[NSArray arrayWithObjects:@"Name",@"ID",@"Image",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Silence",@"Vibrate",nil];
        
        
        NSString *increments = @"";
        
        for (NSInteger i=0; i < [[SaveCore getProfiles:profileItem] count]; i++) {
            
            increments = [increments stringByAppendingString:[self getListMial:[SaveCore getMedicinewithProfileID:[[[SaveCore getProfiles:profileItem] objectAtIndex:i] valueForKey:@"ID"]]]];
            
        }
//        //20-11-2017
//         if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"]){
//             increments = [increments stringByReplacingOccurrencesOfString:@"stk" withString:@""];
//         }
        
        [controller2 setMessageBody:increments isHTML:YES];
        [self.navigationController presentViewController:controller2 animated:YES completion:NULL];
        
    }
    else
    {
        [toolbarDown setAlpha:1];
        
        /* //27-06-2017 change
        UIAlertView *alertit = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"there is no mail active!",nil)
                                                         delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
        [alertit show];
        */
        
        
        UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"there is no mail active!",nil) preferredStyle:UIAlertControllerStyleAlert];
        [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
            
        });

        
    }
    
    
}




-(void)MultiPopDown

{
    
}


-(void)setitemsDictionary:(NSString*)set name:(NSString*)name

{
    
}


- (void)application:(UIApplication *)application  didReceiveLocalNotification:(UILocalNotification *)notification {
    
    
    
    //NSLog(@"%@", notification);
    
    
    
    if (application.applicationIconBadgeNumber-notification.applicationIconBadgeNumber<0) {
        
    }
    else
        
        [UIApplication sharedApplication].applicationIconBadgeNumber=application.applicationIconBadgeNumber-notification.applicationIconBadgeNumber;
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",[notification.userInfo valueForKey:@"ID_Medicine"]]]) {
        
        
        NSData *dataExtra= [[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",[notification.userInfo valueForKey:@"ID_Medicine"]]];
        
        UILocalNotification *localNotif = [NSKeyedUnarchiver unarchiveObjectWithData:dataExtra];
        ////////////////////////////////////////////////////NSLog(@"Remove localnotification  are %@", localNotif);
        
        
        [[UIApplication sharedApplication] cancelLocalNotification:localNotif];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString  stringWithFormat:@"Extra_%@",[notification.userInfo valueForKey:@"ID_Medicine"]]];
        
    }
    
    
    if (notification) {
        
        NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDir = [paths2 objectAtIndex:0];
        
        
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Caches/%@", documentsDir, notification.soundName]];
        
        
        [RecordingAct playRecording:url];
        
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm"];
        
        
        
        
        
        
        [self canPerformAction:@selector(TakenPostoneUp:)  withSender:0];
        
        [self performSelector:@selector(TakenPostoneUp:) withObject:0];
        
        [self canPerformAction:@selector(TakenSkipUp:)  withSender:0];
        
        [self performSelector:@selector(TakenSkipUp:) withObject:0];
        
        
        [self canPerformAction:@selector(TakenOrtookUp:)  withSender:0];
        
        [self performSelector:@selector(TakenOrtookUp:) withObject:0];
        
        [self canPerformAction:@selector(PostNotfication:)  withSender:0];
        
        
        [self performSelector:@selector(PostNotfication:) withObject:0];
        
        
        [CurrentReminder setDictionary:notification.userInfo];
        
        
        
        [curentChoise removeAllObjects];
        [curentChoise addObject:notification];
        
        
        
        [alert fresh];
        
        
        
        
        [alert changeData:[SaveCore getMedicinewithID:[notification.userInfo valueForKey:@"ID_Medicine"]] date:notification.fireDate];
        
        
        [self PostNotfication:YES];
        
    }
    
    else
    {
    }
    
    
    
    
    
    Countnote++;
    
    
    
    
}




-(BOOL)SeeifEndingDateIsPast:(UILocalNotification *)notification {
    
    
    BOOL set;
    
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd 00:00:01 PM +0000"];
    
    
    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    NSDate *let =[dateConvert dateFromString:[notification.userInfo valueForKey:@"Duration"]];
    
    
    
    if ([NSLocalizedString([notification.userInfo valueForKey:@"Duration"],nil) isEqualToString:NSLocalizedString(@"Never",nil)])
    {
        set = YES;
        
    }
    else
    {
        if  ([let compare:notification.fireDate]==NSOrderedSame)
            
        {
            
            set = NO;
            
            
        }
        else if  ([let compare:notification.fireDate]==NSOrderedDescending)
            
            
        {
            
            
            
            set = YES;
            
        }
        else {
            
            
            set = NO;
        }
    }
    
    return set;
}



-(void)switchsceen

{
    
    
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (NSInteger i =([notificationArray count]); i > 0; i--) {
        
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        
        
        
        NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDir = [paths2 objectAtIndex:0];
        
        
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Caches/%@", documentsDir, notif.soundName]];
        
        
        NSDate *currDate = [NSDate date];
        
        if  ([notif.fireDate compare:currDate]==NSOrderedSame)
            
        {
            
            
            [RecordingAct performSelector:@selector(playRecording:) withObject:url afterDelay:0.5];
            
            
        }
        else if ([notif.fireDate compare:currDate]==NSOrderedAscending)
            
            
        {
            
            [RecordingAct performSelector:@selector(playRecording:) withObject:url afterDelay:0.2];
        }
        else if ([notif.fireDate compare:currDate]==NSOrderedDescending)
            
            
        {
            
            
            
            
            
        }
        
        
        
        
        
        
        
    }
    
    
    
    NSArray *profileItem =[NSArray arrayWithObjects:@"Name",@"ID",@"Image",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Silence",@"Vibrate",nil];
    
    
    
    
    
    
    if ([[SaveCore getProfiles:profileItem] count] ==1) {
        
        
        
        
        int64_t delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            
            NSError *error = nil;
            NSManagedObjectContext *context = self.managedObjectContext;
            
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                                      inManagedObjectContext:context];
            [fetchRequest setEntity:entity];
            
            NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
            
            for (NSInteger i =0; i < [fetchedObjects count]; i++) {
                
                
                Profiles *newCourse = [fetchedObjects objectAtIndex:i];
                CurrentID=newCourse.id;
                
                currentProfile =newCourse;
                
                
            }
            
            
            
            MedicineViewController *controller2 = [[MedicineViewController alloc]init];
            
            [Navigaioncopy removeAllObjects];
            [Navigaioncopy addObject:controller2];
            
            [self.navigationController setViewControllers:Navigaioncopy animated:NO];
            
            
            
            
        });
        
        
    }
    
    else
        
    {
        
        
        NSError *error = nil;
        NSManagedObjectContext *context = self.managedObjectContext;
        
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                                  inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        
        for (NSInteger i =0; i < [fetchedObjects count]; i++) {
            
            
            Profiles *newCourse = [fetchedObjects objectAtIndex:i];
            CurrentID=newCourse.id;
            
            currentProfile =newCourse;
        }
        
        
        MedsViewController *controller3 = [[MedsViewController alloc]init];
        
        int64_t delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            
            
            [Navigaioncopy removeAllObjects];
            [Navigaioncopy addObject:controller3];
            
            
            
            [self.navigationController setViewControllers:Navigaioncopy animated:NO];
            
            
            
            
        });
        
        
        
        
    }
    
    
    
}


-(void)switchsceenSet:(NSString*)ID

{
    
}


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
 
    
   	[[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    [self saveContext];
    
}


- (void)showReminder2:(UILocalNotification *)text {
    
    
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"CF_Medcare://"]];
    
}

- (void)showReminder:(UILocalNotification *)text {
    
    
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"CF_Medcare://"]];
    
    
    
    MedsViewController *controller1 = [[MedsViewController alloc]init];
    MedicineViewController *controller2 = [[MedicineViewController alloc]init];
    
    [self.Navigaioncopy removeAllObjects];
    [self.Navigaioncopy addObject:controller1];
    
    [self.navigationController setViewControllers:self.Navigaioncopy animated:NO];
    
    [self.navigationController pushViewController:controller2 animated:NO];
    
    
    NSMutableArray *listTotal=[[NSMutableArray alloc] initWithObjects:@"Icons_smile",@"Icons_send",@"Icons_Alert",@"Icons_email",@"Icons_info",@"Icons_settings",nil];
    
    
    for(NSInteger i=0; i<=[listTotal count]-1; i++)
    {
        
        
        UIButton *setimagt = (UIButton *)[window viewWithTag:i+31];
        
        [setimagt setEnabled:YES];
        
        
    }
    
    
    
    
    
    if (Active) {
        
        
        
        
        
    }
    
    else
    {
        
        
    }
    
    
    
    
}

-(void)GetID:(NSString*)termit
{
    
    [medView GetID:termit];
}

-(void)ShowAlert:(NSString*)Titel Messageit:(NSString*)Message Custimize:(NSString*)How parant:(UIViewController*)set

{
    ////////////////////////NSLog(@"#%@#", Message);
    ////////////////////////NSLog(@"#%@#", Titel);
    
    AlertCustimize *alertCust = [[AlertCustimize alloc] init];
    [alertCust setTitle:NSLocalizedString(Titel,nil)];
    [alertCust setMessage:NSLocalizedString(Message,nil)];
    [alertCust setDelegate:set];
    
    if ([How isEqualToString:NSLocalizedString(@"Delete",nil)]) {
        [alertCust MakeDeleteIt];
    }
    if ([How isEqualToString:NSLocalizedString(@"Remind", nil)]) {
        [alertCust RemindMeLater];
    }
    if ([How isEqualToString:NSLocalizedString(@"OK",nil)]) {
        [alertCust JustOk];
    }
    
    
    [alertCust show];
    
    
}


-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{
    
    akertactive =YES;
}


- (NSString *)newUUID
{
    
    
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
    NSString *result = [[NSString alloc] initWithFormat:@"%@", [(NSString *)CFBridgingRelease(uuidStr) lowercaseString]];
    
    return result;
}


- (NSString *)newUUID2
{
    
    
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
    NSString *result = [[NSString alloc] initWithFormat:@"%@", [(NSString *)CFBridgingRelease(uuidStr) lowercaseString]];
    
    return result;
}


- (NSString *)newUUIDShort
{
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
    NSString *result = [[NSString alloc] initWithFormat:@"%@", [(NSString *)CFBridgingRelease(uuidStr) lowercaseString]];
    NSArray *numbers2 = [result componentsSeparatedByString:@"-"];
    
    
    return [numbers2 objectAtIndex:0];
}

- (void) didRotate:(NSNotification *)notification{
    
    
    
    UIDeviceOrientation myOrientation = [[UIDevice currentDevice] orientation];
    if (myOrientation == UIInterfaceOrientationLandscapeLeft) {
        
        [[NSUserDefaults standardUserDefaults] setObject: @"UIInterfaceOrientationLandscapeLeft" forKey: @"Orentation"];
        
    }
    if (myOrientation == UIInterfaceOrientationLandscapeRight) {
        
        [[NSUserDefaults standardUserDefaults] setObject: @"UIInterfaceOrientationLandscapeRight" forKey: @"Orentation"];
        
        //[balk setFrame:CGRectMake(320-70, 0,  70, 480+HeigtWindow)];
    }
    if (myOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        
        [[NSUserDefaults standardUserDefaults] setObject: @"UIInterfaceOrientationPortraitUpsideDown" forKey: @"Orentation"];
        //[balk setFrame:CGRectMake(0, 410+HeigtWindow, 320, 70)];
        
    }
    if (myOrientation == UIInterfaceOrientationPortrait) {
        
        [[NSUserDefaults standardUserDefaults] setObject: @"UIInterfaceOrientationPortrait" forKey: @"Orentation"];
        //[balk setFrame:CGRectMake(0, 410+HeigtWindow, 320, 70)];
        
    }
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        for (NSInteger i =0; i < [[[self navigationController] viewControllers] count]; i++) {
            
            [[[[self navigationController] viewControllers] objectAtIndex:i] turnbuttons:[[NSUserDefaults standardUserDefaults] valueForKey:@"Orentation"]];
            
        }
    });
    
    
    
    
    
    
}

-(void)Overwritedatabase {
    
    
    NSArray *Items = [NSMutableArray arrayWithObjects: @"Copy.sqlite",nil];
    //we maken eerst een Array aan, deze fucntie kan namelijk later ook handig zijn om ander items naar de documents te verhuizen
    
    for(NSInteger i=0; i<=[Items count]-1; i++)
    {
        
        
        
        
        BOOL success;
        NSError *error;
        
        //FileManager - je maakt dus toegang tot je files in je app met Filemanager.
        NSFileManager *FileManager = [NSFileManager defaultManager];
        
        
        
        //je vraag naar het juitse pad. NSLibraryDirectory of NSLibraryDirectory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        //je vraagt het eerst pad aan in je array.
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //en bout het pad op
        NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:@"Model.sqlite"];
        
        
        //Check if the file exists or not.
        success = [FileManager fileExistsAtPath:databasePath];
        
        
        //If the database is present then quit.
        if(success) return;
        
        if (success) {
            
        }
        else
        {
            
        }
        
        
        
        
        //the database does not exists, so we will copy it to the users document directory]
        NSString *dbPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[Items objectAtIndex:i]];
        
        //Copy the database file to the users document directory.
        success = [FileManager copyItemAtPath:dbPath toPath:databasePath error:&error];
        
        //If the above operation is not a success then display a message.
        //Error message can be seen in the debugger's console window.
        if(!success)
            
        {
            NSAssert1(0, @"Failed to copy the database. Error: %@.", [error localizedDescription]);
            //we kijken of het pad succesvol is of niet
            
        }
        
        
        else
            
        {
            
        }}
}

-(void)createDatabaseIfNeeded {
    
    
    NSArray *Items = [NSMutableArray arrayWithObjects:@"Profiles.sqlite",@"Copy.sqlite",@"android.mp3",@"honk.mp3",@"news.mp3",@"Standard.mp3",@"ting.mp3",@"twinkle.mp3",@"Leeg.mp3",@"Medhot.png",@"Medhot@2x.png",@"Prescription.png", @"Prescription@2x.png", nil];
    
    //we maken eerst een Array aan, deze fucntie kan namelijk later ook handig zijn om ander items naar de documents te verhuizen
    
    for(NSInteger i=0; i<=[Items count]-1; i++)
    {
        
        
        
        
        BOOL success;
        NSError *error;
        
        //FileManager - je maakt dus toegang tot je files in je app met Filemanager.
        NSFileManager *FileManager = [NSFileManager defaultManager];
        
        
        
        //je vraag naar het juitse pad. NSLibraryDirectory of NSLibraryDirectory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        
        //je vraagt het eerst pad aan in je array.
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //en bout het pad op
        NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/Caches/%@",[Items objectAtIndex:i]]];
        
        
        //Check if the file exists or not.
        success = [FileManager fileExistsAtPath:databasePath];
        
        
        //If the database is present then quit.
        if(success) return;
        
        if (success) {
            
        }
        else
        {
            
        }
        
        
        
        
        //the database does not exists, so we will copy it to the users document directory]
        NSString *dbPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[Items objectAtIndex:i]];
        
        //Copy the database file to the users document directory.
        success = [FileManager copyItemAtPath:dbPath toPath:databasePath error:&error];
        [UIApplication sharedApplication].applicationIconBadgeNumber=0;
        
        
        //If the above operation is not a success then display a message.
        //Error message can be seen in the debugger's console window.
        if(!success)
            
        {
            NSAssert1(0, @"Failed to copy the database. Error: %@.", [error localizedDescription]);
            //we kijken of het pad succesvol is of niet
            
        }
        
        
        else
            
        {
            
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
            
            NSString *documents = [paths objectAtIndex:0];
            
            NSString *fullExtension = [NSString stringWithFormat:@".mp3"];
            
            
            
            for (NSString *fileName in [fileManager contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/Caches/",documents] error:nil ] ) {
                
                
                if ([fileName rangeOfString:fullExtension].location != NSNotFound) {
                    
                    
                    if ([fileName isEqualToString:@"Leeg.mp3"]) {
                        
                        NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                        [set setObject:[fileName stringByReplacingOccurrencesOfString:@".mp3" withString:@""] forKey:@"title"];
                        [set setObject:@"444" forKey:@"ID"];
                        [set setObject:[NSString stringWithFormat:@"%@/%@", documents, fileName] forKey:@"url"];
                        
                        [SaveData insertRecoring:set];
                        
                        
                        
                        
                        recordingMusic =[set valueForKey:@"ID"];
                        recordingURL = [set valueForKey:@"url"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[set valueForKey:@"ID"] forKey: @"recordingMusic"];
                        [[NSUserDefaults standardUserDefaults] setObject:[set valueForKey:@"url"] forKey: @"recordingURL"];
                        
                    }
                    else
                    {
                        
                        NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                        [set setObject:[fileName stringByReplacingOccurrencesOfString:@".mp3" withString:@""] forKey:@"title"];
                        [set setObject:[self newUUID] forKey:@"ID"];
                        [set setObject:[NSString stringWithFormat:@"%@/%@", documents, fileName] forKey:@"url"];
                        
                        [SaveData insertMusic:set];
                        
                        
                        chosenMusic =[set valueForKey:@"Audio"];
                        chosenURL = [set valueForKey:@"url"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[set valueForKey:@"ID"] forKey: @"chosenMusic"];
                        [[NSUserDefaults standardUserDefaults] setObject:[set valueForKey:@"url"] forKey: @"chosenURL"];
                        
                    }
                    
                    
                }
            }
        }
        
    }
}


-(void)seeifiRunOut:(NSDictionary*)what
{
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", [what valueForKey:@"ID_Medicine"]];
    
    NSArray *notificationArray = [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    

    
    
    
    if ([notificationArray count]>0) {
        
        

        
    
    NSMutableDictionary *sender =[[notificationArray valueForKey:@"userInfo"] objectAtIndex:0];
        
    
 
        
        
       NSLog(@"akertactive %@", [sender valueForKey:@"Remaining"]);
        
        
        NSArray *typit = [[GetData getDrugTypeStrength:[sender valueForKey:@"Type"]] objectAtIndex:0];
        
    
        
        NSString *stringit =@"-";
        
        
        if ([[typit valueForKey:@"Drug_types"] isEqualToString:NSLocalizedString(@"Other",nil)]) {
            
            stringit =[sender valueForKey:@"Type"];
            
            
        }
        else if ([[typit valueForKey:@"Drug_types"] isEqualToString:NSLocalizedString(@"Injection",nil)]) {
            
            stringit =@"";
            
            
        }
        else if ([[typit valueForKey:@"Drug_types"] isEqualToString:NSLocalizedString(@"Liquid",nil)]) {
            
            stringit =@"";
            
            
        }
        
        else
            
            
        {
            if ([[sender valueForKey:@"Remaining"] intValue]>1) {
            
                  stringit =[typit valueForKey:@"Drug_types"];
                
            }
            
            else
            {
                
                  stringit =[typit valueForKey:@"Drug_less"];
            }
          
        }
        
       NSLog(@"stringit %@", sender);
     
        
        
        if ([[sender  valueForKey:@"ColorGreen"] isEqualToString:@"NO"]) {
            
        }
        else
        {
            
            if ([[sender valueForKey:@"Remaining"] isEqualToString:@"0"]) {
              
                /*
                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You have run out of medication for %@ (%@ %@)" ,nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"]]
                                                                delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                [alert2 show];
                */
                
                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You have run out of medication for %@ (%@ %@)" ,nil) , [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"]]preferredStyle:UIAlertControllerStyleAlert];
                [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                    
                });

                
                
            }
            else
            {
                
                
                if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                    
                    if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"])
                    {
                        NSLog(@"1");

                        if ([[sender valueForKey:@"Remaining"] intValue]<=[[sender valueForKey:@"Refilling"] intValue]) {


                                        if ([[sender valueForKey:@"Remaining"] isEqualToString:@"0"]) {

/* //27-06-2017 change
                                            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You have run out of medication for %@ (%@ %@)" ,nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"]]
                                                                                            delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                                            [alert2 show];
*/
                                            
                                            UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You have run out of medication for %@ (%@ %@)" ,nil) , [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"]] preferredStyle:UIAlertControllerStyleAlert];
                                            [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
                                            
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                
                                                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                                                
                                            });

                                            
                                        }

                            else

                            {
                                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You are running out of medication for %@ (%@ %@) only %@ %@ left",nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"], [sender valueForKey:@"Remaining"], stringit]
                                                                                delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                                [alert2 show];
                               
                                
                                /*
                                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You are running out of medication for %@ (%@ %@) only %@ %@ left",nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"], [sender valueForKey:@"Remaining"], stringit] preferredStyle:UIAlertControllerStyleAlert];
                                
                                [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    akertactive =YES;
                                }]];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                   
                                    [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                                });
                                 */
                                
                                
                            }
                            
                        }
                        
                        else
                        {
                            ////////////////////////////////////////////////////////////////////NSLog(@"2");
                            

                        }
                    }
                    else
                    {
                        ////////////////////////////////////////////////////////////////////NSLog(@"3");
                        
                        if ([[sender valueForKey:@"Remaining"] intValue]<=[[sender valueForKey:@"Refilling"] intValue]) {


                               NSLog(@"2");

                            if ([[sender valueForKey:@"Remaining"] isEqualToString:@"0"]) {

/* //27-06-2017 change
                                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You have run out of medication for %@ (%@ %@)" ,nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"]]
                                                                                delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                                [alert2 show];
                                */
                                
                                
                                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You have run out of medication for %@ (%@ %@)" ,nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"]] preferredStyle:UIAlertControllerStyleAlert];
                                
                                [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    
                                }]];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                                });


                            }

                            else

                            {
                                
                                
                                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You are running out of medication for %@ (%@ %@) only %@ %@ left",nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"], [sender valueForKey:@"Remaining"], stringit]
                                                                                delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                                [alert2 show];

                                /*
                                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You are running out of medication for %@ (%@ %@) only %@ %@ left",nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"], [sender valueForKey:@"Remaining"], stringit] preferredStyle:UIAlertControllerStyleAlert];
                                
                                [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    akertactive =YES;
                                }]];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                                });
*/
                                
                                
                            }
                            
                        }
                        
                        else
                        {
                            ////////////////////////////////////////////////////////////////////NSLog(@"2");

                        }
                        
                        
                        
                    }
                    
                }
                else
                {
                    if ([[typit valueForKey:@"Drug_types"] isEqualToString:@"Other"])
                    {

                        if ([[sender valueForKey:@"Remaining"] intValue]<=[[sender valueForKey:@"Refilling"] intValue]) {


                             NSLog(@"2");


                            if ([[sender valueForKey:@"Remaining"] isEqualToString:@"0"]) {
                            /* //27-06-2017 change
                                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You have run out of medication for %@ (%@ %@)" ,nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"]]
                                                                                delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                                [alert2 show];
*/
                                
                                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You have run out of medication for %@ (%@ %@)" ,nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"]] preferredStyle:UIAlertControllerStyleAlert];
                                
                                [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    
                                }]];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                                });
                                

                                
                                
                            }

                            else

                            {
 //27-06-2017 change
                                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You are running out of medication for %@ (%@ %@) only %@ %@ left",nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"], [sender valueForKey:@"Remaining"], stringit]
                                                                                delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                                [alert2 show];

                                /*
                                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You are running out of medication for %@ (%@ %@) only %@ %@ left",nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"], [sender valueForKey:@"Remaining"], stringit] preferredStyle:UIAlertControllerStyleAlert];
                                
                                [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    akertactive =YES;
                                }]];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                                });

                                */
                                
                                
                            }
                            
                        }
                        
                        else
                        {
                            ////////////////////////////////////////////////////////////////////NSLog(@"2");

                            
                        }
                    }
                    else
                    {
                        ////////////////////////////////////////////////////////////////////NSLog(@"3");
                        
                        if ([[sender valueForKey:@"Remaining"] intValue]<=[[sender valueForKey:@"Refilling"] intValue]) {

                                        if ([[sender valueForKey:@"Remaining"] isEqualToString:@"0"]) {

                                            /* //27-06-2017 change
                                            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You have run out of medication for %@ (%@ %@)" ,nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"]]
                                                                                            delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                                            [alert2 show];

                                            */
                                            
                                            UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You have run out of medication for %@ (%@ %@)" ,nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"]] preferredStyle:UIAlertControllerStyleAlert];
                                            
                                            [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                
                                            }]];
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                
                                                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                                            });

                                            
                                            
                                            
                                        }

                            else

                            {
                                
                                //add condition 30-10-2017
                                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                                {
                                    if ([stringit isEqualToString:@"Tablett"])
                                    {
                                        stringit = @"tabletter";
                                    }
                                    
                                }
                                
                                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You are running out of medication for %@ (%@ %@) only %@ %@ left",nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"], [sender valueForKey:@"Remaining"], stringit]
                                                                                delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                                [alert2 show];

                                /*
                                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily Reminder",nil) message:[NSString stringWithFormat:NSLocalizedString(@"You are running out of medication for %@ (%@ %@) only %@ %@ left",nil), [sender valueForKey:@"Name"], [sender valueForKey:@"Strength"], [sender valueForKey:@"Strength_unit"], [sender valueForKey:@"Remaining"], stringit] preferredStyle:UIAlertControllerStyleAlert];
                                
                                [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    akertactive =YES;
                                }]];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                                });

                                */
                                
                            }
                            
                        }
                        
                        else
                        {
                            ////////////////////////////////////////////////////////////////////NSLog(@"2");

                            
                        }
                        
                        
                        
                    }
                }
                
                
                
            }
            
            
        }
        
    }
    
   
    
}




-(void)didPresentAlertView:(UIAlertView *)alertView

{
    
    ////NSLog(@"let op");
    
    
}



-(void)createDatabaseIfNeededWith:(NSString*)set {
    
    
    
}





+(sqlite3*)getNewDBconnectionWith:(NSString*)set{
    
    //deze functie ie alsvolgt, inmiddels zit Abel op mijn schoot, met mijn iphone!
    
    sqlite3 *newDBconnection;
    
    
    // je maakt een sqlite3 aan en noemt hem newDBconnection
    
    
    // je zegt waar hij staat:
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/Caches/%@.sqlite", set]];
    
    //we weten inmiddels dat hij daar is neergezet en maken verbinding met sqlite3_open, soms is kopy past dom!!
    
    if(sqlite3_open([path UTF8String],&newDBconnection) == SQLITE_OK){
        
        
        
    }
    else{
        
    }
    
    return newDBconnection;
    
}

+(sqlite3*)getNewDBconnection{
    
    //deze functie ie alsvolgt, inmiddels zit Abel op mijn schoot, met mijn iphone!
    
    sqlite3 *newDBconnection;
    
    
    // je maakt een sqlite3 aan en noemt hem newDBconnection
    
    
    // je zegt waar hij staat:
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);     NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/Caches/Profiles.sqlite"]];

    
    //we weten inmiddels dat hij daar is neergezet en maken verbinding met sqlite3_open, soms is kopy past dom!!
    
    
    if(sqlite3_open([path UTF8String],&newDBconnection) == SQLITE_OK){
        
        NSLog(@"Sqlite open");
        
    }
    else{
        sqlite3 *newDBconnection = NULL;
        


        // je maakt een sqlite3 aan en noemt hem newDBconnection
        
        
        // je zegt waar hij staat:
        
        //05-09-2017 add new code for handle app crash (sqlite not open).
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/Caches/Profiles.sqlite"]];
        
        NSLog(@"do something %s",sqlite3_errmsg(newDBconnection));

        //we weten inmiddels dat hij daar is neergezet en maken verbinding met sqlite3_open, soms is kopy past dom!!
        if(sqlite3_open([path UTF8String],&newDBconnection) == SQLITE_OK){
            
            NSLog(@"Sqlite open");
            
        }
        else
        {
            NSLog(@"do something %s",sqlite3_errmsg(newDBconnection));
            
            NSLog(@"Sqlite Not open");
        }
    }
    
    return newDBconnection;
    
}





- (void)setProgress:(float)progress
{
    
}


-(void)save:(NSString*)url setName:(NSString*)name

{
    
    
    [progressBar setAlpha:0];
    
    
    
    BOOL success;
    
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, name];
    
    NSFileManager *FileManager = [NSFileManager defaultManager];
    
    success = [FileManager fileExistsAtPath:jpegFilePath];
    
    if(success) return;
    
    if (success) {
        
        
        
        
        
    }
    else
    {
        
        
        [progressBar setAlpha:0];
        
        
        
    }
    
    
    
    
}

-(void)finisched:(UIViewController*)parant

{
    
}

-(void)finischedit

{
    Vol =YES;
}


-(void)save2

{
    
    
    NSArray *test= [[NSArray alloc] initWithObjects:  @"http://vimeo.com/57515252/download?t=1359018068&v=139358773&s=006a22f4d48b4dbd24ff874dfef0d6a4",@"http://vimeo.com/57516019/download?t=1359018112&v=139360786&s=94ea719dc16b09a0be76a01561b050f3",@"http://vimeo.com/57517349/download?t=1359018141&v=139364349&s=2c993648a11b91cd2a7350125c5968cd",@"http://vimeo.com/57518127/download?t=1359018158&v=139366586&s=41db69513ccacda00e605129fcb0f3c8",@"http://vimeo.com/57518986/download?t=1359018174&v=139368605&s=cb3952be6367d14420d4f8332ce45722",@"http://vimeo.com/57519776/download?t=1359018193&v=139370857&s=8b2491ac1500006e74341d7042b561fb", nil];
    
    
    
    for(NSInteger i=0; i<=[test count]-1; i++)
    {
        
        
        [progressBar setAlpha:0];
        
        
        
        BOOL success;
        
        
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat: @"Menopur%li.mp4", (long)i]];
        
        NSFileManager *FileManager = [NSFileManager defaultManager];
        
        success = [FileManager fileExistsAtPath:jpegFilePath];
        
        if(success) return;
        
        if (success) {
            
            
            
            
            
        }
        else
        {
            
            
            [progressBar setAlpha:0];
            
            
        }
        
        
    }
    
    
    
}


-(void)MultiPop

{
    [self TakenPostpone:NO];
    
    
     Multicontroller = [[MultipleViewController alloc]init];
    
    
    [self.window.rootViewController presentViewController:Multicontroller animated:YES completion:Nil];
    [Multicontroller reloaddata];
    
}

-(void)getlogin


{
    
    
    //////////////NSLog(@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"Loggedin"]);
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"Loggedin"] isEqualToString:@"Argree"]) {
        
        
        
        
    }
    
    else
        
    {    //LoginViewController.h
        
        [toolbarDown setAlpha:0];
        
        
        LoginViewController *controller = [[LoginViewController alloc]init];
        
        
        [self.navigationController presentViewController:controller animated:YES completion:Nil];
        
    }
    
    
    
    
}

#pragma mark - applicationWill

- (void)applicationWillResignActive:(UIApplication *)application
{
    //NSLog(@"applicationWillResignActive");
    
    //NSLog(@"applicationWillResignActive %i", application.applicationIconBadgeNumber);
    Active =NO;
    
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [application setApplicationIconBadgeNumber:0];
    
    for (int k =0; k <[alert.NotificationList count]; k++) {
        
        
        NSArray *set = [[alert.NotificationList objectAtIndex:k] valueForKey:@"Name"];
        
        //NSLog(@"%@", set);
        
        
        
        [application setApplicationIconBadgeNumber:application.applicationIconBadgeNumber+[set count]];
        
    }
    
    
    
    //NSLog(@"applicationDidEnterBackground %i", application.applicationIconBadgeNumber);
    Active =NO;
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
    if ([self isNetworkReachable])
    {
        [self sendHitsInBackground];
    }
    
    
    [application setApplicationIconBadgeNumber:0];
    
    [application setApplicationIconBadgeNumber:0];
    
    for (int k =0; k <[alert.NotificationList count]; k++) {
        
        
        NSArray *set = [[alert.NotificationList objectAtIndex:k] valueForKey:@"Name"];
        
        //NSLog(@"%@", set);
        
        
        
        [application setApplicationIconBadgeNumber:application.applicationIconBadgeNumber+[set count]];
        
    }
    
    
    
    
    
    //NSLog(@"applicationWillEnterForeground %i", application.applicationIconBadgeNumber);
    
    FromScreen=YES;
    
}

-(void)seeIfNotification

{
    
    //NSLog(@"seeIfNotification");
    
    
    
    Active =YES;
    
    
    
    
    [alert fresh];
    
    
    
    double delayInSeconds = 0.2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        
        
        
        if ([alert.NotificationList count]==0) {
            
            
        }
        else
        {
            
            
            
            [self PostNotfication:YES];
        }
        
    });
    
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{


    [self check_term_and_condtion_version];

    //NSLog(@"applicationDidBecomeActive");
    

    //NSLog(@"applicationWillEnterForeground %i", application.applicationIconBadgeNumber);
    

    NSDate *now = [[NSDate alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(fireDate <= %@)", now];
    NSArray *notificationArray = [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    
    for (NSInteger i =[notificationArray count]; i > 0; i--) {
        
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        
        
        
        
        
        if ([NSLocalizedString([notif.userInfo valueForKey:@"Duration"],nil) isEqualToString:NSLocalizedString(@"Never",nil)]) {
            
        }
        else
        {
            
            
            NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
            [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [setdata setDateFormat:@"EEEE dd, MMMM yyyy"];
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            
            NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[setdata dateFromString:[notif.userInfo valueForKey:@"Duration"]]];
            
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:23];
            // Notification will fire in one minute
            [dateComps setMinute:59];
            [dateComps setSecond:59];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            
            
            

            if  ([itemDate compare:notif.fireDate]==NSOrderedSame)
                
            {
                
                [Notification ActivateNotification:[notif.userInfo valueForKey:@"ID_Medicine"]  active:@"YES"];
                
            }
            else if  ([itemDate compare:notif.fireDate]==NSOrderedDescending)
                
                
            {
                [Notification ActivateNotification:[notif.userInfo valueForKey:@"ID_Medicine"]  active:@"YES"];
            }
            else if  ([itemDate compare:notif.fireDate]==NSOrderedAscending)
                
                
            {
                
                
                [Notification ActivateNotification:[notif.userInfo valueForKey:@"ID_Medicine"]  active:@"NO"];
                
                
                NSError *error = nil;
                NSManagedObjectContext *context = self.managedObjectContext;
                
                
                NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"id_medicine = %@", [notif.userInfo valueForKey:@"ID_Medicine"]];
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                [fetchRequest setEntity:[NSEntityDescription entityForName:@"Medicine" inManagedObjectContext:[self managedObjectContext]]];
                [fetchRequest setPredicate:predicate2];
                
                
                NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
                
                for (NSInteger i =0; i < [fetchedObjects count]; i++) {
                    
                    //////////////////////////NSLog(@"fetchedObjects %@", [fetchedObjects objectAtIndex:i]);
                    
                }
                
                [[UIApplication sharedApplication] cancelLocalNotification:notif];
                
                
                
                int64_t delayInSeconds = 0.1;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    
                    
                    NSError *error = nil;
                    NSManagedObjectContext *context = self.managedObjectContext;
                    
                    
                    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                                              inManagedObjectContext:context];
                    [fetchRequest setEntity:entity];
                    
                    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
                    
                    
                    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
                        
                        
                        Profiles *newCourse = [fetchedObjects objectAtIndex:i];
                        CurrentID=newCourse.id;
                        
                        currentProfile =newCourse;
                        
                        ////////////////////////NSLog(@"%@", currentProfile);
                        
                        
                    }
                    
                    
                    MedicineViewController *controller2 = [[MedicineViewController alloc]init];
                    
                    
                    [Navigaioncopy removeAllObjects];
                    [Navigaioncopy addObject:controller2];
                    
                    [self.navigationController setViewControllers:Navigaioncopy animated:NO];
                    
                    
                });
                
            }
            
        }
    }
    
    
    UITableView *setimage = (UITableView *)[self.navigationController.view viewWithTag:8978];
    
    [setimage reloadData];
    
    

    [self performSelector:@selector(reloadTableview:) withObject:@"applicationDidBecomeActive" afterDelay:0.1];
    

    
    Active =YES;
    
    
    [alert fresh];
    
    
    if ([alert.NotificationList count]==0) {
        
        
        [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    }
    else
    {
        
        
        [self performSelector:@selector(switchsceen) withObject:NULL afterDelay:0.3];
        [self PostNotfication:YES];
        
    }
    
    
    [application setApplicationIconBadgeNumber:0];
    
    for (int k =0; k <[alert.NotificationList count]; k++) {
        
        
        NSArray *set = [[alert.NotificationList objectAtIndex:k] valueForKey:@"Name"];
        
        //NSLog(@"%@", set);
        
        
        
        [application setApplicationIconBadgeNumber:application.applicationIconBadgeNumber+[set count]];
        
    }
    

}



#pragma mark - take it



-(void)PostNotfication:(BOOL)UpDown

{
    
    
    
    [self TakenPostoneUp:NO];
    
    
    
    
    
    
    
    if (Editing) {
        
    }else
    {
        if (FromScreen) {
            
        }
        else
        {
            
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:0.5f];
            
            if (!UpDown) {
                [alert setFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
                Notify = NO;
                
            }
            else
            {
                if ([alert.NotificationList count]==0) {
                    
                }
                else
                {
                    [alert setFrame:CGRectMake(10, 70, 300, 300)];
                    
                    
                    
                    
                    
                    
                }
                Notify = YES;
                
            }
            
            
            [UIView commitAnimations];
            
        }
        
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //[self SeeBadges];
            
            
            [alert.table reloadData];
            
            
            
            
            if ([alert.NotificationList count]==0) {
                
                
                [alert setFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
                
            }
            else
            {
                
                
            }
        });
        
        
    }
    
    
    
}


-(void)TakenPostpone:(BOOL)UpDown

{
    
    
    ////////////////////////////////////////////////NSLog(@"TakenPostpone");
    [toolbarDown setAlpha:1];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    
    
    [UIView setAnimationDuration:0.5f];
    
    if (!UpDown) {
        
        
        
        if ([alert.NotificationList count] ==1) {
            
            NSDictionary *dictionary = [alert.NotificationList objectAtIndex:0];
            NSArray *array = [dictionary objectForKey:@"Name"];
            
            if ([array count] >1) {
                [postpone setFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
                
            }
            else
            {
                
                [postpone setFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
                
            }
            
        }
        else  if ([alert.NotificationList count] ==0) {
            
            [postpone setFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
            
            
        }
        else
        {
            
            [postpone setFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
        }
        
        
        
        
        
        
    }
    else
    {
        
        if ([alert.NotificationList count] ==1) {
            
            NSDictionary *dictionary = [alert.NotificationList objectAtIndex:0];
            NSArray *array = [dictionary objectForKey:@"Name"];
            
            if ([array count] >1) {
                
                [postpone setFrame:CGRectMake(10, 100, 300, 300)];
            }
            else
            {
                
                [self TakenPostoneUp:YES];
                [postpone setFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
            }
            
        }
        else  if ([alert.NotificationList count] ==0) {
            
            [self TakenPostoneUp:YES];
            [postpone setFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
            
            
        }
        else
        {
            
            [postpone setFrame:CGRectMake(10, 100, 300, 300)];
        }
        
        
        
        
        
        
        
        
    }
    
    
    [UIView commitAnimations];
    
    
    
    
    
    
}

-(void)Skipped:(UIButton*)sender
{
    
    [self addActionForGoogleAnalytics:@"Postpone Actions"];
    
    [toolbarDown setAlpha:1];
    //[self loadingviewOn];
    Choisestring =@"";
    Choisestring =sender.titleLabel.text;
    [self TakenPostpone:NO];
    
    
    if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"Postpone",nil)]) {
        
        
        [title setText:NSLocalizedString(@"Postpone To",nil)];
        [self TakenPostoneUp:YES];
    }
    if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"Postpone All",nil)]) {
        
        
        [title setText:NSLocalizedString(@"Postpone To",nil)];
        [self TakenPostoneUp:YES];
    }
    
    else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"Take",nil)]) {
        
        
        [self TakenOrtookUp:YES];
    }
    
    else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"Skip",nil)]) {
        
        [self TakenSkipUp:YES];
        
    }
    else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"Cancel",nil)]) {
        
        
        
        [self PostNotfication:YES];
        
    }
    
    
}


-(void)PostponeMore:(UIButton*)sender
{
    
    [self addActionForGoogleAnalytics:@"Postpone Actions"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [toolbarDown setAlpha:1];
    
    [appDelegate.toolbarDown setAlpha:0];
    [appDelegate MultiPop];
    
    [self TakenPostpone:NO];
    [Multicontroller setWhatyouWant:@"Postpone"];
    
    if ([alert.NotificationList count] ==1) {
        
        NSDictionary *dictionary = [alert.NotificationList objectAtIndex:0];
        NSArray *array = [dictionary objectForKey:@"Name"];
        
        if ([array count]>1) {
            
            Choisestring =NSLocalizedString(@"Postpone",nil);
            
            
        }
        else
        {
            Choisestring =@"";
            
        }
        
    }
    
    
    
    
}


-(void)Take:(UIButton*)sender
{
    
    [toolbarDown setAlpha:1];
    
    Choisestring = sender.titleLabel.text;
    [self PostNotfication:NO];
    [self TakenPostpone:NO];
    [self TakenOrtookUp:YES];
    
    
    
    
}

-(void)Skip:(UIButton*)sender

{
    
    [toolbarDown setAlpha:1];
    
    
    Choisestring =sender.titleLabel.text;
    [self PostNotfication:NO];
    [self TakenPostpone:NO];
    [self TakenOrtookUp:NO];
    [self TakenSkipUp:YES];
    
    
    
    
}

-(void)SkipMore:(UIButton*)sender

{
 
    [self addActionForGoogleAnalytics:@"Skip Actions"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [appDelegate.toolbarDown setAlpha:0];
    [appDelegate MultiPop];
    
    
    //WhatyouWant
    
    [self TakenSkipUp:NO];
    [self TakenPostpone:NO];
    [self PostNotfication:NO];
    [self TakenPostpone:NO];
    [self TakenOrtookUp:NO];
    [Multicontroller setWhatyouWant:@"SkipMore"];
    
    
}

-(void)TakenSkipUp:(BOOL)UpDown

{
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    [toolbarDown setAlpha:1];
    
    [UIView setAnimationDuration:0.5f];
    
    if (!UpDown) {
        
        
        
        
        [skiptake setFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
        
        
        
    }
    else
    {
        
        
        [skiptake setFrame:CGRectMake(10, 100, 300, 300)];
        
        
        
        
    }
    
    
    [UIView commitAnimations];
    
    
    
    
    
}

-(void)SkipIt:(UIButton*)sender
{
    [self addActionForGoogleAnalytics:@"Skip Actions"];
    
    [toolbarDown setAlpha:1];
    
    
    [self TakenSkipUp:NO];
    [self PostNotfication:NO];
    [self TakenPostpone:NO];
    [self TakenOrtookUp:NO];
    
    if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"Skip",nil)]) {
        
        //NSLog(@"%@", alert.NotificationList);
        
        
        if ([alert.NotificationList count]==1) {
            
            NSDictionary *dictionary = [alert.NotificationList objectAtIndex:0];
            NSArray *array = [dictionary objectForKey:@"Name"];
            UILocalNotification *notif = [array objectAtIndex:0];
            [CurrentReminder setDictionary:notif.userInfo];
            
            [Notification postSkip:CurrentReminder andNote:notif];
            
            
        }
        else if ([alert.NotificationList count]==0)
        {
            
            
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", chosenCourse.id_medicine];
            NSMutableArray *notificationArray = [[NSMutableArray alloc] initWithArray:[[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate]];
            
            
            //NSLog(@"%@", notificationArray);
            
            if ([notificationArray count]>0) {
                
                
                
                UILocalNotification *notif = [notificationArray objectAtIndex:0];
                
                //27-07-2017 add New Code for Resolve DateTime Issue
                NSDate *prevDate;
                for (NSInteger i =0; i < ([notificationArray count]); i++) {
                    
                    UILocalNotification *notif1 = [notificationArray objectAtIndex:i];
                    if (i<1) {
                        prevDate = notif1.fireDate;
                    }
                    if( [prevDate timeIntervalSinceDate:notif1.fireDate] > 0 )
                    {
                        prevDate = notif1.fireDate;
                        notif = notif1;
                    }
                }
                //27-07-2017 add New Code for Resolve DateTime Issue
                
                [CurrentReminder setDictionary:notif.userInfo];
                
                [Notification postSkip:CurrentReminder  andNote:notif];
                
                
                
            }
            
            
        }
        
        
        
        
        
        [self performSelector:@selector(reloadTableview:) withObject:@"SkipIt" afterDelay:0.1];
        
        
    }
    else   if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"Skip All",nil)]) {
        
        NSDate *now = [[NSDate alloc] init];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(fireDate <= %@)", now];
        NSMutableArray *notificationArray = [[NSMutableArray alloc] initWithArray:[[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate]];
        
        
        
        for (NSInteger i=0 ; i<[notificationArray count]; i++) {
            
            
            
            UILocalNotification *notif = [notificationArray objectAtIndex:i];
            
            
            [CurrentReminder setDictionary:notif.userInfo];
            
            [Notification postSkip:CurrentReminder  andNote:notif];
            
        }
        
        
        [self performSelector:@selector(reloadTableview:) withObject:@"SkipIt" afterDelay:0.1];
        
    }
    
    
    else
    {
        
        
    }
    
    
    
    
    double delayInSeconds = 0.2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        if ([alert.NotificationList count] ==0) {
            
            
        }
        else
        {
            [self PostNotfication:YES];
            
        }
        
    });
    
    
    UITableView *setimage = (UITableView *)[self.navigationController.view viewWithTag:8978];
    
    [setimage reloadData];
    
    
    
    
    
}



-(void)SeeBadges


{
    
    
    
    
}

-(BOOL)SeePostpone:(Medicine*)set


{
    BOOL insert =NO;
    
    
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    
    for (NSInteger i =[notificationArray count]; i > 0; i--) {
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        if ([set.id_medicine isEqualToString:[notif.userInfo valueForKey:@"ID_Medicine"]]) {
            
            if ([[notif.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
                
                insert =YES;
            }
            
        }
    }
    
    
    return insert;
    
}





-(void)TakenOrtookUp:(BOOL)UpDown
{
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    
    
    [UIView setAnimationDuration:0.5f];
    
    if (!UpDown) {
        
        [taketook setFrame:CGRectMake(10, window.frame.size.height, 300, 300)];
        
    }
    else
    {
        [taketook setFrame:CGRectMake(10, 100, 300, 300)];
        
        
    }
    
    
    [UIView commitAnimations];
    
    
    
    
}

-(void)TakenOrtook:(UIButton*)sender
{
    [self addActionForGoogleAnalytics:@"Take Actions"];
    
    pickerTaal.datePickerMode = UIDatePickerModeTime;
    
    //[self loadingviewOn];
    
    [self TakenPostoneUp:NO];
    
    Choisestring = sender.titleLabel.text;
    
    
    [self TakenOrtookUp:NO];
    
    
    if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"Take Now",nil)])
    {
        
        
        if ([alert.NotificationList count] == 1) {
            
            NSDictionary *dictionary = [alert.NotificationList objectAtIndex:0];
            NSArray *array = [dictionary objectForKey:@"Name"];
            
            for (NSInteger k =0 ; k<[array count]; k++) {
                
                
                UILocalNotification *notif = [array objectAtIndex:k];
                
                [CurrentReminder setDictionary:notif.userInfo];
                
                
                NSDate *now = [[NSDate alloc] init];
                
                
                [Notification postTake:CurrentReminder date:now andNote:notif];
                
            }
            
            [self performSelector:@selector(reloadTableview:) withObject:@"TakenOrtook" afterDelay:0.1];
            
        }
        
        else if ([alert.NotificationList count]==0)
        {
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", chosenCourse.id_medicine];
            NSMutableArray *notificationArray = [[NSMutableArray alloc] initWithArray:[[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate]];
            
            
            if ([notificationArray count]>0) {
                
                
                NSDate *now = [[NSDate alloc] init];
                
                UILocalNotification *notif = [notificationArray objectAtIndex:0];
                
                //27-07-2017 add New Code for Resolve DateTime Issue
                NSDate *prevDate;
                for (NSInteger i =0; i < ([notificationArray count]); i++) {
                    
                    UILocalNotification *notif1 = [notificationArray objectAtIndex:i];
                    if (i<1) {
                        prevDate = notif1.fireDate;
                    }
                    if( [prevDate timeIntervalSinceDate:notif1.fireDate] > 0 )
                    {
                        prevDate = notif1.fireDate;
                        notif = notif1;
                    }
                }
                //27-07-2017 add New Code for Resolve DateTime Issue
                
                [CurrentReminder setDictionary:notif.userInfo];
                
                [Notification postTake:CurrentReminder date:now andNote:notif];
                
                
            }
            
        }
    }
    else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"I Already Took",nil)])
        
    {
        pickerTaal.datePickerMode = UIDatePickerModeDateAndTime;
        
        [title setText:NSLocalizedString(@"Time taken",nil)];
        [self TakenPostoneUp:YES];
        
        
        
    }
    else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"I Already Took All",nil)])
        
    {
        pickerTaal.datePickerMode = UIDatePickerModeDateAndTime;
        
        [title setText:NSLocalizedString(@"Time taken",nil)];
        [self TakenPostoneUp:YES];
        
    }
    else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"Take All Now",nil)])
        
    {
        
        
        
        NSDate *now = [[NSDate alloc] init];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(fireDate <= %@)", now];
        NSMutableArray *notificationArray = [[NSMutableArray alloc] initWithArray:[[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate]];
        
        
        
        for (NSInteger i=0 ; i<[notificationArray count]; i++) {
            
            
            
            UILocalNotification *notif = [notificationArray objectAtIndex:i];
            
            [CurrentReminder setDictionary:notif.userInfo];
            
            NSDate *now = [[NSDate alloc] init];
            
            
            [Notification postTake:CurrentReminder date:now andNote:notif];
            
        }
        
        
        [self performSelector:@selector(reloadTableview:) withObject:@"TakenOrtook" afterDelay:0.4];
        
        
        
    }
    
    else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"More...",nil)])
        
    {
        
        
        pickerTaal.datePickerMode = UIDatePickerModeDateAndTime;
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        [appDelegate.toolbarDown setAlpha:0];
        [appDelegate MultiPop];
        
        [Multicontroller setWhatyouWant:NSLocalizedString(@"take",nil)];
    }
    
    
    
}



-(void)SkipMoreAction:(UIBarButtonItem*)sender

{
    
    
    for (NSInteger i=0; i<[selectedMeds count]; i++) {
        
        
        UILocalNotification *notif = [selectedMeds objectAtIndex:i];
        
        [CurrentReminder setDictionary:notif.userInfo];
        
        [title setText:@" "];
        
        [alert.NotificationList removeAllObjects];
        [Notification postSkip:CurrentReminder andNote:notif];
        
        
        
        
    }
    
    
    
    [self performSelector:@selector(reloadTableview:) withObject:@"SkipMoreAction" afterDelay:0.1];
    
    
    [Multicontroller.table reloadData];
    
    [selectedMeds removeAllObjects];
    UITableView *setimage = (UITableView *)[self.navigationController.view viewWithTag:8978];
    
    [setimage reloadData];
    
    
    
    double delayInSeconds = 0.2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        
        
        
        if ([alert.NotificationList count] ==0) {
            
        }
        else
        {
            [self PostNotfication:YES];
        }
        
    });
    
    Choisestring =@"";
    
    
}


-(void)PostponeMoreAction:(UIBarButtonItem*)sender

{
    
    
    
    
    for (NSInteger i=0; i<[selectedMeds count]; i++) {
        
        
        
        UILocalNotification *notif = [selectedMeds objectAtIndex:i];
        
        [CurrentReminder setDictionary:notif.userInfo];
        
        
        [title setText:NSLocalizedString(@"Postpone To",nil)];
        
        [Notification postPone:CurrentReminder date:Constrant andNote:notif];
        
    }
    
    [self performSelector:@selector(reloadTableview:) withObject:@"PostponeMoreAction" afterDelay:0.1];
    
    [Multicontroller.table reloadData];
    
}

-(void)TakenPostoneUp:(BOOL)UpDown

{
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (NSInteger i =([notificationArray count]); i > 0; i--) {
        
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        if ([[notif.userInfo valueForKey:@"ID_Medicine" ] isEqualToString:[CurrentReminder valueForKey:@"ID_Medicine"]])
        {
            
            
            if ([Choisestring isEqualToString:NSLocalizedString(@"I Already Took All",nil)]) {
                
                
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:notif.fireDate];
                NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:notif.fireDate];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:0];
                
                NSDate *now = [[NSDate alloc] init];
                
                // Constrant = [calendar dateFromComponents:dateComps];
                [pickerTaal setMinimumDate:NULL];
                [pickerTaal setMaximumDate:now];
                
                
                
                [pickerTaal setDate:notif.fireDate];
                
            }
            else if ([Choisestring isEqualToString:NSLocalizedString(@"I Already Took",nil)]) {
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:notif.fireDate];
                NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:notif.fireDate];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]-24];
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:0];
                
                
                
                
                
                NSDate *now = [[NSDate alloc] init];
                [pickerTaal setDate:notif.fireDate];
                [pickerTaal setMinimumDate:NULL];
                [pickerTaal setMaximumDate:now];
                
            }
            
            else
            {
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:pickerTaal.date];
                NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:pickerTaal.date];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:0];
                
                
                //Constrant = [calendar dateFromComponents:dateComps];
                
                
                NSDate *now = [[NSDate alloc] init];
                [pickerTaal setDate:pickerTaal.date];
                [pickerTaal setMinimumDate:now];
                [pickerTaal setMaximumDate:NULL];
                
            }
            
            
        }
        
    }
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    
    
    [UIView setAnimationDuration:0.5f];
    
    if (!UpDown) {
        
        
        [Postponeview setFrame:CGRectMake(0, window.frame.size.height+5, 320, 340)];
        
        
        
    }
    else
    {
        [Postponeview setFrame:CGRectMake(0, window.frame.size.height-240, 320, 340)];
        
        
        
        
        
    }
    
    
    [UIView commitAnimations];
    
    
    
    
    
}




-(void)Postponeit:(UIBarButtonItem*)sender
{
    
    [alert fresh];
    
    
    pickerTaal.datePickerMode = UIDatePickerModeTime;
    
    //[self loadingviewOn];
    
    
    [self TakenPostoneUp:NO];
    
    
    if ([sender.title isEqualToString:NSLocalizedString(@" Cancel",nil)])
    {
        
        
    }
    else if ([sender.title isEqualToString:NSLocalizedString(@"Done ",nil)]) {
        
        
        
        if ([alert.NotificationList count]==0) {
            
            if ([Choisestring isEqualToString:NSLocalizedString(@"I Already Took",nil)]) {
                
                
                ////NSLog(@"%@", chosenCourse.id_medicine);
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", chosenCourse.id_medicine];
                NSMutableArray *notificationArray = [[NSMutableArray alloc] initWithArray:[[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate]];
                
                
                if ([notificationArray count]>0) {
                    
                    
                    
                    UILocalNotification *notif = [notificationArray objectAtIndex:0];
                    //27-07-2017 add New Code for Resolve DateTime Issue
                    NSDate *prevDate;
                    for (NSInteger i =0; i < ([notificationArray count]); i++) {
                        
                        UILocalNotification *notif1 = [notificationArray objectAtIndex:i];
                        if (i<1) {
                            prevDate = notif1.fireDate;
                        }
                        if( [prevDate timeIntervalSinceDate:notif1.fireDate] > 0 )
                        {
                            prevDate = notif1.fireDate;
                            notif = notif1;
                        }
                    }
                    //27-07-2017 add New Code for Resolve DateTime Issue
                    
                    
                    [CurrentReminder setDictionary:notif.userInfo];
                    
                    
                    
                    
                    [title setText:NSLocalizedString(@"Postpone To",nil)];
                    
                    
                    [Notification postTakeEarly:CurrentReminder date:Constrant andNote:notif];
                    
                    
                    
                    Choisestring =@"";
                    
                }
                
            }
            
            else  if ([Choisestring isEqualToString:NSLocalizedString(@"Postpone",nil)]) {
                
                
                
                UILocalNotification *notif = [curentChoise objectAtIndex:0];
                [CurrentReminder setDictionary:notif.userInfo];
                
                
                
                [Notification postPone:CurrentReminder date:Constrant andNote:notif];
            }
            
            
        }
        else
        {
            
            if ([Choisestring isEqualToString:NSLocalizedString(@"I Already Took All",nil)]) {
                
                
                NSDate *now = [[NSDate alloc] init];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(fireDate <= %@)", now];
                NSMutableArray *notificationArray = [[NSMutableArray alloc] initWithArray:[[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate]];
                
                
                
                for (NSInteger i=0 ; i<[notificationArray count]; i++) {
                    
                    
                    
                    UILocalNotification *notif = [notificationArray objectAtIndex:i];
                    [CurrentReminder setDictionary:notif.userInfo];
                    
                    
                    
                    
                    [title setText:NSLocalizedString(@"Postpone To",nil)];
                    
                    
                    [Notification postTakeEarly:CurrentReminder date:Constrant andNote:notif];
                    
                    
                    
                    Choisestring =@"";
                    
                    
                }
                
                
                
                
                
                
                [self performSelector:@selector(reloadTableview:) withObject:@"Postponeit" afterDelay:0.1];
            }
            else  if ([Choisestring isEqualToString:NSLocalizedString(@"I Already Took",nil)]) {
                
                
                
                if ([alert.NotificationList count]==0)
                {
                    
                    
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", chosenCourse.id_medicine];
                    NSMutableArray *notificationArray = [[NSMutableArray alloc] initWithArray:[[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate]];
                    
                    
                    
                    if ([notificationArray count]>0) {
                        
                        
                        
                        UILocalNotification *notif = [notificationArray objectAtIndex:0];
                        
                        //27-07-2017 add New Code for Resolve DateTime Issue
                        NSDate *prevDate;
                        for (NSInteger i =0; i < ([notificationArray count]); i++) {
                            
                            UILocalNotification *notif1 = [notificationArray objectAtIndex:i];
                            if (i<1) {
                                prevDate = notif1.fireDate;
                            }
                            if( [prevDate timeIntervalSinceDate:notif1.fireDate] > 0 )
                            {
                                prevDate = notif1.fireDate;
                                notif = notif1;
                            }
                        }
                        //27-07-2017 add New Code for Resolve DateTime Issue
                        
                        [CurrentReminder setDictionary:notif.userInfo];
                        
                        
                        
                        [Notification postTakeEarly:CurrentReminder date:Constrant andNote:notif];
                        
                        
                        ////NSLog(@"let op");
                        
                    }
                    
                }
                
                else
                {
                    for (NSInteger i=0 ; i<[alert.NotificationList count]; i++) {
                        
                        
                        NSDictionary *dictionary = [alert.NotificationList objectAtIndex:i];
                        NSArray *array = [dictionary objectForKey:@"Name"];
                        
                        
                        
                        for (NSInteger k =0 ; k<[array count]; k++) {
                            
                            
                            double delayInSeconds = 0.2*(i*k);
                            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                
                                UILocalNotification *notif = [array objectAtIndex:k];
                                [CurrentReminder setDictionary:notif.userInfo];
                                
                                
                                
                                
                                [title setText:NSLocalizedString(@"Postpone To",nil)];
                                
                                
                                
                                [Notification postTakeEarly:CurrentReminder date:Constrant andNote:notif];
                                
                                
                                
                                Choisestring =@"";
                                
                            });
                            
                        }
                        
                        
                        
                    }
                    
                }
                
                
                [self performSelector:@selector(reloadTableview:) withObject:@"Postponeit" afterDelay:0.1];
                
                
            }
            else  if ([Choisestring isEqualToString:NSLocalizedString(@"Postpone",nil)]) {
                
                
                
                if ([selectedMeds count]>0) {
                    
                    
                    for (NSInteger i=0 ; i<[selectedMeds count]; i++) {
                        
                        
                        
                        
                        
                        UILocalNotification *notif = [selectedMeds objectAtIndex:i];
                        
                        [alert.NotificationList removeObject:notif];
                        
                        [CurrentReminder setDictionary:notif.userInfo];
                        
                        [Notification postPone:CurrentReminder date:Constrant andNote:notif];
                        
                        
                        
                    }
                    
                    
                    [selectedMeds removeAllObjects];
                    
                    
                    [Multicontroller dismissViewControllerAnimated:YES completion:NULL];
                    
                    Choisestring =@"";
                    
                    
                }
                
                else
                {
                    if ([alert.NotificationList count]>0) {
                        
                        
                        for (NSInteger i=0 ; i<[alert.NotificationList count]; i++) {
                            
                            
                            
                            
                            NSDictionary *dictionary = [alert.NotificationList objectAtIndex:i];
                            NSArray *array = [dictionary objectForKey:@"Name"];
                            
                            for (NSInteger k =0 ; k<[array count]; k++) {
                                
                                double delayInSeconds = 0.2*(k*i);
                                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                    
                                    UILocalNotification *notif = [array objectAtIndex:k];
                                    [CurrentReminder setDictionary:notif.userInfo];
                                    
                                    [Notification postPone:CurrentReminder date:Constrant andNote:notif];
                                    
                                    
                                });
                                
                                
                                
                            }
                            
                        }
                    }
                    
                    
                    Choisestring =@"";
                    
                }
                
                
                
                [self performSelector:@selector(reloadTableview:) withObject:@"Postponeit" afterDelay:0.1];
                
            }
            else  if ([Choisestring isEqualToString:NSLocalizedString(@"Postpone All",nil)]) {
                
                
                
                NSDate *now = [[NSDate alloc] init];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(fireDate <= %@)", now];
                NSMutableArray *notificationArray = [[NSMutableArray alloc] initWithArray:[[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate]];
                
                
                
                for (NSInteger i=0 ; i<[notificationArray count]; i++) {
                    
                    
                    UILocalNotification *notif = [notificationArray objectAtIndex:i];
                    [CurrentReminder setDictionary:notif.userInfo];
                    [title setText:NSLocalizedString(@"Postpone To",nil)];
                    
                    
                    [Notification postPone:CurrentReminder date:Constrant andNote:notif];
                    
                    Choisestring =@"";
                    
                }
                
                
                
                
                
                [self performSelector:@selector(reloadTableview:) withObject:@"Postponeit" afterDelay:0.1];
                
                
            }
            else  if ([Choisestring isEqualToString:NSLocalizedString(@"More...",nil)]) {
                
                
                
                for (NSInteger i=0; i<[selectedMeds count]; i++) {
                    
                    
                    UILocalNotification *notif = [selectedMeds objectAtIndex:i];
                    [CurrentReminder setDictionary:notif.userInfo];
                    
                    
                    
                    [alert.NotificationList removeObject:notif];
                    [title setText:NSLocalizedString(@"Postpone To",nil)];
                    
                    
                    [Notification postTake:CurrentReminder date:Constrant andNote:notif];
                    
                    
                    
                    
                    
                    
                    
                    
                }
                
                
                [self performSelector:@selector(reloadTableview:) withObject:@"Postponeit" afterDelay:0.1];
                
                [selectedMeds removeAllObjects];
                
                
                [Multicontroller.table reloadData];
                
                [Multicontroller dismissViewControllerAnimated:YES completion:NULL];
                
                
                
            }
            
            
        }
        
        
    }
    
    
    
    
    
    
    UITableView *setimage = (UITableView *)[self.navigationController.view viewWithTag:8978];
    
    [setimage reloadData];
    
    double delayInSeconds = 0.2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        if ([alert.NotificationList count] ==0) {
        }
        else
        {
            [self PostNotfication:YES];
            
            
        }
        
    });
    
    
    
}

-(void)Postpone:(UIButton*)sender

{
    
    
    pickerTaal.datePickerMode = UIDatePickerModeTime;
    //[self loadingviewOn];

    Choisestring =sender.titleLabel.text;
    [self PostNotfication:NO];
    
    
    
    if ([alert.NotificationList count] >0) {
        
        
        for (NSInteger i=0 ; i<[alert.NotificationList count]; i++) {
            
            
            NSDictionary *dictionary = [alert.NotificationList objectAtIndex:i];
            NSArray *array = [dictionary objectForKey:@"Name"];
            
            
            if ([array count]>1) {
                
                [self TakenPostpone:YES];
            }
            else
            {
                [title setText:NSLocalizedString(@"Postpone To",nil)];
                [self TakenPostpone:YES];
            }
            
        }
        
        
        
    }
    else  if ([alert.NotificationList count] ==0) {
        ;
        [title setText:NSLocalizedString(@"Postpone To",nil)];
        [self TakenPostpone:YES];
        
    }
    else
        
    {
        
        
        [self TakenPostpone:YES];
        
    }
    
    
}



- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            
            abort();
        }
    }
    
    
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
//        _managedObjectContext = [[NSManagedObjectContext alloc] init]; //10-07-2017
        _managedObjectContext = [[NSManagedObjectContext alloc]initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSLog(@"PATH : %@",storeURL);
    
    NSDictionary *options =[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],NSMigratePersistentStoresAutomaticallyOption,[NSNumber numberWithBool:YES],NSInferMappingModelAutomaticallyOption,nil];
    NSError *error = nil;
    
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        
        abort();
    }
    
    return _persistentStoreCoordinator;
}


#pragma mark - seeifEmptyMood


-(void)seeifEmptyMood

{
    
    
    
    NSMutableArray *copy = [GetData getHistoryTotal];
    
    NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
    [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel setDateFormat:[self convertString:@"dd/MM/yyyy hh:mm a"]];
    
    
    NSDateFormatter *dateLevel = [[NSDateFormatter alloc] init];
    [dateLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateLevel setDateFormat:@"dd/MM/yyyy"];
    
    NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
    [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel24 setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"History"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count]==0) {
        
        
        for (NSInteger i =0; i < [copy count]; i++) {
            
            
            currentHistory =(History*) [NSEntityDescription insertNewObjectForEntityForName:@"History" inManagedObjectContext:[self managedObjectContext]];
            
            currentHistory.id =[[copy objectAtIndex:i] valueForKey:@"ID"];
            currentHistory.date =[dateLevel dateFromString:[[copy objectAtIndex:i] valueForKey:@"Date"]];
            currentHistory.count =[[copy objectAtIndex:i] valueForKey:@"Count"];
            
            currentHistory.time =[dateFormatLevel dateFromString:[[copy objectAtIndex:i] valueForKey:@"Time"]];
            currentHistory.name =[[copy objectAtIndex:i] valueForKey:@"Name"];
            currentHistory.dose =[[copy objectAtIndex:i] valueForKey:@"Dose"];
            currentHistory.taken =[[copy objectAtIndex:i] valueForKey:@"Taken"];
            currentHistory.idmedicine =[[copy objectAtIndex:i] valueForKey:@"ID_medicine"];
            currentHistory.count =[[copy objectAtIndex:i] valueForKey:@"Count"];
            currentHistory.timeaction =[dateFormatLevel dateFromString:[[copy objectAtIndex:i] valueForKey:@"Time_Action"]];
            currentHistory.action =[[copy objectAtIndex:i] valueForKey:@"Action"];
            currentHistory.quantity =[[copy objectAtIndex:i] valueForKey:@"Quantity"];
            currentHistory.type =[[copy objectAtIndex:i] valueForKey:@"Type"];
            currentHistory.timeacaction24 =[dateFormatLevel dateFromString:[[copy objectAtIndex:i] valueForKey:@"Time_Action_24"]];
            currentHistory.originaltime =[dateFormatLevel dateFromString:[[copy objectAtIndex:i] valueForKey:@"Original_Time"]];
            currentHistory.actiondate =[dateFormatLevel dateFromString:[[copy objectAtIndex:i] valueForKey:@"Action_Date"]];
            currentHistory.send =@"NO";
            
            
            ////NSLog(@"%@", currentHistory);
            
            NSError *error = nil;
            NSManagedObjectContext *context = self.managedObjectContext;
            if (![context save:&error]) {
                
            }
            
            
        }
        
    }
    else
    {
        
        
        
        
        
    }
    
    
    
}

-(void)undoHistory:(UILocalNotification*)set
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ////////////////////////////////////////////////////////////NSLog(@"%@", set.userInfo);
    
    NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
    [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel setDateFormat:[appDelegate convertString:@"hh:mm a"]];
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:set.fireDate];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:set.fireDate];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    [dateCompsEarly setDay:[dateComponents day]];
    [dateCompsEarly setMonth:[dateComponents month]];
    [dateCompsEarly setYear:[dateComponents year]];
    [dateCompsEarly setHour:[timeComponents hour]];
    [dateCompsEarly setMinute:timeComponents.minute];
    [dateCompsEarly setSecond:30];
    NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
    
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"History"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        currentHistory = [fetchedObjects objectAtIndex:i];
        
        if ([currentHistory.taken isEqualToString:NSLocalizedString(@"Postponed",nil)])
        {
            
            if ([[dateFormatLevel stringFromDate:set.fireDate] isEqualToString:[dateFormatLevel stringFromDate:currentHistory.originaltime]])
                
            {
                
                if ([[set.userInfo valueForKey:@"Name"] isEqualToString:currentHistory.name]) {
                    
                    [context deleteObject:currentHistory];
                    
                    
                    if (![context save:&error]) {
                        
                    }
                    else
                        
                    {
                        
                        
                    }
                    
                    
                }
                
            }
            
            
        }
        
        else
        {
            
            
            if ([[dateFormatLevel stringFromDate:itemNext] isEqualToString:[dateFormatLevel stringFromDate:currentHistory.originaltime]])
                
            {
                
                if ([[set.userInfo valueForKey:@"Name"] isEqualToString:currentHistory.name]) {
                    
                    
                    ////////////////////////////////////////////////////////////NSLog(@"%@", newCourse);
                    
                    [context deleteObject:currentHistory];
                    
                    
                    if (![context save:&error]) {
                        
                    }
                    else
                        
                    {
                        
                        
                    }
                    
                }
                
            }
        }
        
        
        
    }
    
    
    
    
    
    [self performSelector:@selector(reloadTableview:) withObject:@"insertHistory" afterDelay:0.1];
    
    
    
    
    
    
}

-(void)seeifEmptyProfiles

{
    NSArray *profileItem =[NSArray arrayWithObjects:@"Name",@"ID",@"Image",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Silence",@"Vibrate",nil];
    
    
    NSMutableArray *copy = [SaveCore getProfiles:profileItem];
    
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count]==0) {
        
        
        for (NSInteger i =0; i < [copy count]; i++) {
            
            
            Profiles *newCourse =(Profiles*) [NSEntityDescription insertNewObjectForEntityForName:@"Profiles" inManagedObjectContext:[self managedObjectContext]];
            
            newCourse.id =[[copy objectAtIndex:i] valueForKey:@"ID"];
            newCourse.audio =[[copy objectAtIndex:i] valueForKey:@"Audio"];
            newCourse.coloralpha =[[copy objectAtIndex:i] valueForKey:@"ColorGreen"];
            newCourse.colorblue =[[copy objectAtIndex:i] valueForKey:@"ColorRed"];
            newCourse.colorRed =[[copy objectAtIndex:i] valueForKey:@"Coloralpha"];
            newCourse.colorGreen =[[copy objectAtIndex:i] valueForKey:@"Colorblue"];
            
            NSData *myData = UIImagePNGRepresentation([[copy objectAtIndex:i] valueForKey:@"Image"]);
            newCourse.image =myData;
            newCourse.name =[[copy objectAtIndex:i] valueForKey:@"Name"];
            newCourse.silence =[[copy objectAtIndex:i] valueForKey:@"Silence"];
            newCourse.vibrate =[[copy objectAtIndex:i] valueForKey:@"Vibrate"];
            
            NSError *error = nil;
            NSManagedObjectContext *context = self.managedObjectContext;
            if (![context save:&error]) {
                
            }
            
            
        }
        
    }
    else
    {
        
        
        
        
        
    }
    
    
    
}

-(void)goSeeIfProfileSend

{
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSInteger i =0; i < ([fetchedObjects count]); i++) {
        
        currentProfile= [fetchedObjects objectAtIndex:i];
        
        
        
        [insertPatient insertPatientNew:[[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"] with:currentProfile];
        
        //////////////NSLog(@"not send %@", currentProfile);
        
        NSError *error = nil;
        NSManagedObjectContext *context = self.managedObjectContext;
        
        NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc] init];
        
        [fetchRequest2 setEntity:[NSEntityDescription entityForName:@"History"
                                             inManagedObjectContext:[self managedObjectContext]]];
        
        
        
        NSString *query = [NSString stringWithFormat:@"(id contains [cd]'%@') AND (send contains [cd]'%@')", currentProfile.id, @"NO"];
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:query];
        
        
        [fetchRequest2 setPredicate:predicate];
        
        
        [fetchRequest2 setFetchLimit:5];
        
        NSArray *fetchedObjects2 = [context executeFetchRequest:fetchRequest2 error:&error];
        
        for (NSInteger k =0; k < ([fetchedObjects2 count]); k++) {
            
            
            currentHistory= [fetchedObjects2 objectAtIndex:k];
            //////////////NSLog(@"%@", currentHistory);
            
            if ([currentHistory.send isEqualToString:@"YES"]) {
                
                
                //////////NSLog(@"YES %@", currentHistory);
            }
            else
            {
                
                //////////NSLog(@"%@", currentHistory);
                
                //03-08-2017
                
//                [insertHistory InsertEventwhatmissing:currentHistory];
                
            }
        }
    }
}

-(void)goSeeIfMedicineisSend

{
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Medicine"
                                        inManagedObjectContext:[self managedObjectContext]]];
    
    
    
    //@"name.length > 0"
    
    NSString *query = [NSString stringWithFormat:@"send contains [cd]'%@'", @"NO"];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:query];
    
    
    [fetchRequest setPredicate:predicate];
    
    
    
    
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSInteger i =0; i < ([fetchedObjects count]); i++) {
        
        chosenCourse= [fetchedObjects objectAtIndex:i];
        
        if ([chosenCourse.send isEqualToString:@"YES"]) {
            
            
        }
        else
        {
            
            
            //////////////NSLog(@"send %@", chosenCourse);
            
            //03-08-2017
//            [insertMeds insertMedicineMissing:[[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"] with:chosenCourse];
            
            
            
            
        }
        
        
        
        
        
    }
    
    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSError *error = nil;
        
        if (![context save:&error]) {
            
        }
        
    });
    
    
    
    
}


-(void)goSeeIfHistorySend

{
    
    ////NSLog(@"goSeeIfHistorySend");
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"History"
                                        inManagedObjectContext:[self managedObjectContext]]];
    
    
    
    
    
    NSString *query = [NSString stringWithFormat:@"(send contains [cd]'%@') OR (send < 1)", @"NO"];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:query];
    
    
    [fetchRequest setPredicate:predicate];
    
    
    [fetchRequest setFetchLimit:4];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSInteger i =0; i < ([fetchedObjects count]); i++) {
        
        currentHistory= [fetchedObjects objectAtIndex:i];
        
        ////NSLog(@"send %@", [fetchedObjects objectAtIndex:i]);
        
        //03-08-2017
//        [insertHistory insertHistoryMissing:[[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"] with:currentHistory];
        
        
        
        
    }
    
    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSError *error = nil;
        
        
        if (![context save:&error]) {
            
        }
        
    });
}

-(void)insertMedicine:(NSMutableDictionary*)set
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSData *data;
    NSDate *now = [[NSDate alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", [set valueForKey:@"ID_Medicine"]];


    NSArray *notificationArray = [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
      NSDate *prevDate=[[NSDate alloc]init];
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        if (i<1) {
            prevDate = notif.fireDate;
        }
        if( [prevDate timeIntervalSinceDate:notif.fireDate] > 0 )
        {
            prevDate = notif.fireDate;
            now = notif.fireDate;
            data = [NSKeyedArchiver archivedDataWithRootObject:notif];
        }
    }
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:[appDelegate convertString:@"hh:mm a EEEE dd/MM/yy"]];
    
    
    BOOL Insert = NO;
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"id_medicine = %@", [set valueForKey:@"ID_Medicine"]];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Medicine" inManagedObjectContext:[self managedObjectContext]]];
    [fetchRequest setPredicate:predicate2];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        appDelegate.chosenCourse = [fetchedObjects objectAtIndex:i];
        //////////////////NSLog(@"%@", fetchedObjects);
        
        appDelegate.chosenCourse.id =[set valueForKey:@"ID"];
        appDelegate.chosenCourse.name =[set valueForKey:@"Name"];
        appDelegate.chosenCourse.type =[set valueForKey:@"Type"];
        appDelegate.chosenCourse.strength =[set valueForKey:@"Strength"];
        appDelegate.chosenCourse.profile =[set valueForKey:@"Profile"];
        appDelegate.chosenCourse.schedule =[set valueForKey:@"Schedule"];
        appDelegate.chosenCourse.frequency =[set valueForKey:@"Frequency"];
        appDelegate.chosenCourse.remaining =[set valueForKey:@"Remaining"];
        appDelegate.chosenCourse.filling =[set valueForKey:@"Filling"];
        appDelegate.chosenCourse.doctor =[set valueForKey:@"Doctor"];
        appDelegate.chosenCourse.prescription =[set valueForKey:@"Prescription"];
        appDelegate.chosenCourse.notes =[set valueForKey:@"Notes"];
        appDelegate.chosenCourse.image =[set valueForKey:@"Image"];
        appDelegate.chosenCourse.id_medicine =[set valueForKey:@"ID_Medicine"];
        appDelegate.chosenCourse.colorRed =[set valueForKey:@"ColorRed"];
        appDelegate.chosenCourse.colorGreen =[set valueForKey:@"ColorGreen"];
        appDelegate.chosenCourse.colorblue =[set valueForKey:@"Colorblue"];
        appDelegate.chosenCourse.audio =[set valueForKey:@"Audio"];
        appDelegate.chosenCourse.coloralpha =[set valueForKey:@"Coloralpha"];
        appDelegate.chosenCourse.quantity =[set valueForKey:@"Quantity"];
        appDelegate.chosenCourse.duration =[set valueForKey:@"Duration"];
        appDelegate.chosenCourse.instructions =[set valueForKey:@"Instructions"];
        appDelegate.chosenCourse.perscriptionImage =[set valueForKey:@"PerscriptionImage"];
        
        if ([[NSString stringWithFormat:@"%@", [set valueForKey:@"Starting"]] rangeOfString:@":"].location==NSNotFound)
        {
            //////////////NSLog(@"NSNotFound");
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy"];
            NSDate *dateit = [dateFormatter dateFromString:[set valueForKey:@"Starting"]];
            appDelegate.chosenCourse.starting1 =dateit;
        }
        else
        {
            //////////////NSLog(@"Found");
            
            appDelegate.chosenCourse.starting1 =[set valueForKey:@"Starting"];
        }
        
        appDelegate.chosenCourse.recording =[set valueForKey:@"Recording"];
        appDelegate.chosenCourse.frequency_text =[set valueForKey:@"Frequency_text"];
        appDelegate.chosenCourse.firedate = now;
        appDelegate.chosenCourse.changeDate =@" ";
        appDelegate.chosenCourse.notifications =[set valueForKey:@"Notifications"];
        appDelegate.chosenCourse.times =[set valueForKey:@"Times"];
        appDelegate.chosenCourse.dates =[set valueForKey:@"Dates"];
        appDelegate.chosenCourse.message =[set valueForKey:@"Message"];
        appDelegate.chosenCourse.day_number =[set valueForKey:@"Day_number"];
        appDelegate.chosenCourse.phone_pharmacy =[set valueForKey:@"Phone_Pharmacy"];
        appDelegate.chosenCourse.phone_doctor =[set valueForKey:@"Phone_Doctor"];
        appDelegate.chosenCourse.email_doctor =[set valueForKey:@"eMail_Doctor"];
        appDelegate.chosenCourse.email_pharmacy =[set valueForKey:@"eMail_Pharmacy"];
        appDelegate.chosenCourse.pharmacy =[set valueForKey:@"Pharmacy"];
        appDelegate.chosenCourse.refilling=[set valueForKey:@"Refilling"];
        appDelegate.chosenCourse.strength_unit =[set valueForKey:@"Strength_unit"];
        appDelegate.chosenCourse.dose_unit=[set valueForKey:@"Dose_unit"];
        appDelegate.chosenCourse.notifications = data;
        appDelegate.chosenCourse.location =[set valueForKey:@"Location"];
        appDelegate.chosenCourse.active =[set valueForKey:@"active"];
        
        
        
        
        if (![context save:&error]) {
            
        }
        else
            
        {
            
            
        }
        
        
        Insert=YES;
        
        
        break;
        
        
        
        
    }
    
    
    if (!Insert) {
        
        appDelegate.chosenCourse =(Medicine*) [NSEntityDescription insertNewObjectForEntityForName:@"Medicine" inManagedObjectContext:[self managedObjectContext]];
        
        
        
        appDelegate.chosenCourse.id =[set valueForKey:@"ID"];
        appDelegate.chosenCourse.name =[set valueForKey:@"Name"];
        appDelegate.chosenCourse.type =[set valueForKey:@"Type"];
        appDelegate.chosenCourse.profile =[set valueForKey:@"Profile"];
        appDelegate.chosenCourse.schedule =[set valueForKey:@"Schedule"];
        appDelegate.chosenCourse.frequency =[set valueForKey:@"Frequency"];
        appDelegate.chosenCourse.remaining =[set valueForKey:@"Remaining"];
        appDelegate.chosenCourse.filling =[set valueForKey:@"Filling"];
        appDelegate.chosenCourse.doctor =[set valueForKey:@"Doctor"];
        appDelegate.chosenCourse.prescription =[set valueForKey:@"Prescription"];
        appDelegate.chosenCourse.notes =[set valueForKey:@"Notes"];
        appDelegate.chosenCourse.image =[set valueForKey:@"Image"];
        appDelegate.chosenCourse.id_medicine =[set valueForKey:@"ID_Medicine"];
        appDelegate.chosenCourse.colorRed =[set valueForKey:@"ColorRed"];
        appDelegate.chosenCourse.colorGreen =[set valueForKey:@"ColorGreen"];
        appDelegate.chosenCourse.colorblue =[set valueForKey:@"Colorblue"];
        appDelegate.chosenCourse.audio =[set valueForKey:@"Audio"];
        appDelegate.chosenCourse.coloralpha =[set valueForKey:@"Coloralpha"];
        appDelegate.chosenCourse.quantity =[set valueForKey:@"Quantity"];
        appDelegate.chosenCourse.duration =[set valueForKey:@"Duration"];
        appDelegate.chosenCourse.instructions =[set valueForKey:@"Instructions"];
        appDelegate.chosenCourse.perscriptionImage =[set valueForKey:@"PerscriptionImage"];
        
        
        
        
        if ([[NSString stringWithFormat:@"%@", [set valueForKey:@"Starting"]] rangeOfString:@":"].location==NSNotFound)
            
        {
            
            
            //////////////NSLog(@"NSNotFound");
            
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy"];
            NSDate *dateit = [dateFormatter dateFromString:[set valueForKey:@"Starting"]];
            appDelegate.chosenCourse.starting1 =dateit;
            
        }
        
        else
        {
            
            
            //////////////NSLog(@"Found");
            
            appDelegate.chosenCourse.starting1 =[set valueForKey:@"Starting"];
            
        }
        
        
        appDelegate.chosenCourse.recording =[set valueForKey:@"Recording"];
        appDelegate.chosenCourse.frequency_text =[set valueForKey:@"Frequency_text"];
        appDelegate.chosenCourse.firedate =now;
        appDelegate.chosenCourse.changeDate =@" ";
        appDelegate.chosenCourse.notifications =[set valueForKey:@"Notifications"];
        appDelegate.chosenCourse.times =[set valueForKey:@"Times"];
        appDelegate.chosenCourse.dates =[set valueForKey:@"Dates"];
        appDelegate.chosenCourse.message =[set valueForKey:@"Message"];
        appDelegate.chosenCourse.day_number =[set valueForKey:@"Day_number"];
        appDelegate.chosenCourse.phone_pharmacy =[set valueForKey:@"Phone_Pharmacy"];
        appDelegate.chosenCourse.phone_doctor =[set valueForKey:@"Phone_Doctor"];
        appDelegate.chosenCourse.email_doctor =[set valueForKey:@"eMail_Doctor"];
        appDelegate.chosenCourse.email_pharmacy =[set valueForKey:@"eMail_Pharmacy"];
        appDelegate.chosenCourse.pharmacy =[set valueForKey:@"Pharmacy"];
        appDelegate.chosenCourse.refilling=[set valueForKey:@"Refilling"];
        appDelegate.chosenCourse.strength_unit =[set valueForKey:@"Strength_unit"];
        appDelegate.chosenCourse.dose_unit=[set valueForKey:@"Dose_unit"];
        appDelegate.chosenCourse.notifications =data;
        appDelegate.chosenCourse.location =[set valueForKey:@"Location"];
        appDelegate.chosenCourse.active =[set valueForKey:@"active"];
        
        
        
        
        
        if (![context save:&error]) {
            
        }
        else
            
        {
            
            
        }
        
        
    }
    else
    {
        
        
        
    }
    
    
    
    
}

-(NSString*)changesDone:(NSString*)text

{
    NSArray *go =[[NSUserDefaults standardUserDefaults] valueForKey:@"AppleLanguages"];
    
    NSString *strLang=[go objectAtIndex:0];
    
    
   if ([strLang isEqualToString:@"de"])
    {
        
        
        return [[text stringByReplacingOccurrencesOfString:@"PM" withString:NSLocalizedString(@"PM",nil)]stringByReplacingOccurrencesOfString:@"AM" withString:NSLocalizedString(@"AM",nil)];
        
    }
    
    else if ([strLang isEqualToString:@"el"])
    {
        
        
        return [[text stringByReplacingOccurrencesOfString:@"PM" withString:NSLocalizedString(@"PM",nil)]stringByReplacingOccurrencesOfString:@"AM" withString:NSLocalizedString(@"AM",nil)];
        
    }
    
    else
    {
        
        
        return [[text stringByReplacingOccurrencesOfString:NSLocalizedString(@"PM",nil) withString:@"PM"]stringByReplacingOccurrencesOfString:NSLocalizedString(@"AM",nil) withString:@"AM"];
        
        
    }
    
    
    
}
-(void)insertMedicineNew:(NSString*)termit with:(NSMutableDictionary*)send

{
    [insertMeds insertMedicineNew:termit with:send];
}


-(void)deleteUndo:(NSString*)ID
{
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Notifications"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        Notifications *newCourse = [fetchedObjects objectAtIndex:i];
        
        if ([ID isEqualToString:newCourse.id]) {
            
            ////////////////////////////////////////////////////NSLog(@"newCourse %@", newCourse);
            [context deleteObject:newCourse];
            
            if (![context save:&error]) {
                
            }
            else
                
            {
                
                
            }
            
        }
        
    }
    
    
    //////////////////////////////////////////////////////NSLog(@"%@",ID);
    
}


-(void)insertSetting;
{
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Settings"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count]==0) {
        
        
        Settings *newCourse =(Settings*) [NSEntityDescription insertNewObjectForEntityForName:@"Settings" inManagedObjectContext:[self managedObjectContext]];
        
        
        newCourse.id =@"1";
        newCourse.disturb_in =@"12:00";
        newCourse.disturb_in_ad =@"12:00";
        newCourse.disturb_out =@"AM";
        newCourse.disturb_out_ad =@"AM";
        newCourse.early =@"30";
        newCourse.late =@"30";
        newCourse.history =@"5";
        newCourse.second_alert =@"0";
        
        if (![context save:&error]) {
            
        }
        else
            
        {
            
            
        }
        
    }
    
}





-(void)insertNotifications:(NSMutableDictionary*)set withdata:(NSData*)datajobs
{
    ////NSLog(@"insertNotifications");
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    BOOL *Insert = NO;
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", [set valueForKey:@"ID_Medicine"]];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Notifications" inManagedObjectContext:[self managedObjectContext]]];
    [fetchRequest setPredicate:predicate];
    
    
    NSSortDescriptor *sortDescriptor = nil;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"id"
                                                 ascending:NO];
    NSArray *sortDescriptors = nil;
    sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    ////NSLog(@"%@", fetchedObjects);
    
    
    for (NSInteger i =0; i < ([fetchedObjects count]); i++) {
        
        Notifications *newCourse = [fetchedObjects objectAtIndex:i];
        
        
        UILocalNotification *notif = (UILocalNotification*)[NSKeyedUnarchiver unarchiveObjectWithData:datajobs];
        
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notif];
        
        
        
        if ([[notif.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            //////////////////////////////////////////////////NSLog(@"1");
        }
        
        else
        {
            
            //////////////////////////////////////////////////NSLog(@"2");
            
            if (notif.repeatInterval ==0) {
                
            }
            else
            {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                
                [dateFormatter setDateFormat:[self convertString:@"dd/MM/yyyy hh:mm a"]];
                
                newCourse.id =[set valueForKey:@"ID_Medicine"];
                newCourse.notifications = data;
                newCourse.firedate =[dateFormatter stringFromDate:notif.fireDate];
                newCourse.changeDate =[dateFormatter stringFromDate:notif.fireDate];
                
                ////////////////////////////////////////////////////NSLog(@"%@ %@", newCourse, notif.fireDate);
                
                if (![context save:&error]) {
                    
                }
                else
                    
                {
                    
                    
                }
                
                Insert =YES;
                
                break;
            }
            
            
        }
        
        break;
    }
    
    
    
    if (!Insert) {
        
        //////////////////////////////////////////////////NSLog(@"3");
        
        Notifications *newCourse =(Notifications*) [NSEntityDescription insertNewObjectForEntityForName:@"Notifications" inManagedObjectContext:[self managedObjectContext]];
        
        newCourse.id =[set valueForKey:@"ID_Medicine"];
        newCourse.notifications =datajobs;
        newCourse.firedate =[set valueForKey:@"Firedate"];
        newCourse.changeDate =[set valueForKey:@"ChangeDate"];
        
        
        // //////////////////////////////////////////////////NSLog(@"%@", newCourse);
        
        
        if (![context save:&error]) {
            
        }
        else
            
        {
            
            
        }
        
    }
    
    
    
}

-(BOOL)insertProfile:(NSMutableDictionary*)set

{
    BOOL *Insert = NO;
    
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        Profiles *newCourse = [fetchedObjects objectAtIndex:i];
        
        if ([[set valueForKey:@"ID"] isEqualToString:newCourse.id]) {
            
            
            newCourse.id =[set valueForKey:@"ID"];
            newCourse.audio =[set valueForKey:@"Audio"];
            newCourse.colorGreen =[set valueForKey:@"ColorGreen"];
            newCourse.colorRed =[set valueForKey:@"ColorRed"];
            newCourse.coloralpha=[set valueForKey:@"Coloralpha"];
            newCourse.colorblue =[set valueForKey:@"Colorblue"];
            
            NSData *myData = UIImagePNGRepresentation([set valueForKey:@"Image"]);
            newCourse.image =myData;
            newCourse.name =[set valueForKey:@"Name"];
            newCourse.silence =[set valueForKey:@"Silence"];
            newCourse.vibrate =[set valueForKey:@"Vibrate"];
            
            Insert=YES;
            
            
            if (![context save:&error]) {
                
            }
            else
            {
                
                
            }
        }
    }
    
    
    if (!Insert) {
        
        Profiles *newCourse =(Profiles*) [NSEntityDescription insertNewObjectForEntityForName:@"Profiles" inManagedObjectContext:[self managedObjectContext]];
        
        
        newCourse.id =[set valueForKey:@"ID"];
        newCourse.audio =[set valueForKey:@"Audio"];
        newCourse.colorGreen =[set valueForKey:@"ColorGreen"];
        newCourse.colorRed =[set valueForKey:@"ColorRed"];
        newCourse.coloralpha =[set valueForKey:@"Coloralpha"];
        newCourse.colorblue =[set valueForKey:@"Colorblue"];
        
        NSData *myData = UIImagePNGRepresentation([set valueForKey:@"Image"]);
        newCourse.image =myData;
        newCourse.name =[set valueForKey:@"Name"];
        newCourse.silence =[set valueForKey:@"Silence"];
        newCourse.vibrate =[set valueForKey:@"Vibrate"];
        
        
        if (![context save:&error]) {
            
        }
        else
            
        {
            
            
        }
    }
    else
    {
        
    }
    return Insert;
}


-(NSArray*)GetProfiles
{
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
}

-(Notifications*)GetnotificationID:(NSString*)ID
{
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Notifications"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    Notifications *newCourse;
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        newCourse = [fetchedObjects objectAtIndex:i];
        
        if ([newCourse.id isEqualToString:ID]) {
            
            
            newCourse =[fetchedObjects objectAtIndex:i];
            
            break;
            
        }
        
        
    }
    
    
    return newCourse;
    
}



-(Profiles*)GetProfilesWithID:(NSString*)ID

{
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    
    
    
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", ID];
    
    NSArray *notificationArray = [fetchedObjects filteredArrayUsingPredicate:predicate];
    
    
    
    Profiles *newCourse;
    for (NSInteger i =0; i < [notificationArray count]; i++) {
        
        newCourse = [notificationArray objectAtIndex:i];
        
        if ([newCourse.id isEqualToString:ID]) {
            
            
            newCourse =[notificationArray objectAtIndex:i];
            
            
        }
        
        
    }
    
    
    return newCourse;
    
}




-(void)removeNotification:(NSString*)set

{
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Notifications"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        currentHistory = [fetchedObjects objectAtIndex:i];
        
        if ([currentHistory.id isEqualToString:set]) {
            
            
            
            
            [context deleteObject:currentHistory];
            
            if (![context save:&error]) {
                
            }
            else
                
            {
                
                
            }
        }
        
        
    }
    
    
    
    
}

-(void)removeHistory:(NSString*)set

{
    
    ////////////////////////////////////////////////NSLog(@"%@", set);
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idmedicine = %@", set];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"History" inManagedObjectContext:[self managedObjectContext]]];
    [fetchRequest setPredicate:predicate];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSInteger i =[fetchedObjects count]-1; i >0; i++) {
        
        
        currentHistory = [fetchedObjects objectAtIndex:i];
        
        ////////////////////////////////////////////////NSLog(@"%@", newCourse);
        
        [context deleteObject:currentHistory];
        
        break;
        
        
    }
    
    
    
    
}

-(void)removeMedicinePerAccount:(NSString*)set

{
    
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    ////NSLog(@"set %@", set);
    
    
    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"id = %@", set];
    NSFetchRequest *fetchRequest3 = [[NSFetchRequest alloc] init];
    [fetchRequest3 setEntity:[NSEntityDescription entityForName:@"Medicine" inManagedObjectContext:[appDelegate managedObjectContext]]];
    [fetchRequest3 setPredicate:predicate3];
    
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest3 error:&error];
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        Medicine *newCourse = [fetchedObjects objectAtIndex:i];
        
        ////NSLog(@"weg %@", newCourse);
        
        
        [context deleteObject:newCourse];
        
        if (![context save:&error]) {
            
        }
        else
            
        {
            
            
        }
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", newCourse.id_medicine];
        
        NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
        
        
        for (NSInteger k =[notificationArray count]-1; k >= 0; k--) {
            
            UILocalNotification *notif = [notificationArray objectAtIndex:k];
            
            [[UIApplication sharedApplication] cancelLocalNotification:notif];
            
            ////NSLog(@"weg %@", notif.fireDate);
            
            
        }
        
        
    }
    
    
}

-(void)removeMedicine:(NSString*)set

{
    
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    ////NSLog(@"set %@", set);
    
    
    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"id_medicine = %@", set];
    NSFetchRequest *fetchRequest3 = [[NSFetchRequest alloc] init];
    [fetchRequest3 setEntity:[NSEntityDescription entityForName:@"Medicine" inManagedObjectContext:[appDelegate managedObjectContext]]];
    [fetchRequest3 setPredicate:predicate3];
    
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest3 error:&error];
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        Medicine *newCourse = [fetchedObjects objectAtIndex:i];
        
        ////NSLog(@"weg %@", newCourse);
        
        
        [context deleteObject:newCourse];
        
        if (![context save:&error]) {
            
        }
        else
            
        {
            
            
        }
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", set];
        
        NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
        
        
        for (NSInteger k =[notificationArray count]-1; k >= 0; k--) {
            
            UILocalNotification *notif = [notificationArray objectAtIndex:k];
            
            [[UIApplication sharedApplication] cancelLocalNotification:notif];
            
            ////NSLog(@"weg %@", notif.fireDate);
            
            
        }
        
        
    }
    
    
}

-(void)removeProfile:(NSString*)set

{
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        Profiles *newCourse = [fetchedObjects objectAtIndex:i];
        
        if ([newCourse.id isEqualToString:set]) {
            
            
            
            [context deleteObject:newCourse];
            
            if (![context save:&error]) {
                
            }
            else
                
            {
                
                
            }
        }
        
        
    }
    
}

-(void)checkPostponed:(NSMutableDictionary*)sender

{
    ////////////////////////////////////////NSLog(@"%@", sender);
    
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
    [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel setDateFormat:[self convertString:@"dd/MM/yyyy hh:mm a"]];
    
    NSDateFormatter *dateFormatLevel2 = [[NSDateFormatter alloc] init];
    [dateFormatLevel2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel2 setDateFormat:[appDelegate convertString:@"hh:mm a"]];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"History" inManagedObjectContext:[self managedObjectContext]]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idmedicine = %@ AND time == %@", [sender valueForKey:@"ID_medicine"], [dateFormatLevel dateFromString:[sender valueForKey:@"Original_Time"]]];
    
    [fetchRequest setPredicate:predicate];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        if ([[sender valueForKey:@"Taken"] isEqualToString:NSLocalizedString(@"Postponed",nil)]) {
            
        }
        else
        {
            currentHistory = [fetchedObjects objectAtIndex:i];
            
            
            appDelegate.postponedtime =[dateFormatLevel2 stringFromDate:currentHistory.timeaction];
            
            ////////////////////////////////////////NSLog(@"newCourse %@", myApp.postponedtime);
            
        }
        
    }
    
}

-(void)insertHistory:(NSMutableDictionary*)set

{
    
    
    
    ////NSLog(@"insertHistory %@", set);
    
    
    
    [self checkPostponed:set];
    
    NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
    [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel setDateFormat:[self convertString:@"dd/MM/yyyy hh:mm a"]];
    
    
    NSDateFormatter *dateLevel = [[NSDateFormatter alloc] init];
    [dateLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateLevel setDateFormat:@"dd/MM/yyyy"];
    
    NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
    [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel24 setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    
    
    NSDate *itemNext;
    
    if ([[set valueForKey:@"Taken"] isEqualToString:NSLocalizedString(@"Postponed",nil)])
    {
        
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatLevel dateFromString:[set valueForKey:@"Original_Time"]]];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatLevel dateFromString:[set valueForKey:@"Original_Time"]]];
        
        NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:00];
        itemNext = [calendar dateFromComponents:dateCompsEarly];
    }
    else
    {
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatLevel dateFromString:[set valueForKey:@"Original_Time"]]];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatLevel dateFromString:[set valueForKey:@"Original_Time"]]];
        
        NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:30];
        itemNext = [calendar dateFromComponents:dateCompsEarly];
    }
    
    
    
    //////////////////////////////////////////////////////////////////NSLog(@"%@", itemNext);
    
    BOOL *Insert = NO;
    
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idmedicine = %@", [set valueForKey:@"ID_Medicine"]];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"History" inManagedObjectContext:[self managedObjectContext]]];
    [fetchRequest setPredicate:predicate];
    
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        currentHistory = [fetchedObjects objectAtIndex:i];
        
        
        if ([currentHistory.taken isEqualToString:NSLocalizedString(@"Postponed",nil)])
        {
            Insert=NO;
            
            if ([[dateFormatLevel stringFromDate:[set valueForKey:@"Original_Time"]] isEqualToString:[dateFormatLevel stringFromDate:currentHistory.originaltime]]) {
                
                
                
                [context deleteObject:currentHistory];
                
                
                
            }
            
            
            if ([[dateFormatLevel stringFromDate:itemNext] isEqualToString:[dateFormatLevel stringFromDate:currentHistory.originaltime]]) {
                
                
                
                
                
                currentHistory.id =[set valueForKey:@"ID"];
                currentHistory.date =[dateLevel dateFromString:[set valueForKey:@"Date"]];
                currentHistory.count =[set valueForKey:@"Count"];
                
                currentHistory.time =[dateFormatLevel dateFromString:[set valueForKey:@"Time"]];
                currentHistory.name =[set valueForKey:@"Name"];
                currentHistory.dose =[set valueForKey:@"Dose"];
                
                if ([[set valueForKey:@"Extra"] isEqualToString:@"Postponed"]) {
                    
                    
                    
                    currentHistory.taken =[NSString stringWithFormat:@"%@#",[set valueForKey:@"Taken"]];
                    
                }
                else
                {
                    currentHistory.taken =[set valueForKey:@"Taken"];
                    
                }
                currentHistory.idmedicine =[set valueForKey:@"ID_medicine"];
                currentHistory.timeaction =[dateFormatLevel dateFromString:[set valueForKey:@"Time_Action"]];
                currentHistory.action =[set valueForKey:@"Action"];
                currentHistory.quantity =[set valueForKey:@"Quantity"];
                currentHistory.type =[set valueForKey:@"Type"];
                currentHistory.timeacaction24 =[dateFormatLevel dateFromString:[set valueForKey:@"Time_Action_24"]];
                currentHistory.originaltime =itemNext;
                currentHistory.actiondate =[dateFormatLevel dateFromString:[set valueForKey:@"Action_Date"]];
                currentHistory.send =@"NO";
                currentHistory.sendid =[set valueForKey:@"sendid"];
                
                AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                if (myApp.postponedtime) {
                    
                    currentHistory.know =myApp.postponedtime;
                }
                else
                {
                    
                    currentHistory.know =[set valueForKey:@"Time_Action"];
                    
                }
                Insert=YES;
                
                ////NSLog(@"%@", currentHistory);
                
                break;
                
            }
            
            
            if (![context save:&error]) {
                
            }
            else
                
            {
                
                
            }
        }
    }
    
    
    if (!Insert) {
        
        currentHistory =(History*) [NSEntityDescription insertNewObjectForEntityForName:@"History" inManagedObjectContext:[self managedObjectContext]];
        
        
        currentHistory.id =[set valueForKey:@"ID"];
        currentHistory.date =[dateLevel dateFromString:[set valueForKey:@"Date"]];
        currentHistory.count =[set valueForKey:@"Count"];
        
        currentHistory.time =[dateFormatLevel dateFromString:[set valueForKey:@"Time"]];
        currentHistory.name =[set valueForKey:@"Name"];
        currentHistory.dose =[set valueForKey:@"Dose"];
        if ([[set valueForKey:@"Extra"] isEqualToString:@"Postponed"]) {
            
            currentHistory.taken =[NSString stringWithFormat:@"%@#",[set valueForKey:@"Taken"]];
        }
        else
        {
            currentHistory.taken =[set valueForKey:@"Taken"];
            
        }
        currentHistory.idmedicine =[set valueForKey:@"ID_medicine"];
        currentHistory.timeaction =[dateFormatLevel dateFromString:[set valueForKey:@"Time_Action"]];
        currentHistory.action =[set valueForKey:@"Action"];
        currentHistory.quantity =[set valueForKey:@"Quantity"];
        currentHistory.type =[set valueForKey:@"Type"];
        currentHistory.timeacaction24 =[dateFormatLevel dateFromString:[set valueForKey:@"Time_Action_24"]];
        currentHistory.originaltime =itemNext;
        currentHistory.actiondate =[dateFormatLevel dateFromString:[set valueForKey:@"Action_Date"]];
        currentHistory.send =@"NO";
        currentHistory.sendid =[set valueForKey:@"sendid"];
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        if (myApp.postponedtime) {
            
            currentHistory.know =myApp.postponedtime;
        }
        else
        {
            
            currentHistory.know =[set valueForKey:@"Time_Action"];
            
        }
        
        
        ////NSLog(@"%@", currentHistory);
        
        if (![context save:&error]) {
            
        }
        else
            
        {
            
            
        }
    }
    else
    {
        
        
    
        
    }
    
    
    //03-08-2017
//   [self InsertEventwith:set];
    
    
    
    
    
    [self reloadTableview:@"insertHistory"];
    
    [self performSelector:@selector(reloadTableview:) withObject:@"insertHistory" afterDelay:0.1];
    
}



- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Void Methods

-(BOOL)isNetworkReachable
{
    BOOL internet_reachable = false;
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];

    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"Please check your network connection");
        internet_reachable = false;
    }
    else
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
        
        NetworkStatus status = [reachability currentReachabilityStatus];
        
        if(status == NotReachable)
        {
            //No internet
            internet_reachable = false;
        }
        else if (status == ReachableViaWiFi)
        {
            //Connected to WiFi
            internet_reachable = true;
        }
        else if (status == ReachableViaWWAN)
        {
            //Connected to 3G
            internet_reachable = true;
        }
    }
    
    return internet_reachable;
}

- (void)sendHitsInBackground
{
//    __block BOOL taskExpired = NO;
//    
//    __block UIBackgroundTaskIdentifier taskId =
//    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
//        taskExpired = YES;
//    }];
//    
//    if (taskId == UIBackgroundTaskInvalid) {
//        return;
//    }
    
    __weak AppDelegate *weakSelf = self;
    
    self.dispatchHandler = ^(GAIDispatchResult result) {
        
        if (result == kGAIDispatchGood) {
            [[GAI sharedInstance] dispatchWithCompletionHandler:weakSelf.dispatchHandler];
        } else {
//            [[UIApplication sharedApplication] endBackgroundTask:taskId];
        }
    };
    
//    [[GAI sharedInstance] dispatchWithCompletionHandler:self.dispatchHandler];
    
    
}

//#pragma mark - Google Analytics
//
//-(void)googleAnalyticsGroupping
//{
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:4]value:@"CFDaily Info"];
//}


#pragma mark - Google Analytics - Action

-(void)addActionForGoogleAnalytics:(NSString *)actionName
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"button_press"
                                                           label:actionName
                                                           value:nil] build]];
}

@end


