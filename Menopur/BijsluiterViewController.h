//
//  InstructiefilmViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BijsluiterViewController : UIViewController

@property (nonatomic, retain) UIButton *Bijsluiter_button;
@property (nonatomic, retain) UIButton *Brochure_button;
@property (nonatomic, retain) UIButton *Website_button;
@property (nonatomic, retain) IBOutlet UILabel *MerkenLabel;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbarDown;
@property (nonatomic, retain) IBOutlet UILabel *setbackground;
-(void) turnbuttons:(NSString*) setter;



@end
