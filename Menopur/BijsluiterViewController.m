//
//  InstructiefilmViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "BijsluiterViewController.h"
#import "ZoomingPDFViewerViewController.h"
#import "PDFrViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "FolderViewController.h"
#import "WebsitesViewController.h"
#import "InstructiefilmViewController.h"
#import "VragenViewController.h"
#import "BijsluiterViewController.h"
#import "AppDelegate.h"

@interface BijsluiterViewController ()

@end

@implementation BijsluiterViewController
@synthesize toolbarDown;
@synthesize setbackground;
@synthesize Bijsluiter_button;
@synthesize Brochure_button;
@synthesize Website_button;
@synthesize MerkenLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSLog(@"InstructiefilmViewController");


    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 87/1.9, 59/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



    UIImage *image = [UIImage imageNamed:@"Logo_bar.png"];
	UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0, 0,136, 34);

    self.navigationItem.titleView = imageView;


    setbackground =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [setbackground setBackgroundColor:[UIColor colorWithRed:0.627 green:0.851 blue:0.961 alpha:1.000]];

    [self.view addSubview:setbackground];

    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = setbackground.layer.bounds;
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                         nil];
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.1f],
                            [NSNumber numberWithFloat:0.2f],
                            [NSNumber numberWithFloat:0.3f],
                            [NSNumber numberWithFloat:0.4f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.6f],
                            nil];



    [setbackground.layer addSublayer:shineLayer];

    toolbarDown = [[UIToolbar alloc] init];
    toolbarDown.frame = CGRectMake(0, self.view.frame.size.height-88, self.view.frame.size.width, 44);
    toolbarDown.tintColor = [UIColor colorWithRed:0.204 green:0.714 blue:0.816 alpha:1.000];




    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];


    UIButton *info = [UIButton buttonWithType:UIButtonTypeCustom];
    [info setTag:44];
    [info setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [info addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    info.layer.cornerRadius = 2;
    [info setTag:22];
    [info setImage:[UIImage imageNamed:@"Info_Logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *info_button = [[UIBarButtonItem alloc] initWithCustomView:info];


    UIButton *Logo_pdf = [UIButton buttonWithType:UIButtonTypeCustom];
    [Logo_pdf setTag:44];
    [Logo_pdf setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [Logo_pdf addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
      Logo_pdf.layer.cornerRadius = 2;
    [Logo_pdf setTag:23];
    [Logo_pdf setImage:[UIImage imageNamed:@"Logo_pdf.png"] forState:UIControlStateNormal];
    UIBarButtonItem *logo_button = [[UIBarButtonItem alloc] initWithCustomView:Logo_pdf];



        //Film_Logo.png


    UIButton *Film_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    [Film_logo setTag:44];
    [Film_logo setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [Film_logo addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    Film_logo.layer.cornerRadius = 2;
    [Film_logo setTag:24];
    [Film_logo setImage:[UIImage imageNamed:@"Film_Logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *film_button = [[UIBarButtonItem alloc] initWithCustomView:Film_logo];


    NSArray *items2 = [NSArray arrayWithObjects:
                       film_button, flexItem,logo_button,flexItem,info_button,
                       nil];
    toolbarDown.items = items2;
    
    
    [self.view addSubview:toolbarDown];

    Bijsluiter_button = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2, 10,300, 89)];
    [Bijsluiter_button addTarget:self action:@selector(Bijsluiter:) forControlEvents:UIControlEventTouchUpInside];
    [Bijsluiter_button setImage:[UIImage imageNamed:@"Bijlster_Button.png"] forState:UIControlStateNormal];
    [self.view addSubview:Bijsluiter_button];
  

    Brochure_button = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2, 110,300, 89)];
    [Brochure_button addTarget:self action:@selector(Brochure:) forControlEvents:UIControlEventTouchUpInside];
    [Brochure_button setImage:[UIImage imageNamed:@"Brochure_Button.png"] forState:UIControlStateNormal];
    [self.view addSubview:Brochure_button];
  
    Website_button = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2, 210,300, 89)];
    [Website_button addTarget:self action:@selector(Website:) forControlEvents:UIControlEventTouchUpInside];
    [Website_button setImage:[UIImage imageNamed:@"Website_Button.png"] forState:UIControlStateNormal];
    [self.view addSubview:Website_button];


    MerkenLabel= [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-290)/2, 300, 270, 100)];
    MerkenLabel.backgroundColor = [UIColor clearColor];
    [MerkenLabel setText: @"Bij medische vragen, bijwerkingen en/of andere medische zaken wordt u geadviseerd uw arts te contacteren."];
    MerkenLabel.font = [UIFont systemFontOfSize:16];
    [MerkenLabel setNumberOfLines:3];
    [MerkenLabel setTextAlignment:NSTextAlignmentLeft];
    MerkenLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
    [MerkenLabel setBackgroundColor:[UIColor clearColor]];
    [self.view   addSubview:MerkenLabel];

    [MerkenLabel setAlpha:1];

    
	// Do any additional setup after loading the view.
}
-(void)PresentView:(UIButton*)sender

{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    for (int i =1; i < [[[self navigationController] viewControllers] count]; i++) {

        UIViewController *controller = [[[self navigationController] viewControllers] objectAtIndex:i];
        [[self navigationController] popToViewController:controller animated:NO];


    }




    int64_t delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

        if (sender.tag ==22) {



            VragenViewController *controller = [[VragenViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];

            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];






        }
        if (sender.tag ==23) {


            BijsluiterViewController *controller = [[BijsluiterViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];

            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];

        }
        if (sender.tag ==24) {



            InstructiefilmViewController *controller = [[InstructiefilmViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];
            
            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];
            
            
        }
        
        
    });
    
    
}


-(void)viewWillAppear:(BOOL)animated
{


    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void) Brochure:(UIButton*)sender
{



    ZoomingPDFViewerViewController *controller = [[ZoomingPDFViewerViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
  
}


-(void) Bijsluiter:(UIButton*)sender
{


    PDFrViewController *controller = [[PDFrViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];


}

-(void) turnbuttons:(NSString*) setter

{

}


-(void) Website:(UIButton*)sender
{

    WebsitesViewController *controller = [[WebsitesViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];

}

-(void) backAction
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
