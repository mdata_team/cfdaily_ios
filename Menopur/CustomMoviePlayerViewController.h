//
//  CustomMoviePlayerViewController.h
//
//  Copyright iOSDeveloperTips.com All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface CustomMoviePlayerViewController : UIViewController 
{
    MPMoviePlayerController *moviePlayerViewController;
    NSURL 									*movieURL;
     NSString *RechtsLinks;
    NSTimer *levelTimer;
    NSInteger Timestart;
}

- (id)initWithPath:(NSString *)moviePath initWithTime:(NSInteger) timeNow;
- (void)readyPlayer;

@end
