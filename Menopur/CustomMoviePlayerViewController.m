//
//  CustomMoviePlayerViewController.m
//
//  Copyright iOSDeveloperTips.com All rights reserved.
//

#import "CustomMoviePlayerViewController.h"

#pragma mark -
#pragma mark Compiler Directives & Static Variables

@implementation CustomMoviePlayerViewController

/*---------------------------------------------------------------------------
 * 
 *--------------------------------------------------------------------------*/
- (id)initWithPath:(NSString *)moviePath initWithTime:(NSInteger)timeNow;
{
	// Initialize and create movie URL
    if ((self = [super init]))
    {
        movieURL = [NSURL fileURLWithPath:moviePath];    
        [movieURL retain];
    }
	return self;
}

/*---------------------------------------------------------------------------
 * For 3.2 and 4.x devices
 * For 3.1.x devices see moviePreloadDidFinish:
 *--------------------------------------------------------------------------*/
- (void) moviePlayerLoadStateChanged:(NSNotification*)notification 
{
	// Unless state is unknown, start playback
	if ([moviePlayerViewController loadState] != MPMovieLoadStateUnknown)
    {
        // Remove observer
        [[NSNotificationCenter 	defaultCenter] 
         removeObserver:self
         name:MPMoviePlayerLoadStateDidChangeNotification 
         object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector:@selector(receivedRotate:)
                                                     name:UIDeviceOrientationDidChangeNotification
                                                   object: nil];
        
        // When tapping movie, status bar will appear, it shows up
        // in portrait mode by default. Set orientation to landscape
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
		// Rotate the view for landscape playback
        [[self view] setBounds:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
		[[self view] setCenter:CGPointMake(self.view.frame.size.height/2, self.view.frame.size.width/2)];
		[[self view] setTransform:CGAffineTransformMakeRotation(M_PI / 2)]; 
        
        RechtsLinks = @"Links";
        
		// Set frame of movieplayer
		[[moviePlayerViewController view] setFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
        
        // Add movie player as subview
        [[self view] addSubview:[moviePlayerViewController view]];   
        
		// Play the movie
        
        //levelTimer = [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self selector: @selector(SetCurrenttime) userInfo:nil repeats: YES];
    
        
		[moviePlayerViewController play];
        
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
	}
}

-(void) SetCurrenttime

{
    NSLog(@"%@", movieURL);
    NSLog(@"Time %.2f", [moviePlayerViewController currentPlaybackTime]);
    
    
    
}

/*---------------------------------------------------------------------------
 * For 3.1.x devices
 * For 3.2 and 4.x see moviePlayerLoadStateChanged: 
 *--------------------------------------------------------------------------*/
- (void) moviePreloadDidFinish:(NSNotification*)notification 
{
	// Remove observer
	[[NSNotificationCenter 	defaultCenter] 
     removeObserver:self
     name:MPMoviePlayerLoadStateDidChangeNotification
     object:nil];
    
	// Play the movie
 	[moviePlayerViewController play];
   
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)receivedRotate:(NSNotification*)notif {
    UIDeviceOrientation interfaceOrientation = [[UIDevice currentDevice] orientation];
    switch (interfaceOrientation) {
            
        case UIInterfaceOrientationPortrait: {
            
      
           
           NSLog(@"changeOrientationUp");
              [self performSelector:@selector(setContentFrames) withObject:nil afterDelay:0.3f];
             [[UIApplication sharedApplication] setStatusBarHidden:YES];
            break;
        }
        case UIInterfaceOrientationPortraitUpsideDown: {
        
           
            NSLog(@"changeOrientationDown");
              [self performSelector:@selector(setContentFrames) withObject:nil afterDelay:0.3f];
             [[UIApplication sharedApplication] setStatusBarHidden:YES];
            break;
        }
        case UIInterfaceOrientationLandscapeLeft: {
 
            
            NSLog(@"changeOrientationLeft");
              [self performSelector:@selector(setContentFrames) withObject:nil afterDelay:0.3f];
             [[UIApplication sharedApplication] setStatusBarHidden:YES];
            break;
        }
        case UIInterfaceOrientationLandscapeRight: {
           
            
            NSLog(@"changeOrientationRight");
             [self performSelector:@selector(setContentFrames) withObject:nil afterDelay:0.3f];
             [[UIApplication sharedApplication] setStatusBarHidden:YES];
            
            break;
        }
        default:
            break;
    } 
    
    
    
}   

-(void) setContentFrames
{
    
    UIDeviceOrientation interfaceOrientation = [[UIDevice currentDevice] orientation];
    
    if (interfaceOrientation == UIDeviceOrientationLandscapeRight)
        {
         
              RechtsLinks = @"Rechts";
              NSLog(@"RechtsLinks %@", RechtsLinks);
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            //TotalTwitter.alpha = 0.0;
           
            [[self view] setBounds:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
            [[self view] setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
            [[self view] setTransform:CGAffineTransformMakeRotation((M_PI / 2)*3)]; 
                    // Set frame of movieplayer
            [[moviePlayerViewController view] setFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
            [UIView commitAnimations];  
        }
        else if (interfaceOrientation == UIDeviceOrientationLandscapeLeft)
        {
           
            RechtsLinks = @"Links";
              NSLog(@"RechtsLinks %@", RechtsLinks);
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [[self view] setBounds:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
            [[self view] setCenter:CGPointMake(self.view.frame.size.height/2, self.view.frame.size.width/2)];
            [[self view] setTransform:CGAffineTransformMakeRotation(M_PI / 2)]; 
            [[moviePlayerViewController view] setFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
                [UIView commitAnimations]; 
        }
	
        else if (interfaceOrientation == UIDeviceOrientationPortrait)
        {
             NSLog(@"UIDeviceOrientationPortrait %@", RechtsLinks);
           
            if ([RechtsLinks isEqualToString:@"Links"]) {
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:0.5];
                [[self view] setBounds:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
                 [[self view] setCenter:CGPointMake(self.view.frame.size.height/2, self.view.frame.size.width/2)];
                 [[self view] setTransform:CGAffineTransformMakeRotation((M_PI) *4)]; 
                
                // Set frame of movieplayer
                [[moviePlayerViewController view] setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                  [UIView commitAnimations]; 
            }
            else if ([RechtsLinks isEqualToString:@"Rechts"]) {
              
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:0.5];
                [[self view] setBounds:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
                [[self view] setCenter:CGPointMake(self.view.frame.size.height/2, self.view.frame.size.width/2)];
                [[self view] setTransform:CGAffineTransformMakeRotation((M_PI) *2)]; 
                
                // Set frame of movieplayer
                [[moviePlayerViewController view] setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)]; 
                 [UIView commitAnimations];     
                     
                 }
            
            
            
            }
         else if (interfaceOrientation == UIDeviceOrientationPortraitUpsideDown)
        {
            
            NSLog(@"UIDeviceOrientationPortrait %@", RechtsLinks);
            
            if ([RechtsLinks isEqualToString:@"Links"]) {
                
                
            }
            else if ([RechtsLinks isEqualToString:@"Rechts"]) {
                
                   
                
            }

                 
                 
                 
                 
                 }
         
    
}

-(void) setContentFrames1
{

 UIDeviceOrientation interfaceOrientation = [[UIDevice currentDevice] orientation];
if (interfaceOrientation == UIDeviceOrientationLandscapeLeft) {
    
    
    [[self view] setBounds:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
    [[self view] setCenter:CGPointMake(384, 512)];
    [[self view] setTransform:CGAffineTransformMakeRotation(M_PI / 2)]; 
    
    // Set frame of movieplayer
    [[moviePlayerViewController view] setFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
    
}
    else if (interfaceOrientation == UIDeviceOrientationLandscapeRight) {
        
        
        [[self view] setBounds:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
        [[self view] setCenter:CGPointMake(384, 512)];
        [[self view] setTransform:CGAffineTransformMakeRotation(-M_PI / 2)]; 
        
        // Set frame of movieplayer
        [[moviePlayerViewController view] setFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
    }
    else if (interfaceOrientation == UIDeviceOrientationPortrait) {
        
        
     
    }
    else if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
       
    
       
    }
}
/*---------------------------------------------------------------------------
 * 
 *--------------------------------------------------------------------------*/
- (void) moviePlayBackDidFinish:(NSNotification*)notification 
{    
    
 	// Remove observer
    [[NSNotificationCenter 	defaultCenter] 
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification 
     object:nil];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self dismissModalViewControllerAnimated:YES];
         [levelTimer invalidate];
    });
    
    
}

/*---------------------------------------------------------------------------
 *
 *--------------------------------------------------------------------------*/
- (void) readyPlayer
{
    
 	moviePlayerViewController =  [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    
    if ([moviePlayerViewController respondsToSelector:@selector(loadState)]) 
    {
        // Set movie player layout
        [moviePlayerViewController setControlStyle:MPMovieControlStyleFullscreen];
        [moviePlayerViewController setFullscreen:YES];
        
		// May help to reduce latency
		[moviePlayerViewController prepareToPlay];
        
		// Register that the load state changed (movie is ready)
		[[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(moviePlayerLoadStateChanged:) 
                                                     name:MPMoviePlayerLoadStateDidChangeNotification 
                                                   object:nil];
        
	}  
    else
    {
        // Register to receive a notification when the movie is in memory and ready to play.
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(moviePreloadDidFinish:) 
                                                     name:MPMoviePlayerLoadStateDidChangeNotification 
                                                   object:nil];
        
        
        
    }
    
    // Register to receive a notification when the movie has finished playing. 
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(moviePlayBackDidFinish:) 
                                                 name:MPMoviePlayerPlaybackDidFinishNotification 
                                               object:nil];
    
}

/*---------------------------------------------------------------------------
 * 
 *--------------------------------------------------------------------------*/
- (void) loadView
{
    [self setView:[[[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]] autorelease]];
	[[self view] setBackgroundColor:[UIColor blackColor]];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}
/*---------------------------------------------------------------------------
 *  
 *--------------------------------------------------------------------------*/
- (void)dealloc 
{
	[moviePlayerViewController release];
    [movieURL release];
	[super dealloc];
}

@end
