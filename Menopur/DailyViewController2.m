    //
    //  ViewController.m
    //  Menopur
    //
    //  Created by Jeffrey Snijder on 20-01-13.
    //  Copyright (c) 2013 Menopur. All rights reserved.
    //

#import "DailyViewController2.h"
#import <QuartzCore/QuartzCore.h>

#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "TimesofdayCell.h"
#import "FrequencyViewController.h"
@interface DailyViewController2 ()

@end

@implementation DailyViewController2
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize table;
@synthesize TitleTextchose;
@synthesize theTime;
@synthesize theDay;
@synthesize theDate;


-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(TekstIDLabel*)set

{

    
      ////////////////////NSLog(@"%@ %@", set.text, self);
    parantLabel =set;
        ////////////////////////////////////////NSLog(@"TekstIDLabel %@", set);
}



- (void)viewDidLoad {


    TimeChoses =[[NSMutableArray alloc]init];

  

    pickerTaal = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, 320, 300)];
    pickerTaal.datePickerMode = UIDatePickerModeTime;
    [pickerTaal setTag:679];
    [pickerTaal setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [pickerTaal addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:pickerTaal];
    
    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:@"Done" style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Cancel:)];



    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];


    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(40, 0.0f, 240, 20)];
    [title setTextColor:[UIColor whiteColor]];
    [title setText:@"Daily"];
    [title setFont:[UIFont boldSystemFontOfSize:18]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];


    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:title];




    UIBarButtonItem *Save = [[UIBarButtonItem alloc]
                             initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self
                             action:@selector(Save:)];





    UIToolbar *toolbarUp = [[UIToolbar alloc] init];
    toolbarUp.frame = CGRectMake(0, -2, 320, 54);
    [toolbarUp setTintColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [self.view addSubview:toolbarUp];


    NSArray *itemsUp = [NSArray arrayWithObjects:Cancel,flexItem,random,flexItem,Save,nil];
    toolbarUp.items = itemsUp;



    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 64, self.view.frame.size.width-20, 44)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];




    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 68, self.view.frame.size.width-40, 35)];
    [TitleText setBackgroundColor:[UIColor whiteColor]];
    [TitleText setDelegate:self];
    [TitleText setText:@"Times per Day (EX.2)"];
    [TitleText setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [TitleText setTag:123+1];
    [TitleText setEditable:NO];
    self.TitleText.selectedRange = NSMakeRange(0, 0);
    [TitleText setFont:[UIFont boldSystemFontOfSize:15]];
    [self.view addSubview:TitleText];
    
    
    



    UILabel *Tekst = [[UILabel alloc] initWithFrame:CGRectMake(12, 120, 240, 20)];
    [Tekst setTextColor:[UIColor clearColor]];
    [Tekst setText:@"Set Times of Day"];
    [Tekst setFont:[UIFont boldSystemFontOfSize:16]];
    [Tekst setTextAlignment:NSTextAlignmentLeft];
    [Tekst setTag:120];
    [Tekst setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [Tekst setBackgroundColor:[UIColor clearColor]];
    [Tekst setNumberOfLines:3];
    [self.view addSubview:Tekst];





    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000];



    UIView *CombiChose = [[UIView alloc] initWithFrame:CGRectMake(10,  152, 300, 44)];
    [CombiChose setBackgroundColor:[UIColor whiteColor]];
    [CombiChose.layer setCornerRadius:10];
    [CombiChose.layer setBorderWidth:1];
    [CombiChose.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [CombiChose.layer setMasksToBounds:YES];
    [self.view addSubview:CombiChose];




    TitleTextchose = [[UITextView alloc] initWithFrame:CGRectMake(15,  154, 170, 35)];
    [TitleTextchose setBackgroundColor:[UIColor whiteColor]];
        //[TitleText setDelegate:self];
    [TitleTextchose setText:@""];
    [TitleTextchose setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [TitleTextchose setTag:123+1];
    TitleTextchose.selectedRange = NSMakeRange(0, 0);
    [TitleTextchose setInputView:pickerTaal];
    [TitleTextchose setFont:[UIFont boldSystemFontOfSize:15]];
    [self.view addSubview:TitleTextchose];

    
    TitleAm = [[UITextView alloc] initWithFrame:CGRectMake( 320-80, 154, 60, 35)];
    [TitleAm setBackgroundColor:[UIColor whiteColor]];
    [TitleAm setDelegate:self];
    [TitleAm setText:@""];
    [TitleAm setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [TitleAm setTag:123+1];
    [TitleAm setFont:[UIFont boldSystemFontOfSize:15]];
    [self.view addSubview:TitleAm];



    table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 200, 310, 200)];
    table.separatorColor = [UIColor clearColor];
    table.backgroundColor = [UIColor clearColor];
    table.rowHeight =260;
    table.delegate = self;
    table.dataSource = self;
    [table setEditing:YES animated:YES];
    [table setEditing:YES];
    [self.view addSubview:table];


    NSDate *now = [[NSDate alloc] init];

     NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
 

    [format setDateFormat:@"dd/MM/yyyy"];
    theDate = [format stringFromDate:now];

     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
     
    [dateFormatter setDateFormat:@"EEEE"];
    
    theDay = [dateFormatter stringFromDate:now];


    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
     
    [timeFormat setDateFormat:@"hh:mm"];
    theTime = [timeFormat stringFromDate:now];
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter2 setDateFormat:@"a"];
    timeframe = [dateFormatter2 stringFromDate:now];
    


}


-(void)valueChanged:(UIDatePicker*)sender
{
   
    ////NSLog(@"%@", sender.date);
    
     NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
   

    [format setDateFormat:@"dd/MM/yyyy"];
    theDate = [format stringFromDate:sender.date];
    
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
   
    
    [dateFormatter setDateFormat:@"EEEE"];
    theDay = [dateFormatter stringFromDate:sender.date];
    
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
  
    [timeFormat setDateFormat:@"hh:mm"];
    theTime = [timeFormat stringFromDate:sender.date];
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter2 setDateFormat:@"a"];
    timeframe = [dateFormatter2 stringFromDate:sender.date];
    
    
        ////NSLog(@"%@", timeframe);
 
    
    
    [TitleTextchose setText:theTime];
    
    [TitleAm setText:timeframe];
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [TimeChoses removeAllObjects];
    [parantIt.Frequencytimer setObject:@"Daily" forKey:@"Frequency"];

    [TitleTextchose setEditable:YES];
    [TitleTextchose becomeFirstResponder];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSLog(@"%@", parantIt.Frequencytimer);
    
    if ([[parantIt.Frequencytimer valueForKey:@"Frequency"] isEqualToString:@"Daily"]) {
       
        
        for (int i=0 ; i<[[parantIt.Frequencytimer valueForKey:@"Moment"] count]; i++) {
       
        NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
        [set setObject:@"Daily" forKey:@"choice"];
        [set setObject:[appDelegate newUUID] forKey:@"ID_Timer"];
        [set setObject:[[parantIt.Frequencytimer valueForKey:@"Moment"] objectAtIndex:i] forKey:@"Moment"];
        [set setObject:TitleTextchose.text forKey:@"Time"];
        [set setObject:theDay forKey:@"CurrentDay"];
        [set setObject:[[parantIt.Frequencytimer valueForKey:@"CurrentDate"] objectAtIndex:i] forKey:@"CurrentDate"];
        [set setObject:@"24" forKey:@"intervalOurs"];
        [set setObject:@"1" forKey:@"intervalDays"];
        [set setObject:[[parantIt.Frequencytimer valueForKey:@"CurrentTime"] objectAtIndex:i]  forKey:@"CurrentTime"];
        [set setObject:@"" forKey:@"Diverence"];
        [set setObject:@"0" forKey:@"Day"];
        
        
        [TimeChoses addObject:set];
            
            
        }
    
        
        [table reloadData];
    }

}

-(void)setTime:(UIButton*)sender
{



}





- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
        {


        [TimeChoses removeObjectAtIndex:indexPath.row];
          
            
         [TitleText setText:[NSString stringWithFormat:@"%i Times per Day (EX.2)",[TimeChoses count]]];


        [table reloadData];




        } else if (editingStyle == UITableViewCellEditingStyleInsert)
            {



            }


}



-(void) addItem:(NSString*) sender
{


}

- (BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [TimeChoses count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(TimesofdayCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {





    [cell FillAllItems:[TimeChoses objectAtIndex:indexPath.row] index:indexPath.row];




}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{



    return 50;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{


}


- (TimesofdayCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {




    NSString *identifier = @"CellIdentifier";
    TimesofdayCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

	if (cell == nil) {

        cell = [[TimesofdayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    }

    else

        {

        }


    return cell;


}

-(void)Cancel:(UIBarButtonItem*) sender

{

    [parantIt.Frequencytimer setObject:@"Daily" forKey:@"Frequency"];
    [parantIt.Frequencytimer setObject:[TimeChoses valueForKey:@"CurrentDate"] forKey:@"CurrentDate"];
    [parantIt.Frequencytimer setObject:[TimeChoses valueForKey:@"CurrentTime"]  forKey:@"CurrentTime"];
    [parantIt.Frequencytimer setObject:[TimeChoses valueForKey:@"Moment"] forKey:@"Moment"];


  
    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)Save:(UIBarButtonItem*) sender

{

    if ([TitleTextchose.text length] ==0) {
        
    }

    else
    {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

        
     

            NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
             [set setObject:@"Daily" forKey:@"choice"];
            [set setObject:[appDelegate newUUID] forKey:@"ID_Timer"];
            [set setObject:timeframe forKey:@"Moment"];
            [set setObject:TitleTextchose.text forKey:@"Time"];
            [set setObject:theDay forKey:@"CurrentDay"];
             [set setObject:theDate forKey:@"CurrentDate"];
            [set setObject:@"24" forKey:@"intervalOurs"];
            [set setObject:@"1" forKey:@"intervalDays"];
            [set setObject:theTime forKey:@"CurrentTime"];
             [set setObject:@"" forKey:@"Diverence"];
             [set setObject:@"0" forKey:@"Day"];
         
          
            [TimeChoses addObject:set];
        
  
        



            [TitleTextchose setText:@""];
    
    //////////////////////////////////////NSLog(@"%@", TimeChoses);


            [TitleText setText:[NSString stringWithFormat:@"%i Times per Day (EX.2)",[TimeChoses count]]];


 

    
      appDelegate.Notevariable =[NSString stringWithFormat:@"%i Times per Day", [TimeChoses count]];
    
    
      [table reloadData];

        
    }

        //[TitleText resignFirstResponder];
        //[self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)setChoice:(NSString*)gothere

{
    
}

-(void)change:(UIButton*)sender

{

    if (sender.tag ==141 ||sender.tag ==181) {

    }
    if (sender.tag ==142 ||sender.tag ==182) {

    }
    if (sender.tag ==143 ||sender.tag ==183) {

    }
    if (sender.tag ==144 ||sender.tag ==184) {

    }


}

-(void)Action:(UIButton*)sender

{


}

-(void)changeChose:(NSString*)sender

{


        ////////////////////////////////////////NSLog(@"%@", sender);
    chosen = sender;


}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

       TitleText.selectedRange = NSMakeRange(0, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [a1 setTag:44];
        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
        [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;


        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            ////////////////////////////////////////////////////NSLog(@"%@", text);
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{


}

-(void)getParant:(FrequencyViewController*)parant

{

    parantIt =parant;


    
    
    
    
}





- (IBAction)SelectChoice:(UIButton *)sender {
    
    
    
    
    
}



@end
