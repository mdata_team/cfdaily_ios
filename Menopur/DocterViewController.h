//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
@class CodePopView, NewMedecineController, TekstIDLabel;
@interface DocterViewController : UIViewController<UITextViewDelegate, ABPeoplePickerNavigationControllerDelegate>
{
    UITextView *TitleText;
    NSString *Compaire;
    UIView *Combi;
    UIView *CombiOther;
    NSString *chosen;
    NSString *chosenID;
}

@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;
@property (weak, nonatomic) IBOutlet UILabel *firstName;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;

@property (nonatomic, retain) IBOutlet  UIImageView *headShot;

@property (nonatomic, retain) IBOutlet UITextView *Phone;
@property (nonatomic, retain) IBOutlet UITextView *eMail;

-(void)setChoice:(NSString*)gothere;
-(void)change:(UIButton*)sender;
-(void)setTextview:(NSString*)text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
@end

