//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//
//sn//contact
#import "DocterViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import <UIKit/UIKit.h>
#import "ActionArrow.h"
#import <AddressBookUI/AddressBookUI.h>

@interface DocterViewController ()

@end

@implementation DocterViewController
@synthesize parantIt;

@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize headShot;
@synthesize eMail;
@synthesize Phone;


-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    parantLabel =(TekstIDLabel*)set;
    
    
    

    
    
        [TitleText setText:set.text];
    
}



- (void)viewDidLoad {

     [self.view.layer setMasksToBounds:YES];
    
       //     [self.view.layer setMasksToBounds:YES];
    
    self.title = NSLocalizedString(@"Prescription Notes",nil);
   
    // this will appear as the title in the navigation bar
 
    
NSArray *names1 =[NSArray arrayWithObjects:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Choose from Contacts",nil)], nil];
    
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:20.0];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
              [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.title;
            [label sizeToFit];
               [label setNumberOfLines:2];
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 40*[names1 count])];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
//    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];
    
    
   for (NSInteger i=0 ; i<[names1 count]; i++) {
        
        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, (40*i), self.view.frame.size.width-80, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
       [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:15]];
        [MedName setText:[names1 objectAtIndex:i]];
        [MedName setTag:180+i];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Combi addSubview:MedName];
        
        
        
        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, (40*i), self.view.frame.size.width-20, 40)];
        [NameButton setTitleit:[names1 objectAtIndex:i]];
        [NameButton setTag:140+i];
        [NameButton.layer setBorderWidth:1];
//        [NameButton.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
       [NameButton.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [NameButton setBackgroundColor:[UIColor clearColor]];
        [Combi addSubview:NameButton];
        
       
       
    }
    
    
   NSArray *names2 =[NSArray arrayWithObjects:@"Doctor",@"Phone_Doctor",@"eMail_Doctor", nil];
    
    UIView *CombiChoice1 = [[UIView alloc] initWithFrame:CGRectMake(10, 60, self.view.frame.size.width-20, 40*[names2 count])];
    [CombiChoice1 setBackgroundColor:[UIColor whiteColor]];
    [CombiChoice1.layer setCornerRadius:10];
    [CombiChoice1.layer setBorderWidth:1.5];
//    [CombiChoice1.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [CombiChoice1.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [CombiChoice1.layer setMasksToBounds:YES];
    [self.view addSubview:CombiChoice1];
    
    
   for (NSInteger i=0 ; i<[names2 count]; i++) {
        
        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, (40*i), self.view.frame.size.width-80, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
       [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:15]];
        [MedName setTitle:[names2 objectAtIndex:i]];
        [MedName setTag:180+i];
//        [MedName setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]];
        [MedName setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [CombiChoice1 addSubview:MedName];
        
        
        
        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, (40*i), self.view.frame.size.width-20, 40)];
        [NameButton setTitleit:[names2 objectAtIndex:i]];
        [NameButton setTag:140+i];
        [NameButton.layer setBorderWidth:1];
//        [NameButton.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
       [NameButton.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [NameButton setBackgroundColor:[UIColor clearColor]];
        [CombiChoice1 addSubview:NameButton];
        
        
       if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
           
           
           //NSLog(@"left");
           CombiChoice1.transform = CGAffineTransformMakeScale(-1, 1);
           
           
           for (UILabel *label in CombiChoice1.subviews) {
               
               
               if ([label isKindOfClass:[UILabel class]]) {
                   
                   label.transform = CGAffineTransformMakeScale(-1, 1);
                   [label setTextAlignment:NSTextAlignmentNatural];
                   
               }
               
           }
           
           
           for (UIButton *label in CombiChoice1.subviews) {
               
               if ([label isKindOfClass:[UIButton class]]) {
                   
                   label.transform = CGAffineTransformMakeScale(-1, 1);
                   [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
               }
           }
           
           
           for (TekstIDLabel *label in CombiChoice1.subviews) {
               
               
               if ([label isKindOfClass:[TekstIDLabel class]]) {
                   
                   label.transform = CGAffineTransformMakeScale(-1, 1);
                   [label setTextAlignment:NSTextAlignmentNatural];
                   
               }
               
           }
           
           
           //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
           
           
       } else {
           
           //NSLog(@"right");
           //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
       }
    }
}


-(void)Change:(UIButton*)sender
{
    
}
-(void)addContact
{
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);

    ABRecordRef person = ABPersonCreate();

    NSString *set = TitleText.text;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFTypeRef)(set) , nil);
    ABRecordSetValue(person, kABPersonLastNameProperty, @"", nil);
    ABAddressBookAddRecord(addressBook, person, nil);
    ABAddressBookSave(addressBook, nil);
    
    CFRelease(person);
}

- (void)showPicker
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];

    
    ABPeoplePickerNavigationController *picker =
    [[ABPeoplePickerNavigationController alloc] init];
   picker.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];

    picker.peoplePickerDelegate = self;

   [self presentViewController:picker animated:YES completion:NULL];
}




-(NSString*)getProfileID
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"ID_Profile"];
}


-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:1];

}

- (void)peoplePickerNavigationControllerDidCancel:
(ABPeoplePickerNavigationController *)peoplePicker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    [self displayPerson:person];
    
    CFDataRef imageData = ABPersonCopyImageData(person);
    UIImage *imgContact = [UIImage imageWithData:CFBridgingRelease(imageData)];
   
    if (imgContact)
    {
    
    NSData *data = [NSData dataWithData:UIImagePNGRepresentation(imgContact)];
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir,@"Doctor.png"];
    
    [data writeToFile:jpegFilePath atomically:YES];
        
        //[headShot setImage:imgContact];
        
    }
    else
    {
        
         // [headShot setImage:[UIImage imageNamed:@"headshot.png"]];
    }
    
    
    
       [self dismissViewControllerAnimated:YES completion:NULL];

    return NO;
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
                         didSelectPerson:(ABRecordRef)person
                                property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier
{
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person
{
    ////NSLog(@"Went here 1 ...");
    
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person];
}


- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person
                                property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier
{
    return NO;
}

- (void)displayPerson:(ABRecordRef)person
{
  NSString* name = (__bridge_transfer NSString*)ABRecordCopyValue(person,
                                                                    kABPersonFirstNameProperty);

    NSString* lastname = (__bridge_transfer NSString*)ABRecordCopyValue(person,
                                                                    kABPersonLastNameProperty);
    
    ABMultiValueRef emailMultiValue = ABRecordCopyValue(person, kABPersonEmailProperty);
    NSArray *Addressesarray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailMultiValue);
           if (emailMultiValue)
    CFRelease(emailMultiValue);
    
    ABMultiValueRef phoneMultiValue = ABRecordCopyValue(person, kABPersonPhoneProperty);
    NSArray *PhoneArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(phoneMultiValue);
           if (phoneMultiValue)
    CFRelease(phoneMultiValue);
   
    for (UIView *view in self.view.subviews) {
        
        for (TekstIDLabel *label in view.subviews) {
            
            if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                [label setText:@" "];
                
            }
            
        }
        
    }

    if ([name length]>1 && [lastname length]>1) {
        
        for (UIView *view in self.view.subviews) {
            
            for (TekstIDLabel *label in view.subviews) {
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    if ([label.title isEqualToString:@"Doctor"]) {
                        
                        [label setText:[NSString stringWithFormat:@"%@ %@", name, lastname]];
                        
                    }
                 
                    
                }
                
            }
            
        }

    }
    
    if ([name length]>1 && [lastname length]==0) {
        
        for (UIView *view in self.view.subviews) {
            
            for (TekstIDLabel *label in view.subviews) {
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    if ([label.title isEqualToString:@"Doctor"]) {
                        
                        [label setText:[NSString stringWithFormat:@"%@", name]];
                        
                    }
                    
                    
                }
                
            }
            
        }
    }
    
       
    
    if (Addressesarray>0) {
     
        for (UIView *view in self.view.subviews) {
            
            
            
            for (TekstIDLabel *label in view.subviews) {
                
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    if ([label.title isEqualToString:@"eMail_Doctor"]) {
                        
                        
                        [label setText:[Addressesarray objectAtIndex:0]];
                        
                    }
                    
                    
                }
                
            }
            
        }
        
    }
   
       if (PhoneArray>0) {
    
           for (UIView *view in self.view.subviews) {
               
               
               
               for (TekstIDLabel *label in view.subviews) {
                   
                   
                   if ([label isKindOfClass:[TekstIDLabel class]]) {
                       
                       if ([label.title isEqualToString:@"Phone_Doctor"]) {
                           
                           
                           [label setText:[PhoneArray objectAtIndex:0]];
                           
                       }
                       
                       
                   }
                   
               }
               
           }
           
          
       }

    
    
    
 
    
}

-(void)change:(UIButton*)sender

{
    
    
    
    NSArray *names1 =[NSArray arrayWithObjects:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Choose from Contacts",nil)],[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Add New Contacts",nil)], nil];
    

    
      for (NSInteger i=0 ; i<[names1 count]; i++) {

           UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
           [setimage setAlpha:0];
           

       }


    UIImageView *setimage = (UIImageView *)[self.view viewWithTag:sender.tag+20];
    [setimage setAlpha:1];

    
  

    chosen = sender.titleLabel.text;

}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)Action:(ActionArrow*)sender

{
    
   

    [sender setBackgroundColor:[UIColor grayColor]];
double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
     [sender setBackgroundColor:[UIColor clearColor]];
    
});
    
    
    if ([sender.titleit isEqualToString:@"Phone_Doctor"]) {
        
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            
            for (UIView *view in self.view.subviews) {
                
                
                
                for (TekstIDLabel *label in view.subviews) {
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        if ([label.title isEqualToString:@"Phone_Doctor"]) {
                            
                         
                          
                            label.text =[label.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
                            label.text =[label.text stringByReplacingOccurrencesOfString:@")" withString:@""];
                            label.text =[label.text stringByReplacingOccurrencesOfString:@" s" withString:@""];
                            label.text =[label.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
                            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",label.text]];
                            [[UIApplication sharedApplication] openURL:url];
                            
                        }
                        
                        
                    }
                    
                }
                
            }
           
       
        } else {
            /* //27-06-2017 change
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Your device doesn't support this feature.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
            [Notpermitted show];
            */
            
            UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Your device doesn't support this feature.",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                
            });
            
        }
    }
    
    else if ([sender.titleit isEqualToString:@"Doctor"]) {
        
    }
    else if ([sender.titleit isEqualToString:@"eMail_Doctor"]) {
     
        
        for (UIView *view in self.view.subviews) {
            
            
            
            for (TekstIDLabel *label in view.subviews) {
                
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    if ([label.title isEqualToString:@"eMail_Doctor"]) {
                        
                      
                        
                        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        [appDelegate SendtheMailtoAdress:label.text];
                        
                    }
                    
                    
                }
                
            }
            
        }
    }
    
    if (sender.tag ==140 ||sender.tag ==180) {

        [self showPicker];

    }
}

-(void)changeChose:(NSString*)sender
{
    chosen = sender;
}

-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
      if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{

}



-(void)getParant:(UIViewController*)parant

{

    parantIt = (NewMedecineController*)parant;
    
    
    for (UIView *view in self.view.subviews) {
        
        
        
        for (TekstIDLabel *label in view.subviews) {
            
            
            if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                if ([label.title isEqualToString:@"Doctor"]) {
                    
                    
                  [label setText:[parantIt.ChosenMedicen  valueForKey:@"Doctor"]];
                    
                }
                if ([label.title isEqualToString:@"Phone_Doctor"]) {
                    
                    
                    [label setText:[parantIt.ChosenMedicen  valueForKey:@"Phone_Doctor"]];
                    
                }
                if ([label.title isEqualToString:@"eMail_Doctor"]) {
                    
                    
                    [label setText: [parantIt.ChosenMedicen valueForKey:@"eMail_Doctor"]];
                    
                }
                
            }
            
        }
        
    }
}

-(void)setChoice:(NSString*)gothere
{
    NSArray *names1 =[NSArray arrayWithObjects:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Choose from Contacts",nil)],[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Add New Contacts",nil)], nil];
    
    for (NSInteger i=0 ; i<[names1 count]; i++) {
        
        UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
        [setimage setAlpha:0];
        
        if ([gothere isEqualToString:[names1 objectAtIndex:i]]) {
            
        }
        else
        {
            chosen=gothere;
        }
        
        if (parantIt) {
            
            
            if ([ [parantIt.MedicineSpecs valueForKey:gothere] isEqualToString:[names1 objectAtIndex:i]]) {
                
                UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
                [setimage setAlpha:1];
                
                UILabel *setimage2 = (UILabel *)[self.view viewWithTag:180+i];
                
                chosen = setimage2.text;
            }
        }
    }
}

- (IBAction)doSomething:(UIButton *)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSArray *names2 =[NSArray arrayWithObjects:@"Doctor",@"Phone_Doctor",@"eMail_Doctor", nil];

        for (NSInteger i=0 ; i<[names2 count]; i++)
        {
            for (UIView *view in self.view.subviews)
            {
                for (TekstIDLabel *label in view.subviews)
                {
                    if ([label isKindOfClass:[TekstIDLabel class]])
                    {
                        if ([label.title isEqualToString:[names2 objectAtIndex:i]])
                        {
                            if (label.text)
                            {
                                [parantIt.ChosenMedicen setObject:label.text forKey:[names2 objectAtIndex:i]];
                                
                                [[NSUserDefaults standardUserDefaults] setObject:label.text forKey:[names2 objectAtIndex:i]];
                                
                                if ([[names2 objectAtIndex:i] isEqualToString:@"Doctor"])
                                {
                                    [parantLabel setText:label.text];
                                }
                            }
                            else
                            {
                                [parantIt.ChosenMedicen setObject:@" " forKey:[names2 objectAtIndex:i]];
                                
                                [[NSUserDefaults standardUserDefaults] setObject:@" " forKey:[names2 objectAtIndex:i]];
                                
                                if ([[names2 objectAtIndex:i] isEqualToString:@""])
                                {
                                    [parantLabel setText:@" "];
                                }
                            }
                        }
                    }
                }
            }
        }
    });
}

- (IBAction)SelectChoice:(UIButton *)sender
{
    
}



@end
