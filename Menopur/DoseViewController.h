//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CodePopView, NewMedecineController, TekstIDLabel, TextViewData, PickerViewContent;
@interface DoseViewController : UIViewController<UITextViewDelegate> {


    UILabel *TitleText;
   NSString *Compaire;
    UIView *Combi;
     UIView *CombiOther;
    NSString *chosen;
      NSString *chosenID;
    NSString *theTime;
    NSString *theDay;
    NSString *Ammount;
    NSString *CompaireDecimal;
    PickerViewContent *content;
    UIView *Location;

}
@property (nonatomic, strong)  UIView *Location;
@property (nonatomic, strong)  TekstIDLabel *MedNameSet;
@property (nonatomic, strong)  UILabel *MedName;
@property (nonatomic, retain) TextViewData *textblock;
@property (nonatomic, retain) IBOutlet UILabel *underTitleText;
@property (nonatomic, retain) IBOutlet NSString *timeframe;
@property (nonatomic, retain) IBOutlet NSString *Ammount;
@property (nonatomic, retain) IBOutlet NSString *Drug_type;
@property (nonatomic, retain) IBOutlet NSString *Drug_types;
@property (nonatomic, retain) IBOutlet  NSMutableArray *TimeChoses;
@property (nonatomic, retain) NSString *theTime;
@property (nonatomic, retain) NSString *theDay;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;

-(void)whatValue:(NSString*)set;
-(void)setChoice:(NSString*)gothere;
-(void)change:(UIButton*)sender;
-(void) setTextview: (NSString *) text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
@end

