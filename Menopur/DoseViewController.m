//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "DoseViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "PickerViewContent.h"
#import "TekstIDLabel.h"
#import "TextViewData.h"
#import "GetData.h"
#import "LocationViewController.h"
@interface DoseViewController ()

@end

@implementation DoseViewController
@synthesize parantIt;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize theTime;
@synthesize theDay;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize underTitleText;
@synthesize Ammount;
@synthesize Drug_type;
@synthesize Drug_types;
@synthesize textblock;
@synthesize MedName;
@synthesize MedNameSet;



-(void)loadView{
    
    
    [super loadView];
}

-(void)whatValue:(NSString*)set
{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    [textblock.Content setText:set];
    
    [textblock.Value setText:appDelegate.dose_unit];

    //9-11-2017
    if (![textblock.Content.text isEqualToString:@""])
    {
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
        {
            NSLog(@"TESTING : %@",[SHARED_APPDELEGATE strMedtype]);
            
            
            if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Tablet"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Spray"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Injektion"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråber"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Plaster"])
            {
                [textblock.Value setText:@"stk"];
            }
            else
            {
                if ([SHARED_APPDELEGATE strMedtype] != nil)
                {
                    [textblock.Value setText:@"stykke"];
                }
            }
        }
    }
    else
    {
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
        {
            NSLog(@"TESTING : %@",[SHARED_APPDELEGATE strMedtype]);
            
            if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Tablet"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Spray"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Injektion"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråber"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Plaster"])
            {
                [textblock.Value setText:@"stk"];
            }
            else
            {
                if ([SHARED_APPDELEGATE strMedtype] != nil)
                {
                    [textblock.Value setText:@"stykke"];
                }
            }
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////NSLog(@"/%@/", appDelegate.dose_unit);
    
    [textblock changeValue];
    
    
    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
    
    ////////////////NSLog(@"%@", typit);
    
    if ([set integerValue]>1)
    {
        
        
        NSString *uitzondering =[[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_less"] withString:[typit valueForKey:@"Drug_types"]];
        
        
        [uitzondering stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"];
        
        [underTitleText setText:[NSString stringWithFormat:@"%@", uitzondering]];
        
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
        {
            if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
            {
                underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mendge"];
                
                underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Spray" withString:@"Mendge"];
                
            }
        }
    }
    
    else
    {
        
        [underTitleText setText:[NSString stringWithFormat:@"%@", [[typit valueForKey:@"Dose_name"]stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"]]];
        
        //15-11
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
        {
            if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
            {
                underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mendge"];
                
                underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Spray" withString:@"Mendge"];
                
            }
        }
    }
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [content rollTo:[NSString stringWithFormat:@"%@", set]];
        [content rollTo:[NSString stringWithFormat:@"%@", textblock.Value]];
        
        
    });
    
}

-(void)whatLabel:(TekstIDLabel*)set

{
    
    
    if ([set.text isEqualToString:@" "]) {
        
        
        
    }
    else
    {
        
        
        
        
        
    }
    
    parantLabel =set;
}


- (void)viewDidLoad {
    
     [self.view.layer setMasksToBounds:YES];
    
    NSLog(@"%@", self);
    
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(Save:) forControlEvents:UIControlEventTouchUpInside];
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
    }
    
    else
    {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
    ////////////////NSLog(@"%@", typit);
    
    Ammount = [[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"];
    
    Drug_type = [typit valueForKey:@"Drug_type"];
    
    Drug_types = [typit valueForKey:@"Drug_types"];
    
    
    
    TimeChoses =[[NSMutableArray alloc]init];
    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Cancel:)];
    
    
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Cancel setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
    
    
    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 24, self.view.frame.size.width-20, 44)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
//    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];
    
    
    
    underTitleText = [[UILabel alloc] initWithFrame:CGRectMake(20, 4, self.view.frame.size.width-40, 35)];
    [underTitleText setBackgroundColor:[UIColor whiteColor]];
    [underTitleText setText:[NSString stringWithFormat:@"%@", Ammount]];
//    [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes

    //16-11
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
    {
        if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
        {
            underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mendge"];
            
            underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Spray" withString:@"Mendge"];
        }
    }
    
    [underTitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
    [Combi addSubview:underTitleText];
    
    
    
    textblock = [[TextViewData alloc] initWithFrame:CGRectMake(20, 4,self.view.frame.size.width-50, 35)];
    [Combi addSubview:textblock];
    
    
    NSDate *now = [[NSDate alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [dateFormatter setDateFormat:@"EEEE"];
    
    theDay = [dateFormatter stringFromDate:now];
    
    
    
    
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [timeFormat setDateFormat:@"hh:mm"];
    theTime = [timeFormat stringFromDate:now];
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        NSLog(@"left");
        Combi.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        underTitleText.transform = CGAffineTransformMakeScale(-1, 1);
        [underTitleText setTextAlignment:NSTextAlignmentNatural];
        
        
        
        
       
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }
    

    
    
    
    NSDateFormatter *timeFormat3 = [[NSDateFormatter alloc] init];
    [timeFormat3 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [timeFormat3 setDateFormat:@"a"];
    
    timeframe = [timeFormat3 stringFromDate:now];
    
    
    
    ////////////////////////NSLog(@"typit %@", [typit valueForKey:@"Location_name"]);
    
    if ([[typit valueForKey:@"Location_name"] isEqualToString:@"-"]) {
        
        
    }
    else
    {
        
        
        Location = [[UIView alloc] initWithFrame:CGRectMake(10, 80, self.view.frame.size.width-20, 45)];
        [Location setBackgroundColor:[UIColor whiteColor]];
        [Location.layer setCornerRadius:10];
        [Location.layer setBorderWidth:1.5];
//        [Location.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
        [Location.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [Location.layer setMasksToBounds:YES];
        [self.view addSubview:Location];
        
        
        MedName = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-80, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setText:NSLocalizedString(@"Location:",nil)];
        [MedName setTextAlignment:NSTextAlignmentLeft];
        [MedName setFont:[UIFont boldSystemFontOfSize:15]];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Location addSubview:MedName];
        
        MedNameSet = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 3, self.view.frame.size.width-65, 35)];
        [MedNameSet setBackgroundColor:[UIColor clearColor]];
        [MedNameSet setTextColor:[UIColor blackColor]];
        [MedNameSet setTextAlignment:NSTextAlignmentRight];
        [MedNameSet setText:NSLocalizedString(@"In left ear",nil)];
        [MedNameSet setFont:[UIFont systemFontOfSize:12]];
        [MedNameSet setNumberOfLines:2];
        [Location addSubview:MedNameSet];
        
        [MedNameSet setText:[parantIt.ChosenMedicen valueForKey:@"Location"]];
        
        
        UIButton *Start = [[UIButton alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3, 60/2, 72/2)];
        [Start setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [Start addTarget:self action:@selector(goto) forControlEvents:UIControlEventTouchUpInside];
        [Start setTag:172];
        
        [Location addSubview:Start];
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
            
            NSLog(@"left");
            Location.transform = CGAffineTransformMakeScale(-1, 1);
            
            
            MedName.transform = CGAffineTransformMakeScale(-1, 1);
            [MedName setTextAlignment:NSTextAlignmentNatural];
            
            
            
            
            MedName.transform = CGAffineTransformMakeScale(-1, 1);
            [MedName setTextAlignment:NSTextAlignmentNatural];
            
            
            [MedNameSet setFrame:CGRectMake(0, 3, self.view.frame.size.width-165, 35)];
            
            [MedNameSet setBackgroundColor:[UIColor blueColor]];
            
            
            //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
            
            
        } else {
            
            //NSLog(@"right");
            //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
        }
        

        
    }
    
    
    NewMedecineController *oldscreen =(NewMedecineController *)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
    
    ////////////////////////////////////////////////////////////////////NSLog(@"%@", oldscreen.ChosenMedicen);
    
    [self getParant:oldscreen];
    
    [self whatValue:[oldscreen.ChosenMedicen valueForKey:@"Quantity"]];
    

    //NSLog(@"%@", self.view.subviews);

}

-(void)goto

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
    
    //NSLog(@"%@ %@", [typit valueForKey:@"Locations"], MedNameSet);
    
    LocationViewController *controller = [[LocationViewController alloc]init];
    [controller whatLabel:MedNameSet];
    [controller whatValue:[typit valueForKey:@"Locations"]];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)Cancel:(UIBarButtonItem*) sender

{
    
    
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)Save:(UIBarButtonItem*) sender

{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    [parantIt setitemsDictionary:appDelegate.dose_unit name:@"Dose_unit"];
    [parantIt setitemsDictionary:textblock.Content.text name:@"Quantity"];
    [parantIt setitemsDictionary:MedNameSet.text name:@"Location"];
    
    
    ////////////////////////////////////////////////////////////////////////////NSLog(@"%@", MedName.text);
    
    
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    
}


-(void)change:(UIButton*)sender

{
    
    
    
    if (sender.tag ==140 ||sender.tag ==180) {
        
        
    }
    if (sender.tag ==141 ||sender.tag ==181) {
        
    }
    if (sender.tag ==142 ||sender.tag ==182) {
        
        
        TimesofDayViewController *controller = [[TimesofDayViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (sender.tag ==143 ||sender.tag ==183) {
        
    }
    if (sender.tag ==144 ||sender.tag ==184) {
        
    }
    
    
    
    
    
    
}

-(void)Action:(UIButton*)sender

{
    
    
}

-(void)setChoice:(NSString*)gothere

{
    
}

-(void)changeChose:(NSString*)sender

{
    
    
    chosen = sender;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    //////////////////////     [self.view.layer setMasksToBounds:YES];
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
    ////////////////NSLog(@"%@", typit);
    
    
    
    CompaireDecimal = NSLocalizedString([typit valueForKey:@"Dose_unit"],nil);
    //Med Strenght
    
    //9-11-2017
    
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
    {
        if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Tablet"] || [[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Spray"])
        {
            CompaireDecimal = @"stk";
        }
    }
    
    
    NSArray *numbers2 = [CompaireDecimal componentsSeparatedByString:@","];
    
    if ([numbers2 count]>1) {
        
        
        content = [[PickerViewContent alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-180, 320, 180)];
        
        
        [content Decimal:CompaireDecimal pernt:self];
        
        
        
        [self.view insertSubview:content aboveSubview:appDelegate.toolbarDown];
        
        
    }
    else
    {
        content = [[PickerViewContent alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-180, 320, 180)];
        
        [content Decimal:CompaireDecimal pernt:self];
        
        [self.view insertSubview:content aboveSubview:appDelegate.toolbarDown];
        
        
    }
    
    
    [appDelegate.toolbarDown setAlpha:0];
    

    
    
}

-(void)chosenValue:(NSString*)sender;


{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
    
    ////////////////NSLog(@"%@", typit);
    
    if ([sender integerValue]>1)
    {
        
        
        NSString *uitzondering =[[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_less"] withString:[typit valueForKey:@"Drug_types"]];
        
        
        [underTitleText setText:[NSString stringWithFormat:@"%@", [uitzondering stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"]]];
     
        //15-11
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
        {
            if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
            {
                underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mendge"];
                
                underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Spray" withString:@"Mendge"];
            }
        }
        
    }
    
    else
    {
        
        
        [underTitleText setText:[NSString stringWithFormat:@"%@", [[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"]]];
        
        //15-11
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
        {
            if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
            {
                underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mendge"];
                
                underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Spray" withString:@"Mendge"];
            }
        }
    }
    
    [textblock.Content setText:sender];
    
    [textblock changeValue];
    
}

-(void)chosenMonth:(NSString*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.dose_unit = NSLocalizedString(sender,nil);
    [textblock.Value setText:NSLocalizedString(sender,nil)];
    
    [textblock changeValue];
    
    
    
}


- (IBAction)doSomething:(UIButton *)sender {
    
}

- (IBAction)doSomething2:(UIButton *)sender {
    
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [parantIt setitemsDictionary:textblock.Content.text name:@"Strength"];
        [parantIt setitemsDictionary:appDelegate.strength_unit name:@"Strength_unit"];
        
        if ([MedNameSet.text isEqualToString:@" "]) {
            [parantIt setitemsDictionary:@" " name:@"Location"];
        }
        else
            
        {
            [parantIt setitemsDictionary:MedNameSet.text name:@"Location"];
        }
        
        
        
    });
    
    
}

-(void) backButtonPressed
{
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    else
    {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
}

-(void) setTextview: (NSString *) text
{
    
    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [TitleText setText:text];
        
    });
}



-(void)hideAndseek:(NSString*)set
{
    
    
}

-(void)getParant:(UIViewController*)parant
{
    parantIt = (NewMedecineController*)parant;
}




- (IBAction)SelectChoice:(UIButton *)sender {
    
}

@end
