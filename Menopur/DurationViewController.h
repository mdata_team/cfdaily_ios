//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CodePopView, FrequencyViewControllerNew, TekstIDLabel;
@interface DurationViewController : UIViewController<UITextViewDelegate> {


   NSString *Compaire;

}
@property (nonatomic, retain) NSMutableArray *Navigaioncopy;
@property (nonatomic, retain) FrequencyViewControllerNew *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;
@property (nonatomic, retain) IBOutlet NSString *titleEdit;
@property (nonatomic, retain) IBOutlet UIDatePicker *pickerTaal;
@property (nonatomic, assign) NSInteger countDict;
@property (nonatomic, retain) IBOutlet UIView *CombiIn;
@property (nonatomic, retain) IBOutlet UITextView *TitleTextIn;


@property (nonatomic, retain) IBOutlet UITextView *Copytext;

@property (nonatomic, retain) IBOutlet UIView *CombiOut;
@property (nonatomic, retain) IBOutlet UITextView *TitleTextOut;

-(void) setTextview: (NSString *) text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
-(void)calculateInterval:(NSDate*)sender count:(NSInteger)interval;
-(void)setChoice:(NSString*)gothere;
-(void)change:(UIButton*)sender;

@end

