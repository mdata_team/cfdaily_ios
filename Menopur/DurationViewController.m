//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "DurationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewProfileController.h"
#import "NewMedecineController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "TimeSchedulare.h"
#import "GetData.h"
#import "Addbutton.h"
#import "TekstIDLabel.h"
#import "FrequencyViewControllerNew.h"
@interface DurationViewController ()

@end

@implementation DurationViewController
@synthesize parantIt;
@synthesize pickerTaal;
@synthesize CombiIn;
@synthesize CombiOut;
@synthesize TitleTextIn;
@synthesize TitleTextOut;
@synthesize parantLabel;
@synthesize Navigaioncopy;
@synthesize Copytext;
@synthesize countDict;
@synthesize titleEdit;


-(void)loadView{
    
    
    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [TitleTextOut setText:set.text];
        
        parantLabel =(TekstIDLabel*)set;
        
        if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Starting Date",nil)]) {
            
            
            
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            
            [format setDateFormat:@"dd/MM/yyyy"];
            
            if ([format dateFromString:TitleTextOut.text]) {
                [pickerTaal setDate:[format dateFromString:TitleTextOut.text]];
                
            }
            
            
        }
        
        if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Reminder Time",nil)]) {
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            
            
            
            //[format setDateFormat:[myApp convertString:@"hh:mm a"]];
            
            [format setDateFormat:[myApp convertString:@"hh:mm a"]];
            
            if ([format dateFromString:TitleTextOut.text]) {
                [pickerTaal setDate:[format dateFromString:TitleTextOut.text]];
            }
            
        }
        
        if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Ending Date",nil)]) {
            
            
            
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            
            [format setDateFormat:@"dd/MM/yyyy"];
            
            if ([format dateFromString:TitleTextOut.text]) {
                
                [pickerTaal setDate:[format dateFromString:TitleTextOut.text]];
                
            }
        }
        
        
    });
    
    
    
    
}


-(void)calculateInterval:(NSDate*)sender count:(NSInteger)interval

{
    
    
    [parantIt.table.TimesSet removeAllObjects];
    
    for (NSInteger i=0 ; i<24/interval; i++) {
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        
        
//        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:sender]; //26-06-2017
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:sender];
        
        
        NSDateComponents *dateComps = [[NSDateComponents alloc] init];
        [dateComps setHour:[timeComponents hour]+(interval*i)];
        [dateComps setMinute:[timeComponents minute]];
        [dateComps setSecond:0];
        
        NSDate *itemDate  = [calendar dateFromComponents:dateComps];
        
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [dateFormat setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        [set setObject:[dateFormat stringFromDate:itemDate] forKey:@"Time"];
        
        
        if ([dateFormat stringFromDate:itemDate]) {
            
            [parantIt.table.TimesSet addObject:set];
        }
        else
            
        {
            
            
        }
        
        
    }
    
    
    BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
    if (strsetting) {
        
        //NSLog(@"YES");
    }
    else
        
    {
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
        [parantIt.table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
        
        NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"am", @"a.m.", @"vorm.", @"μ.μ.",  @"AM"];
        
        
        NSArray *notificationArray= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate];
        
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"pm", @"p.m.", @"nachm.", @"π.μ.",  @"PM"];
        
        
        NSArray *notificationArray2= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate2];
        
        [parantIt.table.TimesSet removeAllObjects];
        
        [parantIt.table.TimesSet addObjectsFromArray:notificationArray];
        [parantIt.table.TimesSet addObjectsFromArray:notificationArray2];
        
        
        NSArray *copy = [parantIt.table.TimesSet copy];
        NSInteger index = [copy count] - 1;
        for (id object in [copy reverseObjectEnumerator]) {
            if ([parantIt.table.TimesSet indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                [parantIt.table.TimesSet removeObjectAtIndex:index];
            }
            index--;
        }
        
    }
    
    
}



- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];
    
    
    
    
    //     [self.view.layer setMasksToBounds:YES];
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
    
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];

    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(Back:) forControlEvents:UIControlEventTouchUpInside];
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal]; a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
    }
    
    else
    {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
    
    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    
    
    if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Starting Date",nil)]) {
        
        
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            
            self.navigationItem.title =NSLocalizedString(@"Starting Date", nil);
            // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:20.0];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            [label sizeToFit];
            
        }
        NSDate *now = [[NSDate alloc] init];
        
        
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [format setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        
        
        NSCalendar* cal = [NSCalendar currentCalendar];
//        NSDateComponents* comp = [cal components:NSWeekdayCalendarUnit fromDate:now]; //26-06-2017 change
        NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:now];
        
        parantIt.CurrentDay = [comp weekday];
        
        
        
        UILabel *underTitleText = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, self.view.frame.size.width-20, 35)];
        [underTitleText setBackgroundColor:[UIColor clearColor]];
        [underTitleText setText:NSLocalizedString(@"Starting Date",nil)];
//        [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [underTitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
        [self.view addSubview:underTitleText];
        
        
        CombiOut = [[UIView alloc] initWithFrame:CGRectMake(10, 10+34, self.view.frame.size.width-20, 44)];
        [CombiOut setBackgroundColor:[UIColor whiteColor]];
        [CombiOut.layer setCornerRadius:10];
        [CombiOut.layer setBorderWidth:1.5];
//        [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
        [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [CombiOut.layer setMasksToBounds:YES];
        [self.view addSubview:CombiOut];
        
        
        
        
        TitleTextOut = [[UITextView alloc] initWithFrame:CGRectMake(15, 14+34, self.view.frame.size.width-40, 35)];
        [TitleTextOut setBackgroundColor:[UIColor whiteColor]];
        [TitleTextOut setDelegate:self];
//        [TitleTextOut setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]]; //28-06-2017 changes
        [TitleTextOut setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [TitleTextOut setTag:123+1];
        [TitleTextOut setEditable:YES];
        [TitleTextOut setText:[format stringFromDate:now]];
        [TitleTextOut setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [self.view addSubview:TitleTextOut];
        
        
        
        pickerTaal = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-300, 320, 300)];
        
        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
        
        if (!strsetting)
        {
            
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US_POSIX",nil)];
            pickerTaal.locale = locale;
            
        }
        else
            
        {
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)];
            pickerTaal.locale = locale;
            
        }
        
        
        
        
        
        
        pickerTaal.datePickerMode = UIDatePickerModeDate;
        [pickerTaal setTag:679];
        [pickerTaal addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:pickerTaal];
        
        
        [pickerTaal setMinimumDate:now];
        [pickerTaal setMaximumDate:NULL];
        
        
        
        
        
        
        
    }
    else if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Reminder Time",nil)]) {
        
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            self.navigationItem.title =NSLocalizedString(@"Reminder Time",nil);
            // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:20.0];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            [label sizeToFit];
            
            
        }
        
        
        
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSDate *now = [[NSDate alloc] init];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        
        //[format setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        [format setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        
        UILabel *underTitleText = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, self.view.frame.size.width-20, 35)];
        [underTitleText setBackgroundColor:[UIColor clearColor]];
        [underTitleText setText:NSLocalizedString(@"Reminder Time",nil)];
//        [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [underTitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
        [self.view addSubview:underTitleText];
        
        
        CombiOut = [[UIView alloc] initWithFrame:CGRectMake(10, 10+34, self.view.frame.size.width-20, 44)];
        [CombiOut setBackgroundColor:[UIColor whiteColor]];
        [CombiOut.layer setCornerRadius:10];
        [CombiOut.layer setBorderWidth:1.5];
//        [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
        [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [CombiOut.layer setMasksToBounds:YES];
        [self.view addSubview:CombiOut];
        
        
        
        
        TitleTextOut = [[UITextView alloc] initWithFrame:CGRectMake(15, 14+34, self.view.frame.size.width-40, 35)];
        [TitleTextOut setBackgroundColor:[UIColor whiteColor]];
        [TitleTextOut setDelegate:self];
//        [TitleTextOut setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]]; //28-06-2017 changes
        [TitleTextOut setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [TitleTextOut setTag:123+1];
        [TitleTextOut setEditable:YES];
        [TitleTextOut setText:[format stringFromDate:now]];
        [TitleTextOut setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [self.view addSubview:TitleTextOut];
        
        
        
        pickerTaal = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-300, 320, 300)];
        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
        
        if (!strsetting)
        {
            
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US_POSIX",nil)];
            pickerTaal.locale = locale;
            
        }
        else
            
        {
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)];
            pickerTaal.locale = locale;
            
        }
        
        
        
        
        pickerTaal.datePickerMode = UIDatePickerModeTime;
        [pickerTaal setTag:679];
        [pickerTaal addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:pickerTaal];
        
        
        
        
    }
    else if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Starting Time",nil)]) {
        
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            self.navigationItem.title =NSLocalizedString(@"Starting Time",nil);
            // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:20.0];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            [label sizeToFit];
            
        }
        
        NSDate *now = [[NSDate alloc] init];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        //[format setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        [format setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        
        
        UILabel *underTitleText = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, self.view.frame.size.width-20, 35)];
        [underTitleText setBackgroundColor:[UIColor clearColor]];
        [underTitleText setText:NSLocalizedString(@"Starting Time",nil)];
//        [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [underTitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
        [self.view addSubview:underTitleText];
        
        
        CombiOut = [[UIView alloc] initWithFrame:CGRectMake(10, 10+34, self.view.frame.size.width-20, 44)];
        [CombiOut setBackgroundColor:[UIColor whiteColor]];
        [CombiOut.layer setCornerRadius:10];
        [CombiOut.layer setBorderWidth:1.5];
//        [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; // 28-06-2017 changes
        [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [CombiOut.layer setMasksToBounds:YES];
        [self.view addSubview:CombiOut];
        
        
        
        
        TitleTextOut = [[UITextView alloc] initWithFrame:CGRectMake(15, 14+34, self.view.frame.size.width-40, 35)];
        [TitleTextOut setBackgroundColor:[UIColor whiteColor]];
        [TitleTextOut setDelegate:self];
//        [TitleTextOut setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]]; //28-06-2017 changes
        [TitleTextOut setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [TitleTextOut setTag:123+1];
        [TitleTextOut setEditable:YES];
        [TitleTextOut setText:[format stringFromDate:now]];
        [TitleTextOut setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [self.view addSubview:TitleTextOut];
        
        
        pickerTaal = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-300, 320, 300)];
        
        
        
        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
        
        
        
        if (!strsetting)
        {
            
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US_POSIX",nil)];
            pickerTaal.locale = locale;
            
        }
        else
            
        {
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)];
            pickerTaal.locale = locale;
            
        }
        
        
        
        
        
        pickerTaal.datePickerMode = UIDatePickerModeTime;
        [pickerTaal setTag:679];
        [pickerTaal addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:pickerTaal];
        
        
        
        
        
    }
    else if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Every",nil)]) {
        
        
        
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        NSDate *now = [[NSDate alloc] init];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        
        //[format setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        [format setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        
        UILabel *underTitleText = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, self.view.frame.size.width-20, 35)];
        [underTitleText setBackgroundColor:[UIColor clearColor]];
        [underTitleText setText:NSLocalizedString(@"Ending Date",nil)];
//        [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [underTitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
        [self.view addSubview:underTitleText];
        
        
        CombiOut = [[UIView alloc] initWithFrame:CGRectMake(10, 10+34, self.view.frame.size.width-20, 44)];
        [CombiOut setBackgroundColor:[UIColor whiteColor]];
        [CombiOut.layer setCornerRadius:10];
        [CombiOut.layer setBorderWidth:1.5];
//        [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
        [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [CombiOut.layer setMasksToBounds:YES];
        [self.view addSubview:CombiOut];
        
        
        
        
        TitleTextOut = [[UITextView alloc] initWithFrame:CGRectMake(15, 14+34, self.view.frame.size.width-40, 35)];
        [TitleTextOut setBackgroundColor:[UIColor whiteColor]];
        [TitleTextOut setDelegate:self];
//        [TitleTextOut setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]]; //28-06-2017 changes
        [TitleTextOut setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [TitleTextOut setTag:123+1];
        [TitleTextOut setEditable:YES];
        [TitleTextOut setText:[format stringFromDate:now]];
        [TitleTextOut setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [self.view addSubview:TitleTextOut];
        
        
        pickerTaal = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-300, 320, 300)];
        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
        
        if (!strsetting)
        {
            
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US_POSIX",nil)];
            pickerTaal.locale = locale;
            
        }
        else
            
        {
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)];
            pickerTaal.locale = locale;
            
        }
        
        
        pickerTaal.datePickerMode = UIDatePickerModeTime;
        [pickerTaal setTag:679];
        [pickerTaal addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:pickerTaal];
        
        
        
        
        
    }
    
    
    else if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Ending Date",nil)]) {
        
        
        
        self.navigationItem.title =NSLocalizedString(@"Ending Date",nil);
        // this will appear as the title in the navigation bar
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:20.0];
        label.textAlignment = NSTextAlignmentCenter;
        // ^-Use NSTextAlignmentCenter for older SDKs.
        label.textColor = [UIColor whiteColor]; // change this color
        self.navigationItem.titleView = label;
        [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
        label.text = self.navigationItem.title;
        [label sizeToFit];
        
        NSDate *now = [[NSDate alloc] init];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [format setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        
        UILabel *underTitleText = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, self.view.frame.size.width-20, 35)];
        [underTitleText setBackgroundColor:[UIColor clearColor]];
        [underTitleText setText:NSLocalizedString(@"Ending Date",nil)];
//        [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [underTitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
        [self.view addSubview:underTitleText];
        
        
        CombiOut = [[UIView alloc] initWithFrame:CGRectMake(10, 10+34, self.view.frame.size.width-20, 44)];
        [CombiOut setBackgroundColor:[UIColor whiteColor]];
        [CombiOut.layer setCornerRadius:10];
        [CombiOut.layer setBorderWidth:1.5];
//        [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
        [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [CombiOut.layer setMasksToBounds:YES];
        [self.view addSubview:CombiOut];
        
        
        
        
        TitleTextOut = [[UITextView alloc] initWithFrame:CGRectMake(15, 14+34, self.view.frame.size.width-40, 35)];
        [TitleTextOut setBackgroundColor:[UIColor whiteColor]];
        [TitleTextOut setDelegate:self];
//        [TitleTextOut setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]]; //28-06-2017 changes
        [TitleTextOut setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [TitleTextOut setTag:123+1];
        [TitleTextOut setEditable:YES];
        [TitleTextOut setText:[format stringFromDate:now]];
        [TitleTextOut setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [self.view addSubview:TitleTextOut];
        
        
        pickerTaal = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-300, 320, 300)];
        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
        
        if (!strsetting)
        {
            
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US_POSIX",nil)];
            pickerTaal.locale = locale;
            
        }
        else
            
        {
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)];
            pickerTaal.locale = locale;
            
        }
        
        
        pickerTaal.datePickerMode = UIDatePickerModeDate;
        [pickerTaal setTag:679];
        [pickerTaal addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:pickerTaal];
        
        
        
        
        
    }
    
    
    
    if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Instructions",nil)]) {
        
    }
    else
    {
        
        //[TitleTextIn setInputAccessoryView:pickerTaal];
        
        //[TitleTextOut setInputAccessoryView:pickerTaal];
        
        
        //[TitleTextOut becomeFirstResponder];
    }
    
    
    
    
}


-(void)textViewDidBeginEditing:(UITextView *)textView

{
    Copytext =textView;
}

-(void)valueChanged:(UIDatePicker*)sender
{
    
    
    
    
    if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Starting Date",nil)]) {
        
        
        if ([parantIt.chosenFrequentcy isEqualToString:NSLocalizedString(@"Every X Days",nil)]||[parantIt.chosenFrequentcy isEqualToString:NSLocalizedString(@"Daily",nil)]) {
            
            
            
            [parantIt.DayRow removeLastObject];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            
            [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy"];
            NSString *theDay = [dateFormatter stringFromDate:sender.date];
            
            
            
            NSCalendar* cal = [NSCalendar currentCalendar];
//            NSDateComponents* comp = [cal components:NSWeekdayCalendarUnit fromDate:sender.date]; //26-06-2017
            NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:sender.date];
            
            parantIt.CurrentDay = [comp weekday];
            
            
            NSArray *names =[NSArray arrayWithObjects:@"Su,1",@"Mo,2",@"Tu,3",@"We,4",@"Th,5",@"Fr,6",@"Sa,7", nil];
            
            for (NSInteger i =0; i < [names count]; i++) {
                
                
                NSArray *numbers2 = [[names objectAtIndex:i] componentsSeparatedByString:@","];
                
                
                if (parantIt.CurrentDay == [[numbers2 objectAtIndex:1] intValue]) {
                    
                    
                    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                    [set setObject:NSLocalizedString([numbers2 objectAtIndex:0],nil) forKey:@"Day"];
                    [set setObject:[numbers2 objectAtIndex:1]  forKey:@"Number"];
                    
                    if ([parantIt.DayRow containsObject:set]) {
                        
                    }
                    else
                    {
                        
                        [parantIt.DayRow addObject:set];
                        
                    }
                    
                    
                    [TitleTextIn setText:[dateFormatter stringFromDate:sender.date]];
                    
                    
                    [TitleTextOut setText:[dateFormatter stringFromDate:sender.date]];
                    
                    //13-11-2017
                    
                    NSString *strTemp = TitleTextOut.text;
                    strTemp = [strTemp stringByReplacingOccurrencesOfString:@"," withString:@"."];
                    [TitleTextOut setText:strTemp];
                    
                }
                
                
            }
            
            
            [Copytext setText:theDay];
            
        }
        else
        {
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            
            [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy"];
            NSString *theDay = [dateFormatter stringFromDate:sender.date];
            
            
            [TitleTextIn setText:[dateFormatter stringFromDate:sender.date]];
            
            
            [TitleTextOut setText:[dateFormatter stringFromDate:sender.date]];
            
            
            
            [Copytext setText:theDay];
        }
        
        
    }
    
    
    
    if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Ending Date",nil)]) {
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy"];
        NSString *theDay = [dateFormatter stringFromDate:sender.date];
        
        
        [TitleTextIn setText:[dateFormatter stringFromDate:sender.date]];
        
        
        [TitleTextOut setText:[dateFormatter stringFromDate:sender.date]];
        
        
        
        [Copytext setText:theDay];
        
    }
    
    if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Every",nil)]) {
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [dateFormatter setDateFormat:[myApp convertString:@"hh:mm a"]];
        NSString *theDay = [dateFormatter stringFromDate:sender.date];
        
        
        [TitleTextIn setText:[dateFormatter stringFromDate:sender.date]];
        
        
        [TitleTextOut setText:[dateFormatter stringFromDate:sender.date]];
        
        
        
        [Copytext setText:theDay];
        
    }
    
    
    if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Reminder Time",nil)]) {
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [dateFormatter setDateFormat:[myApp convertString:@"hh:mm a"]];
        NSString *theDay = [dateFormatter stringFromDate:sender.date];
        
        
        [TitleTextIn setText:[dateFormatter stringFromDate:sender.date]];
        
        
        [TitleTextOut setText:[dateFormatter stringFromDate:sender.date]];
        
        
        
        [Copytext setText:theDay];
        
    }
    
    
    
    if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Starting Time",nil)]) {
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [dateFormatter setDateFormat:[myApp convertString:@"hh:mm a"]];
        NSString *theDay = [dateFormatter stringFromDate:sender.date];
        
        
        [TitleTextIn setText:[dateFormatter stringFromDate:sender.date]];
        
        
        [TitleTextOut setText:[dateFormatter stringFromDate:sender.date]];
        
        
        
        [Copytext setText:theDay];
        
    }
    
    
    
    
    
    
    
}

-(void)Cancel:(UIBarButtonItem*) sender

{
    
    
    
    
    //[parantIt getchoice:TimeChoses];
    [TitleTextIn resignFirstResponder];
    [TitleTextOut resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
    [parantLabel setText:TitleTextOut.text];
    
    
    
    
}


-(void)Save:(UIBarButtonItem*) sender

{
    
    
    
    
    [parantLabel setText:TitleTextIn.text];
    [TitleTextIn resignFirstResponder];
    [TitleTextOut resignFirstResponder];
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    [parantLabel setText:TitleTextOut.text];
    
    
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    
    //////////////     [self.view.layer setMasksToBounds:YES];
    
    
    if ([TitleTextOut.text isEqualToString:NSLocalizedString(@"add new time",nil)]) {
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        NSDate *now = [[NSDate alloc] init];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        
        //[format setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        [format setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        
        [TitleTextOut setText:[format stringFromDate:now]];
        
    }
    else
        
    {
        
        
        
    }
    [TitleTextIn setKeyboardType:UIKeyboardTypeDefault];
    
    TitleTextIn.selectedRange = NSMakeRange(0, 0);
    [TitleTextIn setEditable:YES];
    
    
    
}

-(void) backButtonPressed
{
    
    if (TitleTextIn.editable == NO) {
        
        Compaire = [NSString stringWithFormat:@"%@",  TitleTextIn.text];
        
        TitleTextIn.selectedRange = NSMakeRange(0, 0);
        [TitleTextIn setEditable:YES];
        //[TitleTextIn becomeFirstResponder];
        
        //self.navigationItem.leftBarButtonItem = nil;
        
        
        
    }
    else
    {
        
        
        
        [TitleTextIn setEditable:NO];
        [TitleTextIn resignFirstResponder];
        
        //UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
       // [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
            [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
        
    }
    
    
}

-(void) setTextview: (NSString *) text
{
    
    [TitleTextIn setText:text];
    
    
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    
    
    
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextView *)textField {
    
    
    return YES;
}

-(void)hideAndseek:(NSString*)set
{
    
}



-(void)getParant:(UIViewController*)parant

{
    
    parantIt =(FrequencyViewControllerNew*)parant;
    
}

-(void)reorder


{
    
    BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
    if (strsetting) {
        //NSLog(@"YES");
    }
    else
        
    {
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
        [parantIt.table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
        
        NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"am", @"a.m.", @"vorm.", @"μ.μ.", @"AM"];
        
        NSArray *notificationArray= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate];
        
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)@)", @"pm", @"p.m.", @"nachm.", @"π.μ.", @"PM"];
        
        NSArray *notificationArray2= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate2];
        
        [parantIt.table.TimesSet removeAllObjects];
        
        [parantIt.table.TimesSet addObjectsFromArray:notificationArray];
        [parantIt.table.TimesSet addObjectsFromArray:notificationArray2];
        
        
        
        NSArray *copy = [parantIt.table.TimesSet copy];
        NSInteger index = [copy count] - 1;
        for (id object in [copy reverseObjectEnumerator]) {
            if ([parantIt.table.TimesSet indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                [parantIt.table.TimesSet removeObjectAtIndex:index];
            }
            index--;
        }
        
    }
    
}


- (IBAction)Back:(UIButton *)sender {
    
    
    if ([titleEdit isEqualToString:@"Edit"]) {
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [dateFormat setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        
        
        NSDateFormatter *dateFormat24 = [[NSDateFormatter alloc] init];
        [dateFormat24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormat24 setDateFormat:[myApp convertString:@"hh:mm a"]];
        
        
        
        
        NSDate *now =[dateFormat dateFromString:[myApp changesDone:parantLabel.text]];
        
        //
        
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        
        
        
        [set setObject:[dateFormat24 stringFromDate:now]  forKey:@"Date"];
        [set setObject:parantLabel.text forKey:@"Time"];
        
        
        NSDate *now2 =[dateFormat dateFromString:TitleTextOut.text];
        
        NSMutableDictionary *set2 = [[NSMutableDictionary alloc]init];
        
        
        [set2 setObject:[dateFormat24 stringFromDate:now2]  forKey:@"Date"];
        [set2 setObject:TitleTextOut.text forKey:@"Time"];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
        [parantIt.table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
        
        [parantIt.table.TimesSet replaceObjectAtIndex:[parantIt.table.TimesSet indexOfObject:set] withObject:set2];
        
        
        BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
        
        if (strsetting) {
            //NSLog(@"YES");
        }
        else
            
        {
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
            [parantIt.table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
            
            NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"am", @"a.m.", @"vorm.", @"μ.μ.", @"AM"];
            
            NSArray *notificationArray= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate];
            
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"pm", @"p.m.", @"nachm.", @"π.μ.", @"PM"];
            NSArray *notificationArray2= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate2];
            
            [parantIt.table.TimesSet removeAllObjects];
            
            [parantIt.table.TimesSet addObjectsFromArray:notificationArray];
            [parantIt.table.TimesSet addObjectsFromArray:notificationArray2];
            
            
            NSArray *copy = [parantIt.table.TimesSet copy];
            NSInteger index = [copy count] - 1;
            for (id object in [copy reverseObjectEnumerator]) {
                if ([parantIt.table.TimesSet indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                    [parantIt.table.TimesSet removeObjectAtIndex:index];
                }
                index--;
            }
            
        }
        
        [parantIt.table reloadData];
        
        
        
        [parantLabel setText:TitleTextOut.text];
        
    }
    else
    {
        
        
        if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Reminder Time",nil)]) {
            
            [parantIt.add_button.EndingViewText setText:TitleTextOut.text];
            [parantIt.add_button.AddButton setHidden:NO];
        }
        if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Starting Time",nil)]) {
            
            [parantLabel setText:TitleTextOut.text];
            [parantIt.DayinaRow removeAllObjects];
            
            
            
            AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            if ([NSLocalizedString(parantIt.EveryViewText.title,nil) isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
                
                
                if ([parantIt.DayinaRow count]==0) {
                    
                    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                    
                    
                    [set setObject:[myApp changesDone:parantIt.Name.text] forKey:@"Date"];
                    [set setObject:@" " forKey:@"DateAssent"];
                    [set setObject:@"" forKey:@"Day_number"];
                    [set setObject:parantIt.EndingViewText.text forKey:@"Ending"];
                    [set setObject:parantIt.EveryViewText.text forKey:@"Interval"];
                    [set setObject:self.title forKey:@"Setting"];
                    
                    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                    [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
                    
                    NSDateFormatter *dateConverted = [[NSDateFormatter alloc]init];
                    [dateConverted setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                    [dateConverted setDateFormat:@"dd/mm/yyyy"];
                    
                    [set setObject:[dateConvert dateFromString:parantIt.Name.text] forKey:@"Starting"];
                    NSString *uploadString = [[parantIt.table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
                    
                    [set setObject:uploadString forKey:@"Times"];
                    [parantIt.DayinaRow  addObject:set];
                    
                    //////////////////////NSLog(@"%@", set);
                    
                    
                    
                }
                else
                    
                {
                    for (NSInteger i=0 ; i<[parantIt.DayinaRow count]; i++) {
                        
                        
                        NSString *value =[NSString stringWithFormat:@"%i", [parantIt.EveryViewText.text intValue]*24];
                        
                        [[parantIt.DayinaRow objectAtIndex:i] removeObjectForKey:@"Interval"];
                        [[parantIt.DayinaRow objectAtIndex:i] setObject:value forKey:@"Interval"];
                        
                        [[parantIt.DayinaRow objectAtIndex:i] removeObjectForKey:@"Setting"];
                        [[parantIt.DayinaRow objectAtIndex:i] setObject:self.title forKey:@"Setting"];
                        
                        
                        
                    }
                }
                
                
            }
            
        
            
        }
        if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Every",nil)]) {
            
            [parantLabel setText:TitleTextOut.text];
            
            
            
        }
        if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Ending Date",nil)]) {
            
            [parantLabel setText:TitleTextOut.text];
            
            
        }
        if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Starting Date",nil)]) {
            
            
            [parantLabel setText:TitleTextOut.text];
            
            
            
        }
        
        
        
    }
    
    
    
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    
    
}


-(NSString*)getnumberOfday:(NSString*)day

{
    NSString *StringReturn;
    
    
    NSArray *names =[NSArray arrayWithObjects:@"Su",@"Mo",@"Tu",@"We",@"Th",@"Fr",@"Sa", nil];
    NSArray *number =[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7", nil];
    
    
    for (NSInteger i=0 ; i<[names count]; i++) {
        
        if ([day isEqualToString:NSLocalizedString([names objectAtIndex:i],nil)]) {
            
            StringReturn =[number objectAtIndex:i];
        }
        
        
    }
    
    return StringReturn;
    
}

- (IBAction)SelectChoice:(UIButton *)sender {
    
    
    
    
    
}



@end
