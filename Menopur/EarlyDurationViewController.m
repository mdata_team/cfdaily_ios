    //
    //  ViewController.m
    //  Menopur
    //
    //  Created by Jeffrey Snijder on 20-01-13.
    //  Copyright (c) 2013 Menopur. All rights reserved.
    //

#import "EarlyDurationViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "SaveData.h"
#import "Settings.h"
@interface EarlyDurationViewController ()

@end

@implementation EarlyDurationViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize theTime;
@synthesize theDay;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize underTitleText;
@synthesize theDate;



-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    
    
    parantLabel =(TekstIDLabel*)set;
    
    [TitleText setText:set.text];
    
    
    if ([TitleText.text length]<4) {
        
        
        if ([TitleText.text length] ==0) {
            
            [underTitleText setText:[NSString stringWithFormat:@"   %@",NSLocalizedString(@"Minutes earlier",nil)]];
        }
        if ([TitleText.text length] ==1) {
            
            [underTitleText setText:[NSString stringWithFormat:@"     %@",NSLocalizedString(@"Minutes earlier",nil)]];
        }
        if ([TitleText.text length] ==2) {
            
            [underTitleText setText:[NSString stringWithFormat:@"      %@",NSLocalizedString(@"Minutes earlier",nil)]];
        }
        if ([TitleText.text length] ==3) {
            
            [underTitleText setText:[NSString stringWithFormat:@"       %@",NSLocalizedString(@"Minutes earlier",nil)]];
        }
        
        
    }
    else
    {
        [TitleText setText:@""];
        [underTitleText setText:[NSString stringWithFormat:@"   %@",NSLocalizedString(@"Minutes earlier",nil)]];
    }
    
    
}

-(void)setChoice:(NSString*)gothere

{
    
}

- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];

    
    //     [self.view.layer setMasksToBounds:YES];

    TimeChoses =[[NSMutableArray alloc]init];


    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];

    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Cancel:)];

    
    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Cancel setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }
    



    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];


    UIView *textit  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 160, 20)];
    
    [title setTextColor:[UIColor whiteColor]];
    [title setText:NSLocalizedString(@"Enter Earlier",nil)];
    [title setFont:[UIFont boldSystemFontOfSize:15]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];
    [textit addSubview:title];


    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:textit];




    UIBarButtonItem *Save = [[UIBarButtonItem alloc]
                             initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleBordered target:self
                             action:@selector(Save:)];
 

    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Save setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }




    UIToolbar *toolbarUp = [[UIToolbar alloc] init];
    toolbarUp.frame = CGRectMake(0, -2, 320, 54);
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        
        
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forToolbarPosition:0 barMetrics:0];
        
    }
    
    else
    {
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forToolbarPosition:0 barMetrics:0];
    }
    
    [toolbarUp setTranslucent:NO];
    [self.view addSubview:toolbarUp];


    NSArray *itemsUp = [NSArray arrayWithObjects:Cancel,flexItem,random,flexItem,Save,nil];
    toolbarUp.items = itemsUp;



    UILabel *Tekst = [[UILabel alloc] initWithFrame:CGRectMake(12, 68, 280, 20)];
    [Tekst setTextColor:[UIColor clearColor]];
    [Tekst setText:NSLocalizedString(@"Set Early Time Durations", nil)];
    [Tekst setFont:[UIFont boldSystemFontOfSize:14]];
    [Tekst setTextAlignment:NSTextAlignmentLeft];
    [Tekst setTag:120];
//    [Tekst setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //29-06-2017 changes
    [Tekst setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [Tekst setBackgroundColor:[UIColor clearColor]];
    [Tekst setNumberOfLines:3];
    [self.view addSubview:Tekst];


    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];


    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 104, self.view.frame.size.width-20, 44)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
//    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //29-06-2017 changes
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];



    underTitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 4, self.view.frame.size.width-40, 35)];
    [underTitleText setBackgroundColor:[UIColor whiteColor]];
    [underTitleText setDelegate:self];
    [underTitleText setText:[NSString stringWithFormat:@"   %@",NSLocalizedString(@"Minutes earlier",nil)]];
//    [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //29-06-2017 changes
    [underTitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
    [Combi addSubview:underTitleText];




    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(15, 4, self.view.frame.size.width-40, 35)];
    [TitleText setBackgroundColor:[UIColor clearColor]];
    [TitleText setDelegate:self];
    [TitleText setText:@""];
//    [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //29-06-2017 changes
    [TitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [TitleText setTag:123+1];


    [TitleText setFont:[UIFont boldSystemFontOfSize:15]];
    [Combi addSubview:TitleText];



    NSDate *now = [[NSDate alloc] init];


     NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
 
 
    [format setDateFormat:@"dd/MM/yyyy"];
    theDate = [format stringFromDate:now];


     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     
    [dateFormatter setDateFormat:@"EEEE"];
    
    theDay = [dateFormatter stringFromDate:now];


        


    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [timeFormat setDateFormat:@"hh:mm"];
    theTime = [timeFormat stringFromDate:now];



        

    NSDateFormatter *timeFormat3 = [[NSDateFormatter alloc] init];
     [timeFormat3 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [timeFormat3 setDateFormat:@"a"];
    
    timeframe = [timeFormat3 stringFromDate:now];


    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        NSLog(@"left");
        Combi.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        
        
        underTitleText.transform = CGAffineTransformMakeScale(-1, 1);
        [underTitleText setTextAlignment:NSTextAlignmentNatural];
        
        
        TitleText.transform = CGAffineTransformMakeScale(-1, 1);
        [TitleText setTextAlignment:NSTextAlignmentNatural];
        //textblock.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }
    


}

-(void)Cancel:(UIBarButtonItem*) sender

{



    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];

}


-(void)Save:(UIBarButtonItem*) sender

{
    [self saveTheChanges];
    return;
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Save the changes?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    alert.tag=5555;
//    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==5555) {
        if (buttonIndex==1) {
            [self saveTheChanges];
        }
    }
}

-(void)saveTheChanges
{
    [SaveData insertSetting:@"Early" setnames:TitleText.text];
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = myApp.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Settings"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count] >0) {
        
        Settings *newCourse =[fetchedObjects objectAtIndex:0];
        
        newCourse.early =TitleText.text;
        
        
    }
    
    
    
    
    
    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];

}

-(NSString*) convertToOurs:(NSString*)ours

{


    return [NSString stringWithFormat:@"%i", [ours intValue]/24];
}

-(void)change:(UIButton*)sender

{

    if (sender.tag ==140 ||sender.tag ==180) {


    }
    if (sender.tag ==141 ||sender.tag ==181) {

    }
    if (sender.tag ==142 ||sender.tag ==182) {


        TimesofDayViewController *controller = [[TimesofDayViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (sender.tag ==143 ||sender.tag ==183) {

    }
    if (sender.tag ==144 ||sender.tag ==184) {

    }

       




}

-(void)Action:(UIButton*)sender

{


}

-(void)changeChose:(NSString*)sender

{


    chosen = sender;


}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    [TitleText setKeyboardType:UIKeyboardTypeDecimalPad];
   
    TitleText.selectedRange = NSMakeRange([TitleText.text length], 0);
    [TitleText setEditable:YES];
    [TitleText becomeFirstResponder];
}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
        
        
         
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;


        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void)textViewDidChange:(UITextView *)textView
{

    if ([textView.text length]<4) {


        if ([textView.text length] ==0) {

            [underTitleText setText:[NSString stringWithFormat:@"    %@",NSLocalizedString(@"Minutes earlier",nil)]];
        }
        if ([textView.text length] ==1) {

            [underTitleText setText:[NSString stringWithFormat:@"     %@",NSLocalizedString(@"Minutes earlier",nil)]];
        }
        if ([textView.text length] ==2) {

            [underTitleText setText:[NSString stringWithFormat:@"      %@",NSLocalizedString(@"Minutes earlier",nil)]];
        }
        if ([textView.text length] ==3) {
            
            [underTitleText setText:[NSString stringWithFormat:@"        %@",NSLocalizedString(@"Minutes earlier",nil)]];
        }
        
        
        
    }
    else
        {
        [textView setText:@""];
        [underTitleText setText:[NSString stringWithFormat:@"   %@",NSLocalizedString(@"Minutes earlier",nil)]];
        }
    
    
    
    
    
    
    
    
}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  
    
    
}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {
    
    
    
	return YES;
}

-(void)hideAndseek:(NSString*)set
{
    
    
}

-(void)getParant:(UIViewController*)parant

{
    
    parantIt =(FrequencyViewController*)parant;
    
    
    
    
    
    
}




- (IBAction)SelectChoice:(UIButton *)sender {
    
    
    
    
    
}



@end
