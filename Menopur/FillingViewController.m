//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "FillingViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "CustumPickerRefill.h"
#import "PickerViewContent.h"
#import "TextViewData.h"
@interface FillingViewController ()

@end

@implementation FillingViewController
@synthesize parantIt;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize theTime;
@synthesize theDay;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize underTitleText;
@synthesize pickerTaal;
@synthesize textblock;



-(void)loadView{


    [super loadView];
}


-(void)whatValue:(NSString*)set

{
    
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
  
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
    [textblock.Content setText:set];
    [textblock.Value setText:appDelegate.dose_unit];
    
    [textblock changeValue];
    
    
    CompaireDecimal =NSLocalizedString([typit valueForKey:@"Dose_unit"],nil);
    
    
    NSArray *numbers2 = [CompaireDecimal componentsSeparatedByString:@","];
    
    if ([numbers2 count]>1) {
        
        
        PickerViewContent *content = [[PickerViewContent alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-180, 320, 180)];
       // [content setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        
        [content Decimal:CompaireDecimal pernt:self];
        
        [self.view addSubview:content];
        
      
        
        
    }
    else
    {
        PickerViewContent *content = [[PickerViewContent alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-180, 320, 180)];
       // [content setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        
        [content Decimal:CompaireDecimal pernt:self];
      
        [self.view addSubview:content];
        
      
    }
        
          [appDelegate.toolbarDown setAlpha:0];
      });

}



-(void)whatLabel:(UILabel*)set

{
    
  
    
    
    if ([set.text isEqualToString:@" "]) {
        
        
        
    }
    else
        
    {        
        
    }
    
    parantLabel =(TekstIDLabel*)set;
    
    


        
}
-(void)chosenValue:(NSString*)sender;


{
    
    [textblock.Content setText:sender];
    
    [textblock changeValue];
}

-(void)chosenMonth:(NSString*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.dose_unit = NSLocalizedString(sender,nil);
    [textblock.Value setText:NSLocalizedString(sender,nil)];
    
    [textblock changeValue];
    
    
    
}


- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];

    NSLog(@"%@", self);
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

      TimeChoses =[[NSMutableArray alloc]init];


    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];

    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Cancel:)];
    
    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    ////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Cancel setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }



    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];

    
    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
    
      UIView *textit  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];

   UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 160, 40)];
    [title setTextColor:[UIColor whiteColor]];
    [title setText:[typit valueForKey:@"Refill_name"]];
    
    //15-11
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
    {
        if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
        {
            title.text = [title.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mendge"];
        }
    }
    
    
    [title setFont:[UIFont boldSystemFontOfSize:12]];
    [title setNumberOfLines:2];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];
    [textit addSubview:title];


    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:textit];




    UIBarButtonItem *Save = [[UIBarButtonItem alloc]
                             initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleBordered target:self
                             action:@selector(Save:)];

    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Save setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }






    UIToolbar *toolbarUp = [[UIToolbar alloc] init];
    toolbarUp.frame = CGRectMake(0, -2, 320, 54);
    //[toolbarUp setBarTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
   
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        
        
//        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk"] forToolbarPosition:0 barMetrics:0]; //29-06-2017 changes
[toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forToolbarPosition:0 barMetrics:0];
        //balk_nav.png
    }
    
    else
    {
//        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk.png"] forToolbarPosition:0 barMetrics:0]; //29-06-2017 changes
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forToolbarPosition:0 barMetrics:0];

    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        
        
//       [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk"] forToolbarPosition:0 barMetrics:0]; //29-06-2017 changes
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forToolbarPosition:0 barMetrics:0];
        
    }
    
    else
    {
//        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk.png"] forToolbarPosition:0 barMetrics:0]; //29-06-2017 changes
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forToolbarPosition:0 barMetrics:0];

    }
    
    [toolbarUp setTranslucent:NO];
   [self.view addSubview:toolbarUp];


    NSArray *itemsUp = [NSArray arrayWithObjects:Cancel,flexItem,random,flexItem,Save,nil];
    toolbarUp.items = itemsUp;


    

    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];


    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 64, self.view.frame.size.width-20, 44)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
//    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];



    underTitleText = [[UILabel alloc] initWithFrame:CGRectMake(20, 4, self.view.frame.size.width-40, 35)];
    [underTitleText setBackgroundColor:[UIColor whiteColor]];
    [underTitleText setText:[NSString stringWithFormat:@"%@", [typit valueForKey:@"Refill_name"]]];
    
    //15-11
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
    {
        if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
        {
            underTitleText.text = [underTitleText.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mendge"];
        }
    }

    
//    [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [underTitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];


    [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
    [Combi addSubview:underTitleText];
    
    [underTitleText sizeToFit];
    
    [underTitleText setFrame:CGRectMake(20, 4, underTitleText.frame.size.width, 35)];

    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
    {
        [underTitleText setAlpha:1];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
        
    {
          [underTitleText setAlpha:0];
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
    {
        
        
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
    {
        [underTitleText setAlpha:0];
    }

    
    textblock = [[TextViewData alloc] initWithFrame:CGRectMake(0, 68,100, 35)];
    [self.view addSubview:textblock];

    NSDate *now = [[NSDate alloc] init];

     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     
    [dateFormatter setDateFormat:@"EEEE"];
    
    theDay = [dateFormatter stringFromDate:now];


   


    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [timeFormat setDateFormat:@"hh:mm"];
    theTime = [timeFormat stringFromDate:now];

   


    NSDateFormatter *timeFormat3 = [[NSDateFormatter alloc] init];
     [timeFormat3 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [timeFormat3 setDateFormat:@"a"];
    
    timeframe = [timeFormat3 stringFromDate:now];
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        NSLog(@"left");
        Combi.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        
        
        underTitleText.transform = CGAffineTransformMakeScale(-1, 1);
        [underTitleText setTextAlignment:NSTextAlignmentNatural];
        
        
        textblock.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }
    
    

    
   
}

-(void)Cancel:(UIBarButtonItem*) sender

{
    [self dismissViewControllerAnimated:YES completion:NULL];


}



-(void)Save:(UIBarButtonItem*) sender

{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    

    [parantIt setitemsDictionary:appDelegate.dose_unit name:@"Dose_unit"];
    [parantIt setitemsDictionary:textblock.Content.text name:@"Filling"];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
}

-(void)change:(UIButton*)sender

{

    if (sender.tag ==140 ||sender.tag ==180) {


    }
    if (sender.tag ==141 ||sender.tag ==181) {

    }
    if (sender.tag ==142 ||sender.tag ==182) {


        TimesofDayViewController *controller = [[TimesofDayViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (sender.tag ==143 ||sender.tag ==183) {

    }
    if (sender.tag ==144 ||sender.tag ==184) {

    }
    
   
  
  

    
}

-(void)Action:(UIButton*)sender

{

  
}

-(void)changeChose:(NSString*)sender

{


    chosen = sender;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

   
}


-(void) backButtonPressed
{

//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    ////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
    }
    
    else
    {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;


}

-(void) setTextview: (NSString *) text
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}
-(void)setChoice:(NSString*)gothere

{
    
}

-(void)textViewDidChange:(UITextView *)textView
{
    
   
 AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


      [underTitleText setText:[NSString stringWithFormat:@"%@", [[[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0] valueForKey:@"Refill_name"]]];




    

}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{
}

-(void)getParant:(UIViewController*)parant

{

    parantIt =(NewMedecineController*)parant;




  
    
}




- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
