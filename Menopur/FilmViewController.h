//
//  FilmViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 23-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilmViewController : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *TextLabel;
@property (nonatomic, retain) IBOutlet UIButton *Film_logo;
@property (nonatomic, retain) IBOutlet UIButton *Logo_pdf;
@property (nonatomic, retain) IBOutlet UIButton *info;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbarDown;
-(void) turnbuttons:(NSString*) setter;


@end
