//
//  FilmViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 23-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "FilmViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "InstructiefilmViewController.h"
#import "VragenViewController.h"
#import "BijsluiterViewController.h"
#import "AppDelegate.h"

@interface FilmViewController ()

@end

@implementation FilmViewController
@synthesize TextLabel;
@synthesize Film_logo;
@synthesize Logo_pdf;
@synthesize info;
@synthesize toolbarDown;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {


        NSLog(@"InstructiefilmViewController");


        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [a1 setTag:44];
        [a1 setFrame:CGRectMake(2, 2, 87/1.9, 59/1.9)];
        [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;



        UIImage *image = [UIImage imageNamed:@"Logo_bar.png"];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(0, 0,136, 34);

        self.navigationItem.titleView = imageView;


        UILabel *setbackground =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [setbackground setBackgroundColor:[UIColor colorWithRed:0.627 green:0.851 blue:0.961 alpha:1.000]];

        [self.view addSubview:setbackground];

        CAGradientLayer *shineLayer = [CAGradientLayer layer];
        shineLayer.frame = setbackground.layer.bounds;
        shineLayer.colors = [NSArray arrayWithObjects:
                             (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                             (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                             (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                             (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                             (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                             (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                             nil];
        shineLayer.locations = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:0.1f],
                                [NSNumber numberWithFloat:0.2f],
                                [NSNumber numberWithFloat:0.3f],
                                [NSNumber numberWithFloat:0.4f],
                                [NSNumber numberWithFloat:0.5f],
                                [NSNumber numberWithFloat:0.6f],
                                nil];



        [setbackground.layer addSublayer:shineLayer];


        
        toolbarDown = [[UIToolbar alloc] init];
        toolbarDown.frame = CGRectMake(0, self.view.frame.size.height-88, self.view.frame.size.width, 44);
        toolbarDown.tintColor = [UIColor colorWithRed:0.204 green:0.714 blue:0.816 alpha:1.000];




        UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:nil
                                                                                  action:nil];


        
        info = [UIButton buttonWithType:UIButtonTypeCustom];
        [info setTag:44];
        [info setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
        [info addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
        info.layer.cornerRadius = 2;
    [info setTag:22];
        [info setImage:[UIImage imageNamed:@"Info_Logo.png"] forState:UIControlStateNormal];
        UIBarButtonItem *info_button = [[UIBarButtonItem alloc] initWithCustomView:info];


        

        Logo_pdf = [UIButton buttonWithType:UIButtonTypeCustom];
        [Logo_pdf setTag:44];
        [Logo_pdf setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
        [Logo_pdf addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
          Logo_pdf.layer.cornerRadius = 2;
    [Logo_pdf setTag:23];
        [Logo_pdf setImage:[UIImage imageNamed:@"Logo_pdf.png"] forState:UIControlStateNormal];
        UIBarButtonItem *logo_button = [[UIBarButtonItem alloc] initWithCustomView:Logo_pdf];



            //Film_Logo.png


        Film_logo = [UIButton buttonWithType:UIButtonTypeCustom];
        [Film_logo setTag:44];
        [Film_logo setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
        [Film_logo addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
        Film_logo.layer.cornerRadius = 2;
    [Film_logo setTag:24];
        [Film_logo setImage:[UIImage imageNamed:@"Film_Logo.png"] forState:UIControlStateNormal];
        UIBarButtonItem *film_button = [[UIBarButtonItem alloc] initWithCustomView:Film_logo];
        
        
        NSArray *items2 = [NSArray arrayWithObjects:
                           film_button, flexItem,logo_button,flexItem,info_button,
                           nil];
        toolbarDown.items = items2;
        
        
        [self.view addSubview:toolbarDown];



        TextLabel= [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2, 6, 300, 60)];
        TextLabel.backgroundColor = [UIColor clearColor];
        [TextLabel setText: @"Kies uw dosering voor de videoinstructie"];
        TextLabel.font = [UIFont boldSystemFontOfSize:25];
        [TextLabel setNumberOfLines:3];
        [TextLabel setTextAlignment:NSTextAlignmentCenter];
        TextLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
        [TextLabel setBackgroundColor:[UIColor clearColor]];
        [self.view  addSubview:TextLabel];
        
        [TextLabel setAlpha:1];

        
    }
    return self;
}

-(void)PresentView:(UIButton*)sender

{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    for (int i =1; i < [[[self navigationController] viewControllers] count]; i++) {

        UIViewController *controller = [[[self navigationController] viewControllers] objectAtIndex:i];
        [[self navigationController] popToViewController:controller animated:NO];


    }




    int64_t delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

        if (sender.tag ==22) {



            VragenViewController *controller = [[VragenViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];

            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];






        }
        if (sender.tag ==23) {


            BijsluiterViewController *controller = [[BijsluiterViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];

            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];

        }
        if (sender.tag ==24) {



            InstructiefilmViewController *controller = [[InstructiefilmViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];
            
            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];
            
            
        }
        
        
    });
    
    
}

-(void) turnbuttons:(NSString*) setter

{
 NSLog(@"turnbuttons");
}

-(void) backAction
{

    
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
