//
//  InstructiefilmViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PDFScrollView;
@interface FolderViewController : UIViewController <UIScrollViewDelegate>


@property (nonatomic, retain) IBOutlet UILabel *MerkenLabel;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbarDown;
@property (nonatomic, retain) IBOutlet UILabel *setbackground;
@property (nonatomic, retain) PDFScrollView *thumbPdfView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollViewSpread;
@property (nonatomic, assign) CGPDFDocumentRef pdf1;
@property (nonatomic, assign) NSInteger pageNo;
@property (nonatomic, assign) CFURLRef pdfit;
@property (nonatomic, assign) int pageCount;


@property (nonatomic, assign) CGRect frameit;
@property (nonatomic, retain) IBOutlet UIButton *Facebook;
@property (nonatomic, retain) IBOutlet UIButton *Email;

-(void)setpdf:(CFURLRef)pdf setPage:(NSInteger)page;
-(void) turnbuttons:(NSString*) setter;

@end
