    //
    //  ViewController.h
    //  Menopur
    //
    //  Created by Jeffrey Snijder on 20-01-13.
    //  Copyright (c) 2013 Menopur. All rights reserved.
    //

#import <UIKit/UIKit.h>

@class CodePopView, FrequencyViewController, TekstIDLabel;
@interface FromToViewController : UIViewController<UITextViewDelegate, UIPickerViewDelegate> {

    UITextView *Textpicker;
    UIButton *Postpone;
    UIButton *Postpone2;
    UITextView *TitleText;
     UITextView *FromTitleText;
    NSString *Compaire;
    UIView *Combi;
    UIView *CombiOther;
    NSString *chosen;
    NSString *chosenID;
    NSString *theTime;
    NSString *theDay;
     NSString *theDate;
    UILabel *Tekst;
     UILabel *FromTekst;
    NSString *timeframe;
    NSString *timeframefrom;
   NSString *timeframeuntill;
    NSTimer *timer;
    UIDatePicker *pickerTaal;
  
}
@property (nonatomic, retain) NSString *theDate;
@property (nonatomic, retain) NSString *theTime;
@property (nonatomic, retain) NSString *theDay;
@property (nonatomic, retain) IBOutlet UITextView *TitleTextchose;
@property (nonatomic, retain) IBOutlet UILabel *FromTekst;
@property (nonatomic, retain) IBOutlet UITextView *FromTitleTextchose;
@property (nonatomic, retain) IBOutlet UITableView *table;
@property (nonatomic, retain) IBOutlet  NSMutableArray *TimeChoses;
@property (nonatomic, retain) IBOutlet NSString *timeframefrom;
@property (nonatomic, retain) IBOutlet NSString *timeframeuntill;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) FrequencyViewController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;

-(void)setChoice:(NSString*)gothere;
-(void)change:(UIButton*)sender;
-(void) setTextview: (NSString *) text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
-(void) addItem:(NSString*) sender;
-(NSString*)setstring:(NSString*)go;
@end

