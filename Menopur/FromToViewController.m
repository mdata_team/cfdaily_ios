    //
    //  ViewController.m
    //  Menopur
    //
    //  Created by Jeffrey Snijder on 20-01-13.
    //  Copyright (c) 2013 Menopur. All rights reserved.
    //

#import "FromToViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "TimesofdayCell.h"
#import "FrequencyViewControllerNew.h"
#import "SaveData.h"
#import "Settings.h"
@interface FromToViewController ()

@end

@implementation FromToViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize TimeChoses;
@synthesize table;
@synthesize TitleTextchose;
@synthesize theTime;
@synthesize theDay;
@synthesize theDate;
@synthesize timeframefrom;
@synthesize timeframeuntill;
@synthesize FromTekst;



-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    

    parantLabel =(TekstIDLabel*)set;
        
}

-(void)setChoice:(NSString*)gothere

{
    
}

- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];
    
    
    
    //     [self.view.layer setMasksToBounds:YES];
    
    TimeChoses =[[NSMutableArray alloc]init];
    
    
    
    pickerTaal = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-300, 320, 300)];
     BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
     if (!strsetting)
    {
        
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US_POSIX",nil)];
        pickerTaal.locale = locale;
        
    }
    else
        
    {
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)];
        pickerTaal.locale = locale;
        
    }
    
    pickerTaal.datePickerMode = UIDatePickerModeTime;
    [pickerTaal setTag:679];
    [pickerTaal addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:pickerTaal];
    

    
    
    NSDate *now = [[NSDate alloc] init];
	[pickerTaal setDate:now animated:YES];
    
    
    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    
    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Cancel:)];
    
    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Cancel setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }
    
    
    
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    
    UIView *textit  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 160, 20)];
    [title setTextColor:[UIColor whiteColor]];
    [title setText:@"Do not Disturb"];
    [title setFont:[UIFont boldSystemFontOfSize:15]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];
    [textit addSubview:title];
    
    
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:textit];
    
    
    
    
    UIBarButtonItem *Save = [[UIBarButtonItem alloc]
                             initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleBordered target:self
                             action:@selector(Save:)];
    
    
    
    
    
    UIToolbar *toolbarUp = [[UIToolbar alloc] init];
    toolbarUp.frame = CGRectMake(0, -2, 320, 54);
    
  
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        
        
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk"] forToolbarPosition:0 barMetrics:0];
        
    }
    
    else
    {
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk.png"] forToolbarPosition:0 barMetrics:0];
    }
    [toolbarUp setTranslucent:NO];
    [self.view addSubview:toolbarUp];
    
    
    NSArray *itemsUp = [NSArray arrayWithObjects:Cancel,flexItem,random,flexItem,Save,nil];
    toolbarUp.items = itemsUp;
    
    
    
    
    
    
    FromTekst = [[UILabel alloc] initWithFrame:CGRectMake(12, 68, 290, 20)];
    [FromTekst setTextColor:[UIColor clearColor]];
    [FromTekst setText:@"Do not Disturb from"];
    [FromTekst setFont:[UIFont boldSystemFontOfSize:16]];
    [FromTekst setTextAlignment:NSTextAlignmentLeft];
    [FromTekst setTag:120];
    [FromTekst setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    [FromTekst setBackgroundColor:[UIColor clearColor]];
    [FromTekst setNumberOfLines:3];
    [self.view addSubview:FromTekst];
    
    
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];

    
    timeframefrom= [[[GetData getSetting] valueForKey:@"Disturb_in_ad"] objectAtIndex:0];
    
    
    UIView *CombiChose = [[UIView alloc] initWithFrame:CGRectMake(10,  96, 300, 44)];
    [CombiChose setBackgroundColor:[UIColor whiteColor]];
    [CombiChose.layer setCornerRadius:10];
    [CombiChose.layer setBorderWidth:1];
    [CombiChose.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [CombiChose.layer setMasksToBounds:YES];
    [self.view addSubview:CombiChose];
    
    
    UIView *CombiChose2 = [[UIView alloc] initWithFrame:CGRectMake(10,  146, 300, 44)];
    [CombiChose2 setBackgroundColor:[UIColor whiteColor]];
    [CombiChose2.layer setCornerRadius:10];
    [CombiChose2.layer setBorderWidth:1];
    [CombiChose2.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [CombiChose2.layer setMasksToBounds:YES];
    [self.view addSubview:CombiChose2];
    
    
    
    UILabel *Starting = [[UILabel alloc] initWithFrame:CGRectMake(12, 4, 100, 35)];
    [Starting setTextColor:[UIColor clearColor]];
    [Starting setText:NSLocalizedString(@"Starting",nil)];
    [Starting setFont:[UIFont boldSystemFontOfSize:16]];
    [Starting setTextAlignment:NSTextAlignmentLeft];
    [Starting setTag:120];
    [Starting setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    [Starting setBackgroundColor:[UIColor clearColor]];
    [Starting setNumberOfLines:3];
    [CombiChose addSubview:Starting];
    
    
    UILabel *Ending = [[UILabel alloc] initWithFrame:CGRectMake(12, 4, 100, 35)];
    [Ending setTextColor:[UIColor clearColor]];
    [Ending setText:NSLocalizedString(@"Ending",nil)];
    [Ending setFont:[UIFont boldSystemFontOfSize:16]];
    [Ending setTextAlignment:NSTextAlignmentLeft];
    [Ending setTag:120];
    [Ending setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    [Ending setBackgroundColor:[UIColor clearColor]];
    [Ending setNumberOfLines:3];
    [CombiChose2 addSubview:Ending];
    
    
    
    
    FromTitleText = [[UITextView alloc] initWithFrame:CGRectMake(215, 102, 90, 35)];
    [FromTitleText setBackgroundColor:[UIColor whiteColor]];
    //[TitleText setDelegate:self];
    [FromTitleText setText:[[[GetData getSetting] valueForKey:@"Disturb_in"] objectAtIndex:0]];
    [FromTitleText setTextColor:[UIColor blackColor]];
    [FromTitleText setTag:123+1];
    FromTitleText.selectedRange = NSMakeRange(0, 0);
       [FromTitleText setDelegate:self];
    [FromTitleText setFont:[UIFont systemFontOfSize:15]];
    [self.view addSubview:FromTitleText];
    

    
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];

    
    
    timeframeuntill= [[[GetData getSetting] valueForKey:@"Disturb_in_ad"] objectAtIndex:0];
   
    
    TitleTextchose = [[UITextView alloc] initWithFrame:CGRectMake(215, 152, 90, 35)];
    [TitleTextchose setBackgroundColor:[UIColor whiteColor]];
    [TitleTextchose setText:[[[GetData getSetting] valueForKey:@"Disturb_out"] objectAtIndex:0]];
    [TitleTextchose setTextColor:[UIColor blackColor]];
    [TitleTextchose setTag:123+1];

    TitleTextchose.selectedRange = NSMakeRange(0, 0);
    [TitleTextchose setDelegate:self];
    [TitleTextchose setFont:[UIFont systemFontOfSize:15]];
    [self.view addSubview:TitleTextchose];
    
    
    
    
     NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
 
    [format setDateFormat:@"dd/MM/yyyy"];
    theDate = [format stringFromDate:now];
    
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     
    [dateFormatter setDateFormat:@"EEEE"];
    
    theDay = [dateFormatter stringFromDate:now];
   
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"a"];
    timeframefrom = [dateFormatter2 stringFromDate:pickerTaal.date];

    
    
    
    [Textpicker setText:theDay];
   
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
    timer =NULL;
}


-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    Textpicker = textView;
}
-(void)valueChanged:(UIDatePicker*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     
    [dateFormatter setDateFormat:[appDelegate convertString:@"hh:mm a"]];
    theDay = [dateFormatter stringFromDate:sender.date];
    
    
    if ([Textpicker isEqual:FromTitleText]) {
      
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"a"];
        timeframefrom = [dateFormatter2 stringFromDate:sender.date];
    }
    
    if ([Textpicker isEqual:TitleTextchose]) {
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"a"];
        timeframeuntill = [dateFormatter2 stringFromDate:sender.date];
    }
    


    
    [Textpicker setText:theDay];
    
    
    
}


-(void)viewDidAppear:(BOOL)animated
{

    
    //////////////     [self.view.layer setMasksToBounds:YES];


     [FromTitleText setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [FromTitleText setEditable:YES];
    [FromTitleText becomeFirstResponder];
}



-(void) addItem:(NSString*) sender
{
    
}

-(void)Cancel:(UIBarButtonItem*) sender

{




    //[parantIt getchoice:TimeChoses];
    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)Save:(UIBarButtonItem*) sender

{
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [SaveData insertSetting:@"Disturb_in" setnames:FromTitleText.text];
    [SaveData insertSetting:@"Disturb_out" setnames:TitleTextchose.text];
    
    [SaveData insertSetting:@"Disturb_in_ad" setnames:timeframefrom];
    [SaveData insertSetting:@"Disturb_out_ad" setnames:timeframeuntill];

    
    NSError *error = nil;
    NSManagedObjectContext *context = myApp.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Settings"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count] >0) {
        
        Settings *newCourse =[fetchedObjects objectAtIndex:0];
        
        
        newCourse.disturb_in =FromTitleText.text;
        newCourse.disturb_out =TitleTextchose.text;
        
        newCourse.disturb_in_ad =timeframefrom;
        newCourse.disturb_out_ad =timeframeuntill;
        
            ////////////////////////////////////////////////////NSLog(@"%@", newCourse);
        
        
    }
    
    if (![context save:&error]) {
        
    }
    else
        
    {
        
        
    }
    

    
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)Save2:(UIBarButtonItem*) sender

{
    

        [SaveData insertSetting:@"Disturb_in" setnames:[self setstring:FromTitleText.text]];
        [SaveData insertSetting:@"Disturb_out" setnames:[self setstring:TitleTextchose.text]];
        
        [SaveData insertSetting:@"Disturb_in_ad" setnames:timeframefrom];
        [SaveData insertSetting:@"Disturb_out_ad" setnames:timeframeuntill];
    
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSError *error = nil;
    NSManagedObjectContext *context = myApp.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Settings"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count] >0) {
  
    Settings *newCourse =[fetchedObjects objectAtIndex:0];
    
    
    newCourse.disturb_in =FromTitleText.text;
    newCourse.disturb_out =TitleTextchose.text;
    
    newCourse.disturb_in_ad =timeframefrom;
    newCourse.disturb_out_ad =timeframeuntill;
        
        
    }
    
    if (![context save:&error]) {
        
    }
    else
        
    {
        
        
    }
   

  [self dismissViewControllerAnimated:YES completion:NULL];
    
    
}


-(NSString*)setstring:(NSString*)go

{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
   
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     
    [dateFormatter setDateFormat:[appDelegate convertString:@"hh:mm a"]];
    NSDate *myDate = [[NSDate alloc] init];
    myDate = [dateFormatter dateFromString:go];
    
     NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
 
    [format setDateFormat:@"hh:mm"];
   
    
    return [format stringFromDate:myDate];
    
    
}

-(void)change:(UIButton*)sender

{

    if (sender.tag ==141 ||sender.tag ==181) {

    }
    if (sender.tag ==142 ||sender.tag ==182) {

    }
    if (sender.tag ==143 ||sender.tag ==183) {

    }
    if (sender.tag ==144 ||sender.tag ==184) {

    }


}

-(void)Action:(UIButton*)sender

{


}

-(void)changeChose:(NSString*)sender

{


       
    chosen = sender;


}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

       TitleText.selectedRange = NSMakeRange(0, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;


        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}



- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{


}

-(void)getParant:(UIViewController*)parant

{

    parantIt =(FrequencyViewController*)parant;


    
    
    
    
}





- (IBAction)SelectChoice:(UIButton *)sender {
    
    
    
    
    
}



@end
