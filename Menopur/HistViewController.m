//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "HistViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "NewProfileController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "HistoryViewController.h"
#import "NewMedecineController.h"
#import "SettingsViewcontroller.h"
#import <Social/Social.h>
#import "NHMailActivity.h"
#import "MedicineViewController.h"
#import "HistViewController.h"
#import "MedsViewController.h"
#import "InfoViewController.h"
#import "MultipleViewController.h"
#import "Profiles.h"
#import "actionButton.h"

@interface HistViewController ()

@end

@implementation HistViewController

@synthesize Vraagantwoord;
@synthesize table;
@synthesize controller;
@synthesize button;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    ////////////////////////NSLog(@"HistViewController");

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    self.navigationItem.hidesBackButton = YES;

    int64_t delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){


        [appDelegate.Navigaioncopy removeAllObjects];
        [appDelegate.Navigaioncopy addObject:self];
       
        [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];



    });

    self.navigationItem.title =NSLocalizedString(@"History",nil);
    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
    // this will appear as the title in the navigation bar
     UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    // ^-Use NSTextAlignmentCenter for older SDKs.
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
          [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
    label.text = self.navigationItem.title;
    [label sizeToFit];
        
    }
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


    Vraagantwoord =[[NSMutableArray alloc]init];





    table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0, 320, self.view.bounds.size.height-88)];
    table.separatorColor = [UIColor clearColor];
    table.backgroundColor = [UIColor clearColor];
    table.rowHeight =120;
    table.delegate = self;
    table.dataSource = self;
    table.layer.shadowOffset = CGSizeMake(3, 0);
    table.layer.shadowOpacity = 2;
    table.layer.shadowRadius = 2.0;
    [table setEditing:NO];
    [table setEditing:NO];
    [self.view addSubview:table];
      [table release];



    
    
    if ([MFMailComposeViewController canSendMail])
        button.enabled = YES;
  

   [appDelegate seeIfNotification];
    [appDelegate.toolbarDown setAlpha:1];
    
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.managedObjectContext = myApp.managedObjectContext;
    
    
    
    NSError *error =nil;
    
    if (![[self fetchedResultsController] performFetch:&error])
    {
        
        abort();
        
        
    }
    
    table.delegate = self;
    table.dataSource = self;
    
    [table reloadData];


  
}


-(void)Next:(UIButton*)sender

{

    if (sender.tag ==31) {
        StartViewController *controller3 = [[StartViewController alloc]init];
        [self.navigationController pushViewController:controller3 animated:NO];
    
    }
    if (sender.tag ==32) {


      

    }
    if (sender.tag ==33) {


        MedsViewController *controller3 = [[MedsViewController alloc]init];
        [self.navigationController pushViewController:controller3 animated:NO];

       
    }

    if (sender.tag ==34) {

        [self Offerte];

    }
    if (sender.tag ==35) {


            //MedicineViewController.h
    }

    if (sender.tag ==36) {

        SettingsViewcontroller *controller4 = [[SettingsViewcontroller alloc]init];
        [self.navigationController pushViewController:controller4 animated:NO];
    }

    
}




-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{


    
    
    if (index==1) {



    }

    else
        {



        
        }
        }

-(void) CreateAccount

{

  

   

}


- (void) viewDidAppear:(BOOL)animated {

    
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [appDelegate seeIfNotification];
    [appDelegate.toolbarDown setAlpha:1];
    
 
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    
    
    NSError *error =nil;
    
    if (![[self fetchedResultsController] performFetch:&error])
    {
        
        abort();
        
        
    }
    
    table.delegate = self;
    table.dataSource = self;
    
    [table reloadData];



    
}
-(void)setNew

{
    
}



-(void)PresentView:(actionButton*)sender

{
    
    ////////////////////////////NSLog(@"%@", sender.chosenCourse);
    
    
    HistoryViewController *controller2 = [[HistoryViewController alloc]init];
    [self.navigationController pushViewController:controller2 animated:YES];
    
    [controller2 setItems:sender.chosenCourse];
    
    [controller2 release];

    
    
}


- (IBAction)Cancel:(UIButton *)sender {


}

- (IBAction)OK:(UIButton *)sender {

 
   
 
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{

    return YES;
}


-(void)setParent

{

   
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}




- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self becomeFirstResponder];
	[self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)popview

{
    
     
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    
    InfoViewController *controller2 = [[InfoViewController alloc]init];
    [self presentViewController:controller2 animated:YES completion:Nil];
}





- (void)Offerte

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];


    
    
    
}


#pragma mark - Fetched Result Controller section



-(NSFetchedResultsController*) fetchedResultsController
{
    
    if (_fetchedResultsController !=nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                                   ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    
    _fetchedResultsController =[[NSFetchedResultsController alloc]initWithFetchRequest:fetchRequest managedObjectContext:[self managedObjectContext] sectionNameKeyPath:@"name" cacheName:nil];
    
    _fetchedResultsController.delegate =self;
    
    return _fetchedResultsController;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}


#pragma mark - Table view delegate





-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller


{
    
    [table beginUpdates];
    
    
   
    
    
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller


{
    [table endUpdates];
    
    
    
}

-(void) controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [table insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [table deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            break;
        case NSFetchedResultsChangeMove:
            break;
    }
}


-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = table;
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate: {
            Profiles *changedCourse = [self.fetchedResultsController objectAtIndexPath:indexPath];
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.textLabel.text = changedCourse.id;
        }
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    id<NSFetchedResultsSectionInfo> secInfo =[[self.fetchedResultsController sections] objectAtIndex:section];
    
    return [secInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileCell *cell = (ProfileCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[ProfileCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    
    
    Profiles *cours =[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    
    
    [cell FillAllItems:cours];
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    [cell setTag:indexPath.row +200];
    
    [cell getparent:self];
    
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    
    
    
    
    UILabel *sectionHeader2;
    
    
    
   
    sectionHeader2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 15)];
    sectionHeader2.backgroundColor = [UIColor whiteColor];
    sectionHeader2.textColor = [UIColor whiteColor];
    sectionHeader2.font = [UIFont boldSystemFontOfSize:16];
    sectionHeader2.text = [[[self.fetchedResultsController sections] objectAtIndex:section]name];
    sectionHeader2.textAlignment=NSTextAlignmentCenter;
    
    [sectionHeader2 setHidden:NO];
    
    return sectionHeader2;
    
    
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section

{
    
    
    
    return [[[self.fetchedResultsController sections] objectAtIndex:section]name];
    
    
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSManagedObjectContext *contex =self.managedObjectContext;
        Profiles *cours =[self.fetchedResultsController objectAtIndexPath:indexPath];
        [contex deleteObject:cours];
        
        NSError *error = nil;
        NSManagedObjectContext *context = self.managedObjectContext;
        if (![context save:&error]) {
            
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{

}


@end
