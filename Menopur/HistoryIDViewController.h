//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "History.h"
#import "Medicine.h"
#import "Profiles.h"
//#import "ZipArchive/Main.h"

@class CodePopView, Monthview;
@interface HistoryIDViewController : UIViewController<UITextViewDelegate, UIAlertViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate,UITableViewDelegate, MFMailComposeViewControllerDelegate, UISearchBarDelegate>


@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, assign, getter=isShouldBeginEditing) BOOL shouldBeginEditing;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerCopy;
@property (strong, nonatomic) IBOutlet UITableView *TableViewTime;

@property (strong, nonatomic) IBOutlet UINavigationBar *Navigation;
@property (nonatomic, strong) IBOutlet UILabel *Chosen;

@property (nonatomic, strong) History *currenCourse;



@property (nonatomic, retain) NSMutableDictionary *make;
@property (nonatomic, retain) Monthview *monthView;
@property (nonatomic, retain) UILabel *NameLabel;
@property (nonatomic, retain) UIImageView *headShot;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) NSMutableArray *HistSectionList;
@property (nonatomic, retain) NSMutableArray *SectionList;
@property (nonatomic, assign) NSString *SetOrder;
@property (nonatomic, assign) NSString *ordering;
@property (nonatomic, assign)NSString *MedID;
@property (nonatomic, retain) UILabel *sectionHeader;

@property (nonatomic, retain) UIButton *MonthButton;


@property (nonatomic, retain) NSMutableArray *DateResult;
@property (nonatomic, retain) NSMutableArray *HistSectionListDate;
@property (nonatomic, retain) NSMutableArray  *DateList;
@property (nonatomic, retain) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UISearchBar *searbarTo;

@property (nonatomic, retain) UIButton *button;

@property (nonatomic, retain) NSString *quantityString;
@property (nonatomic, retain) NSString *remainingString;
@property (nonatomic, retain) NSString *strengthString;

@property (nonatomic, retain) UIView *ProfileView;
@property (nonatomic, retain) NSString *chosenID;
- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)setItems:(Medicine*)set setPros:(Profiles*)pro;
-(void)chosenMonth:(NSString*)sender;
-(void)AddItems:(NSString*)set;
-(void)viewanimate;
@end
