//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "HistoryIDViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "NewProfileController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "HistoryCell.h"
#import "Monthview.h"
#import "InfoViewController.h"
#import "MultipleViewController.h"
#import "HistoryViewController.h"
#import "SaveCore.h"

@interface HistoryIDViewController ()

@end

@implementation HistoryIDViewController
@synthesize sectionHeader;
@synthesize MonthButton;
@synthesize NameLabel;
@synthesize headShot;
@synthesize Vraagantwoord;
@synthesize table;
@synthesize make;
@synthesize chosenID;
@synthesize ProfileView;
@synthesize monthView;
@synthesize button;
@synthesize SectionList;
@synthesize DateList;
@synthesize DateResult;
@synthesize HistSectionList;
@synthesize MedID;
@synthesize HistSectionListDate;
@synthesize SetOrder;
@synthesize ordering;
@synthesize TableViewTime;
@synthesize Navigation;
@synthesize currenCourse;
@synthesize Chosen;
@synthesize searbarTo;
@synthesize shouldBeginEditing;
@synthesize fetchedResultsControllerCopy;
@synthesize quantityString;
@synthesize remainingString;
@synthesize strengthString;




- (void)viewDidLoad
{
    
//    //***************** googleAnalyticsGroupping ******************
//    
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:3]value:@"Medicine History"];
//
//    //***************** googleAnalyticsGroupping ******************
    
    //********** Google Analytics ****************
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"H_Medicine History_iOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //********** Google Analytics ****************
    
    
    
    [SHARED_APPDELEGATE setNavigationbarIconImage:self.navigationController setimge:NO];
    
    ordering  =NSLocalizedString(@"By Date",nil);
    
    
    
    ////////////////////////NSLog(@"HistoryIDViewController");
    self.navigationItem.title =NSLocalizedString(@"Medicine History",nil);
    
    
    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    
    
    HistSectionListDate =[[NSMutableArray alloc]init];
    
    
    SetOrder = @"Name";
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 60, 320, self.view.bounds.size.height-30-130)];
    table.separatorColor = [UIColor grayColor];
    table.backgroundColor = [UIColor clearColor];
    table.rowHeight =80;
    [table setSeparatorInset:UIEdgeInsetsZero];
    [table setTag:89788];
    [table setEditing:NO];
    [table setEditing:NO];
    [self.view addSubview:table];
    
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        //NSLog(@"left");
        
        table.transform = CGAffineTransformMakeScale(-1, 1);
        
    }
    else
    {
        
    }
    
    
    
    sectionHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, 320, 20)];
    sectionHeader.backgroundColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.font = [UIFont boldSystemFontOfSize:16];
    sectionHeader.text =  NSLocalizedString(@"Medicines Sorted By Name", nil);
    sectionHeader.textAlignment=NSTextAlignmentCenter;
    
    sectionHeader.layer.shadowOffset = CGSizeMake(3, 0);
    sectionHeader.layer.shadowOpacity = 1;
    sectionHeader.layer.shadowRadius = 1.0;
    [self.view addSubview:sectionHeader];
    
    
    
    ProfileView = [[UIView alloc] initWithFrame:CGRectMake(-5, 0, 330, 60)];
    
    
    [self.view addSubview:ProfileView];
    
    
    
    
    
    headShot = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, 40, 40)];
//    [headShot setBackgroundColor:[UIColor blackColor]];
    [headShot setTag:130];
    [headShot setImage:[UIImage imageNamed:@"headshot"]];
    [ProfileView addSubview:headShot];
    
    
    NameLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 20, 210, 20)];
//    [NameLabel setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //29-06-2017 changes
    [NameLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [NameLabel setFont:[UIFont boldSystemFontOfSize:16]];
    [NameLabel setTag:100];
    [NameLabel setBackgroundColor:[UIColor clearColor]];
    [NameLabel setNumberOfLines:3];
    [ProfileView addSubview:NameLabel];
    
    
    monthView = [[Monthview alloc] initWithFrame:CGRectMake(220, 20,80,22)];
    [monthView setBackgroundColor:[UIColor colorWithRed:0.114 green:0.533 blue:0.576 alpha:1.000]];
    
    [monthView setParant:self];
    [monthView.layer setBorderColor:[UIColor colorWithRed:0.349 green:0.565 blue:0.965 alpha:1.000].CGColor];
    [monthView.layer setMasksToBounds:YES];
    [monthView.layer setCornerRadius:4];
    [monthView.layer setBorderWidth:1.5];
    [monthView.layer setBorderWidth:1];
    [monthView.layer setCornerRadius:10];
    [self.view addSubview:monthView];
    
    
    MonthButton = [[UIButton alloc] initWithFrame:CGRectMake(200, 20,100,22)];
    [MonthButton setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    [MonthButton addTarget:self action:@selector(viewanimate) forControlEvents:UIControlEventTouchUpInside];
    [MonthButton.titleLabel setFont:[UIFont boldSystemFontOfSize:10]];
    [MonthButton setTitle:NSLocalizedString(@"By Name", nil) forState:UIControlStateNormal];
    [MonthButton setBackgroundImage:[UIImage imageNamed:@"Month_view.png"] forState:UIControlStateNormal];
    [self.view addSubview:MonthButton];
    
    
    CAGradientLayer *shineLayer1 = [CAGradientLayer layer];
    shineLayer1.frame = MonthButton.layer.bounds;
    shineLayer1.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:0.2f alpha:0.4f].CGColor,
                          nil];
    shineLayer1.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.2f],
                             [NSNumber numberWithFloat:0.4f],
                             [NSNumber numberWithFloat:0.6f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];
    [MonthButton.layer setCornerRadius:4];
    [MonthButton.layer setBorderWidth:1.5];
    [MonthButton.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [MonthButton.layer addSublayer:shineLayer1];
    
    
    NSArray *profileItem =[NSArray arrayWithObjects:@"Name",@"ID",@"Image",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Silence",@"Vibrate",nil];
    
    
    make =[[NSMutableDictionary alloc]init];
    
    make = [[SaveCore getProfiles:profileItem] objectAtIndex:0];
    
    
    
    
    
    if ([[SaveCore getProfiles:profileItem] count] ==1) {
        
        self.navigationItem.hidesBackButton = YES;
        
        
        chosenID = [make valueForKey:@"ID"];
        
        
        
        UIColor *colorit = [UIColor colorWithRed:[[make valueForKey:@"ColorRed"] floatValue] green:[[make valueForKey:@"ColorGreen"]  floatValue] blue:[[make valueForKey:@"Colorblue"]  floatValue] alpha:1];
        
        
        
        
        [ProfileView setBackgroundColor:colorit];
        ProfileView.layer.shadowOffset = CGSizeMake(3, 0);
        ProfileView.layer.shadowOpacity = 2;
        ProfileView.layer.shadowRadius = 2.0;
        [NameLabel setText:[make valueForKey:@"Name"]];
        [headShot setImage:[make valueForKey:@"Image"]];
        
//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        ////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:14];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            [label sizeToFit];
            
            [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
            
            // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:14];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            [label sizeToFit];
            
            
            [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
        
    }
    
    else
    {
        
//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        ////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:20.0];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            [label sizeToFit];
            
            [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
            [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
        
    }
    
    //34 × 31 pixels
    
    UIButton *a2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a2 setTag:44];
    [a2 setFrame:CGRectMake(20, 2, 34, 31)];
    [a2 addTarget:self action:@selector(showalert) forControlEvents:UIControlEventTouchUpInside];
    [a2 setImage:[UIImage imageNamed:@"Save_hist.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random2 = [[UIBarButtonItem alloc] initWithCustomView:a2];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = random2;
    
    
    if ([MFMailComposeViewController canSendMail])
        button.enabled = YES;
    
    
    
    [super viewDidLoad];
    
    
}


-(void)AddItems


{
    
    
}

-(NSFetchedResultsController*) fetchedResultsController
{
    
    if (_fetchedResultsController !=nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"History"
                                        inManagedObjectContext:[self managedObjectContext]]];
    
    
    
    NSString *query = [NSString stringWithFormat:@"idmedicine contains [cd]'%@'", MedID];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:query];
    
    
    [fetchRequest setPredicate:predicate];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                                   ascending:NO];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"originaltime" ascending:NO];
    NSSortDescriptor *sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"timeaction"
                                                                    ascending:NO];
    NSArray *sortDescriptors = nil;
    sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor,sortDescriptor2,sortDescriptor3, nil];
    
    
    
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    
    _fetchedResultsController =[[NSFetchedResultsController alloc]initWithFetchRequest:fetchRequest managedObjectContext:[self managedObjectContext] sectionNameKeyPath:@"name" cacheName:nil];
    
    _fetchedResultsController.delegate =self;
    
    fetchedResultsControllerCopy=_fetchedResultsController;
    
    return _fetchedResultsController;}





-(void)viewanimate

{
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];
    
    
    
    
    if (monthView.frame.size.height == 22) {
        [monthView setFrame:CGRectMake(220, 20,80,22*4)];
        [monthView.table setFrame:CGRectMake(0.0, 22, 80, 22*3)];
        
    }
    else
    {
        [monthView setFrame:CGRectMake(220, 20,80,22)];
        [monthView.table setFrame:CGRectMake(0.0, 22, 80, 22)];
        
    }
    
    [UIView commitAnimations];
    
    
    
}








-(void) backAction
{
    
    
    [[self navigationController] popViewControllerAnimated:YES];
}



-(void)showalert
{
    
    [self addActionForGoogleAnalytics:@"Medicine History – Share"];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Send History in Email\nor in CSV file",nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertEmail = [UIAlertAction actionWithTitle:NSLocalizedString(@"Email",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self addActionForGoogleAnalytics:@"Medicine History - Email"];
        [self Makepdf];
    }];
    UIAlertAction *alertCSV = [UIAlertAction actionWithTitle:NSLocalizedString(@"CSV",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self addActionForGoogleAnalytics:@"Medicine History - CSV "];
        [self MakeCSV];
    }];
    
    [alert addAction:alertEmail];
    [alert addAction:alertCSV];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SHARED_APPDELEGATE window].rootViewController presentViewController:alert animated:YES completion:nil];
        
    });
}


/* //26-06-2017 change AlertController insted of Alertview
-(void)showalert

{
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:NSLocalizedString(@"Send History in Email\nor in CSV file",nil)];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464749];
    [alert addButtonWithTitle:NSLocalizedString(@"Email",nil)];
    [alert addButtonWithTitle:NSLocalizedString(@"CSV",nil)];
    [alert show];
}
*/

/*
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{
    
    
    if (index==1) {
        
        
        [self MakeCSV];
        
    }
    
    else
    {
        
        [self Makepdf];
        
        
    }
}
*/

-(void) CreateAccount

{
    NewProfileController *controller = [[NewProfileController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
    
}




-(void)setItems:(Medicine*)set setPros:(Profiles*)pro
{
    
    
    MedID =set.id_medicine;
    
    [table setFrame:CGRectMake(0.0, 60, 320, self.view.bounds.size.height-120)];
    [sectionHeader setAlpha:0];
    
    [monthView setAlpha:0];
    [MonthButton setAlpha:0];
    SetOrder =@"Name";
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.managedObjectContext = myApp.managedObjectContext;
    NSError *error =nil;
    
    if (![[self fetchedResultsController] performFetch:&error])
    {
        
        abort();
        
        
    }
    
    
    
    
    
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    ////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
    }
    
    else
    {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
    
    
    
    chosenID = set.id;
    
    
    UIColor *colorit = [UIColor colorWithRed:[pro.colorRed floatValue] green:[pro.colorGreen  floatValue] blue:[pro.colorblue  floatValue] alpha:1];
    
    
    
    
    
    
    [ProfileView setBackgroundColor:colorit];
    ProfileView.layer.shadowOffset = CGSizeMake(3, 0);
    ProfileView.layer.shadowOpacity = 2;
    ProfileView.layer.shadowRadius = 2.0;
    [NameLabel setText:pro.name];
    [headShot setImage:[UIImage imageWithData:pro.image]];
    
    
    
    table.delegate = self;
    table.dataSource = self;
    [table reloadData];
    
    
}


- (void)Offerte

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    
    
    
    
    
}










-(void)viewDidAppear:(BOOL)animated
{
    
    //////////////////////     [self.view.layer setMasksToBounds:YES];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate seeIfNotification];
    [appDelegate.toolbarDown setAlpha:1];
    
}







- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - chosenMonth

-(void)chosenValue:(NSString*)sender;


{
    
}

-(void)chosenMonth:(NSString*)sender

{
    
    
    
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];
    
    
    
    
    
    if ([sender isEqualToString:[NSLocalizedString(@"By Name", nil) lowercaseString]]) {
        
        
        //MedID
        
        ordering  =[NSLocalizedString(@"By Date",nil)  lowercaseString];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        [fetchRequest setEntity:[NSEntityDescription entityForName:@"History"
                                            inManagedObjectContext:[self managedObjectContext]]];
        
        
        
        NSSortDescriptor *sortDescriptor = nil;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                     ascending:YES];
        NSArray *sortDescriptors = nil;
        sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        
        
        
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        // Edit the section name key path and cache name if appropriate.
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[self managedObjectContext] sectionNameKeyPath:@"name" cacheName:nil];
        
        fetchedResultsControllerCopy =_fetchedResultsController;
        aFetchedResultsController.delegate = self;
        _fetchedResultsController = aFetchedResultsController;
        
        
        NSError *error = nil;
        if (![[self fetchedResultsController] performFetch:&error])
        {
            
            abort();
        }
        
        
        
        [table reloadData];
        
        
        
        
        
    }
    else if ([[sender lowercaseString] isEqualToString:[NSLocalizedString(@"By Date",nil) lowercaseString]])
        
    {
        
        
        ordering  =NSLocalizedString(@"By Date",nil);
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        [fetchRequest setEntity:[NSEntityDescription entityForName:@"History"
                                            inManagedObjectContext:[self managedObjectContext]]];
        
        
        
        
        
        NSSortDescriptor *sortDescriptor = nil;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"originaltime"
                                                     ascending:NO];
        NSArray *sortDescriptors = nil;
        sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        
        
        
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        // Edit the section name key path and cache name if appropriate.
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[self managedObjectContext] sectionNameKeyPath:@"name" cacheName:nil];
        
        aFetchedResultsController.delegate = self;
        _fetchedResultsController = aFetchedResultsController;
        
        
        NSError *error = nil;
        if (![[self fetchedResultsController] performFetch:&error])
        {
            
            abort();
        }
        
        [table reloadData];
        
        
        
    }
    
    
    [UIView commitAnimations];
    
    
    
    
    
}



#pragma mark - navigation

-(void) turnbuttons:(NSString*) setter

{
}



-(void)PresentView:(UIButton*)sender
{
    
    
    
    
    HistoryViewController *controller2 = [[HistoryViewController alloc]init];
    [self.navigationController pushViewController:controller2 animated:YES];
    
    if ([[Vraagantwoord valueForKey:@"ID"] containsObject:sender.titleLabel.text]) {
        [controller2 setItems:[Vraagantwoord objectAtIndex:[[Vraagantwoord valueForKey:@"ID"] indexOfObject:sender.titleLabel.text]]];
        
    }
    
    
    
    
}

- (IBAction)instructie:(UIButton *)sender {
    
    
    
}

- (IBAction)Vragen:(UIButton *)sender {
    
    
}


- (IBAction)bijsluiter:(UIButton *)sender {
    
    
    
}

- (IBAction)about:(UIButton *)sender {
    
    
    
}


- (IBAction)Cancel:(UIButton *)sender {
    
    
}

- (IBAction)OK:(UIButton *)sender {
    
    
    
    
}

#pragma mark - textView


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


#pragma mark - Makemail



- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self becomeFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void) MakeCSV
{
    //********** Google Analytics ****************
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"M_List of Medicines email button_iOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //********** Google Analytics ****************
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Medicine History",nil)]) {
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:NSLocalizedString(@"CFDaily Medicine History Data",nil)];
        
        
        NSError *error = nil;
        NSManagedObjectContext *context = self.managedObjectContext;
        
        
        
        
        
        
        
        NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"id_medicine = %@", MedID];
        NSFetchRequest *fetchRequest3 = [[NSFetchRequest alloc] init];
        [fetchRequest3 setEntity:[NSEntityDescription entityForName:@"Medicine" inManagedObjectContext:[appDelegate managedObjectContext]]];
        [fetchRequest3 setPredicate:predicate3];
        
        
        NSArray *fetchedObjects3 = [context executeFetchRequest:fetchRequest3 error:&error];
        
        
        NSMutableString *mainStringextra=[[NSMutableString alloc]initWithString:@""];
        
        
        for (NSInteger i =0; i < [fetchedObjects3 count]; i++) {
            
            
            Medicine *newCourse = [fetchedObjects3 objectAtIndex:i];
            
            
            
            
            float total = ([self calculculateWithoutPostponed:newCourse.id_medicine] *100)-(((long)[self calculculate:newCourse.id_medicine] *100));
            float pcntYes = ([self calculculateWithoutPostponed:newCourse.id_medicine] *100);
            float pcntNo = total/pcntYes;
            
            /*
             
             ·         French: Niveau d’observance
             ·         German:  Angabe der Therapietreue in Prozent
             ·         Dutch:  Therapietrouw percentage
             
             */
            
            if (i==0) {
                
                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
                {
                    [mainStringextra appendFormat:@"\"Medicine Name\",\"Adherence Percentage\""];
                    [mainStringextra appendFormat:@"\n"];
                }
                //07-09-2017
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                {
                    [mainStringextra appendFormat:@"\"Lægemiddelnavn\",\"Adherence Percentage\""];
                    [mainStringextra appendFormat:@"\n"];
                }
                //07-09-2017
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                {
                    [mainStringextra appendFormat:@"\"Medisinnavn\",\"Adherensprosent\""];
                    [mainStringextra appendFormat:@"\n"];
                }

                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
                {
                    [mainStringextra appendFormat:@"\"Nom de Médicament\",\"Niveau d’observance\""];
                    [mainStringextra appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
                {
                    [mainStringextra appendFormat:@"\"Medizin Name\",\"Angabe der Therapietreue in Prozent\""];
                    [mainStringextra appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
                {
                    [mainStringextra appendFormat:@"\"Ονομασία Φαρμάκου\",\"Ποσοστό Συμμόρφωσης\""];
                    [mainStringextra appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
                {
                    [mainStringextra appendFormat:@"\"Medicijn naam\",\"Therapietrouw percentage\""];
                    [mainStringextra appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
                {
                    [mainStringextra appendFormat:@"\"שם התרופה\",\"אחוז היענות לטיפול\""];
                    [mainStringextra appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
                    
                {
                    [mainStringextra appendFormat:@""];
                    [mainStringextra appendFormat:@"\n"];
                    
                }
                
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
                    
                {
                    [mainStringextra appendFormat:@"\"Nome farmaco\",\"Percentuale di adesione\""];
                    [mainStringextra appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
                {
                    [mainStringextra appendFormat:@"\"Nombre del medicamento\",\"Porcentaje de cumplimiento terapéutico\""];
                    [mainStringextra appendFormat:@"\n"];
                }
                
                
                NSString *string=newCourse.name;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainStringextra appendFormat:@"\"%@\"",string];
                
                
                string=[NSString stringWithFormat:@"%.2f%%", (pcntNo*100)];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainStringextra appendFormat:@",\"%@\"",string];
                
                
                
                
                
                [mainStringextra appendFormat:@"\n"];
                
                
            }
            else
                
            {
                
                NSString *string=newCourse.name;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainStringextra appendFormat:@"\"%@\"",string];
                
                
                string=[NSString stringWithFormat:@"%.2f%%", (pcntNo*100)];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainStringextra appendFormat:@",\"%@\"",string];
                
                
                
                
                [mainStringextra appendFormat:@"\n"];
                
            }
        }
        
        
        NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
        
        
        NSString *documentsDir2 = [paths2 objectAtIndex:0];
     
        
        NSData* settingsDataExtra;
        
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
        {
            
            
            NSString *documentsDirectoryPath2 = [NSString stringWithFormat:@"%@/Caches/",documentsDir2];
            NSString *filePath2 = [documentsDirectoryPath2  stringByAppendingPathComponent:NSLocalizedString(@"ProfileHistorySummary.text",nil)];
            
  
            
            
        settingsDataExtra = [mainStringextra dataUsingEncoding: NSUTF8StringEncoding];
        
        ////NSLog(@"%@", filePath2);
        
        [mainStringextra writeToFile:filePath2 atomically:YES encoding:NSUTF8StringEncoding error:NULL];
            
        }
        
        else
        {
            
            NSString *documentsDirectoryPath2 = [NSString stringWithFormat:@"%@/Caches/",documentsDir2];
            NSString *filePath2 = [documentsDirectoryPath2  stringByAppendingPathComponent:NSLocalizedString(@"ProfileHistorySummary.csv",nil)];
       
            
           settingsDataExtra = [mainStringextra dataUsingEncoding: NSUTF8StringEncoding];
            
            ////NSLog(@"%@", filePath2);
            
            [mainStringextra writeToFile:filePath2 atomically:YES encoding:NSUTF8StringEncoding error:NULL];
        }
        
        
        
        
        
        
        
        
        
        NSMutableString *mainString=[[NSMutableString alloc]initWithString:@""];
        
        
        
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        [fetchRequest setEntity:[NSEntityDescription entityForName:@"History"
                                            inManagedObjectContext:[self managedObjectContext]]];
        
        
        
        
        
        
        
        NSString *query = [NSString stringWithFormat:@"idmedicine contains [cd]'%@'", MedID];
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:query];
        
        
        [fetchRequest setPredicate:predicate];
        
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                                       ascending:NO];
        NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"originaltime" ascending:YES];
        NSSortDescriptor *sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"timeaction"
                                                                        ascending:NO];
        NSArray *sortDescriptors = nil;
        sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor,sortDescriptor2,sortDescriptor3, nil];
        
        
        
        
        
        
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        NSDateFormatter *set = [[NSDateFormatter alloc] init];
        [set setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [set setDateFormat:@"dd/MM/yyyy"];
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"hh:mm a"]];
        
        
        for (NSInteger i=0; i < [fetchedObjects count]; i++) {
            
            
            
            
            
            History *newCourse = [fetchedObjects objectAtIndex:i];
            
            
            
            
            
            if (i==0) {
                
                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
                {
                    [mainString appendFormat:@"\"Scheduled Date\",\"Scheduled Time\",\"Medicine Name\",\"Strength\",\"Dose\",\"Instructions\",\"Action Date\",\"Action Time\",\"Action\""];
                    [mainString appendFormat:@"\n"];
                }
                //07-09-2017
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                {
                    [mainString appendFormat:@"\"Aftalt dato\",\"Aftalt tid\",\"Lægemiddelnavn\",\"Styrke\",\"Dosis\",\"Instruktioner\",\"Handlingsdato\",\"Handlingstidspunkt\",\"Handling\""];
                    [mainString appendFormat:@"\n"];
                }
                //07-09-2017
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                {
                    [mainString appendFormat:@"\"Planlagt dato\",\"Planlagt tid\",\"Medisinnavn\",\"Styrke\",\"Dose\",\"Instruksjoner\",\"Handlingsdato\",\"Handlingstid\",\"Handling\""];
                    [mainString appendFormat:@"\n"];
                }

                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
                {
                    [mainString appendFormat:@"\"Date prévue\",\"Heure prévue\",\"Nom de Médicament\",\"Dosage\",\"Quantité\",\"Indications\",\"Date Actions\",\"Heure Actions\",\"Action\""];
                    [mainString appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
                {
                    [mainString appendFormat:@"\"Medizin Geschichte\",\"Geplanten Zeit\",\"Medizin Name\",\"Stärke\",\"Dosis\",\"Anweisungen\",\"Aktivitätsdatum\",\"Aktionszeit\",\"Aktion\""];
                    [mainString appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
                {
                    [mainString appendFormat:@"\"Schema Datum\",\"Schema tijd\",\"Medicijn naam\",\"Sterkte\",\"Dosering\",\"Instrukties\",\"Handeling Datum\",\"Handeling tijd\",\"Handeling\""];
                    [mainString appendFormat:@"\n"];;
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
                    
                {
                    [mainString appendFormat:@"\"Programma Data\",\"Programma assunzione\",\"Nome farmaco\",\"Forza cerotto\",\"Dose\",\"Istruzioni\",\"Azione Data\",\"Azione assunzione\",\"Azione\""];
                    [mainString appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
                {
                    [mainString appendFormat:@"\"Πρόγραμμα ημερομηνία\",\"Χρόνος Προγράμματος\",\"Ονομασία φαρμάκου\",\"Ισχύς\",\"Δόση\",\"Οδηγίες\",\"Ημερομηνία Δράσης\",\"Ώρα Δράσης\",\"Δράση\""];
                    [mainString appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
                {
                    [mainString appendFormat:@"\"Scheduled Date\",\"Scheduled Time\",\"Medicine Name\",\"Strength\",\"Dose\",\"Instructions\",\"Action Date\",\"Action Time\",\"Action\""];
                    [mainString appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
                    
                {
                    [mainString appendFormat:@"\"Scheduled Date\",\"Scheduled Time\",\"Medicine Name\",\"Strength\",\"Dose\",\"Instructions\",\"Action Date\",\"Action Time\",\"Action\""];
                    [mainString appendFormat:@"\n"];
                    
                }
               else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
                {
                    [mainString appendFormat:@"\"Scheduled Date\",\"Scheduled Time\",\"Nombre del medicamento\",\"Concentración\",\"Dose\",\"Instrucciones\",\"Action Date\",\"Action Time\",\"Action\""];
                    [mainString appendFormat:@"\n"];
                }
                
                //
                
                
                
                
                NSString *string=[set stringFromDate:newCourse.date];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@"\"%@\"",string];
                
                
                string=[dateFormatLevel stringFromDate:newCourse.originaltime];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                string=newCourse.name;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=newCourse.dose;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=newCourse.quantity;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                
                NSString *string2=newCourse.type;
                string2=[string2 stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@ %@\"",string, string2];
                
                string=newCourse.action;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                string=[set stringFromDate:newCourse.date];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                
                string=[dateFormatLevel stringFromDate:newCourse.timeaction];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                string=NSLocalizedString(newCourse.taken,nil);
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                [mainString appendFormat:@"\n"];
                
                
            }
            else
                
            {
                
                NSString *string=[set stringFromDate:newCourse.date];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@"\"%@\"",string];
                
                
                string=[dateFormatLevel stringFromDate:newCourse.originaltime];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                string=newCourse.name;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=newCourse.dose;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=newCourse.quantity;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                
                NSString *string2=newCourse.type;
                string2=[string2 stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@ %@\"",string, string2];
                
                string=newCourse.action;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                string=[set stringFromDate:newCourse.date];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                
                string=[dateFormatLevel stringFromDate:newCourse.timeaction];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
              string=NSLocalizedString(newCourse.taken,nil);
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                [mainString appendFormat:@"\n"];
                
            }
        }
        
     
        
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
        {
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
            
            
            NSString *documentsDir = [paths objectAtIndex:0];
            
            
            NSString *documentsDirectoryPath = [NSString stringWithFormat:@"%@/Caches/",documentsDir];
            NSString *filePath = [documentsDirectoryPath  stringByAppendingPathComponent:NSLocalizedString(@"MedicineHistoryData.txt",nil)];
            
            //NSLog(@"%@", filePath);
            
            NSData* settingsData;
            
            settingsData = [mainString dataUsingEncoding: NSUTF8StringEncoding];
            
            if ([mainString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:NULL])
                
                
                [mailer setMessageBody:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Profile Name:",nil) , NameLabel.text] isHTML:YES];
            
            [mailer addAttachmentData:settingsDataExtra mimeType:@"text/cvs" fileName:NSLocalizedString(@"ProfileHistorySummary.csv",nil)];
            [mailer addAttachmentData:settingsData mimeType:@"text/txt" fileName:NSLocalizedString(@"MedicineHistoryData.txt",nil)];
            mailer.navigationBar.tintColor =[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
            
            [self presentViewController:mailer animated:YES completion:NULL];
        }
        
        else
        {
            
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
            
            
            NSString *documentsDir = [paths objectAtIndex:0];
            
            
            NSString *documentsDirectoryPath = [NSString stringWithFormat:@"%@/Caches/",documentsDir];
            NSString *filePath = [documentsDirectoryPath  stringByAppendingPathComponent:NSLocalizedString(@"MedicineHistoryData.csv",nil)];
            
            //NSLog(@"%@", filePath);
            
            NSData* settingsData;
            
            settingsData = [mainString dataUsingEncoding: NSUTF8StringEncoding];
            
            if ([mainString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:NULL])
                
                
                [mailer setMessageBody:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Profile Name:",nil) , NameLabel.text] isHTML:YES];
            
            [mailer addAttachmentData:settingsDataExtra mimeType:@"text/cvs" fileName:NSLocalizedString(@"ProfileHistorySummary.csv",nil)];
            [mailer addAttachmentData:settingsData mimeType:@"text/cvs" fileName:NSLocalizedString(@"MedicineHistoryData.csv",nil)];
            mailer.navigationBar.tintColor =[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
            
            [self presentViewController:mailer animated:YES completion:NULL];
        }
        
        
        
      
    }
    
    else
    {
        
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:NSLocalizedString(@"CFDaily Medicine History Data",nil)];
        
        
        NSMutableString *mainString=[[NSMutableString alloc]initWithString:@""];
        
        
        NSError *error = nil;
        NSManagedObjectContext *context = self.managedObjectContext;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"History"
                                                  inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                                       ascending:NO];
        NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"originaltime"
                                                                        ascending:NO];
        NSSortDescriptor *sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"timeaction"
                                                                        ascending:NO];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor,sortDescriptor2,sortDescriptor3, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        NSDateFormatter *set = [[NSDateFormatter alloc] init];
        [set setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [set setDateFormat:@"dd/MM/yyyy"];
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"hh:mm a"]];
        
        
        for (NSInteger i=0; i < [fetchedObjects count]; i++) {
            
            
            
            History *newCourse = [fetchedObjects objectAtIndex:i];
            
            if (i==0) {
                if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
                {
                    
                    
                    [mainString appendFormat:@"\"Scheduled Date\",\"Scheduled Time\",\"Medicine Name\",\"Strength\",\"Dose\",\"Instructions\",\"Action Date\",\"Action Time\",\"Action\""];
                    [mainString appendFormat:@"\n"];
                }
                //07-09-2017
               else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                {
                    
                    
                    [mainString appendFormat:@"\"Aftalt dato\",\"Aftalt tid\",\"Lægemiddelnavn\",\"Styrke\",\"Dosis\",\"Instruktioner\",\"Handlingsdato\",\"Handlingstidspunkt\",\"Handling\""];
                    [mainString appendFormat:@"\n"];
                }
                
                //07-09-2017
               else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
               {
                   
                   
                   [mainString appendFormat:@"\"Planlagt dato\",\"Planlagt tid\",\"Medisinnavn\",\"Styrke\",\"Dose\",\"Instruksjoner\",\"Handlingsdato\",\"Handlingstid\",\"Handling\""];
                   [mainString appendFormat:@"\n"];
               }

                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
                {
                    [mainString appendFormat:@"\"Date prévue\",\"Heure prévue\",\"Nom de Médicament\",\"Dosage\",\"Quantité\",\"Indications\",\"Date Actions\",\"Heure Actions\",\"Action\""];
                    [mainString appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
                {
                    [mainString appendFormat:@"\"Medizin Geschichte\",\"Geplanten Zeit\",\"Medizin Name\",\"Stärke\",\"Dosis\",\"Anweisungen\",\"Aktivitätsdatum\",\"Aktionszeit\",\"Aktion\""];
                    [mainString appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
                {
                    [mainString appendFormat:@"\"Πρόγραμμα ημερομηνία\",\"Χρόνος Προγράμματος\",\"Ονομασία φαρμάκου\",\"Ισχύς\",\"Δόση\",\"Οδηγίες\",\"Ημερομηνία Δράσης\",\"Ώρα Δράσης\",\"Δράση\""];
                    [mainString appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
                {
                    [mainString appendFormat:@"\"Schema Datum\",\"Schema tijd\",\"Medicijn naam\",\"Sterkte\",\"Dosering\",\"Instrukties\",\"Handeling Datum\",\"Handeling tijd\",\"Handeling\""];
                    [mainString appendFormat:@"\n"];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
                    
                {
                    [mainString appendFormat:@"\"Programma Data\",\"Programma assunzione\",\"Nome farmaco\",\"Forza cerotto\",\"Dose\",\"Istruzioni\",\"Azione Data\",\"Azione assunzione\",\"Azione\""];
                    [mainString appendFormat:@"\n"];
                    
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
                    
                {
                    [mainString appendFormat:@"\"Scheduled Date\",\"Scheduled Time\",\"Medicine Name\",\"Strength\",\"Dose\",\"Instructions\",\"Action Date\",\"Action Time\",\"Action\""];
                    
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
                    
                {
                        [mainString appendFormat:@"\"Scheduled Date\",\"Scheduled Time\",\"Medicine Name\",\"Strength\",\"Dose\",\"Instructions\",\"Action Date\",\"Action Time\",\"Action\""];
                    
                }
               else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
                {
                    
                    
                    [mainString appendFormat:@"\"Scheduled Date\",\"Scheduled Time\",\"Nombre del medicamento\",\"Concentración\",\"Dose\",\"Instrucciones\",\"Action Date\",\"Action Time\",\"Action\""];
                    [mainString appendFormat:@"\n"];
                }

                else
                {
                    
                    
                        [mainString appendFormat:@"\"Scheduled Date\",\"Scheduled Time\",\"Medicine Name\",\"Strength\",\"Dose\",\"Instructions\",\"Action Date\",\"Action Time\",\"Action\""];
                }
                
                
                
                [mainString appendFormat:@"\n"];
                
                
                NSString *string=[set stringFromDate:newCourse.date];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@"\"%@\"",string];
                
                
                string=[dateFormatLevel stringFromDate:newCourse.originaltime];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                string=newCourse.name;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=newCourse.dose;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=newCourse.quantity;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                
                NSString *string2=newCourse.type;
                string2=[string2 stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@ %@\"",string, string2];
                
                string=newCourse.action;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                string=[set stringFromDate:newCourse.date];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                
                string=[dateFormatLevel stringFromDate:newCourse.timeaction];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
               string=NSLocalizedString(newCourse.taken,nil);
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                [mainString appendFormat:@"\n"];
                
                
            }
            else
                
            {
                
                NSString *string=[set stringFromDate:newCourse.date];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@"\"%@\"",string];
                
                
                string=[dateFormatLevel stringFromDate:newCourse.originaltime];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                string=newCourse.name;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=newCourse.dose;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=newCourse.quantity;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                
                NSString *string2=newCourse.type;
                string2=[string2 stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@ %@\"",string, string2];
                
                string=newCourse.action;
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                string=[set stringFromDate:newCourse.date];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                
                string=[dateFormatLevel stringFromDate:newCourse.timeaction];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
               string=NSLocalizedString(newCourse.taken,nil);
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                [mainString appendFormat:@"\n"];
                
            }
        }
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
        
        
        NSString *documentsDir = [paths objectAtIndex:0];
        
     
    
        
        
        NSData *settingsData;
        
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
        {
          
            
            
            NSString *documentsDirectoryPath = [NSString stringWithFormat:@"%@/Caches/",documentsDir];
            NSString *filePath = [documentsDirectoryPath  stringByAppendingPathComponent:NSLocalizedString(@"MedicineHistoryData.txt",nil)];
            
            
            settingsData = [mainString dataUsingEncoding: NSUTF8StringEncoding];
            
            NSString* theString = [[NSString alloc] initWithData:settingsData encoding:NSUTF8StringEncoding];

                 //NSLog(@"%@", theString);
            
            if ([mainString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:NULL])
                
                [mailer setMessageBody:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Profile Name:",nil), NameLabel.text] isHTML:YES];
            [mailer addAttachmentData:settingsData mimeType:@"text/txt" fileName:NSLocalizedString(@"txt",nil)];
            mailer.navigationBar.tintColor =[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
            [self presentViewController:mailer animated:YES completion:NULL];
            
        }
        
        else
        {
            
            
            NSString *documentsDirectoryPath = [NSString stringWithFormat:@"%@/Caches/",documentsDir];
            NSString *filePath = [documentsDirectoryPath  stringByAppendingPathComponent:NSLocalizedString(@"MedicineHistoryData.tcsv",nil)];
            
            
            settingsData = [mainString dataUsingEncoding: NSUTF8StringEncoding];
            
            
            if ([mainString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:NULL])
                
                [mailer setMessageBody:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Profile Name:",nil), NameLabel.text] isHTML:YES];
            [mailer addAttachmentData:settingsData mimeType:@"text/cvs" fileName:NSLocalizedString(@"MedicineHistoryData.csv",nil)];
            mailer.navigationBar.tintColor =[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
            [self presentViewController:mailer animated:YES completion:NULL];
        }
        
        ////////////////////NSLog(@"%@", mainString);
        
        
        
        
    }
    
    
    
    
}




-(void) Makepdf
{
    
//    [self googleAnalyticsGroupping];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    MFMailComposeViewController *composer = [[MFMailComposeViewController alloc] init];
    [composer setMailComposeDelegate:self];
    
    NSString *increments2 = @"";
    
    NSString *tableFirstAdherence =@"";
    
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {
        
        /*
         
         ·         French: Niveau d’observance
         ·         German:  Angabe der Therapietreue in Prozent
         ·         Dutch:  Therapietrouw percentage
         
         */
        
        tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  @"Medicine Name", @"Adherence Percentage"];
        
    }
        //07-09-2017
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
    {
        tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  @"Lægemiddelnavn", @"Adherence Percentage"];
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
    {
        tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  @"Nom de Médicament", @"Niveau d’observance"];
        
    }
    //07-09-2017
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
    {
        tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  @"Medisinnavn", @"Adherensprosent"];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
    { tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  @"Medizin Name", @"Angabe der Therapietreue in Prozent"];
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
    {
        
        tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  @"Medicijn naam", @" Therapietrouw percentage"];
        
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
        
    {
        tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  @"Nome farmaco", @" Percentuale di adesione"];
        
    }
    
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
        
    {
        tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  @"Ονομασία Φαρμάκου", @" Ποσοστό Συμμόρφωσης"];
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
        
    {
           tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  @"  שם התרופה", @"אחוז היענות לטיפול"];
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
        
    {
          tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  NSLocalizedString(@"Medicine Name",nil), NSLocalizedString(@"Adherence Percentage",nil)];
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
    {
        
        tableFirstAdherence =[NSString stringWithFormat:@"<table width=\"400\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",  @"Nombre del medicamento", @"Porcentaje de cumplimiento terapéutico"];
        
    }

    
    
    
    increments2 = [increments2 stringByAppendingString:tableFirstAdherence];
    
    NSError *error = nil;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    
    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"id_medicine = %@", MedID];
    NSFetchRequest *fetchRequest3 = [[NSFetchRequest alloc] init];
    [fetchRequest3 setEntity:[NSEntityDescription entityForName:@"Medicine" inManagedObjectContext:[appDelegate managedObjectContext]]];
    [fetchRequest3 setPredicate:predicate3];
    
    
    NSArray *fetchedObjects3 = [context executeFetchRequest:fetchRequest3 error:&error];
    
    
    
    for (NSInteger i =0; i < [fetchedObjects3 count]; i++) {
        
        
        Medicine *newCourse = [fetchedObjects3 objectAtIndex:i];
        
        
        ////NSLog(@"%@", newCourse);
        
        increments2 = [increments2 stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
        increments2 = [increments2 stringByAppendingString:newCourse.name];
        increments2 = [increments2 stringByAppendingString:@"</th>"];
        
        
        float total = ([self calculculateWithoutPostponed:newCourse.id_medicine] *100)-(((long)[self calculculate:newCourse.id_medicine] *100));
        float pcntYes = ([self calculculateWithoutPostponed:newCourse.id_medicine] *100);
        float pcntNo = total/pcntYes;
        
        
        increments2 = [increments2 stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#000000\">"];
        increments2 = [increments2 stringByAppendingString:[NSString stringWithFormat:@"%.2f%%", (pcntNo*100)]];
        increments2 = [increments2 stringByAppendingString:@"</th>"];
        
        increments2 = [increments2 stringByAppendingString:@"<tr>"];
        
        
    }
    
    
    
    
    
    
    increments2 = [increments2 stringByAppendingString:@"</tr></table>"];
    
    
    
    
    
    NSString *increments = @"";
    
    
    
    
    
    NSString *tableFirstLine =@"";
    
    
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {
        
        tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",
                         
                         @"Scheduled Date", @"Scheduled Time", @"Medicine Name", @"Strength", @"Dose", @"Instructions", @"Action Date", @"Action Time", @"Action"];
        
    }
    //07-09-2017
  else  if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
    {
        
        tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",
                         
                         @"Aftalt dato", @"Aftalt tid", @"Lægemiddelnavn", @"Styrke", @"Dosis", @"Instruktioner", @"Handlingsdato", @"Handlingstidspunkt", @"Handling"];
        
    }
    //07-09-2017
  else  if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
  {
      
      tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",
                       
                       @"Planlagt dato", @"Planlagt tid", @"Medisinnavn", @"Styrke", @"Dose", @"Instruksjoner", @"Handlingsdato", @"Handlingstid", @"Handling"];
      
  }

    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
    {
        tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>", @"Date prévue", @"Heure prévue", @"Nom de Médicament", @"Dosage", @"Quantité", @"Indications", @"Date d’ action", @"Heure d’ action", @"Action"];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
    {
        
        tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>", @"Medizin Geschichte",@"Geplanten Zeit",@"Medizin Name",@"Stärke",@"Dosis",@"Anweisungen",@"Aktivitätsdatum",@"Aktionszeit",@"Aktion"];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
    {
        
        tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",
                         
                         @"Schema Datum", @"Schema tijd", @"Medicijn naam", @"Sterkte", @"Dosering", @"Instrukties", @"Handeling Datum", @"Handeling tijd", @"Handeling"];
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
        
    {
        tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",
                         
                         @"Programma Data", @"Programma assunzione", @"Nome farmaco", @"Forza cerotto", @"Dose", @"Istruzioni", @"Azione Data", @"Azione assunzione", @"Azione"];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
    {
        
        tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",
                         
                         @"Ημερομηνία Προγράμματος", @"Χρόνος Προγράμματος", @"Ονομασία Φαρμάκου", @"Ισχύς", @"Δόση", @"Οδηγίες", @"Ημερομηνία Ενέργειας", @"Ώρα Ενέργειας", @"Ενέργεια"];
        
        
    }
    
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
    {
        
        tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",
                         
                         NSLocalizedString(@"Scheduled Date",nil), NSLocalizedString(@"Scheduled Time",nil), NSLocalizedString(@"Medicine Name",nil), NSLocalizedString(@"Strength",nil), NSLocalizedString(@"Dose",nil), NSLocalizedString(@"Instructions",nil), NSLocalizedString(@"Action Date",nil), NSLocalizedString(@"Action Time",nil), NSLocalizedString(@"Action",nil)];
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
        
    {
        tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",
                         
                         NSLocalizedString(@"Scheduled Date",nil), NSLocalizedString(@"Scheduled Time",nil), NSLocalizedString(@"Medicine Name",nil), NSLocalizedString(@"Strength",nil), NSLocalizedString(@"Dose",nil), NSLocalizedString(@"Instructions",nil), NSLocalizedString(@"Action Date",nil), NSLocalizedString(@"Action Time",nil), NSLocalizedString(@"Action",nil)];
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
    {
        
        tableFirstLine =[NSString stringWithFormat:@"<table width=\"1200\" border=\"1\"><tr><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th><th scope=\"col\"><font face=\"Arial\" font size=\"0.50\"  color=\"000000\" scope=\"col\" >%@</th></tr>",
                         
                         @"Scheduled Date", @"Scheduled Time", @"Nombre del medicamento", @"Concentración", @"Dose", @"Instrucciones", @"Action Date", @"Action Time", @"Action"];
        
    }

    
    
    
    increments = [increments stringByAppendingString:tableFirstLine];
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"History"
                                        inManagedObjectContext:[self managedObjectContext]]];
    
    
    
    NSString *query = [NSString stringWithFormat:@"idmedicine contains [cd]'%@'", MedID];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:query];
    
    
    [fetchRequest setPredicate:predicate];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                                   ascending:NO];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"originaltime" ascending:YES];
    NSSortDescriptor *sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"timeaction"
                                                                    ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor,sortDescriptor2,sortDescriptor3, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSInteger i=0; i < [fetchedObjects count]; i++) {
        
        
        History *newCourse = [fetchedObjects objectAtIndex:i];
        
        //20-11-2017
        NSString *strMedTYPE = @"";
        NSPredicate *predicatte = [NSPredicate predicateWithFormat:@"name == %@",newCourse.name];
        NSArray *array_predicate_filter = [[SHARED_APPDELEGATE array_AllMedicine] filteredArrayUsingPredicate:predicatte];
        if (array_predicate_filter.count > 0){
            strMedTYPE = [[array_predicate_filter objectAtIndex:0] valueForKey:@"type"];
        }
        
        NSArray *typit = [[GetData getDrugTypeStrength:newCourse.type] objectAtIndex:0];
        
        
        if ([newCourse.type isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
            if (newCourse.quantity) {
                
                if ([newCourse.dose isEqualToString:@"piece"]) {
                    
                    if ([newCourse.quantity intValue] >1) {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@",newCourse.type, [typit valueForKey:@"Drug_types"]];

                        //20-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            quantityString = [NSString stringWithFormat:@"%@",strMedTYPE];
                            
                            if ([strMedTYPE isEqualToString:@"Sprays"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprays"];
                            }
                            
                            if ([strMedTYPE isEqualToString:@"Tablet"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablet" withString:@"Tabletter"];
                            }
                        }
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"]){
                            quantityString = [NSString stringWithFormat:@"%@",strMedTYPE];
                            
                            if ([strMedTYPE isEqualToString:@"Spray"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprayer"];
                            }
                            
                            if ([strMedTYPE isEqualToString:@"Tablett"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                            }
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }
                        
                    }
                    else
                    {
                        quantityString =[NSString stringWithFormat:@"%@ %@",newCourse.type, [typit valueForKey:@"Drug_less"]];
                        //20-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString = [NSString stringWithFormat:@"%@",strMedTYPE];
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }
                    }
                }
                
                else
                {
                    quantityString =[NSString stringWithFormat:@"%@",newCourse.type];
                    //20-11-2017
                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                    {
                        quantityString = [NSString stringWithFormat:@"%@",strMedTYPE];
                        
                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                    }
                }
                
            }
            else
            {
                if ([newCourse.dose isEqualToString:@"piece"]) {
                    
                    if ([newCourse.quantity intValue] >1) {
                        
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", newCourse.type,[typit valueForKey:@"Drug_types"]];
                        
                        //20-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            quantityString = [NSString stringWithFormat:@"%@",strMedTYPE];
                            
                            if ([strMedTYPE isEqualToString:@"Sprays"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprays"];
                            }
                            
                            if ([strMedTYPE isEqualToString:@"Tablet"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablet" withString:@"Tabletter"];
                            }
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"]){

                            quantityString = [NSString stringWithFormat:@"%@",strMedTYPE];
                            
                            if ([strMedTYPE isEqualToString:@"Spray"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprayer"];
                            }
                            
                            if ([strMedTYPE isEqualToString:@"Tablett"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                            }
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }

                    }
                    
                    else
                    {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", newCourse.type,[typit valueForKey:@"Drug_less"]];
                        //20-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString = [NSString stringWithFormat:@"%@",strMedTYPE];
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }
                    }
                    
                    
                    
                }
                
                else
                {
                    quantityString =[NSString stringWithFormat:@"%@",newCourse.type];
                    
                    //20-11-2017
                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                    {
                        quantityString = [NSString stringWithFormat:@"%@",strMedTYPE];
                        
                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                    }
                }
                
            }
            
            
        }
        else
        {
            
            if (newCourse.quantity) {
                
                if ([newCourse.dose isEqualToString:@"piece"]) {
                    
                    if ([newCourse.quantity intValue] >1) {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@",newCourse.quantity, newCourse.type, [typit valueForKey:@"Drug_types"]];
                        
                        //20-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            quantityString =[NSString stringWithFormat:@"%@ %@",newCourse.quantity,strMedTYPE];
                            
                            if ([strMedTYPE isEqualToString:@"Sprays"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprays"];
                            }
                            
                            if ([strMedTYPE isEqualToString:@"Tablet"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablet" withString:@"Tabletter"];
                            }
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }
                        
                        //20-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString =[NSString stringWithFormat:@"%@ %@",newCourse.quantity,strMedTYPE];
                            
                            if ([strMedTYPE isEqualToString:@"Spray"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprayer"];
                            }
                            
                            if ([strMedTYPE isEqualToString:@"Tablett"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                            }
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }
                        

                    }
                    else
                    {
                        quantityString =[NSString stringWithFormat:@"%@ %@ %@",newCourse.quantity, newCourse.type, [typit valueForKey:@"Drug_less"]];
                        
                        //20-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString =[NSString stringWithFormat:@"%@ %@",newCourse.quantity,strMedTYPE];
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }
                        

                    }
                    
                    
                }
                
                else
                {
                    quantityString =[NSString stringWithFormat:@"%@ %@",newCourse.quantity, newCourse.type];
                    
                    //20-11-2017
                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] )
                    {
                        quantityString =[NSString stringWithFormat:@"%@ %@",newCourse.quantity, strMedTYPE];
                        
                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprays"];
                        
                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablet" withString:@"Tabletter"];

                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                    }

                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                    {
                        quantityString =[NSString stringWithFormat:@"%@ %@",newCourse.quantity, strMedTYPE];

                        if ([newCourse.quantity intValue] > 1)
                        {
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprayer"];
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];
                        }
                    }
                }
            }
            else
            {
                if ([newCourse.dose isEqualToString:@"piece"]) {
                    
                    if ([newCourse.quantity intValue] >1) {
                        
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@",newCourse.type,[typit valueForKey:@"Drug_types"]];
                        
                        //20-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                        {
                            quantityString =[NSString stringWithFormat:@"%@",strMedTYPE];
                            
                            if ([strMedTYPE isEqualToString:@"Sprays"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprays"];
                            }
                            
                            if ([strMedTYPE isEqualToString:@"Tablett"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                            }
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }
                        
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString =[NSString stringWithFormat:@"%@",strMedTYPE];
                            
                            if ([strMedTYPE isEqualToString:@"Spray"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Spray" withString:@"Sprayer"];
                            }
                            
                            if ([strMedTYPE isEqualToString:@"Tablett"])
                            {
                                quantityString = [quantityString stringByReplacingOccurrencesOfString:@"Tablett" withString:@"Tabletter"];
                            }
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }
                       
                    }
                    
                    else
                    {
                        
                        quantityString =[NSString stringWithFormat:@"%@ %@", newCourse.type,[typit valueForKey:@"Drug_less"]];
                        //20-11-2017
                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                        {
                            quantityString =[NSString stringWithFormat:@"%@",strMedTYPE];
                            
                            quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                        }

                    }
                    
                    
                    
                }
                
                else
                {
                    quantityString =[NSString stringWithFormat:@"%@",newCourse.type];
                    //20-11-2017
                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
                    {
                        quantityString =[NSString stringWithFormat:@"%@",strMedTYPE];
                        
                        quantityString = [quantityString stringByReplacingOccurrencesOfString:@"stk" withString:@""];

                    }
                }
                
            }
        }
        
        
        ////////////////////////////////////////////////////NSLog(@"%@", newCourse);
        
        NSDateFormatter *set = [[NSDateFormatter alloc] init];
        [set setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [set setDateFormat:@"dd/MM/yyyy"];
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"hh:mm a"]];
        
        
        
        increments = [increments stringByAppendingString:@"<tr><th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
        increments = [increments stringByAppendingString:[set stringFromDate:newCourse.date]];
        increments = [increments stringByAppendingString:@"</th>"];
        
        
        
        
        increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
        increments = [increments stringByAppendingString:[dateFormatLevel stringFromDate:newCourse.originaltime]];
        increments = [increments stringByAppendingString:@"</th>"];
        
        
        
        
        increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
        increments = [increments stringByAppendingString:newCourse.name];
        increments = [increments stringByAppendingString:@"</th>"];
        
        
        
        increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
        increments = [increments stringByAppendingString:newCourse.dose];
        increments = [increments stringByAppendingString:@"</th>"];
        
        
        
        
        increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
        increments = [increments stringByAppendingString:quantityString];
        increments = [increments stringByAppendingString:@"</th>"];
        
        
        
        
        increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
        increments = [increments stringByAppendingString:newCourse.action];
        increments = [increments stringByAppendingString:@"</th>"];
        
        
        
        increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
        increments = [increments stringByAppendingString:[set stringFromDate:newCourse.date]];
        increments = [increments stringByAppendingString:@"</th>"];
        
        
        
        increments = [increments stringByAppendingString:@"<th scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"000000\" scope=\"col\" >"];
        increments = [increments stringByAppendingString:[dateFormatLevel stringFromDate:newCourse.timeaction]];
        increments = [increments stringByAppendingString:@"</th>"];
        
        
        
        if ([newCourse.taken isEqualToString:@"Taken"]||[newCourse.taken isEqualToString:@"Taken#"]) {
            
            
            
            increments = [increments stringByAppendingString:@"<th bgcolor=\"#125c0f\" scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"#FFFFFF\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:NSLocalizedString(@"Taken",nil)];
            increments = [increments stringByAppendingString:@"</th>"];
                
           
            
        }
        if ([newCourse.taken isEqualToString:@"Taken earlier"]||[newCourse.taken isEqualToString:@"Taken earlier#"]) {
            
            increments = [increments stringByAppendingString:@"<th bgcolor=\"#126362\" scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"#FFFFFF\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:NSLocalizedString(@"Taken earlier",nil)];
            increments = [increments stringByAppendingString:@"</th>"];
            
            
        }
        if ([newCourse.taken isEqualToString:@"Taken late"]||[newCourse.taken isEqualToString:@"Taken late#"]) {
            
            
            
            increments = [increments stringByAppendingString:@"<th  bgcolor=\"#f2351a\" scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"#FFFFFF\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:NSLocalizedString(@"Taken late",nil)];
            increments = [increments stringByAppendingString:@"</th>"];
            
        }
        if ([newCourse.taken isEqualToString:@"Missed"]) {
            
            
            
            increments = [increments stringByAppendingString:@"<th  bgcolor=\"#f2351a\"  scope=\"col\"><font face=\"Arial\"  font size=\"0.50\" color=\"#FFFFFF\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:NSLocalizedString([newCourse.taken stringByReplacingOccurrencesOfString:@"#" withString:@""],nil)];
            increments = [increments stringByAppendingString:@"</th>"];
            
            
        }
        if ([newCourse.taken isEqualToString:@"Skipped"]||[newCourse.taken isEqualToString:@"Skipped#"]) {
            
            increments = [increments stringByAppendingString:@"<th  bgcolor=\"#d45a19\"  scope=\"col\"><font face=\"Arial\" font size=\"0.50\" color=\"#FFFFFF\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:NSLocalizedString([newCourse.taken stringByReplacingOccurrencesOfString:@"#" withString:@""],nil)];
            increments = [increments stringByAppendingString:@"</th>"];
            
            
        }
        if ([newCourse.taken isEqualToString:@"Postponed"]||[newCourse.taken isEqualToString:@"Postponed#"]) {
            
            increments = [increments stringByAppendingString:@"<th  bgcolor=\"#0f006d\" scope=\"col\"><font face=\"Arial\"   font size=\"0.50\" color=\"#FFFFFF\" scope=\"col\" >"];
            increments = [increments stringByAppendingString:NSLocalizedString([newCourse.taken stringByReplacingOccurrencesOfString:@"#" withString:@""],nil)];
            increments = [increments stringByAppendingString:@"</th>"];
            
        }
        
        
        
        
        
        increments = [increments stringByAppendingString:@"<tr>"];
        
    }
    if ([MFMailComposeViewController canSendMail]) {
        
        
        //CFDaily Medicine History Data en ook medicen document moet of profile en Medince
        [composer setToRecipients:[NSArray arrayWithObjects:@"", nil]];
        [composer setSubject:NSLocalizedString(@"CFDaily Medicine History Data",nil)];
        [composer setMessageBody:[NSString stringWithFormat:@"%@ %@ %@<p> </p><p> </p>%@", NSLocalizedString(@"Profile Name:",nil), NameLabel.text, increments2, increments] isHTML:YES];
        
        [composer setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
        [self presentViewController:composer animated:YES completion:NULL];
        [composer.navigationBar setTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        [composer.navigationBar setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forBarMetrics:0];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [appDelegate.toolbarDown setAlpha:0];
    }
    
}

#pragma mark - Tableview

-(NSInteger)calculculateWithoutPostponed:(NSString*)sender

{
    
    ////NSLog(@"calculculateWithoutPostponed op ID");
    
    //Skipped
    
    NSError *error = nil;
    
    NSFetchRequest *fetchRequestSkipped = [[NSFetchRequest alloc] init];
    
    [fetchRequestSkipped setEntity:[NSEntityDescription entityForName:@"History"
                                               inManagedObjectContext:[self managedObjectContext]]];
    
    
    //    /(taken CONTAINS %@)
    NSString *querySkipped = [NSString stringWithFormat:@"(idmedicine contains [cd]'%@') and (taken != 'Postponed')", sender];
    
    
    NSPredicate *predicateSkipped = [NSPredicate predicateWithFormat:querySkipped];
    
    
    [fetchRequestSkipped setPredicate:predicateSkipped];
    
    
    NSSortDescriptor *sortDescriptorSkipped = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                                          ascending:NO];
    NSSortDescriptor *sortDescriptor2Skipped = [[NSSortDescriptor alloc] initWithKey:@"originaltime" ascending:YES];
    NSSortDescriptor *sortDescriptor3Skipped = [[NSSortDescriptor alloc] initWithKey:@"timeaction"
                                                                           ascending:NO];
    NSArray *sortDescriptorsSkipped = [[NSArray alloc] initWithObjects:sortDescriptorSkipped,sortDescriptor2Skipped,sortDescriptor3Skipped, nil];
    [fetchRequestSkipped setSortDescriptors:sortDescriptorsSkipped];
    
    
    NSArray *fetchedObjectsSkipped = [[self managedObjectContext] executeFetchRequest:fetchRequestSkipped error:&error];
    ////NSLog(@"fetchedObjectsSkipped %lu", (unsigned long)[fetchedObjectsSkipped count]);
    
    return [fetchedObjectsSkipped count];
    
}


-(NSInteger)calculculate:(NSString*)sender

{
    
    //Skipped
    
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    
    NSFetchRequest *fetchRequestSkipped = [[NSFetchRequest alloc] init];
    
    [fetchRequestSkipped setEntity:[NSEntityDescription entityForName:@"History"
                                               inManagedObjectContext:[self managedObjectContext]]];
    
    
    
    NSString *querySkipped = [NSString stringWithFormat:@"(idmedicine contains [cd]'%@') and (taken contains 'Skipped')", sender];
    
    
    NSPredicate *predicateSkipped = [NSPredicate predicateWithFormat:querySkipped];
    
    
    [fetchRequestSkipped setPredicate:predicateSkipped];
    
    
    NSSortDescriptor *sortDescriptorSkipped = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                                          ascending:NO];
    NSSortDescriptor *sortDescriptor2Skipped = [[NSSortDescriptor alloc] initWithKey:@"originaltime" ascending:YES];
    NSSortDescriptor *sortDescriptor3Skipped = [[NSSortDescriptor alloc] initWithKey:@"timeaction"
                                                                           ascending:NO];
    NSArray *sortDescriptorsSkipped = [[NSArray alloc] initWithObjects:sortDescriptorSkipped,sortDescriptor2Skipped,sortDescriptor3Skipped, nil];
    [fetchRequestSkipped setSortDescriptors:sortDescriptorsSkipped];
    
    
    NSArray *fetchedObjectsSkipped = [context executeFetchRequest:fetchRequestSkipped error:&error];
    
    
    return [fetchedObjectsSkipped count];
    
}
-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller


{
    
    [table beginUpdates];
    
    
    
    
    
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller


{
    [table endUpdates];
    
    
    
}

-(void) controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [table insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [table deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            break;
        case NSFetchedResultsChangeMove:
            break;
    }
}


-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = table;
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            break;
            
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    id<NSFetchedResultsSectionInfo> secInfo =[[self.fetchedResultsController sections] objectAtIndex:section];
    
    return [secInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryCell *cell = (HistoryCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[HistoryCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    
    
    History *cours =[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    
    [cell FillAllItems:cours];
    
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    UILabel *sectionHeader2;
    
    
    
    if ([ordering isEqualToString:NSLocalizedString(@"By Name", nil)]) {
        
        NSString *result =[[[self.fetchedResultsController sections] objectAtIndex:section]name];
        
        
        sectionHeader2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 15)];
//        sectionHeader2.backgroundColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]; //29-06-2017 changes
        [sectionHeader2 setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];

        sectionHeader2.textColor = [UIColor whiteColor];
        sectionHeader2.font = [UIFont boldSystemFontOfSize:16];
        sectionHeader2.text =  result;
        sectionHeader2.textAlignment=NSTextAlignmentCenter;
        
        sectionHeader2.layer.shadowOffset = CGSizeMake(3, 0);
        sectionHeader2.layer.shadowOpacity = 1;
        sectionHeader2.layer.shadowRadius = 1.0;
        
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
            
            //NSLog(@"left");
            
            sectionHeader2.transform = CGAffineTransformMakeScale(-1, 1);
            
        }
        else
        {
            
        }
        
        
        return sectionHeader2;
        
        
        
    }
    
    else {
        NSString *result =[[[self.fetchedResultsController sections] objectAtIndex:section]name];
        
        
        sectionHeader2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 0)];
//        sectionHeader2.backgroundColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]; //29-06-2017 changes
        [sectionHeader2 setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        sectionHeader2.textColor = [UIColor whiteColor];
        sectionHeader2.font = [UIFont boldSystemFontOfSize:16];
        sectionHeader2.text =  result;
        sectionHeader2.textAlignment=NSTextAlignmentCenter;
        
        sectionHeader2.layer.shadowOffset = CGSizeMake(3, 0);
        sectionHeader2.layer.shadowOpacity = 1;
        sectionHeader2.layer.shadowRadius = 1.0;
        
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
            
            //NSLog(@"left");
            
            sectionHeader2.transform = CGAffineTransformMakeScale(-1, 1);
            
        }
        else
        {
            
        }
        
        
        
        
    }
    
    
    return sectionHeader2;
    
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section

{
    
    
    
    return [[[self.fetchedResultsController sections] objectAtIndex:section]name];
    
    
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSManagedObjectContext *contex =self.managedObjectContext;
        History *cours =[self.fetchedResultsController objectAtIndexPath:indexPath];
        [contex deleteObject:cours];
        
        NSError *error = nil;
        NSManagedObjectContext *context = self.managedObjectContext;
        if (![context save:&error]) {
            
        }
    }
}


#pragma mark - searching




- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar

{
    
    
    
    [searchBar resignFirstResponder];
    
    
    
    
    
    
    
    
}


-(void)AddItems:(NSString*)set

{
    
}


- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    
    if ([ordering isEqualToString:NSLocalizedString(@"By Name", nil)]) {
        
        [searchBar resignFirstResponder];
        
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        [fetchRequest setEntity:[NSEntityDescription entityForName:@"History"
                                            inManagedObjectContext:[self managedObjectContext]]];
        
        
        
        
        NSString *query = [NSString stringWithFormat:@"name contains [cd]'%@'", searchBar.text];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:query];
        
        
        [fetchRequest setPredicate:predicate];
        
        NSSortDescriptor *sortDescriptor = nil;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                     ascending:YES];
        NSArray *sortDescriptors = nil;
        sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        
        
        
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        // Edit the section name key path and cache name if appropriate.
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[self managedObjectContext] sectionNameKeyPath:@"name" cacheName:nil];
        
        fetchedResultsControllerCopy =_fetchedResultsController;
        aFetchedResultsController.delegate = self;
        _fetchedResultsController = aFetchedResultsController;
        
        
        NSError *error = nil;
        if (![[self fetchedResultsController] performFetch:&error])
        {
            
            abort();
        }
        
        
        
        [table reloadData];
        
    }
    
    else if ([[ordering  lowercaseString] isEqualToString:[NSLocalizedString(@"By Date",nil) lowercaseString]])
    {
        
        [searchBar resignFirstResponder];
        
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        [fetchRequest setEntity:[NSEntityDescription entityForName:@"History"
                                            inManagedObjectContext:[self managedObjectContext]]];
        
        
        
        NSString *query = [NSString stringWithFormat:@"date contains [cd]'%@'", searchBar.text];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:query];
        
        
        [fetchRequest setPredicate:predicate];
        
        NSSortDescriptor *sortDescriptor = nil;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                     ascending:YES];
        NSArray *sortDescriptors = nil;
        sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        
        
        
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        // Edit the section name key path and cache name if appropriate.
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[self managedObjectContext] sectionNameKeyPath:@"name" cacheName:nil];
        
        fetchedResultsControllerCopy =_fetchedResultsController;
        aFetchedResultsController.delegate = self;
        _fetchedResultsController = aFetchedResultsController;
        
        
        NSError *error = nil;
        if (![[self fetchedResultsController] performFetch:&error])
        {
            
            abort();
        }
        
        
        
        [table reloadData];
        
        
    }
    
    
    
    
    
    
}

- (void) searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
    // TODO - dynamically update the search results here, if we choose to do that.
    
    if (![theSearchBar isFirstResponder]) {
        // The user clicked the [X] button while the keyboard was hidden
        
        
        
        _fetchedResultsController=fetchedResultsControllerCopy;
        
        [table reloadData];
        
        
        shouldBeginEditing = NO;
    }
    else if ([searchText length] == 0) {
        // The user clicked the [X] button or otherwise cleared the text.
        
        
        
        _fetchedResultsController=fetchedResultsControllerCopy;
        
        [table reloadData];
        
        
        
        [theSearchBar performSelector: @selector(resignFirstResponder)
                           withObject: nil
                           afterDelay: 0.1];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)bar {
    // reset the shouldBeginEditing BOOL ivar to YES, but first take its value and use it to return it from the method call
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}



- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar

{
    
    [searchBar resignFirstResponder];
    
}



//#pragma mark - Google Analytics
//
//-(void)googleAnalyticsGroupping
//{
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:2]value:@"List of Medicines email button"];
//}


#pragma mark - Google Analytics - Action

-(void)addActionForGoogleAnalytics:(NSString *)actionName
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"button_press"
                                                           label:actionName
                                                           value:nil] build]];
}

@end


