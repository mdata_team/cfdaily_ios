//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>

@class CodePopView, Monthview;
@interface HistoryViewController : UIViewController<UITextViewDelegate, UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, retain) NSMutableDictionary *make;
@property (nonatomic, retain) Monthview *monthView;
@property (nonatomic, assign) CGFloat rood;
@property (nonatomic, assign) CGFloat groen;
@property (nonatomic, assign) CGFloat blauw;
@property (nonatomic, assign) CGFloat doorzicht;
@property (nonatomic, retain) UILabel *NameLabel;
@property (nonatomic, retain) UIImageView *headShot;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) NSMutableArray *HistSectionList;
@property (nonatomic, retain) NSMutableArray *SectionList;
@property (nonatomic, assign) NSString *SetOrder;
@property (nonatomic, assign)NSString *MedID;
@property (nonatomic, retain) UILabel *sectionHeader;

@property (nonatomic, retain) UIButton *MonthButton;


@property (nonatomic, retain) NSMutableArray *DateResult;
@property (nonatomic, retain) NSMutableArray *HistSectionListDate;
@property (nonatomic, retain) NSMutableArray  *DateList;
@property (nonatomic, retain) IBOutlet UITableView *table;


@property (nonatomic, retain) UIButton *button;

@property (nonatomic, retain) UIView *ProfileView;
@property (nonatomic, retain) NSString *chosenID;
- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)setItems:(NSMutableDictionary*)set;
-(void)chosenMonth:(NSString*)sender;

-(void)viewanimate;
-(void)setItemsFrom:(NSMutableDictionary*)set andID:(NSString*)Medid;
@end
