//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "HistoryViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "NewProfileController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "HistoryCell.h"
#import "Monthview.h"
#import "InfoViewController.h"
#import "MultipleViewController.h"

@interface HistoryViewController ()

@end

@implementation HistoryViewController
@synthesize rood;
@synthesize sectionHeader;
@synthesize groen;
@synthesize MonthButton;
@synthesize blauw;
@synthesize doorzicht;
@synthesize NameLabel;
@synthesize headShot;
@synthesize Vraagantwoord;
@synthesize table;
@synthesize make;
@synthesize chosenID;
@synthesize ProfileView;
@synthesize monthView;
@synthesize button;
@synthesize SectionList;
@synthesize DateList;
@synthesize DateResult;
@synthesize HistSectionList;
@synthesize MedID;
@synthesize HistSectionListDate;
@synthesize SetOrder;


- (void)viewDidLoad
{
    
    
    ////NSLog(@"viewDidLoad");
    
    
    self.navigationItem.title =  @"Profile History";
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    
    
    HistSectionListDate =[[NSMutableArray alloc]init];
    
  
    SetOrder = @"Name";
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 75, 320, self.view.bounds.size.height-30-120)];
    table.separatorColor = [UIColor grayColor];
    table.backgroundColor = [UIColor clearColor];
    table.rowHeight =120;
    table.delegate = self;
    table.dataSource = self;
    [table setTag:89788];
    [table setEditing:NO];
    [self.view addSubview:table];
    
    
   sectionHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, 320, 20)];
    sectionHeader.backgroundColor = [UIColor colorWithRed:0.235 green:0.435 blue:0.953 alpha:1.000];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.font = [UIFont boldSystemFontOfSize:16];
    sectionHeader.text =  @"Medicines Sorted By Name";
    sectionHeader.textAlignment=NSTextAlignmentCenter;
    
    sectionHeader.layer.shadowOffset = CGSizeMake(3, 0);
    sectionHeader.layer.shadowOpacity = 1;
    sectionHeader.layer.shadowRadius = 1.0;
    [self.view addSubview:sectionHeader];
    
    
    
    ProfileView = [[UIView alloc] initWithFrame:CGRectMake(-5, 0, 330, 60)];
    
    
    [self.view addSubview:ProfileView];
    
    
    
    
    
    headShot = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, 40, 40)];
    [headShot setBackgroundColor:[UIColor blackColor]];
    [headShot setTag:130];
    [headShot setImage:[UIImage imageNamed:@"headshot.png"]];
    [ProfileView addSubview:headShot];
    
    
    NameLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 20, 210, 20)];
    [NameLabel setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
    [NameLabel setFont:[UIFont boldSystemFontOfSize:16]];
    [NameLabel setTag:100];
    [NameLabel setBackgroundColor:[UIColor clearColor]];
    [NameLabel setNumberOfLines:3];
    [ProfileView addSubview:NameLabel];
    
    
    monthView = [[Monthview alloc] initWithFrame:CGRectMake(220, 20,80,22)];
    [monthView setBackgroundColor:[UIColor colorWithRed:0.114 green:0.533 blue:0.576 alpha:1.000]];
    
    [monthView setParant:self];
    [monthView.layer setBorderColor:[UIColor colorWithRed:0.349 green:0.565 blue:0.965 alpha:1.000].CGColor];
    [monthView.layer setMasksToBounds:YES];
    [monthView.layer setCornerRadius:4];
    [monthView.layer setBorderWidth:1.5];
    [monthView.layer setBorderWidth:1];
    [monthView.layer setCornerRadius:10];
    [self.view addSubview:monthView];
    
    
    MonthButton = [[UIButton alloc] initWithFrame:CGRectMake(220, 20,80,22)];
    [MonthButton setBackgroundColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
    [MonthButton addTarget:self action:@selector(viewanimate) forControlEvents:UIControlEventTouchUpInside];
    [MonthButton.titleLabel setFont:[UIFont boldSystemFontOfSize:10]];
       [MonthButton setTitle:@"By Name" forState:UIControlStateNormal];
    [MonthButton setBackgroundImage:[UIImage imageNamed:@"Month_view.png"] forState:UIControlStateNormal];
    [self.view addSubview:MonthButton];
    
    
    CAGradientLayer *shineLayer1 = [CAGradientLayer layer];
    shineLayer1.frame = MonthButton.layer.bounds;
    shineLayer1.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:0.2f alpha:0.4f].CGColor,
                          nil];
    shineLayer1.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.2f],
                             [NSNumber numberWithFloat:0.4f],
                             [NSNumber numberWithFloat:0.6f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];
    [MonthButton.layer setCornerRadius:4];
    [MonthButton.layer setBorderWidth:1.5];
    [MonthButton.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
    [MonthButton.layer addSublayer:shineLayer1];
    
    
       NSArray *profileItem =[NSArray arrayWithObjects:@"Name", @"ID", @"Image", @"ColorRed", @"ColorGreen", @"Colorblue", @"Coloralpha", @"Audio",@"Silence",@"Vibrate",nil];
    
    
    make =[[NSMutableDictionary alloc]init];
    
    make = [[GetData getProfiles:profileItem] objectAtIndex:0];
    
    ////NSLog(@"%@", make);
    
    
    
    if ([[GetData getProfiles:profileItem] count] ==1) {
        
        self.navigationItem.hidesBackButton = YES;
        
        
        chosenID = [make valueForKey:@"ID"];
        
        UIColor *colorit = [UIColor colorWithRed:[[make valueForKey:@"ColorRed"] floatValue] green:[[make valueForKey:@"ColorGreen"]  floatValue] blue:[[make valueForKey:@"Colorblue"]  floatValue] alpha:[[make valueForKey:@"Coloralpha"]  floatValue]];
        
        const CGFloat *components = CGColorGetComponents(colorit.CGColor);
        rood = components[0];
        groen = components[1];
        blauw = components[2];
        doorzicht = components[3];
        
        
        
        [ProfileView setBackgroundColor:colorit];
        ProfileView.layer.shadowOffset = CGSizeMake(3, 0);
        ProfileView.layer.shadowOpacity = 2;
        ProfileView.layer.shadowRadius = 2.0;
        [NameLabel setText:[make valueForKey:@"Name"]];
        [headShot setImage:[make valueForKey:@"Image"]];
        
        
        
    }
    
    else
    {
        
        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [a1 setTag:44];
        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
        
    }
    
    //34 × 31 pixels
    
    UIButton *a2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a2 setTag:44];
    [a2 setFrame:CGRectMake(20, 2, 34, 31)];
    [a2 addTarget:self action:@selector(showalert) forControlEvents:UIControlEventTouchUpInside];
    [a2 setImage:[UIImage imageNamed:@"Save_hist.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random2 = [[UIBarButtonItem alloc] initWithCustomView:a2];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = random2;
    
    
    
    
    
    
    
    if ([MFMailComposeViewController canSendMail])
        button.enabled = YES;
    
    if ([Vraagantwoord count] ==0) {
        
    }
    else
        
    {
        table.delegate = self;
        table.dataSource = self;
        [table reloadData];
        
    }
    
    
    [super viewDidLoad];
    
    
}


-(void)viewanimate

{
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];
    
    
    
    
    if (monthView.frame.size.height == 22) {
        [monthView setFrame:CGRectMake(220, 20,80,22*3)];
        [monthView.table setFrame:CGRectMake(0.0, 22, 80, 22*3)];
        
    }
    else
    {
        [monthView setFrame:CGRectMake(220, 20,80,22)];
        [monthView.table setFrame:CGRectMake(0.0, 22, 80, 22)];
        
    }
    
    [UIView commitAnimations];
    
}





-(void)chosenMonth:(NSString*)sender

{
    
    
    ////NSLog(@"%@", sender);
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];
    
    table.delegate = NULL;
    table.dataSource = NULL;
    [Vraagantwoord removeAllObjects];
    [HistSectionListDate removeAllObjects];
    [SectionList removeAllObjects];
    
    

    if ([sender isEqualToString:@"By Name"]) {
        
        
        [table setFrame:CGRectMake(0.0, 75, 320, self.view.bounds.size.height-120)];
        [sectionHeader setText:@"Medicines Sorted By Name"];
        [MonthButton setTitle:@"By Name" forState:UIControlStateNormal];
        [MonthButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        SetOrder = @"Name";
        
            NSArray *profileItem =[NSArray arrayWithObjects:@"Name", @"ID", @"Image", @"ColorRed", @"ColorGreen", @"Colorblue", @"Coloralpha", @"Audio",@"Silence",@"Vibrate",nil];
        
        [self setItems:[[GetData getProfiles:profileItem] objectAtIndex:[[[GetData getProfiles:profileItem] valueForKey:@"ID"] indexOfObject:chosenID]]];
        
  
        
        table.delegate = self;
        table.dataSource = self;
        [table reloadData];
        

    
    
    }
    else
        
    {
        
        [table setFrame:CGRectMake(0.0, 60, 320, self.view.bounds.size.height-120)];
        [sectionHeader setText:@"Medicines sorted By Date"];
        [MonthButton setTitle:@"By Date" forState:UIControlStateNormal];
        [MonthButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        SetOrder = @"Date";
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"mm/dd/yy"];
        
        NSComparator compareDates = ^(id string1, id string2) {
            NSDate *date1 = [formatter dateFromString:string1];
            NSDate *date2 = [formatter dateFromString:string2];
            
            return [date1 compare:date2];
        };
        
        
        
        HistSectionListDate =[GetData getHistoryTotal];
     
        
        
        table.delegate = self;
        table.dataSource = self;
        [table reloadData];
        
        
    }
    
        [UIView commitAnimations];

    
        
            
}

-(void) Makepdf
{
    
    
       //NSLog(@"%@",HistSectionList);
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date_time" ascending:FALSE];
    [HistSectionList sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    
    MFMailComposeViewController *composer = [[MFMailComposeViewController alloc] init];
    [composer setMailComposeDelegate:self];
    NSString *recipeTitle = @"<h5>Profile name: ";
    recipeTitle = [recipeTitle stringByAppendingString:[make valueForKey:@"Name"]];
    recipeTitle = [recipeTitle stringByAppendingString:@"</h5>"];
    
    // 450;
    
    //
    NSString *tableFirstLine = @"<TABLE border=\"1\" valign=\"top\" width=\"1200\" bordercolor=\"#e6e6e6\" cellpadding=\"5\" cellspacing=\"1\"><tr><td width=\"100\" align=\"center\" bgcolor=\"#38a6b0\"><font face=\"Arial\" color=\"fffff\"><p style=\"margin-left:125px\">Scheduled/Date</p></font></td><td width=\"100\" align=\"center\" bgcolor=\"#38a6b0\"><font face=\"Arial\" color=\"fffff\"><p style=\"margin-left:125px\">Scheduled/Time</p></font></td><td width=\"100\" align=\"center\" bgcolor=\"#38a6b0\"><font face=\"Arial\" color=\"fffff\"><p style=\"margin-left:125px\">Medicine/name</p></font></td><td width=\"100\" align=\"center\" bgcolor=\"#38a6b0\"><font face=\"Arial\" color=\"fffff\"><p style=\"margin-left:125px\">Strength</p></font></td><td width=\"100\" align=\"center\" bgcolor=\"#38a6b0\"><font face=\"Arial\" color=\"fffff\"><p style=\"margin-left:125px\">Dose</p></font><td width=\"100\" align=\"center\" bgcolor=\"#38a6b0\"><font face=\"Arial\" color=\"fffff\"><p style=\"margin-left:125px\">Instructions</p></font><td width=\"100\" align=\"center\" bgcolor=\"#38a6b0\"><font face=\"Arial\" color=\"fffff\"><p style=\"margin-left:125px\">Action/Date</p></font><td width=\"200\" align=\"center\" bgcolor=\"#38a6b0\"><font face=\"Arial\" color=\"fffff\"><p style=\"margin-left:125px\">Action/Time</p></font><td width=\"100\" align=\"center\" bgcolor=\"#38a6b0\"><font face=\"Arial\" color=\"fffff\"><p style=\"margin-left:125px\">Action</p></font><td width=\"150\" align=\"center\" bgcolor=\"#38a6b0\"><font face=\"Arial\" color=\"fffff\"><p style=\"margin-left:125px\"> Remarks </p></font></td></tr></td>";
    NSString *increments = @"";
    
    
    increments = [increments stringByAppendingString:recipeTitle];
    increments = [increments stringByAppendingString:tableFirstLine];
    
    
    for (int i=0; i < [HistSectionList count]; i++) {
        
        
     
        
        NSDateFormatter *set = [[NSDateFormatter alloc] init];
        [set setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        
        [set setDateFormat:@"dd/MM/yyyy"];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        
        [format setDateFormat:@"dd/MM/yyyy"];
        
   
        ////NSLog(@"%@", [format stringFromDate:[set dateFromString:[[HistSectionList objectAtIndex:i] valueForKey:@"Date"]]]);
        
        
        increments = [increments stringByAppendingString:@"<tr><td width=\"200\" align=\"center\"><font face=\"Arial\"  font size=\"1pt\" color=\"38a6b0\">"];
        increments = [increments stringByAppendingString:[format stringFromDate:[set dateFromString:[[HistSectionList objectAtIndex:i] valueForKey:@"Date"]]]];
        
        

        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.50\" color=\"#38a6b0\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Original_Time"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Name"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Dose"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\%\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Quantity"]];
           increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Type"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Action"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Date"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        increments = [increments stringByAppendingString:@"</td><td width=\"200\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
        increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Time_Action"]];
        //increments = [increments stringByAppendingString:@"</td></tr>"];
        
        
        
        
        
      
        
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]isEqualToString:@"Taken"]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#125c0f\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]isEqualToString:@"Taken earlier"]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#126362\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]isEqualToString:@"Taken late"]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#f2351a\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]isEqualToString:@"Missed"]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#f2351a\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]isEqualToString:@"Skipped"]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#d45a19\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        if ([[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"] isEqualToString:@"Postponed"]) {
            
            
            increments = [increments stringByAppendingString:@"</td><td width=\"200\" bgcolor=\"#0f006d\" align=\"center\"><font face=\"Arial\" font size=\"0.30\" color=\"#38a6b0\">"];
            increments = [increments stringByAppendingString:[[HistSectionList objectAtIndex:i] valueForKey:@"Taken"]];
            //increments = [increments stringByAppendingString:@"</td></tr>"];
        }
        
        
        
        
        
        increments = [increments stringByAppendingString:@"</td></tr>"];
        
    }
    if ([MFMailComposeViewController canSendMail]) {
        
        
        if ([self.navigationItem.title isEqualToString:@"Profile History"]) {
        
        //CF MedCare Medicine History Data en ook medicen document moet of profile en Medince
        [composer setToRecipients:[NSArray arrayWithObjects:@"", nil]];
        [composer setSubject:@"CF MedCare Profile History Data"];
        [composer setMessageBody:increments isHTML:YES];
        [composer setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
         [self presentViewController:composer animated:YES completion:NULL];
        [composer.navigationBar setTintColor:[UIColor colorWithRed:0.125 green:0.318 blue:0.949 alpha:1.000]];
        [composer.navigationBar setBackgroundImage:[UIImage imageNamed:@"balk.png"] forBarMetrics:0];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [appDelegate.toolbarDown setAlpha:0];
            
        }
        
        else
            
        {
            
            //CF MedCare Medicine History Data en ook medicen document moet of profile en Medince
            [composer setToRecipients:[NSArray arrayWithObjects:@"", nil]];
            [composer setSubject:@"CF MedCare Medicine History Data"];
            [composer setMessageBody:increments isHTML:YES];
            [composer setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
             [self presentViewController:composer animated:YES completion:NULL];
            [composer.navigationBar setTintColor:[UIColor colorWithRed:0.125 green:0.318 blue:0.949 alpha:1.000]];
            [composer.navigationBar setBackgroundImage:[UIImage imageNamed:@"balk.png"] forBarMetrics:0];
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            [appDelegate.toolbarDown setAlpha:0];
            
        }
    }
    
}

-(void) MakeCSV
{
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date_time" ascending:FALSE];
    [HistSectionList sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    
    
    if ([self.navigationItem.title isEqualToString:@"Profile History"]) {
    
    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
    mailer.mailComposeDelegate = self;
    [mailer setSubject:@"CF MedCare Profile History Data"];
   
 
    NSMutableString *mainString=[[NSMutableString alloc]initWithString:@""];
    
    

    
    for(int i=0;i<[HistSectionList count];i++ ) {
  
        if (i==0) {
           
            [mainString appendFormat:@"\"Scheduled\nDate\",\"Scheduled\nTime\",\"Medicine name\",\"Strength\",\"Dose\",\"Instructions\",\"Action\nDate\",\"Action\nTime\",\"Action\",\"Remarks\""];
             [mainString appendFormat:@"\n"];
            
            
            NSString *string=[[HistSectionList objectAtIndex:i]objectForKey:@"Date"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@"\"%@\"",string];
            
            
            string=[[HistSectionList objectAtIndex:i]objectForKey:@"Original_Time"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@",\"%@\"",string];
            
            
            string=[[HistSectionList objectAtIndex:i]objectForKey:@"Name"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@",\"%@\"",string];
            
            string=[[HistSectionList objectAtIndex:i]objectForKey:@"Dose"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@",\"%@\"",string];
            
            string=[[HistSectionList objectAtIndex:i]objectForKey:@"Quantity"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            
            NSString *string2=[[HistSectionList objectAtIndex:i]objectForKey:@"Type"];
            string2=[string2 stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@",\"%@ %@\"",string, string2];
            
            string=[[HistSectionList objectAtIndex:i]objectForKey:@"Action"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@",\"%@\"",string];
            
            
            
            string=[[HistSectionList objectAtIndex:i]objectForKey:@"Date"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@",\"%@\"",string];
            
            
            
            
            string=[[HistSectionList objectAtIndex:i]objectForKey:@"Time_Action"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@",\"%@\"",string];
            
            
            string=[[HistSectionList objectAtIndex:i]objectForKey:@"Taken"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@",\"%@\"",string];
            
            [mainString appendFormat:@",\"%@\"",@"late"];
            
            
            [mainString appendFormat:@"\n"];
            

        }
        else
            
        {
        
        NSString *string=[[HistSectionList objectAtIndex:i]objectForKey:@"Date"];
        string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
        [mainString appendFormat:@"\"%@\"",string];
            
        
        string=[[HistSectionList objectAtIndex:i]objectForKey:@"Original_Time"];
        string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
        [mainString appendFormat:@",\"%@\"",string];
        
        
        string=[[HistSectionList objectAtIndex:i]objectForKey:@"Name"];
        string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
        [mainString appendFormat:@",\"%@\"",string];
            
            string=[[HistSectionList objectAtIndex:i]objectForKey:@"Dose"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@",\"%@\"",string];
            
            string=[[HistSectionList objectAtIndex:i]objectForKey:@"Quantity"];
            string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            
             NSString *string2=[[HistSectionList objectAtIndex:i]objectForKey:@"Type"];
            string2=[string2 stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            [mainString appendFormat:@",\"%@ %@\"",string, string2];
          
        string=[[HistSectionList objectAtIndex:i]objectForKey:@"Action"];
        string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
        [mainString appendFormat:@",\"%@\"",string];
            
   
        
        string=[[HistSectionList objectAtIndex:i]objectForKey:@"Date"];
        string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
        [mainString appendFormat:@",\"%@\"",string];
     
            
        
        
        string=[[HistSectionList objectAtIndex:i]objectForKey:@"Time_Action"];
        string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
        [mainString appendFormat:@",\"%@\"",string];
        
     
        string=[[HistSectionList objectAtIndex:i]objectForKey:@"Taken"];
        string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
        [mainString appendFormat:@",\"%@\"",string];
            
            [mainString appendFormat:@",\"%@\"",@"late"];
      
   
        [mainString appendFormat:@"\n"];
            
        }
    }
    
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
         NSString *documentsDirectoryPath = [NSString stringWithFormat:@"%@/Caches/",[paths objectAtIndex:0]];
        NSString *filePath = [documentsDirectoryPath  stringByAppendingPathComponent:@"ProfileHistoryData.csv"];
        
        
        NSData* settingsData;
        settingsData = [mainString dataUsingEncoding: NSASCIIStringEncoding];
        
        if ([settingsData writeToFile:filePath atomically:YES])
            ////NSLog(@"writeok");
            
            
            [mailer addAttachmentData:settingsData mimeType:@"text/cvs" fileName:@"ProfileHistoryData.csv"];
         mailer.navigationBar.tintColor =[UIColor colorWithRed:0.125 green:0.318 blue:0.949 alpha:1.000];
            [self presentModalViewController:mailer animated:YES];
    }
    
    else
    {
        
        
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"CF MedCare Medicine History Data"];
        
        
        NSMutableString *mainString=[[NSMutableString alloc]initWithString:@""];
      
        
        
        for(int i=0;i<[HistSectionList count];i++ ) {
            
            if (i==0) {
                
                [mainString appendFormat:@"\"Scheduled\nDate\",\"Scheduled\nTime\",\"Medicine name\",\"Strength\",\"Dose\",\"Instructions\",\"Action\nDate\",\"Action\nTime\",\"Action\",\"Remarks\""];
                [mainString appendFormat:@"\n"];
                
                
                NSString *string=[[HistSectionList objectAtIndex:i]objectForKey:@"Date"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@"\"%@\"",string];
                
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Original_Time"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Name"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Dose"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Quantity"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                
                NSString *string2=[[HistSectionList objectAtIndex:i]objectForKey:@"Type"];
                string2=[string2 stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@ %@\"",string, string2];
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Action"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Date"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Time_Action"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Taken"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                [mainString appendFormat:@",\"%@\"",@"late"];
                
                
                [mainString appendFormat:@"\n"];
                
                
            }
            else
                
            {
                
                NSString *string=[[HistSectionList objectAtIndex:i]objectForKey:@"Date"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@"\"%@\"",string];
                
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Original_Time"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Name"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Dose"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Quantity"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                
                NSString *string2=[[HistSectionList objectAtIndex:i]objectForKey:@"Type"];
                string2=[string2 stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@ %@\"",string, string2];
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Action"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Date"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Time_Action"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                
                string=[[HistSectionList objectAtIndex:i]objectForKey:@"Taken"];
                string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                [mainString appendFormat:@",\"%@\"",string];
                
                [mainString appendFormat:@",\"%@\"",@"late"];
                
                
                [mainString appendFormat:@"\n"];
                
            }
        }
        
        
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
             NSString *documentsDirectoryPath = [NSString stringWithFormat:@"%@/Caches/",[paths objectAtIndex:0]];
            NSString *filePath = [documentsDirectoryPath  stringByAppendingPathComponent:@"MedicineHistoryData.csv"];
            
            
            NSData* settingsData;
            settingsData = [mainString dataUsingEncoding: NSASCIIStringEncoding];
            
            if ([settingsData writeToFile:filePath atomically:YES])
                ////NSLog(@"writeok");
                
                
                [mailer addAttachmentData:settingsData mimeType:@"text/cvs" fileName:@"MedicineHistoryData.csv"];
         mailer.navigationBar.tintColor =[UIColor colorWithRed:0.125 green:0.318 blue:0.949 alpha:1.000];
        [self presentViewController:mailer animated:YES completion:NULL];
        
    }
    
    

 
}


-(void) backAction
{
    
    
    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)showalert

{
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Send History in Email\nor in CSV file"];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464749];
    [alert addButtonWithTitle:@"Email"];
    [alert addButtonWithTitle:@"CSV"];
    [alert show];
}


-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{
    
    
    if (index==1) {
        
        
        [self MakeCSV];
        
    }
    
    else
    {
        
        [self Makepdf];
        
        
    }
}

-(void) CreateAccount

{
    NewProfileController *controller = [[NewProfileController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void)setItemsFrom:(NSMutableDictionary*)set andID:(NSString*)Medid;

{
      [table setFrame:CGRectMake(0.0, 60, 320, self.view.bounds.size.height-120)];
    [sectionHeader setAlpha:0];
        [self.navigationItem setTitle:@"Medicine History"];
    [monthView setAlpha:0];
    [MonthButton setAlpha:0];
    SetOrder =@"Name";
    
    MedID =Medid;
    NSArray *profileItem =[NSArray arrayWithObjects:@"ID",@"Date",@"Time",@"Name",@"Dose",@"Taken",@"ID_Medicine",@"Count",@"Time_Action",@"Action",@"Quantity",@"Type", @"Original_Time", @"Action_Date",nil];
    
    HistSectionList =[[NSMutableArray alloc]init];
    
    
    SectionList =[[NSMutableArray alloc]init];
    
    
    
    Vraagantwoord =[[NSMutableArray alloc]init];
    
    
    
    HistSectionList= [GetData getHistory:profileItem OnMedID:Medid];
    
    
    if ([HistSectionList count]==0) {
        
        
        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [a1 setTag:44];
        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
    }
    else
    {
        
        
        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [a1 setTag:44];
        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
        
        
        DateList =[[NSMutableArray alloc]init];
        
        Vraagantwoord =[[NSMutableArray alloc]init];
        DateResult =[[NSMutableArray alloc]init];
        
        chosenID = [set valueForKey:@"ID"];
        
        
        UIColor *colorit = [UIColor colorWithRed:[[set valueForKey:@"ColorRed"] floatValue] green:[[set valueForKey:@"ColorGreen"]  floatValue] blue:[[set valueForKey:@"Colorblue"]  floatValue] alpha:[[set valueForKey:@"Coloralpha"]  floatValue]];
        
        const CGFloat *components = CGColorGetComponents(colorit.CGColor);
        rood = components[0];
        groen = components[1];
        blauw = components[2];
        doorzicht = components[3];
        
        
        
        [ProfileView setBackgroundColor:colorit];
        ProfileView.layer.shadowOffset = CGSizeMake(3, 0);
        ProfileView.layer.shadowOpacity = 2;
        ProfileView.layer.shadowRadius = 2.0;
        [NameLabel setText:[set valueForKey:@"Name"]];
        [headShot setImage:[set valueForKey:@"Image"]];
        
        
         
        
        
        [SectionList addObject:[[NSMutableArray alloc]init]];
        
        for (int i=0 ; i<[[GetData getHistory:profileItem OnMedID:MedID] count]; i++) {
            
            [[SectionList objectAtIndex:0] addObject:[[GetData getHistory:profileItem OnMedID:MedID] objectAtIndex:i]];
            
            
            
            
        }
        
        for (int i=0 ; i<[SectionList count]; i++) {
            
            NSDictionary *countriesLivedInDict = [NSDictionary dictionaryWithObject:[SectionList objectAtIndex:i] forKey:@"Name"];
            [Vraagantwoord addObject:countriesLivedInDict];
            
            
        }
        
        
        
        if ([Vraagantwoord count] ==0) {
            
        }
        else
            
        {
            table.delegate = self;
            table.dataSource = self;
            [table reloadData];
            
        }
        
        
        
        
        
    }
    
    
    
}



-(void)setItems:(NSMutableDictionary*)set

{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"mm/dd/yy"];
    
    NSComparator compareDates = ^(id string1, id string2) {
        NSDate *date1 = [formatter dateFromString:string1];
        NSDate *date2 = [formatter dateFromString:string2];
        
        return [date1 compare:date2];
    };
    
    
    
    SetOrder =@"Name";
    
    
    HistSectionList =[[NSMutableArray alloc]init];
    
    
    SectionList =[[NSMutableArray alloc]init];
    
    
    
    Vraagantwoord =[[NSMutableArray alloc]init];
    
    
    MedID =[set valueForKey:@"ID"];
    NSArray *profileItem =[NSArray arrayWithObjects:@"ID",@"Date",@"Time",@"Name",@"Dose",@"Taken",@"ID_Medicine",@"Count",@"Time_Action",@"Action",@"Quantity",@"Type", @"Time_Action_24",@"Original_Time",@"Action_Date",nil];
    
  

    
   HistSectionList= [GetData getHistory:profileItem OnID:[set valueForKey:@"ID"]];
    
    

    
    if ([HistSectionList count]==0) {
        
    }
    else
    {
        
        
        chosenID = [set valueForKey:@"ID"];
     
        
        
        
        UIColor *colorit = [UIColor colorWithRed:[[set valueForKey:@"ColorRed"] floatValue] green:[[set valueForKey:@"ColorGreen"]  floatValue] blue:[[set valueForKey:@"Colorblue"]  floatValue] alpha:[[set valueForKey:@"Coloralpha"]  floatValue]];
        
        const CGFloat *components = CGColorGetComponents(colorit.CGColor);
        rood = components[0];
        groen = components[1];
        blauw = components[2];
        doorzicht = components[3];
        
        
        
        [ProfileView setBackgroundColor:colorit];
        ProfileView.layer.shadowOffset = CGSizeMake(3, 0);
        ProfileView.layer.shadowOpacity = 2;
        ProfileView.layer.shadowRadius = 2.0;
        [NameLabel setText:[set valueForKey:@"Name"]];
        [headShot setImage:[set valueForKey:@"Image"]];
        
        
        NSMutableArray *history =[[NSMutableArray alloc] init];
        history = [GetData getHistory:profileItem OnID:chosenID];
        
        NSSortDescriptor *sortByDate = [[NSSortDescriptor alloc] initWithKey:@"Original_Time" ascending:NO];
        NSSortDescriptor *sortByID = [[NSSortDescriptor alloc] initWithKey:@"ID" ascending:YES];
        [history sortUsingDescriptors:[NSArray arrayWithObjects:sortByDate, sortByID, nil]];
        
        
        NSLog(@" history %@", history);
        
        for (int i=0 ; i<[history count]; i++) {
             [SectionList addObject:[[NSMutableArray alloc]init]];
            
            if ([[history valueForKey:@"ID_Medicine"] containsObject:[[history valueForKey:@"ID_Medicine"] objectAtIndex:i]]) {
                
                [[SectionList objectAtIndex:[[history valueForKey:@"ID_Medicine"] indexOfObject:[[history valueForKey:@"ID_Medicine"] objectAtIndex:i]]] addObject:[history objectAtIndex:i]];
                
                
            }
            
        }
        
   
        
        
        for (int i=0 ; i<[SectionList count]; i++) {
            
            NSDictionary *countriesLivedInDict = [NSDictionary dictionaryWithObject:[SectionList objectAtIndex:i] forKey:@"Name"];
            
          
            NSArray *array = [countriesLivedInDict objectForKey:@"Name"];
            
           
            if ([array count] ==0) {
                
            }
            else
            {
            
                
                [Vraagantwoord addObject:countriesLivedInDict];
                
            }
            
            
        }
        
      
        
        
        if ([Vraagantwoord count] ==0) {
            
        }
        else
            
        {
            table.delegate = self;
            table.dataSource = self;
            [table reloadData];
            
        }
        
    }
    
    
    
    for (id key in Vraagantwoord)
    {
     
            
                 ////NSLog(@"%@", key);
        
    }
    
}


- (void)Offerte

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    

    
    
    
}






- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self becomeFirstResponder];
	[self dismissViewControllerAnimated:YES completion:NULL];
}




-(void)MultiPop

{
    
    ////NSLog(@"MultiPop");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    
   appDelegate.Multicontroller = [[MultipleViewController alloc]init];
    
    
    [self presentViewController:appDelegate.Multicontroller animated:YES completion:Nil];
    [appDelegate.Multicontroller reloaddata];
      
}

-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:1];
    
}



-(void)PresentView:(UIButton*)sender

{
    
}

- (IBAction)instructie:(UIButton *)sender {
    
    
    
}

- (IBAction)Vragen:(UIButton *)sender {
    
    
}


- (IBAction)bijsluiter:(UIButton *)sender {
    
    
    
}

- (IBAction)about:(UIButton *)sender {
    
    
    
}


- (IBAction)Cancel:(UIButton *)sender {
    
    
}

- (IBAction)OK:(UIButton *)sender {
    
    
    
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ([SetOrder isEqualToString:@"Name"])
        return [Vraagantwoord count];
    else
        return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *sectionHeader2;
    if ([SetOrder isEqualToString:@"Name"])
    {
        NSDictionary *dictionary = [Vraagantwoord objectAtIndex:section];
        NSArray *array = [dictionary objectForKey:@"Name"];
        
        
        NSMutableDictionary *notif = [array objectAtIndex:0];
        
        NSString *result = [notif valueForKey:@"Name"];
        
        
        sectionHeader2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 15)];
        sectionHeader2.backgroundColor = [UIColor colorWithRed:0.235 green:0.435 blue:0.953 alpha:1.000];
        sectionHeader2.textColor = [UIColor whiteColor];
        sectionHeader2.font = [UIFont boldSystemFontOfSize:16];
        sectionHeader2.text =  result;
        sectionHeader2.textAlignment=NSTextAlignmentCenter;
        
        sectionHeader2.layer.shadowOffset = CGSizeMake(3, 0);
        sectionHeader2.layer.shadowOpacity = 1;
        sectionHeader2.layer.shadowRadius = 1.0;
        
         [sectionHeader2 setHidden:NO];
        
    }
    else
    {
        
      
        [sectionHeader2 setHidden:YES];
    }
    
    if ([SetOrder isEqualToString:@"Name"])
        return sectionHeader2;
    else 
        return sectionHeader2;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *array;
    if ([SetOrder isEqualToString:@"Name"])
    {
        //Number of rows it should expect should be based on the section
        NSDictionary *dictionary = [Vraagantwoord objectAtIndex:section];
        array = [dictionary objectForKey:@"Name"];
    }
    
    if ([SetOrder isEqualToString:@"Name"])
        return [array count];
    else
        return  [HistSectionListDate count];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *result;
    if ([SetOrder isEqualToString:@"Name"])
        
    {
        NSDictionary *dictionary = [Vraagantwoord objectAtIndex:section];
        NSArray *array = [dictionary objectForKey:@"Name"];
        NSMutableDictionary *notif = [array objectAtIndex:0];
        result = [notif valueForKey:@"Name"];
        
    }
    else
    {
        
    }
    
    
    ////NSLog(@"%@", [array objectAtIndex:0]);
    if ([SetOrder isEqualToString:@"Name"])
        return result;
    else
        return NULL;
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(HistoryCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if ([SetOrder isEqualToString:@"Name"])
    {
        NSDictionary *dictionary = [Vraagantwoord objectAtIndex:indexPath.section];
        NSArray *array = [dictionary objectForKey:@"Name"];
        NSMutableDictionary *notif = [array objectAtIndex:indexPath.row];
        
        
        ////NSLog(@"notif %@", notif);
        
        [cell FillAllItems:notif];
        
    }
    else
    {
        
        [cell FillAllItems:[HistSectionListDate objectAtIndex:indexPath.row]];
    }
    
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    return 80;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    
    
    
    
    
    
}


- (HistoryCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    NSString *identifier = @"CellIdentifier";
    HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
	if (cell == nil) {
        
        cell = [[HistoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    else
        
    {
        
    }
    
    
    return cell;
    
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{
    ////NSLog////NSLog(@"turnbuttons");
}

@end
