    //
    //  ViewController.m
    //  Menopur
    //
    //  Created by Jeffrey Snijder on 20-01-13.
    //  Copyright (c) 2013 Menopur. All rights reserved.
    //

#import "HoursViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "TimeSchedulare.h"
#import "FrequencyViewControllerNew.h"
#import "PickerViewFrequentie.h"
@interface HoursViewController ()

@end

@implementation HoursViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize theTime;
@synthesize theDay;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize underTitleText;
@synthesize theDate;
@synthesize pickerTaal;
@synthesize titleEdit;
@synthesize countDict;


-(void)loadView{
    
    
    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    
    parantLabel =(TekstIDLabel*)set;
    
    [TitleText setText:set.text];
    if ([self.title isEqualToString:NSLocalizedString(@"Every X Hours",nil)]) {
        
        
        if ([TitleText.text length]<4) {
            
            
            if ([TitleText.text length] ==0) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@   %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
            }
            if ([TitleText.text length] ==1) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@     %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
            }
            if ([TitleText.text length] ==2) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@      %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
            }
            if ([TitleText.text length] ==3) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@        %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
            }
            
            
        }
        else
        {
            [TitleText setText:@""];
            [underTitleText setText:[NSString stringWithFormat:@"%@  %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
        }
    }
    
    
    
    
    else if ([self.title isEqualToString:NSLocalizedString(@"Every X Days",nil)])
    {
        if ([TitleText.text length]<4) {
            
            
            if ([TitleText.text length] ==0) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@   %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
            }
            if ([TitleText.text length] ==1) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@     %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
            }
            if ([TitleText.text length] ==2) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@      %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
            }
            if ([TitleText.text length] ==3) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@        %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
            }
            
            
        }
        else
        {
            [TitleText setText:@""];
            [underTitleText setText:[NSString stringWithFormat:@"%@  %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
        }
    }
    
    
    
}



- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];
    
    
    //     [self.view.layer setMasksToBounds:YES];
    
    
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
    }
    
    else
    {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
    
    
    if ([self.title isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
        
        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
            //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            
            self.navigationItem.title =NSLocalizedString(@"Every X Days",nil);
                // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:20.0];
            label.textAlignment = NSTextAlignmentCenter;
                // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            [label sizeToFit];
            
        }
        
        
        TimeChoses =[[NSMutableArray alloc]init];
        
        
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        appDelegate.Notevariable =@" ";
        
        
        
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(40, 0.0f, 240, 20)];
        [title setTextColor:[UIColor whiteColor]];
        [title setText:@"Enter Hours"];
        [title setFont:[UIFont boldSystemFontOfSize:18]];
        [title setTextAlignment:NSTextAlignmentCenter];
        [title setTag:120];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setNumberOfLines:3];
        
        
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
        
        
        Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 24, self.view.frame.size.width-20, 44)];
        [Combi setBackgroundColor:[UIColor whiteColor]];
        [Combi.layer setCornerRadius:10];
        [Combi.layer setBorderWidth:1.5];
//        [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; // 02-010-2017
        [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];

        //[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000] 129 144 199
        [Combi.layer setMasksToBounds:YES];
        [self.view addSubview:Combi];
        
        
        
        underTitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 28, self.view.frame.size.width-40, 35)];
        [underTitleText setBackgroundColor:[UIColor whiteColor]];
        [underTitleText setDelegate:self];
//        [underTitleText setText:[NSString stringWithFormat:@"%@  %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]]; //03-10-2017
        [underTitleText setText:[NSString stringWithFormat:@"%@    %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
        [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        
        
        [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
        [self.view addSubview:underTitleText];
        
        
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
            
        {
        
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
        
            
                }
            
            else    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
                
            {
            
                TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
                [TitleText setBackgroundColor:[UIColor clearColor]];
                [TitleText setDelegate:self];
                [TitleText setText:@""];
                [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
                [TitleText setTag:123+1];
        
                
                
                                }
                
                else    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
                    
                {
                
                    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(49, 28, self.view.frame.size.width-40, 35)];
                    [TitleText setBackgroundColor:[UIColor clearColor]];
                    [TitleText setDelegate:self];
                    [TitleText setText:@""];
                    [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
                    [TitleText setTag:123+1];
                    
                    
                                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
                    
                {
                    
                    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(49, 28, self.view.frame.size.width-40, 35)];
                    [TitleText setBackgroundColor:[UIColor clearColor]];
                    [TitleText setDelegate:self];
                    [TitleText setText:@""];
                    [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
                    [TitleText setTag:123+1];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
                    
                {
                    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(49, 28, self.view.frame.size.width-40, 35)];
                    [TitleText setBackgroundColor:[UIColor clearColor]];
                    [TitleText setDelegate:self];
                    [TitleText setText:@""];
                    [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
                    [TitleText setTag:123+1];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
                {
                    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
                    [TitleText setBackgroundColor:[UIColor clearColor]];
                    [TitleText setDelegate:self];
                    [TitleText setText:@""];
                    [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
                    [TitleText setTag:123+1];
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
                    
                {
                    
                    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
                    [TitleText setBackgroundColor:[UIColor clearColor]];
                    [TitleText setDelegate:self];
                    [TitleText setText:@""];
                    [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
                    [TitleText setTag:123+1];
                    
                    
                }
                else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
                    
                {
                    
                    [underTitleText setFrame:CGRectMake(14, 28, self.view.frame.size.width-40, 35)];
                    
                    
                    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(49, 28, self.view.frame.size.width-40, 35)];
                    [TitleText setBackgroundColor:[UIColor clearColor]];
                    [TitleText setDelegate:self];
                    [TitleText setText:@""];
                    [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
                    [TitleText setTag:123+1];
                }
                    
       else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
            
        {
            
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
            
        }
       else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
       {
           
           TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
           [TitleText setBackgroundColor:[UIColor clearColor]];
           [TitleText setDelegate:self];
           [TitleText setText:@""];
           [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
           [TitleText setTag:123+1];
           
           
       }
       else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
       {
           
           TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
           [TitleText setBackgroundColor:[UIColor clearColor]];
           [TitleText setDelegate:self];
           [TitleText setText:@""];
           [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
           [TitleText setTag:123+1];
           
           
       }


        [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue], 28, self.view.frame.size.width-40, 35)];

        
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
        {
            [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] + 10, 28, self.view.frame.size.width-40, 35)];
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
        {
            [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] -20, 28, self.view.frame.size.width-40, 35)];
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
        {
            [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] - 2 , 28, self.view.frame.size.width-40, 35)];
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
        {
            [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] + 4 , 28, self.view.frame.size.width-40, 35)];
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
        {
            [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] + 7 , 28, self.view.frame.size.width-40, 35)];
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
        {
            [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] + 7 , 28, self.view.frame.size.width-40, 35)];
        }
        
        [TitleText setFont:[UIFont boldSystemFontOfSize:15]];
        [self.view addSubview:TitleText];
        
        
        
        NSDate *now = [[NSDate alloc] init];
        
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [format setDateFormat:@"dd/MM/yyyy"];
        theDate = [format stringFromDate:now];
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [dateFormatter setDateFormat:@"EEEE"];
        
        theDay = [dateFormatter stringFromDate:now];
        
        
        
        
        
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [timeFormat setDateFormat:@"hh:mm"];
        theTime = [timeFormat stringFromDate:now];
        
        
        
        
        
        
        NSDateFormatter *timeFormat2 = [[NSDateFormatter alloc] init];
        [timeFormat2 setDateFormat:@"a"];
        timeframe = [timeFormat2 stringFromDate:now];
        
        
        
        
        
        pickerTaal = [[PickerViewFrequentie alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, 320, 300)];
        [pickerTaal setparant:self];
        [self.view addSubview:pickerTaal];
        
        [TitleText setKeyboardType:UIKeyboardTypeDecimalPad];
        
    }
    
    
    else if ([self.title isEqualToString:NSLocalizedString(@"Every X Hours",nil)])
        
    {
        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
            //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            
            self.navigationItem.title =NSLocalizedString(@"Every X Hours",nil);
                // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:20.0];
            label.textAlignment = NSTextAlignmentCenter;
                // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            [label sizeToFit];
            
        }
        
        
        TimeChoses =[[NSMutableArray alloc]init];
        
        
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
        
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        appDelegate.Notevariable =@" ";
        
        
        
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(40, 0.0f, 240, 20)];
        [title setTextColor:[UIColor whiteColor]];
        [title setText:@"Enter Hours"];
        [title setFont:[UIFont boldSystemFontOfSize:18]];
        [title setTextAlignment:NSTextAlignmentCenter];
        [title setTag:120];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setNumberOfLines:3];
        
        
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
        
        
        Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 24, self.view.frame.size.width-20, 44)];
        [Combi setBackgroundColor:[UIColor whiteColor]];
        [Combi.layer setCornerRadius:10];
        [Combi.layer setBorderWidth:1.5];
//        [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //02-10-2017
        [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];

        //        //[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000] 129 144 199

        [Combi.layer setMasksToBounds:YES];
        [self.view addSubview:Combi];
        
        
        
        underTitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 28, self.view.frame.size.width-40, 35)];
        [underTitleText setBackgroundColor:[UIColor whiteColor]];
        [underTitleText setDelegate:self];
        [underTitleText setText:[NSString stringWithFormat:@"%@  %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
        [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        
        
        [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
        [self.view addSubview:underTitleText];
        
        
        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
            
        {
            
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
        }
        else    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
            
        {
            
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
        }
        else    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
            
        {
            
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
        }
        
        else    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
            
        {
            
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
            
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
            
        {
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
        {
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
            
        {
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
        }
        
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
            
        {
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
        {
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
        {
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
        }
        else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
        {
            TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 28, self.view.frame.size.width-40, 35)];
            [TitleText setBackgroundColor:[UIColor clearColor]];
            [TitleText setDelegate:self];
            [TitleText setText:@""];
            [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [TitleText setTag:123+1];
            
        }


        
        [TitleText setFont:[UIFont boldSystemFontOfSize:15]];
        [self.view addSubview:TitleText];
        
        
        
        NSDate *now = [[NSDate alloc] init];
        
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [format setDateFormat:@"dd/MM/yyyy"];
        theDate = [format stringFromDate:now];
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [dateFormatter setDateFormat:@"EEEE"];
        
        theDay = [dateFormatter stringFromDate:now];
        
        
        
        
        
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [timeFormat setDateFormat:@"hh:mm"];
        theTime = [timeFormat stringFromDate:now];
        
        
        
        
        
        
        NSDateFormatter *timeFormat2 = [[NSDateFormatter alloc] init];
        [timeFormat2 setDateFormat:@"a"];
        timeframe = [timeFormat2 stringFromDate:now];
        
        
        
        
        
        pickerTaal = [[PickerViewFrequentie alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-300, 320, 300)];
        [pickerTaal setparant:self];
        [self.view addSubview:pickerTaal];
        
        [TitleText setInputView:pickerTaal];
        
        
    }
    
}

-(void)setChoice:(NSString*)gothere

{
    
}

-(void)Cancel:(UIBarButtonItem*) sender

{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([parantIt.Choicetimer count] ==0) {
        appDelegate.Notevariable =@"";
    }
    else
    {
        appDelegate.Notevariable =[NSString stringWithFormat:@"Every %lu Hours",(unsigned long)[parantIt.Choicetimer count]];
        
    }
    
    
    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)Save:(UIBarButtonItem*) sender

{
    

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
    [set setObject:@"Hours" forKey:@"choice"];
    [set setObject:[appDelegate newUUID] forKey:@"ID_Timer"];
    [set setObject:timeframe forKey:@"Moment"];
    [set setObject:theTime forKey:@"Time"];
    [set setObject:theDay forKey:@"CurrentDay"];
    [set setObject:theDate forKey:@"CurrentDate"];
    [set setObject:TitleText.text forKey:@"intervalOurs"];
    [set setObject:[self convertToOurs:TitleText.text] forKey:@"intervalDays"];
    [set setObject:theTime forKey:@"CurrentTime"];
    [set setObject:@"0" forKey:@"Diverence"];
    [set setObject:@"0" forKey:@"Day"];
    
    
    
    [TimeChoses addObject:set];
    
    
    [TitleText resignFirstResponder];
    
    appDelegate.Notevariable =[NSString stringWithFormat:@"Every %@ Hours", TitleText.text];
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
}


- (IBAction)doSomething:(UIButton *)sender {
    

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        
        
        [parantLabel setText:TitleText.text];
        
        
        if ([self.title isEqualToString:NSLocalizedString(@"Every X Hours",nil)]) {
            
            
            
            NSDateFormatter *format2 = [[NSDateFormatter alloc] init];
            [format2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            
            [format2 setDateFormat:[appDelegate convertString:@"hh:mm a"]];
            
            [self calculateInterval:[format2 dateFromString:parantIt.StartingTimeText.text] count:[TitleText.text intValue]];
            
            if ([parantIt.DayinaRow count]==0) {
                
                
                NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                [set setObject:parantIt.Name.text forKey:@"Date"];
                [set setObject:@" " forKey:@"DateAssent"];
                [set setObject:@"" forKey:@"Day_number"];
                [set setObject:parantIt.EndingViewText.text forKey:@"Ending"];
                [set setObject:TitleText.text forKey:@"Interval"];
                [set setObject:self.title forKey:@"Setting"];

                NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];

                NSDateFormatter *dateConverted = [[NSDateFormatter alloc]init];
                [dateConverted setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateConverted setDateFormat:@"dd/mm/yyyy"];

                [set setObject:[dateConvert dateFromString:parantIt.Name.text] forKey:@"Starting"];
                
                
                BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
                
                   if (strsetting) {
                     //NSLog(@"YES");
                }
                else
                    
                {
                    
                    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
                    [parantIt.table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
                    
                     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"am", @"a.m.", @"vorm.", @"μ.μ.", @"AM"];
                    
                    NSArray *notificationArray= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate];
                    
                    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"pm", @"p.m.", @"nachm.", @"π.μ.", @"PM"];
                    NSArray *notificationArray2= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate2];
                    
                    [parantIt.table.TimesSet removeAllObjects];
                    
                    [parantIt.table.TimesSet addObjectsFromArray:notificationArray];
                    [parantIt.table.TimesSet addObjectsFromArray:notificationArray2];
                    
                    
                    NSArray *copy = [parantIt.table.TimesSet copy];
                    NSInteger index = [copy count] - 1;
                    for (id object in [copy reverseObjectEnumerator]) {
                        if ([parantIt.table.TimesSet indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                            [parantIt.table.TimesSet removeObjectAtIndex:index];
                        }
                        index--;
                    }
               
                }

                NSString *uploadString = [[parantIt.table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
                [set setObject:uploadString forKey:@"Times"];
                [parantIt.DayinaRow  addObject:set];


                //////////////////////NSLog(@"%@", set);
                
                
                
            }
            else
                
            {
               for (NSInteger i=0 ; i<[parantIt.DayinaRow count]; i++) {
                    
                    
                    
                    
                    [[parantIt.DayinaRow objectAtIndex:i] removeObjectForKey:@"Interval"];
                    [[parantIt.DayinaRow objectAtIndex:i] setObject:TitleText.text forKey:@"Interval"];
                    
                    [[parantIt.DayinaRow objectAtIndex:i] removeObjectForKey:@"Setting"];
                    [[parantIt.DayinaRow objectAtIndex:i] setObject:self.title forKey:@"Setting"];
                    
                    
                    
                }
                
            }
            
            
            
        }
        else if ([self.title isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
            
            
            
            
            if ([parantIt.DayinaRow count]==0) {
                
                NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                [set setObject:parantIt.Name.text forKey:@"Date"];
                [set setObject:@" " forKey:@"DateAssent"];
                [set setObject:@"" forKey:@"Day_number"];
                [set setObject:parantIt.EndingViewText.text forKey:@"Ending"];
                [set setObject:TitleText.text forKey:@"Interval"];
                [set setObject:self.title forKey:@"Setting"];

                NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];

                NSDateFormatter *dateConverted = [[NSDateFormatter alloc]init];
                [dateConverted setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateConverted setDateFormat:@"dd/mm/yyyy"];

                [set setObject:[dateConvert dateFromString:parantIt.Name.text] forKey:@"Starting"];
                
                BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
                
                  if (strsetting) {
                    //NSLog(@"YES");
                }
                else
                    
                {
                    
                    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
                    [parantIt.table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
                    
                     NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"am", @"a.m.", @"vorm.", @"μ.μ.", @"AM"];
                    
                    NSArray *notificationArray= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate];
                    
                     NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"pm", @"p.m.", @"nachm.", @"π.μ.", @"PM"];
                    
                    NSArray *notificationArray2= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate2];
                    
                    [parantIt.table.TimesSet removeAllObjects];
                    
                    [parantIt.table.TimesSet addObjectsFromArray:notificationArray];
                    [parantIt.table.TimesSet addObjectsFromArray:notificationArray2];
                    
                 
                    
                    NSArray *copy = [parantIt.table.TimesSet copy];
                    NSInteger index = [copy count] - 1;
                    for (id object in [copy reverseObjectEnumerator]) {
                        if ([parantIt.table.TimesSet indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                            [parantIt.table.TimesSet removeObjectAtIndex:index];
                        }
                        index--;
                    }
                }


                NSString *uploadString = [[parantIt.table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
                
                [set setObject:uploadString forKey:@"Times"];
                [parantIt.DayinaRow  addObject:set];
                


                //////////////////////NSLog(@"%@", set);
                
            }
            else
                
            {
               for (NSInteger i=0 ; i<[parantIt.DayinaRow count]; i++) {
                    
                    
                    NSString *value =[NSString stringWithFormat:@"%i", [TitleText.text intValue]*24];
                    
                    [[parantIt.DayinaRow objectAtIndex:i] removeObjectForKey:@"Interval"];
                    [[parantIt.DayinaRow objectAtIndex:i] setObject:value forKey:@"Interval"];
                    
                    [[parantIt.DayinaRow objectAtIndex:i] removeObjectForKey:@"Setting"];
                    [[parantIt.DayinaRow objectAtIndex:i] setObject:self.title forKey:@"Setting"];
                    
                    
                    
                }
                
            }
            
            
        }
        
        
        
        
        /*
         
         [parantIt.DayinaRow setObject:value withKey:@"Interval"];
         
         [dictionary setObject:TitleText.text withKey:newKey];
         [dictionary removeObjectForKey:oldKey];"];
         [dictionary removeObjectForKey:oldKey];
         
         
         */
        parantIt.Interval =TitleText.text;
        [[self navigationController] popViewControllerAnimated:YES];
        
        
    });
    
    
    
}




-(void)calculateInterval:(NSDate*)sender count:(NSInteger)interval

{
    
    
    if (interval) {
        
        
        [parantIt.table.TimesSet removeAllObjects];
        
       for (NSInteger i=0 ; i<24/interval; i++) {
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            
            
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:sender];
            
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setHour:[timeComponents hour]+(interval*i)];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:0];
            
            NSDate *itemDate  = [calendar dateFromComponents:dateComps];
            
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            
            [dateFormat setDateFormat:[appDelegate convertString:@"hh:mm a"]];
            
            
            
            
            
            NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
            [set setObject:[dateFormat stringFromDate:itemDate] forKey:@"Time"];
            
            
            if ([dateFormat stringFromDate:itemDate]) {
                
                [parantIt.table.TimesSet addObject:set];
            }
            else
                
            {
                
                
            }
            
            
        }
    }
    
    else
    {
        
        
    }
    
    BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
      if (strsetting) {
         //NSLog(@"YES");
    }
    else
        
    {
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
        [parantIt.table.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
        
        NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) ", @"am", @"a.m.", @"vorm.", @"μ.μ.", @"AM"];
      
        
        NSArray *notificationArray= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate];
    
        
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"pm", @"p.m.", @"nachm.", @"π.μ.",  @"PM"];
        
        
        
        NSArray *notificationArray2= [parantIt.table.TimesSet filteredArrayUsingPredicate:predicate2];
        
        [parantIt.table.TimesSet removeAllObjects];
        
        [parantIt.table.TimesSet addObjectsFromArray:notificationArray];
        [parantIt.table.TimesSet addObjectsFromArray:notificationArray2];
        
        
        NSArray *copy = [parantIt.table.TimesSet copy];
        NSInteger index = [copy count] - 1;
        for (id object in [copy reverseObjectEnumerator]) {
            if ([parantIt.table.TimesSet indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                [parantIt.table.TimesSet removeObjectAtIndex:index];
            }
            index--;
        }
        
    
    }

    
}


-(NSString*) convertToOurs:(NSString*)ours

{
    
    
    return [NSString stringWithFormat:@"%i", [ours intValue]/24];
}

-(void)change:(UIButton*)sender

{
    
    if (sender.tag ==140 ||sender.tag ==180) {
        
        
    }
    if (sender.tag ==141 ||sender.tag ==181) {
        
    }
    if (sender.tag ==142 ||sender.tag ==182) {
        
        
        TimesofDayViewController *controller = [[TimesofDayViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (sender.tag ==143 ||sender.tag ==183) {
        
    }
    if (sender.tag ==144 ||sender.tag ==184) {
        
    }
    
    
    
    
    
    
}

-(void)Action:(UIButton*)sender

{
    
    
}

-(void)changeChose:(NSString*)sender

{
    
    
    chosen = sender;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    
    TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
    [TitleText becomeFirstResponder];
}


-(void) backButtonPressed
{
    
    if (TitleText.editable == NO) {
        
        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];
        
        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
        [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];
        
            //self.navigationItem.leftBarButtonItem = nil;
        
        
        
    }
    else
    {
        
        
        
        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];
        
//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
            //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
            [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
        
    }
    
    
}



-(void) setTextview: (NSString *) text
{
    
    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [TitleText setText:text];
        
    });
    
}

#pragma mark - UITextField - Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    
    
    
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    ////////////////NSLog(@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]);
    
    [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] , 28, self.view.frame.size.width-40, 35)];

    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
    {
        [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] + 10, 28, self.view.frame.size.width-40, 35)];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
    {
        [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] -20 , 28, self.view.frame.size.width-40, 35)];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {
        [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] - 2 , 28, self.view.frame.size.width-40, 35)];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
    {
        [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] + 4 , 28, self.view.frame.size.width-40, 35)];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
    {
        [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] + 7 , 28, self.view.frame.size.width-40, 35)];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
    {
        [TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue] + 7 , 28, self.view.frame.size.width-40, 35)];
    }


    
    
    
    if ([self.title isEqualToString:NSLocalizedString(@"Every X Hours",nil)]) {
        
        if ([textView.text length]<4) {
            
            
            if ([textView.text length] ==0) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@   %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
            }
            if ([textView.text length] ==1) {
                
                
                [underTitleText setText:[NSString stringWithFormat:@"%@     %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
            }
            if ([textView.text length] ==2) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@      %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
            }
            if ([textView.text length] ==3) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@        %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"DHoursays",nil)]];
            }
            
            
        }
        else
        {
            [textView setText:@""];
            [underTitleText setText:[NSString stringWithFormat:@"%@   %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
        }
    }
    
    
    
    
    else if ([self.title isEqualToString:NSLocalizedString(@"Every X Days",nil)])
    {
    
        
        if ([textView.text length]<4) {
            
            
            if ([textView.text length] ==0) {
                
                [underTitleText setText:[NSString stringWithFormat:@"%@   %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
            }
            if ([textView.text length] ==1) {
                
                
//                [underTitleText setText:[NSString stringWithFormat:@"%@     %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]]; //03-10-2017
                [underTitleText setText:[NSString stringWithFormat:@"%@        %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];

            }
            if ([textView.text length] ==2) {
//                
//                [underTitleText setText:[NSString stringWithFormat:@"%@      %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
                //03-10-2017
                [underTitleText setText:[NSString stringWithFormat:@"%@         %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
            }
            if ([textView.text length] ==3) {
                
//                [underTitleText setText:[NSString stringWithFormat:@"%@        %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]]; //03-10-2017
                [underTitleText setText:[NSString stringWithFormat:@"%@           %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];

            }
            
            
        }
        else
        {
            [textView setText:@""];
            
            [underTitleText setText:[NSString stringWithFormat:@"%@  %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
        }
    }
    
    
    
    
    
    
    
    
    
    
}

-(void) textViewDidBeginEditing:(UITextView *)textView
{
    
    
    
    
}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {
    
    
    
	return YES;
}

-(void)hideAndseek:(NSString*)set
{
    
    
}

-(void)getParant:(UIViewController*)parant

{
    
    parantIt =(FrequencyViewControllerNew*)parant;
    
    
    
    
    
    
}




- (IBAction)SelectChoice:(UIButton *)sender {
    
    
    
    
    
}



@end
