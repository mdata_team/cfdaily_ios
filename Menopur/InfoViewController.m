//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "InfoViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "ThermsViewController.h"
#import "ReviewedViewcontroller.h"
#import "ActionArrow.h"
@interface InfoViewController ()

@end

@implementation InfoViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize theTime;
@synthesize theDay;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize underTitleText;
@synthesize navController;



-(void)loadView{

    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    

    parantLabel =(TekstIDLabel*)set;
    
}



- (void)viewDidLoad {

    [super viewDidLoad];
    
    //********** Google Analytics ****************
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"I_CFDaily Info (main screen) _iOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //********** Google Analytics ****************
    
         [self.view.layer setMasksToBounds:YES];


      TimeChoses =[[NSMutableArray alloc]init];

    
    
    //CFDaily info

    
    
    UIBarButtonItem *Done = [[UIBarButtonItem alloc]
                             initWithTitle:NSLocalizedString(@"Done",nil) style:UIBarButtonSystemItemCancel target:self
                             action:@selector(Cancel:)];


    
    self.navigationItem.leftBarButtonItem = Done;
    

    

    self.title =NSLocalizedString(@"CFDaily Info",nil);
    

    [self.view setBackgroundColor:[UIColor colorWithRed:250.0f/255.0f green:241.0f/255.0f blue:235.0f/255.0f alpha:1.000]];
    
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];

    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        // this will appear as the title in the navigation bar
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:16];
        label.textAlignment = NSTextAlignmentCenter;
        // ^-Use NSTextAlignmentCenter for older SDKs.
        label.textColor = [UIColor whiteColor]; // change this color
        self.navigationItem.titleView = label;
          [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
        label.text = self.title;
        [label sizeToFit];

              [Done setTintColor:[UIColor whiteColor]];
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forBarMetrics:20];
        [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
        [self.navigationController.navigationBar setTranslucent:NO];
     
        
    }
    
    else
        
    {
        
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forBarMetrics:0];
        [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
      
        
    }
    
    [self.navigationController.navigationBar setTranslucent:NO];

    
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"]||[NSLocalizedString(@"Taal",nil) isEqualToString:@"English"]||[NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"]||[NSLocalizedString(@"Taal",nil) isEqualToString:@"German"]||[NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"]||[NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"] || [NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
    {
        //11-07-2017 change in array "Endorsed by" insted of "Reviewed by"
        
//        NSArray *names =[NSArray arrayWithObjects:@"Terms & Privacy Policy",@"Visit Website", @"Endorsed by", @"Write a Review",@"Tell a Friend",@"Send us your Feedback",@"Report a Problem", @"About Developer", nil]; //26-07-2017 Remove "Endorsed by" from array

        
        NSArray *names =[NSArray arrayWithObjects:@"Privacy Policy & Terms of Use",@"Visit Website", @"Write a Review",@"Tell a Friend",@"Send us your Feedback",@"Report a Problem", @"About Developer", nil];
        
        
        for (NSInteger i=0 ; i<[names count]; i++) {
            
            UIView *CombiOut = [[UIView alloc] initWithFrame:CGRectMake(10, 20+(49*i), self.view.frame.size.width-20, 40)];
            [CombiOut setBackgroundColor:[UIColor whiteColor]];
            [CombiOut.layer setCornerRadius:10];
            [CombiOut.layer setBorderWidth:1.5];
//            [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
            [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
            [CombiOut.layer setMasksToBounds:YES];
            [self.view addSubview:CombiOut];
            //05-09-2017 change "self.view.frame.size.width-20" to "self.view.frame.size.width-65"
            TekstIDLabel *MedName= [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-80, 40)];

        // [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        //  MedName.lineBreakMode = NSLineBreakByWordWrapping;
            [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];


            [MedName setFont:[UIFont boldSystemFontOfSize:13]];

            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:2];
            MedName.textAlignment = NSTextAlignmentLeft;

            [MedName setText:[[NSString stringWithFormat:@"%@", NSLocalizedString([names objectAtIndex:i],nil)] stringByReplacingOccurrencesOfString:@"\n" withString:@" "]];

            if ([MedName.text isEqualToString:@"   Geschäftsbedingungen und Datenschutz"]) {
               
                NSMutableString *mu = [NSMutableString stringWithString:MedName.text];
                [mu insertString:@"     " atIndex:36];
                [MedName setText:[NSString stringWithFormat:@"%@",mu]];
            }

            [MedName setTag:120];

            MedName.lineBreakMode = NSLineBreakByWordWrapping;
            [CombiOut addSubview:MedName];

            ActionArrow *End = [[ActionArrow alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3, 60/2, 72/2)];
            [End setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
            [End setTitleit:NSLocalizedString([names objectAtIndex:i],nil)];
            [End setTag:171];
            [End addTarget:self action:@selector(ActionURL:) forControlEvents:UIControlEventTouchUpInside];
            [CombiOut addSubview:End];


            if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                
                
                //NSLog(@"left");
                CombiOut.transform = CGAffineTransformMakeScale(-1, 1);
                

                for (UILabel *label in CombiOut.subviews) {
                    
                    
                    if ([label isKindOfClass:[UILabel class]]) {
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                for (TekstIDLabel *label in CombiOut.subviews) {
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                for (UIButton *label in CombiOut.subviews) {
                    
                    
                    if ([label isKindOfClass:[UIButton class]]) {
                        
                        label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                        [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                
                
                
                //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
                
                
            } else {
                
                //NSLog(@"right");
                //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
            }
            
        }
        
    
        
    }
    
    else
    {
        
          NSArray *names =[NSArray arrayWithObjects:NSLocalizedString(@"Privacy Policy & Terms of Use", nil),@"Visit Website",@"Write a Review",@"Tell a Friend",@"Send us your Feedback",@"Report a Problem",@"About Developer", nil];
   
        
        for (NSInteger i=0 ; i<[names count]; i++) {
            
            UIView *CombiOut = [[UIView alloc] initWithFrame:CGRectMake(10, 20+(49*i), self.view.frame.size.width-20, 40)];
            [CombiOut setBackgroundColor:[UIColor whiteColor]];
            [CombiOut.layer setCornerRadius:10];
            [CombiOut.layer setBorderWidth:1.5];
//            [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
            [CombiOut.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
            [CombiOut.layer setMasksToBounds:YES];
            [self.view addSubview:CombiOut];
            
            TekstIDLabel *MedName;
            if (i==0)
            {
                MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-65, 40)];
            }
            else
            {
                MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-20, 40)];
            }
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:13]];
            [MedName setText:[NSString stringWithFormat:@"   %@", NSLocalizedString([names objectAtIndex:i],nil)]];
            //07-09-2017 for danish
            if ([MedName.text isEqualToString:@"   Vilkår og Politik om beskyttelse af personlige oplysninger"]) {
                
                NSMutableString *mu = [NSMutableString stringWithString:MedName.text];
                [mu insertString:@"    " atIndex:42];
                [MedName setText:[NSString stringWithFormat:@"%@",mu]];
            }
            [MedName setTag:120];
            MedName.lineBreakMode = NSLineBreakByCharWrapping;
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [CombiOut addSubview:MedName];
            
            
            
            ActionArrow *End = [[ActionArrow alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3, 60/2, 72/2)];
            [End setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
            [End setTitleit:NSLocalizedString([names objectAtIndex:i],nil)];
            [End setTag:171];
            [End addTarget:self action:@selector(ActionURL:) forControlEvents:UIControlEventTouchUpInside];
            [CombiOut addSubview:End];
            
            
            if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                
                
                //NSLog(@"left");
                
                
                CombiOut.transform = CGAffineTransformMakeScale(-1, 1);
                
                
                
                for (UILabel *label in CombiOut.subviews) {
                    
                    
                    if ([label isKindOfClass:[UILabel class]]) {
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                for (TekstIDLabel *label in CombiOut.subviews) {
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                for (UIButton *label in CombiOut.subviews) {
                    
                    
                    if ([label isKindOfClass:[UIButton class]]) {
                        
                        label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                        [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                
           
            
            //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
            
            
        } else {
            
            //NSLog(@"right");
            //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
        }

        
        }
    }
}

-(void)ActionURL:(ActionArrow*)sender
{
    
    if ([sender.titleit isEqualToString:NSLocalizedString(@"Privacy Policy & Terms of Use",nil)]) {

        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

        [appDelegate doPolicy:self.navigationController];
        //********** Google Analytics ****************
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"I_Terms and Privacy Policy_iOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        //********** Google Analytics ****************
        

    }


//26-007-2017 comment code of "Endorsed by"
//    else if ([sender.titleit isEqualToString:NSLocalizedString(@"Endorsed by",nil)]) {
//        
//        
//   ReviewedViewcontroller *controller = [[ReviewedViewcontroller alloc]init];
//        [self.navigationController pushViewController:controller animated:YES];
//    }
    else if ([sender.titleit isEqualToString:NSLocalizedString(@"Write a Review",nil)]) {

        //********** Google Analytics ****************
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"I_Write a review_iOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        //********** Google Analytics ****************

        [self addActionForGoogleAnalytics:@"Write a review"];
        
//        // ************************* googleAnalyticsGroupping ************************
//        
//        id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//        [tracker set:[GAIFields contentGroupForIndex:4]value:@"Write a review"];
//        
//        // ************************* googleAnalyticsGroupping ************************
        
        NSArray * language = [NSLocale preferredLanguages] ;

        NSArray *language1 = [[NSBundle mainBundle] preferredLocalizations] ;

        NSLog(@"Current Local : %@",language1);
        
//        NSString *strLanguge = [[NSUserDefaults standardUserDefaults]valueForKey:@"preferred_language"];
//        NSLog(@"preferred_language : %@",strLanguge);

//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/app/cf-medcare-cystic-fibrosis/id658766860?mt=8&ign-mpt=uo%3D2"]];
      //20-11
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/cfdaily-eu/id1314615718?ls=1&mt=8"]];
    
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/nl/app/cf-medcare-cystic-fibrosis/id658766860?mt=8&ign-mpt=uo%3D2"]];
    }
    else if ([sender.titleit isEqualToString:NSLocalizedString(@"Tell a Friend",nil)]) {
     
        //********** Google Analytics ****************
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"I_Tell a friend_iOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        //********** Google Analytics ****************

        
        [self addActionForGoogleAnalytics:@"Tell a friend"];
        
//        // ************************* googleAnalyticsGroupping ************************
//        
//        id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//        [tracker set:[GAIFields contentGroupForIndex:4]value:@"Tell a friend"];
//        
//        // ************************* googleAnalyticsGroupping ************************

        
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
       
    [appDelegate SendtheMailtoAdress:@"" subject:@"CFDaily" with:self.navigationController];
       
      
    }
    else if ([sender.titleit isEqualToString:NSLocalizedString(@"Send us your Feedback",nil)]) {
        
        //********** Google Analytics ****************
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"I_Send up your feedback_iOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        //********** Google Analytics ****************
        
        [self addActionForGoogleAnalytics:@"Send feedback"];
        
//        // ************************* googleAnalyticsGroupping ************************
//        
//        id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//        [tracker set:[GAIFields contentGroupForIndex:4]value:@"Send up your feedback"];
//        
//        // ************************* googleAnalyticsGroupping ************************
        

 
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
       [appDelegate SendtheMailtoAdress:@"support@mdata.gr" subject:[NSString stringWithFormat:@"CFDaily %@",NSLocalizedString(@"Feedback",nil)] with:self.navigationController];
        
    }
    else if ([sender.titleit isEqualToString:NSLocalizedString(@"Report a Problem",nil)]) {
        
        [self addActionForGoogleAnalytics:@"Report a problem"];
        
        //********** Google Analytics ****************
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"I_Report a problem_iOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        //********** Google Analytics ****************

        
//        // ************************* googleAnalyticsGroupping ************************
//        
//        id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//        [tracker set:[GAIFields contentGroupForIndex:4]value:@"Report a problem"];
//        
//        // ************************* googleAnalyticsGroupping ************************
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate SendtheMailtoAdress:@"support@mdata.gr" subject:[NSString stringWithFormat:@"CFDaily %@",NSLocalizedString(@"Report Problem",nil)] with:self.navigationController];
        
        
    }
    else if ([sender.titleit isEqualToString:NSLocalizedString(@"Visit Website",nil)]) {
        
        //********** Google Analytics ****************
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"I_Visit website_iOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
        //********** Google Analytics ****************
        
        
        [self addActionForGoogleAnalytics:@"Visit Website"];
      
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate doWebview:self.navigationController];
     
       
    }
    else if ([sender.titleit isEqualToString:NSLocalizedString(@"View FAQ",nil)]) {
        /*
        UIAlertView *alertit = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"The FAQ section will be available soon!",nil)
                                                         delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
        [alertit show];
        */
        
        UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:@"The FAQ section will be available soon!" preferredStyle:UIAlertControllerStyleAlert];
        [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
            
        });

        
    }
    else if ([sender.titleit isEqualToString:NSLocalizedString(@"About Developer",nil)]) {
        
        //********** Google Analytics ****************
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"I_AAbout Developer_iOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        //********** Google Analytics ****************
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [appDelegate doMData:self.navigationController];
        
    }
}

-(void)Cancel:(UIBarButtonItem*) sender

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:1];

        //[self.view.superview method]

    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];


}

-(void)Save:(UIBarButtonItem*) sender

{
    

    [parantLabel setText:TitleText.text];
    [parantIt setitemsDictionary:TitleText.text name:[self.navigationItem.title stringByReplacingOccurrencesOfString:@"" withString:@""]];


        //[parantIt getchoice:TimeChoses];
    
    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];


}

-(void)change:(UIButton*)sender

{

    if (sender.tag ==140 ||sender.tag ==180) {


    }
    if (sender.tag ==141 ||sender.tag ==181) {

    }
    if (sender.tag ==142 ||sender.tag ==182) {


        TimesofDayViewController *controller = [[TimesofDayViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (sender.tag ==143 ||sender.tag ==183) {

    }
    if (sender.tag ==144 ||sender.tag ==184) {

    }
    
   
  
  

    
}

-(void)Action:(UIButton*)sender

{

  
}

-(void)changeChose:(NSString*)sender

{


    chosen = sender;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    [TitleText setKeyboardType:UIKeyboardTypeDecimalPad];
   
    TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
    [TitleText becomeFirstResponder];
}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void)textViewDidChange:(UITextView *)textView
{
    
   


    if ([textView.text length]<4) {


        if ([textView.text length] ==0) {

            
                [underTitleText setText:[NSString stringWithFormat:@"   %@",NSLocalizedString(@"Quantity Per Filling",nil)]];
           
        }
        if ([textView.text length] ==1) {
            
            [underTitleText setText:[NSString stringWithFormat:@"    %@",NSLocalizedString(@"Quantity Per Filling",nil)]];
         
        }
        if ([textView.text length] ==2) {
            
            [underTitleText setText:[NSString stringWithFormat:@"      %@",NSLocalizedString(@"Quantity Per Filling",nil)]];

        }
        if ([textView.text length] ==3) {
            
            [underTitleText setText:[NSString stringWithFormat:@"         %@",NSLocalizedString(@"Quantity Per Filling",nil)]];
        
        }


    }
    else
        {
        [textView setText:@""];
            
            [underTitleText setText:[NSString stringWithFormat:@"   %@",NSLocalizedString(@"Quantity Per Filling",nil)]];

        }
    

    




    

}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{
}

-(void)getParant:(UIViewController*)parant

{

    parantIt =(NewMedecineController*)parant;




  
    
}




- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}

#pragma mark - Google Analytics - Action

-(void)addActionForGoogleAnalytics:(NSString *)actionName
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"button_press"
                                                           label:actionName
                                                           value:nil] build]];
}



@end
