//
//  Monthview.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrengthViewControllerScroll.h"
@interface Inhoudview : UIView <UITableViewDataSource, UITableViewDelegate>


-(void)setParant:(StrengthViewControllerScroll*)set;
@property (nonatomic, retain) IBOutlet UITableView *table;
@property (nonatomic, retain) StrengthViewControllerScroll *Parantit;
@property (nonatomic, retain) NSMutableArray *MonthList;


@end
