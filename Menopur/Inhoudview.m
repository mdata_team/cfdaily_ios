//
//  Monthview.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "Inhoudview.h"
#import "GetData.h"
#import "StrengthViewControllerScroll.h"
#import "AppDelegate.h"

@implementation Inhoudview
@synthesize table;
@synthesize Parantit;
@synthesize MonthList;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

        MonthList =[[NSMutableArray alloc] init];

        table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 22, 320, 22)];
        table.separatorColor = [UIColor grayColor];
        table.backgroundColor = [UIColor whiteColor];
        table.rowHeight =45;
        table.delegate = self;
        table.dataSource = self;
        [table setEditing:NO];
        [self addSubview:table];
        
       

    }
    return self;
}


-(void)setParant:(StrengthViewControllerScroll*)set

{
  
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

  
    NSArray *numbers2 = [[[[GetData getDrugTypeStrength:appDelegate.TypeMed] valueForKey:@"Strength_unit"] objectAtIndex:0] componentsSeparatedByString:@","];

 

   for (NSInteger i =0; i < [numbers2 count]; i++) {



        NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
        [set setObject:[numbers2 objectAtIndex:i] forKey:@"Content"];
        [set setObject:[NSString stringWithFormat:@"%li", (long)i] forKey:@"Number"];
        

        [MonthList addObject:set];
        
    }
    

    Parantit =set;

   
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [MonthList count];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{



    return 45;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{

    [Parantit chosenMonth:[[MonthList valueForKey:@"Content"] objectAtIndex:indexPath.row]];


}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {




    NSString *identifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

	if (cell == nil) {

        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellSelectionStyleGray reuseIdentifier:identifier];

        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];

        

        
    }
    
    else
        
        {
        
        }

    cell.textLabel.text = [[MonthList valueForKey:@"Content"] objectAtIndex:indexPath.row];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];

  
    return cell;
    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
