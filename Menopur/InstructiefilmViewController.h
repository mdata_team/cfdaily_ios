//
//  InstructiefilmViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>


@class VideoPlayer, FilmViewController, CustomMoviePlayerViewController;
@interface InstructiefilmViewController : UIViewController <MPMediaPlayback>
@property (nonatomic, retain) CustomMoviePlayerViewController *moviePlayer;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbarDown;
@property (nonatomic, retain) IBOutlet UILabel *setbackground;
@property (nonatomic, retain) VideoPlayer *movieViewController;
@property (nonatomic, assign) float playertime;
@property (nonatomic, assign) CFTimeInterval playertimeNow;
@property (nonatomic, retain) NSTimer *TimeTimer;
@property (nonatomic, assign, getter=isPlayingMovie) BOOL isPlayingMovie;
@property (nonatomic, retain) IBOutlet UIButton *Instructie_button;
@property (nonatomic, retain) IBOutlet UILabel *MerkenLabel;
@property (nonatomic, retain) IBOutlet UILabel *TextLabel;
@property (nonatomic, retain) IBOutlet UIButton *button;
@property (nonatomic, retain) IBOutlet UIButton *Film_logo;
@property (nonatomic, retain) IBOutlet UIButton *Logo_pdf;
@property (nonatomic, retain) IBOutlet UIButton *info;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *flexItem;
-(void) turnbuttons:(NSString*) setter;

@end
