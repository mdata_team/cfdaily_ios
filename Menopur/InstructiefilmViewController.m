//
//  InstructiefilmViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "InstructiefilmViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "FilmViewController.h"
#import "VideoPlayer.h"
#import "AppDelegate.h"
#import "VragenViewController.h"
#import "BijsluiterViewController.h"

@interface InstructiefilmViewController ()

@end

@implementation InstructiefilmViewController
@synthesize toolbarDown;
@synthesize setbackground;
@synthesize movieViewController;
@synthesize playertime;
@synthesize playertimeNow;
@synthesize TimeTimer;
@synthesize isPlayingMovie;
@synthesize Instructie_button;
@synthesize MerkenLabel;
@synthesize TextLabel;
@synthesize button;
@synthesize Film_logo;
@synthesize Logo_pdf;
@synthesize info;
@synthesize flexItem;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];



    NSLog(@"InstructiefilmViewController");


    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 87/1.9, 59/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



    UIImage *image = [UIImage imageNamed:@"Logo_bar.png"];
	UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0, 0,136, 34);

    self.navigationItem.titleView = imageView;

    
    setbackground =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [setbackground setBackgroundColor:[UIColor colorWithRed:0.627 green:0.851 blue:0.961 alpha:1.000]];

    [self.view addSubview:setbackground];

    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = setbackground.layer.bounds;
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                         nil];
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.1f],
                            [NSNumber numberWithFloat:0.2f],
                            [NSNumber numberWithFloat:0.3f],
                            [NSNumber numberWithFloat:0.4f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.6f],
                            nil];



    [setbackground.layer addSublayer:shineLayer];

    toolbarDown = [[UIToolbar alloc] init];
    toolbarDown.frame = CGRectMake(0, self.view.frame.size.height-88, self.view.frame.size.width, 44);
    toolbarDown.tintColor = [UIColor colorWithRed:0.204 green:0.714 blue:0.816 alpha:1.000];




   flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];


    info = [UIButton buttonWithType:UIButtonTypeCustom];
    [info setTag:44];
    [info setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [info addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    info.layer.cornerRadius = 2;
    [info setTag:22];
    [info setImage:[UIImage imageNamed:@"Info_Logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *info_button = [[UIBarButtonItem alloc] initWithCustomView:info];


    Logo_pdf = [UIButton buttonWithType:UIButtonTypeCustom];
    [Logo_pdf setTag:44];
    [Logo_pdf setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [Logo_pdf addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
      Logo_pdf.layer.cornerRadius = 2;
    [Logo_pdf setTag:23];
    [Logo_pdf setImage:[UIImage imageNamed:@"Logo_pdf.png"] forState:UIControlStateNormal];
    UIBarButtonItem *logo_button = [[UIBarButtonItem alloc] initWithCustomView:Logo_pdf];



        //Film_Logo.png


    Film_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    [Film_logo setTag:44];
    [Film_logo setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [Film_logo addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    Film_logo.layer.cornerRadius = 2;
    [Film_logo setTag:24];
    [Film_logo setImage:[UIImage imageNamed:@"Film_Logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *film_button = [[UIBarButtonItem alloc] initWithCustomView:Film_logo];


    NSArray *items2 = [NSArray arrayWithObjects:
                       film_button, flexItem,logo_button,flexItem,info_button,
                       nil];
    toolbarDown.items = items2;


    [self.view addSubview:toolbarDown];


    NSArray *array1 =[NSArray arrayWithObjects:@"Dosering-1.png", @"Dosering-2.png", @"Dosering-3.png", @"Dosering-4.png", @"Dosering-5.png", @"Dosering-6.png", nil];




    NSArray *array2 =[NSArray arrayWithObjects:@"375-Mobiel.m4v", @"1500-Mobiel.m4v", @"750-Mobiel.m4v",   @"1870-Mobiel.m4v",@"1125-Mobiel.m4v", @"2250-Mobiel.m4v", nil];




    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod"] || [deviceType isEqualToString:@"iPod touch"] || [deviceType isEqualToString:@"iPhone Simulator"]) {

    for (int i=0 ; i<[array1 count]; i++) {

        if (i==0||i==1) {


            UIButton *button_Dosering = [[UIButton alloc] initWithFrame:CGRectMake(15+((305/2)*i), 20+(110/2),275/2, 91/2)];
            [button_Dosering addTarget:self action:@selector(instructie:) forControlEvents:UIControlEventTouchUpInside];
            [button_Dosering setTitle:[array2 objectAtIndex:i] forState:UIControlStateNormal];
            [button_Dosering setImage:[UIImage imageNamed:[array1 objectAtIndex:i]] forState:UIControlStateNormal];
            [self.view addSubview:button_Dosering];

               [button_Dosering setTag:1+i];
        }

        if (i==2||i==3) {


            UIButton *button_Dosering = [[UIButton alloc] initWithFrame:CGRectMake(15+((305/2)*(i-2)), 20+((110/2)*2),275/2, 91/2)];
            [button_Dosering addTarget:self action:@selector(instructie:) forControlEvents:UIControlEventTouchUpInside];
            [button_Dosering setTitle:[array2 objectAtIndex:i] forState:UIControlStateNormal];
            [button_Dosering setImage:[UIImage imageNamed:[array1 objectAtIndex:i]] forState:UIControlStateNormal];
            [self.view addSubview:button_Dosering];

               [button_Dosering setTag:1+i];
        }
        if (i==4||i==5) {


            UIButton *button_Dosering = [[UIButton alloc] initWithFrame:CGRectMake(15+((305/2)*(i-4)), 20+((110/2)*3),275/2, 91/2)];
            [button_Dosering addTarget:self action:@selector(instructie:) forControlEvents:UIControlEventTouchUpInside];
            [button_Dosering setTitle:[array2 objectAtIndex:i] forState:UIControlStateNormal];
            [button_Dosering setImage:[UIImage imageNamed:[array1 objectAtIndex:i]] forState:UIControlStateNormal];
            [self.view addSubview:button_Dosering];

               [button_Dosering setTag:1+i];
        }
        

        
    }

        


    button = [[UIButton alloc] initWithFrame:CGRectMake(15, 20+(115/2)*4, 579/2, 91/2)];
    [button addTarget:self action:@selector(instructie:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"andere dosering" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"Dosering-7.png"] forState:UIControlStateNormal];
    [self.view addSubview:button];

       [button setTag:7];



    TextLabel= [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2, 6, 300, 60)];
    TextLabel.backgroundColor = [UIColor clearColor];
    [TextLabel setText: @"Kies uw dosering voor de videoinstructie"];
    TextLabel.font = [UIFont boldSystemFontOfSize:25];
    [TextLabel setNumberOfLines:3];
    [TextLabel setTextAlignment:NSTextAlignmentCenter];
    TextLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
    [TextLabel setBackgroundColor:[UIColor clearColor]];
    [self.view  addSubview:TextLabel];

    [TextLabel setAlpha:1];


    MerkenLabel= [[UILabel alloc] initWithFrame:CGRectMake(20, 266, 270, 100)];
    MerkenLabel.backgroundColor = [UIColor clearColor];
    [MerkenLabel setText: @"IE = Internationale Eenheden"];
    MerkenLabel.font = [UIFont systemFontOfSize:18];
    [MerkenLabel setNumberOfLines:1];
    [MerkenLabel setTextAlignment:NSTextAlignmentLeft];
    MerkenLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
    [MerkenLabel setBackgroundColor:[UIColor clearColor]];
    [self.view   addSubview:MerkenLabel];

    [MerkenLabel setAlpha:1];




    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:UIDeviceOrientationDidChangeNotification object:nil];

    

    Instructie_button = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-(578/2))/2, 500,578/2, 271/2)];

    [Instructie_button addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [Instructie_button setImage:[UIImage imageNamed:@"Dosering.png"] forState:UIControlStateNormal];
    [self.view addSubview:Instructie_button];
    [Instructie_button setAlpha:0];

    }

    else
        {

        for (int i=0 ; i<[array1 count]; i++) {

            if (i==0||i==1) {


                UIButton *button_Dosering = [[UIButton alloc] initWithFrame:CGRectMake(235+((305/2)*i), 20+(110/2),275/2, 91/2)];
                [button_Dosering addTarget:self action:@selector(instructie:) forControlEvents:UIControlEventTouchUpInside];
                [button_Dosering setTitle:[array2 objectAtIndex:i] forState:UIControlStateNormal];
                [button_Dosering setImage:[UIImage imageNamed:[array1 objectAtIndex:i]] forState:UIControlStateNormal];
                [self.view addSubview:button_Dosering];

                [button_Dosering setTag:1+i];
            }

            if (i==2||i==3) {


                UIButton *button_Dosering = [[UIButton alloc] initWithFrame:CGRectMake(235+((305/2)*(i-2)), 20+((110/2)*2),275/2, 91/2)];
                [button_Dosering addTarget:self action:@selector(instructie:) forControlEvents:UIControlEventTouchUpInside];
                [button_Dosering setTitle:[array2 objectAtIndex:i] forState:UIControlStateNormal];
                [button_Dosering setImage:[UIImage imageNamed:[array1 objectAtIndex:i]] forState:UIControlStateNormal];
                [self.view addSubview:button_Dosering];

                [button_Dosering setTag:1+i];
            }
            if (i==4||i==5) {


                UIButton *button_Dosering = [[UIButton alloc] initWithFrame:CGRectMake(235+((305/2)*(i-4)), 20+((110/2)*3),275/2, 91/2)];
                [button_Dosering addTarget:self action:@selector(instructie:) forControlEvents:UIControlEventTouchUpInside];
                [button_Dosering setTitle:[array2 objectAtIndex:i] forState:UIControlStateNormal];
                [button_Dosering setImage:[UIImage imageNamed:[array1 objectAtIndex:i]] forState:UIControlStateNormal];
                [self.view addSubview:button_Dosering];

                [button_Dosering setTag:1+i];
            }



        }




        button = [[UIButton alloc] initWithFrame:CGRectMake(235, 20+(115/2)*4, 579/2, 91/2)];
        [button addTarget:self action:@selector(instructie:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"andere dosering" forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"Dosering-7.png"] forState:UIControlStateNormal];
        [self.view addSubview:button];

        [button setTag:7];



        TextLabel= [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2, 6, 300, 60)];
        TextLabel.backgroundColor = [UIColor clearColor];
        [TextLabel setText: @"Kies uw dosering voor de videoinstructie"];
        TextLabel.font = [UIFont boldSystemFontOfSize:25];
        [TextLabel setNumberOfLines:3];
        [TextLabel setTextAlignment:NSTextAlignmentCenter];
        TextLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
        [TextLabel setBackgroundColor:[UIColor clearColor]];
        [self.view  addSubview:TextLabel];

        [TextLabel setAlpha:1];


        MerkenLabel= [[UILabel alloc] initWithFrame:CGRectMake(235, 266, 270, 100)];
        MerkenLabel.backgroundColor = [UIColor clearColor];
        [MerkenLabel setText: @"IE = Internationale Eenheden"];
        MerkenLabel.font = [UIFont systemFontOfSize:18];
        [MerkenLabel setNumberOfLines:1];
        [MerkenLabel setTextAlignment:NSTextAlignmentLeft];
        MerkenLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
        [MerkenLabel setBackgroundColor:[UIColor clearColor]];
        [self.view   addSubview:MerkenLabel];

        [MerkenLabel setAlpha:1];




        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didRotate:)
                                                     name:UIDeviceOrientationDidChangeNotification object:nil];
        
        
        
        Instructie_button = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-(578/2))/2, 500,578/2, 271/2)];
        [Instructie_button addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [Instructie_button setImage:[UIImage imageNamed:@"Dosering.png"] forState:UIControlStateNormal];
        [self.view addSubview:Instructie_button];
        [Instructie_button setAlpha:0];
        
        }

	// Do any additional setup after loading the view.
}

- (void) didRotate:(NSNotification *)notification{

    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    [appDelegate.progressBar setAlpha:0];

    
    UIDeviceOrientation myOrientation = [[UIDevice currentDevice] orientation];
    if (myOrientation == UIInterfaceOrientationLandscapeLeft) {



         [movieViewController.view setTransform:CGAffineTransformMakeRotation(-M_PI/2)];
        [movieViewController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+20)];
         [movieViewController.view setCenter:CGPointMake(self.view.frame.size.width/2, ((self.view.frame.size.height+20)/2))];
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
        });
      


        
    }
    else if (myOrientation == UIInterfaceOrientationLandscapeRight) {

        [movieViewController.view setTransform:CGAffineTransformMakeRotation(M_PI/2)];
          [movieViewController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+20)];
         [movieViewController.view setCenter:CGPointMake(self.view.frame.size.width/2, ((self.view.frame.size.height+20)/2))];


        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
        });



    }
    else if (myOrientation == UIInterfaceOrientationPortrait) {


           [movieViewController.view setTransform:CGAffineTransformMakeRotation(0)];
         [movieViewController.view setFrame:CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height)];

        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
        });


    }
    else if (myOrientation == UIInterfaceOrientationPortraitUpsideDown) {

         [movieViewController.view setTransform:CGAffineTransformMakeRotation(0)];
         [movieViewController.view setFrame:CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height)];

        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
        });

    }
    
    
    
}


-(void)PresentView:(UIButton*)sender

{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    for (int i =1; i < [[[self navigationController] viewControllers] count]; i++) {

        UIViewController *controller = [[[self navigationController] viewControllers] objectAtIndex:i];
        [[self navigationController] popToViewController:controller animated:NO];
        
        
    }

    
   

int64_t delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

    if (sender.tag ==22) {



        VragenViewController *controller = [[VragenViewController alloc]init];
        NSArray *viewControllers = [[self navigationController] viewControllers];

        [appDelegate.Navigaioncopy removeAllObjects];
        [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
        [appDelegate.Navigaioncopy addObject:controller];

        [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];



    }
    if (sender.tag ==23) {


        BijsluiterViewController *controller = [[BijsluiterViewController alloc]init];
        NSArray *viewControllers = [[self navigationController] viewControllers];

        [appDelegate.Navigaioncopy removeAllObjects];
        [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
        [appDelegate.Navigaioncopy addObject:controller];

        [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];

    }
    if (sender.tag ==24) {



        InstructiefilmViewController *controller = [[InstructiefilmViewController alloc]init];
        NSArray *viewControllers = [[self navigationController] viewControllers];

        [appDelegate.Navigaioncopy removeAllObjects];
        [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
        [appDelegate.Navigaioncopy addObject:controller];
        
        [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];
        
        
    }

    
});


}

- (IBAction)instructie:(UIButton *)sender {



    NSArray *test;

    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod"] || [deviceType isEqualToString:@"iPod touch"] || [deviceType isEqualToString:@"iPhone Simulator"]) {


    test= [[NSArray alloc] initWithObjects:@"http://vimeo.com/57515252/download?t=1359019825&v=139358773&s=660163a035bd95d3bf392213b1256cc1", @"http://vimeo.com/57518127/download?t=1359019862&v=139366586&s=ff1fc6700b441faaf0ee64b4d7581234",  @"http://vimeo.com/57516019/download?t=1359019795&v=139360786&s=27bbebb4c10921da90b9fe7480ce67ed", @"http://vimeo.com/57518986/download?t=1359145195&v=139368605&s=9d8c84487bbb3629fe24ab499576ea34", @"http://vimeo.com/57517349/download?t=1359021081&v=139364347&s=7cf28e17b92f22e96405251d6e715015", @"http://vimeo.com/57519776/download?t=1359020022&v=139370857&s=9d45ae853cb318ea7f26156212a268de", nil];

    }

    else
        {
         test =[NSArray arrayWithObjects:@"http://vimeo.com/57515252/download?t=1359021762&v=139358775&s=8ad965c7797526284d3a5e38a19b4bc3", @"http://vimeo.com/57518127/download?t=1359021744&v=139366585&s=a596b494c67a66b2707b075c024be2d9", @"http://vimeo.com/57516019/download?t=1359021700&v=139360783&s=4d46b76da7e865fc072d51ab10f4e7ff", @"http://vimeo.com/57518986/download?t=1359021778&v=139368603&s=52d4458c3beefb868af2134c37287014",@"http://vimeo.com/57517349/download?t=1359021720&v=139364348&s=883c307c6bee97f077d06701417b41a9", @"http://vimeo.com/57519776/download?t=1359021802&v=139370854&s=2d5c9521715fc81ecf57084cd3d5b89f", nil];
        
        }


        // 


    NSArray *array1 =[NSArray arrayWithObjects:@"375-Mobiel.mp4", @"1500-Mobiel.mp4", @"750-Mobiel.mp4", @"1870-Mobiel.mp4",@"1125-Mobiel.mp4", @"2250-Mobiel.mp4", nil];


   


    if ( sender.tag ==7) {
        
    }
    else
        {

        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate save:[test objectAtIndex:sender.tag-1] setName:[array1 objectAtIndex:sender.tag-1]];
        

        }


    NSArray *array2 =[NSArray arrayWithObjects:@"375-Mobiel.m4v", @"1500-Mobiel.m4v", @"750-Mobiel.m4v",   @"1870-Mobiel.m4v",@"1125-Mobiel.m4v", @"2250-Mobiel.m4v", @"andere dosering", nil];



    if ([sender.titleLabel.text isEqualToString:@"andere dosering"]) {



        for (int i=0 ; i<[array2 count]; i++) {



            UIButton *gone = (UIButton *)[self.view viewWithTag:(i+1)];
            [gone setAlpha:0];

        }

        [Instructie_button setAlpha:1];
        [MerkenLabel setAlpha:0];
        [Instructie_button setFrame:CGRectMake((self.view.frame.size.width-(578/2))/2, 90,578/2, 271/2)];
        
        
    }
    else
        {


        BOOL success;

        success =NO;

        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

        NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir, [array1 objectAtIndex:[array2 indexOfObject:sender.titleLabel.text]]];

        NSFileManager *FileManager = [NSFileManager defaultManager];

        success = [FileManager fileExistsAtPath:jpegFilePath];

       
        if (success) {

            


            NSLog(@"sender %@", sender.titleLabel.text);

                //NSString *urlStr = [[NSBundle mainBundle] pathForResource:sender.titleLabel.text  ofType:@""];
            NSURL *url = [NSURL fileURLWithPath:jpegFilePath];

            movieViewController = [[VideoPlayer alloc] initWithContentURL:url];
            movieViewController.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;


                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification object: movieViewController.moviePlayer];




            [self.navigationController setNavigationBarHidden:YES animated:YES];
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
            [self presentMoviePlayerViewControllerAnimated:movieViewController];

            double delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [movieViewController.view setFrame:CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height)];
                [self.navigationController setNavigationBarHidden:YES animated:YES];
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
            });
           


        }
        else
            {


            NSLog(@"%@", sender.titleLabel.text);

            NSString *urlStr = [[NSBundle mainBundle] pathForResource:sender.titleLabel.text  ofType:@""];
            NSURL *url = [NSURL fileURLWithPath:urlStr];

            movieViewController = [[VideoPlayer alloc] initWithContentURL:url];
            movieViewController.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;

           

            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification object: movieViewController.moviePlayer];


            
            [self presentMoviePlayerViewControllerAnimated:movieViewController];

            double delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [movieViewController.view setFrame:CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height)];
                [self.navigationController setNavigationBarHidden:YES animated:YES];
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
            });


            }

  
        



        }



    

}




-(void) TimerVideoTime
{


        //playertime =videoPlayer.currentPlaybackTime;
		NSLog(@"voor %f", playertime);

	
	
	
}



-(void)movieFinishedCallback:(VideoPlayer*)aNotification{



    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    [appDelegate.progressBar setAlpha:0];

    NSLog(@"stop %@", aNotification.observationInfo);
        //[videoPlayer stop];



}

-(void)viewWillAppear:(BOOL)animated

{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    [appDelegate.progressBar setAlpha:0];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    [appDelegate.progressBar setAlpha:0];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
}


-(void)dismissMoviePlayerViewControllerAnimated
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    [appDelegate.progressBar setAlpha:0];
}

-(void) backAction
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


       [appDelegate.progressBar setAlpha:0];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{
 NSLog(@"turnbuttons");
}

@end
