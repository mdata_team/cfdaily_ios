//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CodePopView, NewMedecineController, TekstIDLabel;
@interface LocationViewController : UIViewController<UITextViewDelegate> {


    UITextView *TitleText;
   NSString *Compaire;
    UIView *Combi;
     UIView *CombiOther;
    NSString *chosen;
}
@property (nonatomic, strong) NSMutableArray *totaal;
@property (nonatomic, strong) IBOutlet UITextView *TitleText;
@property (nonatomic, strong) NSString *Compaire;
@property (nonatomic, strong) IBOutlet UIView *Combi;
@property (nonatomic, strong) IBOutlet UIView *CombiOther;
@property (nonatomic, strong) NSString *chosen;
@property (nonatomic, strong) IBOutlet UIView *Location;
@property (nonatomic, strong) IBOutlet UIButton *Start;
@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;

-(void)whatValue:(NSString*)set whatlabels:(UILabel*)label;
-(void)whatValue:(NSString*)set;

-(void)setChoice:(NSString*)gothere;
-(void)change:(UIButton*)sender;
-(void) setTextview: (NSString *) text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(TekstIDLabel*)set;
-(void)hideAndseek:(NSString*)set;
@end

