//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "LocationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "ActionArrow.h"

@interface LocationViewController ()

@end

@implementation LocationViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize parantLabel;
@synthesize Compaire;
@synthesize Combi;
@synthesize CombiOther;
@synthesize chosen;
@synthesize Location;
@synthesize Start;
@synthesize totaal;


-(void)loadView{


    [super loadView];
}

-(void)whatValue:(NSString*)set
{
    
    
    
    
    totaal =[[NSMutableArray alloc]initWithArray:[set componentsSeparatedByString:@","]];
    
    
   //NSLog(@"set %@", set);
    
    
    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 40)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];
    
    [Combi setFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 30*[totaal count])];
    
    for (NSInteger i=0 ; i<[totaal count]; i++) {
        
        
        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, (30*i), self.view.frame.size.width-20, 30)];
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:12]];
        [MedName setText:NSLocalizedString([totaal objectAtIndex:i],nil)];
        [MedName setTag:180+i];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [MedName setNumberOfLines:2];
        [Combi addSubview:MedName];
        
        
        
        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, (30*i), self.view.frame.size.width-20, 30)];
        [NameButton setTitleit:[totaal objectAtIndex:i]];
         [NameButton setTag:140+i];
        [NameButton.layer setBorderWidth:1];
        [NameButton.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [NameButton addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
        [NameButton setBackgroundColor:[UIColor clearColor]];
        [Combi addSubview:NameButton];
        
       
        if ([parantLabel.text isEqualToString:NSLocalizedString([totaal objectAtIndex:i],nil)]) {
            
            UIImageView *background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Vink.png"]];
            [background setFrame:CGRectMake(self.view.frame.size.width-50,  4+(30*i), 20, 20)];
            [background setBackgroundColor:[UIColor clearColor]];
            [Combi addSubview:background];
            [background setTag:160+i];
            [background setAlpha:1];
            
        }
        else
            
        {
            
            UIImageView *background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Vink.png"]];
            [background setFrame:CGRectMake(self.view.frame.size.width-50,  4+(30*i), 20, 20)];
            [background setBackgroundColor:[UIColor clearColor]];
            [Combi addSubview:background];
            [background setTag:160+i];
            [background setAlpha:0];
            
        }
        
        
    }
    
    
    
    
    
}


-(void)whatValue:(NSString*)set whatLabel:(UILabel*)label

{
    
     parantLabel =(TekstIDLabel*)label;
    
    
     totaal =[[NSMutableArray alloc]initWithArray:[set componentsSeparatedByString:@","]];
   
    
   //NSLog(@"set %@", set);
    
    
    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 40)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];

     [Combi setFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 30*[totaal count])];
    
   for (NSInteger i=0 ; i<[totaal count]; i++) {
        
       
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, (30*i), self.view.frame.size.width-20, 30)];
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:12]];
        [MedName setText:NSLocalizedString([totaal objectAtIndex:i],nil)];
        [MedName setTag:180+i];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
       [MedName setNumberOfLines:2];
        [Combi addSubview:MedName];
        
        
        
        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, (30*i), self.view.frame.size.width-20, 30)];
        [NameButton setTitleit:[totaal objectAtIndex:i]];
         [NameButton setTag:140+i];
        [NameButton.layer setBorderWidth:1];
        [NameButton.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [NameButton addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
        [NameButton setBackgroundColor:[UIColor clearColor]];
        [Combi addSubview:NameButton];
        
        
        if ([parantLabel.text isEqualToString:NSLocalizedString([totaal objectAtIndex:i],nil)]) {
           
            UIImageView *background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Vink.png"]];
            [background setFrame:CGRectMake(self.view.frame.size.width-50,  4+(30*i), 20, 20)];
            [background setBackgroundColor:[UIColor clearColor]];
            [Combi addSubview:background];
            [background setTag:160+i];
            [background setAlpha:1];
            
        }
        else
            
        {
        
        UIImageView *background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Vink.png"]];
        [background setFrame:CGRectMake(self.view.frame.size.width-50,  4+(30*i), 20, 20)];
        [background setBackgroundColor:[UIColor clearColor]];
        [Combi addSubview:background];
        [background setTag:160+i];
        [background setAlpha:0];
            
        }
        
        
    }
    
    
    
    
    
}

-(void)whatLabel:(UILabel*)set

{
   parantLabel =(TekstIDLabel*)set;
    
  
   
   for (NSInteger i=0 ; i<[totaal count]; i++) {
        
        if ([[totaal objectAtIndex:i] isEqualToString:set.text]) {
            
            UIImageView *setimage = (UIImageView *)[self.view viewWithTag:i+160];
            [setimage setAlpha:1];
            
            chosen =set.text;
        }
        else
        {
            UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
            [setimage setAlpha:0];
        }
       
        
        
    }
    
   
    
    
    
    

    
 
}



- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];


 
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];

    
    self.title = NSLocalizedString(@"Location",nil);


//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:20.0];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
              [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.title;
            [label setNumberOfLines:2];
            [label sizeToFit];

           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
   

    
 
 


  

}

-(void)change:(ActionArrow*)sender

{


    
    //////////////////////////////////////////////////////////////////////////////NSLog(@"%@", sender.titleLabel.text);

      for (NSInteger i=0 ; i<[totaal count]; i++) {

           UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
           [setimage setAlpha:0];
           

       }


    UIImageView *setimage = (UIImageView *)[self.view viewWithTag:sender.tag+20];
    [setimage setAlpha:1];

    [parantLabel setText:NSLocalizedString(sender.titleit,nil)];
    
  
    
}

-(void)Action:(UIButton*)sender

{

    TypeOtherViewController *controller = [[TypeOtherViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
    [controller getParant:self];
    
    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
 UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    // ^-Use NSTextAlignmentCenter for older SDKs.
    label.textColor = [UIColor whiteColor]; // change this color
    controller.navigationItem.titleView = label;
    label.text = controller.navigationItem.title;
         [label setNumberOfLines:2];
    [label sizeToFit];
        
    }
    
    TekstIDLabel *setimage = (TekstIDLabel *)[self.view viewWithTag:2222];
    
    [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", setimage.text]];
    
    
    [controller whatLabel:setimage];
}

-(void)changeChose:(NSString*)sender

{


     chosen = sender;
    
    
}

-(void) viewDidAppear:(BOOL)animated
{


}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}



-(void)hideAndseek:(NSString*)set
{

}

-(void)getParant:(UIViewController*)parant

{

    parantIt =(NewMedecineController*)parant;




  
    
}


- (IBAction)doSomething:(UIButton *)sender {


    [[self navigationController] popViewControllerAnimated:YES];

}

- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
