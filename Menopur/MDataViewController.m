//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "MDataViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "ThermsViewController.h"
@interface MDataViewController ()

@end

@implementation MDataViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize theTime;
@synthesize theDay;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize underTitleText;



-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set
{
  
    parantLabel =(TekstIDLabel*)set;
   
}


- (void)viewDidLoad {    
    
    if ([SHARED_APPDELEGATE isNetworkReachable])
    {
        [SHARED_APPDELEGATE sendHitsInBackground];
    }
    
    [super viewDidLoad];
    
    
    [self.view.layer setMasksToBounds:YES];
    
    
    TimeChoses =[[NSMutableArray alloc]init];


    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];

    
    //A propos du développeur
    
    [self setTitle:NSLocalizedString(@"About Developer",nil)];
    
    

    // this will appear as the title in the navigation bar


//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:14];
            label.textAlignment = NSTextAlignmentCenter;
                     label.adjustsFontSizeToFitWidth = YES;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
              [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.title;
            [label setNumberOfLines:2];
            [label sizeToFit];
    
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



   UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0.0f, 100, 20)];
    [title setTextColor:[UIColor whiteColor]];
    [title setText:NSLocalizedString(@"MData",nil)];
    [title setFont:[UIFont boldSystemFontOfSize:18]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];


  /*

    UITextView *DiscaimerText = [[UITextView alloc] initWithFrame:CGRectMake(0, 6, self.view.frame.size.width, self.view.frame.size.height-46)];
    [DiscaimerText setBackgroundColor:[UIColor clearColor]];
//    [DiscaimerText setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]]; //29-06-2017 changes
    [DiscaimerText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [DiscaimerText setTag:123+1];
    [DiscaimerText setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [DiscaimerText setText:NSLocalizedString(@"M-Data S.A.\n\nM-Data, founded in 1977 is a Technology Company based in Athens Greece.\n\nM-Data offers professional services in the design and development of mobile applications, digital content, and webcasting services.\n\nFor any need you may have, please contact us:\n\nMailing Address:\n\n20 Andrea Papandreou\nGlyfada, 166 75\nAthens, Greece\n\nsupport@mdata.gr\n+30 210 9962680\nwww.mdata.gr",nil)];
    [DiscaimerText setUserInteractionEnabled:YES];
    [DiscaimerText setEditable:NO];
    DiscaimerText.dataDetectorTypes = UIDataDetectorTypeAll;
    [DiscaimerText setScrollEnabled:YES];
    [self.view addSubview:DiscaimerText];
*/
    //07-07-2017 remove TextView code and add Webview
    
    UIWebView *webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 6, self.view.frame.size.width, self.view.frame.size.height-76)];
    webview.delegate = self;
    [webview setBackgroundColor:[UIColor clearColor]];
    
    
     if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
     {
         NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutDeveloper_english" ofType:@"html"];
         NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
         [webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    else if([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
    {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutDeveloper_spanish" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    else if([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
    {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutDeveloper_Danish" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    else if([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
    {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutDeveloper_Norwegian" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    else if([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
    {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutDeveloper_dutch" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    else if([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
    {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutDeveloper_Franch" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];

    }
    else if([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
    {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutDeveloper_German" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];

    }
    else if([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
    {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutDeveloper_Italian" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    else
    {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutDeveloper_english" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    [self.view addSubview:webview];

    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];


      
}

-(void)Cancel:(UIBarButtonItem*) sender

{


        //[self.view.superview method]

    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];


}

-(void)Save:(UIBarButtonItem*) sender

{
    

    [parantLabel setText:TitleText.text];
    [parantIt setitemsDictionary:TitleText.text name:[self.navigationItem.title stringByReplacingOccurrencesOfString:@"" withString:@""]];


        //[parantIt getchoice:TimeChoses];
    
    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];


}

-(void)change:(UIButton*)sender

{

    if (sender.tag ==140 ||sender.tag ==180) {


    }
    if (sender.tag ==141 ||sender.tag ==181) {

    }
    if (sender.tag ==142 ||sender.tag ==182) {


        TimesofDayViewController *controller = [[TimesofDayViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (sender.tag ==143 ||sender.tag ==183) {

    }
    if (sender.tag ==144 ||sender.tag ==184) {

    }
    
  
  

    
}

-(void) backAction
{
    
    
    [[self navigationController] popViewControllerAnimated:YES];
}


-(void)Action:(UIButton*)sender

{

  
}

-(void)changeChose:(NSString*)sender

{


    chosen = sender;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    [TitleText setKeyboardType:UIKeyboardTypeDecimalPad];
   
    TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
    [TitleText becomeFirstResponder];
}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
          
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void)textViewDidChange:(UITextView *)textView
{
    
   


    if ([textView.text length]<4) {


        if ([textView.text length] ==0) {
 [underTitleText setText:[NSString stringWithFormat:@"   %@",NSLocalizedString(@"Quantity Per Filling",nil)]];
          
        }
        if ([textView.text length] ==1) {
 [underTitleText setText:[NSString stringWithFormat:@"    %@",NSLocalizedString(@"Quantity Per Filling",nil)]];
         
        }
        if ([textView.text length] ==2) {
 [underTitleText setText:[NSString stringWithFormat:@"      %@",NSLocalizedString(@"Quantity Per Filling",nil)]];
           
        }
        if ([textView.text length] ==3) {
 [underTitleText setText:[NSString stringWithFormat:@"         %@",NSLocalizedString(@"Quantity Per Filling",nil)]];
    
        }


    }
    else
        {
        [textView setText:@""];
        [underTitleText setText:@"   Quantity Per Filling"];
        }
    

    




    

}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{
}

-(void)getParant:(UIViewController*)parant

{

    parantIt =(NewMedecineController*)parant;




  
    
}




- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked )
    {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    
    return YES;
}



@end
