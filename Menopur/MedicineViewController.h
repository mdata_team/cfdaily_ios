//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "colorButton.h"
#import "Medicine.h"
#import "Profiles.h"

#import "GAIDictionaryBuilder.h"

@class CodePopView, Monthview, colorButton, MedicienActiveNotActive;
@interface MedicineViewController : UIViewController<UITextViewDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, retain) Monthview *monthView;




@property (nonatomic, retain) UILabel *NameLabel;
@property (nonatomic, retain) UIImageView *headShot;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) NSMutableArray *MedicineList;
@property (nonatomic, retain) NSMutableArray *MedicineActive;
@property (nonatomic, retain) IBOutlet MedicienActiveNotActive *table;
@property (nonatomic, retain) NSMutableArray *make;
@property (nonatomic, retain) UIView *ProfileView;
@property (nonatomic, retain) NSString *chosenID;
@property  (nonatomic, retain) NSDictionary *active;
@property  (nonatomic, retain) NSTimer *repeatTimer;


@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContextActive;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerNonActive;
@property (nonatomic, assign, getter=isShouldBeginEditing) BOOL shouldBeginEditing;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerCopy;
@property (strong, nonatomic) IBOutlet UITableView *TableViewTime;

@property (strong, nonatomic) IBOutlet UINavigationBar *Navigation;
@property (nonatomic, strong) IBOutlet UILabel *Chosen;
@property (nonatomic, strong) Profiles *currenPros;
@property (nonatomic, strong) Medicine *currenCourse;
@property (strong, nonatomic) IBOutlet UITableView *searbarTo;

-(void)setItems:(Profiles*)set from:(NSString*)text;
-(void)callFunction:(NSDate*)sender;
-(void)GotToHistory:(colorButton*)sender;
- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)setItems:(Profiles*)set;
-(void)chosenMonth:(NSString*)sender;
-(void) renewMedicine:(Medicine*)sender;
-(void)Take:(NSString*)sender;
-(void)Postpone:(NSString*)sender;
-(void)Skip:(NSString*)sender;
-(void)showOnlyActiveMed:(NSDictionary*)set;
-(void) CreateAccount:(Profiles*)set;
-(void)renewTable;

@end
