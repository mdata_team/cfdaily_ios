//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "MedicineViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "InfoViewController.h"
#import "NewProfileController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "MedCell.h"
#import "Monthview.h"
#import "NewMedecineController.h"
#import "ShowMedecineController.h"
#import "Notification.h"
#import "MultipleViewController.h"
#import "colorButton.h"
#import "HistoryViewController.h"
#import "TakeOrTookPop.h"
#import "SkipPop.h"
#import "HistoryIDViewController.h"
#import "Medicine.h"
#import "Profiles.h"
#import "SaveCore.h"
#import "MedicienActiveNotActive.h"

#import "GAIDictionaryBuilder.h"

@interface MedicineViewController ()

@end

@implementation MedicineViewController


@synthesize make;

@synthesize NameLabel;
@synthesize headShot;
@synthesize Vraagantwoord;
@synthesize table;

@synthesize repeatTimer;
@synthesize chosenID;
@synthesize ProfileView;
@synthesize monthView;
@synthesize active;
@synthesize currenPros;
@synthesize MedicineList;
@synthesize MedicineActive;


#pragma mark - view life cycle

- (void)viewDidLoad
{
    //List of Medicines
    
    //********** Google Analytics ****************
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"M_Medicines List_iOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //********** Google Analytics ****************
    
    if ([SHARED_APPDELEGATE isNetworkReachable])
    {
        [SHARED_APPDELEGATE sendHitsInBackground];
    }
    
    
    [super viewDidLoad];
    
    [self.view.layer setMasksToBounds:YES];
    
    //////////////////////////////NSLog(@"viewDidLoad");
  
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.view setTag:45679];

    MedicineList =[[NSMutableArray alloc] init];

    MedicineActive =[[NSMutableArray alloc] initWithObjects:@"YES",@"NO", nil];
      self.navigationItem.title =@"";

    
//      self.navigationItem.title =@"CFDaily";    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];

    table = [[MedicienActiveNotActive alloc] initWithFrame:CGRectMake(0.0, 59, 320, self.view.bounds.size.height-165)];
    table.CombineActive  = [[NSMutableArray alloc] init];
    table.CombineNonActive  = [[NSMutableArray alloc] init];
       table.Total  = [[NSMutableArray alloc] init];
    table.separatorColor = [UIColor grayColor];
    table.backgroundColor = [UIColor clearColor];
    table.rowHeight =130;
    [table setSeparatorInset:UIEdgeInsetsZero];
    [table setTag:8978];
    [table setEditing:NO];
    [self.view addSubview:table];
    
    ProfileView = [[UIView alloc] initWithFrame:CGRectMake(-5, 0, 330, 60)];
    [ProfileView setBackgroundColor:[UIColor whiteColor]];
    
    ProfileView.layer.shadowOffset = CGSizeMake(3, 0);
    ProfileView.layer.shadowOpacity = 2;
    ProfileView.layer.shadowRadius = 2.0;
    [self.view addSubview:ProfileView];
    
    
    headShot = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, 40, 40)];
//    [headShot setBackgroundColor:[UIColor blackColor]];
    [headShot setTag:130];
    [headShot setImage:[UIImage imageNamed:@"headshot"]];
    [ProfileView addSubview:headShot];
    
    
    NameLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 20, 210, 20)];
//    [NameLabel setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //29-06-2017 chanegs
    [NameLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [NameLabel setFont:[UIFont boldSystemFontOfSize:16]];
    [NameLabel setTag:100];
    [NameLabel setBackgroundColor:[UIColor clearColor]];
    [NameLabel setNumberOfLines:3];
    [ProfileView addSubview:NameLabel];


    
    
    
    UIBarButtonItem *edit = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                             target:self
                             action:@selector(CreateAccount)];
    edit.style = UIBarButtonItemStyleBordered;
    
    
  
 
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [edit setTintColor:[UIColor whiteColor]];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:20.0];
        label.textAlignment = NSTextAlignmentCenter;
        // ^-Use NSTextAlignmentCenter for older SDKs.
        label.textColor = [UIColor whiteColor]; // change this color
        self.navigationItem.titleView = label;
          [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
        label.text = self.navigationItem.title;
        [label sizeToFit];
        
    }
    
    else
    {
        
        
    }

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = edit;
    
    
    
    NSArray *profileItem =[NSArray arrayWithObjects:@"Name",@"ID",@"Image",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Silence",@"Vibrate",nil];
    
    
    make =[[NSMutableArray alloc]initWithArray:[SaveCore getProfiles:profileItem]];
    active =[[NSMutableDictionary alloc]init];
    
    
    
    if ([make count] ==1) {
        
  
        
    }
    
    else
    {
        
    
        
        
//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
        
    }
    
    
    myApp.FromScreen=NO;

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    currenPros =appDelegate.currentProfile;
    
    
    appDelegate.chosenID = appDelegate.currentProfile.id;
    
    
    [appDelegate SetProfile:appDelegate.chosenID];
    
    
    UIColor *colorit = [UIColor colorWithRed:[appDelegate.currentProfile.colorRed floatValue] green:[appDelegate.currentProfile.colorGreen   floatValue] blue:[appDelegate.currentProfile.colorblue  floatValue] alpha:1];

  

    
    [ProfileView setBackgroundColor:colorit];
    [NameLabel setText:appDelegate.currentProfile.name];
    [headShot setImage:[UIImage imageWithData:appDelegate.currentProfile.image]];
    
    
    [table setactive:self];
    
    //////////////////////////NSLog(@"viewDidLoad");
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //9-11-2017
    NSString *strMedtype = nil;
    [SHARED_APPDELEGATE setStrMedtype:strMedtype];
    
    [SHARED_APPDELEGATE setNavigationbarIconImage:self.navigationController setimge:YES];
    
//    [self googleAnalyticsGroupping];
    
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    myApp.FromScreen=NO;
    
    
    [myApp.toolbarDown setAlpha:1];
    
    
    
    [myApp reloadTableview:@"callFunction it"];
    
    
    
    [table setactive:self];
    
    [table reloadData];
    
}

-(void)viewDidAppear:(BOOL)animated

{
    
    
    
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    myApp.FromScreen=NO;
    
    
    [myApp.toolbarDown setAlpha:1];
    
    
    
    [myApp reloadTableview:@"callFunction it"];
    
    
    
    [table setactive:self];
    
    [table reloadData];
    
}



-(void)viewDidDisappear:(BOOL)animated
{
    [repeatTimer invalidate];
    repeatTimer =NULL;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)renewTable
{
    [table  removeFromSuperview];
    table=NULL;
    
    table = [[MedicienActiveNotActive alloc] initWithFrame:CGRectMake(0.0, 59, 320, self.view.bounds.size.height-100)];
    table.CombineActive  = [[NSMutableArray alloc] init];
    table.CombineNonActive  = [[NSMutableArray alloc] init];
    table.Total  = [[NSMutableArray alloc] init];
    table.separatorColor = [UIColor grayColor];
    table.backgroundColor = [UIColor clearColor];
    table.rowHeight =130;
    [table setTag:8978];
    [table setEditing:NO];
    [self.view addSubview:table];
}


-(void)callFunction:(NSDate*)sender
{
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
 
     [table setactive:self];

 
    [myApp seeIfNotification];
    
}



-(void)viewanimate

{


    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];




    if (monthView.frame.size.height == 22) {
        [monthView setFrame:CGRectMake(200, 78,107,22*13)];
        [monthView.table setFrame:CGRectMake(0.0, 22, 320, 22*13)];
      
    }
    else
        {
         [monthView setFrame:CGRectMake(200, 78,107,22)];
        [monthView.table setFrame:CGRectMake(0.0, 22, 320, 22)];
      
        }

        [UIView commitAnimations];
    
}




-(void)chosenMonth:(NSString*)sender

{

}

-(void) backAction
{
    //////////////////////////////NSLog(@"backAction");

    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)showalert

{
    /* //27-06-2017 changes
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"To start please click\n \"New User\" to create your\nprofile and start adding\nyour Meds"];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464749];
    [alert addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [alert addButtonWithTitle:NSLocalizedString(@"New user",nil)];
    [alert show];
    */
    
    UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:@"To start please click\n \"New User\" to create your\nprofile and start adding\nyour Meds" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"New user",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        appDelegate.FromScreen=YES;
        
        NewProfileController *controller = [[NewProfileController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];

    }]];

    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
        
    });

    
    
}


-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{

   if (index==1) {


       AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
       
       appDelegate.FromScreen=YES;
       
        NewProfileController *controller = [[NewProfileController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
     

    }

    else
        {


        }
}

-(void) CreateAccount:(Profiles*)set

{
    
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //////////////////////////NSLog(@"%@", set);
   
  
    NSArray *profileItem =[NSArray arrayWithObjects:@"Name",@"ID",@"Image",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Silence",@"Vibrate",nil];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(ID contains %@)", set.id];
    NSMutableArray *notificationArray = [[NSMutableArray alloc] initWithArray:[[SaveCore getProfiles:profileItem] filteredArrayUsingPredicate:predicate]];
    
    make = [notificationArray objectAtIndex:0];
    
    
    
    UIColor *colorit = [UIColor colorWithRed:[set.colorRed floatValue] green:[set.colorGreen  floatValue] blue:[set.colorblue  floatValue] alpha:1];

    
    [ProfileView setBackgroundColor:colorit];
    [NameLabel setText:set.name];
    [headShot setImage:[UIImage imageWithData:set.image]];
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.CurrentID =[make valueForKey:@"ID"];
    
    appDelegate.FromScreen=YES;
    NewMedecineController *controller = [[NewMedecineController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
    [controller setNew:set.name];
   [controller getParantGo:self];
    [controller editOrShow:@"Add"];
        
        });
   
    
    
}

-(void) CreateAccount

{
    
      AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.CurrentID =currenPros.id;
    
    
    
    //////////////////////////////NSLog(@"%@", appDelegate.CurrentID);
    appDelegate.FromScreen=YES;

    
    NewMedecineController *controller = [[NewMedecineController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
    [controller setNew:currenPros.name];
    [controller getParantGo:self];
    [controller editOrShow:@"Add"];
    
    
}

-(void)setItems:(Profiles*)set from:(NSString*)text

{
    
    //////////////////////////NSLog(@"%@", text);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    currenPros =set;
    
    
    appDelegate.chosenID = set.id;
    
    
    
    
    [appDelegate SetProfile:appDelegate.chosenID];
    
    
    
    UIColor *colorit = [UIColor colorWithRed:[set.colorRed floatValue] green:[set.colorGreen  floatValue] blue:[set.colorblue  floatValue] alpha:1];
;
  
    
    

    
    [ProfileView setBackgroundColor:colorit];
    [NameLabel setText:set.name];
    [headShot setImage:[UIImage imageWithData:set.image]];
    
    
    [table setactive:self];
    
    
    
}



-(void)setItems:(Profiles*)set

{

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    currenPros =set;
    
    
    appDelegate.chosenID = set.id;
    
    
    
    
    [appDelegate SetProfile:appDelegate.chosenID];
    
    
    
    UIColor *colorit = [UIColor colorWithRed:[set.colorRed floatValue] green:[set.colorGreen  floatValue] blue:[set.colorblue  floatValue] alpha:1];
    

    
 
    
    [ProfileView setBackgroundColor:colorit];
    [NameLabel setText:set.name];
    [headShot setImage:[UIImage imageWithData:set.image]];
    
    
    [table setactive:self];



}



-(NSDate*)calculatenow

{
    
    NSDate *now = [[NSDate alloc] init];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:now];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:now];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]+1];
    [dateComps setMinute:[timeComponents minute]];
    [dateComps setSecond:0];
    
    
    return [calendar dateFromComponents:dateComps];
}



-(void)PresentView:(UIButton*)sender

{
    
}

- (IBAction)instructie:(UIButton *)sender {

}

- (IBAction)Vragen:(UIButton *)sender {


}


- (IBAction)bijsluiter:(UIButton *)sender {


    

    
}

- (IBAction)about:(UIButton *)sender {



}


- (IBAction)Cancel:(UIButton *)sender {


}

- (IBAction)OK:(UIButton *)sender {

 
   
 
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}
- (void)Offerte

{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
  
    
}


-(void)showOnlyActiveMed:(NSDictionary*)set

{

    
    active =set;
    
}





- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self becomeFirstResponder];
	[self dismissViewControllerAnimated:YES completion:NULL];
}






-(void)popview

{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    
    InfoViewController *controller = [[InfoViewController alloc]init];
    [self presentViewController:controller animated:YES completion:Nil];
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}


-(void)Take:(NSString*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [appDelegate.taketook setSingle];
    appDelegate.FromScreen= YES;
    [appDelegate TakenOrtookUp:YES];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //[self callFunction:[NSDate date]];
        [table reloadData];
    });
    
}


-(void)Take2:(NSString*)sender
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

 
    [appDelegate.taketook setSingle];
    appDelegate.FromScreen= YES;
    [appDelegate TakenOrtookUp:YES];
  double delayInSeconds = 2.0;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //[self callFunction:[NSDate date]];
    [table reloadData];
});
    

}
-(void)Postpone:(NSString*)sender
{

 
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
      [appDelegate.taketook setSingle];
    appDelegate.FromScreen= YES;
    
    [appDelegate Postpone:NULL];
    appDelegate.Choisestring =NSLocalizedString(@"Postpone",nil);
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //[self callFunction:[NSDate date]];
        [table reloadData];
    });
}

-(void)Skip:(NSString*)sender
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   
    
      [appDelegate.skiptake setSingle];
     appDelegate.FromScreen= YES;
  [appDelegate TakenSkipUp:YES];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //[self callFunction:[NSDate date]];
        [table reloadData];
    });
}

-(void)GotToHistory:(colorButton*)sender
{
    [self addActionForGoogleAnalytics:@"Medicine List - History"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.FromScreen=YES;
    
    HistoryIDViewController *controller2 = [[HistoryIDViewController alloc]init];
    
    [controller2 setItems:sender.chosenCourse setPros:currenPros];
    
    [self.navigationController pushViewController:controller2 animated:YES];
    [SHARED_APPDELEGATE setNavigationbarIconImage:self.navigationController setimge:NO];

    
}


-(void) renewMedicine:(Medicine*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.FromScreen=YES;
    
    ShowMedecineController *controller2 = [[ShowMedecineController alloc]init];
    

    
 
    [self.navigationController pushViewController:controller2 animated:YES];
    
    
    [controller2 setItems:sender];
    
    [controller2 getParantGo:self];
   
    
    
}



-(void) turnbuttons:(NSString*) setter

{

}

//#pragma mark - Google Analytics
//
//-(void)googleAnalyticsGroupping
//{
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:2]value:@"Medicines List"];
//}

#pragma mark - Google Analytics - Action

-(void)addActionForGoogleAnalytics:(NSString *)actionName
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"button_press"
                                                           label:actionName
                                                           value:nil] build]];
}



@end

