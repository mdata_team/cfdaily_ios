//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "MedsProfileViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewProfileController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "HistoryCell.h"
#import "Monthview.h"




@interface MedsProfileViewController ()

@end

@implementation MedsProfileViewController



@synthesize NameLabel;
@synthesize headShot;
@synthesize Vraagantwoord;
@synthesize table;

@synthesize chosenID;
@synthesize ProfileView;
@synthesize monthView;


- (void)viewDidLoad
{
    [super viewDidLoad];

    
    
         [self.view.layer setMasksToBounds:YES];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   


    
    
    Vraagantwoord =[[NSMutableArray alloc]init];
    
  self.navigationItem.title =@"MedsProfileViewController";
    // this will appear as the title in the navigation bar
   
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];





    table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 120, 320, self.view.bounds.size.height-168-120)];
    table.separatorColor = [UIColor clearColor];
    table.backgroundColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.000];
    table.rowHeight =260;
    table.delegate = self;
    table.dataSource = self;
    [table setEditing:NO];
    [table setEditing:NO];
    [self.view addSubview:table];


   ProfileView = [[UIView alloc] initWithFrame:CGRectMake(-5, 0, 330, 120)];


    [self.view addSubview:ProfileView];



    headShot = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 80, 80)];
//    [headShot setBackgroundColor:[UIColor blackColor]];
    [headShot setTag:130];
    [headShot setImage:[UIImage imageNamed:@"headshot"]];
    [ProfileView addSubview:headShot];



  





    NameLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 20, 210, 20)];
    [NameLabel setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    [NameLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [NameLabel setTag:120];
    [NameLabel setBackgroundColor:[UIColor clearColor]];
    [NameLabel setNumberOfLines:3];
    [ProfileView addSubview:NameLabel];


  



        //Month_view.png



//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
    //     [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
           
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:20.0];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
              [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            [label sizeToFit];
    
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;






    monthView = [[Monthview alloc] initWithFrame:CGRectMake(200, 78,107,22)];
    [monthView setBackgroundColor:[UIColor colorWithRed:0.941 green:0.941 blue:0.941 alpha:1.000]];
    [monthView.layer setBorderWidth:1];
    [monthView.layer setCornerRadius:10];
    [monthView setParant:self];
    [monthView.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [monthView.layer setMasksToBounds:YES];
    [self.view addSubview:monthView];


    UIButton *View = [[UIButton alloc] initWithFrame:CGRectMake(200, 78,107,22)];

    [View addTarget:self action:@selector(viewanimate) forControlEvents:UIControlEventTouchUpInside];
    [View setImage:[UIImage imageNamed:@"Month_view.png"] forState:UIControlStateNormal];
    [self.view addSubview:View];


    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        //NSLog(@"left");
        table.transform = CGAffineTransformMakeScale(-1, 1);
        
        
    }
    

    [appDelegate.toolbarDown setAlpha:1];
}

-(void)viewanimate

{


    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];




    if (monthView.frame.size.height == 22) {
        [monthView setFrame:CGRectMake(200, 78,107,22*13)];
        [monthView.table setFrame:CGRectMake(0.0, 22, 320, 22*13)];
      
    }
    else
        {
         [monthView setFrame:CGRectMake(200, 78,107,22)];
        [monthView.table setFrame:CGRectMake(0.0, 22, 320, 22)];
      
        }

        [UIView commitAnimations];
    
}



- (void)viewDidLoad2
{
    [super viewDidLoad];


            [self.view.layer setMasksToBounds:YES];
    

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;


   



    table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0, 320, self.view.bounds.size.height-88)];
    table.separatorColor = [UIColor clearColor];
    table.backgroundColor = [UIColor clearColor];
    table.rowHeight =260;
    table.delegate = self;
    table.dataSource = self;
    table.layer.shadowOffset = CGSizeMake(3, 0);
    table.layer.shadowOpacity = 2;
    table.layer.shadowRadius = 2.0;
    [table setEditing:NO];
    [table setEditing:NO];
    [self.view addSubview:table];




   


    [self showalert];

    
}

-(void)setitemsDictionary:(NSString*)set name:(NSString*)name

{
    
}

-(void)chosenMonth:(NSString*)sender

{




   
    
}

-(void) backAction
{


    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)showalert

{
    /* //27-06-2017 changes
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"To start please click\n \"New User\" to create your\nprofile and start adding\nyour medication"];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464749];
    [alert addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [alert addButtonWithTitle:NSLocalizedString(@"New user",nil)];
    [alert show];
    */
    
    UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:@"To start please click\n \"New User\" to create your\nprofile and start adding\nyour medication" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"New user",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NewProfileController *controller = [[NewProfileController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];

    }]];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
        
    });

    
    
}


-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{


    
    
    if (index==1) {


        NewProfileController *controller = [[NewProfileController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];

    }

    else
        {



        
        }
        }

-(void) CreateAccount

{
    NewProfileController *controller = [[NewProfileController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];

}


-(void)setItems:(NSMutableDictionary*)set

{

    chosenID = [set valueForKey:@"ID"];

    UIColor *colorit = [UIColor colorWithRed:[[set valueForKey:@"ColorRed"] floatValue] green:[[set valueForKey:@"ColorGreen"]  floatValue] blue:[[set valueForKey:@"Colorblue"]  floatValue] alpha:[[set valueForKey:@"Coloralpha"]  floatValue]];




    [ProfileView setBackgroundColor:colorit];
    ProfileView.layer.shadowOffset = CGSizeMake(3, 0);
    ProfileView.layer.shadowOpacity = 2;
    ProfileView.layer.shadowRadius = 2.0;
    [NameLabel setText:[set valueForKey:@"Name"]];
    [headShot setImage:[set valueForKey:@"Image"]];

   


    NSArray *profileItem =[NSArray arrayWithObjects:@"ID",@"Date",@"Time",@"Name",@"Dose",@"Taken",@"ID_Medicine",@"Count",@"Time_Action",@"Action",@"Quantity",@"Type",@"Time_Action_24",@"Original_Time",@"Action_Date",nil];


    Vraagantwoord= [GetData getHistory:profileItem OnID:chosenID];

   //
    [table reloadData];

}

-(void)viewWillAppear:(BOOL)animated
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:1];
    
    double delayInSeconds = 0.2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [appDelegate.toolbarDown setAlpha:1];
    });
    
    
    [table reloadData];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
        [appDelegate.toolbarDown setAlpha:1];
    
    double delayInSeconds = 0.2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [appDelegate.toolbarDown setAlpha:1];
    });
    
 
[table reloadData];

    
  
}

-(void)PresentView:(UIButton*)sender

{
    
}

- (IBAction)instructie:(UIButton *)sender {



}

- (IBAction)Vragen:(UIButton *)sender {


    


        //VragenViewController
    
}


- (IBAction)bijsluiter:(UIButton *)sender {


 
}

- (IBAction)about:(UIButton *)sender {



}


- (IBAction)Cancel:(UIButton *)sender {


}

- (IBAction)OK:(UIButton *)sender {

 
   
 
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [Vraagantwoord count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(HistoryCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {



    [cell FillAllItems:[Vraagantwoord objectAtIndex:indexPath.row]];


}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

  

    return 80;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{





}


- (HistoryCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {




    NSString *identifier = @"CellIdentifier";
    HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

	if (cell == nil) {

        cell = [[HistoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    else
        
        {
        
        }
    
    
    return cell;
    
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{

}

@end
