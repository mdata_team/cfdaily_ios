//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "NHMailActivity.h"
@class CodePopView, NewProfileController, Profiles;
@interface MedsViewController : UIViewController<UITextViewDelegate, UIAlertViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate, UITableViewDelegate, NHCalendarActivityDelegate,NSFetchedResultsControllerDelegate>
@property (nonatomic, retain) NewProfileController *controller;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) IBOutlet UITableView *table;


@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, assign, getter=isShouldBeginEditing) BOOL shouldBeginEditing;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerCopy;
@property (strong, nonatomic) IBOutlet UITableView *TableViewTime;
@property (strong, nonatomic) Profiles *chosenProfile;

@property (nonatomic, retain) UIButton *button;
- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)setNew;


@end
