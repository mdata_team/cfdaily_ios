//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMedecineController.h"
#import "MedicineViewController.h"

@class CodePopView, NewMedecineController, TekstIDLabel, TextViewData;
@interface MinimumViewController : UIViewController<UITextViewDelegate, subViewDelegate> {


    UITextView *TitleText;
   NSString *Compaire;
    UIView *Combi;
     UIView *CombiOther;
    NSString *chosen;
      NSString *chosenID;
    NSString *theTime;
    NSString *theDay;
    NSString *CompaireDecimal;
}

@property (nonatomic, retain) IBOutlet UILabel *underTitleText;
@property (nonatomic, retain) IBOutlet NSString *timeframe;
@property (nonatomic, retain) IBOutlet  NSMutableArray *TimeChoses;
@property (nonatomic, retain) NSString *theTime;
@property (nonatomic, retain) NSString *theDay;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) TextViewData *textblock;
-(void)whatValue:(NSString*)set;

-(void)chosenValue:(NSString*)sender;
-(void)chosenMonth:(NSString*)sender;
-(void)setChoice:(NSString*)gothere;
-(void)change:(UIButton*)sender;
-(void) setTextview: (NSString *) text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
@end


