//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "MinimumViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "PickerViewContent.h"
#import "AppDelegate.h"
#import "TextViewData.h"
@interface MinimumViewController ()

@end

@implementation MinimumViewController
@synthesize parantIt;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize theTime;
@synthesize theDay;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize underTitleText;
@synthesize textblock;



-(void)loadView{


    [super loadView];
}

-(void)whatValue:(NSString*)set

{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    [textblock.Content setText:set];
    [textblock.Value setText:appDelegate.dose_unit];
    
    [textblock changeValue];
    
    
  
    
    CompaireDecimal =[[[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0] valueForKey:@"Dose_unit"];
    
    
    NSArray *numbers2 = [CompaireDecimal componentsSeparatedByString:@","];
    
    if ([numbers2 count]>1) {
        
        
        PickerViewContent *content = [[PickerViewContent alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-180, 320, 180)];
        // [content setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        
        [content Decimal:CompaireDecimal pernt:self];
        
        [self.view addSubview:content];
     
        
    }
    else
    {
        PickerViewContent *content = [[PickerViewContent alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-180, 320, 180)];
        // [content setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        
        [content Decimal:CompaireDecimal pernt:self];
        
        [self.view addSubview:content];
        
     
    }
    

      [appDelegate.toolbarDown setAlpha:0];

}
-(void)whatLabel:(UILabel*)set

{
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    

    
    if ([set.text isEqualToString:@" "]) {
        
        
        
    }
    else
        
    {
     
        
        
    }
    
    parantLabel =(TekstIDLabel*)set;
    
    
      NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
   [underTitleText setText:[typit valueForKey:@"Minimume_name"]];
  
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {
       
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
        
    {
         [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Hebrew"])
        
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Arabic"])
        
    {
          [underTitleText setAlpha:0];
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
        
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
    {
        [underTitleText setAlpha:0];
    }

    
    
    CompaireDecimal =NSLocalizedString([typit valueForKey:@"Dose_unit"],nil);
    //Med Strenght
 
    
    NSArray *numbers2 = [CompaireDecimal componentsSeparatedByString:@","];
    
    if ([numbers2 count]>1) {
        
        
        PickerViewContent *content = [[PickerViewContent alloc] initWithFrame:CGRectMake(0, 0, 200, 180)];
       // [content setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        
        [content Decimal:CompaireDecimal pernt:self];
        
        
        TitleText.inputView = content;
    }
    else
    {
        PickerViewContent *content = [[PickerViewContent alloc] initWithFrame:CGRectMake(0, 0, 200, 180)];
       // [content setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        
        [content Decimal:CompaireDecimal pernt:self];
        
        TitleText.inputView = content;
        
     
    }
    
}



- (void)viewDidLoad {

       NSLog(@"%@", self);
    
    
    [self.view.layer setMasksToBounds:YES];

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

      TimeChoses =[[NSMutableArray alloc]init];


    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];

    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Cancel:)];
   
    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Cancel setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }



    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];


    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
    UIView *textit  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 8, 160, 30)];
    [title setTextColor:[UIColor whiteColor]];
    [title setText:[typit valueForKey:@"Minimume_name"]];
    
    //15-11
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
    {
        if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
        {
            title.text =[title.text stringByReplacingOccurrencesOfString:@"tabletter" withString:@"mengde"];
        }
    }
    
    
    [title setFont:[UIFont boldSystemFontOfSize:10]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setNumberOfLines:2];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];
    [textit addSubview:title];


    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:textit];




    UIBarButtonItem *Save = [[UIBarButtonItem alloc]
                             initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleBordered target:self
                             action:@selector(Save:)];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Save setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }




    UIToolbar *toolbarUp = [[UIToolbar alloc] init];
    toolbarUp.frame = CGRectMake(0, -2, 320, 54);
   
   
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        
        
//        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk"] forToolbarPosition:0 barMetrics:0]; //29-06-2017 changes
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forToolbarPosition:0 barMetrics:0];

    }
    
    else
    {
//        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk.png"] forToolbarPosition:0 barMetrics:0]; //29-06-2017 changes
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forToolbarPosition:0 barMetrics:0];

    }
    [toolbarUp setTranslucent:NO];
   [self.view addSubview:toolbarUp];


    NSArray *itemsUp = [NSArray arrayWithObjects:Cancel,flexItem,random,flexItem,Save,nil];
    toolbarUp.items = itemsUp;

    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];

    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 64, self.view.frame.size.width-20, 44)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
//    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];



    underTitleText = [[UILabel alloc] initWithFrame:CGRectMake(20, 4, self.view.frame.size.width-40, 35)];
    [underTitleText setBackgroundColor:[UIColor clearColor]];
    [underTitleText setText:[typit valueForKey:@"Minimume_name"]];
    
   
    //15-11
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
    {
        if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
        {
            underTitleText.text =[underTitleText.text stringByReplacingOccurrencesOfString:@"tabletter" withString:@"mengde"];
        }
    }
    

    
//    [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [underTitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];

    [underTitleText setFont:[UIFont boldSystemFontOfSize:13]];
    [Combi addSubview:underTitleText];
    
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {
        
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
    {
        [underTitleText setAlpha:0];
    }

    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
        
    {
          [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Ελληνικά"])
        
    {
        [underTitleText setAlpha:0];
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
    {
        [underTitleText setAlpha:0];
    }

    


    
    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 4, self.view.frame.size.width-40, 35)];
    [TitleText setBackgroundColor:[UIColor clearColor]];
    [TitleText setDelegate:self];
    [TitleText setText:@""];
//    [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [TitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [TitleText setTag:123+1];
    [TitleText setTextAlignment:NSTextAlignmentRight];
    
    [TitleText setFont:[UIFont boldSystemFontOfSize:15]];
    [Combi addSubview:TitleText];
    [TitleText setAlpha:0];
    
    
    textblock = [[TextViewData alloc] initWithFrame:CGRectMake(20, 4,self.view.frame.size.width-50, 35)];
    [Combi addSubview:textblock];


    NSDate *now = [[NSDate alloc] init];

     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     
    [dateFormatter setDateFormat:@"EEEE"];
    
    theDay = [dateFormatter stringFromDate:now];


   


    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [timeFormat setDateFormat:@"hh:mm"];
    theTime = [timeFormat stringFromDate:now];

   


    NSDateFormatter *timeFormat3 = [[NSDateFormatter alloc] init];
     [timeFormat3 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [timeFormat3 setDateFormat:@"a"];
    
    timeframe = [timeFormat3 stringFromDate:now];


    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        NSLog(@"left");
        Combi.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        TitleText.transform = CGAffineTransformMakeScale(-1, 1);
        [TitleText setTextAlignment:NSTextAlignmentNatural];
        
        
        
        underTitleText.transform = CGAffineTransformMakeScale(-1, 1);
        [underTitleText setTextAlignment:NSTextAlignmentNatural];
        
        
        //textblock.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }

  
   
}

-(void)Cancel:(UIBarButtonItem*) sender

{

      NSArray *abc=[NSArray arrayWithObjects:@"24hr",@"%",@"a",@"a",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i",@"j",@"k",@"l",@"m",@"n",@"o",@"p",@"q",@"r",@"s",@"t",@"u",@"v",@"w",@"x",@"y",@"z",@"/",@"L",@"24hr",@"U",@"I",@" ",@"%",@"%" , @",",nil];
    
   for (NSInteger i=0 ; i<[abc count]; i++) {
        
        
        
        TitleText.text = [TitleText.text stringByReplacingOccurrencesOfString:[abc objectAtIndex:i] withString:@""];
        
        
    }

        //[self.view.superview method]

    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];


}

-(void)Save:(UIBarButtonItem*) sender

{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [parantLabel setText:[NSString stringWithFormat:@"%@ %@", TitleText.text, appDelegate.dose_unit]];
    
    
    
    [TitleText resignFirstResponder];
    
    [parantIt setitemsDictionary:appDelegate.dose_unit name:@"Dose_unit"];
    [parantIt setitemsDictionary:textblock.Content.text name:@"Refilling"];
    
    [self dismissViewControllerAnimated:YES completion:NULL];

    
    

}

-(void)chosenValue:(NSString*)sender;


{
    
    [textblock.Content setText:sender];
    
    [textblock changeValue];
}

-(void)chosenMonth:(NSString*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.dose_unit = NSLocalizedString(sender,nil);
    [textblock.Value setText:NSLocalizedString(sender,nil)];
    
    [textblock changeValue];
    
    
    
}


-(void)change:(UIButton*)sender

{

    if (sender.tag ==140 ||sender.tag ==180) {


    }
    if (sender.tag ==141 ||sender.tag ==181) {

    }
    if (sender.tag ==142 ||sender.tag ==182) {


        TimesofDayViewController *controller = [[TimesofDayViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (sender.tag ==143 ||sender.tag ==183) {

    }
    if (sender.tag ==144 ||sender.tag ==184) {

    }
    
   
  
  

    
}

-(void)Action:(UIButton*)sender

{

  
}

-(void)changeChose:(NSString*)sender

{


    chosen = sender;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    [TitleText setKeyboardType:UIKeyboardTypeDecimalPad];
   
     TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:NO];
    [TitleText becomeFirstResponder];
}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:NO];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}
-(void)setChoice:(NSString*)gothere

{
    
}

-(void)textViewDidChange:(UITextView *)textView
{
 


    

}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{
}

-(void)getParant:(UIViewController*)parant

{

    parantIt =(NewMedecineController*)parant;




  
    
}




- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
