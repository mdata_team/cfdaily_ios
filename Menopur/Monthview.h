//
//  Monthview.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HistoryViewController;
@interface Monthview : UIView <UITableViewDataSource, UITableViewDelegate>


-(void)setParant:(UIViewController*)set;
@property (nonatomic, retain) IBOutlet UITableView *table;
@property (nonatomic, retain) HistoryViewController *Parantit;
@property (nonatomic, retain) NSMutableArray *MonthList;

@end
