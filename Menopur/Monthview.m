//
//  Monthview.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "Monthview.h"
#import "GetData.h"
#import "HistoryViewController.h"
#import <QuartzCore/QuartzCore.h>

@implementation Monthview
@synthesize table;
@synthesize Parantit;
@synthesize MonthList;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

     

        table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 22, 100, 22)];
        table.separatorColor = [UIColor colorWithRed:0.114 green:0.533 blue:0.576 alpha:1.000];
        table.backgroundColor = [UIColor whiteColor];
        table.rowHeight = 22;
        [table.layer setCornerRadius:3];
        [table.layer setBorderWidth:1.5];
        table.delegate = self;
        table.dataSource = self;
        [table setEditing:NO];
        [self addSubview:table];
        
        
        
        
        
  

    }
    return self;
}


-(void)setParant:(HistoryViewController*)set

{

       MonthList =[[NSMutableArray alloc] initWithObjects: NSLocalizedString(@"By Date",nil), NSLocalizedString(@"By Name", nil),nil];
    Parantit =set;


   
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [MonthList count];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{



    return 22;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{

    [Parantit chosenMonth:[MonthList objectAtIndex:indexPath.row]];
    [Parantit viewanimate];

    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *identifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

	if (cell == nil) {

        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellSelectionStyleGray reuseIdentifier:identifier];

        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];

    }
    
    else
        {
        
        }

//    cell.textLabel.text = [MonthList objectAtIndex:indexPath.row];
//     [cell.textLabel setFont:[UIFont boldSystemFontOfSize:8]];
//    [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
//    [cell.textLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];

    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, cell.frame.size.width, 22)];
    label.text = [MonthList objectAtIndex:indexPath.row];
    [label setFont:[UIFont boldSystemFontOfSize:8]];
//    label.textAlignment = NSTextAlignmentRight;
    [label setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [cell.contentView addSubview:label];
    
//    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
//
//
//        //NSLog(@"left");
//
//          [cell.textLabel setTextAlignment:NSTextAlignmentNatural];
//
//    }
//    else
//    {
//
//    }
    
 

  
    return cell;
    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
