//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMedecineController.h"
#import "MedicineViewController.h"

@class CodePopView, NewMedecineController, TekstIDLabel;
@interface MultipleViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    
    
    UITextView *TitleText;
    NSString *Compaire;
    UIView *Combi;
    UIView *CombiOther;
    NSString *chosen;
    NSString *chosenID;
    NSString *theTime;
    NSString *theDay;
    UILabel *titlelabel;
     NSString *WhatyouWant;
    
    UIBarButtonItem *tmpSender;
}
-(void)reloadAll:(UILocalNotification*)Remove;
@property (nonatomic, retain) NSString *WhatyouWant;
@property (nonatomic, retain) IBOutlet UILabel *titlelabel;
@property (nonatomic, retain) IBOutlet UIView *Postponeview;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbarDown;
@property (nonatomic, retain) NSMutableArray *SectionList;
@property (nonatomic, retain) IBOutlet UITextView *underTitleText;
@property (nonatomic, retain) IBOutlet NSString *timeframe;
@property (nonatomic, retain) IBOutlet  NSMutableArray *TimeChoses;
@property (nonatomic, retain) IBOutlet  NSMutableArray *PtofileMulti;
@property (nonatomic, retain) NSString *theTime;
@property (nonatomic, retain) NSString *theDay;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;
@property (nonatomic, retain) IBOutlet UITableView *table;

-(void)reloaddata;
-(void)whatLabel:(UILabel*)set;
@end


