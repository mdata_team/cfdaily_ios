//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "MultipleViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "AlertCellMore.h"
#import "AlertPop.h"
#import "SaveCore.h"
#import "Notification.h"
#define MIN_CELL_HEIGHT 30
@interface MultipleViewController ()

@end

@implementation MultipleViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize theTime;
@synthesize theDay;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize underTitleText;
@synthesize table;
@synthesize SectionList;
@synthesize WhatyouWant;
@synthesize PtofileMulti;
@synthesize titlelabel;



-(void)loadView{
    
    
    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    
    
    parantLabel =(TekstIDLabel*)set;
    
}


-(void)setLanguageSpecificItems
{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"AppleLanguages"] isEqual:[NSArray arrayWithObjects:@"en", nil]]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"nl", nil]
                                                  forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil]
                                                  forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}


- (void)viewDidLoad {

    NSLog(@"%@", self);
    
    
    TimeChoses =[[NSMutableArray alloc]init];
    
  
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Cancel:)];
  
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    
    // NSLog((@"%@",[deviceType substringWithRange:range]);

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        [Cancel setTintColor:[UIColor whiteColor]];
    }
    else
    {
    }

    
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    UIBarButtonItem *Done = [[UIBarButtonItem alloc]
                             initWithTitle:NSLocalizedString(@"Done",nil) style:UIBarButtonSystemItemCancel target:self
                             action:@selector(Done:)];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
     [Done setTintColor:[UIColor whiteColor]];
    }
    
    
    UIView *textit  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 40)];
    
    titlelabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 160, 40)];
    [titlelabel setTextColor:[UIColor whiteColor]];
    [titlelabel setFont:[UIFont boldSystemFontOfSize:12]];
    [titlelabel setTextAlignment:NSTextAlignmentCenter];
    [titlelabel setTag:120];
    [titlelabel setNumberOfLines:2];
    [titlelabel setBackgroundColor:[UIColor clearColor]];
    [titlelabel setNumberOfLines:3];
    
    [textit addSubview:titlelabel];
  
    UIBarButtonItem *randomtitle = [[UIBarButtonItem alloc] initWithCustomView:textit];
    

    
 
 
    
    
    
    UIToolbar *toolbarUp = [[UIToolbar alloc] init];
    toolbarUp.frame = CGRectMake(0, -2, 320, 54);
    
   
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        
        
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forToolbarPosition:0 barMetrics:0];
        
    }
    
    else
    {
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk_nav.png"] forToolbarPosition:0 barMetrics:0];
    }
    [toolbarUp setTranslucent:NO];
    [self.view addSubview:toolbarUp];
    
    
    NSArray *itemsUp = [NSArray arrayWithObjects:Cancel,flexItem,randomtitle,flexItem,Done,nil];
    toolbarUp.items = itemsUp;
    
 
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];

    
 
    
    
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];
    
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, 320, self.view.frame.size.height-54)];
    table.separatorColor = [UIColor blackColor];
//    table.backgroundColor =[UIColor colorWithRed:0.859 green:0.918 blue:0.949 alpha:1.000]; //01-07-2017
    table.backgroundColor =[UIColor colorWithRed:250.0f/255.0f green:241.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
    table.rowHeight =60;
    [table.layer setBorderWidth:1];
    [table.layer setBorderColor:[UIColor colorWithRed:0.216 green:0.627 blue:0.651 alpha:1.000].CGColor];
    table.delegate = self;
    table.dataSource = self;
    [table setEditing:NO];
    [table setSeparatorInset:UIEdgeInsetsZero];
    [self.view addSubview:table];
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    ////////////////////////////////NSLog(@"%@", appDelegate.alert.NotificationList);
    
    [appDelegate.toolbarDown setAlpha:0];
    
    
}

-(void)Cancel:(UIBarButtonItem*) sender

{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.FromScreen=NO;
    
    //[self.view.superview method]
    
 
    [appDelegate.toolbarDown setAlpha:1];
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
 

    
    
}



-(void)Done:(UIBarButtonItem*) sender

{
    // Ashok
    // the refrence of this button is taken in global variable, so this can be sent to in the method of appdelegate when clicked on the SAVE button in UIAlertview.
    tmpSender=sender;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.FromScreen=NO;
    
    if ([WhatyouWant isEqualToString:@"take"]) {
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate Postponeit:tmpSender];
        
        
    }
    else if ([WhatyouWant isEqualToString:@"SkipMore"]) {
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate SkipMoreAction:tmpSender];
        //SkipIt
    }
    
    else if ([WhatyouWant isEqualToString:@"Postpone"]) {
        
        
        [appDelegate PostponeMoreAction:tmpSender];
        //SkipIt
        
    }
    [appDelegate.toolbarDown setAlpha:1];
    
    [self dismissViewControllerAnimated:YES completion:NULL];


    return;
    /* 26-06-2017 change
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Save the changes?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    alert.tag=5555;
//    [alert show];
*/
     
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Save the changes?" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self saveTheChanges];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
       //in before code alert show commnted that's why we are comment below line.
//        [[SHARED_APPDELEGATE window].rootViewController presentViewController:alert animated:Yellow completion:nil];
        
    });
 
 }

/*
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==5555) {
        if (buttonIndex==1)
        {
            [self saveTheChanges];
        }
    }
}
 */

-(void)saveTheChanges
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.FromScreen=NO;
    
    if ([WhatyouWant isEqualToString:@"take"]) {
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate Postponeit:tmpSender];
        
        
    }
    else if ([WhatyouWant isEqualToString:@"SkipMore"]) {
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate SkipMoreAction:tmpSender];
        //SkipIt
    }
    
    else if ([WhatyouWant isEqualToString:@"Postpone"]) {
        
        
        [appDelegate PostponeMoreAction:tmpSender];
        //SkipIt
        
    }
    [appDelegate.toolbarDown setAlpha:1];
    
    [self dismissViewControllerAnimated:YES completion:NULL];

    
}




- (void)tableView:(UITableView *)tableView willDisplayCell:(AlertCellMore *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
 
    
    [cell.label setAlpha:1];
 
    [cell.label setFrame:CGRectMake(0, 0, 320, 15)];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSDictionary *dictionary = [appDelegate.alert.NotificationList objectAtIndex:indexPath.section];
    NSArray *array = [dictionary objectForKey:@"Name"];
    
    
    [cell FillAllItems:[array objectAtIndex:indexPath.row] index:indexPath.row];
    

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    return [appDelegate.alert.NotificationList count];
}



-(void)reloaddata


{
        NSArray *profileItem =[NSArray arrayWithObjects:@"Name",@"ID",@"Image",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Silence",@"Vibrate",nil];
    
    
   
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.FromScreen=YES;
    
  
    
       PtofileMulti =[[NSMutableArray alloc]init];
    
    PtofileMulti =[SaveCore getProfiles:profileItem];
    
    
    [appDelegate.toolbarDown setAlpha:0];
    
    
    [table reloadData];
    
}



-(void)reloadAll:(UILocalNotification*)Remove

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    

    
    [appDelegate.selectedMeds removeObject:Remove];
 
    
    [self.table reloadData];
    
 
    
    //

    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    NSString *result;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *dictionary = [appDelegate.alert.NotificationList objectAtIndex:section];
    NSArray *array = [dictionary objectForKey:@"Name"];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];

    
    if ([array count]>0)
    {
        
        UILocalNotification *notif = [array objectAtIndex:0];
        
        
        
        result = [notif.userInfo valueForKey:@"Profile"];
        
       
        
        
        NSInteger dexer =  [[PtofileMulti valueForKey:@"ID"] indexOfObject:[notif.userInfo valueForKey:@"ID"]];
        
        //Profiles *setcours = [appDelegate GetProfilesWithID:[sender.userInfo valueForKey:@"ID"]];
        
        UIColor *colorit = [UIColor colorWithRed:[[[PtofileMulti valueForKey:@"ColorRed"] objectAtIndex:dexer] floatValue] green:[[[PtofileMulti valueForKey:@"ColorGreen"] objectAtIndex:dexer] floatValue] blue:[[[PtofileMulti valueForKey:@"Colorblue"] objectAtIndex:dexer] floatValue] alpha:[[[PtofileMulti valueForKey:@"Coloralpha"] objectAtIndex:dexer] floatValue]];
        

        
        
        UILabel *sectionHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        sectionHeader.backgroundColor = colorit;
        sectionHeader.textColor = [UIColor blackColor];
        sectionHeader.font = [UIFont systemFontOfSize:14];
        sectionHeader.text =  [NSString stringWithFormat:@"              %@", result];
        sectionHeader.textAlignment=NSTextAlignmentLeft;
         [view addSubview:sectionHeader];
        
        
        UILabel *Line = [[UILabel alloc] initWithFrame:CGRectMake(0, 49, 320, 1)];
        Line.backgroundColor = [UIColor grayColor];
        [view addSubview:Line];
        
        UIImageView *headShot = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
        [headShot setImage:[[PtofileMulti valueForKey:@"Image"] objectAtIndex:dexer]];
        [headShot setBackgroundColor:[UIColor grayColor]];
        [headShot setTag:130];
        [view addSubview:headShot];
        
    
             
        
    }
    else
    {
       
    }
    
    
         return view;
    

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   return 50;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *dictionary = [appDelegate.alert.NotificationList objectAtIndex:section];
    NSArray *array = [dictionary objectForKey:@"Name"];
    
  
    
    return [array count];
}




- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
   
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *dictionary = [appDelegate.alert.NotificationList objectAtIndex:section];
    NSArray *array = [dictionary objectForKey:@"Name"];
    
    if ([array count]>1)
    {
    
        
        UILocalNotification *notif = [array objectAtIndex:0];
        
        NSString *result = [notif.userInfo valueForKey:@"Profile"];
    
        
        return [NSString stringWithFormat:@"For %@", result];
        
    }
    
    else
        
        
    {
        return NULL;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    NSDictionary *dictionary = [appDelegate.alert.NotificationList objectAtIndex:indexPath.section];
    NSArray *array = [dictionary objectForKey:@"Name"];
    
    
    UILocalNotification *notif = [array  objectAtIndex:indexPath.row];
    
    if ([[notif.userInfo valueForKey:@"Instructions"] isEqualToString:@" "]) {
        
        
        
        if ([[notif.userInfo valueForKey:@"Instructions"] isEqualToString:@" "]) {
            
            if ([[notif.userInfo valueForKey:@"Location"] isEqualToString:@" "]) {

                return 60;

            }
            else
            {


                return [self calculateTextHeight:[notif.userInfo valueForKey:@"Location"] width:240 fontSize:13]+65; //05-07-2017 changes "+65" insted of "+60"
            }
            
        }
        else
        {
         
            
            return [self calculateTextHeight:[notif.userInfo valueForKey:@"Instructions"] width:240 fontSize:13]+65; //05-07-2017 changes "+65" insted of "+60"
        }
    }
    
    else
    {
        return [self calculateTextHeight:[NSString stringWithFormat:@"%@\n%@",[notif.userInfo valueForKey:@"Location"], [notif.userInfo valueForKey:@"Instructions"]] width:240 fontSize:13]+65; //05-07-2017 changes "+65" insted of "+60"
    }
}


- (CGFloat)calculateTextHeight:(NSString*)text width:(CGFloat)nWidth fontSize:(CGFloat)nFontSize{
	
    /*CGSize constraint = CGSizeMake(nWidth, MAXFLOAT);
	CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:nFontSize] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
	CGFloat height = MAX(size.height, MIN_CELL_HEIGHT);
	return height+10;
     */
    
    //05-07-2017 change all comment code and write new below code
    
    NSString *cellText = [text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    UIFont *cellFont = [UIFont boldSystemFontOfSize:nFontSize];
    CGSize constraintSize = CGSizeMake(nWidth, MAXFLOAT);
    CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:CGSizeMake(240, constraintSize.height) lineBreakMode:NSLineBreakByWordWrapping];
    return labelSize.height;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *dictionary = [appDelegate.alert.NotificationList objectAtIndex:indexPath.section];
    NSArray *array = [dictionary objectForKey:@"Name"];
    
    
    UILocalNotification *notif = [array  objectAtIndex:indexPath.row];
    
    

    
   if ([appDelegate.selectedMeds containsObject:notif]) {
        
        [appDelegate.selectedMeds removeObject:notif];
    }
    else
        
    {
        [appDelegate.selectedMeds addObject:notif];
        
    }
    
  

    if ([WhatyouWant isEqualToString:@"take"]) {
           [appDelegate TakenPostoneUp:YES];
        
       [appDelegate.pickerTaal setMinimumDate:NULL];
        [appDelegate.pickerTaal setMaximumDate:NULL];
        [appDelegate.title setText:NSLocalizedString(@"Time taken",nil)];

        
        
        [table setFrame:CGRectMake(0, 50, 320, self.view.frame.size.height-54-240)];
    }
    else if ([WhatyouWant isEqualToString:@"SkipMore"]) {
        
        
        
    }
    else if ([WhatyouWant isEqualToString:@"Postpone"]) {
        
           NSDate *now = [[NSDate alloc] init];
           [appDelegate TakenPostoneUp:YES];
        [appDelegate.pickerTaal setMinimumDate:now];
        [appDelegate.pickerTaal setMaximumDate:NULL];
        [appDelegate.title setText:NSLocalizedString(@"Postpone To",nil)];

        
        [table setFrame:CGRectMake(0, 50, 320, self.view.frame.size.height-54-240)];
        
        
        //SkipIt
        
    }
    
 
    
    
    
   
        [table reloadData];
    
    

    
  
    
}

-(void)viewDidAppear:(BOOL)animated
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    ////////////////////////NSLog(@"%@", WhatyouWant);
    
     if ([WhatyouWant isEqualToString:@"take"]) {
         [titlelabel setText:NSLocalizedString(@"Select to Take",nil)];
     }
    else if ([WhatyouWant isEqualToString:@"SkipMore"]) {
         [titlelabel setText:NSLocalizedString(@"Select to Skip",nil)];
    }
    else if ([WhatyouWant isEqualToString:@"Postpone"]) {
        [titlelabel setText:NSLocalizedString(@"Select to Postpone",nil)];
    }
   
     [appDelegate.toolbarDown setAlpha:0];
   
    [self reloaddata];
    
}

- (AlertCellMore *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"CellIdentifier";
    AlertCellMore *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
	if (cell == nil) {
        cell = [[AlertCellMore alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    else
    {
    }
    //18-07-2017 add new line
//    cell.contentView.backgroundColor = [UIColor colorWithRed:218.0f/255.0f green:226.0f/255.0f blue:239.0f/255.0f alpha:1.000];
    //19-07-2017
    cell.contentView.backgroundColor = [UIColor whiteColor];
    
    
    return cell;
    
}

@end
