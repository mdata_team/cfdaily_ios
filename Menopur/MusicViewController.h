    //
    //  ViewController.h
    //  Menopur
    //
    //  Created by Jeffrey Snijder on 20-01-13.
    //  Copyright (c) 2013 Menopur. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class CodePopView, Monthview, TekstIDLabel;
@interface MusicViewController : UIViewController<UITextViewDelegate, UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate, MPMediaPickerControllerDelegate>

@property (nonatomic, retain) Monthview *monthView;




@property (nonatomic, retain) UILabel *NameLabel;
@property (nonatomic, retain) UIImageView *headShot;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) IBOutlet UITableView *table;
@property (nonatomic, retain) NSMutableArray *listofsongs;
@property (nonatomic, strong) IBOutlet TekstIDLabel *parantLabel;
@property (nonatomic, retain) UIView *ProfileView;
@property (nonatomic, retain) NSString *chosenID;
- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)setItems:(NSMutableDictionary*)set;
-(void)chosenMonth:(NSString*)sender;
-(void)PresentView:(UIButton*)sender;
- (NSString *)newUUID;
-(void)whatLabel:(UILabel*)set;
@end
