    //
    //  ViewController.m
    //  Menopur
    //
    //  Created by Jeffrey Snijder on 20-01-13.
    //  Copyright (c) 2013 Menopur. All rights reserved.
    //

#import "MusicViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "NewProfileController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "SaveData.h"
#import "MedCell.h"
#import "Monthview.h"
#import "NewMedecineController.h"
#import "RecordingAct.h"
#import "PlayCell.h"
#import "TekstIDLabel.h"


@interface MusicViewController ()

@end

@implementation MusicViewController



@synthesize NameLabel;
@synthesize headShot;
@synthesize Vraagantwoord;
@synthesize table;
@synthesize listofsongs;
@synthesize chosenID;
@synthesize ProfileView;
@synthesize monthView;
@synthesize parantLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];

//    [self googleAnalyticsGroupping];


    Vraagantwoord =[[NSMutableArray alloc]init];

    [self loadSongsFromDocuments];


    self.navigationItem.title = NSLocalizedString(@"Select Alert", nil)
    ;


 



//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:14];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            label.numberOfLines =2;
              [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.navigationItem.title;
            [label sizeToFit];
    
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;




 
  
    // this will appear as the title in the navigation bar
  

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


    table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0, 320, self.view.bounds.size.height-44)];
    table.separatorColor = [UIColor grayColor];
    table.backgroundColor = [UIColor clearColor];
    table.rowHeight =60;

    table.delegate = self;
    table.dataSource = self;

    //[table setEditing:YES animated:YES];
    //[table setEditing:YES];

    [table setEditing:NO];
    [self.view addSubview:table];
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        //NSLog(@"left");
        
        self.view.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        for (UIView *content in self.view.subviews) {
            
            
            for (UILabel *label in content.subviews) {
                
                
                if ([label isKindOfClass:[UILabel class]]) {
                    
                    
                    label.transform = CGAffineTransformMakeScale(-1, 1);
                    [label setTextAlignment:NSTextAlignmentNatural];
                    
                }
                
            }
            
            for (UIButton *label in content.subviews) {
                
                
                if ([label isKindOfClass:[UIButton class]]) {
                    
                    label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                    [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                    
                }
                
            }
            
            
            
            
        }
        
        
    }
    
    else
    {
        
    }

}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
        {


        [SaveData RemovetMusic:[[Vraagantwoord valueForKey:@"ID"] objectAtIndex:indexPath.row]];

        [Vraagantwoord removeObjectAtIndex:indexPath.row];


        [table reloadData];




        } else if (editingStyle == UITableViewCellEditingStyleInsert)
            {



            }


}


-(void)whatLabel:(UILabel*)set

{
    parantLabel =(TekstIDLabel*)set;
    ////////////////////////////////////////////////////////////////////////////NSLog(@"TekstIDLabel %@", set);
    
    
}

- (void)loadSongsFromDocuments {

    
    Vraagantwoord =[GetData getMusic];
    
    
    if ([Vraagantwoord count]==0) {
        
   
   listofsongs=[[NSMutableArray alloc] init];

        
    }
    else
    {

    }

    
    [table reloadData];


}



-(void)imagePickerControllerDidCancel:(UIImagePickerController *) picker
{
	[picker dismissViewControllerAnimated:YES  completion:nil];

}

-(void) mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection
{

    MPMediaItem *anItem = (MPMediaItem *)[mediaItemCollection.items objectAtIndex:0];

    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
    [set setObject:[anItem valueForProperty: MPMediaItemPropertyTitle] forKey:@"title"];
    [set setObject:[anItem valueForProperty: MPMediaItemPropertyAssetURL] forKey:@"url"];
    [set setObject:[self newUUID] forKey:@"ID"];


    if ([[Vraagantwoord valueForKey:@"title"] containsObject:[anItem valueForProperty: MPMediaItemPropertyTitle]]) {

    }
    else
        {
    
            
            [Vraagantwoord addObject:set];

        }


    [SaveData insertMusic:set];


    [self dismissViewControllerAnimated:YES completion:NULL];

    [table reloadData];
}

- (void) mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}




- (IBAction)showMediaPicker
{
    
    
 
    
    NSFileManager *FileManager = [NSFileManager defaultManager];
    
    
    NSString *dbPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"honk.mp3"];
    NSString *dbPath2 = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Writeable/honk49.mp3"];
    //Copy the database file to the users document directory.
    BOOL success = [FileManager copyItemAtPath:dbPath toPath:dbPath2 error:NULL];
    
    if (success) {
     
    }
    else
    {
     
    }
    
    
    
    
    //Copy the database file to the users document directory.
  
 
    
    [RecordingAct startRecordingCopy:@"Copy.pcm"];
    
  
}


-(void)viewanimate

{


    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];




    if (monthView.frame.size.height == 22) {
        [monthView setFrame:CGRectMake(200, 78,107,22*13)];
        [monthView.table setFrame:CGRectMake(0.0, 22, 320, 22*13)];

    }
    else
        {
        [monthView setFrame:CGRectMake(200, 78,107,22)];
        [monthView.table setFrame:CGRectMake(0.0, 22, 320, 22)];

        }

    [UIView commitAnimations];

}



-(void)chosenMonth:(NSString*)sender

{




       

}

-(void) backAction
{


    [[self navigationController] popViewControllerAnimated:YES];
}


-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{


        

    if (index==1) {


        NewProfileController *controller = [[NewProfileController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];

    }

    else
        {




        }
}

-(void) CreateAccount

{
    NewMedecineController *controller = [[NewMedecineController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];

}


-(void)setItems:(NSMutableDictionary*)set

{



    chosenID = [set valueForKey:@"ID"];

    UIColor *colorit = [UIColor colorWithRed:[[set valueForKey:@"ColorRed"] floatValue] green:[[set valueForKey:@"ColorGreen"]  floatValue] blue:[[set valueForKey:@"Colorblue"]  floatValue] alpha:[[set valueForKey:@"Coloralpha"]  floatValue]];




    [ProfileView setBackgroundColor:colorit];
    [NameLabel setText:[set valueForKey:@"Name"]];
    [headShot setImage:[set valueForKey:@"Image"]];


    [table reloadData];

}

- (NSString *)newUUID
{
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
    NSString *result = [[NSString alloc] initWithFormat:@"%@", [(NSString *)CFBridgingRelease(uuidStr) lowercaseString]];
    return result;
}




-(void)PresentView:(ActionArrow*)sender
{
    //11-07-2017 Add new Code For Play Audio
    //************
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:table];
    NSIndexPath *indexPath = [table indexPathForRowAtPoint:buttonPosition];

//    NSLog(@"%@",indexPath);
//    
//    NSLog(@"Vraagantwoord : %@",Vraagantwoord);
 
    NSString *Soundname = [[[[[[Vraagantwoord objectAtIndex:indexPath.row] valueForKey:@"url"] componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."] firstObject];
    //************
    
    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDir = [paths2 objectAtIndex:0];
    
    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Caches/%@.mp3", documentsDir, Soundname]];
    
        [RecordingAct playRecording:url];
    [table reloadData];
   
   

    
 
}

- (IBAction)instructie:(UIButton *)sender {

}

- (IBAction)Vragen:(UIButton *)sender {




        //VragenViewController

}


- (IBAction)bijsluiter:(UIButton *)sender {





}

- (IBAction)about:(UIButton *)sender {



}


- (IBAction)Cancel:(UIButton *)sender {


}

- (IBAction)OK:(UIButton *)sender {




}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{


    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{

}
- (void)textViewDidEndEditing:(UITextView *)textView
{

}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{

}


- (void)textViewDidChangeSelection:(UITextView *)textView
{

}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [Vraagantwoord count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(PlayCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {



    [cell FillAllItems:[[Vraagantwoord valueForKey:@"title"] objectAtIndex:indexPath.row] index:indexPath.row];

    [cell.title setText:[[Vraagantwoord valueForKey:@"title"] objectAtIndex:indexPath.row]];

    [cell getparent:self];



        //play.png




}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{







    return 60;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{


    




}


- (PlayCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {




    NSString *identifier = @"CellIdentifier";
    PlayCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

	if (cell == nil) {

        cell = [[PlayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    }

    else

        {

        }
    


    return cell;


}





-(void)select:(ActionArrow*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [parantLabel setText:sender.titleit];
    
    ////////////////////////////NSLog(@"%@",  appDelegate.currentProfile);
    
  appDelegate.currentProfile.audio = sender.titleit;
   [table reloadData];
   
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{
       
}

//#pragma mark - Google Analytics
//
//-(void)googleAnalyticsGroupping
//{
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:1]value:@"Add Profile Sound"];
//}

@end
