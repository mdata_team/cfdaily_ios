//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "NameItemViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewProfileController.h"
#import "NewMedecineController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
@interface NameItemViewController ()

@end

@implementation NameItemViewController
@synthesize parantIt;

@synthesize TitleText;
@synthesize parantLabel;


-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    
    //////////////////////////////////////////////////////NSLog(@"|%@|", set.text);
    
    [TitleText setText:set.text];
    
 

    parantLabel =(TekstIDLabel*)set;
    
}



- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];



    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];



//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
   



                Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 44)];
                [Combi setBackgroundColor:[UIColor whiteColor]];
                [Combi.layer setCornerRadius:10];
                [Combi.layer setBorderWidth:1.5];
//                [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
                [Combi.layer setMasksToBounds:YES];
                [self.view addSubview:Combi];



    
    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 3, self.view.frame.size.width-40, 35)];
    [TitleText setBackgroundColor:[UIColor whiteColor]];
    [TitleText setDelegate:self];
    //                [TitleText setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]];//28-06-2017 changes
    [TitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [TitleText setTag:123+1];
    [TitleText setEditable:NO];
    [TitleText setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    [Combi addSubview:TitleText];
    
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        NSLog(@"left");
        Combi.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        TitleText.transform = CGAffineTransformMakeScale(-1, 1);
        [TitleText setTextAlignment:NSTextAlignmentNatural];
        
        
        
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }

    
    
    NewMedecineController *oldscreen =(NewMedecineController *)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
    
    ////////////////////////////////////////////////////////////////////NSLog(@"%@", oldscreen.ChosenMedicen);
    
    [TitleText setText:[oldscreen.ChosenMedicen valueForKey:@"Name"]];


}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    [TitleText setKeyboardType:UIKeyboardTypeDefault];
   
    TitleText.selectedRange = NSMakeRange([TitleText.text length], 0);
    [TitleText setEditable:YES];
    [TitleText becomeFirstResponder];
}

-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
        [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

    



        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
                UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [TitleText setText:text];

    });

}

-(void)textViewDidChange:(UITextView *)textView
{
    
   
 
 
}

-(void) textViewDidBeginEditing:(UITextView *)textView
{
    

}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {


	return YES;
}

-(void)hideAndseek:(NSString*)set
{

}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
  
   if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}


-(void)getParant:(UIViewController*)parant

{

    parantIt =(NewMedecineController*)parant;
    
}

-(void)Save:(UIBarButtonItem*) sender

{


    [[self navigationController] popViewControllerAnimated:YES];

    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [parantLabel setText:TitleText.text];
        [parantIt setitemsDictionary:TitleText.text name:@"Name"];
        
        
        
        
        
    });
    
}

- (IBAction)doSomething:(UIButton *)sender {



    [[self navigationController] popViewControllerAnimated:YES];
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSRange range = NSMakeRange (0, 1);
        
        if (![TitleText.text isEqualToString:@""]) //26-06-2017 add condition for app crash when textvalue "" and its find range
          {
            if ([[TitleText.text substringWithRange:range] isEqualToString:@" "])
            {
                NSString *str = TitleText.text;
                [TitleText setText:[str substringWithRange:NSMakeRange(1, [str length] - 1)]];
            }
        }
        
        [parantLabel setText:TitleText.text];
        [parantIt setitemsDictionary:TitleText.text name:@"Name"];
        
    });


}

- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
