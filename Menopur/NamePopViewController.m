//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "NamePopViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"

#import "FrequencyViewControllerNew.h"
@interface NamePopViewController ()

@end

@implementation NamePopViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;

@synthesize theTime;
@synthesize theDay;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize underTitleText;



-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(TekstIDLabel*)set

{
    
    
}



- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];


      TimeChoses =[[NSMutableArray alloc]init];


    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];

    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Cancel:)];
   
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Cancel setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }



    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];


    UIView *textit  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 160, 20)];
    [title setTextColor:[UIColor whiteColor]];
    [title setText:@ "Medicine Name"];
    [title setFont:[UIFont boldSystemFontOfSize:15]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];
    [textit addSubview:textit];


    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:textit];




    UIBarButtonItem *Save = [[UIBarButtonItem alloc]
                             initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleBordered target:self
                             action:@selector(Save:)];
   
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Save setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }



    UIToolbar *toolbarUp = [[UIToolbar alloc] init];
    toolbarUp.frame = CGRectMake(0, -2, 320, 54);
   
   
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        
        
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk"] forToolbarPosition:0 barMetrics:0];
        
    }
    
    else
    {
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk.png"] forToolbarPosition:0 barMetrics:0];
    }
    [toolbarUp setTranslucent:NO];
   [self.view addSubview:toolbarUp];


    NSArray *itemsUp = [NSArray arrayWithObjects:Cancel,flexItem,random,flexItem,Save,nil];
    toolbarUp.items = itemsUp;


    

    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];


    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 64, self.view.frame.size.width-20, 44)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];



    underTitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 68, self.view.frame.size.width-40, 35)];
    [underTitleText setBackgroundColor:[UIColor whiteColor]];
    [underTitleText setDelegate:self];
    [underTitleText setText:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Medicine Name",nil)]];
    [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];

    [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
    [self.view addSubview:underTitleText];


    

    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 68, self.view.frame.size.width-40, 35)];
    [TitleText setBackgroundColor:[UIColor clearColor]];
    [TitleText setDelegate:self];
    [TitleText setText:@""];
    [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    [TitleText setTag:123+1];

   
    [TitleText setFont:[UIFont boldSystemFontOfSize:15]];
    [self.view addSubview:TitleText];



    NSDate *now = [[NSDate alloc] init];

     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     
    [dateFormatter setDateFormat:@"EEEE"];
    
    theDay = [dateFormatter stringFromDate:now];


   


    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [timeFormat setDateFormat:@"hh:mm"];
    theTime = [timeFormat stringFromDate:now];

   


    NSDateFormatter *format2 = [[NSDateFormatter alloc] init];
    [format2 setDateFormat:@"a"];
    timeframe = [format2 stringFromDate:now];

   
}

-(void)Cancel:(UIBarButtonItem*) sender

{


    

    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];


    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //[parantLabel setText:TitleText.text];
            //[parantIt setitemsDictionary:TitleText.text name:[self.navigationItem.title stringByReplacingOccurrencesOfString:@"" withString:@""]];
        
        
        
        
        
    });
    
}

-(void)setChoice:(NSString*)gothere

{
    
}

-(void)Save:(UIBarButtonItem*) sender

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
     [set setObject:@"Hours" forKey:@"choice"];
    [set setObject:[appDelegate newUUID] forKey:@"ID"];
    [set setObject:timeframe forKey:@"Moment"];
    [set setObject:theTime forKey:@"Time"];
    [set setObject:theDay forKey:@"CurrentDay"];
    [set setObject:TitleText.text forKey:@"intervalOurs"];
     [set setObject:@"0" forKey:@"intervalDays"];
    [set setObject:theTime forKey:@"CurrentTime"];
    [set setObject:@"" forKey:@"Day"];



    [TimeChoses addObject:set];


        //[parantIt getchoice:TimeChoses];
    
    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];


}

-(void)change:(UIButton*)sender

{

    if (sender.tag ==140 ||sender.tag ==180) {


    }
    if (sender.tag ==141 ||sender.tag ==181) {

    }
    if (sender.tag ==142 ||sender.tag ==182) {


        TimesofDayViewController *controller = [[TimesofDayViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (sender.tag ==143 ||sender.tag ==183) {

    }
    if (sender.tag ==144 ||sender.tag ==184) {

    }
    
   
  
  

    
}

-(void)Action:(UIButton*)sender

{

  
}

-(void)changeChose:(NSString*)sender

{


    chosen = sender;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    [TitleText setKeyboardType:UIKeyboardTypeDefault];
   
    TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
    [TitleText becomeFirstResponder];
}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void)textViewDidChange:(UITextView *)textView
{
  


    if ([textView.text length]<4) {


        if ([textView.text length] ==0) {

            [underTitleText setText:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Quantity Per Filling",nil)]];
        }
        if ([textView.text length] ==1) {

            [underTitleText setText:[NSString stringWithFormat:@"    %@", NSLocalizedString(@"Quantity Per Filling",nil)]];
        }
        if ([textView.text length] ==2) {

            [underTitleText setText:[NSString stringWithFormat:@"      %@", NSLocalizedString(@"Quantity Per Filling",nil)]];
        }
        if ([textView.text length] ==3) {

            [underTitleText setText:[NSString stringWithFormat:@"      %@", NSLocalizedString(@"Quantity Per Filling",nil)]];
        }


    }
    else
        {
        [textView setText:@""];
        [underTitleText setText:@"   Quantity Per Filling"];
        }
    

    




    

}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{


}

-(void)getParant:(UIViewController*)parant

{

    parantIt =parant;




  
    
}




- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
