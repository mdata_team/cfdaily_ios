//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "NameViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CodePopView.h"
#import "NewProfileController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
@interface NameViewController ()

@end

@implementation NameViewController
@synthesize parantIt;
@synthesize TitleText;


-(void)loadView{


    [super loadView];
}



- (void)viewDidLoad {


    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"DiapDown.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    [barButtonItem setTintColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    self.navigationItem.rightBarButtonItem = barButtonItem;
  

    self.navigationItem.title = @"Name";
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000];


    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
   


    NSArray *names1 =[NSArray arrayWithObjects:@"   Med Name:", @"   Strength:", @"   Med Type:", @"   Assign to Profile:", @"   Schedule:", @"   Frequency:", @"   Med Quantity Remaining:", @"   Med Quantity per Filling:",@"   Alert Quantity for Refilling:", @"   Doctor:", @"   Pharmacy Prescription:",@"   Notes:", nil];


        for (int i=0 ; i<[names1 count]; i++) {


            if ([self.navigationItem.title isEqualToString:[names1 objectAtIndex:i]]) {

                NSLog(@"%@", [names1 objectAtIndex:i]);
            }
            else
                {



                TitleText = [[UITextView alloc] initWithFrame:CGRectMake(10, 28, 300, 190)];
                [TitleText setBackgroundColor:[UIColor colorWithRed:0.808 green:0.847 blue:0.898 alpha:0.8]];
                [TitleText setDelegate:self];
                [TitleText setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]];
                [TitleText setTag:123+1];
                [TitleText setEditable:NO];
                TitleText.layer.cornerRadius = 8.0f;
                TitleText.layer.cornerRadius = 8.0f;
                TitleText.layer.masksToBounds = YES;
                TitleText.layer.borderWidth = 2.0f;
                TitleText.layer.borderColor = [UIColor colorWithRed:0.808 green:0.847 blue:0.898 alpha:0.5].CGColor;
                [TitleText setFont:[UIFont fontWithName:@"Helvetica" size:16]];
                [self.view addSubview:TitleText];
                
                }
        }



}

-(void) viewDidAppear:(BOOL)animated
{


}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

    



        self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [a1 setTag:44];
        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
        [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            ////////////NSLog(@"%@", text);
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void) textViewDidBeginEditing:(UITextView *)textView
{


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)getParant:(NewProfileController*)parant

{

    parantIt =parant;
    
}

- (IBAction)doSomething:(UIButton *)sender {



   
    [[self navigationController] popViewControllerAnimated:YES];

double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
     [parantIt.Nameit setText:TitleText.text];
});

}

- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
