//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeveyPopColorView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "BufferedNavigationController.h"
#import "MedicineViewController.h"
#import "SSPhotoCropperViewController.h"
#import "SSPhotoCropperViewController.h"
#import "SchedularView.h"

@protocol subViewDelegate, SchedularView;
@class CodePopView, LeveyPopColorView, ViewController, MedicineViewController, MedChoiceView;
@interface NewMedecineController2 : UIViewController<UITextViewDelegate, UIAlertViewDelegate, MPMediaPickerControllerDelegate, SSPhotoCropperDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property (nonatomic, retain) NSMutableDictionary *ChosenMedicen;
@property (nonatomic, retain) ViewController *parantIt;
@property (nonatomic, retain) MedicineViewController *parantGo;
@property (nonatomic, retain) UIImageView *headShot;
@property (nonatomic, retain) NSMutableDictionary *MedicineSpecs;
@property (nonatomic, retain) MPMusicPlayerController *musicPlayer;
@property (nonatomic, retain) NSMutableArray *selectedNotifictions;
@property (nonatomic, assign) CGFloat rood;
@property (nonatomic, assign) CGFloat groen;
@property (nonatomic, assign) CGFloat blauw;
@property (nonatomic, assign) CGFloat doorzicht;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollViewSpread;
@property (nonatomic, strong) IBOutlet UIView *Combi;
@property (nonatomic, strong) SchedularView *Schdules;
@property (nonatomic, strong) IBOutlet UIView *Quantity;
@property (nonatomic, strong) IBOutlet UIView *Meds;
@property (nonatomic, strong) IBOutlet UIView *Contacts;
@property (nonatomic, strong) IBOutlet UIView *Alerts;
@property (nonatomic, strong) IBOutlet UIView *Dose;

@property (nonatomic, retain) MedChoiceView *medChoise;

@property (nonatomic, retain) LeveyPopColorView *color;
@property (nonatomic, retain) UILabel *Nameit;
@property (nonatomic, retain) UILabel *Sound;
@property (nonatomic, retain) UILabel *ColorField;
@property (nonatomic, retain) NSString *chosenID;
@property (nonatomic, retain) IBOutlet UIButton *Save;
@property (nonatomic, retain) IBOutlet UIButton *Cancel;
@property (nonatomic, retain) IBOutlet UIButton *Delete;
@property (nonatomic, retain) NSString *photchoice;
- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)setItems:(NSMutableDictionary*)set;
-(void)setNew:(NSString*)set;
-(void)getParant:(ViewController*)parant;
-(void)editOrShow:(NSString*)sender;
-(void)getParantGo:(UIViewController*)parant;
-(void)plaese:(NSString*)set;
-(void)photCropper:(UIImage*)image;
-(void)hideAndseek:(NSString*)set;
-(void)setitemsDictionary:(NSString*)set name:(NSString*)name;
-(void)setItemsChangeID:(NSMutableDictionary*)set;
-(void) photoCropperDidCancel:(SSPhotoCropperViewController *)photoCropper;
- (void) photoCropper:(SSPhotoCropperViewController *)photoCropper
         didCropPhoto:(UIImage *)photo;
-(void)DynamicCell:(NSInteger)difference;

    
@end

@protocol subViewDelegate
-(void)somethingHappened:(id)sender;
@end