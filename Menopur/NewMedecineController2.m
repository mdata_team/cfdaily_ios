//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "NewMedecineController2.h"
#import "MedsProfileViewController.h"
#import "MedicineViewController.h"
#import "StrengthViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AlertViewController.h"
#import "NameItemViewController.h"
#import "ProfileViewController.h"
#import "SaveData.h"
#import "TekstIDLabel.h"
#import "DocterViewController.h"
#import "AppDelegate.h"
#import "TypeViewController.h"
#import "PerscriptionViewController.h"
#import "FrequencyViewController.h"
#import "RemainingViewController.h"
#import "DoseViewController.h"
#import "FillingViewController.h"
#import "NoteViewController.h"
#import "MedsViewController.h"
#import "Notification.h"
#import "RecordingViewController.h"
#import "GetData.h"
#import "DurationViewController.h"
#import "MedChoiceView.h"
#import "SSPhotoCropperViewController.h"
#import "SchedularView.h"
#import "TimersTableView.h"
@interface NewMedecineController2 ()

@end

@implementation NewMedecineController2

@synthesize color;
@synthesize rood;
@synthesize Alerts;
@synthesize groen;
@synthesize blauw;
@synthesize doorzicht;
@synthesize musicPlayer;
@synthesize headShot;
@synthesize Nameit;
@synthesize Sound;
@synthesize ColorField;
@synthesize chosenID;
@synthesize parantIt;
@synthesize MedicineSpecs;
@synthesize ChosenMedicen;
@synthesize selectedNotifictions;
@synthesize parantGo;
@synthesize Save;
@synthesize Cancel;
@synthesize Delete;
@synthesize scrollViewSpread;
@synthesize medChoise;
@synthesize photchoice;
@synthesize Combi;
@synthesize Quantity;
@synthesize Meds;
@synthesize Contacts;
@synthesize Schdules;
@synthesize Dose;


-(void) backAction
{

    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Save medicine?"];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464749];
    [alert addButtonWithTitle:@"Save"];
    [alert addButtonWithTitle:@"Cancel"];
    [alert show];


}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{
    
    
    ////////////////////////////////////NSLog(@"%i", index);
    
    if (alertView.tag ==50000) {
        
         if (index==1) {
            
              
         }
        
        else
            
        {
            [self photCropper:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"Image"]]];
        }
        
        
        
    }
    else
    {
    if (index==1) {
      
        
        [[self navigationController] popViewControllerAnimated:YES];
  
     
        
    }
    
    else
    {
        
    [self save:NULL];
 
        
        
    }
        
        
    }
}


-(void)photCropper:(UIImage*)image

{
    
   
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    
    SSPhotoCropperViewController *photoCropper =
    [[SSPhotoCropperViewController alloc] initWithPhoto:image
                                               delegate:self
                                                 uiMode:SSPCUIModePresentedAsModalViewController
                                        showsInfoButton:YES];
    [photoCropper setMinZoomScale:0.25f];
    [photoCropper setMaxZoomScale:3];
    

    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:photoCropper];
    [self presentViewController:nc animated:YES completion:NULL];
 
}


-(void)hideAndseek:(NSString*)set

{
    
}
-(void)somethingHappened:(id)sender

{
    
}

- (void)viewDidLoad
{




    [self.view setBackgroundColor:[UIColor whiteColor]];



   selectedNotifictions  = [[NSMutableArray alloc]init];

        //Month_view.png



    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



    musicPlayer = [MPMusicPlayerController iPodMusicPlayer];



	[musicPlayer beginGeneratingPlaybackNotifications];


    scrollViewSpread = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-88)];
    [scrollViewSpread setCanCancelContentTouches:NO];
    scrollViewSpread.showsHorizontalScrollIndicator = YES;
    [scrollViewSpread setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


    scrollViewSpread.delaysContentTouches=YES;
    scrollViewSpread.clipsToBounds = YES;
    scrollViewSpread.scrollEnabled = YES;
    scrollViewSpread.pagingEnabled = NO;
    [scrollViewSpread setDelegate:self];
    scrollViewSpread.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.view  addSubview:scrollViewSpread];


    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 220)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    Combi.layer.shadowOffset = CGSizeMake(3, 0);
    Combi.layer.shadowOpacity = 2;
    Combi.layer.shadowRadius = 2.0;
    [scrollViewSpread addSubview:Combi];
    
    
    Dose = [[UIView alloc] initWithFrame:CGRectMake(10, 290, self.view.frame.size.width-20, 79)];
    [Dose setBackgroundColor:[UIColor whiteColor]];
    [Dose.layer setCornerRadius:10];
    [Dose.layer setBorderWidth:1.5];
    [Dose.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Dose.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Dose];
    
    
   
    Schdules = [[SchedularView alloc] initWithFrame:CGRectMake(10, 380, self.view.frame.size.width-20, 79+39+39+39+39)];
    [Schdules setBackgroundColor:[UIColor whiteColor]];
    [Schdules.layer setCornerRadius:10];
    [Schdules.layer setBorderWidth:1.5];
    [Schdules.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Schdules.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Schdules];
    [Schdules setShedulare:self];
    
    
    
    Alerts = [[UIView alloc] initWithFrame:CGRectMake(10, 509, self.view.frame.size.width-20, 79)];
    [Alerts setBackgroundColor:[UIColor whiteColor]];
    [Alerts.layer setCornerRadius:10];
    [Alerts.layer setBorderWidth:1.5];
    [Alerts.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Alerts.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Alerts];
    
    
    Quantity = [[UIView alloc] initWithFrame:CGRectMake(10, 609, self.view.frame.size.width-20, 128+39)];
    [Quantity setBackgroundColor:[UIColor whiteColor]];
    [Quantity.layer setCornerRadius:10];
    [Quantity.layer setBorderWidth:1.5];
    [Quantity.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Quantity.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Quantity];
    
    
    
    Contacts = [[UIView alloc] initWithFrame:CGRectMake(10, 789, self.view.frame.size.width-20, 118)];
    [Contacts setBackgroundColor:[UIColor whiteColor]];
    [Contacts.layer setCornerRadius:10];
    [Contacts.layer setBorderWidth:1.5];
    [Contacts.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Contacts.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Contacts];
    
 
    
     /*
    

    
  
    
      
      Meds = [[UIView alloc] initWithFrame:CGRectMake(10, 610+39, self.view.frame.size.width-20, 118)];
      [Meds setBackgroundColor:[UIColor whiteColor]];
      [Meds.layer setCornerRadius:10];
      [Meds.layer setBorderWidth:1.5];
      [Meds.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
      [Meds.layer setMasksToBounds:YES];
      [scrollViewSpread addSubview:Meds];
   
    
   
    
    */

    NSArray *names1 =[NSArray arrayWithObjects:@"   Medicine Name:", @"   Medicine Type:", @"   Strength:", nil];
    NSArray *Cell1 =[NSArray arrayWithObjects:@"Name#88", @"Type#82", @"Strength#76",nil];

    for (int i=0 ; i<[names1 count]; i++) {


        NSArray *numbers = [[Cell1 objectAtIndex:i] componentsSeparatedByString:@"#"];


        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names1 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Combi addSubview:MedName];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-60, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
          [Name setTextAlignment:NSTextAlignmentRight];
        [Name setTitle:[numbers objectAtIndex:0]];
        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Combi addSubview:Name];

        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:[numbers objectAtIndex:0] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Combi addSubview:NameButton];





    }

    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir, @"Medhot.png"];
    
    
    //[data writeToFile:jpegFilePath atomically:YES];
    
  
    headShot = [[UIImageView alloc] initWithFrame:CGRectMake(10, 130, 80, 80)];
    [headShot setBackgroundColor:[UIColor blackColor]];
    [headShot setTag:130];
    [headShot setImage:[UIImage imageWithContentsOfFile:jpegFilePath]];
        //[headShot setImage:[UIImage imageNamed:@"headshot.png"]];
    [Combi addSubview:headShot];



    UIButton *Photocover = [[UIButton alloc]  initWithFrame:CGRectMake(10, 130, 80, 80)];
    [Photocover setTitle:@"Photo" forState:UIControlStateNormal];
    [Photocover setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];

    [Photocover setTag:160];
    [Photocover addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:Photocover];




    UIButton *Photo = [[UIButton alloc]  initWithFrame:CGRectMake(100, 180, 39, 33)];
    [Photo setImage:[UIImage imageNamed:@"photo.png"] forState:UIControlStateNormal];
    [Photo setTitle:@"Photo" forState:UIControlStateNormal];
    [Photo setTag:160];
    [Photo addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:Photo];



    UIView *Assign = [[UIView alloc] initWithFrame:CGRectMake(10, 240, self.view.frame.size.width-20, 40)];
    [Assign setBackgroundColor:[UIColor whiteColor]];
    [Assign.layer setCornerRadius:10];
    [Assign.layer setBorderWidth:1.5];
    [Assign.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Assign.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Assign];


    NSArray *names2 =[NSArray arrayWithObjects:@"   Assign to Profile:", nil];
    NSArray *Cell2 =[NSArray arrayWithObjects:@"Profile#130", nil];

    for (int i=0 ; i<[names2 count]; i++) {

        NSArray *numbers = [[Cell2 objectAtIndex:i] componentsSeparatedByString:@"#"];


        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names2 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Assign addSubview:MedName];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-60, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
          [Name setTextAlignment:NSTextAlignmentRight];
        [Name setTitle:[numbers objectAtIndex:0]];

        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Assign addSubview:Name];


        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:[numbers objectAtIndex:0] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Assign addSubview:NameButton];



    }



    NSArray *names4 =[NSArray arrayWithObjects:@"   Alert Recording:",@"   Alert Message Text:", nil];
    
    NSArray *Cell4 =[NSArray arrayWithObjects:@"Recording#140", @"Message#100", nil];
    
 
    for (int i=0 ; i<[names4 count]; i++) {
        
        
        NSArray *numbers = [[Cell4 objectAtIndex:i] componentsSeparatedByString:@"#"];
        
        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names4 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Alerts addSubview:MedName];
        
        
        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-60, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTextAlignment:NSTextAlignmentRight];
        [Name setTitle:[numbers objectAtIndex:0]];
        
        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Alerts addSubview:Name];
        
        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:[numbers objectAtIndex:0] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Alerts addSubview:NameButton];
        
        
    }

    
    
    
    
    
    NSArray *names8 =[NSArray arrayWithObjects:@"   Quantity per Dose:",@"   Instructions:", nil];
    
    NSArray *Cell8 =[NSArray arrayWithObjects:@"Quantity#140", @"Instructions#100", nil];

 
    for (int i=0 ; i<[names8 count]; i++) {


        NSArray *numbers = [[Cell8 objectAtIndex:i] componentsSeparatedByString:@"#"];

        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names8 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Dose addSubview:MedName];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-60, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
          [Name setTextAlignment:NSTextAlignmentRight];
        [Name setTitle:[numbers objectAtIndex:0]];

        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Dose addSubview:Name];

        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:[numbers objectAtIndex:0] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Dose addSubview:NameButton];


    }







    NSArray *names5 =[NSArray arrayWithObjects:@"   Quantity Remaining:", @"   Quantity per Filling:",@"   Minimum Quantity\n   for Refilling:",@"   Alert Quantity for Refilling:", nil];

   NSArray *Cell5 =[NSArray arrayWithObjects:@"Remaining#182", @"Filling#180", @"Minimum#180", @"Refilling#202", nil];
    
    for (int i=0 ; i<[names5 count]; i++) {

        NSArray *numbers = [[Cell5 objectAtIndex:i] componentsSeparatedByString:@"#"];

          if (i<2) {
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names5 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Quantity addSubview:MedName];
              
          }
        if (i==2) {
            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, (39*i), self.view.frame.size.width-20, 50)];
            [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:14]];
            [MedName setText:[names5 objectAtIndex:i]];
            [MedName setTag:120];
            [MedName.layer setBorderWidth:1];
            [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Quantity addSubview:MedName];
            
        }
          if (i==3) {
              TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, (39*i)+10, self.view.frame.size.width-20, 40)];
              [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
              [MedName setFont:[UIFont boldSystemFontOfSize:14]];
              [MedName setText:[names5 objectAtIndex:i]];
              [MedName setTag:120];
              [MedName.layer setBorderWidth:1];
              [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
              [MedName setBackgroundColor:[UIColor clearColor]];
              [MedName setNumberOfLines:3];
              [Quantity addSubview:MedName];
              
          }

        if (i<3) {


            TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-60, 40)];
            [Name setTextColor:[UIColor blackColor]];
            [Name setFont:[UIFont systemFontOfSize:14]];
            [Name setText:@""];
            [Name setTextAlignment:NSTextAlignmentRight];
            [Name setTitle:[numbers objectAtIndex:0]];

            [Name setTag:120];
            [Name setBackgroundColor:[UIColor clearColor]];
            [Name setNumberOfLines:3];
            [Quantity addSubview:Name];

          

            UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
            [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
            [NameButton setTitle:[numbers objectAtIndex:0] forState:UIControlStateNormal];
            [NameButton setTag:140];
            [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
            [Quantity addSubview:NameButton];

        }
        else
            {

            
            UISwitch *swActive = [[UISwitch alloc] initWithFrame:CGRectMake(215, 16+(39*i), 100,50)];
            [swActive setTag:100+i];
                //sukker ben ik!!
            [swActive setOnTintColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000]];
            [swActive addTarget:self action:@selector(switchValueReminder:) forControlEvents:UIControlEventValueChanged];
            [Quantity addSubview:swActive];



            }


      


    }



    NSArray *names6 =[NSArray arrayWithObjects:@"   Doctor:", @"   Pharmacy Prescription:",@"   Notes:", nil];

       NSArray *Cell6 =[NSArray arrayWithObjects:@"Doctor#65", @"Prescription#172", @"Notes#60", nil];

    for (int i=0 ; i<[names6 count]; i++) {


        NSArray *numbers = [[Cell6 objectAtIndex:i] componentsSeparatedByString:@"#"];

        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names6 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Contacts addSubview:MedName];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-60, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
          [Name setTextAlignment:NSTextAlignmentRight];
        [Name setTitle:[numbers objectAtIndex:0]];

        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Contacts addSubview:Name];

        
        if ([[numbers objectAtIndex:0] isEqualToString:@"Prescription"]||[[numbers objectAtIndex:0] isEqualToString:@"Notes"]) {

             [Name setNumberOfLines:1];
          
        }

        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:[numbers objectAtIndex:0] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Contacts addSubview:NameButton];
        
        
    }
    
    [scrollViewSpread setContentSize:CGSizeMake(320, 920+39)];
    
        //142 × 36 pixels
    

    Save = [[UIButton alloc] initWithFrame:CGRectMake(10, 770+39, 142/1.1,  36/1.1)];
    [Save setBackgroundColor:[UIColor clearColor]];
    [Save setShowsTouchWhenHighlighted:NO];
    [Save setImage:[UIImage imageNamed:@"save.png"] forState:UIControlStateNormal];
    [Save setAutoresizesSubviews:YES];
    [Save addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:Save];
    [Save setAlpha:0];
    


        //142 × 36 pixels

    Cancel = [[UIButton alloc] initWithFrame:CGRectMake(180,770+39, 142/1.1,  36/1.1)];
    [Cancel setBackgroundColor:[UIColor clearColor]];
    [Cancel setShowsTouchWhenHighlighted:NO];
    [Cancel setImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
    [Cancel setAutoresizesSubviews:YES];
    [Cancel addTarget:self action:@selector(Cancel:) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:Cancel];
      [Cancel setAlpha:0];



        //91 × 38 pixels
    Delete = [[UIButton alloc] initWithFrame:CGRectMake(225,990+39, 91/1.1,  38/1.1)];
    [Delete setBackgroundColor:[UIColor clearColor]];
    [Delete setShowsTouchWhenHighlighted:NO];
    [Delete setImage:[UIImage imageNamed:@"Delete.png"] forState:UIControlStateNormal];
    [Delete setAutoresizesSubviews:YES];
    [Delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:Delete];
     [Delete setAlpha:1];

  
    
   // MedChoiceView.h
    
 
    
   
    medChoise = [[MedChoiceView alloc] initWithFrame:CGRectMake(10,10,300,460)];
    [medChoise setBackgroundColor:[UIColor whiteColor]];
    [medChoise setAutoresizesSubviews:YES];
    [medChoise.layer setBorderWidth:1];
    [medChoise.layer setCornerRadius:10];
    [medChoise.layer setMasksToBounds:YES];
    [medChoise getparant:self];
    [medChoise.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [self.view addSubview:medChoise];
    [medChoise setAlpha:0];
   

  
    
     [self DynamicCell:34+39+39];
    
    [super viewDidLoad];
    
    
    
        // Do any additional setup after loading the view, typically from a nib.
}

-(void)DynamicCell:(NSInteger)difference

{
    
    
    //NSLog(@"difference");
    
    [Alerts setFrame:CGRectMake(10, 516+difference, self.view.frame.size.width-20, 79)];
    
    [Quantity setFrame:CGRectMake(10, 609+difference, self.view.frame.size.width-20, 128+39)];
    [Contacts setFrame:CGRectMake(10, 789+difference, self.view.frame.size.width-20, 118)];
    
    
    [scrollViewSpread setContentSize:CGSizeMake(320, 920+39+difference)];
    
}



-(void)clear:(UIButton*)sender

{
    
 
    
    ChosenMedicen =[[NSMutableDictionary alloc]init];
    
    NSArray *names =[NSArray arrayWithObjects:@"ID", @"Name", @"Strength", @"Type", @"Profile", @"Schedule", @"Frequency", @"Remaining", @"Filling", @"Refilling", @"Doctor", @"Prescription", @"Notes", @"Image", @"ID_Medicine", @"ColorRed", @"ColorGreen", @"Colorblue", @"Coloralpha", @"Audio", @"Quantity", @"Duration", @"Instructions", @"PerscriptionImage",@"Starting",@"Recording", @"Frequency_text", nil];
    
    for (int i=0 ; i<[names count]; i++) {
        
        
        [[NSUserDefaults standardUserDefaults] setObject:@" " forKey:[names objectAtIndex:i]];
        
        [ChosenMedicen setObject:@" " forKey:[names objectAtIndex:i]];
        
        
        
    }
    
    //////////////////////////////////NSLog(@"%@", ChosenMedicen);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    chosenID = [appDelegate newUUID];
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir, @"Medhot.png"];
    
    [ChosenMedicen setObject:chosenID forKey:@"ID_Medicine"];
    [ChosenMedicen setObject:appDelegate.CurrentID forKey:@"ID"];
    [ChosenMedicen setObject:jpegFilePath forKey:@"Image"];
    
    [ChosenMedicen setObject:jpegFilePath  forKey:@"PerscriptionImage"];
    
    
    [self setItems:ChosenMedicen];
}


-(void)editOrShow:(NSString*)sender

{
    
    [medChoise setAlpha:0];
    [self.scrollViewSpread setAlpha:1];
    
    
        if ([sender isEqualToString:@"Add"]) {
            
          
            self.title =@"Add Medicine";
            [Delete setAlpha:0];
            
            
            NSData *data = [NSData dataWithData:UIImagePNGRepresentation(headShot.image)];
            
            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];
            
            
            [data writeToFile:jpegFilePath atomically:YES];

            
        }
    
      if ([sender isEqualToString:@"Edit"]) {
          
           self.title =@"Edit Medicine";
          [Delete setAlpha:1];
      }
    /*

    
    if ([sender isEqualToString:@"Add"]) {

        
         self.navigationItem.title = @"Add Med";
        [Save setAlpha:1];
        [Cancel setAlpha:1];
        [Delete setAlpha:0];
        
        if ([[GetData getMedicine] count] ==0) {
             [medChoise setAlpha:0];
              [self.scrollViewSpread setAlpha:1];
        }
        
        else
        {
           [medChoise setAlpha:1];
             [self.scrollViewSpread setAlpha:0];
            
            NSArray *profileItem =[NSArray arrayWithObjects:@"Name", @"ID", @"Image", @"ColorRed", @"ColorGreen", @"Colorblue", @"Coloralpha",@"Audio", nil];
            if ([[GetData getProfiles:profileItem] count] ==1) {
                
                
                [self.scrollViewSpread setAlpha:1];
                [medChoise setAlpha:0];
                
                
            }
        }
        
    }
    if ([sender isEqualToString:@"Edit"]) {

         self.navigationItem.title = @"Edit Med";
        [Save setAlpha:1];
        [Cancel setAlpha:0];
          [Delete setAlpha:1];
           [medChoise setAlpha:0];
         [self.scrollViewSpread setAlpha:1];
    }

    
     */
    
}



-(void)switchValueReminder:(UISwitch*)sender

{

    if (sender.tag ==102) {

        if (sender.isOn) {

            [ChosenMedicen setObject:@"YES" forKey:@"Refilling"];
        }
        else
            {
            [ChosenMedicen setObject:@"NO" forKey:@"Refilling"];
            }
    }
    if (sender.tag ==100) {

        if (sender.isOn) {

            [ChosenMedicen setObject:@"YES" forKey:@"Schedule"];
        }
        else
            {
            [ChosenMedicen setObject:@"NO" forKey:@"Schedule"];
            }
    }

       
}

- (IBAction)showMediaPicker:(id)sender
{


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate.toolbarDown setAlpha:0];

    
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];

    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    [self presentViewController:mediaPicker animated:YES completion:NULL];

}

-(void)Next:(UIButton*)sender

{

}

- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{

    //////////////////NSLog(@"mediaPicker");
    
    
    
    MPMediaItem *anItem = (MPMediaItem *)[mediaItemCollection.items objectAtIndex:0];

    NSURL *assetURL = [anItem valueForProperty: MPMediaItemPropertyAssetURL];


    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

    NSString *documentsDir = [paths2 objectAtIndex:0];





    NSData *data2 = [NSData dataWithContentsOfURL:assetURL];
    
    [data2 writeToFile:[NSString stringWithFormat:@"%@/Copy.mp3", documentsDir] atomically:YES];


    //////////////////////////////////NSLog(@"MPMediaItemPropertyAssetURL %@", assetURL);

    [self dismissViewControllerAnimated:YES completion:NULL];


}

- (void) photoCropper:(SSPhotoCropperViewController *)photoCropper
         didCropPhoto:(UIImage *)photo
{
 
    
    
    [headShot setImage:[self thumbWithSideOfLength2:120 image:photo]];
    [photoCropper dismissViewControllerAnimated:YES  completion:nil];
    
    NSData *data = [NSData dataWithData:UIImagePNGRepresentation(photo)];
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];
    
    
    [data writeToFile:jpegFilePath atomically:YES];
    [ChosenMedicen setObject:jpegFilePath forKey:@"Image"];
    
}


- (void) photoCropperDidCancel:(SSPhotoCropperViewController *)photoCropper
{
    
    
    
    [photoCropper dismissViewControllerAnimated:YES  completion:nil];
}

-(void)getCameraPicture:(UIButton*)sender
{

    photchoice =@"getCameraPicture";
    
    //////////////////NSLog(@"getCameraPicture");

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate.toolbarDown setAlpha:0];


    

	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.allowsEditing = YES;
#if (TARGET_IPHONE_SIMULATOR)
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
#endif
	[self presentViewController: picker animated:YES completion:NULL];
}


-(void)imagePickerController:(UIImagePickerController *)picker
      didFinishPickingImage : (UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{

    
     //////////////////NSLog(@"imagePickerController");
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

  
    [headShot setImage:[self thumbWithSideOfLength2:120 image:image]];

    
    
    NSData *data = [NSData dataWithData:UIImagePNGRepresentation(image)];
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];
    
    
    [data writeToFile:jpegFilePath atomically:YES];
    [ChosenMedicen setObject:jpegFilePath forKey:@"Image"];

    if ([photchoice isEqualToString:@"selectExitingPicture"]) {
        
    }
    else if ([photchoice isEqualToString:@"getCameraPicture"]) {
        
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
    }


    [self dismissViewControllerAnimated:YES completion:NULL];
    double delayInSeconds = 0.4;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"crop existing image?"];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:50000];
    [alert addButtonWithTitle:@"Crop"];
    [alert addButtonWithTitle:@"NO"];
    [alert show];

});
   

}


- (UIImage *)thumbWithSideOfLength2:(float)length image:(UIImage*)mainImage {

    UIImage *thumbnail;

   if (mainImage.size.width< length) {

        CGSize itemSiz1 = CGSizeMake(mainImage.size.width*(length/mainImage.size.width), mainImage.size.height*(length/mainImage.size.width));

        UIGraphicsBeginImageContextWithOptions(itemSiz1, NO, 0.0);

        CGRect imageRect2 = CGRectMake(0.0, 0.0, itemSiz1.width, itemSiz1.height);
        [mainImage drawInRect:imageRect2];

        mainImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }

    UIImageView *mainImageView = [[UIImageView alloc] initWithImage:mainImage];
    BOOL widthGreaterThanHeight = (mainImage.size.width > mainImage.size.height);
    float sideFull = (widthGreaterThanHeight) ? mainImage.size.height : mainImage.size.width;
    CGRect clippedRect = CGRectMake(0, 0, sideFull, sideFull);

    UIGraphicsBeginImageContext(CGSizeMake(length, length));
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextClipToRect( currentContext, clippedRect);
    CGFloat scaleFactor = length/sideFull;
    if (widthGreaterThanHeight) {

        CGContextTranslateCTM(currentContext, -((mainImage.size.width-sideFull)/2)*scaleFactor, 0);

    }
    else {
        CGContextTranslateCTM(currentContext, 0, -((mainImage.size.height - sideFull) / 2) * scaleFactor);
    }

    CGContextScaleCTM(currentContext, scaleFactor, scaleFactor);
    [mainImageView.layer renderInContext:currentContext];
    thumbnail = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return thumbnail;

}


-(void)setItems:(NSMutableDictionary*)set

{
    

    ChosenMedicen =[[NSMutableDictionary alloc]init];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];



    ChosenMedicen = set;


    chosenID = [set valueForKey:@"ID_Medicine"];
    
    
    
    if ([selectedNotifictions count] ==0)
        
    {
        
        NSMutableArray *give = [Notification GetNotification:chosenID];
        
        for (int i=0; i<[give count]; i++) {
            
            NSMutableDictionary *add = [[NSMutableDictionary alloc] init];
            
            
            
            [add setObject:[[give objectAtIndex:i] valueForKey:@"CurrentDate"] forKey:@"CurrentDate"];
            [add setObject:[[give objectAtIndex:i] valueForKey:@"CurrentDay"] forKey:@"CurrentDay"];
            [add setObject:[[give objectAtIndex:i] valueForKey:@"CurrentTime"] forKey:@"CurrentTime"];
            [add setObject:[[give objectAtIndex:i] valueForKey:@"Day"] forKey:@"Day"];
            [add setObject:[[give objectAtIndex:i] valueForKey:@"Diverence"] forKey:@"Diverence"];
            [add setObject:[[give objectAtIndex:i] valueForKey:@"ID_Timer"] forKey:@"ID_Timer"];
            [add setObject:[[give objectAtIndex:i] valueForKey:@"Moment"] forKey:@"Moment"];
            [add setObject:[[give objectAtIndex:i] valueForKey:@"Time"] forKey:@"Time"];
             [add setObject:[[give objectAtIndex:i] valueForKey:@"choice"] forKey:@"choice"];
            [add setObject:[[give objectAtIndex:i] valueForKey:@"intervalDays"] forKey:@"intervalDays"];
            [add setObject:[[give objectAtIndex:i] valueForKey:@"intervalOurs"] forKey:@"intervalOurs"];
            
            [selectedNotifictions addObject:add];
        }
        
    }
    
    


      [ChosenMedicen setObject:chosenID forKey:@"ID_Medicine"];
     [ChosenMedicen setObject:appDelegate.CurrentID forKey:@"ID"];


    for (UIScrollView *scroll in self.view.subviews) {

        for (UIView *view in scroll.subviews) {



            for (TekstIDLabel *label in view.subviews) {

                if ([label isKindOfClass:[TekstIDLabel class]]) {

                    if (label.title) {


                          //NSLog(@"%@", label.title);
                        
                        if ([label.title isEqualToString:@"Recording"]) {
                            
                            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"recordingMusic"] isEqualToString:@"444"]) {
                                
                            }
                            else if ([[NSUserDefaults standardUserDefaults] valueForKey:@"recordingMusic"]) {
                            
                                [label setText:[[[GetData getRecording] valueForKey:@"title"]objectAtIndex:[[[GetData getRecording] valueForKey:@"ID"] indexOfObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"recordingMusic"]]]];
                                
                            }
                            else
                                
                            {
                                
                                
                        //  
                            }

                        }
                        
                        else
                        {
                            
                            if ([label.title isEqualToString:@"Frequency"]) {
                                
                                //////////////////////////NSLog(@"%@", selectedNotifictions);
                                
                                if ([selectedNotifictions count] ==0) {
                                  
                                      [label setText:@" "];
                                }
                                else
                                {
                                [label setText:[ChosenMedicen valueForKey:@"Frequency_text"]];
                                    
                                }
                               
                            }
                            else
                            {

                                [label setText:[set valueForKey:label.title]];
                                
                            }
                            
                        }

                            if ([label.title isEqualToString:@"Type"]) {


                                appDelegate.TypeMed =[set valueForKey:label.title];
                            }

                      
                    }
                    
                    
                }
                
                
            }
            
            
        }
        
    }
    


    [headShot setImage:[UIImage imageWithContentsOfFile:[set valueForKey:@"Image"]]];



    UISwitch * swActive2 = (UISwitch *)[self.view viewWithTag:102];

    if ([[ChosenMedicen valueForKey:@"Refilling"] isEqualToString:@"YES"]) {

        [swActive2 setOn:TRUE animated:TRUE];
    }
    else   if ([[ChosenMedicen valueForKey:@"Refilling"] isEqualToString:@"NO"])
        {
        [swActive2 setOn:FALSE animated:TRUE];

        }
        else
            
        {
           [swActive2 setOn:TRUE animated:TRUE];   
        }


    
  

    UISwitch * swActive = (UISwitch *)[self.view viewWithTag:100];

    if ([[ChosenMedicen valueForKey:@"Schedule"] isEqualToString:@"YES"]) {

        [swActive setOn:TRUE animated:FALSE];
          ////////////////////////////////NSLog(@"%@1", [ChosenMedicen valueForKey:@"Schedule"]);
    }
    else if ([[ChosenMedicen valueForKey:@"Schedule"] isEqualToString:@"NO"])
    {
        [swActive2 setOn:FALSE animated:TRUE];
          ////////////////////////////////NSLog(@"%@2", [ChosenMedicen valueForKey:@"Schedule"]);
        
    }
    else
        
    {
        [swActive2 setOn:FALSE animated:TRUE];
          ////////////////////////////////NSLog(@"%@3", [ChosenMedicen valueForKey:@"Schedule"]);
    }
    



    appDelegate.PerscriptionNotes = [set valueForKey:@"Notes"];
    appDelegate.Notes = [set valueForKey:@"Prescription"];
        [Schdules setfreguency:[ChosenMedicen valueForKey:@"ID_Medicine"]];
    
    
    
    
}


-(void)setItemsChangeID:(NSMutableDictionary*)set

{
    
    ChosenMedicen =[[NSMutableDictionary alloc]init];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    ChosenMedicen = set;
    
    
    chosenID = [appDelegate newUUID];
    
    [ChosenMedicen setObject:chosenID forKey:@"ID_Medicine"];
    [ChosenMedicen setObject:appDelegate.CurrentID forKey:@"ID"];
    [ChosenMedicen setObject:@" " forKey:@"Frequency"];
   
    
    for (UIScrollView *scroll in self.view.subviews) {
        
        for (UIView *view in scroll.subviews) {
            
            
            
            for (TekstIDLabel *label in view.subviews) {
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    if (label.title) {
                        
                        
                        if ([label.title isEqualToString:@"Recording"]) {
                            
                            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"recordingMusic"] isEqualToString:@"444"]) {
                                
                            }
                            else
                            {
                                
                                [label setText:[[[GetData getRecording] valueForKey:@"title"]objectAtIndex:[[[GetData getRecording] valueForKey:@"ID"] indexOfObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"recordingMusic"]]]];
                                
                            }
                            
                        }
                        
                        else
                        {
                            
                            [label setText:[set valueForKey:label.title]];
                            
                        }
                        
                        if ([label.title isEqualToString:@"Type"]) {
                            
                            
                            appDelegate.TypeMed =[set valueForKey:label.title];
                        }
                        
                        
                    }
                    
                    
                }
                
                
            }
            
            
        }
        
    }
    
    
    [headShot setImage:[UIImage imageWithContentsOfFile:[set valueForKey:@"Image"]]];
    
    
    
    UISwitch * swActive2 = (UISwitch *)[self.view viewWithTag:102];
    
    if ([[ChosenMedicen valueForKey:@"Refilling"] isEqualToString:@"YES"]) {
        
        [swActive2 setOn:TRUE animated:TRUE];
    }
    else
    {
        [swActive2 setOn:FALSE animated:TRUE];
        
    }
    
    
    
    UISwitch * swActive = (UISwitch *)[self.view viewWithTag:100];
    
    if ([[ChosenMedicen valueForKey:@"Schedule"] isEqualToString:@"YES"]) {
        
        [swActive setOn:TRUE animated:FALSE];
    }
    else
    {
        [swActive setOn:FALSE animated:FALSE];
        
    }
    
    
    appDelegate.PerscriptionNotes = [set valueForKey:@"Notes"];
    appDelegate.Notes = [set valueForKey:@"Prescription"];
    

    
}




- (void) viewWillAppear:(BOOL)animated {
	
    
}


-(void)setNew:(NSString*)set;

{
    
    ////////////////////////////NSLog(@"setNew");

    ChosenMedicen =[[NSMutableDictionary alloc]init];
    
    NSArray *names =[NSArray arrayWithObjects:@"ID", @"Name", @"Strength", @"Type", @"Profile", @"Schedule", @"Frequency", @"Remaining", @"Filling", @"Refilling", @"Doctor", @"Prescription", @"Notes", @"Image", @"ID_Medicine", @"ColorRed", @"ColorGreen", @"Colorblue", @"Coloralpha", @"Audio", @"Quantity", @"Duration", @"Instructions", @"PerscriptionImage",@"Starting",@"Recording", @"Frequency_text", nil];

    for (int i=0 ; i<[names count]; i++) {

        ChosenMedicen = [GetData getMedicinewithIDCopy:@"70f855d5-72cc-4092-acac-f31cf812a805"];
        
        
        [ChosenMedicen setObject:set forKey:@"Profile"];
        
       
     
        
        

        
        
    }
    
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir, @"Medhot.png"];
    
   
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    chosenID = [appDelegate newUUID];

     [ChosenMedicen setObject:chosenID forKey:@"ID_Medicine"];
     [ChosenMedicen setObject:appDelegate.CurrentID forKey:@"ID"];
     [ChosenMedicen setObject:jpegFilePath forKey:@"Image"];
     [ChosenMedicen setObject:jpegFilePath forKey:@"PerscriptionImage"];
   
         ////////////////////////////NSLog(@"%@", ChosenMedicen);

    
    [self setItems:ChosenMedicen];
}

-(void)viewDidAppear:(BOOL)animated

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    appDelegate.Editing=YES;
    [appDelegate.toolbarDown setAlpha:1];
    
      //////NSLog(@"editing");
}


-(void)viewDidDisappear:(BOOL)animated

{
   
  
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *) picker
{


 


    
	[picker dismissViewControllerAnimated:YES  completion:nil];

        //////////////////////////////////////////////NSLog(@"dismissViewControllerAnimated");


}


- (void) mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{

 


    
		[self dismissViewControllerAnimated:YES completion:NULL];
}




- (IBAction)showMediaPicker
{
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];

    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    [self presentViewController:mediaPicker animated:YES completion:NULL];
}


-(void) changeColorFridge
{
    LeveyPopColorView *pinview01 = (LeveyPopColorView *)[self.view  viewWithTag:3459148];
    if (pinview01) {

        [pinview01 removeFromSuperview];


    }
    else
        {


        color = [[LeveyPopColorView alloc] initWithTitle:@"Share Photo to..." options:NULL setIt:2];
            // lplv.isModal = NO;
        [color setTag:3459148];
        [color showInView:self.view animated:YES];
        [color getParent:self get:3];


        }

    
    
    
    
    
}

-(void)plaese:(NSString*)set

{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Fill in a %@ first to continue", set]
                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    
}


-(void) save:(UIButton*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.Editing=NO;
    
 
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
     [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm"];

   
    
    if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Atleast fill in a name"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        
    }
    else
    {
   

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
 

        
        
        NSArray *names =[NSArray arrayWithObjects:@"ID", @"Name", @"Strength", @"Type", @"Profile", @"Schedule", @"Frequency", @"Remaining", @"Filling", @"Refilling", @"Doctor", @"Prescription", @"Notes", @"Image", @"ID_Medicine", @"ColorRed", @"ColorGreen", @"Colorblue", @"Coloralpha", @"Audio", @"Quantity", @"Duration", @"Instructions", @"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text", nil];
        
        
        
     
        
        ////////////////////NSLog(@"ChosenMedicen %@", ChosenMedicen);
        
        
        
    [SaveData insertMedecineSQL:ChosenMedicen setnames:names];

        if ([[ChosenMedicen valueForKey:@"Frequency"] isEqualToString:@" "]) {
            
        }
        else
        {
            [SaveData RemovetNotification:chosenID];
            [Notification RemoveNotification:chosenID];
            
            
            for (int i=0; i<[selectedNotifictions count]; i++) {
                
                
              
                   NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                
                set  = ChosenMedicen;
                set = [selectedNotifictions objectAtIndex:i];
                
                [set setObject:chosenID forKey:@"ID_Medicine"];
                [set setObject:appDelegate.CurrentID forKey:@"ID"];
                
                if (appDelegate.recordingMusic) {
                    [set setObject:appDelegate.recordingMusic forKey:@"Recording"];
                }
                else
                    
                {
                    [set setObject:@"444" forKey:@"Recording"];
                }
                
                
                for (int i=0; i<[names count]; i++) {
                    
                    if ([[names objectAtIndex:i] isEqualToString:@"Image"])
                        
                    {
                        
                        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                        NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];
                        [set setObject:jpegFilePath  forKey:[names objectAtIndex:i]];
                        
                    }
                    else  if ([[names objectAtIndex:i] isEqualToString:@"Remaining"])
                        
                    {
                         [set setObject:[ChosenMedicen valueForKey:@"Filling"]  forKey:@"Filling"];
                    }
                    else if ([[names objectAtIndex:i] isEqualToString:@"PerscriptionImage"])
                        
                    {
                        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                        NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir, [NSString stringWithFormat:@"Perscription_%@.png", chosenID]];
                        [set setObject:jpegFilePath  forKey:[names objectAtIndex:i]];
                    }
                    else
                        
                    {
                        
                        [set setObject:[ChosenMedicen valueForKey:[names objectAtIndex:i]] forKey:[names objectAtIndex:i]];
                        
                    }
                }
                
                
                
            
                
                
                
                
                [SaveData insertNotification:set];
                
                
                [Notification setDate:set];

        
        
        }
        
 
        
        
    
  
   

    }
     
  

     [[self navigationController] popViewControllerAnimated:YES];
        
      
        appDelegate.Editing=NO;
       

    }
         
     
   

    
    [appDelegate.Profile removeAllObjects];
    
    NSArray *names =[NSArray arrayWithObjects:@"ID", @"Name", @"Strength", @"Type", @"Profile", @"Schedule", @"Frequency", @"Remaining", @"Filling", @"Refilling", @"Doctor", @"Prescription", @"Notes", @"Image", @"ID_Medicine", @"ColorRed", @"ColorGreen", @"Colorblue", @"Coloralpha", @"Audio", @"Quantity", @"Duration", @"Instructions", @"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text", nil];
    
    
    for (int i=0 ; i<[names count]; i++) {
        
        
        [[NSUserDefaults standardUserDefaults] setObject:@" " forKey:[names objectAtIndex:i]];
        
    }


}

-(void)showalert

{
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"To start please click\n \"New User\" to create your\nprofile and start adding\nyour Meds"];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464749];
    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"New user"];
    [alert show];
}

-(void)getParant:(ViewController*)parant

{
    parantIt = parant;
}


-(void) CreateAccount

{

}
-(void)Action:(UIButton*)sender

{
    
    ////////////////////////////////////NSLog(@"%@", ChosenMedicen);

    
    //NSLog(@"%@", sender.titleLabel.text);
    if ([sender.titleLabel.text isEqualToString:@"Name:"]) {

        
        
    }
    if ([sender.titleLabel.text isEqualToString:@"Color"]) {

        [self changeColorFridge];
    }
    if ([sender.titleLabel.text isEqualToString:@"Alert"]) {


        [self showMediaPicker];
           
    }
    if ([sender.titleLabel.text isEqualToString:@"Photo"]) {

        //[self getCameraPicture:sender];
        
         [self makeAchoice];
    }
    else
        {
            
            if ([sender.titleLabel.text isEqualToString:@"Name"]) {
                
                
              
                    NameItemViewController *controller = [[NameItemViewController alloc]init];
                    [self.navigationController pushViewController:controller animated:YES];
                    
                    
                    for (UIScrollView *scroll in self.view.subviews) {
                        
                        for (UIView *view in scroll.subviews) {
                            
                            
                            
                            for (TekstIDLabel *label in view.subviews) {
                                
                                if ([label isKindOfClass:[TekstIDLabel class]]) {
                                    
                                    
                                    if ([sender.titleLabel.text isEqualToString:label.title]) {
                                        [controller whatLabel:label];
                                        
                                    }
                                    
                                }}}}
                    
                    [controller getParant:self];
                    [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
                    [controller hideAndseek:sender.titleLabel.text];
                    
               
                
            }


   
        else if ([sender.titleLabel.text isEqualToString:@"Instructions"]||[sender.titleLabel.text isEqualToString:@"Duration"]) {


            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {
                
                [self plaese:@"name"];
                
                
            }
            
            else
            {
            FrequencyViewController *controller = [[FrequencyViewController alloc]init];
                [self.navigationController pushViewController:controller animated:YES];
                
               
                
                for (UIScrollView *scroll in self.view.subviews) {
                    
                    for (UIView *view in scroll.subviews) {
                        
                        
                        
                        for (TekstIDLabel *label in view.subviews) {
                            
                            if ([label isKindOfClass:[TekstIDLabel class]]) {
                                
                                if ([sender.titleLabel.text isEqualToString:label.title]) {
                                    
                                    
                                    [controller whatLabel:label];
                                    [controller setfreguency:[ChosenMedicen valueForKey:@"ID_Medicine"]];
                                    
                                }
                                
                            }}}}
                
                [controller getParant:self];
                [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
                [controller hideAndseek:sender.titleLabel.text];
                [controller setChoice:sender.titleLabel.text];
               
                [controller.Frequencytimer setObject:[ChosenMedicen valueForKey:@"Starting"] forKey:@"Starting"];
                [controller.Frequencytimer setObject:[ChosenMedicen valueForKey:@"Duration"] forKey:@"Duration"];
                [controller.Frequencytimer setObject:[ChosenMedicen valueForKey:@"Frequency"] forKey:@"Frequency"];
                [controller.Frequencytimer setObject:Schdules.related.listofitemsCopyed forKey:@"FrequencyList"];
   
         

            
            }
            
        
        }
            
      
        else if ([sender.titleLabel.text isEqualToString:@"Filling"]) {
            

            if ([[ChosenMedicen valueForKey:@"Type"] isEqualToString:@" "]) {
                
                [self plaese:@ "Medicine type"];
                
                
            }
            
            else
            {


            FillingViewController *controller = [[FillingViewController alloc]init];
            [self presentViewController:controller animated:YES completion:Nil];
            [controller getParant:self];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {


                            if ([sender.titleLabel.text isEqualToString:label.title]) {
                                [controller whatLabel:label];

                            }

                        }}}}

                //[controller getParant:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            
            
            }
            
            
            
        }
        else if ([sender.titleLabel.text isEqualToString:@"Quantity"]) {

          
            if ([[ChosenMedicen valueForKey:@"Type"] isEqualToString:@" "]) {
                
                [self plaese:@ "Medicine type"];
                
                
            }
            
            else
            {

            DoseViewController *controller = [[DoseViewController alloc]init];
            [self presentViewController:controller animated:YES completion:Nil];
            [controller getParant:self];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {


                            if ([sender.titleLabel.text isEqualToString:label.title]) {
                                [controller whatLabel:label];

                            }

                        }}}}

                //[controller getParant:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
                
            }
           
            
            
        }

        else if ([sender.titleLabel.text isEqualToString:@"Remaining"]) {

            if ([[ChosenMedicen valueForKey:@"Type"] isEqualToString:@" "]) {
                
                [self plaese:@ "Medicine type"];
                
                
            }
            
            else
            {

            RemainingViewController *controller = [[RemainingViewController alloc]init];
            [self presentViewController:controller animated:YES completion:Nil];
            [controller getParant:self];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {


                            if ([sender.titleLabel.text isEqualToString:label.title]) {
                                [controller whatLabel:label];

                            }

                        }}}}

                //[controller getParant:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            
            }
            
            
            
            
        }
        else if ([sender.titleLabel.text isEqualToString:@"Strength"]) {

            ////////////////////////////////////NSLog(@"Type %@",[ChosenMedicen valueForKey:@"Type"]);
            
            if ([[ChosenMedicen valueForKey:@"Type"] isEqualToString:@" "]) {
                
                [self plaese:@ "Medicine Type"];
                
                
            }
            else
            {

            StrengthViewController *controller = [[StrengthViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {


                            if ([sender.titleLabel.text isEqualToString:label.title]) {
                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParant:self];
           
            [controller hideAndseek:sender.titleLabel.text];
            
            
            
            }
            
        }
        else if ([sender.titleLabel.text isEqualToString:@"Notes"]) {

            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {
                
                [self plaese:@"name"];
                
                
            }
            
            else
            {

            NoteViewController *controller = [[NoteViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleLabel.text isEqualToString:label.title]) {


                                    [controller whatLabel:label];

                            }

                        }}}}
            
                [controller getParant:self];
                
            }
            

        }

        else if ([sender.titleLabel.text isEqualToString:@"Type"]) {


            TypeViewController *controller = [[TypeViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleLabel.text isEqualToString:label.title]) {


                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParant:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            [controller setChoice:sender.titleLabel.text];
        }


        else if ([sender.titleLabel.text isEqualToString:@"Profile"]) {


            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {
                
                [self plaese:@"name"];
                
                
            }
            
            else
            {
            ProfileViewController *controller = [[ProfileViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
      

            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                             if ([sender.titleLabel.text isEqualToString:label.title]) {
                            
                            
                            [controller whatLabel:label];

                             }
                            
                        }}}}

            [controller getParant:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            [controller setChoice:sender.titleLabel.text];
                
            }
        }

        else if ([sender.titleLabel.text isEqualToString:@"Doctor"]) {

            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {
                
               // [self plaese:@"name"];
                
                
            }
            
            else
            {

            DocterViewController *controller = [[DocterViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleLabel.text isEqualToString:label.title]) {


                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParant:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            [controller setChoice:sender.titleLabel.text];
                
            }
        }
        else if ([sender.titleLabel.text isEqualToString:@"Prescription"]) {

            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {
                
                [self plaese:@"name"];
                
                
            }
            
            else
            {

            PerscriptionViewController *controller = [[PerscriptionViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleLabel.text isEqualToString:label.title]) {


                                [controller whatLabel:label];

                            }

                        }}}}
            
            [controller getParant:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            [controller setChoice:sender.titleLabel.text];


          
                [controller.headShot setImage:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"PerscriptionImage"]]];
                controller.PerscriptionImage =[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"PerscriptionImage"]];

            }
            
        }
        else if ([sender.titleLabel.text isEqualToString:@"Frequency"]) {

            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {
                
                [self plaese:@"name"];
                
                
            }
            
            else
            {

            FrequencyViewController *controller = [[FrequencyViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
                
                


            for (UIScrollView *scroll in self.view.subviews) {

                for (SchedularView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleLabel.text isEqualToString:label.title]) {

                                
                                [controller whatLabel:Schdules.Frequent];
                                [controller setfreguency:[ChosenMedicen valueForKey:@"ID_Medicine"]];

                            }

                        }}}}
            
            [controller getParant:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            [controller setChoice:sender.titleLabel.text];
                
                [controller.Frequencytimer setObject:[ChosenMedicen valueForKey:@"Starting"] forKey:@"Starting"];
                [controller.Frequencytimer setObject:[ChosenMedicen valueForKey:@"Duration"] forKey:@"Duration"];
               
                
            }
        }
            
             else if ([sender.titleLabel.text isEqualToString:@"Recording"]) {
                 
                
                 RecordingViewController *controller = [[RecordingViewController alloc]init];
                 [self.navigationController pushViewController:controller animated:YES];
                 
                 for (UIScrollView *scroll in self.view.subviews) {
                     
                     for (UIView *view in scroll.subviews) {
                         
                         
                         
                         for (TekstIDLabel *label in view.subviews) {
                             
                             if ([label isKindOfClass:[TekstIDLabel class]]) {
                                 
                                 if ([sender.titleLabel.text isEqualToString:label.title]) {
                                     
                                     
                                     [controller whatLabel:label];
                                     
                                 }
                                 
                             }}}}
                 
                 [controller getParant:self];
                 [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", sender.titleLabel.text]];
                 [controller hideAndseek:sender.titleLabel.text];
                 [controller setChoice:sender.titleLabel.text];
              
             
                 
             }

    }
        
          
}


-(void)makeAchoice


{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@""
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Library" , @"Camera",nil];
    sheet.delegate = self;
    sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    
    [sheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    ////////////////////////////////////NSLog(@"%i",buttonIndex);
    
    if (buttonIndex ==1) {
        
        [self selectExitingPicture];
    }
    
    if (buttonIndex==2) {
        
        [self getCameraPicture:NULL];
        
    }
    
    
}

-(void)selectExitingPicture
{
    
    //////////////////NSLog(@"selectExitingPicture");
    
    photchoice =@"selectExitingPicture";
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    
    
    
	if([UIImagePickerController isSourceTypeAvailable:
        UIImagePickerControllerSourceTypePhotoLibrary])
	{
		UIImagePickerController *picker= [[UIImagePickerController alloc]init];
		picker.delegate = self;
       picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
		[self presentViewController:picker animated:YES  completion:NULL];
	
	}
	
    
}


- (void)setSelectedColor:(UIColor*)colorit{


    const CGFloat *components = CGColorGetComponents(colorit.CGColor);
     rood = components[0];
     groen = components[1];
     blauw = components[2];
     doorzicht = components[3];


    [ColorField setBackgroundColor:colorit];

    
}

-(void)setitemsDictionary:(NSString*)set name:(NSString*)name

{
    
      [[NSUserDefaults standardUserDefaults] setObject:set forKey:name];
    
  


  
    ////////////////////////////NSLog(@"name %@ %@",  [[NSUserDefaults standardUserDefaults] valueForKey:name], set);

    
    if (set) {
          [ChosenMedicen setObject:set forKey:name];
    }
    
     UISwitch * swActive = (UISwitch *)[self.view viewWithTag:100];
    if ([name isEqualToString:@"Frequency"]) {
         [swActive setOn:TRUE animated:FALSE];
          [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Schedule"];
            [ChosenMedicen setObject:@"YES" forKey:@"Schedule"];
        
    }
    
    /*
    
     UISwitch * swActive3 = (UISwitch *)[self.view viewWithTag:102];
    
    if ([name isEqualToString:@"Refilling"]) {
       
           [swActive3 setOn:TRUE animated:FALSE];
     
    }

     
     */

    ////////////////////////////////////////////NSLog(@"ChosenMedicen %@", ChosenMedicen);
 
    


}

- (IBAction)Cancel:(UIButton *)sender {


    [[self navigationController] popViewControllerAnimated:YES];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.Editing=NO;
   
}


- (IBAction)Delete:(UIButton *)sender {

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


                                           
   
    [SaveData RemovetMedice:[ChosenMedicen valueForKey:@"ID_Medicine"]];
    [SaveData RemovetNotification:[ChosenMedicen valueForKey:@"ID_Medicine"]];
    
    [Notification RemoveNotification:[ChosenMedicen valueForKey:@"ID_Medicine"]];
    
    if ( [[[self navigationController] viewControllers] count] ==4) {
        
        MedicineViewController *controller2 = [[[self navigationController] viewControllers] objectAtIndex:1];
        [controller2.Vraagantwoord removeObjectAtIndex:[[controller2.Vraagantwoord valueForKey:@"ID_Medicine"] indexOfObject:[ChosenMedicen valueForKey:@"ID_Medicine"]]];
        [controller2.table reloadData];
        
        [appDelegate.Navigaioncopy removeAllObjects];
        [appDelegate.Navigaioncopy addObject:[[[self navigationController] viewControllers] objectAtIndex:0]];
        [appDelegate.Navigaioncopy addObject:controller2];
        
        [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];
        
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else
    {
        MedicineViewController *controller2 = [[[self navigationController] viewControllers] objectAtIndex:0];
          [controller2.Vraagantwoord removeObjectAtIndex:[[controller2.Vraagantwoord valueForKey:@"ID_Medicine"] indexOfObject:[ChosenMedicen valueForKey:@"ID_Medicine"]]];
        [controller2.table reloadData];
       
        [appDelegate.Navigaioncopy removeAllObjects];
        [appDelegate.Navigaioncopy addObject:controller2];
        [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];
        
    }
 
 
}







-(void)getParantGo:(UIViewController*)parant;

{

    parantGo =parant;
    
}
- (IBAction)OK:(UIButton *)sender {

 
   
 
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{
 //////////////////////////////////NSLog(@"turnbuttons");
}

@end
