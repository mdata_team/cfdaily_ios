//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeveyPopColorView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "BufferedNavigationController.h"
#import "Profiles.h"
#import "AppDelegate.h"

@class CodePopView, LeveyPopColorView, StartViewController, TekstIDLabel;
@interface NewProfileController : UIViewController<UITextViewDelegate, UIAlertViewDelegate, MPMediaPickerControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>


@property (nonatomic, retain) IBOutlet NSString *New;
@property (nonatomic, retain)  UILabel *titlelabel;
@property (nonatomic, retain) StartViewController *parantIt;
@property (nonatomic, retain) UIImageView *headShot;
@property (nonatomic, retain) MPMusicPlayerController *musicPlayer;




@property (nonatomic, assign)UIButton *Cancel;
@property (nonatomic, retain) LeveyPopColorView *color;
@property (nonatomic, retain) UILabel *Nameit;
@property (nonatomic, retain) TekstIDLabel *Sound;
@property (nonatomic, retain) UILabel *ColorField;
@property (nonatomic, retain) NSString *chosenID;
@property (nonatomic, strong) Profiles *chosenCourse;
@property (nonatomic, strong) NSString *Editor;
@property (nonatomic, retain)  UILabel *Alert;
- (IBAction)OK:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)setItems:(Profiles*)set;
-(void)setNew;
-(void)getParant:(UIViewController*)parant;
-(void)setitemsDictionary:(NSString*)set name:(NSString*)name;
-(void)plaese:(NSString*)set;
@end
