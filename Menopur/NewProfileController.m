//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "NewProfileController.h"
#import "MusicViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AlertViewController.h"
#import "NameViewController.h"
#import "SaveData.h"
#import "SaveCore.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "Notification.h"
#import "Profiles.h"
#import "ActionArrow.h"

@interface NewProfileController ()

@end

@implementation NewProfileController

@synthesize color;



@synthesize musicPlayer;
@synthesize headShot;
@synthesize Nameit;
@synthesize Sound;
@synthesize ColorField;
@synthesize chosenID;
@synthesize parantIt;
@synthesize Cancel;
@synthesize chosenCourse;
@synthesize titlelabel;
@synthesize Alert;
@synthesize New;
@synthesize Editor;


- (void)viewDidLoad
{
    
    [self.view.layer setMasksToBounds:YES];
    
    
    //////////////////////////NSLog(@"Editor %@", Editor);
    
    
    
    if ([Editor isEqualToString:@"edit"]) {
        
        self.navigationItem.title =NSLocalizedString(@"Edit Profile",nil);
        
        //********** Google Analytics ****************

        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"P_Edit Profile_iOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        //********** Google Analytics ****************
        
    }
    else
    {
        
          self.navigationItem.title = NSLocalizedString(@"Add Profile",nil);
      
        //********** Google Analytics ****************
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"P_Add Profile_iOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

        //********** Google Analytics ****************
        
    }
    
    
    
    NSArray *names =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text", nil];
   
   for (NSInteger i=0 ; i<[names count]; i++) {
        
        
        [[NSUserDefaults standardUserDefaults] setObject:@" " forKey:[names objectAtIndex:i]];
        
    }
    
    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    
    
    musicPlayer = [MPMusicPlayerController iPodMusicPlayer];
    
    
    
	[musicPlayer beginGeneratingPlaybackNotifications];
    
    
    UIView *Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 220)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1];
//    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //27-06-2017
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];
    [Combi release];
    
    
    
    //142 × 36 pixels
    
    
    
    headShot = [[UIImageView alloc] initWithFrame:CGRectMake(10, 90, 80, 80)];
//    [headShot setBackgroundColor:[UIColor blackColor]];
    [headShot setTag:130];
    [headShot setImage:[UIImage imageNamed:@"headshot"]];
    [Combi addSubview:headShot];
    [headShot release];
    
    
    ActionArrow *Photocover = [[ActionArrow alloc]  initWithFrame:CGRectMake(10, 90, 80, 80)];
    [Photocover setTitleit:NSLocalizedString(@"Alert",nil)];
   
    [Photocover setTag:160];
    [Photocover addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:Photocover];
    [Photocover release];
    
    
    UILabel *NameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-20, 40)];
//    [NameLabel setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //27-06-2017
    [NameLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [NameLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [NameLabel setText:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Name:",nil)]];
    [NameLabel setTag:120];
    [NameLabel.layer setBorderWidth:1];
//    [NameLabel.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //27-06-2017 change
    [NameLabel.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [NameLabel setBackgroundColor:[UIColor clearColor]];
    [NameLabel setNumberOfLines:3];
    [Combi addSubview:NameLabel];
    [NameLabel release];
    
    
    Nameit = [[UILabel alloc] initWithFrame:CGRectMake(90, 0, self.view.frame.size.width-150, 40)];
//    [Nameit setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [Nameit setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [Nameit setFont:[UIFont systemFontOfSize:14]];
    [Nameit setText:@""];
    [Nameit setTextAlignment:NSTextAlignmentRight];
    [Nameit setTag:120];
    [Nameit setBackgroundColor:[UIColor clearColor]];
    [Nameit setNumberOfLines:3];
    [Combi addSubview:Nameit];
    [Nameit release];
    
    
    ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Combi.frame.size.width-35, 3, 60/2, 72/2)];
    [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
    [NameButton setTitleit:NSLocalizedString(@"Name",nil)];
    [NameButton setTag:140];
    [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:NameButton];
    [NameButton release];
    
    
    UILabel *Color = [[UILabel alloc] initWithFrame:CGRectMake(0, 39, self.view.frame.size.width-20, 40)];
//    [Color setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //27-06-2017 changes
    [Color setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [Color setFont:[UIFont boldSystemFontOfSize:15]];
    [Color setText:[NSString stringWithFormat:@"   %@%@", NSLocalizedString(@"Colour",nil), NSLocalizedString(@":",nil)]];
    [Color setTag:120];
    [Color.layer setBorderWidth:1];
//    [Color.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //27-06-2017 change
    [Color.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Color setBackgroundColor:[UIColor clearColor]];
    [Color setNumberOfLines:3];
    [Combi addSubview:Color];
    [Color release];
    [Color.layer setMasksToBounds:YES];
    
    
    
    ActionArrow *ColorButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Combi.frame.size.width-35, 42, 60/2, 72/2)];
    [ColorButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
    [ColorButton setTitleit:NSLocalizedString(@"Colour",nil)];
    [ColorButton setTag:150];
    [ColorButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:ColorButton];
    [ColorButton release];
    
    
    
    ColorField = [[UILabel alloc] initWithFrame:CGRectMake(220, 44, 30, 30)];
    [ColorField setBackgroundColor:[UIColor whiteColor]];
    [ColorField.layer setBorderWidth:1];
    [ColorField.layer setCornerRadius:10];
//    [ColorField.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //27-06-2017 change
    [ColorField.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [ColorField setNumberOfLines:3];
    [Combi addSubview:ColorField];
    [ColorField release];
    [ColorField.layer setMasksToBounds:YES];
    
    
    
    UIButton *Save = [[UIButton alloc] initWithFrame:CGRectMake(10, 240, 142/1.1,  36/1.1)];
//    [Save setBackgroundColor:[UIColor colorWithRed:0.067 green:0.467 blue:0.082 alpha:1.000]]; //29-06-2017 changes
    [Save setBackgroundColor:[UIColor colorWithRed:0.396 green:0.722 blue:0.533 alpha:1.000]];
    [Save setTitle:NSLocalizedString(@"Save",nil) forState:UIControlStateNormal];
    Save.titleLabel.layer.shadowOffset = CGSizeMake(1, 1);
    Save.titleLabel.layer.shadowOpacity = 1;
    Save.titleLabel.layer.shadowRadius = 1.0;
    [Save.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [Save.titleLabel setTextColor:[UIColor whiteColor]];
    [Save setShowsTouchWhenHighlighted:NO];
    [Save setAutoresizesSubviews:YES];
    [Save addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:[self setbutton:Save]];
    [Save release];
    
    
    
    //142 × 36 pixels
    
    
    Cancel = [[UIButton alloc] initWithFrame:CGRectMake(180, 240, 142/1.1,  36/1.1)];
//    [Cancel setBackgroundColor:[UIColor colorWithRed:0.553 green:0.000 blue:0.047 alpha:1.000]]; //01-07-2017 chanegs
    [Cancel setBackgroundColor:[UIColor colorWithRed:0.998 green:0.396 blue:0.396 alpha:1.000]];
    [Cancel setTitle:NSLocalizedString(@"Delete",nil) forState:UIControlStateNormal];
    Cancel.titleLabel.layer.shadowOffset = CGSizeMake(1, 1);
    Cancel.titleLabel.layer.shadowOpacity = 1;
    Cancel.titleLabel.layer.shadowRadius = 1.0;
    [Cancel.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [Cancel.titleLabel setTextColor:[UIColor whiteColor]];
    [Cancel setShowsTouchWhenHighlighted:NO];
    [Cancel setAutoresizesSubviews:YES];
    [Cancel addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:[self setbutton:Cancel]];
    [Cancel release];
    
    
    
    
    
    Alert = [[UILabel alloc] initWithFrame:CGRectMake(0, 180, self.view.frame.size.width-20, 40)];
//    [Alert setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //27-06-2017 change
    [Alert setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [Alert setFont:[UIFont boldSystemFontOfSize:15]];
    [Alert setTag:120];
    [Alert setText:[NSString stringWithFormat:@"    %@", NSLocalizedString(@"Alert Sound:",nil)]];
    [Alert.layer setBorderWidth:1];
//    [Alert.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //27-06-2017 change
    [Alert.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Alert setBackgroundColor:[UIColor clearColor]];
    [Alert setNumberOfLines:3];
    [Combi addSubview:Alert];
    [Alert release];
    
    
    
    
    ActionArrow *AlertButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Combi.frame.size.width-35, 182, 60/2, 72/2)];
    [AlertButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
    [AlertButton setTitleit:NSLocalizedString(@"Alert",nil)];
    [AlertButton setTag:160];
    [AlertButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:AlertButton];
    [AlertButton release];
    
    
    
    Sound = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 180, 260, 40)];
    [Sound setTextColor:[UIColor blackColor]];
    [Sound setFont:[UIFont systemFontOfSize:14]];
    [Sound setText:@""];
    [Sound setTextAlignment:NSTextAlignmentRight];
    [Sound setTitle:NSLocalizedString(@"Audio",nil)];
    [Sound setTag:120];
    [Sound setBackgroundColor:[UIColor clearColor]];
    [Sound setNumberOfLines:3];
    [Combi addSubview:Sound];
    [Sound release];
 
   
    
    
    ActionArrow *Photo = [[ActionArrow alloc]  initWithFrame:CGRectMake(100, 131, 39, 40)];
//    [Photo setImage:[UIImage imageNamed:@"photo.png"] forState:UIControlStateNormal]; //29-06-2017 changes
    [Photo setImage:[UIImage imageNamed:@"photo"] forState:UIControlStateNormal];
//     [Photo setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    [Photo setTitleit:NSLocalizedString(@"Photo",nil)];
    [Photo setTag:160];
    [Photo addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:Photo];
    [Photo release];
    [Photo.layer setCornerRadius:4];
   
    
    
    
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
//    [a1 setFrame:CGRectMake(2, 2, 50, 30)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        titlelabel = [[UILabel alloc] initWithFrame:CGRectZero];
        titlelabel.backgroundColor = [UIColor clearColor];
        titlelabel.font = [UIFont boldSystemFontOfSize:20.0];
        titlelabel.textAlignment = NSTextAlignmentCenter;
        [titlelabel setText:self.navigationItem.title];
        titlelabel.textColor = [UIColor whiteColor]; // change this color
        self.navigationItem.titleView = titlelabel;
          [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
        [titlelabel sizeToFit];
   
    }
    else
    {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
    
 
    [Cancel setAlpha:0];
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if ([Editor isEqualToString:@"edit"]) {
        [self setItems:appDelegate.currentProfile];
    }
    else
    {
            [self setNew];
    }
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        //NSLog(@"left");
        self.view.transform = CGAffineTransformMakeScale(-1, 1);
        
        for (UIView *view in self.view.subviews) {
            
            for (UILabel *label in view.subviews) {
                
                
                if ([label isKindOfClass:[UILabel class]]) {
                    
                    label.transform = CGAffineTransformMakeScale(-1, 1);
                    [label setTextAlignment:NSTextAlignmentNatural];
                    
                }
                
            }
            for (TekstIDLabel *label in view.subviews) {
                
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    label.transform = CGAffineTransformMakeScale(-1, 1);
                    [label setTextAlignment:NSTextAlignmentLeft];
                    
                }
                
            }
            for (UIButton *label in view.subviews) {
                
                
                if ([label isKindOfClass:[UIButton class]]) {
                    
                    label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                    [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                    
                }
                
            }
            
        }
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }

    
    
    [super viewDidLoad];
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(UIButton*)setbutton:(UIButton*)sender

{
    
    CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
    shineLayer2.frame = sender.layer.bounds;
    shineLayer2.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          nil];
    shineLayer2.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];
    
    
    
    [sender.layer addSublayer:shineLayer2];
    [sender.layer setMasksToBounds:YES];
    [sender.layer setCornerRadius:4];
    [sender.layer setBorderWidth:1.5];
    [sender.layer setBorderColor:sender.backgroundColor.CGColor];
    
    return sender;
}

-(void)plaese:(NSString*)set

{
    
    
    
    /*
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:[NSString stringWithFormat:NSLocalizedString(@"Fill in a %@ first to continue",nil), set]
                                                   delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
    [alert show];
    
    */
    
    UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"Fill in a %@ first to continue",nil) preferredStyle:UIAlertControllerStyleAlert];
    [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
        
    });

    
}






- (IBAction)showMediaPicker:(id)sender
{
    
    
    
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];
    
    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = NSLocalizedString(@"Select songs to play",nil);
    
    
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    
    [self presentViewController:mediaPicker animated:YES completion:NULL];
    
    
    
}




-(void)Next:(UIButton*)sender

{
    
}

- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{
    
    
    
    
    
    MPMediaItem *anItem = (MPMediaItem *)[mediaItemCollection.items objectAtIndex:0];
    
    NSURL *assetURL = [anItem valueForProperty: MPMediaItemPropertyAssetURL];
    
    
    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDir = [paths2 objectAtIndex:0];
    
    
    
    
    
    NSData *data2 = [NSData dataWithContentsOfURL:assetURL];
    
    [data2 writeToFile:[NSString stringWithFormat:@"%@/Caches/Copy.mp3", documentsDir] atomically:YES];
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
}

-(void)makeAchoice
{
    
    NSString *strLibrary = @"";
    
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {
        strLibrary = NSLocalizedString(@"Library", nil);
    }
    else
    {
        strLibrary = NSLocalizedString(@"Librar", nil);
    }
    
    
    UIActionSheet *sheet;
#if TARGET_OS_SIMULATOR
    
    //Simulator
    
    sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"",nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:NSLocalizedString(@"Cancel", nil)
//                                              otherButtonTitles:NSLocalizedString(@"Library",nil),nil];
                               otherButtonTitles:strLibrary,nil];

#else
    
    // Device
    sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"",nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:NSLocalizedString(@"Cancel", nil)
                                              /*otherButtonTitles:NSLocalizedString(@"Library",nil) ,*/
                               otherButtonTitles:strLibrary,NSLocalizedString(@"Camera", nil), nil];

#endif

    
//    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"",nil)
//                                                       delegate:self
//                                              cancelButtonTitle:nil
//                                         destructiveButtonTitle:NSLocalizedString(@"Cancel", nil)
//                                              otherButtonTitles:NSLocalizedString(@"Library",nil) , NSLocalizedString(@"Camera",nil),nil];
    
    sheet.delegate = self;
    sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    
    [sheet showInView:self.view];
    [sheet release];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex ==1) {
        
        [self selectExitingPicture];
    }
    
    if (buttonIndex==2) {
        
        [self getCameraPicture:NULL];
        
    }

}






-(void)getCameraPicture:(UIButton*)sender
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];

	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.allowsEditing = YES;
#if (TARGET_IPHONE_SIMULATOR)
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
#endif
	[self presentViewController: picker animated:YES completion:NULL];
}


-(void)imagePickerController:(UIImagePickerController *)picker
      didFinishPickingImage : (UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{
    
    
    
    
    [headShot setImage:[self thumbWithSideOfLength2:120 image:image]];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
    
    
}


-(void)selectExitingPicture
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    
    
    
	if([UIImagePickerController isSourceTypeAvailable:
        UIImagePickerControllerSourceTypePhotoLibrary])
	{
		UIImagePickerController *picker= [[UIImagePickerController alloc]init];
		picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
		[self presentViewController:picker animated:YES  completion:NULL];
		[picker release];
	}
	
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *) picker
{
	[picker dismissViewControllerAnimated:YES completion:NULL];
    
    
}

- (UIImage *)thumbWithSideOfLength2:(float)length image:(UIImage*)mainImage {
    
    UIImage *thumbnail;
    
    if (mainImage.size.width< length) {
        
        CGSize itemSiz1 = CGSizeMake(mainImage.size.width*(length/mainImage.size.width), mainImage.size.height*(length/mainImage.size.width));
        
        UIGraphicsBeginImageContextWithOptions(itemSiz1, NO, 0.0);
        
        CGRect imageRect2 = CGRectMake(0.0, 0.0, itemSiz1.width, itemSiz1.height);
        [mainImage drawInRect:imageRect2];
        
        mainImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    UIImageView *mainImageView = [[UIImageView alloc] initWithImage:mainImage];
    BOOL widthGreaterThanHeight = (mainImage.size.width > mainImage.size.height);
    float sideFull = (widthGreaterThanHeight) ? mainImage.size.height : mainImage.size.width;
    CGRect clippedRect = CGRectMake(0, 0, sideFull, sideFull);
    
    UIGraphicsBeginImageContext(CGSizeMake(length, length));
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextClipToRect( currentContext, clippedRect);
    CGFloat scaleFactor = length/sideFull;
    if (widthGreaterThanHeight) {
        
        CGContextTranslateCTM(currentContext, -((mainImage.size.width-sideFull)/2)*scaleFactor, 0);
        
    }
    else {
        CGContextTranslateCTM(currentContext, 0, -((mainImage.size.height - sideFull) / 2) * scaleFactor);
    }
    
    CGContextScaleCTM(currentContext, scaleFactor, scaleFactor);
    [mainImageView.layer renderInContext:currentContext];
    thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return thumbnail;
    
}



-(void)setNew

{

    
    New =@"New";
    
    self.navigationItem.title =NSLocalizedString(@"Add Profile",nil);


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.currentProfile.id =[appDelegate newUUID];
    appDelegate.currentProfile.name =@" ";
    
    
    NSData *myData = UIImagePNGRepresentation([UIImage imageNamed:@"headshot"]);
    appDelegate.currentProfile.image =myData;
    

    
    appDelegate.currentProfile.colorRed=@"1.0";
    appDelegate.currentProfile.coloralpha=@"1.0";
    appDelegate.currentProfile.colorGreen=@"1.0";
    appDelegate.currentProfile.colorblue =@"1.0";
    appDelegate.currentProfile.audio=@"twinkle";
    appDelegate.currentProfile.silence= @"NO";
    appDelegate.currentProfile.vibrate=@"NO";
    

    [Sound setText:appDelegate.currentProfile.audio];
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
     
        [Sound setTextAlignment:NSTextAlignmentLeft];
        
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }
    
    
    [Cancel setAlpha:0];
    
  
    
    
}



-(void)setItems:(Profiles*)set

{
   New =@"Old";
    
   self.navigationItem.title =NSLocalizedString(@"Edit Profile",nil);
    
   titlelabel.text = self.navigationItem.title;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.currentProfile =set;
    
    chosenCourse =set;
    chosenID = set.id;
    
    UIColor *colorit = [UIColor colorWithRed:[set.colorRed floatValue] green:[set.colorGreen   floatValue] blue:[set.colorblue  floatValue] alpha:1];
    
    
    if ([colorit isEqual:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]]) {
        
        colorit =[UIColor whiteColor];
        
    }
    else if ([colorit isEqual:[UIColor colorWithRed:1 green:1 blue:0.0 alpha:0.0]]) {
        
        colorit =[UIColor whiteColor];
        
    }
    

    
    
    
    [ColorField setBackgroundColor:colorit];
    [Nameit setText:set.name];
    [Sound setText:set.audio];
    [headShot setImage:[UIImage imageWithData:set.image]];
    
    
    if ([set.silence isEqualToString:@"YES"]) {
        
//        [Sound setAlpha:0];
        //18-07-2017
        [Sound setAlpha:1];
        [Sound setText:@"Silent"];

    }
    else
    {
        
        [Sound setAlpha:1];
    }
    
      [Cancel setAlpha:1];
    

}




- (void) viewDidAppear:(BOOL)animated {
	
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:1];
    if ([appDelegate.currentProfile.silence isEqualToString:@"YES"]) {
        
//        [Sound setAlpha:0];
        //18-07-2014
        [Sound setAlpha:1];
        [Sound setText:@"Silent"];
    }
    else
    {
        
        [Sound setAlpha:1];
    }
    
    appDelegate.FromScreen=NO;
    
    
    [appDelegate seeIfNotification];
}




-(void) changeColorFridge
{
    LeveyPopColorView *pinview01 = (LeveyPopColorView *)[self.view  viewWithTag:3459148];
    if (pinview01) {
        
        [pinview01 removeFromSuperview];
        
        
    }
    else
    {
        
        //NSLocalizedString(@"Share Photo to...",nil)
        color = [[LeveyPopColorView alloc] initWithTitle:NSLocalizedString(@"Share Photo to...",nil) options:NULL setIt:2];
        color.delegate = self;
        // lplv.isModal = NO;
        [color setTag:3459148];
        [color showInView:self.view animated:YES];
        [color getParent:self get:3];
        
        
    }
    
    
    
    
    
    
}




-(void) save:(UIButton*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    if ([Nameit.text isEqualToString:@""]) {
        
        [self plaese:NSLocalizedString(@"name",nil)];
    }
    else
    {
        
 
        
        
        
        
        if ([ColorField.backgroundColor isEqual:[UIColor colorWithWhite:1 alpha:1]]) {
            
            appDelegate.currentProfile.colorRed=@"1.0";
            appDelegate.currentProfile.coloralpha=@"1.0";
            appDelegate.currentProfile.colorGreen=@"1.0";
            appDelegate.currentProfile.colorblue =@"1.0";
            
        
        }
        else
        {
            
            const CGFloat * colorComponents = CGColorGetComponents(ColorField.backgroundColor.CGColor);
          
            appDelegate.currentProfile.colorRed=[NSString stringWithFormat:@"%f", colorComponents[0]];
            appDelegate.currentProfile.coloralpha=[NSString stringWithFormat:@"%f", colorComponents[3]];
            appDelegate.currentProfile.colorGreen=[NSString stringWithFormat:@"%f", colorComponents[1]];
            appDelegate.currentProfile.colorblue =[NSString stringWithFormat:@"%f", colorComponents[2]];
        }
        
        
        
        
        
    
        appDelegate.currentProfile.name=Nameit.text;;
        NSData *myData = UIImagePNGRepresentation(headShot.image);
        appDelegate.currentProfile.image =myData;
        
        
        
     
        
        if ([appDelegate.currentProfile.silence isEqualToString:@"YES"]||[appDelegate.currentProfile.silence isEqualToString:@"NO"]) {
          
         
            
        }
        else
        {
            
          appDelegate.currentProfile.silence= @"NO";
            
        }
        
        if (chosenCourse.vibrate) {
          
        }
        else
        {
            
          appDelegate.currentProfile.vibrate=@"NO";
        }
                NSError *error = nil;
        NSManagedObjectContext *context = appDelegate.managedObjectContext;
     //03-08-2017
        
//       [appDelegate insertPatientNew:[[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"] with:appDelegate.currentProfile];
        
        if (![context save:&error]) {
            
        }
        else
            
        {
            
            
        }
        
        
        [Notification SilenceNotification:appDelegate.currentProfile.id];
        
   
       
        //////////////////////////NSLog(@"%@", self.navigationItem.title);
        
        if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Add Profile",nil)]) {
            [parantIt showalert:@"New"];
            
        }
        
        else
        {
            
        }
        
        
        
        double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    
    [[self navigationController] popViewControllerAnimated:YES];
});
 
        
        
    }
   
//    [self googleAnalyticsGroupping];
    
    
}

-(void) backAction
{
    
    if ([self.navigationItem.title isEqualToString:NSLocalizedString(@"Add Profile",nil)]) {
       
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        NSManagedObjectContext *contex =appDelegate.managedObjectContext;
        
        [contex deleteObject:appDelegate.currentProfile];
        
        NSError *error = nil;
        NSManagedObjectContext *context = appDelegate.managedObjectContext;
        if (![context save:&error]) {
            
        }
        
    }
    else
        
    {
     
    }
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}

-(void)showalert

{
    
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle: NSLocalizedString(@"To start please click\n \"New User\" to create your\nprofile and start adding\nyour medication",nil)
     ];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464749];
    [alert addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [alert addButtonWithTitle:NSLocalizedString(@"New user",nil)];
    [alert show];
}

-(void)getParant:(UIViewController*)parant

{
    //////////////////////////NSLog(@"parant %@", parant);
    
    parantIt = (StartViewController*)parant;
    
    
    
    
}




-(void)Action:(ActionArrow*)sender
{
 
    if ([sender.titleit isEqualToString:NSLocalizedString(@"Name", nil)]) {
        
        // Name
        if ([Editor isEqualToString:@"edit"])
        {
            //********** Google Analytics ****************
            
            id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
            [tracker set:kGAIScreenName value:@"P_Edit Profile Name_iOS"];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
            
            //********** Google Analytics ****************
        }
        else
        {
            //********** Google Analytics ****************
            
            id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
            [tracker set:kGAIScreenName value:@"P_Add Profile Name_iOS"];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

            //********** Google Analytics ****************
        }
        
        NameViewController *controller = [[NameViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        
        [controller getParant:self];
        [controller setTextview:Nameit.text];
        
        
    }
    if ([sender.titleit isEqualToString:NSLocalizedString(@"Colour", nil)]) {
        
        //NSLocalizedString(@"Color:", nil)
        [self changeColorFridge];
    }
    if ([sender.titleit isEqualToString:NSLocalizedString(@"Alert", nil)]) {
        
        // Sound
        if ([Editor isEqualToString:@"edit"])
        {
            //********** Google Analytics ****************
            
            id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
            [tracker set:kGAIScreenName value:@"P_Edit Profile Sound_iOS"];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

            //********** Google Analytics ****************
        }
        else
        {
            //********** Google Analytics ****************
            
            id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
            [tracker set:kGAIScreenName value:@"P_Add Profile Sound_iOS"];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

            //********** Google Analytics ****************
        }
        
        AlertViewController *controller = [[AlertViewController alloc]init];
        
        [self.navigationController pushViewController:controller animated:YES];
        
        [controller whatLabel:Sound];
        [controller setItems:chosenCourse];
        
        
        
    }
    
    if ([sender.titleit isEqualToString:NSLocalizedString(@"Photo", nil)]) {
        
        // NSLocalizedString(@"Photo:", nil)
        [self makeAchoice];
    }
    
}

- (void)setSelectedColor:(UIColor*)colorit{
    
    
    
    if ([colorit isEqual:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]]) {
        
        colorit =[UIColor whiteColor];
        
    }

    
    
    [ColorField setBackgroundColor:colorit];
    
    
}


- (IBAction)DeleteWith:(UIButton *)sender {
    
    
    
    
}


- (IBAction)Delete:(UIButton *)sender {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([[SaveCore getMedicinewithProfileID:chosenCourse.id] count] ==0) {

        
        //NSLocalizedString(@"Are you sure you wish to delete this profile?", nil)
        [appDelegate ShowAlert:NSLocalizedString(@"￼Warning!",nil) Messageit:NSLocalizedString(@"Are you sure you wish to delete this profile?", nil) Custimize:NSLocalizedString(@"Delete", nil) parant:self];
        
    }
    else
    {
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate ShowAlert:NSLocalizedString(@"￼Warning!", nil) Messageit:NSLocalizedString(@"This profile contains medicine data. All data will be lost! Are you sure, you want to delete this profile?", nil) Custimize:NSLocalizedString(@"Delete", nil) parant:self];
        
    }
    
    
    
}


-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{
    

    
    if (index==1) {
        
        
        
    }
    
    else
    {
        
     
        if (chosenID) {
            
            
            
             AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
         
          
            [appDelegate removeMedicinePerAccount:chosenID];
            [appDelegate removeProfile:chosenID];
           
            
        }
        
        else
        {
            
        }
        
        double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    
     [[self navigationController] popViewControllerAnimated:YES];
});
       
        
        
        
    }
}




- (IBAction)OK:(UIButton *)sender {
    
    
    
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}
-(void)setitemsDictionary:(NSString*)set name:(NSString*)name

{
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{
    
}

//#pragma mark - Google Analytics
//
//-(void)googleAnalyticsGroupping
//{
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:1]value:@"Add Profile"];
//}


@end
