//
//  PDFrViewController.h
//  Medicijnen reminder
//
//  Created by Jeffrey Snijder on 15-06-12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ASIHTTPRequest, PDFScrollView;

@interface PDFrViewController : UIViewController <UIScrollViewDelegate>
@property (nonatomic, retain) IBOutlet UIScrollView *scrollViewSpread;
@property (nonatomic, retain) PDFScrollView *thumbPdfView;
@property (nonatomic, assign) CGPDFDocumentRef pdf1;
@property (nonatomic, assign) int pageCount;
-(void) turnbuttons:(NSString*) setter;
@end
