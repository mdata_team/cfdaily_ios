//
//  PDFrViewController.m
//  Medicijnen reminder
//
//  Created by Jeffrey Snijder on 15-06-12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PDFrViewController.h"
#import "ZoomingPDFViewerViewController.h"
#import "PDFScrollView.h"
#import <QuartzCore/QuartzCore.h>

@implementation PDFrViewController
@synthesize thumbPdfView;
@synthesize pdf1;
@synthesize pageCount;
@synthesize scrollViewSpread;

- (void)viewDidLoad
{
    [super viewDidLoad];



    NSLog(@"InstructiefilmViewController");


    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 87/1.9, 59/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



    UIImage *image = [UIImage imageNamed:@"Logo_bar.png"];
	UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0, 0,136, 34);

    self.navigationItem.titleView = imageView;


    scrollViewSpread = [[UIScrollView alloc] initWithFrame:CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height-44)];
    [scrollViewSpread setCanCancelContentTouches:NO];
    scrollViewSpread.showsHorizontalScrollIndicator = YES;
    [scrollViewSpread setBackgroundColor:[UIColor whiteColor]];
    scrollViewSpread.delaysContentTouches=YES;
    scrollViewSpread.clipsToBounds = NO;
    scrollViewSpread.scrollEnabled = YES;
    scrollViewSpread.pagingEnabled = YES;
    [scrollViewSpread setDelegate:self];
    scrollViewSpread.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.view  addSubview:scrollViewSpread];
    [scrollViewSpread setCenter:CGPointMake((self.view.frame.size.width)/2, (self.view.frame.size.height-44)/2)];


    pdf1 = CGPDFDocumentCreateWithURL((CFURLRef)CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("Bijsluiter.pdf"), NULL, NULL) );
    pageCount = CGPDFDocumentGetNumberOfPages(pdf1);


    for (int i=0 ; i<pageCount; i++) {



        thumbPdfView = [[PDFScrollView alloc] initWithFrame:CGRectMake((self.view.frame.size.width)*i, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
        [thumbPdfView setCanCancelContentTouches:NO];
        thumbPdfView.showsHorizontalScrollIndicator = YES;
        [thumbPdfView setBackgroundColor:[UIColor whiteColor]];
        thumbPdfView.delaysContentTouches=YES;
        thumbPdfView.clipsToBounds = NO;
        thumbPdfView.scrollEnabled = YES;
        thumbPdfView.pagingEnabled = YES;
        [thumbPdfView setDelegate:thumbPdfView];
        thumbPdfView.maximumZoomScale = 1*2;
        thumbPdfView.minimumZoomScale = 1;
        thumbPdfView.clipsToBounds = NO;
        [thumbPdfView setTag:i+200];
        thumbPdfView.indicatorStyle = UIScrollViewIndicatorStyleBlack;
        [scrollViewSpread  addSubview:thumbPdfView];



        NSURL *pdfURL = [[NSBundle mainBundle] URLForResource:@"Bijsluiter" withExtension:@"pdf"];

        CGPDFDocumentRef PDFDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)pdfURL);

        CGPDFPageRef PDFPage = CGPDFDocumentGetPage(PDFDocument, 1+i);
        [thumbPdfView setPDFPage:PDFPage];

        CGPDFDocumentRelease(PDFDocument);






    }

    [scrollViewSpread setContentSize:CGSizeMake(((self.view.frame.size.width)*pageCount), thumbPdfView.frame.size.height-44)];

 


}


-(void) backAction
{


    [[self navigationController] popViewControllerAnimated:YES];
}



-(void) turnbuttons:(NSString*) setter

{
    NSLog(@"turnbuttons %@", setter);




    if ([setter isEqualToString:@"UIInterfaceOrientationLandscapeLeft"]) {

        /*
         for (int i=0 ; i<pageCount; i++) {

         PDFScrollView *gone = (PDFScrollView *)[self.view viewWithTag:(i+200)];

         [gone turnbuttons:setter];
         [gone setFrame:CGRectMake(-80, self.view.frame.size.height*i, self.view.frame.size.height-20, self.view.frame.size.width)];
         [gone setTransform:CGAffineTransformMakeRotation(-M_PI / 2)];


         [gone setNeedsDisplay];
         [scrollViewSpread setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+(self.view.frame.size.height*i))];

         }


         */



    }
    else if ([setter isEqualToString:@"UIInterfaceOrientationLandscapeRight"]) {


        /*
         for (int i=0 ; i<pageCount; i++) {

         PDFScrollView *gone = (PDFScrollView *)[self.view viewWithTag:(i+200)];

         [gone turnbuttons:setter];

         [gone setFrame:CGRectMake(0, self.view.frame.size.width*i, self.view.frame.size.width, self.view.frame.size.height)];
         [gone setTransform:CGAffineTransformMakeRotation(-M_PI / 2)];


         [gone setNeedsDisplay];
         [scrollViewSpread setContentSize:CGSizeMake(self.view.frame.size.height, thumbPdfView.frame.size.width*i)];

         }


         */

    }
    else if ([setter isEqualToString:@"UIInterfaceOrientationPortraitUpsideDown"])
        {


        /*

         for (int i=0 ; i<pageCount; i++) {

         PDFScrollView *gone = (PDFScrollView *)[self.view viewWithTag:(i+200)];

         [gone setFrame:CGRectMake(0, self.view.frame.size.width*i, self.view.frame.size.width, self.view.frame.size.height)];
         [gone setTransform:CGAffineTransformMakeRotation(0)];

         [gone setNeedsDisplay];

         [scrollViewSpread setContentSize:CGSizeMake(thumbPdfView.frame.size.width*(i+1), thumbPdfView.frame.size.height)];

         }

         */



        }

    else if ([setter isEqualToString:@"UIInterfaceOrientationPortrait"])
        {


        /*


         for (int i=0 ; i<pageCount; i++) {

         PDFScrollView *gone = (PDFScrollView *)[self.view viewWithTag:(i+200)];
         
         [gone setFrame:CGRectMake(self.view.frame.size.width*i, 0, self.view.frame.size.width, self.view.frame.size.height)];
         [gone setTransform:CGAffineTransformMakeRotation(0)];
         
         [gone setNeedsDisplay];
         
         [scrollViewSpread setContentSize:CGSizeMake(self.view.frame.size.width*(i+1), self.view.frame.size.height)];
         
         }
         */
        
        
        }
    
    
    
    
}



@end
