//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CodePopView, NewMedecineController, TekstIDLabel, PerscriptionViewController;
@interface PerscriptionNotesViewController : UIViewController<UITextViewDelegate> {


    UITextView *TitleText;
   NSString *Compaire;
}
@property (nonatomic, retain) PerscriptionViewController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;

-(void) setTextview: (NSString *) text;
-(void)whatLabel:(UILabel*)set;
-(void)getParant:(UIViewController*)parant;
@end

