//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class CodePopView, NewMedecineController, TekstIDLabel;
@interface PerscriptionViewController : UIViewController<UITextViewDelegate, MPMediaPickerControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationBarDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate> {


    UITextView *TitleText;
   NSString *Compaire;
    UIView *Combi;
    UIView *Combitwo;
     UIView *CombiOther;
    NSString *chosen;
      NSString *chosenID;
    UIImage *PerscriptionImage;
}

@property (nonatomic, retain) UIView *Combitwo;
@property (nonatomic, retain) UIImage *PerscriptionImage;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) IBOutlet UIImageView *headShot;
@property (nonatomic, retain) TekstIDLabel *Name;
@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;

-(void)selectExitingPicture;
-(void)setChoice:(NSString*)gothere;
-(void)change:(UIButton*)sender;
-(void) setTextview: (NSString *) text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
@end

