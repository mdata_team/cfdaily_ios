//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "PerscriptionViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "ImageViewController.h"
#import "PerscriptionNotesViewController.h"
#import "ActionArrow.h"

@interface PerscriptionViewController ()

@end

@implementation PerscriptionViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize headShot;
@synthesize Name;
@synthesize PerscriptionImage;
@synthesize Combitwo;


-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    

    parantLabel =(TekstIDLabel*)set;
    
    
    
     [TitleText setText:set.text];
    
    
    
}



- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];

    
     self.title =NSLocalizedString(@"Prescription Notes",nil);

    // this will appear as the title in the navigation bar
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:13];
    label.textAlignment = NSTextAlignmentCenter;
        [label setNumberOfLines:2];
    // ^-Use NSTextAlignmentCenter for older SDKs.
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
      [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
    label.text = self.title;
    [label sizeToFit];

       NSArray *names1 =[NSArray arrayWithObjects:NSLocalizedString(@" Add Prescription Notes",nil),NSLocalizedString(@" Add from Camera",nil), nil];
  

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];




//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



       for (NSInteger i=0 ; i<1; i++) {


            UIView *Combi1 = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 40)];
            [Combi1 setBackgroundColor:[UIColor whiteColor]];
            [Combi1.layer setCornerRadius:10];
            [Combi1.layer setBorderWidth:1.5];
//            [Combi1.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
           [Combi1.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
            [Combi1.layer setMasksToBounds:YES];
            [self.view addSubview:Combi1];
          
           
            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, (40*i), self.view.frame.size.width-20, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
           [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:15]];
            [MedName setText:[names1 objectAtIndex:i]];
            [MedName setTag:180+i];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Combi1 addSubview:MedName];



            ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, (40*i), self.view.frame.size.width-20, 40)];
           [NameButton setTitleit:[names1 objectAtIndex:i]];
            [NameButton setTag:140+i];
            [NameButton.layer setBorderWidth:2];
//            [NameButton.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
           [NameButton.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
            [NameButton addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
            [NameButton setBackgroundColor:[UIColor clearColor]];
            [Combi1 addSubview:NameButton];


            

            UIButton *NameButton2 = [[UIButton alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3+(40*i), 60/2, 72/2)];
            [NameButton2 setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
            [NameButton2 setTitle:[names1 objectAtIndex:i] forState:UIControlStateNormal];
            [NameButton2 setTag:140];
            [NameButton2 addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
            [Combi1 addSubview:NameButton2];
           
           
           if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
               
               
               //NSLog(@"left");
               Combi1.transform = CGAffineTransformMakeScale(-1, 1);
               
               
               
               for (UILabel *label in Combi1.subviews) {
                   
                   
                   if ([label isKindOfClass:[UILabel class]]) {
                       
                       label.transform = CGAffineTransformMakeScale(-1, 1);
                       [label setTextAlignment:NSTextAlignmentNatural];
                       
                   }
                   
               }
               
             
               
               for (TekstIDLabel *label in Combi1.subviews) {
                   
                   
                   if ([label isKindOfClass:[TekstIDLabel class]]) {
                       
                       label.transform = CGAffineTransformMakeScale(-1, 1);
                       [label setTextAlignment:NSTextAlignmentNatural];
                       
                   }
                   
               }
               
               
               //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
               
               
           } else {
               
               //NSLog(@"right");
               //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
           }

                    }
    
    
   for (NSInteger i=0 ; i<1; i++) {
        
        
        UIView *Combi1 = [[UIView alloc] initWithFrame:CGRectMake(10, 60, self.view.frame.size.width-20, 120)];
        [Combi1 setBackgroundColor:[UIColor whiteColor]];
        [Combi1.layer setCornerRadius:10];
        [Combi1.layer setBorderWidth:1.5];
//        [Combi1.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
       [Combi1.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [Combi1.layer setMasksToBounds:YES];
        [self.view addSubview:Combi1];
      
            
            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, (40*i), self.view.frame.size.width-20, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
       [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:15]];
            [MedName setText:[names1 objectAtIndex:1]];
            [MedName setTag:180+i];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Combi1 addSubview:MedName];
            
            
            headShot = [[UIImageView alloc] initWithFrame:CGRectMake(200, 20, 80, 80)];
            [headShot setBackgroundColor:[UIColor grayColor]];
            [headShot setImage:[UIImage imageNamed:@"Prescription.png"]];
            [headShot setTag:130];
            [Combi1 addSubview:headShot];
        
        
        
        if ([[parantIt.ChosenMedicen valueForKey:@"PerscriptionImage"] length]>6) {
            [headShot setImage:[UIImage imageWithContentsOfFile:[parantIt.ChosenMedicen valueForKey:@"PerscriptionImage"]]];
            
            
        }
            
            
//            ActionArrow *photo = [[ActionArrow alloc] initWithFrame:CGRectMake(155, 72,39*0.8, 33*0.8)];
       ActionArrow *photo = [[ActionArrow alloc] initWithFrame:CGRectMake(155, 65,39, 40)];
            [photo setBackgroundColor:[UIColor clearColor]];
            [photo setShowsTouchWhenHighlighted:NO];
//            [photo setImage:[UIImage imageNamed:@"photo.png"] forState:UIControlStateNormal]; //29-06-2017 changes
       [photo setImage:[UIImage imageNamed:@"photo"] forState:UIControlStateNormal];
//            [photo setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
            [photo setAutoresizesSubviews:YES];
            [photo addTarget:self action:@selector(makeAchoice) forControlEvents:UIControlEventTouchUpInside];
            [photo setTag:34];
            [Combi1 addSubview:photo];
          [photo.layer setCornerRadius:4];
            
        
            
            UIButton *Cover = [[UIButton alloc] initWithFrame:CGRectMake(200, 50, 80, 80)];
            [Cover setBackgroundColor:[UIColor clearColor]];
            [Cover setShowsTouchWhenHighlighted:NO];
            [Cover addTarget:self action:@selector(Getimage:) forControlEvents:UIControlEventTouchUpInside];
            [Cover setTag:34];
            [Combi1 addSubview:Cover];
       
       if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
           
           
           //NSLog(@"left");
           Combi1.transform = CGAffineTransformMakeScale(-1, 1);
           
             photo.transform = CGAffineTransformMakeScale(-1, 1);
           
             headShot.transform = CGAffineTransformMakeScale(-1, 1);
           
           for (UILabel *label in Combi1.subviews) {
               
               
               if ([label isKindOfClass:[UILabel class]]) {
                   
                   label.transform = CGAffineTransformMakeScale(-1, 1);
                   [label setTextAlignment:NSTextAlignmentNatural];
                   
               }
               
           }
           
           
           for (UIButton *label in Combi1.subviews) {
               
               
               if ([label isKindOfClass:[UIButton class]]) {
                   
                   label.transform = CGAffineTransformMakeScale(-1, 1);
                   [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                   
               }
               
           }
           
           
           for (TekstIDLabel *label in Combi1.subviews) {
               
               
               if ([label isKindOfClass:[TekstIDLabel class]]) {
                   
                   label.transform = CGAffineTransformMakeScale(-1, 1);
                   [label setTextAlignment:NSTextAlignmentNatural];
                   
               }
               
           }
           
           
           //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
           
           
       } else {
           
           //NSLog(@"right");
           //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
       }

       
       
    }


  
    
    
    Combitwo = [[UIView alloc] initWithFrame:CGRectMake(10, 190, self.view.frame.size.width-20, 180)];
    [Combitwo setBackgroundColor:[UIColor whiteColor]];
    [Combitwo.layer setCornerRadius:10];
    [Combitwo.layer setBorderWidth:1.5];
//    [Combitwo.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Combitwo.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combitwo.layer setMasksToBounds:YES];
    [self.view addSubview:Combitwo];
    
    
    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-30, 180-40)];
    [TitleText setBackgroundColor:[UIColor clearColor]];
    [TitleText setDelegate:self];
    [TitleText setText:@""];
    [TitleText setTextColor:[UIColor blackColor]];
    [TitleText setTag:123+1];
    [TitleText setEditable:NO];
    
    [TitleText setFont:[UIFont systemFontOfSize:15]];
    [Combitwo addSubview:TitleText];
    
    [Combitwo setAlpha:0];
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        //NSLog(@"left");
        Combitwo.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        
        for (UILabel *label in Combitwo.subviews) {
            
            
            if ([label isKindOfClass:[UILabel class]]) {
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        
        
        for (UIButton *label in Combitwo.subviews) {
            
            
            if ([label isKindOfClass:[UIButton class]]) {
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        
        
        for (UITextView *label in Combitwo.subviews) {
            
            
            if ([label isKindOfClass:[UITextView class]]) {
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
                [label setTextAlignment:NSTextAlignmentNatural];
                
            }
            
        }
        
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }
    
}

-(void)Getimage:(UIButton*)sender
{

    ImageViewController *controller = [[ImageViewController alloc]init];
    [self presentViewController:controller animated:YES completion:Nil];
    
    if (headShot.image) {
       [controller getStamp:PerscriptionImage tegit:1];
    }
    else{
        
        
    }
   

}

-(void)change:(ActionArrow*)sender

{
    NSArray *names1 =[NSArray arrayWithObjects:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Choose from Contacts",nil)],[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Add New Contacts",nil)], nil];

    
      for (NSInteger i=0 ; i<[names1 count]; i++) {

           UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
           [setimage setAlpha:0];
           

       }


    UIImageView *setimage = (UIImageView *)[self.view viewWithTag:sender.tag+20];
    [setimage setAlpha:1];

    
  

    chosen = sender.titleit;

  
    
}

-(void)Action:(UIButton*)sender

{

    PerscriptionNotesViewController *controller = [[PerscriptionNotesViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];

}

-(void)changeChose:(NSString*)sender

{


   
     chosen = sender;
    
    
}

-(void) viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [TitleText setText:appDelegate.Notes];
    
    
    if ([appDelegate.Notes length]>3) {
        
        [Combitwo setAlpha:1];
    }
    else{
         [Combitwo setAlpha:0];
    }



}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];


        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {




//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
       
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{
}



-(void)getParant:(UIViewController*)parant

{

    parantIt =(NewMedecineController*)parant;

    
    if ([UIImage imageWithContentsOfFile:[parantIt.ChosenMedicen valueForKey:@"PerscriptionImage"]]) {
           [headShot setImage:[UIImage imageWithContentsOfFile:[parantIt.ChosenMedicen valueForKey:@"PerscriptionImage"]]];
        
        
        PerscriptionImage = [UIImage imageWithContentsOfFile:[parantIt.ChosenMedicen valueForKey:@"PerscriptionImage"]];
    }
    else
    {
        [headShot setImage:[UIImage imageNamed:@"Prescription.png"]];
    }




  
    
}


-(void)setChoice:(NSString*)gothere

{


}

- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{

    MPMediaItem *anItem = (MPMediaItem *)[mediaItemCollection.items objectAtIndex:0];

    NSURL *assetURL = [anItem valueForProperty: MPMediaItemPropertyAssetURL];


    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);

    NSString *documentsDir = [NSString stringWithFormat:@"/Caches/%@",[paths2 objectAtIndex:0]];





    NSData *data2 = [NSData dataWithContentsOfURL:assetURL];

    [data2 writeToFile:[NSString stringWithFormat:@"%@/Copy.mp3", documentsDir] atomically:YES];

    [self dismissViewControllerAnimated:YES completion:NULL];


}


-(void)makeAchoice
{
    
    NSString *strLibrary = @"";
    
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {
        strLibrary = NSLocalizedString(@"Library", nil);
    }
    else
    {
        strLibrary = NSLocalizedString(@"Librar", nil);
    }

    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"",nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:NSLocalizedString(@"Cancel", nil)
                                              /*otherButtonTitles:NSLocalizedString(@"Library",nil)*/
                                              otherButtonTitles:strLibrary, NSLocalizedString(@"Camera",nil),nil];
    sheet.delegate = self;
    sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    
    [sheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex ==1) {
        
        [self selectExitingPicture];
    }
    
    if (buttonIndex==2) {
        
        [self getCameraPicture:NULL];
    }
    
    
}



-(void)selectExitingPicture
{
    
    //////////////NSLog(@"selectExitingPicture");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    
    
    
	if([UIImagePickerController isSourceTypeAvailable:
        UIImagePickerControllerSourceTypePhotoLibrary])
	{
		UIImagePickerController *picker= [[UIImagePickerController alloc]init];
		picker.delegate = self;
       picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
		[self presentViewController:picker animated:YES  completion:NULL];
	
	}
	
    
}


-(void)getCameraPicture:(UIButton*)sender
{

     //////////////NSLog(@"getCameraPicture");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate.toolbarDown setAlpha:0];


    
	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.allowsEditing = YES;
#if (TARGET_IPHONE_SIMULATOR)
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
#endif
	[self presentViewController: picker animated:YES completion:NULL];
}


-(void)imagePickerController:(UIImagePickerController *)picker
      didFinishPickingImage : (UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{



    PerscriptionImage = image;

    [headShot setImage:[self thumbWithSideOfLength2:120 image:image]];

    if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        
    }
    else
    {
       UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
        
    }


    [self dismissViewControllerAnimated:YES completion:NULL];

}


- (UIImage *)thumbWithSideOfLength2:(float)length image:(UIImage*)mainImage {

    UIImage *thumbnail;

    if (mainImage.size.width< length) {

        CGSize itemSiz1 = CGSizeMake(mainImage.size.width*(length/mainImage.size.width), mainImage.size.height*(length/mainImage.size.width));

        UIGraphicsBeginImageContextWithOptions(itemSiz1, NO, 0.0);

        CGRect imageRect2 = CGRectMake(0.0, 0.0, itemSiz1.width, itemSiz1.height);
        [mainImage drawInRect:imageRect2];

        mainImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }

    UIImageView *mainImageView = [[UIImageView alloc] initWithImage:mainImage];
    BOOL widthGreaterThanHeight = (mainImage.size.width > mainImage.size.height);
    float sideFull = (widthGreaterThanHeight) ? mainImage.size.height : mainImage.size.width;
    CGRect clippedRect = CGRectMake(0, 0, sideFull, sideFull);

    UIGraphicsBeginImageContext(CGSizeMake(length, length));
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextClipToRect( currentContext, clippedRect);
    CGFloat scaleFactor = length/sideFull;
    if (widthGreaterThanHeight) {

        CGContextTranslateCTM(currentContext, -((mainImage.size.width-sideFull)/2)*scaleFactor, 0);

    }
    else {
        CGContextTranslateCTM(currentContext, 0, -((mainImage.size.height - sideFull) / 2) * scaleFactor);
    }

    CGContextScaleCTM(currentContext, scaleFactor, scaleFactor);
    [mainImageView.layer renderInContext:currentContext];
    thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return thumbnail;
    
}


- (IBAction)doSomething:(UIButton *)sender {


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    [[self navigationController] popViewControllerAnimated:YES];

double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){


    if (PerscriptionImage) {
        
        NSData *data = [NSData dataWithData:UIImagePNGRepresentation(PerscriptionImage)];
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Perscription_%@.png", appDelegate.CurrentID]];
        
        
        [data writeToFile:jpegFilePath atomically:YES];
        
        [parantIt.ChosenMedicen setObject:jpegFilePath forKey:@"PerscriptionImage"];
        
        
    }
    else
    {
        
    }

  
    
        if (appDelegate.Notes ==NULL) {
        }
    else
    {

     [parantIt.ChosenMedicen setObject:appDelegate.Notes forKey:@"Prescription"];
        [parantLabel setText:appDelegate.Notes];
        
            
        }
    
});

}

- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
