//
//  VVSCell.h
//  VVS
//
//  Created by Jeffrey Snijder on 05-12-12.
//  Copyright (c) 2012 Kieran Lafferty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StartViewController.h"
#import "StartViewController.h"
#import "ProfileCell.h"


@interface ProfileCell : UITableViewCell

@property (nonatomic, retain) IBOutlet  UIViewController *lostView;
@property (nonatomic, retain) IBOutlet UIView *ProfileView;
@property (nonatomic, retain) IBOutlet UIImageView *headShot;
@property (nonatomic, retain) IBOutlet NSString *IDIt;
@property (nonatomic, retain) IBOutlet UILabel *NameLabel;
-(void) FillAllItems:(Profiles*)set;
-(void)getparent:(UIViewController*)set;
@end
