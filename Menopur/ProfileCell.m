//
//  VVSCell.m
//  VVS
//
//  Created by Jeffrey Snijder on 05-12-12.
//  Copyright (c) 2012 Kieran Lafferty. All rights reserved.
//

#import "ProfileCell.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "actionButton.h"
#import "ActionArrow.h"
@implementation ProfileCell

@synthesize ProfileView;
@synthesize NameLabel;
@synthesize headShot;
@synthesize IDIt;
@synthesize lostView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
       ProfileView = [[UIView alloc] initWithFrame:CGRectMake(-5, 0.0f, 330, 120)];
        [self addSubview:ProfileView];


       

        headShot = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 80, 80)];
//        [headShot setBackgroundColor:[UIColor blackColor]];
         [headShot setTag:130];
        [headShot setImage:[UIImage imageNamed:@"headshot"]];
        [ProfileView addSubview:headShot];


        

        NameLabel = [[UILabel alloc] initWithFrame:CGRectMake(115, 20, 190, 20)];
//        [NameLabel setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //29-06-2017 changes
        [NameLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [NameLabel setFont:[UIFont boldSystemFontOfSize:16]];
          [NameLabel setTag:120];
        [NameLabel setBackgroundColor:[UIColor clearColor]];
        [NameLabel setNumberOfLines:3];
        [ProfileView addSubview:NameLabel];

        [self addSubview:ProfileView];


        actionButton *info2 = [[actionButton alloc]  initWithFrame:CGRectMake(285, 40, 60/2, 72/2)];
        [info2 setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [info2 setTag:148];
        [ProfileView addSubview:info2];
    
        
//        actionButton *info = [[actionButton alloc]  initWithFrame:CGRectMake(0, 0, 320, 100)]; // 25-10-2017
        actionButton *info = [[actionButton alloc]  initWithFrame:CGRectMake(285, 40, 60/2, 72/2)];
        [info setBackgroundColor:[UIColor clearColor]];
        [info setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        [info setTag:140];
        [ProfileView addSubview:info];

        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
            
            //NSLog(@"left");
            
            self.transform = CGAffineTransformMakeScale(-1, 1);
            
            
            for (UIView *content in self.subviews) {
                
                
                for (UILabel *label in content.subviews) {
                    
                    
                    if ([label isKindOfClass:[UILabel class]]) {
                        
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                
                for (UIButton *label in content.subviews) {
                    
                    
                    if ([label isKindOfClass:[UIButton class]]) {
                        
                        label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                        [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                
                
                
                
            }
            
            
        }
        
        else
        {
            
        }
        

       
    }
    return self;
}


-(void)getparent:(UIViewController*)set
{

    actionButton *button = (actionButton *)[self viewWithTag:(140)];

    lostView =set;
    [button addTarget:lostView action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
}


-(void) FillAllItems:(Profiles*)set
{
    
    UIColor *colorit = [UIColor colorWithRed:[set.colorRed floatValue] green:[set.colorGreen floatValue] blue:[set.colorblue floatValue] alpha:1];
    
    


    IDIt = set.id;
    NameLabel = (UILabel *)[self viewWithTag:(120)];
    [NameLabel setText:set.name];



     headShot = (UIImageView *)[self viewWithTag:(130)];
    [headShot setImage:[UIImage imageWithData:set.image]];


    actionButton *button = (actionButton *)[self viewWithTag:(140)];
    [button setChosenCourse:set];
    [button setTitle:set.id forState:UIControlStateNormal];
    

    [ProfileView setBackgroundColor:colorit];

    
 
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
