//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "ProfileViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "SaveCore.h"
#import "ActionArrow.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController
@synthesize parantIt;

@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;



-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    

    parantLabel =(TekstIDLabel*)set;
    
     for (NSInteger i=0 ; i<[Vraagantwoord count]; i++) {
          
          //
          
          if ([[[Vraagantwoord valueForKey:@"Name"] objectAtIndex:i] isEqualToString:set.text]) {
              
              UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
              [setimage setAlpha:1];
              
              chosen = set.text;
              
              chosenID = [[Vraagantwoord valueForKey:@"ID"] objectAtIndex:i];
              
              [appDelegate.Profile addObject:[[Vraagantwoord valueForKey:@"ID"] objectAtIndex:i]];
              
              
          }
          else
          {
              UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
              [setimage setAlpha:0];
          }
          

          
      }
    
}



- (void)viewDidLoad {
    
    
    NSLog(@"%@", self);

         [self.view.layer setMasksToBounds:YES];

       NSArray *profileItem =[NSArray arrayWithObjects:@"Name",@"ID",@"Image",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Silence",@"Vibrate",nil];


    Vraagantwoord = [[NSMutableArray alloc] init];
    Vraagantwoord= [SaveCore getProfiles:profileItem];
    

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];




//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;


        Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 40*[Vraagantwoord count])];
        [Combi setBackgroundColor:[UIColor whiteColor]];
        [Combi.layer setCornerRadius:10];
        [Combi.layer setBorderWidth:2];
//        [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];

        [Combi.layer setMasksToBounds:YES];
        [self.view addSubview:Combi];


       for (NSInteger i=0 ; i<[Vraagantwoord count]; i++) {


            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, (40*i), self.view.frame.size.width-20, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
           [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];

            [MedName setFont:[UIFont boldSystemFontOfSize:15]];
            [MedName setText:[[Vraagantwoord valueForKey:@"Name"] objectAtIndex:i]];
            [MedName setTag:180+i];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Combi addSubview:MedName];



            ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, (40*i), self.view.frame.size.width-20, 40)];
            [NameButton setTitleit:[[Vraagantwoord valueForKey:@"Name"] objectAtIndex:i]];
            [NameButton setTag:140+i];
            [NameButton.layer setBorderWidth:1];
//            [NameButton.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
           [NameButton.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
            [NameButton addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
            [NameButton setBackgroundColor:[UIColor clearColor]];
            [Combi addSubview:NameButton];


            
            UIImageView *background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Vink.png"]];
            [background setFrame:CGRectMake(self.view.frame.size.width-50,  10+(40*i), 20, 20)];
            [background setBackgroundColor:[UIColor clearColor]];
            [Combi addSubview:background];
            [background setTag:160+i];
               [background setAlpha:0];
            
           if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
               
               
               //NSLog(@"left");
               Combi.transform = CGAffineTransformMakeScale(-1, 1);
               
               
               MedName.transform = CGAffineTransformMakeScale(-1, 1);
               [MedName setTextAlignment:NSTextAlignmentNatural];
               
               
           }


        }
    
    
  
    
}


-(void)change:(ActionArrow*)sender

{
    
   
  
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    UIImageView *setimage = (UIImageView *)[self.view viewWithTag:sender.tag+20];
    [setimage setAlpha:1];
    
    [appDelegate.Profile removeAllObjects];
    
    
  [appDelegate.Profile addObject:[[Vraagantwoord valueForKey:@"ID"] objectAtIndex:[[Vraagantwoord valueForKey:@"Name"] indexOfObject:sender.titleit]]];
    

    chosen = sender.titleit;

     chosenID = [[Vraagantwoord valueForKey:@"ID"] objectAtIndex:[[Vraagantwoord valueForKey:@"Name"] indexOfObject:sender.titleit]];
   
         for (NSInteger i=0 ; i<[Vraagantwoord count]; i++) {
    
    if ([[[Vraagantwoord valueForKey:@"Name"] objectAtIndex:i] isEqualToString:sender.titleLabel.text]) {
        
        UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
        [setimage setAlpha:1];
        
     
        
    }
    else
    {
        UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
        [setimage setAlpha:0];
    }
    
              
          }
  
    
}

-(void)Action:(UIButton*)sender

{

  
}

-(void)changeChose:(NSString*)sender

{


   
     chosen = sender;
    
    
}

-(void) viewDidAppear:(BOOL)animated
{


}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{

}


-(void)getParant:(UIViewController*)parant

{

    parantIt =(NewMedecineController*)parant;




  
    
}


-(void)setChoice:(NSString*)gothere

{
  
/*

    NSArray *names1 =[NSArray arrayWithObjects:@"Drops",@"Inhaler", nil];

   
   for (NSInteger i=0 ; i<[names1 count]; i++) {

        

        UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
        [setimage setAlpha:0];


        if ([gothere isEqualToString:[names1 objectAtIndex:i]]) {




        }
        else
            {
            
            chosen=gothere;
            
            }

           if (parantIt) {

        if ([ [parantIt.MedicineSpecs valueForKey:gothere] isEqualToString:[names1 objectAtIndex:i]]) {

         

                UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
                [setimage setAlpha:1];

                UILabel *setimage2 = (UILabel *)[self.view viewWithTag:180+i];

                chosen = setimage2.text;
            
        }

           }
        
    }
   
 */

}




- (IBAction)doSomething:(UIButton *)sender {



    [[self navigationController] popViewControllerAnimated:YES];


    if ([parantLabel.title isEqualToString:chosen]) {

    }
    else
        {

            
        //[parantLabel setText:[[Vraagantwoord valueForKey:@"Name"] objectAtIndex:[[Vraagantwoord valueForKey:@"ID"] indexOfObject:chosenID]]];
        //[parantIt setitemsDictionary:chosenID name:@"ID"];
        
        //[parantIt setitemsDictionary:parantLabel.title name:@"Profile"];

        }


}

- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
