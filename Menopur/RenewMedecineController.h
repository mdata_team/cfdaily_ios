//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeveyPopColorView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "BufferedNavigationController.h"

@class CodePopView, LeveyPopColorView, ViewController;
@interface RenewMedecineController : UIViewController<UITextViewDelegate, UIAlertViewDelegate, MPMediaPickerControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, retain) ViewController *parantIt;
@property (nonatomic, retain) UIImageView *headShot;
@property (nonatomic, retain) MPMusicPlayerController *musicPlayer;
@property (nonatomic, assign) CGFloat rood;
@property (nonatomic, assign) CGFloat groen;
@property (nonatomic, assign) CGFloat blauw;
@property (nonatomic, assign) CGFloat doorzicht;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbarDown;
@property (nonatomic, retain) LeveyPopColorView *color;
@property (nonatomic, retain) UILabel *Nameit;
@property (nonatomic, retain) UILabel *Sound;
@property (nonatomic, retain) UILabel *ColorField;
@property (nonatomic, retain) NSString *chosenID;
@property (nonatomic, retain) NSMutableDictionary *ChosenMedicen;

- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)setItems:(NSMutableDictionary*)set;
-(void)setItemsNone:(NSMutableDictionary*)set;
-(void)setNew;
-(void)getParant:(ViewController*)parant;
@end
