//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//
#import "NewMedecineController.h"
#import "RenewMedecineController.h"
#import <QuartzCore/QuartzCore.h>

#import "AlertViewController.h"
#import "NameViewController.h"
#import "SaveData.h"
#import "GetData.h"
#import "AppDelegate.h"
#import "TekstIDLabel.h"

#import "FrequencyViewController.h"
#import "PerscriptionViewController.h"
#import "DocterViewController.h"
#import "ProfileViewController.h"
#import "TypeViewController.h"
#import "StrengthViewController.h"
#import "NameItemViewController.h"
#import "RemainingViewController.h"
#import "FillingViewController.h"


@interface NewMedecineController ()

@end

@implementation RenewMedecineController
@synthesize toolbarDown;
@synthesize color;
@synthesize rood;
@synthesize groen;
@synthesize blauw;
@synthesize doorzicht;
@synthesize musicPlayer;
@synthesize headShot;
@synthesize Nameit;
@synthesize Sound;
@synthesize ColorField;
@synthesize chosenID;
@synthesize parantIt;
@synthesize ChosenMedicen;



- (void)viewDidLoad
{
    


 


        //Month_view.png

    ChosenMedicen =[[NSMutableDictionary alloc]init];


    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;

    

    musicPlayer = [MPMusicPlayerController iPodMusicPlayer];



	[musicPlayer beginGeneratingPlaybackNotifications];


    UIScrollView *scrollViewSpread = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
    [scrollViewSpread setCanCancelContentTouches:NO];
    scrollViewSpread.showsHorizontalScrollIndicator = YES;
    [scrollViewSpread setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


    scrollViewSpread.delaysContentTouches=YES;
    scrollViewSpread.clipsToBounds = NO;
    scrollViewSpread.scrollEnabled = YES;
    scrollViewSpread.pagingEnabled = YES;
    [scrollViewSpread setDelegate:self];
    scrollViewSpread.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.view  addSubview:scrollViewSpread];
    

    UIView *Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 220)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:5];
    [Combi.layer setBorderWidth:1.5];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Combi];


    NSArray *names1 =[NSArray arrayWithObjects:@"   Med Name:", @"   Med Type:",@"   Strength:", nil];
    NSArray *Cell1 =[NSArray arrayWithObjects:@"Name",  @"Type", @"Strength", nil];
    
      for (int i=0 ; i<[names1 count]; i++) {
          
    TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
    [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
    [MedName setFont:[UIFont boldSystemFontOfSize:14]];
    [MedName setText:[names1 objectAtIndex:i]];
    [MedName setTag:120];
    [MedName.layer setBorderWidth:1];
    [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [MedName setBackgroundColor:[UIColor clearColor]];
    [MedName setNumberOfLines:3];
    [Combi addSubview:MedName];


          




    TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([MedName.text length]*8, 39*i, self.view.frame.size.width-140, 40)];
    [Name setTextColor:[UIColor blackColor]];
    [Name setFont:[UIFont systemFontOfSize:14]];
    [Name setText:@""];
    [Name setTitle:[Cell1 objectAtIndex:i]];
    [Name setTag:120];
    [Name setBackgroundColor:[UIColor clearColor]];
    [Name setNumberOfLines:3];
    [Combi addSubview:Name];

    UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
    [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
    [NameButton setTitle:[Cell1 objectAtIndex:i] forState:UIControlStateNormal];
    [NameButton setTag:140];
    [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:NameButton];


          


      }


    headShot = [[UIImageView alloc] initWithFrame:CGRectMake(10, 130, 80, 80)];
    [headShot setBackgroundColor:[UIColor blackColor]];
    [headShot setTag:130];
        //[headShot setImage:[UIImage imageNamed:@"headshot.png"]];
    [Combi addSubview:headShot];


    UIButton *Photo = [[UIButton alloc]  initWithFrame:CGRectMake(100, 180, 39, 33)];
    [Photo setImage:[UIImage imageNamed:@"photo.png"] forState:UIControlStateNormal];
    [Photo setTitle:@"Photo" forState:UIControlStateNormal];
    [Photo setTag:160];
    [Photo addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:Photo];



    UIView *Assign = [[UIView alloc] initWithFrame:CGRectMake(10, 240, self.view.frame.size.width-20, 40)];
    [Assign setBackgroundColor:[UIColor whiteColor]];
    [Assign.layer setCornerRadius:5];
    [Assign.layer setBorderWidth:1.5];
    [Assign.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Assign.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Assign];


    NSArray *names2 =[NSArray arrayWithObjects:@"   Assign to Profile:", nil];
      NSArray *Cell2 =[NSArray arrayWithObjects:@"Profile", nil];

    for (int i=0 ; i<[names2 count]; i++) {

        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names2 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Assign addSubview:MedName];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([MedName.text length]*8, 39*i, self.view.frame.size.width-130, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTitle:[Cell1 objectAtIndex:i]];

        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Assign addSubview:Name];

        
        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:[Cell1 objectAtIndex:i] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Assign addSubview:NameButton];

        
        
    }



    UIView *Schdules = [[UIView alloc] initWithFrame:CGRectMake(10, 290, self.view.frame.size.width-20, 79)];
    [Schdules setBackgroundColor:[UIColor whiteColor]];
    [Schdules.layer setCornerRadius:5];
    [Schdules.layer setBorderWidth:1.5];
    [Schdules.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Schdules.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Schdules];


    NSArray *names3 =[NSArray arrayWithObjects:@"   Schedule:", @"   Frequency:", nil];
     NSArray *Cell3 =[NSArray arrayWithObjects:@"Schedule", @"Frequency", nil];

    for (int i=0 ; i<[names3 count]; i++) {

        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names3 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Schdules addSubview:MedName];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([MedName.text length]*8, 39*i, self.view.frame.size.width-100, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTitle:[Cell3 objectAtIndex:i]];

        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Schdules addSubview:Name];


        if (i>0) {
        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:[Cell3 objectAtIndex:i] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Schdules addSubview:NameButton];

        }
    else
        {
        UISwitch *swActive = [[UISwitch alloc] initWithFrame:CGRectMake(215, 8+(39*i), 100,50)];
        [swActive setTag:100+i];
        [swActive setOnTintColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000]];
            //sukker ben ik!!
        [swActive addTarget:self action:@selector(switchValueReminder:) forControlEvents:UIControlEventValueChanged];
        [Schdules addSubview:swActive];
        }



    }



    UIView *Quantity = [[UIView alloc] initWithFrame:CGRectMake(10, 380, self.view.frame.size.width-20, 118)];
    [Quantity setBackgroundColor:[UIColor whiteColor]];
    [Quantity.layer setCornerRadius:5];
    [Quantity.layer setBorderWidth:1.5];
    [Quantity.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Quantity.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Quantity];



        
    NSArray *names4 =[NSArray arrayWithObjects:@"   Quantity per Dose:", @"   Duration:",@"   Instructions:", nil];

     NSArray *Cell4 =[NSArray arrayWithObjects:@"Quantity", @"Duration", @"Instructions", nil];
    
    for (int i=0 ; i<[names4 count]; i++) {

        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names4 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Quantity addSubview:MedName];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([MedName.text length]*8, 39*i, self.view.frame.size.width-150, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTitle:[Cell4 objectAtIndex:i]];

        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Quantity addSubview:Name];

        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:[Cell4 objectAtIndex:i] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Quantity addSubview:NameButton];
        
        
    }
    



    UIView *Meds = [[UIView alloc] initWithFrame:CGRectMake(10, 510, self.view.frame.size.width-20, 118)];
    [Meds setBackgroundColor:[UIColor whiteColor]];
    [Meds.layer setCornerRadius:5];
    [Meds.layer setBorderWidth:1.5];
    [Meds.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Meds.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Meds];

    
    NSArray *names5 =[NSArray arrayWithObjects:@"   Med Quantity Remaining:", @"   Med Quantity per Filling:",@"   Alert Quantity for Refilling:", nil];

     NSArray *Cell5 =[NSArray arrayWithObjects:@"Remaining", @"Filling", @"Refilling", nil];

    for (int i=0 ; i<[names5 count]; i++) {

        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names5 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Meds addSubview:MedName];

        if (i<2) {


              TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([MedName.text length]*8, 39*i, self.view.frame.size.width-200, 40)];
              [Name setTextColor:[UIColor blackColor]];
              [Name setFont:[UIFont systemFontOfSize:14]];
              [Name setText:@""];
              [Name setTitle:[Cell5 objectAtIndex:i]];

              [Name setTag:120];
              [Name setBackgroundColor:[UIColor clearColor]];
              [Name setNumberOfLines:3];
              [Meds addSubview:Name];
              
        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:[Cell5 objectAtIndex:i] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Meds addSubview:NameButton];

          }
          else
              {
              UISwitch *swActive = [[UISwitch alloc] initWithFrame:CGRectMake(215, 8+(39*i), 100,50)];
              [swActive setTag:100+i];
                  //sukker ben ik!!
               [swActive setOnTintColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000]];
              [swActive addTarget:self action:@selector(switchValueReminder:) forControlEvents:UIControlEventValueChanged];
              [Meds addSubview:swActive];

                
              }
        

        
        
    }



    UIView *Contacts = [[UIView alloc] initWithFrame:CGRectMake(10, 640, self.view.frame.size.width-20, 118)];
    [Contacts setBackgroundColor:[UIColor whiteColor]];
    [Contacts.layer setCornerRadius:5];
    [Contacts.layer setBorderWidth:1.5];
    [Contacts.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
    [Contacts.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Contacts];



   
    NSArray *names6 =[NSArray arrayWithObjects:@"   Doctor:", @"   Pharmacy Prescription:",@"   Notes:", nil];

         NSArray *Cell6 =[NSArray arrayWithObjects:@"Doctor", @"Prescription", @"Notes", nil];

    for (int i=0 ; i<[names6 count]; i++) {

        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names6 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Contacts addSubview:MedName];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([MedName.text length]*8, 39*i, self.view.frame.size.width-70, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTitle:[Cell6 objectAtIndex:i]];

        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Contacts addSubview:Name];

        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(265, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action.png"] forState:UIControlStateNormal];
        [NameButton setTitle:[Cell6 objectAtIndex:i] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Contacts addSubview:NameButton];
        
        
    }

    [scrollViewSpread setContentSize:CGSizeMake(320, 820)];

        //142 × 36 pixels
    


    UIButton *Save = [[UIButton alloc] initWithFrame:CGRectMake(10, 770, 142/1.1,  36/1.1)];
    [Save setBackgroundColor:[UIColor clearColor]];
    [Save setShowsTouchWhenHighlighted:NO];
    [Save setImage:[UIImage imageNamed:@"save.png"] forState:UIControlStateNormal];
    [Save setAutoresizesSubviews:YES];
    [Save addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:Save];


        //142 × 36 pixels

    UIButton *Cancel = [[UIButton alloc] initWithFrame:CGRectMake(180,770, 142/1.1,  36/1.1)];
    [Cancel setBackgroundColor:[UIColor clearColor]];
    [Cancel setShowsTouchWhenHighlighted:NO];
    [Cancel setImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
    [Cancel setAutoresizesSubviews:YES];
    [Cancel addTarget:self action:@selector(Cancel:) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:Cancel];
    

   

    [super viewDidLoad];
  
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)setitemsDictionary:(NSString*)set name:(NSString*)name

{
    if (set) {
            //[Vraagantwoord setObject:set forKey:name];
    }




        // //NSLog(@"MedicineSpecs %@", MedicineSpecs);
    
}


-(void)switchValueReminder:(UISwitch*)sender

{

    //NSLog(@"%i", sender.tag);


        //TekstIDLabel *Labelvanknop = (TekstIDLabel *)[scrollViewSpread viewWithTag:sender.tag+100];


        // [sender setOn:[SaveData saveinstellingen:Labelvanknop.text]];




        // we hebben nu in elk veld hetzlefde staan
}

- (IBAction)showMediaPicker:(id)sender
{
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];

    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    [self presentViewController:mediaPicker animated:YES completion:NULL];

}

-(void)Action:(UIButton*)sender

{

    //NSLog(@"%@", sender.titleLabel.text);
    if ([sender.titleLabel.text isEqualToString:@"Name:"]) {



    }
    if ([sender.titleLabel.text isEqualToString:@"Color"]) {

        [self changeColorFridge];
    }
    if ([sender.titleLabel.text isEqualToString:@"Alert"]) {


        [self showMediaPicker];

    }
    if ([sender.titleLabel.text isEqualToString:@"Photo"]) {

        [self getCameraPicture:sender];
    }
    else
        {



       if ([sender.titleLabel.text isEqualToString:@"Name"]||[sender.titleLabel.text isEqualToString:@"Instructions"]||[sender.titleLabel.text isEqualToString:@"Quantity"]) {


            NameItemViewController *controller = [[NameItemViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {


                            if ([sender.titleLabel.text isEqualToString:label.title]) {
                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParantRenew:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@:", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];





        }
        else if ([sender.titleLabel.text isEqualToString:@"Strength"]) {


            StrengthViewController *controller = [[StrengthViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {


                            if ([sender.titleLabel.text isEqualToString:label.title]) {
                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParantRenew:self];
          
            [controller hideAndseek:sender.titleLabel.text];





        }

        else if ([sender.titleLabel.text isEqualToString:@"Type"]) {


            TypeViewController *controller = [[TypeViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleLabel.text isEqualToString:label.title]) {


                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParantRenew:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@:", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            [controller setChoice:sender.titleLabel.text];
        }


        else if ([sender.titleLabel.text isEqualToString:@"Profile"]) {


            ProfileViewController *controller = [[ProfileViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleLabel.text isEqualToString:label.title]) {


                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParantRenew:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@:", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            [controller setChoice:sender.titleLabel.text];
        }

        else if ([sender.titleLabel.text isEqualToString:@"Doctor"]) {


            DocterViewController *controller = [[DocterViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleLabel.text isEqualToString:label.title]) {


                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParantRenew:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@:", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            [controller setChoice:sender.titleLabel.text];
        }
        else if ([sender.titleLabel.text isEqualToString:@"Prescription"]) {


            PerscriptionViewController *controller = [[PerscriptionViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleLabel.text isEqualToString:label.title]) {

                                
                                [controller whatLabel:label];
                                
                            }
                            
                        }}}}
            
            [controller getParantRenew:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@:", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            [controller setChoice:sender.titleLabel.text];
        }
        else if ([sender.titleLabel.text isEqualToString:@"Frequency"]) {
            
            
            FrequencyViewController *controller = [[FrequencyViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
            
            
            for (UIScrollView *scroll in self.view.subviews) {
                
                for (UIView *view in scroll.subviews) {
                    
                    
                    
                    for (TekstIDLabel *label in view.subviews) {
                        
                        if ([label isKindOfClass:[TekstIDLabel class]]) {
                            
                            if ([sender.titleLabel.text isEqualToString:label.title]) {
                                
                                
                                [controller whatLabel:label];
                                
                            }
                            
                        }}}}
            
            [controller getParantRenew:self];
            [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@:", sender.titleLabel.text]];
            [controller hideAndseek:sender.titleLabel.text];
            [controller setChoice:sender.titleLabel.text];
        }
        
        }
    
    
}


-(void)Next:(UIButton*)sender

{

}

- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{

    MPMediaItem *anItem = (MPMediaItem *)[mediaItemCollection.items objectAtIndex:0];

    NSURL *assetURL = [anItem valueForProperty: MPMediaItemPropertyAssetURL];


    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

    NSString *documentsDir = [paths2 objectAtIndex:0];





    NSData *data2 = [NSData dataWithContentsOfURL:assetURL];
    
    [data2 writeToFile:[NSString stringWithFormat:@"%@/Copy.mp3", documentsDir] atomically:YES];


    //NSLog(@"MPMediaItemPropertyAssetURL %@", assetURL);

    [self dismissViewControllerAnimated:YES completion:NULL];


}



-(void)getCameraPicture:(UIButton*)sender
{


	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.allowsEditing = YES;
#if (TARGET_IPHONE_SIMULATOR)
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
#endif
	[self presentViewController: picker animated:YES completion:NULL];
}


-(void)imagePickerController:(UIImagePickerController *)picker
      didFinishPickingImage : (UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{


  
    [headShot setImage:[self thumbWithSideOfLength2:120 image:image]];

    [self dismissViewControllerAnimated:YES completion:NULL];

}


- (UIImage *)thumbWithSideOfLength2:(float)length image:(UIImage*)mainImage {

    UIImage *thumbnail;

   if (mainImage.size.width< length) {

        CGSize itemSiz1 = CGSizeMake(mainImage.size.width*(length/mainImage.size.width), mainImage.size.height*(length/mainImage.size.width));

        UIGraphicsBeginImageContextWithOptions(itemSiz1, NO, 0.0);

        CGRect imageRect2 = CGRectMake(0.0, 0.0, itemSiz1.width, itemSiz1.height);
        [mainImage drawInRect:imageRect2];

        mainImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }

    UIImageView *mainImageView = [[UIImageView alloc] initWithImage:mainImage];
    BOOL widthGreaterThanHeight = (mainImage.size.width > mainImage.size.height);
    float sideFull = (widthGreaterThanHeight) ? mainImage.size.height : mainImage.size.width;
    CGRect clippedRect = CGRectMake(0, 0, sideFull, sideFull);

    UIGraphicsBeginImageContext(CGSizeMake(length, length));
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextClipToRect( currentContext, clippedRect);
    CGFloat scaleFactor = length/sideFull;
    if (widthGreaterThanHeight) {

        CGContextTranslateCTM(currentContext, -((mainImage.size.width-sideFull)/2)*scaleFactor, 0);

    }
    else {
        CGContextTranslateCTM(currentContext, 0, -((mainImage.size.height - sideFull) / 2) * scaleFactor);
    }

    CGContextScaleCTM(currentContext, scaleFactor, scaleFactor);
    [mainImageView.layer renderInContext:currentContext];
    thumbnail = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return thumbnail;

}

-(void)setItemsNone:(NSMutableDictionary*)set

{

       AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    ChosenMedicen = set;



    chosenID = [appDelegate newUUID];


    for (UIScrollView *scroll in self.view.subviews) {

        for (UIView *view in scroll.subviews) {



            for (TekstIDLabel *label in view.subviews) {

                if ([label isKindOfClass:[TekstIDLabel class]]) {

                    if (label.title) {


                        if (label.text) {

                            [label setText:[set valueForKey:label.title]];

                            NSLog(@"%@", label.text);
                        }
                        else
                            {


                            }

                    }
                    
                    
                }
                
                
            }
            
            
        }
        
    }
    
    
        //NSLog(@"ChosenMedicen %@", ChosenMedicen);
}


-(void)setItems:(NSMutableDictionary*)set

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    ChosenMedicen = set;

 

    chosenID = [set valueForKey:@"ID_Medicine"];

   
    for (UIScrollView *scroll in self.view.subviews) {

        for (UIView *view in scroll.subviews) {



            for (TekstIDLabel *label in view.subviews) {

                if ([label isKindOfClass:[TekstIDLabel class]]) {

                    if (label.title) {


                        if (label.text) {

                            [label setText:[set valueForKey:label.title]];

                            if ([label.title isEqualToString:@"Type"]) {


                                appDelegate.TypeMed =[set valueForKey:label.title];
                            }

                        }
                        else
                            {
                      

                            }


                        
                            //ChosenMedicen
                    }
                    
                    
                }
                
                
            }
            
            
        }

    }


       //NSLog(@"ChosenMedicen %@", ChosenMedicen);
}


- (void) viewWillAppear:(BOOL)animated {
	
}


-(void)setNew

{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    chosenID = [appDelegate newUUID];

    //NSLog(@"%@", chosenID);

  


}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *) picker
{
	[picker dismissViewControllerAnimated:YES  completion:nil];

        //////////////NSLog(@"dismissViewControllerAnimated");


}


- (void) mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{
		[self dismissViewControllerAnimated:YES completion:NULL];
}




- (IBAction)showMediaPicker
{
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];

    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    [self presentViewController:mediaPicker animated:YES completion:NULL];
}


-(void) changeColorFridge
{
    LeveyPopColorView *pinview01 = (LeveyPopColorView *)[self.view  viewWithTag:3459148];
    if (pinview01) {

        [pinview01 removeFromSuperview];


    }
    else
        {


        color = [[LeveyPopColorView alloc] initWithTitle:@"Share Photo to..." options:NULL setIt:2];
            // lplv.isModal = NO;
        [color setTag:3459148];
        [color showInView:self.view animated:YES];
        [color getParent:self get:3];


        }
    
    
    
    
    
    
}


-(void) save:(UIButton*)sender
{




    for (TekstIDLabel *view in self.view.subviews) {

        if ([view isKindOfClass:[TekstIDLabel class]]) {


            //NSLog(@"%@", view.title);

        }
    }

   double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){



    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
    [set setObject:[NSString stringWithFormat:@"%f", rood] forKey:@"ColorRed"];
    [set setObject:[NSString stringWithFormat:@"%f", doorzicht] forKey:@"Coloralpha"];
     [set setObject:[NSString stringWithFormat:@"%f", groen] forKey:@"ColorGreen"];
     [set setObject:[NSString stringWithFormat:@"%f", blauw] forKey:@"Colorblue"];
      [set setObject:Nameit.text forKey:@"Name"];
     [set setObject:headShot.image forKey:@"Image"];
     [set setObject:Sound.text forKey:@"Audio"];
    [set setObject:chosenID forKey:@"ID"];

    //NSLog(@"%@", set);


    NSArray *profileItem =[NSArray arrayWithObjects:@"ID", @"Name", @"Image", @"ColorRed", @"ColorGreen", @"Colorblue", @"Coloralpha",@"Audio", nil];


        //[SaveData insertProfileSQL:set setnames:profileItem new:parantIt];

    
});


}

-(void) backAction
{


   [[self navigationController] popViewControllerAnimated:YES];	

}

-(void)showalert

{
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"To start please click\n \"New User\" to create your\nprofile and start adding\nyour Meds"];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464749];
    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"New user"];
    [alert show];
}

-(void)getParantRenew:(ViewController*)parant

{
    parantIt = parant;
}


-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{

    if (index==1) {



    }

    else
        {

        }
        }

-(void) CreateAccount

{

}


- (void)setSelectedColor:(UIColor*)colorit{


    const CGFloat *components = CGColorGetComponents(colorit.CGColor);
     rood = components[0];
     groen = components[1];
     blauw = components[2];
     doorzicht = components[3];


    [ColorField setBackgroundColor:colorit];

    
}

- (IBAction)Cancel:(UIButton *)sender {


    [[self navigationController] popViewControllerAnimated:YES];

}

- (IBAction)OK:(UIButton *)sender {

 
   
 
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{
 //NSLog(@"turnbuttons");
}

@end
