//
//  Monthview.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "ScedulareView.h"
#import "GetData.h"
#import "FrequencyViewControllerNew.h"

@implementation ScedulareView
@synthesize table;
@synthesize Parantit;
@synthesize MonthList;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

        MonthList =[[NSMutableArray alloc] initWithObjects:@"",NSLocalizedString(@"Daily",nil),NSLocalizedString(@"Weekly",nil),NSLocalizedString(@"Every X Days",nil), nil];

        table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0, 320, 39)];
        table.separatorColor = [UIColor grayColor];
        table.backgroundColor = [UIColor colorWithRed:216.0f/255.0f green:226.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
        table.rowHeight =30;
        table.delegate = self;
        table.dataSource = self;
        [table setEditing:NO];
        [self addSubview:table];
        
       

    }
    return self;
}


-(void)setParant:(FrequencyViewControllerNew*)set

{

    Parantit =set;


   
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [MonthList count];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{



    return 30;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{

   [Parantit chosenSchedule:[MonthList objectAtIndex:indexPath.row]];


}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *identifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

	if (cell == nil) {

        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier]; //04-07-2017 changes "UITableViewCellStyleDefault" insted ot "UITableViewCellSelectionStyleGray"
        
//        [cell setSelectionStyle:UITableViewCellSelectionStyleGray]; //comment line  04-07-2017 changes

        

        
    }
    
    else
        
        {
        
        }

  
    cell.textLabel.text =NULL;
    
    
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0, 300, 30)];
    textLabel.text = [MonthList objectAtIndex:indexPath.row];
    [textLabel setTextAlignment:NSTextAlignmentCenter];
    [textLabel setFont:[UIFont boldSystemFontOfSize:15]];
//    [textLabel setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [textLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
//    [textLabel setBackgroundColor:[UIColor colorWithRed:0.898 green:0.898 blue:0.898 alpha:1.000]];//04-07-2017 changes
    [textLabel setBackgroundColor:[UIColor colorWithRed:216.0f/255.0f green:226.0f/255.0f blue:240.0f/255.0f alpha:1.0f]];
    [[cell contentView] addSubview:textLabel];
    
   
  
    return cell;
    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
