//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "ShowHistoryViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "TimesofdayCell.h"
#import "SaveData.h"
#import "Settings.h"
#import "FrequencyViewControllerNew.h"

@interface ShowHistoryViewController ()

@end

@implementation ShowHistoryViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize table;
@synthesize TitleTextchose;
@synthesize underTitleText;
@synthesize theTime;
@synthesize theDay;
@synthesize theDate;



-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    

    parantLabel =(TekstIDLabel*)set;
    
}



- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];


    TimeChoses =[[NSMutableArray alloc]init];

  //     [self.view.layer setMasksToBounds:YES];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


    UIBarButtonItem *Cancel = [[UIBarButtonItem alloc]
                               initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonSystemItemCancel target:self
                               action:@selector(Cancel:)];
   
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Cancel setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }


    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];


    UIView *textit  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 160, 20)];
    [title setTextColor:[UIColor whiteColor]];
    [title setText:@"History Shown"];
    [title setFont:[UIFont boldSystemFontOfSize:15]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];
    [textit addSubview:title];


    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:textit];




    UIBarButtonItem *Save = [[UIBarButtonItem alloc]
                             initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleBordered target:self
                             action:@selector(Save:)];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [Save setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }



    UIToolbar *toolbarUp = [[UIToolbar alloc] init];
    toolbarUp.frame = CGRectMake(0, -2, 320, 54);
   
  
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        
        
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk"] forToolbarPosition:0 barMetrics:0];
        
    }
    
    else
    {
        [toolbarUp setBackgroundImage:[UIImage imageNamed:@"balk.png"] forToolbarPosition:0 barMetrics:0];
    }
    [toolbarUp setTranslucent:NO];
    [self.view addSubview:toolbarUp];


    NSArray *itemsUp = [NSArray arrayWithObjects:Cancel,flexItem,random,flexItem,Save,nil];
    toolbarUp.items = itemsUp;



    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 64, self.view.frame.size.width-20, 44)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];



    underTitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 68, self.view.frame.size.width-40, 35)];
    [underTitleText setBackgroundColor:[UIColor clearColor]];
    [underTitleText setDelegate:self];
    [underTitleText setText:@"Until  Days"];
    [underTitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    [underTitleText setUserInteractionEnabled:NO];
    [underTitleText setFont:[UIFont boldSystemFontOfSize:15]];
    [self.view addSubview:underTitleText];




    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(63, 68, self.view.frame.size.width-40, 35)];
    [TitleText setBackgroundColor:[UIColor clearColor]];
    [TitleText setDelegate:self];
    [TitleText setText:@""];
    [TitleText setUserInteractionEnabled:NO];
    [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
    [TitleText setTag:123+1];
    

    [TitleText setFont:[UIFont boldSystemFontOfSize:15]];
    [self.view addSubview:TitleText];

    NSDate *now = [[NSDate alloc] init];
    
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     
    [dateFormatter setDateFormat:@"EEEE"];
    
    theDay = [dateFormatter stringFromDate:now];
 


    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [timeFormat setDateFormat:@"hh:mm"];
    theTime = [timeFormat stringFromDate:now];


     NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
 
    [format setDateFormat:@"dd/MM/yyyy"];
    theDate = [format stringFromDate:now];

    

        
        
        NSDateFormatter *format2 = [[NSDateFormatter alloc] init];
        [format2 setDateFormat:@"a"];
        timeframe = [format2 stringFromDate:now];
        
        
    



}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    [TitleText setKeyboardType:UIKeyboardTypeDecimalPad];
   
    TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
    [TitleText becomeFirstResponder];
}




-(NSString*)convertStringToData:(NSString*)set
{

 
  

    NSString *change = set;

    if ([change length] ==5) {
  
    if ([timeframe isEqual:@"AM"]) {

        NSRange range = NSMakeRange (0, 2);
      

        NSRange range2 = NSMakeRange (3, 2);
       

        change = [NSString stringWithFormat:@"%@:%@",[change substringWithRange:range], [change substringWithRange:range2]];

    }
    if ([timeframe isEqual:@"PM"]) {

        NSRange range = NSMakeRange (0, 2);
      

        NSRange range2 = NSMakeRange (3, 2);
       

         change = [NSString stringWithFormat:@"%@:%@",[change substringWithRange:range], [change substringWithRange:range2]];

    }
        
        if ([timeframe isEqual:@"nachm."]) {
            
            NSRange range = NSMakeRange (0, 2);
            
            
            NSRange range2 = NSMakeRange (3, 2);
            
            
            change = [NSString stringWithFormat:@"%@:%@",[change substringWithRange:range], [change substringWithRange:range2]];
            
        }
        if ([timeframe isEqual:@"vorm."]) {
            
            NSRange range = NSMakeRange (0, 2);
            
            
            NSRange range2 = NSMakeRange (3, 2);
            
            
            change = [NSString stringWithFormat:@"%@:%@",[change substringWithRange:range], [change substringWithRange:range2]];
            
        }

    }

    if ([change length] ==4) {

        if ([timeframe isEqual:@"AM"]||[timeframe isEqual:@"a.m."]) {

            NSRange range = NSMakeRange (0, 1);
        

            NSRange range2 = NSMakeRange (2, 2);
           


            change = [NSString stringWithFormat:@"0%@:%@",[change substringWithRange:range], [change substringWithRange:range2]];
            

        }
        if ([timeframe isEqual:@"PM"]||[timeframe isEqual:@"p.m."]) {

            NSRange range = NSMakeRange (0, 1);
          

            NSRange range2 = NSMakeRange (2, 2);
           


            change = [NSString stringWithFormat:@"0%@:%@",[change substringWithRange:range], [change substringWithRange:range2]];
            
        }
        
    }

  
    
    
    return change;


    
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
        {


        [TimeChoses removeObjectAtIndex:indexPath.row];
      

        [table reloadData];




        } else if (editingStyle == UITableViewCellEditingStyleInsert)
            {



            }
    
    
}



-(void) addItem:(NSString*) sender
{


}

- (BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [TimeChoses count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(TimesofdayCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {





    [cell FillAllItems:[TimeChoses objectAtIndex:indexPath.row] index:indexPath.row];




}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{



    return 50;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{





}


- (TimesofdayCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {




    NSString *identifier = @"CellIdentifier";
    TimesofdayCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

	if (cell == nil) {

        cell = [[TimesofdayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    else
        
        {
        
        }
    
    
    return cell;
    

}

-(void)Cancel:(UIBarButtonItem*) sender

{
    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];

}

-(void)Save:(UIBarButtonItem*) sender

{

   [SaveData insertSetting:@"History" setnames:TitleText.text];

    
    
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = myApp.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Settings"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count] >0) {
        
        Settings *newCourse =[fetchedObjects objectAtIndex:0];
        
        newCourse.history =TitleText.text;
        
        
    }
    



    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];



    
}
-(void)setChoice:(NSString*)gothere

{
    
}

-(NSString*) convertToOurs:(NSString*)day

{

    
    return [NSString stringWithFormat:@"%i", [day intValue] *24];
}
-(void)change:(UIButton*)sender

{

    if (sender.tag ==141 ||sender.tag ==181) {

    }
    if (sender.tag ==142 ||sender.tag ==182) {
        
    }
    if (sender.tag ==143 ||sender.tag ==183) {

    }
    if (sender.tag ==144 ||sender.tag ==184) {

    }
  
    
}


-(void)textViewDidChange:(UITextView *)textView
{
    
   
   

        //Untill  Days
   
    if ([textView.text length]<4) {


        if ([textView.text length] ==0) {

            [underTitleText setText:[NSString stringWithFormat:@"%@   %@", NSLocalizedString(@"Until",nil), NSLocalizedString(@"Days",nil)]];
        }
        if ([textView.text length] ==1) {

            [underTitleText setText:[NSString stringWithFormat:@"%@     %@", NSLocalizedString(@"Until",nil), NSLocalizedString(@"Days",nil)]];
        }
        if ([textView.text length] ==2) {

            [underTitleText setText:[NSString stringWithFormat:@"%@      %@", NSLocalizedString(@"Until",nil), NSLocalizedString(@"Days",nil)]];
        }
        if ([textView.text length] ==3) {

            [underTitleText setText:[NSString stringWithFormat:@"%@        %@", NSLocalizedString(@"Until",nil), NSLocalizedString(@"Days",nil)]];
        }


    }
    else
        {
        [textView setText:@""];
               [underTitleText setText:[NSString stringWithFormat:@"%@  %@", NSLocalizedString(@"Until",nil), NSLocalizedString(@"Days",nil)]];
        }
    
    
    
    
    
    
    
    
}

-(void)Action:(UIButton*)sender

{

  
}

-(void)changeChose:(NSString*)sender

{


   
     chosen = sender;
    
    
}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{


}

-(void)getParant:(UIViewController*)parant

{

    parantIt =(FrequencyViewController*)parant;
 
    
}




- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
