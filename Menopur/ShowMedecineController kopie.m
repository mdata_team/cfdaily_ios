//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "ShowMedecineController.h"
#import "MedicineViewController.h"
#import "StrengthViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AlertViewController.h"
#import "NameItemViewController.h"
#import "ProfileViewController.h"
#import "SaveData.h"
#import "TekstIDLabel.h"
#import "DocterViewController.h"
#import "AppDelegate.h"
#import "TypeViewController.h"
#import "PerscriptionViewController.h"
#import "RemainingViewController.h"
#import "DoseViewController.h"
#import "FillingViewController.h"
#import "NoteViewController.h"
#import "NewMedecineController.h"
#import "Notification.h"
#import "GetData.h"
#import "ImageViewController.h"
@interface ShowMedecineController ()

@end

@implementation ShowMedecineController

@synthesize color;
@synthesize rood;
@synthesize groen;
@synthesize scrollViewSpread;
@synthesize blauw;
@synthesize doorzicht;
@synthesize musicPlayer;
@synthesize headShot;
@synthesize Nameit;
@synthesize Sound;
@synthesize ColorField;
@synthesize chosenID;
@synthesize parantIt;
@synthesize MedicineSpecs;
@synthesize ChosenMedicen;
@synthesize selectedNotifictions;
@synthesize parantGo;
@synthesize NotesView;
@synthesize ContendHead;
@synthesize MedNamehead;
@synthesize MedShot;

@synthesize TimesLabel;
@synthesize TimesLabelextra;


@synthesize InstructionsExtra;
@synthesize Instructions;

@synthesize Quantity;
@synthesize Assign;
@synthesize Schdules;
@synthesize Meds;
@synthesize Contacts;


-(void) backAction
{


    [[self navigationController] popViewControllerAnimated:YES];
   

}

- (void)viewDidLoad
{



        //Med info


    self.navigationItem.title = @ "Medicine Info";
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000];

    UIBarButtonItem *edit = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                             target:self
                             action:@selector(Edit:)];
    edit.style = UIBarButtonItemStyleBordered;


    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = edit;




   selectedNotifictions  = [[NSMutableArray alloc]init];

        //Month_view.png


    // [controller2 setItems:[GetData getMedicinewithID:sender]];

    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



    musicPlayer = [MPMusicPlayerController iPodMusicPlayer];



	[musicPlayer beginGeneratingPlaybackNotifications];


   scrollViewSpread = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, self.view.frame.size.height-164)];
    [scrollViewSpread setCanCancelContentTouches:NO];
    scrollViewSpread.showsHorizontalScrollIndicator = YES;
    [scrollViewSpread setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


    scrollViewSpread.delaysContentTouches=YES;
    scrollViewSpread.clipsToBounds = YES;
    scrollViewSpread.scrollEnabled = YES;
    scrollViewSpread.pagingEnabled = NO;
    [scrollViewSpread setDelegate:self];
    scrollViewSpread.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.view  addSubview:scrollViewSpread];


    UIView *Combi = [[UIView alloc] initWithFrame:CGRectMake(-5, 0, self.view.frame.size.width+10, 120)];
    [Combi setBackgroundColor:[UIColor colorWithRed:0.733 green:0.851 blue:0.902 alpha:1.000]];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
    Combi.layer.shadowOffset = CGSizeMake(3, 0);
    Combi.layer.shadowOpacity = 2;
    Combi.layer.shadowRadius = 2.0;
    [self.view addSubview:Combi];



    MedNamehead = [[TekstIDLabel alloc] initWithFrame:CGRectMake(125, 10, self.view.frame.size.width-20, 40)];
    [MedNamehead setTextColor:[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000]];
    [MedNamehead setFont:[UIFont boldSystemFontOfSize:20]];
    [MedNamehead setTag:120];
    [MedNamehead setText:@""];
    [MedNamehead setBackgroundColor:[UIColor clearColor]];
    [MedNamehead setNumberOfLines:3];
    [Combi addSubview:MedNamehead];


    ContendHead = [[TekstIDLabel alloc] initWithFrame:CGRectMake(125, 39, self.view.frame.size.width-140, 60)];
    [ContendHead setTextColor:[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000]];
    [ContendHead setFont:[UIFont systemFontOfSize:14]];
    [ContendHead setText:@"500 ml"];
    [ContendHead setTag:120];
    [ContendHead setBackgroundColor:[UIColor clearColor]];
    [ContendHead setNumberOfLines:4];
    [Combi addSubview:ContendHead];

    headShot = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 100, 100)];
    [headShot setBackgroundColor:[UIColor blackColor]];
    [headShot setTag:130];
    [headShot setImage:[UIImage imageNamed:@"Medhot.png"]];
        //[headShot setImage:[UIImage imageNamed:@"headshot.png"]];



    [Combi addSubview:headShot];

   Quantity = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 196+39)];
    [Quantity setBackgroundColor:[UIColor whiteColor]];
    [Quantity.layer setCornerRadius:10];
    [Quantity.layer setBorderWidth:1.5];
    [Quantity.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
    [Quantity.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Quantity];


    

    

    NSArray *names4 =[NSArray arrayWithObjects:@"   Quantity Per Dose:", @"   Frequency:", @"   Starting:", @"   Ending:",@"   Time(s) of Day:", @"   Instructions:", nil];

    NSArray *Cell4 =[NSArray arrayWithObjects:@"Quantity#140",@"Frequency#90", @"Starting#72", @"Duration#76", @"Times#120",@"Instructions#100", nil];

    for (int i=0 ; i<[names4 count]; i++) {


        NSArray *numbers = [[Cell4 objectAtIndex:i] componentsSeparatedByString:@"#"];

        
        if ([[numbers objectAtIndex:0] isEqualToString:@"Instructions"]) {
           
            
            Instructions = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
            [Instructions setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
            [Instructions setFont:[UIFont boldSystemFontOfSize:14]];
            [Instructions setText:[names4 objectAtIndex:i]];
            [Instructions setTag:120];
            [Instructions.layer setBorderWidth:1];
            [Instructions.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
            [Instructions setBackgroundColor:[UIColor clearColor]];
            [Instructions setNumberOfLines:3];
            [Quantity addSubview:Instructions];
            
            
            InstructionsExtra = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-30, 40)];
            [InstructionsExtra setTextColor:[UIColor blackColor]];
            [InstructionsExtra setFont:[UIFont systemFontOfSize:14]];
            [InstructionsExtra setText:@""];
            [InstructionsExtra setTitle:[numbers objectAtIndex:0]];
            [InstructionsExtra setTextAlignment:NSTextAlignmentRight];
            
            [InstructionsExtra setBackgroundColor:[UIColor clearColor]];
            [InstructionsExtra setNumberOfLines:2];
            [Quantity addSubview:InstructionsExtra];
        }
        
        else if ([[numbers objectAtIndex:0] isEqualToString:@"Times"]) {
            
            
            TimesLabel = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
            [TimesLabel setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
            [TimesLabel setFont:[UIFont boldSystemFontOfSize:14]];
            [TimesLabel setText:[names4 objectAtIndex:i]];
            [TimesLabel setTag:120];
   
            [TimesLabel setBackgroundColor:[UIColor clearColor]];
            [TimesLabel setNumberOfLines:3];
            [Quantity addSubview:TimesLabel];
            
            
            
            TimesLabelextra = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-30, 40)];
            [TimesLabelextra setTextColor:[UIColor blackColor]];
            [TimesLabelextra setFont:[UIFont systemFontOfSize:14]];
            [TimesLabelextra setText:@""];
            [TimesLabelextra setTitle:[numbers objectAtIndex:0]];
            [TimesLabelextra setTextAlignment:NSTextAlignmentRight];
            
            [TimesLabelextra setBackgroundColor:[UIColor clearColor]];
            [TimesLabelextra setNumberOfLines:1];
            [Quantity addSubview:TimesLabelextra];
        }
        
        
        else
            
        {
            
            if (i ==0) {
              
                
                TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
                [MedName setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
                [MedName setFont:[UIFont boldSystemFontOfSize:14]];
                [MedName setText:[names4 objectAtIndex:i]];
                [MedName setTag:4444];
                [MedName.layer setBorderWidth:1];
                [MedName.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
                [MedName setBackgroundColor:[UIColor clearColor]];
                [MedName setNumberOfLines:3];
                [Quantity addSubview:MedName];
            }
            else
            {

        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names4 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Quantity addSubview:MedName];
                
            }


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-30, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTitle:[numbers objectAtIndex:0]];
        [Name setTextAlignment:NSTextAlignmentRight];
        
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:1];
        [Quantity addSubview:Name];
            
        }
        
        
        
    }
    

    Assign = [[UIView alloc] initWithFrame:CGRectMake(10, 216+39, self.view.frame.size.width-20, 40)];
    [Assign setBackgroundColor:[UIColor whiteColor]];
    [Assign.layer setCornerRadius:10];
    [Assign.layer setBorderWidth:1.5];
    [Assign.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
    [Assign.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Assign];


    NSArray *names2 =[NSArray arrayWithObjects:@"   Assign to Profile:", nil];
    NSArray *Cell2 =[NSArray arrayWithObjects:@"Profile#130", nil];

    for (int i=0 ; i<[names2 count]; i++) {

        NSArray *numbers = [[Cell2 objectAtIndex:i] componentsSeparatedByString:@"#"];



        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names2 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Assign addSubview:MedName];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-30, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTitle:[numbers objectAtIndex:0]];
        [Name setTextAlignment:NSTextAlignmentRight];
        
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:1];
        [Assign addSubview:Name];
        
        
        
        
        
    }
    
 

    
    Schdules = [[UIView alloc] initWithFrame:CGRectMake(10, 266+39, self.view.frame.size.width-20, 118-39)];
    [Schdules setBackgroundColor:[UIColor whiteColor]];
    [Schdules.layer setCornerRadius:10];
    [Schdules.layer setBorderWidth:1.5];
    [Schdules.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
    [Schdules.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Schdules];

    
    // NSArray *names4 =[NSArray arrayWithObjects:@"   Alert Recording:",@"   Alert Message Text:", nil];

    NSArray *names3 =[NSArray arrayWithObjects: @"   Alert Recording:", @"   Alert Message Text:",nil];
    NSArray *Cell3 =[NSArray arrayWithObjects: @"Recording#80",  @"Message#150",  nil];
    
    
    for (int i=0 ; i<[names3 count]; i++) {


        NSArray *numbers = [[Cell3 objectAtIndex:i] componentsSeparatedByString:@"#"];


        

        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names3 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Schdules addSubview:MedName];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-30, 40)];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setTitle:[numbers objectAtIndex:0]];
        [Name setTextColor:[UIColor blackColor]];
        [Name setTextAlignment:NSTextAlignmentRight];
        [Name setBackgroundColor:[UIColor clearColor]];
        
        if ([[numbers objectAtIndex:0] isEqualToString:@"Message"]) {
           [Name setNumberOfLines:2];
        }
        else
        {
        [Name setNumberOfLines:1];
            
        }
        [Schdules addSubview:Name];

        ////NSLog(@"numbers %@", [numbers objectAtIndex:0]);


        
        
        
    }

  
    Meds = [[UIView alloc] initWithFrame:CGRectMake(10, 356+39, self.view.frame.size.width-20, 118)];
    [Meds setBackgroundColor:[UIColor whiteColor]];
    [Meds.layer setCornerRadius:10];
    [Meds.layer setBorderWidth:1.5];
    [Meds.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
    [Meds.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Meds];


 
    
    NSArray *names5 =[NSArray arrayWithObjects:@"  Quantity Remaining:", @"   Quantity Per Filling:",@"   Minimum Quantity\n   For Refilling:", nil];

    NSArray *Cell5 =[NSArray arrayWithObjects:@"Remaining#182", @"Filling#180", @"Refilling#192", nil];

    for (int i=0 ; i<[names5 count]; i++) {

        NSArray *numbers = [[Cell5 objectAtIndex:i] componentsSeparatedByString:@"#"];

        
        if (i ==0) {
            
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names5 objectAtIndex:i]];
        [MedName setTag:4445];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Meds addSubview:MedName];
            
        }
        else if (i ==1) {
           
            
            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
            [MedName setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:14]];
            [MedName setText:[names5 objectAtIndex:i]];
            [MedName setTag:4446];
            [MedName.layer setBorderWidth:1];
            [MedName.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Meds addSubview:MedName];
        }
        else if (i ==2) {
            
            
            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
            [MedName setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:14]];
            [MedName setText:[names5 objectAtIndex:i]];
            [MedName setTag:4447];
            [MedName.layer setBorderWidth:1];
            [MedName.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Meds addSubview:MedName];
        }
        else
        {
            
            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
            [MedName setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:14]];
            [MedName setText:[names5 objectAtIndex:i]];
            [MedName setTag:120];
            [MedName.layer setBorderWidth:1];
            [MedName.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Meds addSubview:MedName];
        }
       

        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-30, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTextAlignment:NSTextAlignmentRight];
        [Name setTitle:[numbers objectAtIndex:0]];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:1];
        [Meds addSubview:Name];



        
    }



    Contacts = [[UIView alloc] initWithFrame:CGRectMake(10, 484+39, self.view.frame.size.width-20, 118+300)];
    [Contacts setBackgroundColor:[UIColor whiteColor]];
    [Contacts.layer setCornerRadius:10];
    [Contacts.layer setBorderWidth:1.5];
    [Contacts.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
    [Contacts.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Contacts];



    
  NSArray *names6 =[NSArray arrayWithObjects:@"   Doctor:", @"   Phone Doctor:", @"   Mail Doctor:", @"   Pharmacy:", @"   Phone Pharmacy:", @"   Mail Pharmacy:",   @"   Prescription Notes:", nil];

    NSArray *Cell6 =[NSArray arrayWithObjects:@"Doctor#65", @"Phone_Doctor#65", @"eMail_Doctor#65", @"Pharmacy#172", @"Phone_Pharmacy#172", @"eMail_Pharmacy#172",  @"Prescription#60", nil];

    for (int i=0 ; i<[names6 count]; i++) {
        
        


        NSArray *numbers = [[Cell6 objectAtIndex:i] componentsSeparatedByString:@"#"];


        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];
        [MedName setTextColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[names6 objectAtIndex:i]];
        [MedName setTag:120];
        [MedName.layer setBorderWidth:1];
        [MedName.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Contacts addSubview:MedName];


        
        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-30, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTitle:[numbers objectAtIndex:0]];
        [Name setTextAlignment:NSTextAlignmentRight];
        
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:1];
        [Contacts addSubview:Name];
        
        
        if ([[numbers objectAtIndex:0] isEqualToString:@"Phone_Doctor"]||[[numbers objectAtIndex:0] isEqualToString:@"eMail_Doctor"]||[[numbers objectAtIndex:0] isEqualToString:@"Phone_Pharmacy"]||[[numbers objectAtIndex:0] isEqualToString:@"eMail_Pharmacy"]) {
            
            UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(0, (39*i), self.view.frame.size.width-20, 40)];
            [NameButton setTitle:[numbers objectAtIndex:0] forState:UIControlStateNormal];
            [NameButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [NameButton setTag:140+i];
            [NameButton.layer setBorderWidth:1];
            [NameButton.layer setBorderColor:[UIColor colorWithRed:0.459 green:0.631 blue:0.957 alpha:1.000].CGColor];
            [NameButton addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchUpInside];
            [NameButton setBackgroundColor:[UIColor clearColor]];
            [Contacts addSubview:NameButton];
        }
        
        if (i==6) {
            
            [Name setAlpha:0];
        }
    
       

    }
    
    
    
    NotesView = [[UITextView alloc] initWithFrame:CGRectMake(100, 282, self.view.frame.size.width-130, 120)];
    [NotesView setTextColor:[UIColor blackColor]];
    [NotesView setFont:[UIFont systemFontOfSize:14]];
    [NotesView setText:@""];
    [NotesView setTag:120];
    [NotesView setBackgroundColor:[UIColor clearColor]];
    [NotesView setEditable:NO];
    [Contacts addSubview:NotesView];
    
    
    MedShot = [[UIImageView alloc] initWithFrame:CGRectMake(10, 282, 80, 80)];
    [MedShot setBackgroundColor:[UIColor grayColor]];
    [MedShot setImage:[UIImage imageNamed:@"Prescription.png"]];
    [MedShot setTag:130];
    [Contacts addSubview:MedShot];
    
    
    UIButton *NameButton2 = [[UIButton alloc]  initWithFrame:CGRectMake(10, 282, 80, 80)];
    [NameButton2 setBackgroundColor:[UIColor clearColor]];
    [NameButton2 addTarget:self action:@selector(Getimage:) forControlEvents:UIControlEventTouchUpInside];
    [Contacts addSubview:NameButton2];



    
    [scrollViewSpread setContentSize:CGSizeMake(320, 780+(39*7))];
    
        //142 × 36 pixels

    
    [super viewDidLoad];
    
    
    
        // Do any additional setup after loading the view, typically from a nib.
}
-(void)DynamicCell:(NSInteger)difference

{
    
    [Quantity setFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 196+(39*difference))];
    
    
      [Assign setFrame:CGRectMake(10, 20+196+(39*difference), Assign.frame.size.width, Assign.frame.size.height)];
    
      [Schdules setFrame:CGRectMake(10,  Assign.frame.origin.y+Assign.frame.size.height+10,  Schdules.frame.size.width, Schdules.frame.size.height)];
     [Meds setFrame:CGRectMake(10,  Schdules.frame.origin.y+Schdules.frame.size.height+10,  Meds.frame.size.width, Meds.frame.size.height)];
     [Contacts setFrame:CGRectMake(10,  Meds.frame.origin.y+Meds.frame.size.height+10,  Contacts.frame.size.width, Contacts.frame.size.height)];
    
 
    [Instructions setFrame:CGRectMake(Instructions.frame.origin.x,196+(39*difference)-40, self.view.frame.size.width-20, 40)];
    [InstructionsExtra setFrame:CGRectMake(InstructionsExtra.frame.origin.x, 196+(39*difference)-40, InstructionsExtra.frame.size.width,39)];
    
    
    [TimesLabelextra setFrame:CGRectMake(TimesLabelextra.frame.origin.x, TimesLabelextra.frame.origin.y, TimesLabelextra.frame.size.width, 39*difference)];
    
     [TimesLabelextra setNumberOfLines:difference];
    
    [scrollViewSpread setContentSize:CGSizeMake(320, Contacts.frame.origin.y+Contacts.frame.size.height+100)];
    
   
}


-(void)Getimage:(UIButton*)sender
{
    
    ImageViewController *controller = [[ImageViewController alloc]init];
    [self presentViewController:controller animated:YES completion:Nil];
    
    if ([UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"PerscriptionImage"]]) {
        [controller getStamp:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"PerscriptionImage"]] tegit:1];
    }
    else{
        
        
    }
    
    
}

-(void)action:(UIButton*)sender

{
    
    //c
    
    
    
    [sender setBackgroundColor:[UIColor grayColor]];
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [sender setBackgroundColor:[UIColor clearColor]];
        
    });
    
    UIDevice *device = [UIDevice currentDevice];
    
    if ([sender.titleLabel.text isEqualToString:@"Phone_Doctor"]) {
        
        if ([[device model] isEqualToString:@"iPhone"] ) {
            
           NSString *text =[ChosenMedicen valueForKey:@"Phone_Doctor"];
            
            if (text != nil) {
                NSError *error = NULL;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
                NSArray *matches = [detector matchesInString:text options:0 range:NSMakeRange(0, [text length])];
                if (matches != nil) {
                    for (NSTextCheckingResult *match in matches) {
                        if ([match resultType] == NSTextCheckingTypePhoneNumber) {
                            ////NSLog(@"Found phone number %@", [match phoneNumber]);
                            
                          
                            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",[[match phoneNumber]stringByReplacingOccurrencesOfString:@" " withString:@""]]];
                            [[UIApplication sharedApplication] openURL:url];
                        }
                    }
                }
            }
            
         
            
        }
        else
        {
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"CF MedCare" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Notpermitted show];
        }
        
        
    }
    
    else  if ([sender.titleLabel.text isEqualToString:@"eMail_Doctor"]) {
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate SendtheMailtoAdress:[ChosenMedicen valueForKey:@"eMail_Doctor"]];
        }
    
    else  if ([sender.titleLabel.text isEqualToString:@"Phone_Pharmacy"]) {
        
        if ([[device model] isEqualToString:@"iPhone"] ) {
            
        
            
            
            NSString *text =[ChosenMedicen valueForKey:@"Phone_Pharmacy"];
           
            
            
            if (text != nil) {
                NSError *error = NULL;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
                NSArray *matches = [detector matchesInString:text options:0 range:NSMakeRange(0, [text length])];
                if (matches != nil) {
                    for (NSTextCheckingResult *match in matches) {
                        if ([match resultType] == NSTextCheckingTypePhoneNumber) {
                            ////NSLog(@"Found phone number %@", [match phoneNumber]);
                            
                            
                            
                            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",[[match phoneNumber]stringByReplacingOccurrencesOfString:@" " withString:@""]]];
                            [[UIApplication sharedApplication] openURL:url];
                        }
                    }
                }
            }
           

            
        }
        else
        {
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"CF MedCare" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Notpermitted show];
        }
        
    }
    else  if ([sender.titleLabel.text isEqualToString:@"eMail_Pharmacy"]) {
        
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate SendtheMailtoAdress:[ChosenMedicen valueForKey:@"eMail_Pharmacy"]];
        
        
    }
  
    
    
    
 
    
}


-(void)viewDidAppear:(BOOL)animated
{


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:1];
 
    
  
   [self setItems:[GetData getMedicinewithID:[ChosenMedicen valueForKey:@"ID_Medicine"]]];
    
    
 
    
}


-(void)Edit:(UIButton*) sender

{
   

    NewMedecineController *controller2 = [[NewMedecineController alloc]init];
    [self.navigationController pushViewController:controller2 animated:YES];

    [controller2 setItems:[GetData getMedicinewithID:[ChosenMedicen valueForKey:@"ID_Medicine"]]];
    [controller2 editOrShow:@"Edit"];
}


- (IBAction)showMediaPicker:(id)sender
{
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];

    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    [self presentViewController:mediaPicker animated:YES completion:NULL];

}

-(void)Next:(UIButton*)sender

{

}

- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{

    MPMediaItem *anItem = (MPMediaItem *)[mediaItemCollection.items objectAtIndex:0];

    NSURL *assetURL = [anItem valueForProperty: MPMediaItemPropertyAssetURL];


    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);

    NSString *documentsDir = [NSString stringWithFormat:@"%@/Caches/",[paths2 objectAtIndex:0]];





    NSData *data2 = [NSData dataWithContentsOfURL:assetURL];
    
    [data2 writeToFile:[NSString stringWithFormat:@"%@/Copy.mp3", documentsDir] atomically:YES];


    ////NSLog(@"MPMediaItemPropertyAssetURL %@", assetURL);

    [self dismissViewControllerAnimated:YES completion:NULL];


}



-(void)getCameraPicture:(UIButton*)sender
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate.toolbarDown setAlpha:0];


	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.allowsEditing = YES;
#if (TARGET_IPHONE_SIMULATOR)
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
#endif
	[self presentViewController: picker animated:YES completion:NULL];
}



-(void)imagePickerController:(UIImagePickerController *)picker
      didFinishPickingImage : (UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{


 


  
    [headShot setImage:[self thumbWithSideOfLength2:120 image:image]];
    [ChosenMedicen setObject:headShot.image forKey:@"Image"];

    
       UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);

    [self dismissViewControllerAnimated:YES completion:NULL];

}


- (UIImage *)thumbWithSideOfLength2:(float)length image:(UIImage*)mainImage {

    UIImage *thumbnail;

   if (mainImage.size.width< length) {

        CGSize itemSiz1 = CGSizeMake(mainImage.size.width*(length/mainImage.size.width), mainImage.size.height*(length/mainImage.size.width));

        UIGraphicsBeginImageContextWithOptions(itemSiz1, NO, 0.0);

        CGRect imageRect2 = CGRectMake(0.0, 0.0, itemSiz1.width, itemSiz1.height);
        [mainImage drawInRect:imageRect2];

        mainImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }

    UIImageView *mainImageView = [[UIImageView alloc] initWithImage:mainImage];
    BOOL widthGreaterThanHeight = (mainImage.size.width > mainImage.size.height);
    float sideFull = (widthGreaterThanHeight) ? mainImage.size.height : mainImage.size.width;
    CGRect clippedRect = CGRectMake(0, 0, sideFull, sideFull);

    UIGraphicsBeginImageContext(CGSizeMake(length, length));
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextClipToRect( currentContext, clippedRect);
    CGFloat scaleFactor = length/sideFull;
    if (widthGreaterThanHeight) {

        CGContextTranslateCTM(currentContext, -((mainImage.size.width-sideFull)/2)*scaleFactor, 0);

    }
    else {
        CGContextTranslateCTM(currentContext, 0, -((mainImage.size.height - sideFull) / 2) * scaleFactor);
    }

    CGContextScaleCTM(currentContext, scaleFactor, scaleFactor);
    [mainImageView.layer renderInContext:currentContext];
    thumbnail = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return thumbnail;

}


-(void)setItems:(NSMutableDictionary*)set

{

    ////NSLog(@"setItems");

    ChosenMedicen =[[NSMutableDictionary alloc]init];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

  appDelegate.TypeMed =[set valueForKey:@"Type"];

    ChosenMedicen = set;
    
    
 
    
    if ([[[[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0] valueForKey:@"Dose_name"] isEqualToString:@"-"]) {
        
        TekstIDLabel *Label = (TekstIDLabel *)[self.view viewWithTag:(4444)];
        TekstIDLabel *Label1 = (TekstIDLabel *)[Meds viewWithTag:(4445)];
        TekstIDLabel *Label2 = (TekstIDLabel *)[Meds viewWithTag:(4446)];
        TekstIDLabel *Label3 = (TekstIDLabel *)[Meds viewWithTag:(4447)];
        
        
        [Label setText:[NSString stringWithFormat:@"   %@", @"Quantity Per Dose:"]];
        [Label1 setText:[NSString stringWithFormat:@"   %@", @"Quantity Remaining:"]];
        [Label2 setText:[NSString stringWithFormat:@"   %@", @"Quantity Per Filling:"]];
        [Label3 setText:[NSString stringWithFormat:@"   %@", @"Minimum Quantity\n   For Refilling:"]];
    }
    else
    {
        TekstIDLabel *Label = (TekstIDLabel *)[self.view viewWithTag:(4444)];
         TekstIDLabel *Label1 = (TekstIDLabel *)[Meds viewWithTag:(4445)];
         TekstIDLabel *Label2 = (TekstIDLabel *)[Meds viewWithTag:(4446)];
        TekstIDLabel *Label3 = (TekstIDLabel *)[Meds viewWithTag:(4447)];
        
        [Label setText:[[NSString stringWithFormat:@"   %@", [[[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0] valueForKey:@"Dose_name"]]capitalizedString]];
        [Label1 setText:[[NSString stringWithFormat:@"   %@", [[[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0] valueForKey:@"Remaining_name"]]capitalizedString]];
        
        
        [Label2 setText:[[NSString stringWithFormat:@"   %@", [[[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0] valueForKey:@"Refill_name"]]capitalizedString]];
        [Label3 setText:[[NSString stringWithFormat:@"   Minimum %@\n   For Refilling:", [[[[[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0] valueForKey:@"Refill_name"] stringByReplacingOccurrencesOfString:@" Per Refill" withString:@""] stringByReplacingOccurrencesOfString:@" Per Refill" withString:@""]]capitalizedString]];
        
        
    }
    
    
    
    
    ////NSLog(@"ChosenMedicen %@", ChosenMedicen);
    


    chosenID = [set valueForKey:@"ID_Medicine"];

      [ChosenMedicen setObject:chosenID forKey:@"ID_Medicine"];
     [ChosenMedicen setObject:appDelegate.CurrentID forKey:@"ID"];


    for (UIScrollView *scroll in self.view.subviews) {

        for (UIView *view in scroll.subviews) {



            for (TekstIDLabel *label in view.subviews) {

                if ([label isKindOfClass:[TekstIDLabel class]]) {

                    ////NSLog(@"%@", label.title);
                    if (label.title) {
                        
                        if ([label.title isEqualToString:@"Recording"]) {
                            
                           // [label setText:[[[GetData getRecording] valueForKey:@"title"]objectAtIndex:[[[GetData getRecording] valueForKey:@"ID"] indexOfObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"recordingMusic"]]]];
                          
                        }

                        else
                        {
                            
                            //

                            
                            if ([label.title isEqualToString:@"Frequency"]) {
                                
                                if ([[ChosenMedicen valueForKey:@"Frequency_text"] isEqualToString:@"Daily"]) {
                                   
                                      [label setText:[ChosenMedicen valueForKey:@"Frequency_text"]];
                                }
                                else if ([[ChosenMedicen valueForKey:@"Frequency_text"] isEqualToString:@"Weekly"]) {
                                    
                                    [label setText:[NSString stringWithFormat:@"%@\n%@", [ChosenMedicen valueForKey:@"Frequency_text"],[ChosenMedicen valueForKey:@"Dates"]]];
                                    [label setNumberOfLines:2];
                                }
                                else
                                {
                                   [label setText: [[ChosenMedicen valueForKey:@"Frequency_text"] stringByReplacingOccurrencesOfString:@"X" withString:[ChosenMedicen valueForKey:@"Frequency"]]];
                                    
                                }
                                
                               
                                
                            }
                            else if ([label.title isEqualToString:@"Pharmacy"]) {
                                
                                
                                
                                
                                [label setText:[ChosenMedicen valueForKey:@"Pharmacy"]];
                                
                                
                                
                            }
                            else if ([label.title isEqualToString:@"Phone_Pharmacy"]) {
                                
                                
                                
                                
                                [label setText:[ChosenMedicen valueForKey:@"Phone_Pharmacy"]];
                                
                                
                                
                            }
                            
                          
                            else if ([label.title isEqualToString:@"eMail_Pharmacy"]) {
                                
                                
                                
                                
                                [label setText:[ChosenMedicen valueForKey:@"eMail_Pharmacy"]];
                                
                                
                                
                            }
                            else if ([label.title isEqualToString:@"eMail_Doctor"]) {
                                
                                
                                
                                
                                [label setText:[ChosenMedicen valueForKey:@"eMail_Doctor"]];
                                
                                
                                
                            }
                            
                            else if ([label.title isEqualToString:@"Phone_Doctor"""]) {
                                
                                
                                
                                
                                [label setText:[ChosenMedicen valueForKey:@"Phone_Doctor"""]];
                                
                                
                                
                            }
                            
                            
                          
                            
                            else if ([label.title isEqualToString:@"Times"]) {
                              
                                
                             
                               [label setText:[[ChosenMedicen valueForKey:@"Times"] stringByReplacingOccurrencesOfString:@"," withString:@"\n"]];
                                
                                    NSArray *numbers2 = [[ChosenMedicen valueForKey:@"Times"] componentsSeparatedByString:@","];
                                
                                ////NSLog(@"%@", numbers2);
                                
                                [self DynamicCell:[numbers2 count]];
                                
                            
                                
                                
                            }
                            else
                            {

                                
                                [label setText:[set valueForKey:label.title]];
                                
                            }
                            
                        }
                        

                            if ([label.title isEqualToString:@"Type"]) {

                                appDelegate.TypeMed =[set valueForKey:label.title];
                                
                                
                             

                            }

                        
                        
                    }
                    
                    
                }
                
                
            }
            
            
        }
        
    }

    if ([UIImage imageWithContentsOfFile:[set valueForKey:@"Image"]]) {
           [headShot setImage:[UIImage imageWithContentsOfFile:[set valueForKey:@"Image"]]]; 
    }


    [NotesView setText:[ChosenMedicen valueForKey:@"Prescription"]];
    
    if ([UIImage imageWithContentsOfFile:[set valueForKey:@"PerscriptionImage"]]) {
    [MedShot setImage:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"PerscriptionImage"]]];
            
    }
    
    
    

  [ContendHead setText:[NSString stringWithFormat:@"%@\n%@", [set valueForKey:@"Strength"],[set valueForKey:@"Type"]]];
  
    [MedNamehead setText:[set valueForKey:@"Name"]];




    appDelegate.PerscriptionNotes = [set valueForKey:@"Prescription"];
    appDelegate.Notes = [set valueForKey:@"Notes"];

        //Prescription


    

    ////NSLog(@"ChosenMedicen %@", ChosenMedicen);
}





- (void) viewWillAppear:(BOOL)animated {
	
    
}

-(void)setNew

{
    
}

-(void)hideAndseek:(NSString*)set
{
    
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *) picker
{

 



    
	[picker dismissViewControllerAnimated:YES  completion:nil];

        ////NSLog(@"dismissViewControllerAnimated");


}


- (void) mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{

 



    
		[self dismissViewControllerAnimated:YES completion:NULL];
}




- (IBAction)showMediaPicker
{
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];

    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    [self presentViewController:mediaPicker animated:YES completion:NULL];
}


-(void) changeColorFridge
{
    LeveyPopColorView *pinview01 = (LeveyPopColorView *)[self.view  viewWithTag:3459148];
    if (pinview01) {

        [pinview01 removeFromSuperview];


    }
    else
        {


        color = [[LeveyPopColorView alloc] initWithTitle:@"Share Photo to..." options:NULL setIt:2];
            // lplv.isModal = NO;
        [color setTag:3459148];
        [color showInView:self.view animated:YES];
        [color getParent:self get:3];


        }

    
    
    
    
    
}




-(void)showalert

{
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"To start please click\n \"New User\" to create your\nprofile and start adding\nyour medication"];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464749];
    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"New user"];
    [alert show];
}

-(void)getParant:(ViewController*)parant

{
    parantIt = parant;
}


-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{

    if (index==1) {



    }

    else
        {

        }
        }

-(void) CreateAccount

{

}


- (void)setSelectedColor:(UIColor*)colorit{


    const CGFloat *components = CGColorGetComponents(colorit.CGColor);
     rood = components[0];
     groen = components[1];
     blauw = components[2];
     doorzicht = components[3];


    [ColorField setBackgroundColor:colorit];

    
}

-(void)setitemsDictionary:(NSString*)set name:(NSString*)name

{

    
    if (set) {
          [ChosenMedicen setObject:set forKey:name];
    }

    
}

- (IBAction)Cancel:(UIButton *)sender {


    [[self navigationController] popViewControllerAnimated:YES];

   
}


-(void)getParantGo:(UIViewController*)parant

{

    parantGo =parant;
    
}
- (IBAction)OK:(UIButton *)sender {

 
   
 
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) turnbuttons:(NSString*) setter

{

}

@end
