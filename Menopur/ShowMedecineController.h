//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeveyPopColorView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "BufferedNavigationController.h"
#import "MedicineViewController.h"
#import "Medicine.h"

#import "GAIDictionaryBuilder.h"

@protocol subViewDelegate;
@class CodePopView, LeveyPopColorView, StartViewController, MedicineViewController, TekstIDLabel;
@interface ShowMedecineController: UIViewController<UITextViewDelegate, UIAlertViewDelegate, MPMediaPickerControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, retain) NSMutableDictionary *ChosenMedicen;
@property (nonatomic, retain) StartViewController *parantIt;
@property  (nonatomic, retain) UITextView *NotesView;
@property (nonatomic, retain) UIViewController *parantGo;
@property (nonatomic, retain) UIImageView *headShot;
@property (nonatomic, retain) UIImageView *MedShot;
@property (nonatomic, retain) NSMutableDictionary *MedicineSpecs;
@property (nonatomic, retain) MPMusicPlayerController *musicPlayer;
@property (nonatomic, retain) NSMutableArray *selectedNotifictions;




@property  (nonatomic, retain) TekstIDLabel *ContendHead;
@property  (nonatomic, retain) TekstIDLabel *MedNamehead;

@property (nonatomic, strong) Medicine *chosenCourse;

@property (nonatomic, strong) IBOutlet UIView *Quantity;
@property (nonatomic, strong) IBOutlet UIView *Assign;
@property (nonatomic, strong) IBOutlet UIView *Schdules;
@property (nonatomic, strong) IBOutlet UIView *Meds;
@property (nonatomic, strong) IBOutlet UIView *Contacts;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollViewSpread;
@property (nonatomic, strong) TekstIDLabel *InstructionsExtra;
@property (nonatomic, strong) TekstIDLabel *Instructions;

@property (nonatomic, strong) UIView *Combi;
@property (nonatomic, strong) TekstIDLabel *TimesLabel;
@property (nonatomic, strong) TekstIDLabel *TimesLabelextra;

@property (nonatomic, retain) LeveyPopColorView *color;
@property (nonatomic, retain) UILabel *Nameit;
@property (nonatomic, retain) UILabel *Sound;
@property (nonatomic, retain) UILabel *ColorField;
@property (nonatomic, retain) NSString *chosenID;
- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)setItems:(Medicine*)set;
-(void)setNew;
-(void)getParant:(UIViewController*)parant;
-(void)DynamicCell:(NSInteger)difference;
-(void)getParantGo:(UIViewController*)parant;
-(void)setItemsBack:(NSDictionary*)set;
-(void)hideAndseek:(NSString*)set;
-(void)setitemsDictionary:(NSString*)set name:(NSString*)name;

@end
