//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "ShowMedecineController.h"
#import "MedicineViewController.h"
#import "StrengthViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AlertViewController.h"
#import "NameItemViewController.h"
#import "ProfileViewController.h"
#import "SaveData.h"
#import "TekstIDLabel.h"
#import "DocterViewController.h"
#import "AppDelegate.h"
#import "TypeViewController.h"
#import "PerscriptionViewController.h"
#import "RemainingViewController.h"
#import "DoseViewController.h"
#import "FillingViewController.h"
#import "NoteViewController.h"
#import "NewMedecineController.h"
#import "Notification.h"
#import "GetData.h"
#import "SaveCore.h"
#import "ImageViewController.h"
#import "ActionArrow.h"
@interface ShowMedecineController ()

@end

@implementation ShowMedecineController

@synthesize color;


@synthesize scrollViewSpread;

@synthesize musicPlayer;
@synthesize headShot;
@synthesize Nameit;
@synthesize Sound;
@synthesize ColorField;
@synthesize chosenID;
@synthesize parantIt;
@synthesize MedicineSpecs;
@synthesize ChosenMedicen;
@synthesize selectedNotifictions;
@synthesize parantGo;
@synthesize NotesView;
@synthesize ContendHead;
@synthesize MedNamehead;
@synthesize MedShot;
@synthesize chosenCourse;
@synthesize Combi;

@synthesize TimesLabel;
@synthesize TimesLabelextra;


@synthesize InstructionsExtra;
@synthesize Instructions;

@synthesize Quantity;
@synthesize Assign;
@synthesize Schdules;
@synthesize Meds;
@synthesize Contacts;


-(void) backAction
{
    
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    
    
    
    
}

- (void)viewDidLoad
{
    
    //Medicine Info
    
    //********** Google Analytics ****************
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"M_Medicine Info_iOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //********** Google Analytics ****************
    
    if ([SHARED_APPDELEGATE isNetworkReachable])
    {
        [SHARED_APPDELEGATE sendHitsInBackground];
    }

//    [self googleAnalyticsGroupping];
    
    [super viewDidLoad];
    
    [SHARED_APPDELEGATE setNavigationbarIconImage:self.navigationController setimge:NO];
    
    
    
    
    self.navigationItem.title = NSLocalizedString(@"Medicine Info", nil);
    
    
    UIBarButtonItem *edit = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                             target:self
                             action:@selector(Edit:)];
    edit.style = UIBarButtonItemStyleBordered;
    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [edit setTintColor:[UIColor whiteColor]];
        
        // this will appear as the title in the navigation bar
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        // ^-Use NSTextAlignmentCenter for older SDKs.
        label.textColor = [UIColor whiteColor]; // change this color
        label.numberOfLines=2;
        self.navigationItem.titleView = label;
        [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
        label.text = self.navigationItem.title;
        [label sizeToFit];
    }
    
    else
    {
        
        
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [edit setTintColor:[UIColor whiteColor]];
        
        
    }
    
    else
    {
        
    }
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = edit;
    
    
    
    
    selectedNotifictions  = [[NSMutableArray alloc]init];
    
    //Month_view.png
    
    
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
    }
    
    else
    {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
    
    
    
    musicPlayer = [MPMusicPlayerController iPodMusicPlayer];
    
    
    
    [musicPlayer beginGeneratingPlaybackNotifications];
    
    
    scrollViewSpread = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, self.view.frame.size.height-164)];
    [scrollViewSpread setCanCancelContentTouches:NO];
    scrollViewSpread.showsHorizontalScrollIndicator = YES;
//    [scrollViewSpread setBackgroundColor:[UIColor colorWithRed:0.886 green:0.937 blue:0.957 alpha:1.000]]; //03-07-2017 changes
    [scrollViewSpread setBackgroundColor:[UIColor colorWithRed:250.0/255.0f green:241.0f/255.0 blue:235.0f/255.0f alpha:1.0f]];
    
    scrollViewSpread.delaysContentTouches=YES;
    scrollViewSpread.clipsToBounds = YES;
    scrollViewSpread.scrollEnabled = YES;
    scrollViewSpread.pagingEnabled = NO;
    [scrollViewSpread setDelegate:self];
    scrollViewSpread.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.view  addSubview:scrollViewSpread];
    
    
    Combi = [[UIView alloc] initWithFrame:CGRectMake(-5, 0, self.view.frame.size.width+10, 120)];
    [Combi setBackgroundColor:[UIColor colorWithRed:0.733 green:0.851 blue:0.902 alpha:1.000]];
//    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];//28-06-2017 changes
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    Combi.layer.shadowOffset = CGSizeMake(3, 0);
    Combi.layer.shadowOpacity = 2;
    Combi.layer.shadowRadius = 2.0;
    [self.view addSubview:Combi];
    
    
    
    
    MedNamehead = [[TekstIDLabel alloc] initWithFrame:CGRectMake(125, 10, self.view.frame.size.width-130, 40)];
//    [MedNamehead setTextColor:[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000]]; //28-06-2017 changes
    [MedNamehead setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [MedNamehead setFont:[UIFont boldSystemFontOfSize:20]];
    [MedNamehead setTag:120];
    [MedNamehead setText:@""];
    [MedNamehead setBackgroundColor:[UIColor clearColor]];
    [MedNamehead setNumberOfLines:3];
    [Combi addSubview:MedNamehead];
    
    
    ContendHead = [[TekstIDLabel alloc] initWithFrame:CGRectMake(125, 39, self.view.frame.size.width-140, 60)];
//    [ContendHead setTextColor:[UIColor colorWithRed:0.102 green:0.373 blue:0.478 alpha:1.000]]; //28-06-2017 changes
    [ContendHead setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [ContendHead setFont:[UIFont systemFontOfSize:14]];
    [ContendHead setText:@"500 ml"];
    [ContendHead setTag:120];
    [ContendHead setBackgroundColor:[UIColor clearColor]];
    [ContendHead setNumberOfLines:4];
    [Combi addSubview:ContendHead];
    
    headShot = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 100, 100)];
//    [headShot setBackgroundColor:[UIColor blackColor]];
    [headShot setTag:130];
    [headShot setImage:[UIImage imageNamed:@"Medhot.png"]];
    //[headShot setImage:[UIImage imageNamed:@"headshot.png"]];
    
    
    
    [Combi addSubview:headShot];
    
    Quantity = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 196+39)];
    [Quantity setBackgroundColor:[UIColor whiteColor]];
    [Quantity.layer setCornerRadius:10];
    [Quantity.layer setBorderWidth:1.5];
//    [Quantity.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Quantity.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Quantity.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Quantity];
    
    
    
    
    
    
    
    
    NSArray *names4 =[NSArray arrayWithObjects:NSLocalizedString(@"Quantity Per Dose:",nil),NSLocalizedString(@"Frequency:",nil),NSLocalizedString(@"Starting:",nil),NSLocalizedString(@"Ending:",nil),NSLocalizedString(@"Time(s) per Day:",nil),NSLocalizedString(@"Instructions:",nil), nil];
    
    NSArray *Cell4 =[NSArray arrayWithObjects:@"Quantity#140",@"Frequency#90",@"Starting#72",@"Duration#76",@"Times#120",@"Instructions#120", nil];
    
    for (NSInteger i=0 ; i<[names4 count]; i++) {
        
        
        NSArray *numbers = [[Cell4 objectAtIndex:i] componentsSeparatedByString:@"#"];
        
        
        if ([[numbers objectAtIndex:0] isEqualToString:@"Instructions"]) {
            
            
            Instructions = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//            [Instructions setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 chanegs
            [Instructions setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [Instructions setFont:[UIFont boldSystemFontOfSize:14]];
            [Instructions setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names4 objectAtIndex:i], nil)]];
            [Instructions setTag:120];
            
            [Instructions setBackgroundColor:[UIColor clearColor]];
            [Instructions setNumberOfLines:3];
            [Quantity addSubview:Instructions];
            
            [Instructions sizeToFit];
            [Instructions setCenter:CGPointMake(Instructions.center.x, (39*i)+20)];
            
            
            InstructionsExtra = [[TekstIDLabel alloc] initWithFrame:CGRectMake(Instructions.frame.size.width+14, 39*i, self.view.frame.size.width-(Instructions.frame.size.width+50), Instructions.frame.size.height)];
            [InstructionsExtra setTextColor:[UIColor blackColor]];
            [InstructionsExtra setFont:[UIFont systemFontOfSize:14]];
            [InstructionsExtra setText:@""];
            [InstructionsExtra setTitle:[numbers objectAtIndex:0]];
            
            
            if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                
            [InstructionsExtra setTextAlignment:NSTextAlignmentLeft];
            }
            
            else
                
            {
                 [InstructionsExtra setTextAlignment:NSTextAlignmentRight];
            }
            
            [InstructionsExtra setBackgroundColor:[UIColor clearColor]];
            [InstructionsExtra setNumberOfLines:2];
            [Quantity addSubview:InstructionsExtra];
          
            
            
        }
        
        else if ([[numbers objectAtIndex:0] isEqualToString:@"Times"]) {
            
            
            TimesLabel = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//            [TimesLabel setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [TimesLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [TimesLabel setFont:[UIFont boldSystemFontOfSize:14]];
            [TimesLabel setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names4 objectAtIndex:i], nil)]];
            [TimesLabel setTag:120];
            
            [TimesLabel setBackgroundColor:[UIColor clearColor]];
            [TimesLabel setNumberOfLines:3];
            [Quantity addSubview:TimesLabel];
            
            
            
            [TimesLabel sizeToFit];
            [TimesLabel setCenter:CGPointMake(TimesLabel.center.x, (39*i)+20)];
            
            
            TimesLabelextra = [[TekstIDLabel alloc] initWithFrame:CGRectMake(TimesLabel.frame.size.width-30, 39*i, self.view.frame.size.width-(TimesLabel.frame.size.width+10), 40)];
            [TimesLabelextra setTextColor:[UIColor blackColor]];
            [TimesLabelextra setFont:[UIFont systemFontOfSize:14]];
            [TimesLabelextra setText:@""];
            [TimesLabelextra setTitle:[numbers objectAtIndex:0]];
            
               if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            [TimesLabelextra setTextAlignment:NSTextAlignmentLeft];
               }
            
            else
                
            {
                  [TimesLabelextra setTextAlignment:NSTextAlignmentRight];
            }
            
            [TimesLabelextra setBackgroundColor:[UIColor clearColor]];
            [TimesLabelextra setNumberOfLines:1];
            [Quantity addSubview:TimesLabelextra];
  
            
            
            
        }
        
        else if ([[numbers objectAtIndex:0] isEqualToString:@"Starting"]) {
            
            NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
            [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
            
            TimesLabel = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//            [TimesLabel setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [TimesLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [TimesLabel setFont:[UIFont boldSystemFontOfSize:14]];
            [TimesLabel setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names4 objectAtIndex:i], nil)]];
            [TimesLabel setTag:120];
            
            [TimesLabel setBackgroundColor:[UIColor clearColor]];
            [TimesLabel setNumberOfLines:3];
            [Quantity addSubview:TimesLabel];
            
            
            [TimesLabel sizeToFit];
            [TimesLabel setCenter:CGPointMake(TimesLabel.center.x, (39*i)+20)];
            
            TimesLabelextra = [[TekstIDLabel alloc] initWithFrame:CGRectMake(TimesLabel.frame.size.width+14, 39*i, self.view.frame.size.width-(TimesLabel.frame.size.width+50), 40)];
            [TimesLabelextra setTextColor:[UIColor blackColor]];
            [TimesLabelextra setFont:[UIFont systemFontOfSize:14]];
            [TimesLabelextra setText:@""];
            [TimesLabelextra setTitle:[numbers objectAtIndex:0]];
            
          if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
              
            [TimesLabelextra setTextAlignment:NSTextAlignmentLeft];
              
              
          }
            
            else
                
            {
                [TimesLabelextra setTextAlignment:NSTextAlignmentRight];
            }
            
            [TimesLabelextra setBackgroundColor:[UIColor clearColor]];
            [TimesLabelextra setNumberOfLines:1];
            [Quantity addSubview:TimesLabelextra];
           
            
            
            
        }
        
        else
            
        {
            
            if (i ==0) {
                
                
                TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//                [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
                [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                [MedName setFont:[UIFont boldSystemFontOfSize:14]];
                [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names4 objectAtIndex:i], nil)]];
                [MedName setTag:4444];
                
                [MedName setBackgroundColor:[UIColor clearColor]];
                [MedName setNumberOfLines:3];
                [Quantity addSubview:MedName];
                
                
                [MedName sizeToFit];
                [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];
                
                
                
                TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+14, 39*i, self.view.frame.size.width-(MedName.frame.size.width+50), 40)];
                [Name setTextColor:[UIColor blackColor]];
                [Name setFont:[UIFont systemFontOfSize:14]];
                [Name setText:@""];
                [Name setTitle:[numbers objectAtIndex:0]];
           
                   if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                            [Name setTextAlignment:NSTextAlignmentLeft];
                   }
                else
                    
                {
                         [Name setTextAlignment:NSTextAlignmentRight];
                }
                
                [Name setBackgroundColor:[UIColor clearColor]];
                [Name setNumberOfLines:1];
                [Quantity addSubview:Name];
                
                
                
            }
            else
            {
                
                TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//                [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
                [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                [MedName setFont:[UIFont boldSystemFontOfSize:14]];
                [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names4 objectAtIndex:i], nil)]];
                [MedName setTag:120];
                
                [MedName setBackgroundColor:[UIColor clearColor]];
                [MedName setNumberOfLines:3];
                [Quantity addSubview:MedName];
                
                
                [MedName sizeToFit];
                [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];
                
                
                
                
                
                TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+14, 39*i, self.view.frame.size.width-(MedName.frame.size.width+50), 40)];
                [Name setTextColor:[UIColor blackColor]];
                [Name setFont:[UIFont systemFontOfSize:14]];
                [Name setText:@""];
                [Name setTitle:[numbers objectAtIndex:0]];
                if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                    [Name setTextAlignment:NSTextAlignmentLeft];
                }
                else
                    
                {
                    [Name setTextAlignment:NSTextAlignmentRight];
                }

                
                [Name setBackgroundColor:[UIColor clearColor]];
                [Name setNumberOfLines:1];
                [Quantity addSubview:Name];
                
                
                
            }
            
            
            
        }


        if (i==5) {

            UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//            [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
                    [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [Quantity addSubview:line];
            [line setTag:46664];
        }

        else
        {
        UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//        [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [Quantity addSubview:line];

        }
        
    }
    
    
    Assign = [[UIView alloc] initWithFrame:CGRectMake(10, 216+39, self.view.frame.size.width-20, 40)];
    [Assign setBackgroundColor:[UIColor whiteColor]];
    [Assign.layer setCornerRadius:10];
    [Assign.layer setBorderWidth:1.5];
//    [Assign.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Assign.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Assign.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Assign];
    
    
    NSArray *names2 =[NSArray arrayWithObjects:@"Assign to Profile:", nil];
    NSArray *Cell2 =[NSArray arrayWithObjects:@"Profile#130", nil];
    
    for (NSInteger i=0 ; i<[names2 count]; i++) {
        
        NSArray *numbers = [[Cell2 objectAtIndex:i] componentsSeparatedByString:@"#"];
        
        
        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names2 objectAtIndex:i], nil)]];
        [MedName setTag:120];
        
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Assign addSubview:MedName];
        
        
        
        [MedName sizeToFit];
        [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];
        
        
        
        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+14, 39*i, self.view.frame.size.width-(MedName.frame.size.width+50), 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTitle:[numbers objectAtIndex:0]];
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            [Name setTextAlignment:NSTextAlignmentLeft];
        }
        else
            
        {
            [Name setTextAlignment:NSTextAlignmentRight];
        }

        
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:1];
        [Assign addSubview:Name];
        
        
        
        
        
    }
    
    
    
    
    Schdules = [[UIView alloc] initWithFrame:CGRectMake(10, 266+39, self.view.frame.size.width-20, 39)];
    [Schdules setBackgroundColor:[UIColor whiteColor]];
    [Schdules.layer setCornerRadius:10];
    [Schdules.layer setBorderWidth:1.5];
//    [Schdules.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; // 28-06-2017 changes
    [Schdules.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Schdules.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Schdules];
    
    
    NSArray *names3 =[NSArray arrayWithObjects: NSLocalizedString(@"Alert Message Text:",nil),nil];
    NSArray *Cell3 =[NSArray arrayWithObjects: @"Message#165",  nil];
    
    
    for (NSInteger i=0 ; i<[names3 count]; i++) {
        
        
        NSArray *numbers = [[Cell3 objectAtIndex:i] componentsSeparatedByString:@"#"];
        
        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[NSString stringWithFormat:@"%@", [names3 objectAtIndex:i]]];
        [MedName setTag:120];
        
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:2];
        [Schdules addSubview:MedName];
        
        
        [MedName sizeToFit];
        [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];
        
        
        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+14, 39*i, self.view.frame.size.width-(MedName.frame.size.width+50), 40)];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setTitle:[numbers objectAtIndex:0]];
        [Name setTextColor:[UIColor blackColor]];
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            [Name setTextAlignment:NSTextAlignmentLeft];
        }
        else
            
        {
            [Name setTextAlignment:NSTextAlignmentRight];
        }

        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:2];
        [Schdules addSubview:Name];
        
 
        
        
        
        UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//        [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [Quantity addSubview:line];
        
        
        
        
        
    }
    
    
    Meds = [[UIView alloc] initWithFrame:CGRectMake(10, 356+39, self.view.frame.size.width-20, 118)];
    [Meds setBackgroundColor:[UIColor whiteColor]];
    [Meds.layer setCornerRadius:10];
    [Meds.layer setBorderWidth:1.5];
//    [Meds.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Meds.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Meds.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Meds];
    
    
    
    
    NSArray *names5 =[NSArray arrayWithObjects:@"Quantity Remaining:",@"Quantity Per Filling:",@"Minimum Quantity\n   For Refilling:", nil];
    
    NSArray *Cell5 =[NSArray arrayWithObjects:@"Remaining#182",@"Filling#180",@"Refilling#192", nil];
    
    for (NSInteger i=0 ; i<[names5 count]; i++) {
        
        NSArray *numbers = [[Cell5 objectAtIndex:i] componentsSeparatedByString:@"#"];
        
        
        if (i ==0) {
            
            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:14]];
            [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names5 objectAtIndex:i], nil)]];
            [MedName setTag:4445];
            
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Meds addSubview:MedName];
            
            [MedName sizeToFit];
            [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];
            

            MedName.centerit =39*i;

            UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width, 1)];
//            [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [Meds addSubview:line];
            
            TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+12, 39*i, self.view.frame.size.width-(MedName.frame.size.width+50), 40)];
            [Name setTextColor:[UIColor blackColor]];
            [Name setFont:[UIFont systemFontOfSize:14]];
            [Name setText:@""];
            if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                [Name setTextAlignment:NSTextAlignmentLeft];
            }
            else
                
            {
                [Name setTextAlignment:NSTextAlignmentRight];
            }

            [Name setTitle:[numbers objectAtIndex:0]];
            [Name setBackgroundColor:[UIColor clearColor]];
            [Name setNumberOfLines:1];
            [Meds addSubview:Name];
            
            
            
        }
        else if (i ==1) {
            
            
            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];//28-06-2017 changes
            [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:14]];
            [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names5 objectAtIndex:i], nil)]];
            [MedName setTag:4446];
            
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Meds addSubview:MedName];
            
            [MedName sizeToFit];
            [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];
            
            

            MedName.centerit =39*i;

            UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width, 1)];
//            [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 chanegs
            [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [Meds addSubview:line];
            
            
            TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+12, 39*i, self.view.frame.size.width-(MedName.frame.size.width+50), 40)];
            [Name setTextColor:[UIColor blackColor]];
            [Name setFont:[UIFont systemFontOfSize:14]];
            [Name setText:@""];
            if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                [Name setTextAlignment:NSTextAlignmentLeft];
            }
            else
                
            {
                [Name setTextAlignment:NSTextAlignmentRight];
            }

            [Name setTitle:[numbers objectAtIndex:0]];
            [Name setBackgroundColor:[UIColor clearColor]];
            [Name setNumberOfLines:1];
            [Meds addSubview:Name];
            
        }
        else if (i ==2) {
            
            
            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:14]];
            [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names5 objectAtIndex:i], nil)]];
            [MedName setTag:4447];
            
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Meds addSubview:MedName];
            
            [MedName sizeToFit];
            [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];
            

            MedName.centerit =39*i;

            UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//            [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [Meds addSubview:line];
            
            TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+12, 39*i, self.view.frame.size.width-(MedName.frame.size.width+50), 40)];
            [Name setTextColor:[UIColor blackColor]];
            [Name setFont:[UIFont systemFontOfSize:14]];
            [Name setText:@""];
            if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                [Name setTextAlignment:NSTextAlignmentLeft];
            }
            else
                
            {
                [Name setTextAlignment:NSTextAlignmentRight];
            }

            [Name setTitle:[numbers objectAtIndex:0]];
            [Name setBackgroundColor:[UIColor clearColor]];
            [Name setNumberOfLines:1];
            [Meds addSubview:Name];
            
           
        }
        else
        {
            
            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:14]];
            [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names5 objectAtIndex:i], nil)]];
            [MedName setTag:120];
            
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Meds addSubview:MedName];
            
            [MedName sizeToFit];
            [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];


            MedName.centerit =39*i;

            TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+12, 39*i, self.view.frame.size.width-(MedName.frame.size.width+50), 40)];
            [Name setTextColor:[UIColor blackColor]];
            [Name setFont:[UIFont systemFontOfSize:14]];
            [Name setText:@""];
            if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                [Name setTextAlignment:NSTextAlignmentLeft];
            }
            else
                
            {
                [Name setTextAlignment:NSTextAlignmentRight];
            }

            [Name setTitle:[numbers objectAtIndex:0]];
            [Name setBackgroundColor:[UIColor clearColor]];
            [Name setNumberOfLines:1];
            [Meds addSubview:Name];
            
          
            
            
            
        }
        
        
        
        
        
        
        
        
    }
    
    
    
    Contacts = [[UIView alloc] initWithFrame:CGRectMake(10, 484+39, self.view.frame.size.width-20, 118+300)];
    [Contacts setBackgroundColor:[UIColor whiteColor]];
    [Contacts.layer setCornerRadius:10];
    [Contacts.layer setBorderWidth:1.5];
//    [Contacts.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Contacts.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Contacts.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Contacts];
    
    
    
    
    NSArray *names6 =[NSArray arrayWithObjects:@"Doctor:",@"Phone Doctor:",@"Mail Doctor:",@"Pharmacy:",@"Phone Pharmacy:",@"Mail Pharmacy:",   @"Prescription Notes:", nil];
    
    NSArray *Cell6 =[NSArray arrayWithObjects:@"Doctor#65",@"Phone_Doctor#120",@"eMail_Doctor#100",@"Pharmacy#172",@"Phone_Pharmacy#172",@"eMail_Pharmacy#142",  @"Prescription#60", nil];
    
    for (NSInteger i=0 ; i<[names6 count]; i++) {
        
        
        
        
        NSArray *numbers = [[Cell6 objectAtIndex:i] componentsSeparatedByString:@"#"];
        
        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(12, 39*i, self.view.frame.size.width-20, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names6 objectAtIndex:i],nil)]];
        [MedName setTag:120];
        
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Contacts addSubview:MedName];
        
        
        [MedName sizeToFit];
        [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];
        
        
        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+12, 39*i, self.view.frame.size.width-(MedName.frame.size.width+50), 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        [Name setTitle:[numbers objectAtIndex:0]];
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            [Name setTextAlignment:NSTextAlignmentLeft];
        }
        else
            
        {
            [Name setTextAlignment:NSTextAlignmentRight];
        }

        
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:1];
        [Contacts addSubview:Name];
        
        
        
        UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//        [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [Contacts addSubview:line];
        
        
        if ([[numbers objectAtIndex:0] isEqualToString:@"Phone_Doctor"]||[[numbers objectAtIndex:0] isEqualToString:@"eMail_Doctor"]||[[numbers objectAtIndex:0] isEqualToString:@"Phone_Pharmacy"]||[[numbers objectAtIndex:0] isEqualToString:@"eMail_Pharmacy"]) {
            
            UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(0, (39*i), self.view.frame.size.width-20, 40)];
            [NameButton setTitle:[numbers objectAtIndex:0] forState:UIControlStateNormal];
            [NameButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [NameButton addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchUpInside];
            [NameButton setBackgroundColor:[UIColor clearColor]];
            [Contacts addSubview:NameButton];
        }
        
        if (i==6) {
            
            [Name setAlpha:0];
        }
        
        
        
    }
    
        NotesView = [[UITextView alloc] initWithFrame:CGRectMake(100, 282, self.view.frame.size.width-130, 120)];
    [NotesView setTextColor:[UIColor blackColor]];
    [NotesView setFont:[UIFont systemFontOfSize:14]];
    [NotesView setText:@""];
    [NotesView setTag:120];
    [NotesView setBackgroundColor:[UIColor clearColor]];
    [NotesView setEditable:NO];
    [Contacts addSubview:NotesView];
    
    
    MedShot = [[UIImageView alloc] initWithFrame:CGRectMake(10, 282, 80, 80)];
    [MedShot setBackgroundColor:[UIColor grayColor]];
    [MedShot setImage:[UIImage imageNamed:@"Prescription.png"]];
    [MedShot setTag:130];
    [Contacts addSubview:MedShot];
    
    
    
    
    
    UIButton *NameButton2 = [[UIButton alloc]  initWithFrame:CGRectMake(10, 282, 80, 80)];
    [NameButton2 setBackgroundColor:[UIColor clearColor]];
    [NameButton2 addTarget:self action:@selector(Getimage:) forControlEvents:UIControlEventTouchUpInside];
    [Contacts addSubview:NameButton2];
    
    
    
    
    [scrollViewSpread setContentSize:CGSizeMake(320, 780+(39*7))];
    
    //142 × 36 pixels
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        //NSLog(@"left");
        
        
        
        
        for (UIView *content in scrollViewSpread.subviews) {
            
            
            if ([content isKindOfClass:[UIView class]]) {
                
                content.transform = CGAffineTransformMakeScale(-1, 1);
                
                
                for (UILabel *label in content.subviews) {
                    
                    
                    if ([label isKindOfClass:[UILabel class]]) {
                        
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                  
                        
                    }
                    
                }
                for (TekstIDLabel *label in content.subviews) {
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                     
                        
                    }
                    
                }
                for (UIButton *label in content.subviews) {
                    
                    
                    if ([label isKindOfClass:[UIButton class]]) {
                        
                        label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                  
                        
                    }
                    
                }
                
                
            }
            
        }
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }
    
    
    [self.view.layer setMasksToBounds:YES];
    
    

    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)DynamicCell:(NSInteger)difference

{
    
    //////////////////////////////NSLog(@"DynamicCell %i", difference);
    
    [Quantity setFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 196+(39*difference))];
    
    
    [Assign setFrame:CGRectMake(10, 20+196+(39*difference), Assign.frame.size.width, Assign.frame.size.height)];
    
    [Schdules setFrame:CGRectMake(10,  Assign.frame.origin.y+Assign.frame.size.height+10,  Schdules.frame.size.width, Schdules.frame.size.height)];
    [Meds setFrame:CGRectMake(10,  Schdules.frame.origin.y+Schdules.frame.size.height+10,  Meds.frame.size.width, Meds.frame.size.height)];
    [Contacts setFrame:CGRectMake(10,  Meds.frame.origin.y+Meds.frame.size.height+10,  Contacts.frame.size.width, Contacts.frame.size.height)];
    
    
    [Instructions setFrame:CGRectMake(Instructions.frame.origin.x,196+(39*difference)-40, Instructions.frame.size.width, 40)];
    [InstructionsExtra setFrame:CGRectMake(InstructionsExtra.frame.origin.x, 196+(39*difference)-40, InstructionsExtra.frame.size.width,39)];
    
    
    [TimesLabelextra setFrame:CGRectMake(TimesLabelextra.frame.origin.x, TimesLabelextra.frame.origin.y, TimesLabelextra.frame.size.width, 15+(20*difference))];
    
    [TimesLabelextra setNumberOfLines:difference];
    
    [scrollViewSpread setContentSize:CGSizeMake(320, Contacts.frame.origin.y+Contacts.frame.size.height+100)];
    

    UILabel *line = (UILabel *)[Quantity viewWithTag:(46664)];

    [line setFrame:CGRectMake(0,  Quantity.frame.size.height-40, self.view.frame.size.width-20, 1)];


}


-(void)Getimage:(UIButton*)sender
{
    
    ImageViewController *controller = [[ImageViewController alloc]init];
    [self presentViewController:controller animated:YES completion:Nil];
    
    if ([UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"PerscriptionImage"]]) {
        [controller getStamp:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"PerscriptionImage"]] tegit:1];
    }
    else{
        
        
    }
    
    
}


-(void)action:(UIButton*)sender

{
    
    
    
    [sender setBackgroundColor:[UIColor grayColor]];
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [sender setBackgroundColor:[UIColor clearColor]];
        
    });
    
    UIDevice *device = [UIDevice currentDevice];
    
    if ([sender.titleLabel.text isEqualToString:@"Phone_Doctor"]) {
        
        if ([[device model] isEqualToString:@"iPhone"] ) {
            
            NSString *text =[ChosenMedicen valueForKey:@"Phone_Doctor"];
            
            if (text != nil) {
                NSError *error = NULL;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
                NSArray *matches = [detector matchesInString:text options:0 range:NSMakeRange(0, [text length])];
                if (matches != nil) {
                    for (NSTextCheckingResult *match in matches) {
                        if ([match resultType] == NSTextCheckingTypePhoneNumber) {
                            
                            
                            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",[[match phoneNumber]stringByReplacingOccurrencesOfString:@" " withString:@""]]];
                            [[UIApplication sharedApplication] openURL:url];
                        }
                    }
                }
            }
            
            
            
        }
        else
        {
            /* //27-06-2017 changes
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Your device doesn't support this feature.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
            [Notpermitted show];
            */
            
            UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Your device doesn't support this feature.",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                
            });

            
        }
        
        
    }
    
    else  if ([sender.titleLabel.text isEqualToString:@"eMail_Doctor"]) {
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate SendtheMailtoAdress:[ChosenMedicen valueForKey:@"eMail_Doctor"]];
    }
    
    else  if ([sender.titleLabel.text isEqualToString:@"Phone_Pharmacy"]) {
        
        if ([[device model] isEqualToString:@"iPhone"] ) {
            
            
            
            
            NSString *text =[ChosenMedicen valueForKey:@"Phone_Pharmacy"];
            
            
            
            if (text != nil) {
                NSError *error = NULL;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
                NSArray *matches = [detector matchesInString:text options:0 range:NSMakeRange(0, [text length])];
                if (matches != nil) {
                    for (NSTextCheckingResult *match in matches) {
                        if ([match resultType] == NSTextCheckingTypePhoneNumber) {
                            
                            
                            
                            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",[[match phoneNumber]stringByReplacingOccurrencesOfString:@" " withString:@""]]];
                            [[UIApplication sharedApplication] openURL:url];
                        }
                    }
                }
            }
            
            
            
        }
        else
        {
            /* //27-06-2017 changes
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Your device doesn't support this feature.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
            [Notpermitted show];
            */
            
            UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"Your device doesn't support this feature.",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                
            });

        }
        
    }
    else  if ([sender.titleLabel.text isEqualToString:@"eMail_Pharmacy"]) {
        
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate SendtheMailtoAdress:[ChosenMedicen valueForKey:@"eMail_Pharmacy"]];
        
        
    }
    
    
    
    
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
    
    //////////////////     [self.view.layer setMasksToBounds:YES];
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate seeIfNotification];
    [appDelegate.toolbarDown setAlpha:1];
    
    
    
    
}


-(void)Edit:(UIButton*) sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([appDelegate SeePostpone:appDelegate.chosenCourse]) {
        
        ////////////////////////////////////////////////////////NSLog(@"SeePostpone");
        
        /* //27-06-2017 changes
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"This medicine is postponed, go back and press undo before editing, or history will not report correctly!",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
        [Notpermitted show];
        */
        
        UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CFDaily",nil) message:NSLocalizedString(@"This medicine is postponed, go back and press undo before editing, or history will not report correctly!",nil) preferredStyle:UIAlertControllerStyleAlert];
        [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
            
        });

        
    }
    else
    {
        
        ////////////////////////////////////////////////////////NSLog(@"not post");
        
        // ******************** Google Analytics Grouping ******************
        
        id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
        [tracker set:[GAIFields contentGroupForIndex:2]value:@"M_Edit Medicine_iOS"];
        
        // ******************** Google Analytics Grouping ******************
        
        NewMedecineController *controller2 = [[NewMedecineController alloc]init];
        [self.navigationController pushViewController:controller2 animated:YES];
        
        
        [controller2 setItems:ChosenMedicen];
        [controller2 editOrShow:@"Edit"];
        
    }
}


- (IBAction)showMediaPicker:(id)sender
{
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];
    
    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    
    [self presentViewController:mediaPicker animated:YES completion:NULL];
    
}

-(void)Next:(UIButton*)sender

{
    
}

- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{
    
    MPMediaItem *anItem = (MPMediaItem *)[mediaItemCollection.items objectAtIndex:0];
    
    NSURL *assetURL = [anItem valueForProperty: MPMediaItemPropertyAssetURL];
    
    
    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDir = [NSString stringWithFormat:@"%@/Caches/",[paths2 objectAtIndex:0]];
    
    
    NSData *data2 = [NSData dataWithContentsOfURL:assetURL];
    
    [data2 writeToFile:[NSString stringWithFormat:@"%@/Copy.mp3", documentsDir] atomically:YES];
    
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
}



-(void)getCameraPicture:(UIButton*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
#if (TARGET_IPHONE_SIMULATOR)
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
#endif
    [self presentViewController: picker animated:YES completion:NULL];
}



-(void)imagePickerController:(UIImagePickerController *)picker
      didFinishPickingImage : (UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{
    
    
    
    
    
    
    [headShot setImage:[self thumbWithSideOfLength2:120 image:image]];
    [ChosenMedicen setObject:headShot.image forKey:@"Image"];
    
    
    UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}


- (UIImage *)thumbWithSideOfLength2:(float)length image:(UIImage*)mainImage {
    
    
    
    UIImage *thumbnail;
    
    if (mainImage.size.width< length) {
        
        CGSize itemSiz1 = CGSizeMake(mainImage.size.width*(length/mainImage.size.width), mainImage.size.height*(length/mainImage.size.width));
        
        UIGraphicsBeginImageContextWithOptions(itemSiz1, NO, 0.0);
        
        CGRect imageRect2 = CGRectMake(0.0, 0.0, itemSiz1.width, itemSiz1.height);
        [mainImage drawInRect:imageRect2];
        
        mainImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    UIImageView *mainImageView = [[UIImageView alloc] initWithImage:mainImage];
    BOOL widthGreaterThanHeight = (mainImage.size.width > mainImage.size.height);
    float sideFull = (widthGreaterThanHeight) ? mainImage.size.height : mainImage.size.width;
    CGRect clippedRect = CGRectMake(0, 0, sideFull, sideFull);
    
    UIGraphicsBeginImageContext(CGSizeMake(length, length));
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextClipToRect( currentContext, clippedRect);
    CGFloat scaleFactor = length/sideFull;
    if (widthGreaterThanHeight) {
        
        CGContextTranslateCTM(currentContext, -((mainImage.size.width-sideFull)/2)*scaleFactor, 0);
        
    }
    else {
        CGContextTranslateCTM(currentContext, 0, -((mainImage.size.height - sideFull) / 2) * scaleFactor);
    }
    
    CGContextScaleCTM(currentContext, scaleFactor, scaleFactor);
    [mainImageView.layer renderInContext:currentContext];
    thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return thumbnail;
    
}


-(void)setItems:(Medicine*)set

{
    NSLog(@"setItems");
    
    NSArray *typit = [[GetData getDrugTypeStrength:set.type] objectAtIndex:0];
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.chosenCourse =set;
    
    
    
    
    ChosenMedicen =[[NSMutableDictionary alloc]init];
    
    ChosenMedicen = [SaveCore getMedicinewithID:set.id_medicine];
    
    
    
    
    appDelegate.TypeMed =set.type;
    
    
    if ([[typit valueForKey:@"Dose_name"] isEqualToString:@"-"]) {
        
        TekstIDLabel *Label = (TekstIDLabel *)[self.view viewWithTag:(4444)];
        TekstIDLabel *Label1 = (TekstIDLabel *)[Meds viewWithTag:(4445)];
        TekstIDLabel *Label2 = (TekstIDLabel *)[Meds viewWithTag:(4446)];
        TekstIDLabel *Label3 = (TekstIDLabel *)[Meds viewWithTag:(4447)];
        
        
        [Label setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Quantity Not relevant:",nil)]];
        [Label1 setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Applications Remaining:",nil)]];
        [Label2 setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Applications Per Refill:",nil)]];
        [Label3 setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Minimum Applications\nFor Refilling:",nil)]];
        
        
        

        [self setlabelframe:Label];
        [self setlabelframe:Label1];
        [self setlabelframe:Label2];
        [self setlabelframe:Label3];
        
    }
    else
    {
        TekstIDLabel *Label = (TekstIDLabel *)[self.view viewWithTag:(4444)];
        TekstIDLabel *Label1 = (TekstIDLabel *)[Meds viewWithTag:(4445)];
        TekstIDLabel *Label2 = (TekstIDLabel *)[Meds viewWithTag:(4446)];
        TekstIDLabel *Label3 = (TekstIDLabel *)[Meds viewWithTag:(4447)];
        
        if ([[ChosenMedicen valueForKey:@"Quantity"] integerValue]>1)
        {
            
            
            NSString *uitzondering =[[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_less"] withString:[typit valueForKey:@"Drug_types"]];
            
            
            //////////////////////NSLog(@"%@", uitzondering);
            
            
            
            [Label setText:[NSString stringWithFormat:@"%@%@", [uitzondering stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"], NSLocalizedString(@":",nil)]];
            
        }
        
        else
        {
            
            [Label setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Dose_name"], NSLocalizedString(@":",nil)]];
            
            
            
        }  [Label1 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Remaining_name"], NSLocalizedString(@":",nil)]];
        
        
        if ([[ChosenMedicen valueForKey:@"Remaining"] integerValue]>1)
        {
            
            [Label1 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Remaining_name"], NSLocalizedString(@":",nil)]];
            
            
            
        }
        
        else
        {
            
            NSString *uitzondering =[[typit valueForKey:@"Remaining_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_types"] withString:[typit valueForKey:@"Drug_less"]];
            
            
            [Label1 setText:[NSString stringWithFormat:@"%@%@", [uitzondering stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"], NSLocalizedString(@":",nil)]];
            
        }
        
        
        if ([[ChosenMedicen valueForKey:@"Filling"] integerValue]>1)
        {
            
            
            [Label2 setText:[NSString stringWithFormat:@"%@", [typit valueForKey:@"Refill_name"]]];
            
        }
        
        else
        {
            NSString *uitzondering =[[typit valueForKey:@"Refill_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_types"] withString:[typit valueForKey:@"Drug_less"]];
            
            
            [Label2 setText:[NSString stringWithFormat:@"%@%@", [uitzondering stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"] , NSLocalizedString(@":",nil)]];
            
            
            
        }
        if ([[ChosenMedicen valueForKey:@"Refilling"] integerValue]>1)
        {
            
            
            [Label3 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Minimume_name"], NSLocalizedString(@":",nil)]];
            
            [Label3 setFrame:CGRectMake(12, Label3.frame.origin.y, Label3.frame.size.width,  Label3.frame.size.height)];
            
        }
        
        else
        {
            NSString *uitzondering =[[typit valueForKey:@"Minimume_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_types"] withString:[typit valueForKey:@"Drug_less"]];
            
            
            [Label3 setText:[NSString stringWithFormat:@"%@%@", [uitzondering stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"], NSLocalizedString(@":",nil)]];
            
            [Label3 setFrame:CGRectMake(12, Label3.frame.origin.y, Label3.frame.size.width,  Label3.frame.size.height)];
            
            
        }
        
        
        [self setlabelframe:Label];
        [self setlabelframe:Label1];
        [self setlabelframe:Label2];
        [self setlabelframe:Label3];

    }
    
    
    [self builditems:ChosenMedicen];
    
    
    if ([UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"Image"]]) {
        [headShot setImage:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"Image"]]];
    }
    
    
    [NotesView setText:[ChosenMedicen valueForKey:@"Prescription"]];
    
    if ([[ChosenMedicen valueForKey:@"PerscriptionImage"] length]>6) {
        [MedShot setImage:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"PerscriptionImage"]]];
        
        
    }
    
  
    [ContendHead setText:[NSString stringWithFormat:@"%@ %@\n%@", [ChosenMedicen valueForKey:@"Strength"],[ChosenMedicen valueForKey:@"Strength_unit"],[ChosenMedicen valueForKey:@"Type"]]];
    
    [MedNamehead setText:[ChosenMedicen valueForKey:@"Name"]];
    
    
    
    
    appDelegate.PerscriptionNotes = [ChosenMedicen valueForKey:@"Prescription"];
    appDelegate.Notes = [ChosenMedicen valueForKey:@"Notes"];
   
    
}


-(void)setlabelframe:(TekstIDLabel*)sender

{


    [sender setFrame:CGRectMake(12,sender.center.y-(sender.frame.size.height/2), self.view.frame.size.width, 39)];
    [sender sizeToFit];

    [sender setFrame:CGRectMake(12, sender.center.y-(sender.frame.size.height/2),  sender.frame.size.width, sender.frame.size.height)];
    [sender setCenter:CGPointMake(sender.center.x, sender.centerit+20)];
    
}



-(void)setItemsBack:(NSDictionary*)set

{
    
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.strength_unit =  NSLocalizedString([set valueForKey:@"Strength_unit"],nil);
    appDelegate.dose_unit = NSLocalizedString([set valueForKey:@"Dose_unit"],nil);
    
    
    
    
    appDelegate.TypeMed =[set valueForKey:@"Type"];
    
    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
    
    ChosenMedicen =[[NSMutableDictionary alloc]init];
    
    ChosenMedicen = [SaveCore getMedicinewithID:[set valueForKey:@"ID_Medicine"]];
    
    
    
    
    [self builditems:ChosenMedicen];
    
    
    if ([[typit valueForKey:@"Dose_name"] isEqualToString:@"-"]) {
        
        TekstIDLabel *Label = (TekstIDLabel *)[self.view viewWithTag:(4444)];
        TekstIDLabel *Label1 = (TekstIDLabel *)[Meds viewWithTag:(4445)];
        TekstIDLabel *Label2 = (TekstIDLabel *)[Meds viewWithTag:(4446)];
        TekstIDLabel *Label3 = (TekstIDLabel *)[Meds viewWithTag:(4447)];
        
        
        [Label setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Applications Not relevant:",nil)]];
        [Label1 setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Applications Remaining:",nil)]];
        [Label2 setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Applications Per Refill:",nil)]];
        [Label3 setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Minimum Applications\nFor Refilling:",nil)]];
        
        
        
        
    }
    else
    {
        TekstIDLabel *Label = (TekstIDLabel *)[self.view viewWithTag:(4444)];
        TekstIDLabel *Label1 = (TekstIDLabel *)[Meds viewWithTag:(4445)];
        TekstIDLabel *Label2 = (TekstIDLabel *)[Meds viewWithTag:(4446)];
        TekstIDLabel *Label3 = (TekstIDLabel *)[Meds viewWithTag:(4447)];
        
        
        
        NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
        
        if ([[ChosenMedicen valueForKey:@"Quantity"] integerValue]>1)
        {
            
            
            
            NSString *uitzondering =[[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_types"] withString:[typit valueForKey:@"Drug_types"]];
            
            
            [Label setText:[NSString stringWithFormat:@"%@%@", uitzondering, NSLocalizedString(@":",nil)]];
            
        }
        
        else
        {
            
            NSString *uitzondering =[[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_less"] withString:[typit valueForKey:@"Drug_types"]];
            
            
            [Label setText:[NSString stringWithFormat:@"%@%@", [uitzondering stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"], NSLocalizedString(@":",nil)]];
            
            
        }
        
        [Label setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Dose_name"], NSLocalizedString(@":",nil)]];
        [Label1 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Remaining_name"], NSLocalizedString(@":",nil)]];
        
        
        [Label2 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Refill_name"], NSLocalizedString(@":",nil)]];
        [Label3 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Minimume_name"], NSLocalizedString(@":",nil)]];


        [self setlabelframe:Label];
        [self setlabelframe:Label1];
        [self setlabelframe:Label2];
        [self setlabelframe:Label3];
        
        
        
    }







    if ([UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"Image"]]) {
        [headShot setImage:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"Image"]]];
    }
    
    
    [NotesView setText:[ChosenMedicen valueForKey:@"Prescription"]];
    
    if ([UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"PerscriptionImage"]]) {
        [MedShot setImage:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"PerscriptionImage"]]];
        
    }
    
    
    
    [ContendHead setText:[NSString stringWithFormat:@"%@ %@\n%@", [ChosenMedicen valueForKey:@"Strength"],[ChosenMedicen valueForKey:@"Strength_unit"],[ChosenMedicen valueForKey:@"Type"]]];
    
    
    [MedNamehead setText:[ChosenMedicen valueForKey:@"Name"]];
    
    
    
    
    
    appDelegate.PerscriptionNotes = [ChosenMedicen valueForKey:@"Prescription"];
    appDelegate.Notes = [ChosenMedicen valueForKey:@"Notes"];
    
    //Prescription
    
    
    
    
    
}

#pragma mark - builditmes

-(void)builditems:(NSDictionary*)set

{
    
    
    
NSLog(@"%@", ChosenMedicen);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
    
    
    
    
    for (UIScrollView *scroll in self.view.subviews) {
        
        for (UIView *view in scroll.subviews) {
            
            
            for (UIButton *button in view.subviews) {
                
                
                if ([button isKindOfClass:[UIButton class]]) {
                    
                    [button setAlpha:1];
                    
                    
                    if ([[typit valueForKey:@"Dose_name"] isEqualToString:@"-"]) {
                        
                        if ([button.titleLabel.text isEqualToString:@"Quantity"]) {
                            ////////////////////////////////////////////////////////////////////////NSLog(@"button %@", button.titleLabel.text);
                            [button setAlpha:0];
                        }
                    }
                    else
                    {
                        
                        if ([button.titleLabel.text isEqualToString:@"Quantity"]) {
                            ////////////////////////////////////////////////////////////////////////NSLog(@"button %@", button.titleLabel.text);
                            [button setAlpha:1];
                        }
                    }
                    
                }
            }
            
            for (TekstIDLabel *label in view.subviews) {
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    
                    if (label.title) {
                        
                        
                        
                        if ([label.title isEqualToString:@"Frequency"]) {
                            
                            
                            
                            if ([NSLocalizedString([ChosenMedicen valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Daily",nil)]) {
                                
                                //////////////////NSLog(@"Daily %@", [ChosenMedicen valueForKey:@"Frequency_text"]);
                                
                                //Frequency
                                
                                
                                
                                [label setText:[ChosenMedicen valueForKey:@"Frequency_text"]];
                            }
                            else if ([NSLocalizedString([ChosenMedicen valueForKey:@"Frequency_text"],nil)isEqualToString:NSLocalizedString(@"Weekly",nil)]) {
                                
                                [label setText:[NSString stringWithFormat:@"%@\n%@", [ChosenMedicen valueForKey:@"Frequency_text"],[ChosenMedicen valueForKey:@"Dates"]]];
                                [label setNumberOfLines:2];
                            }
                            else
                            {
                                
                                NSMutableArray *timesIt = [[[ChosenMedicen valueForKey:@"Frequency_text"] componentsSeparatedByString:@" "] mutableCopy];
                                
                                [timesIt replaceObjectAtIndex:1 withObject:[ChosenMedicen valueForKey:@"Frequency"]];
                                    [label setText:[NSString stringWithFormat:@"%@ %@ %@", [timesIt objectAtIndex:0],[timesIt objectAtIndex:1],[timesIt objectAtIndex:2]]];

                                
                                
                                
                                
                                
                            }
                            
                            //////////////////NSLog(@"%@", label);
                            
                            
                        }
                        if ([label.title isEqualToString:@"Recording"]) {
                            
                            
                            if ([[ChosenMedicen valueForKey:@"Recording"] isEqualToString:@"444"]) {
                                
                                [label setText:@" "];
                            }
                            else
                            {
                                
                                
                                
                                [label setText:[[[GetData getRecording] valueForKey:@"title"]  objectAtIndex:[[[GetData getRecording] valueForKey:@"ID"] indexOfObject:[ChosenMedicen valueForKey:@"Recording"]]]];
                            }
                            
                        }
                        else if ([label.title isEqualToString:@"Quantity"]) {
                            
                            
                            //////////////////////NSLog(@"ChosenMedicen %@", [ChosenMedicen valueForKey:@"Dose_unit"]);
                            
                            
                            if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                                
                                [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Quantity"]]];
                            }
                            else
                            {
                                
                                
                                [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Quantity"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                
                            }
                            
                        }
                        else if ([label.title isEqualToString:@"Message"]) {
                            
                            
                            
                            [label setText:[ChosenMedicen valueForKey:@"Message"]];
                            
                        }
                        else if ([label.title isEqualToString:@"Starting"]) {
                            
                            
                            NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                            [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                            [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
                            
                            [label setText:[dateConvert stringFromDate:[ChosenMedicen valueForKey:@"Starting"]]];
                            
                        }
                        else if ([label.title isEqualToString:@"Strength"]) {
                            
                            if ([[ChosenMedicen valueForKey:@"Strength"] isEqualToString:@" "]) {
                                
                            }
                            else
                            {
                                if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                                    
                                    [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Strength"]]];
                                }
                                else
                                {
                                    [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Strength"], [ChosenMedicen valueForKey:@"Strength_unit"]]];
                                    
                                }
                            }
                        }
                        else if ([label.title isEqualToString:@"Strength"]) {
                            
                            if ([[ChosenMedicen valueForKey:@"Strength"] isEqualToString:@" "]) {
                                
                            }
                            else
                            {
                                if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                                    
                                    [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Strength"]]];
                                }
                                else
                                {
                                    [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Strength"], [ChosenMedicen valueForKey:@"Strength_unit"]]];
                                    
                                }
                            }
                        }
                        else if ([label.title isEqualToString:@"Remaining"]) {
                            
                            if ([[ChosenMedicen valueForKey:@"Remaining"] isEqualToString:@" "]) {
                                
                            }
                            else
                            {
                                if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                                    
                                    [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Remaining"]]];
                                }
                                else
                                {
                                    [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Remaining"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                    
                                }
                            }
                        }
                        else if ([label.title isEqualToString:@"Filling"]) {
                            if ([[ChosenMedicen valueForKey:@"Filling"] isEqualToString:@" "]) {
                                
                            }
                            else
                            {
                                if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                                    
                                    
                                    [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Filling"]]];
                                }
                                else
                                {
                                    [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Filling"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                    
                                }
                            }
                        }
                        else if ([label.title isEqualToString:@"Refilling"]) {
                            
                            if ([[ChosenMedicen valueForKey:@"Refilling"] isEqualToString:@" "]) {
                                
                            }
                            else
                            {
                                if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
                                    
                                    [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Refilling"]]];
                                }
                                else
                                {
                                    [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Refilling"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                    
                                }
                            }
                        }
                        
                        else if ([label.title isEqualToString:@"Pharmacy"]) {
                            
                            
                            
                            
                            [label setText:[ChosenMedicen valueForKey:@"Pharmacy"]];
                            
                            
                            
                        }
                        else if ([label.title isEqualToString:@"Phone_Pharmacy"]) {
                            
                            
                            
                            
                            [label setText:[ChosenMedicen valueForKey:@"Phone_Pharmacy"]];
                            
                            
                            
                        }
                        
                        
                        else if ([label.title isEqualToString:@"eMail_Pharmacy"]) {
                            
                            
                            
                            
                            [label setText:[ChosenMedicen valueForKey:@"eMail_Pharmacy"]];
                            
                            
                            
                        }
                        else if ([label.title isEqualToString:@"Doctor"]) {
                            
                            
                            
                            
                            [label setText:[ChosenMedicen valueForKey:@"Doctor"]];
                            
                            
                            
                        }
                        else if ([label.title isEqualToString:@"eMail_Doctor"]) {
                            
                            
                            
                            
                            [label setText:[ChosenMedicen valueForKey:@"eMail_Doctor"]];
                            
                            
                            
                        }
                        
                        else if ([label.title isEqualToString:@"Phone_Doctor"]) {
                            
                            
                            
                            
                            [label setText:[ChosenMedicen valueForKey:@"Phone_Doctor"]];
                            
                            
                            
                        }
                        
                        else if ([label.title isEqualToString:@"Instructions"]) {
                            
                            
                            
                            
                            [label setText:[ChosenMedicen valueForKey:@"Instructions"]];
                            
                            
                            
                        }
                        
                        else if ([label.title isEqualToString:@"Duration"]) {
                            
                            
                            
                            
                            [label setText:[ChosenMedicen valueForKey:@"Duration"]];
                            
                            
                            
                        }
                        
                        else if ([label.title isEqualToString:@"Profile"]) {
                            
                            
                            
                            
                            [label setText:[ChosenMedicen valueForKey:@"Profile"]];
                            
                            
                            
                        }
                        
                        
                        
                        
                        ///Duration
                        
                        
                        
                        
                        else if ([label.title isEqualToString:@"Times"]) {
                            
                            
                            
                            [label setText:[[appDelegate convertList:[ChosenMedicen valueForKey:@"Times"]] stringByReplacingOccurrencesOfString:@"," withString:@"\n"]];
                            
                            NSArray *numbers2 = [[ChosenMedicen valueForKey:@"Times"] componentsSeparatedByString:@","];
                            
                            
                            if ([numbers2 count] ==0) {
                                
                                [self DynamicCell:1];
                            }
                            else
                            {
                                [self DynamicCell:[numbers2 count]];
                                
                            }
                            
                            [label setNumberOfLines:[numbers2 count]];
                            
                            
                            
                            
                            
                        }
                        else
                        {
                            if (label.text) {
                                
                            }
                            else
                            {
                                [label setText:[set valueForKey:label.title]];
                                
                            }
                        }
                        
                        
                        
                        
                        if ([label.title isEqualToString:@"Type"]) {
                            
                            appDelegate.TypeMed =[ChosenMedicen valueForKey:label.title];
                            
                            
                            
                            
                        }
                        
                        
                      
                        
                        
                        
                    }
                    
                
                }
                
            }
            
        }
        
    }
}




- (void) viewWillAppear:(BOOL)animated {
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.FromScreen=NO;
}

-(void)setNew

{
    
}

-(void)hideAndseek:(NSString*)set
{
    
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *) picker
{
    
    
    
    
    
    
    [picker dismissViewControllerAnimated:YES  completion:nil];
    
    
    
    
}


- (void) mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{
    
    
    
    
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}




- (IBAction)showMediaPicker
{
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];
    
    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    
    [self presentViewController:mediaPicker animated:YES completion:NULL];
}


-(void) changeColorFridge
{
    LeveyPopColorView *pinview01 = (LeveyPopColorView *)[self.view  viewWithTag:3459148];
    if (pinview01) {
        
        [pinview01 removeFromSuperview];
        
        
    }
    else
    {
        
        
        color = [[LeveyPopColorView alloc] initWithTitle:NSLocalizedString(@"Share Photo to...",nil) options:NULL setIt:2];
        // lplv.isModal = NO;
        [color setTag:3459148];
        [color showInView:self.view animated:YES];
        [color getParent:self get:3];
        
        
    }
    
    
    
    
    
    
}





-(void)getParant:(UIViewController*)parant

{
    parantIt = (StartViewController*)parant;
}


-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{
    
    if (index==1) {
        
        
        
    }
    
    else
    {
        
    }
}

-(void) CreateAccount

{
    
}


- (void)setSelectedColor:(UIColor*)colorit{
    
    
    
    
    
    [ColorField setBackgroundColor:colorit];
    
    
}

-(void)setitemsDictionary:(NSString*)set name:(NSString*)name

{
    
    
    if (set) {
        [ChosenMedicen setObject:set forKey:name];
    }
    
    
}

- (IBAction)Cancel:(UIButton *)sender {
    
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    
    MedicineViewController *oldscreen =(MedicineViewController *)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-1];
    
    
    if ([oldscreen isKindOfClass:[MedicineViewController class]]) {
        
        
    }
    
    
}


-(void)getParantGo:(UIViewController*)parant

{
    
    parantGo =parant;
    
}
- (IBAction)OK:(UIButton *)sender {
    
    
    
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) turnbuttons:(NSString*) setter

{
    
}

//#pragma mark - Google Analytics
//
//-(void)googleAnalyticsGroupping
//{
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:2]value:@"Medicine Info"];
//}


@end
