//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "NHMailActivity.h"
#import "Profiles.h"


#import "GAIDictionaryBuilder.h"



@class CodePopView, NewProfileController,Disclaimer;

@interface StartViewController : UIViewController<UITextViewDelegate, UIAlertViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate, UITableViewDelegate, NHCalendarActivityDelegate, NSFetchedResultsControllerDelegate>
@property (nonatomic, retain) NewProfileController *controller;
@property (nonatomic, retain) Disclaimer *discalaim;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) IBOutlet UITableView *table;
@property (nonatomic, retain) UILabel *NameLabel;
@property (nonatomic, retain) NSString *strApplicationUUID;
@property (nonatomic, retain) UIButton *button;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, assign, getter=isShouldBeginEditing) BOOL shouldBeginEditing;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerCopy;
@property (strong, nonatomic) IBOutlet UITableView *TableViewTime;

@property (strong, nonatomic) IBOutlet UINavigationBar *Navigation;
@property (nonatomic, strong) IBOutlet UILabel *Chosen;

@property (nonatomic, strong) Profiles *currenCourse;
@property (strong, nonatomic) IBOutlet UITableView *searbarTo;
@property (strong, nonatomic) UIView *Logoview;

- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void)popview;
-(void)showalert:(NSString*)sender;
- (void)Offerte;
@end
