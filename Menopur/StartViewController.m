//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "StartViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewProfileController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "HistoryViewController.h"
#import "NewMedecineController.h"
#import "SettingsViewcontroller.h"
#import <Social/Social.h>
#import "NHMailActivity.h"
#import "MedsViewController.h"
#import "HistViewController.h"
#import "Disclaimer.h"
#import "InfoViewController.h"
#import "MultipleViewController.h"
#import "Notification.h"
#import <QuartzCore/QuartzCore.h>
#import "actionButton.h"
//#import "SSKeychain.h"//sn

@interface StartViewController ()
{
    UIImageView *imageView_headlogo;
}
@end

@implementation StartViewController

@synthesize Vraagantwoord;
@synthesize table;
@synthesize controller;
@synthesize button;
@synthesize discalaim;
@synthesize NameLabel;
@synthesize Logoview;
@synthesize strApplicationUUID;



- (void)viewDidLoad
{
    
    //********** Google Analytics ****************
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"P_Profile List_iOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //********** Google Analytics ****************
    
    if ([SHARED_APPDELEGATE isNetworkReachable])
    {
        [SHARED_APPDELEGATE sendHitsInBackground];
    }

    [super viewDidLoad];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"setDailyImage"] == YES)
    {
       [SHARED_APPDELEGATE setNavigationbarIconImage:self.navigationController setimge:NO];
    }
    else
    {
      // [SHARED_APPDELEGATE setNavigationbarIconImage:self.navigationController setimge:YES];
    }
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"setDailyImage"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        appDelegate.strApplicationUUID  = [[NSUserDefaults standardUserDefaults] valueForKey:@"ApplicationUUID"];
        
        ////////////NSLog(@"NSUserDefaults %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"ApplicationUUID"]);
        
        
        if (appDelegate.strApplicationUUID == nil)
        {
            appDelegate.strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:appDelegate.strApplicationUUID forKey: @"ApplicationUUID"];
            
            ////////////NSLog(@"strApplicationUUID 01 %@", appDelegate.strApplicationUUID);
        }
        else
        {
            ////////////NSLog(@"strApplicationUUID %@", appDelegate.strApplicationUUID);
        }
    }
    else
    {
        appDelegate.strApplicationUUID  = [[NSUserDefaults standardUserDefaults] valueForKey:@"ApplicationUUID"];
        
        if (strApplicationUUID == nil)
        {
            
            appDelegate.strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];

            [[NSUserDefaults standardUserDefaults] setObject:appDelegate.strApplicationUUID forKey: @"ApplicationUUID"];
            
            ////////////NSLog(@"strApplicationUUID %@", appDelegate.strApplicationUUID);
            
        }
        else
        {
            ////////////NSLog(@"strApplicationUUID %@", appDelegate.strApplicationUUID);
            
        }
    }
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"]) {
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:[appDelegate newUUID] forKey: @"AuthenticateUserResult"];
    }
    
    self.navigationItem.hidesBackButton = YES;
    
    int64_t delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [appDelegate.Navigaioncopy removeAllObjects];
        [appDelegate.Navigaioncopy addObject:self];
        
        [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];
        
    });
    
    self.navigationItem.title =NSLocalizedString(@"Privacy Policy & Terms of Use", nil);

//    imageView_headlogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"headlogo"]];
//
//    CGSize imageSize = CGSizeMake(80, 22);
//    CGFloat marginX = (self.navigationController.navigationBar.frame.size.width / 2) - (imageSize.width / 2);
//    
//    imageView_headlogo.frame = CGRectMake(marginX, 12, imageSize.width, imageSize.height);
//
//    [self.navigationController.navigationBar addSubview:imageView_headlogo];

     imageView_headlogo.hidden = YES;

    //////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:14.0];
        label.numberOfLines = 2;
        label.textAlignment = NSTextAlignmentCenter;
        // ^-Use NSTextAlignmentCenter for older SDKs.
        label.textColor = [UIColor whiteColor]; // change this color
        self.navigationItem.titleView = label;
        [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
        label.text = self.navigationItem.title;
       // [label sizeToFit];
    }
    else
    {
        
    }
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    Vraagantwoord =[[NSMutableArray alloc]init];
    
    NameLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 20, 316, 60)];
    [NameLabel setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
//  [NameLabel setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //27-06-2017 changes
    [NameLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [NameLabel setText:NSLocalizedString(@"No profiles have been created.\nTo add a new profile, please press the + button.", nil)];
    [NameLabel setTextAlignment:NSTextAlignmentCenter];
    [NameLabel setTag:120];
    [NameLabel setBackgroundColor:[UIColor clearColor]];
    [NameLabel setNumberOfLines:3];
    [self.view addSubview:NameLabel];


    
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0, 320, self.view.bounds.size.height-88-64)];
        table.separatorColor = [UIColor clearColor];
        table.backgroundColor = [UIColor clearColor];
        table.rowHeight =120;
        table.separatorColor =[UIColor clearColor];
        table.layer.shadowOffset = CGSizeMake(3, 0);
        table.layer.shadowOpacity = 2;
        table.layer.shadowRadius = 2.0;
        [table setEditing:NO];
        [self.view addSubview:table];
        [table release];
        
    }
    else
    {
        table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0, 320, self.view.bounds.size.height-88-64)];
        table.separatorColor = [UIColor clearColor];
        table.backgroundColor = [UIColor clearColor];
        table.rowHeight =120;
        table.separatorColor =[UIColor clearColor];
        table.layer.shadowOffset = CGSizeMake(3, 0);
        table.layer.shadowOpacity = 2;
        table.layer.shadowRadius = 2.0;
        [table setEditing:NO animated:NO];
        [self.view addSubview:table];
        [table release];
    }
    
    //150 × 105 pixels
//    Logoview =[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-190, 330, 85)]; //26-07-2017
    Logoview =[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-180, 330, 85)];
    [Logoview setBackgroundColor:[UIColor clearColor]];
//    [Logoview setBackgroundColor:[UIColor colorWithRed:0.886 green:0.937 blue:0.957 alpha:1.000]]; //26-07-2017
//    [Logoview setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:Logoview];
    
    // 27-07-2017 Comment below 3 lines.
    //Logoview.layer.shadowOffset = CGSizeMake(1, 1);
    //Logoview.layer.shadowOpacity = 0.1;
    //Logoview.layer.shadowRadius = 3;
    
    //187 × 62 pixels
   UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:NSLocalizedString(@"Logo.png", nil)]];
    
    [background setFrame:CGRectMake(0, 0, 320, 75)];
    [Logoview addSubview:background];
    
    [Logoview setAlpha:0];
    
    //http://www.vrtx.com
    
    discalaim = [[Disclaimer alloc] initWithFrame:CGRectMake(0.0, 0, 320, self.view.bounds.size.height)];
    [discalaim setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    [self.view addSubview:discalaim];
    [discalaim release];
    [discalaim setUserInteractionEnabled:YES];
    
    [discalaim setParant:(StartViewController *)self];

    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"Discaimer"] isEqualToString:@"Argree"]) {



    NSString *version = [NSUserDefaults.standardUserDefaults objectForKey:@"current_version"];

      float version_int =  [version floatValue];;

        if (version != nil)
        {
            NSString *oldversion = [NSUserDefaults.standardUserDefaults objectForKey:@"old_version"];

            float  oldversion_int = [oldversion floatValue];

            if (version_int > oldversion_int)
            {

                NSLog(@"show term and use");


            }else{


                NSLog(@"hide term and use");

            [self removeDisclaimernext];

            }
        }

        [Logoview setAlpha:1];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:[appDelegate newUUID] forKey: @"AuthenticateUserResult"];
        
        [Notification CleareNotification];
        [UIApplication sharedApplication].applicationIconBadgeNumber=0;
        [Logoview setAlpha:0];
        
        [appDelegate getlocationsNow];
    }


}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
//    [self googleAnalyticsGroupping];
    
    
//    int counter = [[NSUserDefaults standardUserDefaults]integerForKey:@"NavigationBar"];
//    
//    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"NavigationBar"] > 1)
//    {
//        [SHARED_APPDELEGATE setNavigationbarIconImage:self.navigationController setimge:NO];
//    }
//    else
//    {
//    }
//    [[NSUserDefaults standardUserDefaults]setInteger:counter forKey:@"NavigationBar"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
    
//    [table reloadData];

}

-(void)removeDisclaimer
{

    

    self.navigationItem.title = @"";

    NSString *version = [NSUserDefaults.standardUserDefaults objectForKey:@"current_version"];
    [NSUserDefaults.standardUserDefaults setObject:version forKey:@"old_version"];

    NSLog(@"removeDisclaimer");

    [SHARED_APPDELEGATE setNavigationbarIconImage:self.navigationController setimge:NO];

    [discalaim setAlpha:0];
    [Logoview setAlpha:1];

    imageView_headlogo.hidden = YES;

    self.navigationItem.title =  NSLocalizedString(@"Profile(s)", nil);

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
   
    NSRange range = NSMakeRange (0, 1);
   
    //////////////////////////////////NSLog(@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        // this will appear as the title in the navigation bar
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:20.0];
        label.textAlignment = NSTextAlignmentCenter;
        // ^-Use NSTextAlignmentCenter for older SDKs.
        label.textColor = [UIColor whiteColor]; // change this color
        self.navigationItem.titleView = label;
          [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
        label.text = self.navigationItem.title;
        [label sizeToFit];
        
        UIBarButtonItem *edit = [[UIBarButtonItem alloc]
                                 initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                 target:self
                                 action:@selector(CreateAccount)];
        edit.style = UIBarButtonItemStyleBordered;
        
        [edit setTintColor:[UIColor whiteColor]];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.rightBarButtonItem = edit;
        
        [[NSUserDefaults standardUserDefaults] setObject: @"Argree" forKey: @"Discaimer"];
        
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self showalert:@"Start"];
        });
    }
    
    else
    {
        UIBarButtonItem *edit = [[UIBarButtonItem alloc]
                                 initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                 target:self
                                 action:@selector(CreateAccount)];
        edit.style = UIBarButtonItemStyleBordered;
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.rightBarButtonItem = edit;
        
        [[NSUserDefaults standardUserDefaults] setObject: @"Argree" forKey: @"Discaimer"];
        
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self showalert:@"Start"];
        });
        
    }
}

-(void)removeDisclaimernext
{

    NSString *version = [NSUserDefaults.standardUserDefaults objectForKey:@"current_version"];
    [NSUserDefaults.standardUserDefaults setObject:version forKey:@"old_version"];

    [discalaim setAlpha:0];
    
    imageView_headlogo.hidden = YES;
    self.navigationItem.title =  NSLocalizedString(@"Profile(s)", nil);
    
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        UIBarButtonItem *edit = [[UIBarButtonItem alloc]
                                 initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                 target:self
                                 action:@selector(CreateAccount)];
        edit.style = UIBarButtonItemStyleBordered;
        
        // this will appear as the title in the navigation bar
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:20.0];
        label.textAlignment = NSTextAlignmentCenter;
        // ^-Use NSTextAlignmentCenter for older SDKs.
        label.textColor = [UIColor whiteColor]; // change this color
        self.navigationItem.titleView = label;
          [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
        label.text = self.navigationItem.title;
        [label sizeToFit];
        
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
        
        [edit setTintColor:[UIColor whiteColor]];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.rightBarButtonItem = edit;
        
        [[NSUserDefaults standardUserDefaults] setObject: @"Argree" forKey: @"Discaimer"];
    }
    else
    {
        UIBarButtonItem *edit = [[UIBarButtonItem alloc]
                                 initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                 target:self
                                 action:@selector(CreateAccount)];
        edit.style = UIBarButtonItemStyleBordered;
        
        [edit setTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
      //[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.rightBarButtonItem = edit;
        
        [[NSUserDefaults standardUserDefaults] setObject: @"Argree" forKey: @"Discaimer"];
    }
}
-(void)Next:(UIButton*)sender
{
    if (sender.tag ==31) {
    }
    if (sender.tag ==32) {
        HistViewController *controller2 = [[HistViewController alloc]init];
        [self.navigationController pushViewController:controller2 animated:NO];
    }
    if (sender.tag == 33) {
        MedsViewController *controller3 = [[MedsViewController alloc]init];
        [self.navigationController pushViewController:controller3 animated:NO];
    }
    if (sender.tag == 34) {

        [self Offerte];

    }
    if (sender.tag ==35) {

       [self popview];
    }

    if (sender.tag ==36) {

        SettingsViewcontroller *controller4 = [[SettingsViewcontroller alloc]init];
        
        [self.navigationController pushViewController:controller4 animated:NO];
    }
}

-(void)showalert:(NSString*)sender
{
    if ([sender isEqualToString:@"New"]) {
      
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:NSLocalizedString(@"You have now added your profile and can start adding your medicine(s).",nil)];
        [alert setMessage:@""];
        [alert setDelegate:self];
        [alert setTag:464744];
        [alert addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
        [alert addButtonWithTitle:NSLocalizedString(@"Start",nil)];
        [alert show];
        [alert release];
    }
}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index
{
    //////////////////////////////////////////////////////////////////////////NSLog(@"%i", index);
    if (index==1) {
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate NextMed:NULL];
    }
    else
    {
        
    }
}

-(void) CreateAccount
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
              appDelegate.currentProfile= (Profiles *)[NSEntityDescription insertNewObjectForEntityForName:@"Profiles" inManagedObjectContext:[appDelegate managedObjectContext]];

    controller = [[NewProfileController alloc]init];
    
    controller.Editor = @"Add";
    
    [controller getParant:self];

    [self.navigationController pushViewController:controller animated:YES];
}

- (void) viewDidAppear:(BOOL)animated
{
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    myApp.FromScreen=NO;
    
    self.managedObjectContext = myApp.managedObjectContext;
    
    NSError *error =nil;
    
    if (![[self fetchedResultsController] performFetch:&error])
    {
        abort();
    }

    table.delegate = self;
    table.dataSource = self;

    [table reloadData];
 
    [super viewDidAppear:YES];
    
}

-(void)PresentView:(actionButton*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.currentProfile =sender.chosenCourse;
    
    controller = [[NewProfileController alloc]init];
    
    controller.Editor = @"edit";
    
    [controller getParant:self];

    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)Cancel:(UIButton *)sender {

}

- (IBAction)OK:(UIButton *)sender {
 
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}

-(void)setParent
{

}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}

- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self becomeFirstResponder];
	[self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)popview
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
    
    InfoViewController *viewController = [[InfoViewController alloc] init];
    
    UINavigationController *wrapper = [[[UINavigationController alloc]
                                        initWithRootViewController:viewController]
                                       autorelease];
    
    [self.navigationController presentViewController:wrapper
                                            animated:YES
                                          completion:nil];
}

- (void)Offerte
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];
}

#pragma mark - Fetched Result Controller section
    
-(NSFetchedResultsController*) fetchedResultsController
{
    if (_fetchedResultsController !=nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                                   ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _fetchedResultsController =[[NSFetchedResultsController alloc]initWithFetchRequest:fetchRequest managedObjectContext:[self managedObjectContext] sectionNameKeyPath:@"name" cacheName:nil];
    
    _fetchedResultsController.delegate =self;
    
    return _fetchedResultsController;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

#pragma mark - Table view delegate
-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [table beginUpdates];
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [table endUpdates];
}

-(void) controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [table insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [table deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            break;
        case NSFetchedResultsChangeMove:
            break;
    }
}


-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = table;
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate: {
            Profiles *changedCourse = [self.fetchedResultsController objectAtIndexPath:indexPath];
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.textLabel.text = changedCourse.id;
        }
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //add condition for hidden label 05-09-2017
    if ([[self.fetchedResultsController sections] count] == nil) {
        NameLabel.hidden = NO;
    }
    else
    {
        NameLabel.hidden = YES;
    }
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> secInfo =[[self.fetchedResultsController sections] objectAtIndex:section];
    
    int numberCount = [secInfo numberOfObjects];
    
    if (numberCount != 0)
    {
        NameLabel.hidden = YES;
    }
    else
    {
        NameLabel.hidden = NO;
    }
    
    return [secInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileCell *cell = (ProfileCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[ProfileCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    Profiles *cours =[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    //////////////////NSLog(@"%@", cours);

    [cell FillAllItems:cours];
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    [cell setTag:indexPath.row +200];
    
    [cell getparent:self];

    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *sectionHeader2;

    sectionHeader2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 15)];
    sectionHeader2.backgroundColor = [UIColor whiteColor];
    sectionHeader2.textColor = [UIColor whiteColor];
    sectionHeader2.font = [UIFont boldSystemFontOfSize:16];
    sectionHeader2.text = [[[self.fetchedResultsController sections] objectAtIndex:section]name];
    sectionHeader2.textAlignment=NSTextAlignmentCenter;
    
    [sectionHeader2 setHidden:NO];
    
    return sectionHeader2;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section]name];
}
    
    - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
        // Return YES if you want the specified item to be editable.
        return NO;
    }

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        
//        NSManagedObjectContext *contex =self.managedObjectContext;
//        Profiles *cours =[self.fetchedResultsController objectAtIndexPath:indexPath];
//        [contex deleteObject:cours];
//        
//        NSError *error = nil;
//        NSManagedObjectContext *context = self.managedObjectContext;
//        if (![context save:&error]) {
//            
//        }
//    }
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
}

#pragma mark - Google Analytics

//-(void)googleAnalyticsGroupping
//{
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:1]value:@"Add Profile"];
//}

@end


