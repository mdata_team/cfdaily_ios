//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CodePopView, NewMedecineController, TekstIDLabel, Inhoudview;
@interface StrengthViewController : UIViewController<UITextViewDelegate> {
    
    
    UITextView *TitleText;
    NSString *Compaire;
    UIView *Combi;
    
}
@property (nonatomic, retain) IBOutlet UIButton *button;
@property (nonatomic, retain) Inhoudview *inhoud;
@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) UITextView *underTitleText;
-(void)chosenMonth:(NSString*)sender;
-(void)chosenValue:(NSString*)sender;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
@end

