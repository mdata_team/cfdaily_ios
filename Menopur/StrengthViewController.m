//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "StrengthViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "NewProfileController.h"
#import "NewMedecineController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "Inhoudview.h"

@interface StrengthViewController ()

@end

@implementation StrengthViewController
@synthesize parantIt;


@synthesize parantLabel;
@synthesize inhoud;
@synthesize button;
@synthesize underTitleText;


-(void)loadView{
    
    
    [super loadView];
}




-(void)whatLabel:(UILabel*)set

{
    
    if ([set.text isEqualToString:@" "]) {
        
        
        [TitleText setText: [NSString stringWithFormat:@" %@", NSLocalizedString([[inhoud.MonthList objectAtIndex:0] valueForKey:@"Content"],nil)]];
        
        TitleText.selectedRange = NSMakeRange(0, 0);
        
        
    }
    else
        
    {
      
        
        
        
       NSArray *abc=[NSArray arrayWithObjects:@"24hr",@"%",@"a",@"a",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i",@"j",@"k",@"l",@"m",@"n",@"o",@"p",@"q",@"r",@"s",@"t",@"u",@"v",@"w",@"x",@"y",@"z",@"/",@"L",@"24hr",@"U",@"I",@" ",@"%",@"%" , @",",nil];
        
       for (NSInteger i=0 ; i<[abc count]; i++) {
            
            TitleText.text = [TitleText.text stringByReplacingOccurrencesOfString:[abc objectAtIndex:i] withString:@""];
            
            
        }
        
        NSArray *numbers2 = [set.text componentsSeparatedByString:@" "];
        
     
        
        if ([numbers2 count]==3) {
            
            
            
            [underTitleText setText:[numbers2 objectAtIndex:2]];
            
            NSInteger set = [[numbers2 objectAtIndex:1] length];
            
            [TitleText setText:[NSString stringWithFormat:@"%@ %@",TitleText.text, underTitleText.text]];
            
            
            TitleText.selectedRange = NSMakeRange(set, 0);
            
            
        }
        else
        {
            
        }
        
        
        if ([numbers2 count]==2) {
            
            
            
            [underTitleText setText:[numbers2 objectAtIndex:1]];
            
            NSInteger set = [[numbers2 objectAtIndex:0] length];
            
            [TitleText setText:[NSString stringWithFormat:@"%@ %@",TitleText.text, underTitleText.text]];
            
            
            TitleText.selectedRange = NSMakeRange(set, 0);
            
            
        }
        else
        {
            
        }
        
        
        
        
        
        
        
    }
    
    
    
    
    
    parantLabel =(TekstIDLabel*)set;
}



- (void)viewDidLoad {

    NSLog(@"%@", self);
    
    
    //Med Strenght
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    
    
    
    self.navigationItem.title =NSLocalizedString(@"Medicine Strength",nil);
    // this will appear as the title in the navigation bar
     UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    // ^-Use NSTextAlignmentCenter for older SDKs.
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
      [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
    label.text = self.navigationItem.title;
    [label sizeToFit];
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[deviceType substringWithRange:range] isEqualToString:@"7"]||[[deviceType substringWithRange:range] isEqualToString:@"8"]||[[deviceType substringWithRange:range] isEqualToString:@"9"]) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
    
    
    
    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-130, 45)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];
    
    
    
    
    underTitleText = [[UITextView alloc] initWithFrame:CGRectMake(100, 12, self.view.frame.size.width-40, 35)];
    [underTitleText setBackgroundColor:[UIColor clearColor]];
    [underTitleText setDelegate:self];
    [underTitleText setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]];
    
    [underTitleText setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    [self.view addSubview:underTitleText];
  
    TitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 12, self.view.frame.size.width-150, 35)];
    [TitleText setBackgroundColor:[UIColor clearColor]];
    [TitleText setDelegate:self];
    [TitleText setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]];
    [TitleText setTag:123+1];
    [TitleText setEditable:NO];
    
    [TitleText setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    [self.view addSubview:TitleText];
    
    
    inhoud = [[Inhoudview alloc] initWithFrame:CGRectMake(205, 10,106, 45)];
    [inhoud setBackgroundColor:[UIColor colorWithRed:0.941 green:0.941 blue:0.941 alpha:1.000]];
    [inhoud.layer setBorderWidth:1];
    [inhoud.layer setCornerRadius:10];
    [inhoud setParant:(StrengthViewControllerScroll*)self];
    [inhoud.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [inhoud.layer setMasksToBounds:YES];
    [self.view addSubview:inhoud];
    
    
    
    
    
    button = [[UIButton alloc] initWithFrame:CGRectMake(205, 10,106, 45)];
    
    
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
    [button setTitleColor:[UIColor colorWithRed:0.078 green:0.267 blue:0.341 alpha:1.000] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(viewanimate) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:[UIImage imageNamed:@"inhoud.png"] forState:UIControlStateNormal];
    [self.view addSubview:button];
    
    //inhoud.png
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        //NSLog(@"left");
        self.view.transform = CGAffineTransformMakeScale(-1, 1);
        
        for (UIView *view in self.view.subviews) {
            
            for (UILabel *label in view.subviews) {
                
                
                if ([label isKindOfClass:[UILabel class]]) {
                    
                    label.transform = CGAffineTransformMakeScale(-1, 1);
                    [label setTextAlignment:NSTextAlignmentNatural];
                    
                }
                
            }
            for (TekstIDLabel *label in view.subviews) {
                
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    label.transform = CGAffineTransformMakeScale(-1, 1);
                    [label setTextAlignment:NSTextAlignmentLeft];
                    
                }
                
            }
            for (UIButton *label in view.subviews) {
                
                
                if ([label isKindOfClass:[UIButton class]]) {
                    
                    label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                    [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                    
                }
                
            }
            
        }
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }
    

   
   
    
}

-(void)chosenValue:(NSString*)sender;


{
    
}

-(void)chosenMonth:(NSString*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.dose_unit = NSLocalizedString(sender,nil);
    
    
    underTitleText.text = [TitleText.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
   for (NSInteger i=0 ; i<[inhoud.MonthList count]; i++) {
        
        
        underTitleText.text = [TitleText.text stringByReplacingOccurrencesOfString:[[inhoud.MonthList valueForKey:@"Content"] objectAtIndex:i] withString:@""];
        
        
        
        
    }
    
    
      NSArray *abc=[NSArray arrayWithObjects:@"24hr",@"%",@"a",@"a",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i",@"j",@"k",@"l",@"m",@"n",@"o",@"p",@"q",@"r",@"s",@"t",@"u",@"v",@"w",@"x",@"y",@"z",@"/",@"L",@"24hr",@"U",@"I",@" ",@"%",@"%" , @",",nil];
    
   for (NSInteger i=0 ; i<[abc count]; i++) {
        
        
        
        TitleText.text = [TitleText.text stringByReplacingOccurrencesOfString:[abc objectAtIndex:i] withString:@""];
        
        
    }
    
    NSInteger set = [TitleText.text length];
    [underTitleText setText:[NSString stringWithFormat:@"%@", sender]];
    
    [TitleText setText:[NSString stringWithFormat:@"%@ %@",TitleText.text, sender]];
    
    [button setTitle:[NSString stringWithFormat:@"   %@", sender] forState:UIControlStateNormal];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    TitleText.selectedRange = NSMakeRange(set, 0);
    [self viewanimate];
    
}


-(NSString *)stringCleaner:(NSString *)yourString {
    
    NSScanner *theScanner;
    NSString *text = nil;
    
    theScanner = [NSScanner scannerWithString:yourString];
    
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString:@" " intoString:NULL] ;
        
        [theScanner scanUpToString:@" " intoString:&text] ;
        
 
        
        yourString = [yourString stringByReplacingOccurrencesOfString:
                      [NSString stringWithFormat:@"%@", text] withString:@" "];
        
        
        
        
        
    }
    
    return yourString;
    
    
}


-(void)viewDidAppear:(BOOL)animated
{

//////////////////////NSLog(@"%@", self);

    
    [TitleText setKeyboardType:UIKeyboardTypeNumberPad];
    
    [self stringCleaner:TitleText.text];
    
    [TitleText setEditable:NO];
    [TitleText becomeFirstResponder];
    
    if ([inhoud.MonthList count] >0) {
        
        [button setTitle:[NSString stringWithFormat:@"   %@", NSLocalizedString([[inhoud.MonthList objectAtIndex:0] valueForKey:@"Content"],nil)] forState:UIControlStateNormal];
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    
    
    
}


-(void)viewanimate

{
    
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];
    
    
    
    
    if (inhoud.frame.size.height == 45) {
        [inhoud setFrame:CGRectMake(205, 10,107,45*4)];
        [inhoud.table setFrame:CGRectMake(0.0, 45, 320, 45*3)];
        
    }
    else
    {
        [inhoud setFrame:CGRectMake(205, 10,107,45)];
        [inhoud.table setFrame:CGRectMake(0.0, 45, 320, 45)];
        
    }
    
    [UIView commitAnimations];
    
    
    
    
}



-(void) backButtonPressed
{
    
    if (TitleText.editable == NO) {
        
        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];
        
        
        [TitleText setEditable:NO];
        [TitleText becomeFirstResponder];
        
        
        
        
        
        //self.navigationItem.leftBarButtonItem = nil;
        
        
        
    }
    else
    {
        
        
        
        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];
        
        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [a1 setTag:44];
        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[deviceType substringWithRange:range] isEqualToString:@"7"]||[[deviceType substringWithRange:range] isEqualToString:@"8"]||[[deviceType substringWithRange:range] isEqualToString:@"9"]) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
        
    }
    
    
}




-(void) setTextview: (NSString *) text
{
    
    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [TitleText setText:text];
        
          NSArray *abc=[NSArray arrayWithObjects:@"24hr",@"%",@"a",@"a",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i",@"j",@"k",@"l",@"m",@"n",@"o",@"p",@"q",@"r",@"s",@"t",@"u",@"v",@"w",@"x",@"y",@"z",@"/",@"L",@"24hr",@"U",@"I",@" ",@"%",@"%" , @",",nil];
        
       for (NSInteger i=0 ; i<[abc count]; i++) {
            
            
            
            TitleText.text = [TitleText.text stringByReplacingOccurrencesOfString:[abc objectAtIndex:i] withString:@""];
            
            
        }
        
    });
    
    
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    
    
    
}

-(void) textViewDidBeginEditing:(UITextView *)textView
{
    
    
}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {
    
    
    
	return YES;
}

-(void)hideAndseek:(NSString*)set
{
    
    
}



-(void)getParant:(UIViewController*)parant

{
    
    parantIt =(NewMedecineController*)parant;
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    else
    {
    }
    return YES;
}

- (IBAction)doSomething:(UIButton *)sender {
    
    
    
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [parantLabel setText:TitleText.text];
        
        
        [parantIt setitemsDictionary:[NSString stringWithFormat:@"%@", TitleText.text] name:@"Strength"];
        
        
        
        
        
        
        
        
    });
    
}

- (IBAction)SelectChoice:(UIButton *)sender {
    
    
    
    
    
}



@end
