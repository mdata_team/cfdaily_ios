//
//  ViewController.h
//  Menospur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextViewData.h"

@class CodePopView, NewMedecineController, TekstIDLabel, Inhoudview,CustumPicker, TextViewData;
@interface StrengthViewControllerScroll : UIViewController<UITextViewDelegate> {


    UITextView *TitleText;
   NSString *Compaire;
    UIView *Combi;
    NSString *CompaireDecimal;
    NSString *Adjust;
    NSString *Number;
    
}
@property (nonatomic, strong)  TekstIDLabel *MedName;
@property (nonatomic, strong) CustumPicker *pickerTaal;
@property (nonatomic, retain) TextViewData *textblock;
@property (nonatomic, retain) IBOutlet UIButton *button;
@property (nonatomic, retain) Inhoudview *inhoud;
@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;
@property (nonatomic, retain) UITextView *underTitleText;
@property (nonatomic, assign, getter=isNumberPadShowing) BOOL numberPadShowing;
@property (nonatomic, strong) IBOutlet UIButton *dot;
-(void)chosenMonth:(NSString*)sender;
-(void)chosenValue:(NSString*)sender;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
-(void)whatValue:(NSString*)set;
@end

