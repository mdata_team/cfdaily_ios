//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "StrengthViewControllerScroll.h"
#import <QuartzCore/QuartzCore.h>

#import "NewProfileController.h"
#import "NewMedecineController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "Inhoudview.h"
#import "CustumPicker.h"
#import "CustumPickerRefill.h"
#import "PickerViewContent.h"
#import "TextViewData.h"
#import "LocationViewController.h"


@interface StrengthViewControllerScroll ()

@end

@implementation StrengthViewControllerScroll
@synthesize parantIt;

@synthesize TitleText;
@synthesize parantLabel;
@synthesize inhoud;
@synthesize button;
@synthesize underTitleText;
@synthesize pickerTaal;
@synthesize textblock;
@synthesize MedName;


-(void)loadView{
    
    
    [super loadView];
}


-(void)whatValue:(NSString*)set

{
    double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //
    
    NSLog(@"set #%@#", set);
    
       NSLog(@"set #%@#", appDelegate.strength_unit);
    
    if ([set isEqualToString:@" "]) {
       
        [textblock.Content setText:@" "];
        [textblock.Value setText:NSLocalizedString(@"mg",nil)];
        
          [textblock changeValue];
    }
    else
        
    {
    
    [textblock.Content setText:set];
    [textblock.Value setText:appDelegate.strength_unit];
        ////////////////////////////////////////////////////////////////////////////NSLog(@"%@", appDelegate.strength_unit);
        
        [textblock changeValue];
        
    }
    
    
   
    
});
    
  

}


-(void)whatLabel:(UILabel*)set

{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    [textblock.Content setText:[parantIt.ChosenMedicen valueForKey:@"Strength"]];
      [textblock.Value setText:appDelegate.strength_unit];
    
    [textblock changeValue];
    
    
   
}






- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];
    
  

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    Adjust=@"";
    
    CompaireDecimal =[[[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0] valueForKey:@"Format_Strength"];
    //Med Strenght
    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
    
    
    
    
    self.navigationItem.title =NSLocalizedString(@"Medicine Strength",nil);
    // this will appear as the title in the navigation bar
     UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    // ^-Use NSTextAlignmentCenter for older SDKs.
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
      [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
    label.text = self.navigationItem.title;
    [label sizeToFit];
    
    
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
    
    
    
    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 45)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
//    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    [self.view addSubview:Combi];
    
    

    
    
 
    underTitleText = [[UITextView alloc] initWithFrame:CGRectMake(100, 12, self.view.frame.size.width-40, 35)];
    [underTitleText setBackgroundColor:[UIColor clearColor]];
    [underTitleText setDelegate:self];
    [underTitleText setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]];
    [underTitleText setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    [underTitleText setEditable:NO];
    [self.view addSubview:underTitleText];
 
    
    
 
    
    
    inhoud = [[Inhoudview alloc] initWithFrame:CGRectMake(205, 10,106, 45)];
    [inhoud setBackgroundColor:[UIColor colorWithRed:0.941 green:0.941 blue:0.941 alpha:1.000]];
    [inhoud.layer setBorderWidth:1];
    [inhoud.layer setCornerRadius:10];
    [inhoud setParant:self];
    [inhoud.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
    [inhoud.layer setMasksToBounds:YES];
    [self.view addSubview:inhoud];
    [inhoud setAlpha:0];
    
    
    //TextViewData
    
    
    textblock = [[TextViewData alloc] initWithFrame:CGRectMake(20, 12,self.view.frame.size.width-50, 35)];
    [self.view addSubview:textblock];
    

    
    
    NewMedecineController *oldscreen =(NewMedecineController *)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
    
    ////////////////////////////////////////////////////////////////////NSLog(@"%@", oldscreen.ChosenMedicen);
    
    [self getParant:oldscreen];
    
    [self whatValue:[oldscreen.ChosenMedicen valueForKey:@"Strength"]];
    
    //////////////////////////////////////////////////////////////////////////////NSLog(@"not done");
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        //NSLog(@"left");
        self.view.transform = CGAffineTransformMakeScale(-1, 1);
        
        for (UIView *view in self.view.subviews) {
            
            for (UILabel *label in view.subviews) {
                
                
                if ([label isKindOfClass:[UILabel class]]) {
                    
                    label.transform = CGAffineTransformMakeScale(-1, 1);
                    [label setTextAlignment:NSTextAlignmentNatural];
                    
                }
                
            }
            for (TekstIDLabel *label in view.subviews) {
                
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    label.transform = CGAffineTransformMakeScale(-1, 1);
                    [label setTextAlignment:NSTextAlignmentLeft];
                    
                }
                
            }
            for (UIButton *label in view.subviews) {
                
                
                if ([label isKindOfClass:[UIButton class]]) {
                    
                    label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                    [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                    
                }
                
            }
            
        }
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }
    

   
    
    
}

-(void)goto

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];
    
   //////////////////////NSLog(@"typit %@", [typit valueForKey:@"Locations"]);
    
    LocationViewController *controller = [[LocationViewController alloc]init];
    [controller whatValue:[typit valueForKey:@"Locations"]];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)chosenValue:(NSString*)sender;


{ 
    
    [textblock.Content setText:sender];
    
    [textblock changeValue];
}

-(void)chosenMonth:(NSString*)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.strength_unit = sender;
    [textblock.Value setText:sender];
   
    [textblock changeValue];
    
  
    
}

-(void)viewDidAppear:(BOOL)animated
{
    

   NSLog(@"%@", self);
    
        NSLog(@"viewDidAppear");
    
//////////////////////     [self.view.layer setMasksToBounds:YES];

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    NSArray *numbers2 = [CompaireDecimal componentsSeparatedByString:@","];
    
    if ([numbers2 count]>1) {
        
        
        PickerViewContent *content = [[PickerViewContent alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-180, 320, 180)];
        // [content setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        
          [content Decimalstrength:CompaireDecimal pernt:(StrengthViewControllerScroll*)self];
        [self.view insertSubview:content aboveSubview:appDelegate.toolbarDown];
        
          if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
              
                 content.transform = CGAffineTransformMakeScale(-1, 1);
              
          }
        
        else
        {
            
        }
      
    }
    else
    {
        PickerViewContent *content = [[PickerViewContent alloc] initWithFrame:CGRectMake(0,  self.view.frame.size.height-180, 320, 180)];
        // [content setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        
        [content Decimalstrength:CompaireDecimal pernt:(StrengthViewControllerScroll*)self];
        [self.view insertSubview:content aboveSubview:appDelegate.toolbarDown];
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
               content.transform = CGAffineTransformMakeScale(-1, 1);
        }
        
        else
        {
            
        }
        
        
      
    }
    
    
    [appDelegate.toolbarDown setAlpha:0];
    
    
    
}




-(void)viewanimate

{
    
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.8];
    
    
    
    
    if (inhoud.frame.size.height == 45) {
        [inhoud setFrame:CGRectMake(205, 10,107,45*4)];
        [inhoud.table setFrame:CGRectMake(0.0, 45, 320, 45*3)];
        
    }
    else
    {
        [inhoud setFrame:CGRectMake(205, 10,107,45)];
        [inhoud.table setFrame:CGRectMake(0.0, 45, 320, 45)];
        
    }
    
    [UIView commitAnimations];
    
    
    
    
}



-(void) backButtonPressed
{
    
    if (TitleText.editable == NO) {
        
        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];
        
        
        [TitleText setEditable:NO];
        [TitleText becomeFirstResponder];
        
        
        //self.navigationItem.leftBarButtonItem = nil;
        
        
        
    }
    else
    {
        
        
        
        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];
        
//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
        
    }
    
    
}



-(void)hideAndseek:(NSString*)set
{
    
    
}



-(void)getParant:(UIViewController*)parant

{
    
    parantIt =(NewMedecineController*)parant;
    
}


- (void)textViewDidChange:(UITextView *)textView

{
   
    
}

-(BOOL)countandcompaire:(NSString*)set

{
    
    
    return YES;
}



- (IBAction)doSomething:(UIButton *)sender {
    
    
    
    NSLog(@"doSomething");
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    
    [[self navigationController] popViewControllerAnimated:YES];
    
    
    double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    [parantIt setitemsDictionary:textblock.Content.text name:@"Strength"];
    [parantIt setitemsDictionary:appDelegate.strength_unit name:@"Strength_unit"];
    
    if ([MedName.text isEqualToString:@" "]) {
        [parantIt setitemsDictionary:@" " name:@"Location"];
    }
    else
        
    {
          [parantIt setitemsDictionary:MedName.text name:@"Location"];
    }
   
    
    
});
    
   
}

- (IBAction)SelectChoice:(UIButton *)sender {
    
    
    
    
    
}



@end
