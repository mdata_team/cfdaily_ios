//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMedecineController.h"
#import "MedicineViewController.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"

#import "GAIDictionaryBuilder.h"

#define kReachabilityChangedNotification @"kNetworkReachabilityChangedNotification"

@class CodePopView, NewMedecineController, TekstIDLabel;
@interface ThermsViewController : UIViewController<UIWebViewDelegate>
{
    UITextView *TitleText;
   NSString *Compaire;
    UIView *Combi;
     UIView *CombiOther;
    NSString *chosen;
      NSString *chosenID;
    NSString *theTime;
    NSString *theDay;
}
@property (nonatomic, retain) IBOutlet UITextView *underTitleText;
@property (nonatomic, retain) IBOutlet NSString *timeframe;
@property (nonatomic, retain) IBOutlet  NSMutableArray *TimeChoses;
@property (nonatomic, retain) NSString *theTime;
@property (nonatomic, retain) NSString *theDay;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;
@property (nonatomic, retain) IBOutlet UIView *Postponeview;



-(void)change:(UIButton*)sender;
-(void) setTextview: (NSString *) text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
@end


