//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "ThermsViewController.h"
#import "TimesofDayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "ThermsViewController.h"
@interface ThermsViewController ()
{
    UIActivityIndicatorView *activityIndicator;
    UIWebView *webview;
}
@end

@implementation ThermsViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize Vraagantwoord;
@synthesize parantLabel;
@synthesize theTime;
@synthesize theDay;
@synthesize timeframe;
@synthesize TimeChoses;
@synthesize underTitleText;


-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
    

    parantLabel =(TekstIDLabel*)set;
    
}

- (void)viewDidLoad {

    //Terms & Privacy Policy
    
    //********** Google Analytics ****************
    
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:@"Terms & Privacy Policy"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //********** Google Analytics ****************
    
    if ([SHARED_APPDELEGATE isNetworkReachable])
    {
        [SHARED_APPDELEGATE sendHitsInBackground];
    }

    [super viewDidLoad];

    [self.view.layer setMasksToBounds:YES];


    //NSLog(@"%@", NSLocalizedString(@"Taal",nil) );
    
    [self setTitle:NSLocalizedString(@"Privacy Policy & Terms of Use",nil)];


    // this will appear as the title in the navigation bar
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, 50)];
    label.numberOfLines = 2;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:13];
//    label.textAlignment = NSTextAlignmentCenter;
    label.textAlignment = NSTextAlignmentLeft;
    
    // ^-Use NSTextAlignmentCenter for older SDKs.
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
      [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
    label.text = self.title;
    label.lineBreakMode = NSLineBreakByWordWrapping;
//    [label sizeToFit];
    
      TimeChoses =[[NSMutableArray alloc]init];

//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];

    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];

    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0.0f, 100, 20)];
    [title setTextColor:[UIColor whiteColor]];
    [title setText:@"Info"];
//    [title setFont:[UIFont boldSystemFontOfSize:18]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTag:120];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setNumberOfLines:3];

    //*************** Load Terms And Condition in Webview ****************** //30-06-2017 changes
    webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-56)];


    [webview setBackgroundColor:[UIColor clearColor]];

    
//    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
//    {

//        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"English_Termscondition" ofType:@"html"];
//        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
//        [webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];



 //   }
    //

     [self.view addSubview:webview];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000];

  webview.delegate = self;

    [self initial_setup];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {

    [MBProgressHUD hideAllHUDsForView:webview animated:YES];

}

- (void)webViewDidStartLoad:(UIWebView *)webView {


   [MBProgressHUD showHUDAddedTo:webview animated:YES];
    
}

-(void)termAndConditions_in_Web{
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {

        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_en.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];


    }

    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Francais"])
    {
            [self removefile];
            NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_fr.html";
            NSURL *url = [NSURL URLWithString:fullURL];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            NSError *err = nil;
            NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
            [self SaveInfo:html];
            [webview loadRequest:requestObj];


    }

    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Dutch"])
    {
        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_dt.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"German"])
    {

        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_ge.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Italiano"])
    {
        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_it.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }

    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Spanish"])
    {

        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_sp.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
    {

        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_dn.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
    {
        [self removefile];
        NSString *fullURL = @"http://mdataapps.com/cfdaily/terms_nw.html";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSError *err = nil;
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        [self SaveInfo:html];
        [webview loadRequest:requestObj];

    }
}

-(void)removefile
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"htmlTandCSettings.html"];
    [fileManager removeItemAtPath:filePath error:NULL];
}

- (void)SaveInfo :(NSString *)HTMLString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"htmlTandCSettings.html"];
    NSString *helloStr = HTMLString;
    NSError *error;
    [helloStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
}


#pragma mark - Void Methods

-(BOOL)isNetworkReachable
{
    BOOL internet_reachable = false;

    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];

    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"Please check your network connection");
        internet_reachable = false;
    }
    else
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];

        NetworkStatus status = [reachability currentReachabilityStatus];

        if(status == NotReachable)
        {
            //No internet
            internet_reachable = false;

            NSLog(@"**** Not Reachable ****");
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"htmlTandCSettings.html"];
            NSString* htmlString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];

            if (htmlString != NULL)
            {

                [webview loadHTMLString:htmlString baseURL:nil];

            }else
            {

                [GlobalClass ShowAlertwithTitle:@"Alert" Message:@"Please turn on internet connection" okLabelTitle:@"Ok"];
            }


        }
        else if (status == ReachableViaWiFi || status == ReachableViaWWAN)
        {
            //Connected to WiFi
            internet_reachable = true;

            [self termAndConditions_in_Web];

        }

    }

    return internet_reachable;
}


-(void)initial_setup
{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isNetworkReachable) name:kReachabilityChangedNotification object:nil];

    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        //   [btnBottomButton setEnabled:NO];

        NSLog(@"**** Not Reachable ****");
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"htmlTandCSettings.html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];

        if (htmlString != NULL)
        {
            //  [btnBottomButton setEnabled:YES];
            [webview loadHTMLString:htmlString baseURL:nil];


        }else
        {
            //  [btnBottomButton setEnabled:NO];
            [GlobalClass ShowAlertwithTitle:@"Alert" Message:@"Please turn on internet connection" okLabelTitle:@"Ok"];
        }

    }

    else if
        (networkStatus == ReachableViaWiFi || networkStatus == ReachableViaWWAN){

            [self termAndConditions_in_Web];
            
        }
    
}

-(void)Cancel:(UIBarButtonItem*) sender

{


        //[self.view.superview method]
    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];

}

-(void)Save:(UIBarButtonItem*) sender

{
    

    [parantLabel setText:TitleText.text];
    [parantIt setitemsDictionary:TitleText.text name:[self.navigationItem.title stringByReplacingOccurrencesOfString:@"" withString:@""]];


        //[parantIt getchoice:TimeChoses];
    
    [TitleText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];


}

-(void)change:(UIButton*)sender

{

    if (sender.tag ==140 ||sender.tag ==180) {


    }
    if (sender.tag ==141 ||sender.tag ==181) {

    }
    if (sender.tag ==142 ||sender.tag ==182) {


        TimesofDayViewController *controller = [[TimesofDayViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (sender.tag ==143 ||sender.tag ==183) {

    }
    if (sender.tag ==144 ||sender.tag ==184) {

    }
    

}

-(void) backAction
{
    
    
    [[self navigationController] popViewControllerAnimated:YES];
}


-(void)Action:(UIButton*)sender

{

  
}

-(void)changeChose:(NSString*)sender

{


    chosen = sender;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    [TitleText setKeyboardType:UIKeyboardTypeDecimalPad];
   
    TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
    [TitleText becomeFirstResponder];
}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
        [TitleText setText:text];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {

}

-(void)textViewDidChange:(UITextView *)textView
{
    

    if ([textView.text length]<4) {


        if ([textView.text length] ==0) {

             [underTitleText setText:[NSString stringWithFormat:@"   %@",NSLocalizedString(@"Quantity Per Filling",nil)]];

        }
        if ([textView.text length] ==1) {

             [underTitleText setText:[NSString stringWithFormat:@"    %@",NSLocalizedString(@"Quantity Per Filling",nil)]];
   
        }
        if ([textView.text length] ==2) {

             [underTitleText setText:[NSString stringWithFormat:@"      %@",NSLocalizedString(@"Quantity Per Filling",nil)]];
        
        }
        if ([textView.text length] ==3) {

             [underTitleText setText:[NSString stringWithFormat:@"         %@",NSLocalizedString(@"Quantity Per Filling",nil)]];
       
        }

    }
    else
        {
        [textView setText:@""];
              [underTitleText setText:[NSString stringWithFormat:@"   %@",NSLocalizedString(@"Quantity Per Filling",nil)]];

        }

}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {

	return YES;
}

-(void)hideAndseek:(NSString*)set
{

}

-(void)getParant:(UIViewController*)parant

{
    parantIt =(NewMedecineController*)parant;

}


- (IBAction)SelectChoice:(UIButton *)sender {

    

}



@end
