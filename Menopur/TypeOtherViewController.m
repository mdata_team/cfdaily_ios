//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "TypeOtherViewController.h"
#import "TypeViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "NewProfileController.h"
#import "NewMedecineController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
@interface TypeOtherViewController ()

@end

@implementation TypeOtherViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize parantLabel;


-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
  

    parantLabel =(TekstIDLabel*)set;
  
}



- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];

         [self.view.layer setMasksToBounds:YES];
    
   //     [self.view.layer setMasksToBounds:YES];
    

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];




//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
   


    NSArray *names1 =[NSArray arrayWithObjects:@"   Medicine Name",@"   Strength",@"   Medicine Type",@"   Assign to Profile",@"   Schedule",@"   Frequency",@"   Medicine Min. Quantity Remaining:",@"   Medicine Quantity Per Filling",@"   Alert Quantity for Refilling",@"   Doctor",@"   Pharmacy Prescription",@"   Notes", nil];


       for (NSInteger i=0 ; i<[names1 count]; i++) {

           

            if ([self.navigationItem.title isEqualToString:[names1 objectAtIndex:i]]) {

              
            }
            else
                {
                    
                NSArray *names1 =[NSArray arrayWithObjects:NSLocalizedString(@"Drop",nil), NSLocalizedString(@"Inhaler",nil), NSLocalizedString(@"Injection",nil), NSLocalizedString(@"Liquid",nil), NSLocalizedString(@"Ointment",nil),
                                      NSLocalizedString(@"Patch",nil), NSLocalizedString(@"Tablet",nil),NSLocalizedString(@"Spray",nil),nil];
                    
                    
                NewMedecineController *oldscreen =(NewMedecineController *)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-3];
                    
                    
                   //NSLog(@"%@", oldscreen.ChosenMedicen);

                    if ([names1 containsObject:[oldscreen.ChosenMedicen valueForKey:@"Type"]]) {
                        
                        Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 120)];
                        [Combi setBackgroundColor:[UIColor whiteColor]];
                        [Combi.layer setCornerRadius:10];
                        [Combi.layer setBorderWidth:1.5];
//                        [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
                        [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
                        [Combi.layer setMasksToBounds:YES];
                        [self.view addSubview:Combi];
                        
                        
                        
                        
                        TitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 14, self.view.frame.size.width-40, 100)];
                        [TitleText setBackgroundColor:[UIColor whiteColor]];
                        [TitleText setDelegate:self];
//                        [TitleText setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]];//28-06-2017 chanegs
                        [TitleText setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                        [TitleText setTag:123+1];
                        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
                        [TitleText setEditable:YES];
                        [TitleText becomeFirstResponder];
                        [TitleText setFont:[UIFont fontWithName:@"Helvetica" size:16]];
                        [self.view addSubview:TitleText];
                        
                        
                    }
                    else
                    {

                Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 120)];
                [Combi setBackgroundColor:[UIColor whiteColor]];
                [Combi.layer setCornerRadius:10];
                [Combi.layer setBorderWidth:1.5];
                [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
                [Combi.layer setMasksToBounds:YES];
                [self.view addSubview:Combi];



                
                TitleText = [[UITextView alloc] initWithFrame:CGRectMake(20, 14, self.view.frame.size.width-40, 100)];
                [TitleText setBackgroundColor:[UIColor whiteColor]];
                [TitleText setDelegate:self];
                [TitleText setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]];
                [TitleText setTag:123+1];
                TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
                    [TitleText setEditable:YES];
                    [TitleText becomeFirstResponder];
                [TitleText setFont:[UIFont fontWithName:@"Helvetica" size:16]];
                [self.view addSubview:TitleText];
                        
                        [TitleText setText:[oldscreen.ChosenMedicen valueForKey:@"Type"]];
                        
                    }
                
                }
        }



       //NSLog(@"%@", TitleText.text);


}

-(void) viewDidAppear:(BOOL)animated
{


}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

    



        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
          
        [TitleText setText:text];

    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  


}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{

}

-(void)getParant:(UIViewController*)parant

{

    parantIt =(TypeViewController*)parant;
    
}

- (IBAction)doSomething:(UIButton *)sender {



    
   //NSLog(@"%@", TitleText.text);
   
    [[self navigationController] popViewControllerAnimated:YES];

double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

    if ([TitleText.text length]==0) {
        
    }

    else
        {
    [parantLabel setText:TitleText.text];

    [parantIt setChoice:TitleText.text];

        }

  
    
});

}

- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
