//
//  ViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CodePopView, NewMedecineController, TekstIDLabel;
@interface TypeViewController : UIViewController<UITextViewDelegate> {


    UITextView *TitleText;
   NSString *Compaire;
    UIView *Combi;
     UIView *CombiOther;
    NSString *chosen;
}

@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;

-(void)setChoice:(NSString*)gothere;
-(void)change:(UIButton*)sender;
-(void) setTextview: (NSString *) text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
@end

