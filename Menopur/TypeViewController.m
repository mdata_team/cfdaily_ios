//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "TypeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TypeOtherViewController.h"
#import "ProfileCell.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "TekstIDLabel.h"
#import "ActionArrow.h"

@interface TypeViewController ()

@end

@implementation TypeViewController
@synthesize parantIt;
@synthesize TitleText;
@synthesize parantLabel;


-(void)loadView{


    [super loadView];
}

-(void)whatLabel:(UILabel*)set

{
   parantLabel =(TekstIDLabel*)set;
    
    
    
    NSArray *names1 =[NSArray arrayWithObjects:NSLocalizedString(@"Drop",nil), NSLocalizedString(@"Inhaler",nil), NSLocalizedString(@"Injection",nil), NSLocalizedString(@"Liquid",nil), NSLocalizedString(@"Ointment",nil),
                      NSLocalizedString(@"Patch",nil), NSLocalizedString(@"Tablet",nil),NSLocalizedString(@"Spray",nil),nil];
    
    
  
   for (NSInteger i=0 ; i<[names1 count]; i++) {
        
        if ([[names1 objectAtIndex:i] isEqualToString:set.text]) {
            
            
            TekstIDLabel *setimage3 = (TekstIDLabel *)[self.view viewWithTag:2222];
            [setimage3 setText:NSLocalizedString(@"Other Medicine Type",nil)];
            
            UIImageView *setimage = (UIImageView *)[self.view viewWithTag:i+160];
            [setimage setAlpha:1];
                  chosen =set.text;
            
            
            
            
      
        }
        else
        {
            UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
            [setimage setAlpha:0];
        }
       
       
       
       NewMedecineController *oldscreen =(NewMedecineController *)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
       
       
      //NSLog(@"%@", oldscreen.ChosenMedicen);
       
       
       if ([names1 containsObject:[oldscreen.ChosenMedicen valueForKey:@"Type"]]) {
       
       
       
       TekstIDLabel *setimage = (TekstIDLabel *)[self.view viewWithTag:2222];
       [setimage setText:NSLocalizedString(@"Other Medicine Type",nil)];
           
       }
       else
           
       {
           
       }
       
       

       
        
        
    }

    
    
    

    
 
}



- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];


   //     [self.view.layer setMasksToBounds:YES];
 
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];


//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
     NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
 
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
   

    NSArray *names1 =[NSArray arrayWithObjects:NSLocalizedString(@"Drop",nil), NSLocalizedString(@"Inhaler",nil), NSLocalizedString(@"Injection",nil), NSLocalizedString(@"Liquid",nil), NSLocalizedString(@"Ointment",nil),
                      NSLocalizedString(@"Patch",nil), NSLocalizedString(@"Tablet",nil),NSLocalizedString(@"Spray",nil),nil];
    
    
  

        Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 35*[names1 count])];
        [Combi setBackgroundColor:[UIColor whiteColor]];
        [Combi.layer setCornerRadius:10];
        [Combi.layer setBorderWidth:1.5];
//        [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [Combi.layer setMasksToBounds:YES];
        [self.view addSubview:Combi];


       for (NSInteger i=0 ; i<[names1 count]; i++) {

            
            

            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, (35*i), self.view.frame.size.width-20, 35)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 chanegs
           [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:15]];
            [MedName setText:[names1 objectAtIndex:i]];
            [MedName setTag:180+i];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Combi addSubview:MedName];



            ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, (35*i), self.view.frame.size.width-20, 35)];
            [NameButton setTitleit:[names1 objectAtIndex:i]];
            [NameButton setTag:140+i];
            [NameButton.layer setBorderWidth:1];
//            [NameButton.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
           [NameButton.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
            [NameButton addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
            [NameButton setBackgroundColor:[UIColor clearColor]];
            [Combi addSubview:NameButton];


            
            UIImageView *background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Vink.png"]];
            [background setFrame:CGRectMake(self.view.frame.size.width-50,  10+(35*i), 20, 20)];
            [background setBackgroundColor:[UIColor clearColor]];
            [Combi addSubview:background];
            [background setTag:160+i];
               [background setAlpha:0];
           
           
           if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
               
               
               Combi.transform = CGAffineTransformMakeScale(-1, 1);
               
                 MedName.transform = CGAffineTransformMakeScale(-1, 1);
                 [MedName setTextAlignment:NSTextAlignmentRight];
               
           }
           
           else
           {
               
           }
           
        }



    CombiOther = [[UIView alloc] initWithFrame:CGRectMake(10, 300, self.view.frame.size.width-20, 40)];
    [CombiOther setBackgroundColor:[UIColor whiteColor]];
    [CombiOther.layer setCornerRadius:10];
    [CombiOther.layer setBorderWidth:1.5];
//    [CombiOther.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [CombiOther.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [CombiOther.layer setMasksToBounds:YES];
    [self.view addSubview:CombiOther];

    
    
    TekstIDLabel *MedNameother = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, 3, self.view.frame.size.width-20, 40)];
//    [MedNameother setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
    [MedNameother setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [MedNameother setFont:[UIFont boldSystemFontOfSize:15]];
    [MedNameother setText:NSLocalizedString(@"Other Medicine Type",nil)];
    [MedNameother setTag:2222];
    [MedNameother setBackgroundColor:[UIColor clearColor]];
    [MedNameother setNumberOfLines:3];
    [CombiOther addSubview:MedNameother];
    
    
    
    UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(CombiOther.frame.size.width-35, 3, 60/2, 72/2)];
    [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
    //[NameButton setTitle:@"Other Medicine Type" forState:UIControlStateNormal];
    [NameButton setTag:140];
    [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [CombiOther addSubview:NameButton];
    

    
    NSLog(@"width %f", CombiOther.frame.size.width);
    
       NSLog(@"width %f", NameButton.frame.size.width);

    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
      
        CombiOther.transform = CGAffineTransformMakeScale(-1, 1);
        
        MedNameother.transform = CGAffineTransformMakeScale(-1, 1);
        [MedNameother setTextAlignment:NSTextAlignmentRight];
        
        
        //[NameButton setFrame:CGRectMake(CombiOther.frame.size.width-65, 3, 60/2, 72/2)];
         NameButton.transform = CGAffineTransformMakeScale(-1, 1);
       
        
    }
    
    else
    {
        
    }
    

    
    /*
     
      //NSLog(@"%@", oldscreen.ChosenMedicen);
     

       if ([names1 containsObject:[oldscreen.ChosenMedicen valueForKey:@"Type"]]) {
     

        TekstIDLabel *MedNameother = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, 35, self.view.frame.size.width-20, 40)];
        [MedNameother setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        [MedNameother setFont:[UIFont boldSystemFontOfSize:15]];
        [MedNameother setText:[names2 objectAtIndex:i]];
        [MedNameother setTag:2222];
        [MedNameother setBackgroundColor:[UIColor clearColor]];
        [MedNameother setNumberOfLines:3];
        [CombiOther addSubview:MedNameother];


     

        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3, 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [NameButton setTitle:[names2 objectAtIndex:i] forState:UIControlStateNormal];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [CombiOther addSubview:NameButton];
           
              NSLog(@"%@", [names2 objectAtIndex:i]);
           
          
           if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
               
               
               if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                   
                   
                   CombiOther.transform = CGAffineTransformMakeScale(-1, 1);
                   
                   MedNameother.transform = CGAffineTransformMakeScale(-1, 1);
                   [MedNameother setTextAlignment:NSTextAlignmentRight];
                   
               }
               
               else
               {
                   
               }
               
               
               
           }
           
           else
           {
               
           }

           
       }
       
       else
           
           
       {
           TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(15, (35*i), self.view.frame.size.width-20, 40)];
           [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
           [MedName setFont:[UIFont boldSystemFontOfSize:15]];
           [MedName setText:[oldscreen.ChosenMedicen valueForKey:@"Type"]];
           [MedName setTag:2222];
           [MedName setBackgroundColor:[UIColor clearColor]];
           [MedName setNumberOfLines:3];
           [CombiOther addSubview:MedName];
           
           
        
           
           
           
           UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(self.view.frame.size.width-65, 3, 60/2, 72/2)];
           [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
           [NameButton setTitle:[names2 objectAtIndex:i] forState:UIControlStateNormal];
           [NameButton setTag:140];
           [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
           [CombiOther addSubview:NameButton];
           
              NSLog(@"%@", [oldscreen.ChosenMedicen valueForKey:@"Type"]);
           
           if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
               
               
               if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                   
                   
                   CombiOther.transform = CGAffineTransformMakeScale(-1, 1);
                   
                   MedName.transform = CGAffineTransformMakeScale(-1, 1);
                   [MedName setTextAlignment:NSTextAlignmentRight];
                   
               }
               
               else
               {
                   
               }
               
               
               
           }
           
           else
           {
               
           }

       }
       
       
       
  
    
    }



     */


    NewMedecineController *oldscreen =(NewMedecineController *)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
    
    
   //NSLog(@"%@", oldscreen.ChosenMedicen);
    
    
    [self getParant:oldscreen];
    [self setChoice:[oldscreen.ChosenMedicen valueForKey:@"Strength"]];

}

-(void)change:(ActionArrow*)sender

{

    
    NSArray *names1 =[NSArray arrayWithObjects:NSLocalizedString(@"Drop",nil), NSLocalizedString(@"Inhaler",nil), NSLocalizedString(@"Injection",nil), NSLocalizedString(@"Liquid",nil), NSLocalizedString(@"Ointment",nil),
                      NSLocalizedString(@"Patch",nil), NSLocalizedString(@"Tablet",nil),NSLocalizedString(@"Spray",nil),nil];
    
    
    
    NSArray *names2 =[NSArray arrayWithObjects:@"%",@"mcg",@"mg/L",@"g/mL",@"%",@"mg/hr",@"mg",@"mg", nil];
    
    
     NSArray *names3 =[NSArray arrayWithObjects:@"piece",@"piece",@"mL",@"mL",@"piece",@"piece",@"piece",@"piece", nil];
    

      for (NSInteger i=0 ; i<[names1 count]; i++) {

           UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
           [setimage setAlpha:0];
           

       }

    
    TekstIDLabel *setimage3 = (TekstIDLabel *)[self.view viewWithTag:2222];
    [setimage3 setText:NSLocalizedString(@"Other Medicine Type",nil)];
    
    UIImageView *setimage = (UIImageView *)[self.view viewWithTag:sender.tag+20];
    [setimage setAlpha:1];

    
    
    chosen = sender.titleit;
    
    [SHARED_APPDELEGATE setStrMedtype:chosen];
    
    //NSLog(@"%@", chosen);
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([names1 containsObject:chosen]) {
   
    myApp.strength_unit =  NSLocalizedString([names2 objectAtIndex:[names1 indexOfObject:chosen]],nil);
     myApp.dose_unit =  NSLocalizedString([names3 objectAtIndex:[names1 indexOfObject:chosen]],nil);

    }
    
    //NSLog(@"%@", chosen);
       //NSLog(@"strength_unit %@", myApp.strength_unit);
      //NSLog(@"dose_unit %@", myApp.dose_unit);
}

-(void)Action:(UIButton*)sender

{
    
    NSLog(@"Action");

    TypeOtherViewController *controller = [[TypeOtherViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
    [controller getParant:self];


    TekstIDLabel *setimage = (TekstIDLabel *)[self.view viewWithTag:2222];
    [controller.navigationItem setTitle:[NSString stringWithFormat:@"%@", setimage.text]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    // ^-Use NSTextAlignmentCenter for older SDKs.
    label.textColor = [UIColor whiteColor]; // change this color
    controller.navigationItem.titleView = label;
    label.text = controller.navigationItem.title;
    [label sizeToFit];
    [controller whatLabel:setimage];
    
    
    
}

-(void)changeChose:(NSString*)sender

{
     chosen = sender;
    
    
    
    
}

-(void) viewDidAppear:(BOOL)animated
{


}


-(void) backButtonPressed
{

    if (TitleText.editable == NO) {

        Compaire = [NSString stringWithFormat:@"%@",  TitleText.text];

        TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
    [TitleText setEditable:YES];
        [TitleText becomeFirstResponder];

        //self.navigationItem.leftBarButtonItem = nil;



    }
    else
        {



        [TitleText setEditable:NO];
        [TitleText resignFirstResponder];

//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
       
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
     

        }


}

-(void) setTextview: (NSString *) text
{
  

    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [TitleText setText:text];

    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField {




}

-(void) textViewDidBeginEditing:(UITextView *)textView
{

  

}


- (BOOL)textFieldShouldReturn:(UITextView *)textField {



	return YES;
}

-(void)hideAndseek:(NSString*)set
{

}

-(void)getParant:(UIViewController*)parant

{

    parantIt =(NewMedecineController*)parant;




  
    
}


-(void)setChoice:(NSString*)gothere

{
  
    
    NSArray *names1 =[NSArray arrayWithObjects:NSLocalizedString(@"Drop",nil), NSLocalizedString(@"Inhaler",nil), NSLocalizedString(@"Injection",nil), NSLocalizedString(@"Liquid",nil), NSLocalizedString(@"Ointment",nil),
                      NSLocalizedString(@"Patch",nil), NSLocalizedString(@"Tablet",nil),NSLocalizedString(@"Spray",nil),nil];
    
    
    
    NSArray *names2 =[NSArray arrayWithObjects:@"%",   @"mcg",    @"mg/L",     @"g/mL",   @"%",     @"mg/hr",    @"mg",@"mg", nil];
 
    
    NSArray *names3 =[NSArray arrayWithObjects:@"piece",   @"piece",    @"mL",     @"mL",   @"piece",     @"piece",    @"piece",@"piece", nil];
    
    
    if ([gothere isEqualToString:@"Type"]) {
        
    }

    else
    {

    
        

   
   for (NSInteger i=0 ; i<[names1 count]; i++) {

        

        UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
        [setimage setAlpha:0];


        if ([gothere isEqualToString:[names1 objectAtIndex:i]]) {


            


        }
        else
            {
            
            chosen=gothere;
            
            }

        if (parantIt) {

        if ([ [parantIt.MedicineSpecs valueForKey:gothere] isEqualToString:[names1 objectAtIndex:i]]) {

        
            
            TekstIDLabel *setimage3 = (TekstIDLabel *)[self.view viewWithTag:2222];
            [setimage3 setText:NSLocalizedString(@"Other Medicine Type",nil)];

                UIImageView *setimage = (UIImageView *)[self.view viewWithTag:160+i];
                [setimage setAlpha:1];

                UILabel *setimage2 = (UILabel *)[self.view viewWithTag:180+i];

                chosen = setimage2.text;
            
        }

    }
       
        
    }
        
    }
    
    //NSLog(@"%@", chosen);
    //NSLog(@"%@", names2);
    

   
    if ([names1 containsObject:chosen]) {
        
        
        AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        myApp.strength_unit = NSLocalizedString([names2 objectAtIndex:[names1 indexOfObject:chosen]],nil);
        myApp.dose_unit = NSLocalizedString([names3 objectAtIndex:[names1 indexOfObject:chosen]],nil);
        
        
        TekstIDLabel *setimage = (TekstIDLabel *)[self.view viewWithTag:2222];
        [setimage setText:NSLocalizedString(@"Other Medicine Type",nil)];
        
    }
    else
        
    {
        
    }

    
 



}

- (IBAction)doSomething:(UIButton *)sender {


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


 
    [[self navigationController] popViewControllerAnimated:YES];

double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

    if (chosen) {
      
        if ([chosen isEqualToString:appDelegate.TypeMed])
        {
            

        }
        else
        {
   
            
            appDelegate.TypeMed =chosen;
            [parantLabel setText:chosen];
            [parantIt setitemsDictionary:chosen name:@"Type"];
            
        }

        if ([chosen isEqualToString: NSLocalizedString(@"Ointment",nil)]) {
            
            [parantIt setitemsDictionary:@"1" name:@"Quantity"];
        }
        else
        {
            
            
        }
        
        
     
        [parantIt setitemsDictionary:appDelegate.strength_unit name:@"Strength_unit"];
         [parantIt setitemsDictionary:appDelegate.dose_unit name:@"Dose_unit"];
        
    }
    else
    {

     
    }
    
    
    NSArray *names1 =[NSArray arrayWithObjects:NSLocalizedString(@"Drop",nil), NSLocalizedString(@"Inhaler",nil), NSLocalizedString(@"Injection",nil), NSLocalizedString(@"Liquid",nil), NSLocalizedString(@"Ointment",nil),
                      NSLocalizedString(@"Patch",nil), NSLocalizedString(@"Tablet",nil),NSLocalizedString(@"Spray",nil),nil];
    
    
    NewMedecineController *oldscreen =(NewMedecineController *)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-3];
    
    
   //NSLog(@"%@", oldscreen.ChosenMedicen);
    
    if ([names1 containsObject:[oldscreen.ChosenMedicen valueForKey:@"Type"]]) {
        
        
    }
    
    else
        
    {
        
    }

  
    
});

}

- (IBAction)SelectChoice:(UIButton *)sender {

    
    
    
    
}



@end
