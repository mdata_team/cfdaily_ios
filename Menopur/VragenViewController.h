//
//  InstructiefilmViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPFoldEnumerations.h"
#import "MPFlipEnumerations.h"

@protocol StyleDelegate;
@interface VragenViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *table;
@property (nonatomic, retain) IBOutlet UIButton *Film_logo;
@property (nonatomic, retain) IBOutlet UIButton *Logo_pdf;
@property (nonatomic, retain) IBOutlet UIButton *info;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbarDown;
@property (nonatomic, retain) IBOutlet UILabel *setbackground;
@property (nonatomic, retain) NSMutableArray *Vraagantwoord;
@property (assign, nonatomic) MPFoldStyle foldStyle;
@property (assign, nonatomic) MPFlipStyle flipStyle;
@property (assign, nonatomic, getter = isFold) BOOL fold;
@property (assign, nonatomic) NSUInteger style;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *flexItem;

-(void) turnbuttons:(NSString*) setter;


@property(weak, nonatomic) id<StyleDelegate> styleDelegate;
- (IBAction)donePressed:(id)sender;

@end

@protocol StyleDelegate <NSObject>

- (void)styleDidChange:(NSUInteger)newStyle;

@end
