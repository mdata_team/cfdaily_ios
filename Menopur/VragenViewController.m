//
//  InstructiefilmViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "VragenViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ProfileCell.h"
#import "MPFoldTransition.h"
#import "AppDelegate.h"
#import "MPFlipTransition.h"
#import "InstructiefilmViewController.h"
#import "VragenViewController.h"
#import "BijsluiterViewController.h"


@interface VragenViewController ()

@end

@implementation VragenViewController
@synthesize toolbarDown;
@synthesize setbackground;
@synthesize table;
@synthesize Film_logo;
@synthesize Logo_pdf;
@synthesize info;
@synthesize Vraagantwoord;
@synthesize flexItem;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];


//    /87 × 59 pixels
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 87/1.9, 59/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



    UIImage *image = [UIImage imageNamed:@"Logo_bar.png"];
	UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0, 0,136, 34);

    self.navigationItem.titleView = imageView;

    
    setbackground =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [setbackground setBackgroundColor:[UIColor colorWithRed:0.627 green:0.851 blue:0.961 alpha:1.000]];

    [self.view addSubview:setbackground];

    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = setbackground.layer.bounds;
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                         nil];
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.1f],
                            [NSNumber numberWithFloat:0.2f],
                            [NSNumber numberWithFloat:0.3f],
                            [NSNumber numberWithFloat:0.4f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.6f],
                            nil];



    [setbackground.layer addSublayer:shineLayer];




    Vraagantwoord =[[NSMutableArray alloc]initWithObjects:



                    @"Wat moet ik doen als ik per ongeluk de dikke naald vastpak?#Mocht het gebeuren dat je per ongeluk de naald vastpakt in plaats van het beschermdopje, pak dan een nieuwe naald en gooi de andere weg in de naalddispenser.#1",

                    @"Hoeveel flacons met Menopur kunnen er maximaal in 1 ampul oplosmiddel worden opgelost?#Er kunnen maximaal 3 flacons met Menopur in 1 ampul met oplosmiddel (1 ml) worden opgelost.#2",


                    @"Wat kan ik doen om het injecteren minder gevoelig te maken?#Om een branderig gevoel te verminderen kun je de huid laten drogen nadat je de injectieplaats hebt schoongemaakt met een alcoholdoekje. Het schoonmaken van de injectieplaats met een alcoholdoekje is overigens niet noodzakelijk. Ook kun je, voordat je de huid schoonmaakt, een ijsklontje op de plaats van injectie leggen.#3",

                    @"Stel ik ga een dagje weg, kan ik de oplossing van tevoren klaarmaken?#In principe moet de oplossing pas vlak van tevoren bereid worden maar mocht het echt nodig zijn kun je een reeds klaargemaakte oplossing maximaal 24 uur in de koelkast (bij 2 tot 8 °C) bewaren.#4",

                    @"Waar kan ik de gebruikte flacons, ampullen en naalden weggooien?#Je krijgt van je behandelende arts, verpleegkundige of de apotheek een naalddispenser. Hierin kun je je gebruikte naalden kwijt, evenals de gebruikte flacons en ampullen. Wat kan ik met een volle naalddispenser doen? De volle naalddispenser kun je inleveren bij de apotheek waar je weer een nieuwe, schone naalddispenser krijgt.#5",

                    @"Wat moet ik doen als ik klachten krijg nadat ik de injectie met hormonen heb toegediend?#De mogelijke bijwerkingen van Menopur worden vermeld in de bijsluiter. Zeker als je deze bijwerkingen als ernstig ervaart, is het goed om je behandelende arts of je apotheker te raadplegen. Bijwerkingen die niet in de bijsluiter worden gemeld, dien je altijd direct aan je behandelende arts te melden.#6",


                    @"Wat kan ik doen om de behandeling zo goed mogelijk te laten verlopen?#Als je zwanger wilt worden, is het van belang dat je lichaam in goede conditie is en je goed geïnformeerd de behandeling start.#7",

                    @"Mag ik tijdens een vruchtbaarheids-\nbehandeling medicijnen innemen?#Het is goed om met je behandelende arts teoverleggen welke medicijnen je gebruikt of gaat gebruiken. Je arts zal aangeven of je deze medicijnen kan (blijven) gebruiken.#8", nil];

    



    [self.view addSubview:toolbarDown];



    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod"] || [deviceType isEqualToString:@"iPod touch"] || [deviceType isEqualToString:@"iPhone Simulator"]) {

      


    table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0, 320, self.view.bounds.size.height-88)];
    table.separatorColor = [UIColor clearColor];
    table.backgroundColor = [UIColor clearColor];
    table.rowHeight =260;
    table.delegate = self;
    table.dataSource = self;
    [table setEditing:NO];
    [self.view addSubview:table];

        }

        else

            {

            table = [[UITableView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-320)/2, 0, 320, self.view.bounds.size.height-88)];
            table.separatorColor = [UIColor clearColor];
            table.backgroundColor = [UIColor clearColor];
            table.rowHeight =260;
            table.delegate = self;
            table.dataSource = self;
            [table setEditing:NO];
            [self.view addSubview:table];
            }



    toolbarDown = [[UIToolbar alloc] init];
    toolbarDown.frame = CGRectMake(0, self.view.frame.size.height-88, self.view.frame.size.width, 44);
    toolbarDown.tintColor = [UIColor colorWithRed:0.204 green:0.714 blue:0.816 alpha:1.000];




    flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                             target:nil
                                                             action:nil];


    info = [UIButton buttonWithType:UIButtonTypeCustom];
    [info setTag:44];
    [info setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [info addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    info.layer.cornerRadius = 2;
    [info setTag:22];
    [info setImage:[UIImage imageNamed:@"Info_Logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *info_button = [[UIBarButtonItem alloc] initWithCustomView:info];


    Logo_pdf = [UIButton buttonWithType:UIButtonTypeCustom];
    [Logo_pdf setTag:44];
    [Logo_pdf setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [Logo_pdf addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    Logo_pdf.layer.cornerRadius = 2;
    [Logo_pdf setTag:23];
    [Logo_pdf setImage:[UIImage imageNamed:@"Logo_pdf.png"] forState:UIControlStateNormal];
    UIBarButtonItem *logo_button = [[UIBarButtonItem alloc] initWithCustomView:Logo_pdf];



        //Film_Logo.png


    Film_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    [Film_logo setTag:44];
    [Film_logo setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [Film_logo addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    Film_logo.layer.cornerRadius = 2;
    [Film_logo setTag:24];
    [Film_logo setImage:[UIImage imageNamed:@"Film_Logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *film_button = [[UIBarButtonItem alloc] initWithCustomView:Film_logo];


    NSArray *items2 = [NSArray arrayWithObjects:
                       film_button, flexItem,logo_button,flexItem,info_button,
                       nil];
    toolbarDown.items = items2;


    [self.view addSubview:toolbarDown];

}


-(void)PresentView:(UIButton*)sender

{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    for (int i =1; i < [[[self navigationController] viewControllers] count]; i++) {

        UIViewController *controller = [[[self navigationController] viewControllers] objectAtIndex:i];
        [[self navigationController] popToViewController:controller animated:NO];


    }




    int64_t delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

        if (sender.tag ==22) {



            VragenViewController *controller = [[VragenViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];

            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];






        }
        if (sender.tag ==23) {


            BijsluiterViewController *controller = [[BijsluiterViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];

            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];

        }
        if (sender.tag ==24) {



            InstructiefilmViewController *controller = [[InstructiefilmViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];
            
            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];
            
            
        }
        
        
    });
    
    
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [Vraagantwoord count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(ProfileCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {



    NSArray *numbers2 = [[Vraagantwoord objectAtIndex:indexPath.row] componentsSeparatedByString:@"#"];

        UIColor *colorit=[UIColor colorWithRed:0.235 green:0.675 blue:0.710 alpha:1.000];
    
        ////NSLog(@"%@", numbers2);
    [cell FillAllItems:[numbers2 objectAtIndex:0] facit:[numbers2 objectAtIndex:1] setcolor:colorit id:@"2"];
    [cell setTag:indexPath.row +200];


}



- (IBAction)donePressed:(id)sender {
	if ([self isFold])
        {
            // use the opposite fold style from the selected style
		MPFoldStyle popStyle = MPFoldStyleFlipFoldBit([self style]);

		[self.navigationController popViewControllerWithFoldStyle:popStyle];
        }
	else
        {
            // use the opposite flip style from the selected style
		MPFlipStyle popStyle = MPFlipStyleFlipDirectionBit([self style]);

		[self.navigationController popViewControllerWithFlipStyle:popStyle];
        }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    
   if (indexPath.row ==appDelegate.Selected)
       return 260;

       else

           return 260/2;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{


     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    appDelegate.Selected = indexPath.row;






    [tableView reloadData];



    
}


- (ProfileCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {




    NSArray *numbers2 = [[Vraagantwoord objectAtIndex:indexPath.row] componentsSeparatedByString:@"#"];

    //NSLog(@"%@", numbers2);

    NSString *identifier = @"CellIdentifier";
    ProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

	if (cell == nil) {

        cell = [[ProfileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    }

    else

        {

        }


    return cell;


}

-(void) turnbuttons:(NSString*) setter

{
 //NSLog(@"turnbuttons");
}

-(void) backAction
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
