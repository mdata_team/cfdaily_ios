//
//  SecondViewController.h
//  Voetbalapp
//
//  Created by Jeffrey Snijder on 21-11-12.
//  Copyright (c) 2012 Voetbalapp. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface WebViewController : UIViewController <UIWebViewDelegate>

{
}


 @property (nonatomic, retain) IBOutlet UIToolbar *toolbarUp;
 @property (nonatomic, retain) UIWebView *webView;
-(void)setDiscription:(NSString*)set;

@end
