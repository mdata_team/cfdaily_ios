//
//  SecondViewController.m
//  Voetbalapp
//
//  Created by Jeffrey Snijder on 21-11-12.
//  Copyright (c) 2012 Voetbalapp. All rights reserved.
//

#import "WebViewController.h"
#import "AppDelegate.h"


@interface WebViewController ()

@end

@implementation WebViewController
@synthesize toolbarUp;
@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.image = [UIImage imageNamed:@"0033.png"];
        
        // [DownloadImages Download:@"http://upload.wikimedia.org/wikipedia/commons/b/b3/Happy_face_ball.png"];
        
        
        UIBarButtonItem *done = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem:UIBarButtonItemStylePlain
                                   target:self
                                 action:@selector(Done:)];


        
        
        toolbarUp = [[UIToolbar alloc] init];
        toolbarUp.frame = CGRectMake(0, -2, self.view.frame.size.width, 54);
        toolbarUp.tintColor = [UIColor colorWithRed:0.204 green:0.714 blue:0.816 alpha:1.000];
        [self.view addSubview:toolbarUp];
        

        NSArray *itemsUp = [NSArray arrayWithObjects:done,nil];
        toolbarUp.items = itemsUp;
        
        
    //http://www.vvschagen.nl/modules/nieuws/rss.php?number=10
        
       webView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0, 54, self.view.bounds.size.width, self.view.bounds.size.height-54)];
        webView.backgroundColor = [UIColor whiteColor];
        webView.scalesPageToFit = YES;
        webView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        webView.delegate = self;
        [self.view addSubview:webView];
    
      
        


        
   
        
    }
    return self;
}



-(void)Done:(UIButton*)sender
{
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)setDiscription:(NSString*)set

{

    NSString *urlAddress = [NSString stringWithFormat:@"http://%@", set];
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];




}




-(void) webViewDidStartLoad:(UIWebView *)webView
{

}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

