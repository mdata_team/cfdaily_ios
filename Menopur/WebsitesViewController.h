//
//  InstructiefilmViewController.h
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebsitesViewController : UIViewController
@property (nonatomic, retain) IBOutlet UIButton *Film_logo;
@property (nonatomic, retain) IBOutlet UIButton *Logo_pdf;
@property (nonatomic, retain) IBOutlet UIButton *info;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbarDown;
@property (nonatomic, retain) IBOutlet UILabel *setbackground;
-(void) turnbuttons:(NSString*) setter;



@end
