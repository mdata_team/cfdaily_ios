//
//  InstructiefilmViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "WebsitesViewController.h"
#import "WebViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "InstructiefilmViewController.h"
#import "BijsluiterViewController.h"
#import "VragenViewController.h"
#import "AppDelegate.h"


@interface WebsitesViewController ()

@end

@implementation WebsitesViewController
@synthesize toolbarDown;
@synthesize setbackground;
@synthesize Film_logo;
@synthesize Logo_pdf;
@synthesize info;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];


//    /87 × 59 pixels
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setTag:44];
    [a1 setFrame:CGRectMake(2, 2, 87/1.9, 59/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;



    UIImage *image = [UIImage imageNamed:@"Logo_bar.png"];
	UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0, 0,136, 34);

    self.navigationItem.titleView = imageView;

    
    setbackground =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [setbackground setBackgroundColor:[UIColor colorWithRed:0.627 green:0.851 blue:0.961 alpha:1.000]];

    [self.view addSubview:setbackground];

    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = setbackground.layer.bounds;
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.9].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.6f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4].CGColor,
                         nil];
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.1f],
                            [NSNumber numberWithFloat:0.2f],
                            [NSNumber numberWithFloat:0.3f],
                            [NSNumber numberWithFloat:0.4f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.6f],
                            nil];



    [setbackground.layer addSublayer:shineLayer];

    toolbarDown = [[UIToolbar alloc] init];
    toolbarDown.frame = CGRectMake(0, self.view.frame.size.height-88, self.view.frame.size.width, 44);
    toolbarDown.tintColor = [UIColor colorWithRed:0.204 green:0.714 blue:0.816 alpha:1.000];




    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];




    info = [UIButton buttonWithType:UIButtonTypeCustom];
    [info setTag:44];
    [info setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [info addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    info.layer.cornerRadius = 2;
    [info setTag:22];
    [info setImage:[UIImage imageNamed:@"Info_Logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *info_button = [[UIBarButtonItem alloc] initWithCustomView:info];


    Logo_pdf = [UIButton buttonWithType:UIButtonTypeCustom];
    [Logo_pdf setTag:44];
    [Logo_pdf setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [Logo_pdf addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    Logo_pdf.layer.cornerRadius = 2;
    [Logo_pdf setTag:23];
    [Logo_pdf setImage:[UIImage imageNamed:@"Logo_pdf.png"] forState:UIControlStateNormal];
    UIBarButtonItem *logo_button = [[UIBarButtonItem alloc] initWithCustomView:Logo_pdf];



        //Film_Logo.png


    Film_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    [Film_logo setTag:44];
    [Film_logo setFrame:CGRectMake(-2, 2, 60/2, 53/2)];
    [Film_logo addTarget:self action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    Film_logo.layer.cornerRadius = 2;
    [Film_logo setTag:24];
    [Film_logo setImage:[UIImage imageNamed:@"Film_Logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *film_button = [[UIBarButtonItem alloc] initWithCustomView:Film_logo];


    NSArray *items2 = [NSArray arrayWithObjects:
                       film_button, flexItem,logo_button,flexItem,info_button,
                       nil];
    toolbarDown.items = items2;


    [self.view addSubview:toolbarDown];



    UILabel *MerkenKopLabel= [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-290)/2, 10, 290, 25)];
    MerkenKopLabel.backgroundColor = [UIColor clearColor];
    [MerkenKopLabel setText: @" Handige websites"];
    MerkenKopLabel.font = [UIFont boldSystemFontOfSize:23];
    [MerkenKopLabel setNumberOfLines:30];
    [MerkenKopLabel setTextAlignment:NSTextAlignmentLeft];
    MerkenKopLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
    [MerkenKopLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:MerkenKopLabel];


    NSArray *Text = [NSArray arrayWithObjects: @"www.hulpbijzwangerworden.nl#Website met specifieke informatie over hulp bij zwanger worden.", @"www.freya.nl#Website van de vereniging voor mensen met vruchtbaarheidsproblemen", @"www.nvog.nl#Website van de Nederlandse Vereniging voor Obstetrie en Gynaecologie.", @"www.ferring.nl#Website met informatie over de producent van Menopur", nil];


      for (int i=0 ; i<[Text count]; i++) {


          NSArray *numbers2 = [[Text objectAtIndex:i] componentsSeparatedByString:@"#"];


            UILabel *Line1= [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-290)/2, 45+(80*i), 290, 2)];
          [Line1 setBackgroundColor:[UIColor colorWithRed:0.435 green:0.796 blue:0.867 alpha:1.000]];
          [self.view addSubview:Line1];



          UIButton *URLbutton= [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-290)/2, 45+(80*i), 290, 40)];
          URLbutton.backgroundColor = [UIColor clearColor];
          [URLbutton setTitle:[numbers2 objectAtIndex:0] forState:UIControlStateNormal];
          URLbutton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
          [URLbutton.titleLabel setNumberOfLines:2];
         [URLbutton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
          [URLbutton setTitleColor:[UIColor colorWithRed:0.675 green:0.075 blue:0.153 alpha:1.000] forState:UIControlStateNormal];
          
          [URLbutton addTarget:self action:@selector(Website:) forControlEvents:UIControlEventTouchUpInside];
          [URLbutton setBackgroundColor:[UIColor clearColor]];
          [self.view addSubview:URLbutton];
          

          UILabel *MerkenKopLabel= [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-290)/2, 75+(80*i), 290, 40)];
          MerkenKopLabel.backgroundColor = [UIColor clearColor];
          [MerkenKopLabel setText:[numbers2 objectAtIndex:1]];
          MerkenKopLabel.font = [UIFont systemFontOfSize:14];
          [MerkenKopLabel setNumberOfLines:2];
          [MerkenKopLabel setTextAlignment:NSTextAlignmentLeft];
          MerkenKopLabel.textColor = [UIColor colorWithRed:0.000 green:0.271 blue:0.400 alpha:1.000];
          [MerkenKopLabel setBackgroundColor:[UIColor clearColor]];
          [self.view addSubview:MerkenKopLabel];


      }

    
}


-(void)Website:(UIButton*)sender

{


    WebViewController *controller = [[WebViewController alloc]  init];
    [self presentViewController:controller animated:YES completion:Nil];
    [controller setDiscription:sender.titleLabel.text];


    
}
-(void)viewDidAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton = YES;
}
-(void)PresentView:(UIButton*)sender

{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    for (int i =1; i < [[[self navigationController] viewControllers] count]; i++) {

        UIViewController *controller = [[[self navigationController] viewControllers] objectAtIndex:i];
        [[self navigationController] popToViewController:controller animated:NO];


    }



    NSLog(@"%@", [self navigationController]);

    /*


    int64_t delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

        if (sender.tag ==22) {



            BijsluiterViewController *controller = [[BijsluiterViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];

            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];




        }
        if (sender.tag ==23) {

            VragenViewController *controller = [[VragenViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];

            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];

        }
        if (sender.tag ==24) {



            InstructiefilmViewController *controller = [[InstructiefilmViewController alloc]init];
            NSArray *viewControllers = [[self navigationController] viewControllers];

            [appDelegate.Navigaioncopy removeAllObjects];
            [appDelegate.Navigaioncopy addObject:[viewControllers objectAtIndex:0]];
            [appDelegate.Navigaioncopy addObject:controller];
            
            [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];
            
            
        }
        
        
    });
     
     */
    
    
}


-(void) turnbuttons:(NSString*) setter

{
 NSLog(@"turnbuttons");
}

-(void) backAction
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
