//
//  main.m
//  LifeScout
//
//  Created by Jeffrey Snijder on 27-11-13.
//  Copyright (c) 2013 Livecast. All rights reserved.



#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        NSString *country= [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode];
        
        NSString *countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
        
        NSLog(@"%@", country);
        NSLog(@"%@", countryCode);
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"Savesetting"] isEqualToString:[NSString stringWithFormat:@"%@-%@",country, countryCode]]) {
            
            NSLog(@"nop");
        }
        else
        {
              //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"preferred_language"];
        }
        
        
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"preferred_language"])
        {
            
            if ([[NSLocale availableLocaleIdentifiers] containsObject:[NSString stringWithFormat:@"%@-%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"preferred_language"], countryCode]])
                
            {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@-%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"preferred_language"], countryCode], @"en",  nil] forKey:@"AppleLanguages"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else
                
            {
                for (int k =0; k <[[NSLocale availableLocaleIdentifiers] count]; k++) {
                                       
                    if ([[[NSLocale availableLocaleIdentifiers] objectAtIndex:k] rangeOfString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"preferred_language"]]].location==NSNotFound)
                    {
                        
                        
                    }
                    else
                    {
                        NSLog(@"%@", [[NSLocale availableLocaleIdentifiers] objectAtIndex:k]);
                        
                        //11-07-2017 add condition for not getting "de" first got "kde_TZ" its not register in our Localization string file "de" is register in localization file for "German"

                        //13-07-2017 seprate in all language if conditions
                        //ENGLISH
                        if ([[[NSLocale availableLocaleIdentifiers] objectAtIndex:k] isEqualToString:@"en"])
                        {
                            
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[[NSLocale availableLocaleIdentifiers] objectAtIndex:k], @"en",  nil] forKey:@"AppleLanguages"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        break;
                        }
                        //Danish
                       else if ([[[NSLocale availableLocaleIdentifiers] objectAtIndex:k] isEqualToString:@"da"])
                        {
                            
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[[NSLocale availableLocaleIdentifiers] objectAtIndex:k], @"en",  nil] forKey:@"AppleLanguages"];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            break;
                        }
                        //Norwegian
                       else if ([[[NSLocale availableLocaleIdentifiers] objectAtIndex:k] isEqualToString:@"nb"])
                       {
                           
                           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                           [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[[NSLocale availableLocaleIdentifiers] objectAtIndex:k], @"en",  nil] forKey:@"AppleLanguages"];
                           [[NSUserDefaults standardUserDefaults]synchronize];
                           break;
                       }
                        //German
                       else if ([[[NSLocale availableLocaleIdentifiers] objectAtIndex:k] isEqualToString:@"de"])
                        {
                            
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[[NSLocale availableLocaleIdentifiers] objectAtIndex:k], @"en",  nil] forKey:@"AppleLanguages"];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            break;
                        }
                        //Français
                       else if ([[[NSLocale availableLocaleIdentifiers] objectAtIndex:k] isEqualToString:@"fr"])
                        {
                            
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[[NSLocale availableLocaleIdentifiers] objectAtIndex:k], @"en",  nil] forKey:@"AppleLanguages"];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            break;
                        }
                        //Dutch
                       else if ([[[NSLocale availableLocaleIdentifiers] objectAtIndex:k] isEqualToString:@"nl"])
                        {
                            
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[[NSLocale availableLocaleIdentifiers] objectAtIndex:k], @"en",  nil] forKey:@"AppleLanguages"];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            break;
                        }
                        //Italiano
                       else if ([[[NSLocale availableLocaleIdentifiers] objectAtIndex:k] isEqualToString:@"it"])
                        {
                            
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[[NSLocale availableLocaleIdentifiers] objectAtIndex:k], @"en",  nil] forKey:@"AppleLanguages"];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            break;
                        }
                        //Spanish
                       else if ([[[NSLocale availableLocaleIdentifiers] objectAtIndex:k] isEqualToString:@"es"])
                       {
                           
                           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                           [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[[NSLocale availableLocaleIdentifiers] objectAtIndex:k], @"en",  nil] forKey:@"AppleLanguages"];
                           [[NSUserDefaults standardUserDefaults]synchronize];
                           break;
                       }

                    }
                }
                
            }
        }
        else
        {
            
            if ([[NSLocale availableLocaleIdentifiers] containsObject:[NSString stringWithFormat:@"%@-%@",country, countryCode]])
                
            {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@-%@",country, countryCode], @"en",  nil] forKey:@"AppleLanguages"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else
            {
                for (int k =0; k <[[NSLocale availableLocaleIdentifiers] count]; k++) {
                    
                    if ([[[NSLocale availableLocaleIdentifiers] objectAtIndex:k] rangeOfString:[NSString stringWithFormat:@"%@",country]].location==NSNotFound)
                    {
                        
                    }
                    else
                    {
                        NSLog(@"%@", [[NSLocale availableLocaleIdentifiers] objectAtIndex:k]);
                        
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
                        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[[NSLocale availableLocaleIdentifiers] objectAtIndex:k], @"en",  nil] forKey:@"AppleLanguages"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        break;
                    }
                }
            }
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@-%@",country, countryCode] forKey:@"Savesetting"];
        
        NSLog(@"preferred_language %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"preferred_language"]);
        NSLog(@"AppleLanguages %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"AppleLanguages"]);
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
