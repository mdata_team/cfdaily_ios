//
//  NHCalendarActivity.m
//  Noite Hoje for iPad
//
//  Created by Otavio Cordeiro on 11/25/12.
//  Copyright (c) 2012 Noite Hoje. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining
//  a copy of this software and associated documentation files (the
//  "Software"), to deal in the Software without restriction, including
//  without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to
//  permit persons to whom the Software is furnished to do so, subject to
//  the following conditions:
//
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
//  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
//  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
//  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "NHMailActivity.h"
#import "AppDelegate.h"

@interface NHMailActivity ()
@property (strong) NSMutableArray *events;
@end

@implementation NHMailActivity
@synthesize parantView;

-(id)init
{
    self = [super init];
    
    if (self) {
        self.events = [NSMutableArray array];
    }
    
    return self;
}

- (NSString *)activityType
{
    return NSStringFromClass([self class]);
}

- (NSString *)activityTitle
{
    return NSLocalizedString(@"Mail",nil);
}

- (UIImage *)activityImage
{
    return [UIImage imageNamed:@"NHCalendarActivityIcon"];
}

-(void)setParant:(UIViewController*)parant

{
    parantView =parant;
}

-(BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];

    for (id item in activityItems) {

        
        if ([item isKindOfClass:[NSObject class]] &&
            (status == EKAuthorizationStatusNotDetermined || status == EKAuthorizationStatusAuthorized)) {
            return YES;
        }
    }

    
    return NO;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems
{
    for (id item in activityItems)
        if ([item isKindOfClass:[NSObject class]])
            [self.events addObject:item];
}

- (void)performActivity
{


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    [self activityDidFinish:YES];
    
double delayInSeconds = 0.3;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [appDelegate SendtheMail];
});

}




@end
