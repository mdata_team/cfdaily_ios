//
//  ViewController.m
//  Menopur
//
//  Created by Jeffrey Snijder on 20-01-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "InstructionsViewController.h"
#import "NewMedecineController.h"
#import "MedicineViewController.h"
#import "PharmacyViewController.h"
#import "StrengthViewControllerScroll.h"
#import <QuartzCore/QuartzCore.h>
#import "AlertViewController.h"
#import "NameItemViewController.h"
#import "ProfileViewController.h"
#import "MinimumViewController.h"
#import "SaveData.h"
#import "TekstIDLabel.h"
#import "DocterViewController.h"
#import "AppDelegate.h"
#import "TypeViewController.h"
#import "PerscriptionViewController.h"
#import "FrequencyViewController.h"
#import "RemainingViewController.h"
#import "DoseViewController.h"
#import "FillingViewController.h"
#import "NoteViewController.h"
#import "MedsViewController.h"
#import "Notification.h"
#import "RecordingViewController.h"
#import "GetData.h"
#import "DurationViewController.h"
#import "MedChoiceView.h"
#import "SSPhotoCropperViewController.h"
#import "SchedularView.h"
#import "TimersTableView.h"
#import "MessageViewController.h"
#import "FrequencyViewControllerNew.h"
#import "SaveCore.h"
#import "ActionArrow.h"
#import "ShowMedecineController.h"

@interface NewMedecineController ()

@end

@implementation NewMedecineController


@synthesize color;
@synthesize swActive;

@synthesize Alerts;


@synthesize musicPlayer;
@synthesize headShot;
@synthesize Nameit;
@synthesize Sound;
@synthesize ColorField;
@synthesize chosenID;
@synthesize parantIt;
@synthesize MedicineSpecs;
@synthesize ChosenMedicen;
@synthesize selectedNotifictions;
@synthesize parantGo;
@synthesize Save;
@synthesize Cancel;
@synthesize Delete;
@synthesize scrollViewSpread;
@synthesize medChoise;
@synthesize photchoice;
@synthesize Combi;
@synthesize Quantity;
@synthesize Meds;
@synthesize Contacts;
@synthesize Schdules;
@synthesize Dose;
@synthesize RenewButton;


-(void) backAction
{


    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:NSLocalizedString(@"Save medicine?",nil)];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:50000];
    [alert addButtonWithTitle:NSLocalizedString(@"Save", nil)];
    [alert addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [alert show];


}


#pragma mark - i  alertView

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{

    //////////////////////////////NSLog(@"%i %i", index, alertView.tag);

    if (alertView.tag ==50000) {


        if (index==0) {



            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.SavingDef =YES;


            [self save:NULL];



        }

        else if (index==1)
        {

            [[self navigationController] popViewControllerAnimated:YES];

        }

    }

    else if (alertView.tag ==464569) {


        if (index==0) {


            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


            NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];

            ////////////////////NSLog(@"%@", typit);

            ////////////////////////////////////////////////////////////////////////////NSLog(@"not refill %@", ChosenMedicen);

            NSString *set =[NSString stringWithFormat:@"%i",[[ChosenMedicen objectForKey:@"Filling"] intValue]+[[ChosenMedicen objectForKey:@"Remaining"] intValue]];

            [ChosenMedicen setObject:set  forKey:@"Remaining"];

            if ([[typit valueForKey:@"Dose_name"] isEqualToString:@"-"]) {

                TekstIDLabel *Label = (TekstIDLabel *)[Dose viewWithTag:(4444)];
                TekstIDLabel *Label1 = (TekstIDLabel *)[Quantity viewWithTag:(4445)];
                TekstIDLabel *Label2 = (TekstIDLabel *)[Quantity viewWithTag:(4446)];
                TekstIDLabel *Label3 = (TekstIDLabel *)[Quantity viewWithTag:(4447)];


                [Label setText:[NSString stringWithFormat:@"%@",@""]];
                [Label1 setText:[NSString stringWithFormat:@"%@",@"Quantity Remaining:"]];
                [Label2 setText:[NSString stringWithFormat:@"%@",@"Quantity Per Filling:"]];
                [Label3 setText:[NSString stringWithFormat:@"%@",@"Minimum Quantity\n   For Refilling:"]];

                [self setlabelframe:Label];
                [self setlabelframe:Label1];
                [self setlabelframe:Label2];
                [self setlabelframe:Label3];



            }
            else
            {
                TekstIDLabel *Label = (TekstIDLabel *)[Dose viewWithTag:(4444)];
                TekstIDLabel *Label1 = (TekstIDLabel *)[Quantity viewWithTag:(4445)];
                TekstIDLabel *Label2 = (TekstIDLabel *)[Quantity viewWithTag:(4446)];
                TekstIDLabel *Label3 = (TekstIDLabel *)[Quantity viewWithTag:(4447)];




                if ([[ChosenMedicen valueForKey:@"Quantity"] integerValue]>1)
                {


                    NSString *uitzondering =[[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_less"] withString:[typit valueForKey:@"Drug_types"]];


                    [Label setText:[NSString stringWithFormat:@"%@", [uitzondering stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"]]];

                }

                else
                {
                    [Label setText:[NSString stringWithFormat:@"%@", [[typit valueForKey:@"Dose_name"]stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"]]];

                }




                if ([[ChosenMedicen valueForKey:@"Remaining"] integerValue]>1)
                {



                    [Label1 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Remaining_name"], NSLocalizedString(@":",nil)]];

                }

                else
                {
                    [Label1 setText:[NSString stringWithFormat:@"%@%@",[typit valueForKey:@"Remaining_name"], NSLocalizedString(@":",nil)]];




                }
                if ([[ChosenMedicen valueForKey:@"Filling"] integerValue]>1)
                {

                    [RenewButton setAlpha:1];
                    [Label2 setText:[NSString stringWithFormat:@"%@%@",[typit valueForKey:@"Refill_name"], NSLocalizedString(@":",nil)]];

                }

                else
                {
                    [RenewButton setAlpha:0];
                    [Label2 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Refill_name"], NSLocalizedString(@":",nil)]];




                }
                if ([[ChosenMedicen valueForKey:@"Refilling"] integerValue]>1)
                {


                    [Label3 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Minimume_name"], NSLocalizedString(@":",nil)]];

                }

                else
                {

                    [Label3 setText:[NSString stringWithFormat:@"%@%@",[typit valueForKey:@"Minimume_name"], NSLocalizedString(@":",nil)]];



                }



                [self setlabelframe:Label];
                [self setlabelframe:Label1];
                [self setlabelframe:Label2];
                [self setlabelframe:Label3];


            }





            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if (label.title) {



                                if ([label.title isEqualToString:@"Remaining"]) {

                                    if ([[ChosenMedicen valueForKey:@"Remaining"] isEqualToString:@" "]) {

                                    }
                                    else
                                    {
                                        if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {

                                            [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Remaining"]]];
                                        }
                                        else
                                        {
                                            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                                            {
//                                                [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Remaining"], [ChosenMedicen valueForKey:@"Dose_unit"]]]; //13-11-2017
                                                [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Remaining"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                                
//                                                if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Dråber"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"])
//                                                {
//                                                    label.text = [label.text stringByReplacingOccurrencesOfString:@"stk" withString:@"stykke"];
//                                                }
                                                
                                            }
                                            else
                                            {
                                                [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Remaining"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                            }
                                            
                                        }
                                    }
                                }




                            }

                        }

                    }

                }

            }


        }



        else if (index==1)
        {


        }

    }


    else
    {
        if (index==1) {


        }

        else
        {


            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

            [appDelegate removeMedicine:[ChosenMedicen valueForKey:@"ID_Medicine"]];
            [appDelegate removeHistory:[ChosenMedicen valueForKey:@"ID_Medicine"]];


            if ( [[[self navigationController] viewControllers] count] >3) {

                [appDelegate.Navigaioncopy removeAllObjects];
                [appDelegate.Navigaioncopy addObject:[[[self navigationController] viewControllers] objectAtIndex:0]];

                MedicineViewController *controller = [[MedicineViewController alloc]init];

                [appDelegate.Navigaioncopy addObject:controller];

                [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];

                [[self navigationController] popViewControllerAnimated:YES];



            }
            else
            {
                //////////////////////////////NSLog(@"4");
                [appDelegate.Navigaioncopy removeAllObjects];

                MedicineViewController *controller = [[MedicineViewController alloc]init];
                [appDelegate.Navigaioncopy addObject:controller];
                [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];

                [[self navigationController] popViewControllerAnimated:NO];
            }

        }


    }

}


-(void)photCropper:(ActionArrow*)image
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate.toolbarDown setAlpha:0];

    SSPhotoCropperViewController *photoCropper =
    [[SSPhotoCropperViewController alloc] initWithPhoto:headShot.image
                                               delegate:self
                                                 uiMode:SSPCUIModePresentedAsModalViewController
                                        showsInfoButton:YES];
    [photoCropper setMinZoomScale:0.25f];
    [photoCropper setMaxZoomScale:3];




    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:photoCropper];
    [self presentViewController:nc animated:YES completion:NULL];

}


-(void)hideAndseek:(NSString*)set
{

}
-(void)somethingHappened:(id)sender
{

}

- (void)viewDidLoad
{

    [SHARED_APPDELEGATE setNavigationbarIconImage:self.navigationController setimge:NO];

    [self.view setBackgroundColor:[UIColor colorWithRed:0.886 green:0.937 blue:0.957 alpha:1.000]];



    selectedNotifictions  = [[NSMutableArray alloc]init];

    //Month_view.png
    
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {

        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;

    }
    else
    {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;

    musicPlayer = [MPMusicPlayerController iPodMusicPlayer];

    [musicPlayer beginGeneratingPlaybackNotifications];

    scrollViewSpread = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-88)];
    [scrollViewSpread setCanCancelContentTouches:NO];
    scrollViewSpread.showsHorizontalScrollIndicator = YES;
    //250, 241, 235
    [scrollViewSpread setBackgroundColor:[UIColor colorWithRed:250.0f/255.0f green:241.0f/255.0f blue:235.0f/255.0f alpha:1.000]];


    scrollViewSpread.delaysContentTouches=YES;
    scrollViewSpread.clipsToBounds = NO;
    scrollViewSpread.scrollEnabled = YES;
    scrollViewSpread.pagingEnabled = NO;
    [scrollViewSpread setDelegate:self];
    scrollViewSpread.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.view  addSubview:scrollViewSpread];


    Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 220)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
//    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Combi.layer setMasksToBounds:YES];
    Combi.layer.shadowOffset = CGSizeMake(3, 0);
    Combi.layer.shadowOpacity = 2;
    Combi.layer.shadowRadius = 2.0;
    [scrollViewSpread addSubview:Combi];


    Dose = [[UIView alloc] initWithFrame:CGRectMake(10, 290, self.view.frame.size.width-20, 79)];
    [Dose setBackgroundColor:[UIColor whiteColor]];
    [Dose.layer setCornerRadius:10];
    [Dose.layer setBorderWidth:1.5];
//    [Dose.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changse
    [Dose.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Dose.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Dose];



    Schdules = [[SchedularView alloc] initWithFrame:CGRectMake(10, 380, self.view.frame.size.width-20, 79+39+39+39+39)];
    [Schdules setBackgroundColor:[UIColor whiteColor]];
    [Schdules.layer setCornerRadius:10];
    [Schdules.layer setBorderWidth:1.5];
//    [Schdules.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Schdules.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Schdules.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Schdules];
    [Schdules setShedulare:self];

    



    Alerts = [[UIView alloc] initWithFrame:CGRectMake(10, 524, self.view.frame.size.width-20, 39)];
    [Alerts setBackgroundColor:[UIColor whiteColor]];
    [Alerts.layer setCornerRadius:10];
    [Alerts.layer setBorderWidth:1.5];
//    [Alerts.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Alerts.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Alerts.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Alerts];


    Quantity = [[UIView alloc] initWithFrame:CGRectMake(10, 609, self.view.frame.size.width-20, 128+39)];
    [Quantity setBackgroundColor:[UIColor whiteColor]];
    [Quantity.layer setCornerRadius:10];
    [Quantity.layer setBorderWidth:1.5];
//    [Quantity.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Quantity.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Quantity.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Quantity];


    Contacts = [[UIView alloc] initWithFrame:CGRectMake(10, 789, self.view.frame.size.width-20, 118)];
    [Contacts setBackgroundColor:[UIColor whiteColor]];
    [Contacts.layer setCornerRadius:10];
    [Contacts.layer setBorderWidth:1.5];
//    [Contacts.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Contacts.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Contacts.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Contacts];



    NSArray *names1 =[NSArray arrayWithObjects:NSLocalizedString(@"Name:",nil),NSLocalizedString(@"Type:",nil),NSLocalizedString(@"Strength:",nil), nil];
    NSArray *Cell1 =[NSArray arrayWithObjects:@"Name#88",@"Type#82",@"Strength#76",nil];

    for (NSInteger i=0 ; i<[names1 count]; i++) {


        NSArray *numbers = [[Cell1 objectAtIndex:i] componentsSeparatedByString:@"#"];



        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 39*i, self.view.frame.size.width-20, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:15]];
        [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names1 objectAtIndex:i], nil)]];
        [MedName setTag:120];

        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Combi addSubview:MedName];


        [MedName sizeToFit];
        [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];


        UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//        [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [Combi addSubview:line];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+15, 39*i, (Combi.frame.size.width-20)-((MedName.frame.size.width+5)+30), 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {

            [Name setTextAlignment:NSTextAlignmentLeft];
        }

        else

        {
            [Name setTextAlignment:NSTextAlignmentRight];
        }

        [Name setTitle:[numbers objectAtIndex:0]];
        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Combi addSubview:Name];




        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Alerts.frame.size.width-35, (39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [NameButton setTitleit:[numbers objectAtIndex:0]];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Combi addSubview:NameButton];

        ActionArrow *Extra = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, 3+(39*i), 300, 40)];
        [Extra setBackgroundColor:[UIColor clearColor]];
        [Extra setTitleit:[numbers objectAtIndex:0]];
        [Extra setTag:140];
        [Extra addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Combi addSubview:Extra];






    }

    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, @"Medhot.png"];


    //[data writeToFile:jpegFilePath atomically:YES];


    headShot = [[UIImageView alloc] initWithFrame:CGRectMake(10, 130, 80, 80)];
//    [headShot setBackgroundColor:[UIColor blackColor]];
    [headShot setTag:130];
    [headShot setImage:[UIImage imageNamed:@"Medhot.png"]];

    if ([UIImage imageWithContentsOfFile:jpegFilePath]) {
        [headShot setImage:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"Image"]]];
    }


    [Combi addSubview:headShot];



    ActionArrow *Photocover = [[ActionArrow alloc]  initWithFrame:CGRectMake(10, 130, 80, 80)];
    [Photocover setTitleit:@"Photo"];

    [Photocover setTag:160];
    [Photocover addTarget:self action:@selector(photCropper:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:Photocover];




    ActionArrow *Photo = [[ActionArrow alloc]  initWithFrame:CGRectMake(100, 171, 39, 40)];
//    [Photo setImage:[UIImage imageNamed:@"photo.png"] forState:UIControlStateNormal]; //29-06-2017 changes
    [Photo setImage:[UIImage imageNamed:@"photo"] forState:UIControlStateNormal];
//    [Photo setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
//    [Photo setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
    [Photo setTitleit:@"Photo"];
    [Photo setTag:160];
    [Photo addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
    [Combi addSubview:Photo];
    [Photo.layer setCornerRadius:4];



    UIView *Assign = [[UIView alloc] initWithFrame:CGRectMake(10, 240, self.view.frame.size.width-20, 40)];
    [Assign setBackgroundColor:[UIColor whiteColor]];
    [Assign.layer setCornerRadius:10];
    [Assign.layer setBorderWidth:1.5];
//    [Assign.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Assign.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [Assign.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Assign];


    NSArray *names2 =[NSArray arrayWithObjects:NSLocalizedString(@"Assign to Profile:",nil), nil];
    NSArray *Cell2 =[NSArray arrayWithObjects:@"Profile#130", nil];

    for (NSInteger i=0 ; i<[names2 count]; i++) {

        NSArray *numbers = [[Cell2 objectAtIndex:i] componentsSeparatedByString:@"#"];



        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 39*i, self.view.frame.size.width-20, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:15]];
        [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names2 objectAtIndex:i], nil)]];
        [MedName setTag:120];

        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Assign addSubview:MedName];


        [MedName sizeToFit];
        [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];


        UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//        [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [Assign addSubview:line];

        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+15, 39*i, (Assign.frame.size.width-20)-((MedName.frame.size.width+5)+30), 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {

            [Name setTextAlignment:NSTextAlignmentLeft];
        }

        else

        {
            [Name setTextAlignment:NSTextAlignmentRight];
        }

        [Name setTitle:[numbers objectAtIndex:0]];

        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Assign addSubview:Name];

        [Name setCenter:CGPointMake(Name.center.x, Assign.frame.size.height/2)];



        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Assign.frame.size.width-35, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [NameButton setTitleit:[numbers objectAtIndex:0]];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Assign addSubview:NameButton];


        ActionArrow *Extra = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, 3+(39*i), 300, 40)];
        [Extra setBackgroundColor:[UIColor clearColor]];
        [Extra setTitleit:[numbers objectAtIndex:0]];
        [Extra setTag:140];
        [Extra addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Assign addSubview:Extra];



    }



    NSArray *names4 =[NSArray arrayWithObjects:NSLocalizedString(@"Alert Message Text:",nil), nil];

    NSArray *Cell4 =[NSArray arrayWithObjects:@"Message#165", nil];


    for (NSInteger i=0 ; i<[names4 count]; i++) {


        NSArray *numbers = [[Cell4 objectAtIndex:i] componentsSeparatedByString:@"#"];

//*********** 04-09-2017
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 39*i, self.view.frame.size.width-120, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:15]];
        [MedName setText:[NSString stringWithFormat:@"%@", [names4 objectAtIndex:i]]];
        [MedName setTag:120];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:2];
        [Alerts addSubview:MedName];
//        [MedName sizeToFit];
        [MedName setCenter:CGPointMake(MedName.center.x, Assign.frame.size.height/2)];



//        [MedName sizeToFit];
        [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];

//04-09-2017
        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width-10, 39*i, 75, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setNumberOfLines:2];
        [Name setText:@""];
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {

            [Name setTextAlignment:NSTextAlignmentLeft];
        }

        else

        {
            [Name setTextAlignment:NSTextAlignmentRight];
        }

        // Name.adjustsFontSizeToFitWidth = YES;
        [Name setTitle:[numbers objectAtIndex:0]];
        Name.lineBreakMode = NSLineBreakByCharWrapping;
        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:2];
        [Alerts addSubview:Name];


        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Alerts.frame.size.width-35, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [NameButton setTitleit:[numbers objectAtIndex:0]];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Alerts addSubview:NameButton];




        ActionArrow *Extra = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, 3+(39*i), 300, 40)];
        [Extra setBackgroundColor:[UIColor clearColor]];
        [Extra setTitleit:[numbers objectAtIndex:0]];
        [Extra setTag:140];
        [Extra addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Alerts addSubview:Extra];


    }






    NSArray *names8 =[NSArray arrayWithObjects:@"    ",NSLocalizedString(@"Instructions:",nil), nil];

    NSArray *Cell8 =[NSArray arrayWithObjects:@"Quantity#140",@"Instructions#120", nil];


    for (NSInteger i=0 ; i<[names8 count]; i++) {


        NSArray *numbers = [[Cell8 objectAtIndex:i] componentsSeparatedByString:@"#"];


        if (i==0) {

            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 39*i, self.view.frame.size.width-20, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 chanegs
            [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:15]];
            [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names8 objectAtIndex:i],nil)]];
            [MedName setTag:4444];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Dose addSubview:MedName];


            [MedName sizeToFit];
            [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];

            UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//            [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [Dose addSubview:line];





        }
        else

        {

            TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 39*i, self.view.frame.size.width-20, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:15]];
            [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names8 objectAtIndex:i], nil)]];
            [MedName setTag:120];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Dose addSubview:MedName];

            [MedName sizeToFit];
            [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];

            UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//            [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [Dose addSubview:line];


        }


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake([[numbers objectAtIndex:1] intValue], 39*i, self.view.frame.size.width-[[numbers objectAtIndex:1] intValue]-60, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {

            [Name setTextAlignment:NSTextAlignmentLeft];
        }

        else

        {
            [Name setTextAlignment:NSTextAlignmentRight];
        }

        [Name setTitle:[numbers objectAtIndex:0]];

        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Dose addSubview:Name];

        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Dose.frame.size.width-35, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [NameButton setTitleit:[numbers objectAtIndex:0]];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Dose addSubview:NameButton];

        ActionArrow *Extra = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, 3+(39*i), 300, 40)];
        [Extra setBackgroundColor:[UIColor clearColor]];
        [Extra setTitleit:[numbers objectAtIndex:0]];
        [Extra setTag:140];
        [Extra addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Dose addSubview:Extra];


    }



    NSArray *names5 =[NSArray arrayWithObjects:NSLocalizedString(@"Quantity Remaining:",nil),NSLocalizedString(@"Quantity Per Filling:",nil),NSLocalizedString(@"Minimum Quantity\nFor Refilling:",nil),NSLocalizedString(@"Alert Quantity for Refilling:",nil), nil];

    //  NSArray *Cell5 =[NSArray arrayWithObjects:@"Remaining#182",@"Filling#180",@"Refilling#192", nil];
    NSArray *Cell5 =[NSArray arrayWithObjects:@"Remaining#182",@"Filling#180",@"Refilling#180",@"ColorGreen#202", nil];

    for (NSInteger i=0 ; i<[names5 count]; i++) {

        NSArray *numbers = [[Cell5 objectAtIndex:i] componentsSeparatedByString:@"#"];

        TekstIDLabel *MedName;
        if (i<2) {

            if (i ==0) {
                MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 39*i, self.view.frame.size.width- 20, 40)];
//                [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 chanegs
                [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                [MedName setFont:[UIFont boldSystemFontOfSize:15]];
                [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names5 objectAtIndex:i], nil)]];
                [MedName setTag:4445];

                [MedName setBackgroundColor:[UIColor clearColor]];
                [MedName setNumberOfLines:3];
                [Quantity addSubview:MedName];


                [MedName sizeToFit];
                [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];

                UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//                [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
                [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                [Quantity addSubview:line];

                  MedName.centerit =39*i;

            }
            else if (i ==1) {


                MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 39*i, self.view.frame.size.width-20, 40)];
//                [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 chanegs
                [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                [MedName setFont:[UIFont boldSystemFontOfSize:15]];
                [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names5 objectAtIndex:i], nil)]];
                [MedName setTag:4446];

                [MedName setBackgroundColor:[UIColor clearColor]];
                [MedName setNumberOfLines:3];
                [Quantity addSubview:MedName];


                [MedName sizeToFit];
                [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];

                UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//                [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
                [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                [Quantity addSubview:line];


                MedName.centerit =39*i;
            }

            else
            {
                MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 39*i, self.view.frame.size.width-20, 40)];
//                [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 chanegs
                [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                [MedName setFont:[UIFont boldSystemFontOfSize:15]];
                [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names5 objectAtIndex:i], nil)]];
                [MedName setTag:120];
                [MedName setBackgroundColor:[UIColor clearColor]];
                [MedName setNumberOfLines:3];
                [Quantity addSubview:MedName];



                [MedName sizeToFit];
                [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];


                UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//                [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
                [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                [Combi addSubview:line];

                  MedName.centerit =39*i;

            }

        }
        if (i==2) {

            MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, (39*i), self.view.frame.size.width-20, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:15]];
            [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names5 objectAtIndex:i], nil)]];
            [MedName setTag:4447];
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Quantity addSubview:MedName];


            [MedName sizeToFit];
            [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];

            UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//            [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [Quantity addSubview:line];

            MedName.centerit =39*i;



        }
        if (i==3) {
            MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, (39*i)+10, self.view.frame.size.width-110, 40)];
//            [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedName setFont:[UIFont boldSystemFontOfSize:15]];
            [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([names5 objectAtIndex:i], nil)]];
            [MedName setTag:120];
            MedName.lineBreakMode = NSLineBreakByWordWrapping;
            [MedName setBackgroundColor:[UIColor clearColor]];
            [MedName setNumberOfLines:3];
            [Quantity addSubview:MedName];


//            [MedName sizeToFit];
//            [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];


            UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//            [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [Quantity addSubview:line];

            MedName.centerit =39*i;

        }

        if (i<3) {

//04-09-2017
            TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width - (40), 39*i, self.view.frame.size.width-(MedName.frame.size.width+15), 40)];
            [Name setTextColor:[UIColor blackColor]];
            [Name setFont:[UIFont systemFontOfSize:14]];
            [Name setText:@""];
            if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {

                [Name setTextAlignment:NSTextAlignmentLeft];
            }

            else

            {
                [Name setTextAlignment:NSTextAlignmentRight];
            }

            [Name setTitle:[numbers objectAtIndex:0]];

            [Name setTag:120];
            [Name setBackgroundColor:[UIColor clearColor]];
            [Name setNumberOfLines:3];
            [Quantity addSubview:Name];
            




            UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//            [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 chanegs
            [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [Quantity addSubview:line];



            ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Quantity.frame.size.width-35, 3+(39*i), 60/2, 72/2)];
            [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
            [NameButton setTitleit:[numbers objectAtIndex:0]];
            [NameButton setTag:140];
            [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
            [Quantity addSubview:NameButton];


            ActionArrow *Extra = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, 3+(39*i), 300, 40)];
            [Extra setBackgroundColor:[UIColor clearColor]];
            [Extra setTitleit:[numbers objectAtIndex:0]];
            [Extra setTag:140];
            [Extra addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
            [Quantity addSubview:Extra];

        }
        else
        {

            NSString *deviceType = [UIDevice currentDevice].systemVersion;
            NSRange range = NSMakeRange (0, 1);
            //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                swActive = [[UISwitch alloc] initWithFrame:CGRectMake(240, 14+(39*i), 100,50)];
                [swActive setTag:100+i];
                //sukker ben ik!!
//                [swActive setOnTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changse
//                [swActive setOnTintColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                //11-07-2017
            [swActive setOnTintColor:[UIColor colorWithRed:101.0f/255.0f green:184.0f/255.0f blue:136.0f/255.0f alpha:1.000]];

                [swActive addTarget:self action:@selector(switchValueReminder:) forControlEvents:UIControlEventValueChanged];
                [Quantity addSubview:swActive];
                [swActive setOn:TRUE animated:FALSE];

            }

            else
            {

                swActive = [[UISwitch alloc] initWithFrame:CGRectMake(215, 16+(39*i), 100,50)];
                [swActive setTag:100+i];
                //sukker ben ik!!
//                [swActive setOnTintColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
                [swActive setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
                [swActive addTarget:self action:@selector(switchValueReminder:) forControlEvents:UIControlEventValueChanged];
                [Quantity addSubview:swActive];
                [swActive setOn:TRUE animated:FALSE];
            }




        }





    }



    NSArray *names6 =[NSArray arrayWithObjects:NSLocalizedString(@"Doctor:",nil),NSLocalizedString(@"Pharmacy:",nil),NSLocalizedString(@"Prescription Notes:",nil), nil];

    NSArray *Cell6 =[NSArray arrayWithObjects:@"Doctor#65",@"Pharmacy#172",NSLocalizedString(@"Notes#150",nil), nil];

    for (NSInteger i=0 ; i<[names6 count]; i++) {


        NSArray *numbers = [[Cell6 objectAtIndex:i] componentsSeparatedByString:@"#"];


        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 39*i, self.view.frame.size.width-20, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:15]];
        [MedName setText:[NSString stringWithFormat:@"%@", [names6 objectAtIndex:i]]];
        [MedName setTag:120];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Contacts addSubview:MedName];

        [MedName sizeToFit];
        [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];


        UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//        [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [Contacts addSubview:line];

        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+12, 39*i, (Contacts.frame.size.width-20)-((MedName.frame.size.width+5)+30), 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@""];

        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {

            [Name setTextAlignment:NSTextAlignmentLeft];
        }

        else

        {
            [Name setTextAlignment:NSTextAlignmentRight];
        }


        [Name setTitle:[numbers objectAtIndex:0]];

        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Contacts addSubview:Name];


        if ([[numbers objectAtIndex:0] isEqualToString:@"Prescription"]||[[numbers objectAtIndex:0] isEqualToString:@"Notes"]) {

            [Name setNumberOfLines:1];

        }

        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Contacts.frame.size.width-35, 3+(39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [NameButton setTitleit:[numbers objectAtIndex:0]];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Contacts addSubview:NameButton];


        ActionArrow *Extra = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, 3+(39*i), 300, 40)];
        [Extra setBackgroundColor:[UIColor clearColor]];
        [Extra setTitleit:[numbers objectAtIndex:0]];
        [Extra setTag:140];
        [Extra addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Contacts addSubview:Extra];


    }

    [scrollViewSpread setContentSize:CGSizeMake(320, 920+79)];

    //142 × 36 pixels


    Save = [[UIButton alloc] initWithFrame:CGRectMake(10, 770+39, 142/1.1,  36/1.1)];
    [Save setBackgroundColor:[UIColor clearColor]];
    [Save setShowsTouchWhenHighlighted:NO];
    [Save setImage:[UIImage imageNamed:@"save.png"] forState:UIControlStateNormal];
    [Save setAutoresizesSubviews:YES];
    [Save addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:Save];
    [Save setAlpha:0];



    //142 × 36 pixels

    Cancel = [[UIButton alloc] initWithFrame:CGRectMake(180,770+39, 142/1.1,  36/1.1)];
    [Cancel setBackgroundColor:[UIColor clearColor]];
    [Cancel setShowsTouchWhenHighlighted:NO];
    [Cancel setImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
    [Cancel setAutoresizesSubviews:YES];
    [Cancel addTarget:self action:@selector(Cancel:) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:Cancel];
    [Cancel setAlpha:0];



    Delete = [[UIButton alloc] initWithFrame:CGRectMake(190,990+35, 120,  38/1.1)];
//    [Delete setBackgroundColor:[UIColor colorWithRed:0.553 green:0.000 blue:0.047 alpha:1.000]]; //01-07-2017
    [Delete setBackgroundColor:[UIColor colorWithRed:0.998 green:0.396 blue:0.396 alpha:1.000]];
    [Delete setTitle:NSLocalizedString(@"Delete",nil) forState:UIControlStateNormal];
    Delete.titleLabel.layer.shadowOffset = CGSizeMake(1, 1);
    Delete.titleLabel.layer.shadowOpacity = 1;
    Delete.titleLabel.layer.shadowRadius = 1.0;
    [Delete.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [Delete.titleLabel setTextColor:[UIColor whiteColor]];
    [Delete setShowsTouchWhenHighlighted:NO];
    [Delete setAutoresizesSubviews:YES];
    [Delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:[self setbutton:Delete]];


    //bottom - 119-197-152 //top - 101-184-136
    RenewButton = [[UIButton alloc] initWithFrame:CGRectMake(10,990+35, 300,  38/1.1)];
//    [RenewButton setBackgroundColor:[UIColor colorWithRed:0.000 green:0.502 blue:0.000 alpha:1.000]]; //29-06-2017 changes
    [RenewButton setBackgroundColor:[UIColor colorWithRed:0.396 green:0.722 blue:0.533 alpha:1.000]];
    [RenewButton setTitle:NSLocalizedString(@"Refill",nil) forState:UIControlStateNormal];
    RenewButton.titleLabel.layer.shadowOffset = CGSizeMake(1, 1);
    RenewButton.titleLabel.layer.shadowOpacity = 1;
    RenewButton.titleLabel.layer.shadowRadius = 1.0;
    [RenewButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [RenewButton.titleLabel setTextColor:[UIColor whiteColor]];
    [RenewButton setShowsTouchWhenHighlighted:NO];
    [RenewButton setAutoresizesSubviews:YES];
    [RenewButton addTarget:self action:@selector(Renew:) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:[self setbutton:RenewButton]];



    // MedChoiceView.h




    medChoise = [[MedChoiceView alloc] initWithFrame:CGRectMake(10,10,300,460)];
    [medChoise setBackgroundColor:[UIColor whiteColor]];
    [medChoise setAutoresizesSubviews:YES];
    [medChoise.layer setBorderWidth:1];
    [medChoise.layer setCornerRadius:10];
    [medChoise.layer setMasksToBounds:YES];
    [medChoise getparant:self];
//    [medChoise.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [medChoise.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
    [self.view addSubview:medChoise];
    [medChoise setAlpha:0];




    NSArray *timesIt = [[ChosenMedicen valueForKey:@"Times"] componentsSeparatedByString:@","];


    if ([timesIt count] ==1||[timesIt count] ==0) {


        [self DynamicCell:34+39+39+39];
    }
    else
    {
        [self DynamicCell:(34+39+39+(39*[timesIt count]))];

    }


    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {


        //NSLog(@"left");


        for (UIView *view in self.view.subviews) {


            for (UIView *content in view.subviews) {


                if ([content isKindOfClass:[SchedularView class]]) {


                    content.transform = CGAffineTransformMakeScale(-1, 1);

                    for (UILabel *label in content.subviews) {


                        if ([label isKindOfClass:[UILabel class]]) {

                            label.transform = CGAffineTransformMakeScale(-1, 1);
                            [label setTextAlignment:NSTextAlignmentLeft];

                        }

                    }

                }

                else

                {

                    content.transform = CGAffineTransformMakeScale(-1, 1);


                    for (UILabel *label in content.subviews) {


                        if ([label isKindOfClass:[UILabel class]]) {

                            label.transform = CGAffineTransformMakeScale(-1, 1);

                        }

                    }
                    for (UIImageView *label in content.subviews) {


                        if ([label isKindOfClass:[UIImageView class]]) {

                            label.transform = CGAffineTransformMakeScale(-1, 1);


                        }

                    }
                    for (TekstIDLabel *label in content.subviews) {


                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            label.transform = CGAffineTransformMakeScale(-1, 1);


                        }

                    }
                    for (UIButton *label in content.subviews) {


                        if ([label isKindOfClass:[UIButton class]]) {

                            label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                            [label.titleLabel setTextAlignment:NSTextAlignmentNatural];

                        }

                    }


                }

            }

        }

        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];


    } else {

        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }



    [super viewDidLoad];



    // Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - i  Renew

-(void)Renew:(UIButton*)sender

{

    if ([[ChosenMedicen valueForKey:@"Refilling"] integerValue]>1)
    {
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:NSLocalizedString(@"Start refilling", nil)];
        [alert setMessage:@""];
        [alert setDelegate:self];
        [alert setTag:464569];
        [alert addButtonWithTitle:NSLocalizedString(@"Refill now", nil)];
        [alert addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
        [alert show];

    }
    else


    {



    }

}




-(void)DynamicCell:(NSInteger)difference

{

    //////////////////////////////////NSLog(@"%i", difference);

    ////////////////////////////////////////////////////////////////////////////NSLog(@"%f", Quantity.frame.origin.y);

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    appDelegate.TypeMed =[ChosenMedicen valueForKey:@"Type"];

    NSArray *Times = [[ChosenMedicen valueForKey:@"Times"] componentsSeparatedByString:@","];


    //////////////////////////NSLog(@"%@", typit);

    [Schdules setFrame:CGRectMake(10, 380, self.view.frame.size.width-20, 79+39+(difference/2)+40)];
    [Schdules.times setFrame:CGRectMake(Schdules.times.frame.origin.x, Schdules.times.frame.origin.y,  Schdules.times.frame.size.width, 20*[Times count])];

    [Schdules.related setFrame:CGRectMake(0, 15, 170, (difference/2))];

    [Schdules.ContentView setFrame:CGRectMake(120, 145, 170, 75+(difference/2))];



    [Alerts setFrame:CGRectMake(10, Schdules.frame.origin.y+Schdules.frame.size.height+15, self.view.frame.size.width-20, 39)];



    [Quantity setFrame:CGRectMake(10,Alerts.frame.origin.y+Alerts.frame.size.height+15, self.view.frame.size.width-20, 128+39)];
    [Contacts setFrame:CGRectMake(10,Quantity.frame.origin.y+Quantity.frame.size.height+15, self.view.frame.size.width-20, 118)];

    [Delete setFrame:CGRectMake(190,Contacts.frame.origin.y+Contacts.frame.size.height+15, 120,  38/1.1)];
    [RenewButton setFrame:CGRectMake(10,Contacts.frame.origin.y+Contacts.frame.size.height+15, 150,  38/1.1)];
    [scrollViewSpread setContentSize:CGSizeMake(320, RenewButton.frame.origin.y+RenewButton.frame.size.height+35)];




    ////////////////////////////////////////////////////////////////////////////NSLog(@"%f", Quantity.frame.origin.y);
}

-(UIButton*)setbutton:(UIButton*)sender

{

    CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
    shineLayer2.frame = sender.layer.bounds;
    shineLayer2.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          nil];
    shineLayer2.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];



    [sender.layer addSublayer:shineLayer2];
    [sender.layer setMasksToBounds:YES];
    [sender.layer setCornerRadius:4];
    [sender.layer setBorderWidth:1.5];
    [sender.layer setBorderColor:sender.backgroundColor.CGColor];

    return sender;
}





-(void)clear:(UIButton*)sender

{


    ChosenMedicen =[[NSMutableDictionary alloc]init];

    NSArray *names =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",@"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit", nil];

    for (NSInteger i=0 ; i<[names count]; i++) {


        [[NSUserDefaults standardUserDefaults] setObject:@" " forKey:[names objectAtIndex:i]];

        [ChosenMedicen setObject:@" " forKey:[names objectAtIndex:i]];



    }



    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    chosenID = [appDelegate newUUID];

    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, @"Medhot.png"];

    [ChosenMedicen setObject:chosenID forKey:@"ID_Medicine"];
    [ChosenMedicen setObject:appDelegate.CurrentID forKey:@"ID"];
    [ChosenMedicen setObject:jpegFilePath forKey:@"Image"];

    [ChosenMedicen setObject:jpegFilePath  forKey:@"PerscriptionImage"];



    [self setItems:ChosenMedicen];


}


-(void)editOrShow:(NSString*)sender
{

    //////////////////////////NSLog(@"%@", sender);

    [medChoise setAlpha:0];
    [self.scrollViewSpread setAlpha:1];


    if ([sender isEqualToString:@"Add"])
    {
        //Add Medicine
        
        //********** Google Analytics ****************
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"M_Add Medicine_iOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        //********** Google Analytics ****************
        
        if ([SHARED_APPDELEGATE isNetworkReachable])
        {
            [SHARED_APPDELEGATE sendHitsInBackground];
        }
        
        
        [Delete setAlpha:0];


        //
        self.title =NSLocalizedString(@"Add Medicine", nil);


        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


            // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:15];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            label.numberOfLines =2;
            [self.navigationItem.titleView  setBackgroundColor:[UIColor clearColor]];
            label.text = self.title;
            [label sizeToFit];

        }
        else
        {

        }


        NSData *data = [NSData dataWithData:UIImagePNGRepresentation(headShot.image)];

        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];


        [data writeToFile:jpegFilePath atomically:YES];



    }

    if ([sender isEqualToString:@"Edit"])
    {

       //Edit Medicine
        
//        //********** Google Analytics ****************
//
//        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//        [tracker set:kGAIScreenName value:@"Edit Medicine"];
//        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
//
//        //********** Google Analytics ****************
        
        if ([SHARED_APPDELEGATE isNetworkReachable])
        {
            [SHARED_APPDELEGATE sendHitsInBackground];
        }

        
        
        self.title =NSLocalizedString(@"Edit Medicine",nil);


        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {

            self.title =NSLocalizedString(@"Edit Medicine",nil);
            // this will appear as the title in the navigation bar
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:15];
            label.textAlignment = NSTextAlignmentCenter;
            // ^-Use NSTextAlignmentCenter for older SDKs.
            label.textColor = [UIColor whiteColor]; // change this color
            self.navigationItem.titleView = label;
            label.numberOfLines =2;
            [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
            label.text = self.title;
            [label sizeToFit];

        }



        [Delete setAlpha:1];



        [Schdules setTableview:[self setDaysinaRow:ChosenMedicen]];

    }

}


-(NSMutableArray*) setDaysinaRow:(NSMutableDictionary*)days

{
    NSMutableArray *getIt=[[NSMutableArray alloc]init];




    NSArray *numbers2 = [[days valueForKey:@"Dates"] componentsSeparatedByString:@"#"];


    for (NSInteger i=0 ; i<[numbers2 count]; i++) {

        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy"];


        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatter2 setDateFormat:@"dd, MMMM yyyy"];



        NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
        [set setObject:[numbers2 objectAtIndex:i] forKey:@"Date"];
        [set setObject:@"" forKey:@"DateAssent"];
        [set setObject:[days valueForKey:@"Day_number"] forKey:@"Day_number"];
        [set setObject:[days valueForKey:@"Duration"] forKey:@"Ending"];
        [set setObject:[days valueForKey:@"Frequency"] forKey:@"Interval"];
        [set setObject:[days valueForKey:@"Frequency_text"] forKey:@"Setting"];


        NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
        [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];

        [set setObject:[dateConvert stringFromDate:[days valueForKey:@"Starting"]] forKey:@"Starting"];
        [set setObject:[days valueForKey:@"Times"] forKey:@"Times"];
        [getIt addObject:set];



    }


    return getIt;
}


-(void)switchValueReminder:(UISwitch*)sender
{

    [self addActionForGoogleAnalytics:@"Refill Action "];
    
    if (sender.isOn) {

        [ChosenMedicen setObject:@"YES" forKey:@"ColorGreen"];
        [sender setOn:TRUE animated:FALSE];
    }
    else
    {
        [ChosenMedicen setObject:@"NO" forKey:@"ColorGreen"];
        [sender setOn:FALSE animated:FALSE];
    }




}

- (IBAction)showMediaPicker:(id)sender
{


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate.toolbarDown setAlpha:0];


    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];

    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    [self presentViewController:mediaPicker animated:YES completion:NULL];

}

-(void)Next:(UIButton*)sender

{

}

- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{



    MPMediaItem *anItem = (MPMediaItem *)[mediaItemCollection.items objectAtIndex:0];

    NSURL *assetURL = [anItem valueForProperty: MPMediaItemPropertyAssetURL];


    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);

    NSString *documentsDir = [NSString stringWithFormat:@"/Caches/%@",[paths2 objectAtIndex:0]];





    NSData *data2 = [NSData dataWithContentsOfURL:assetURL];

    [data2 writeToFile:[NSString stringWithFormat:@"%@/Copy.mp3", documentsDir] atomically:YES];



    [self dismissViewControllerAnimated:YES completion:NULL];


}

- (void) photoCropper:(SSPhotoCropperViewController *)photoCropper
         didCropPhoto:(UIImage *)photo
{



    [headShot setImage:[self thumbWithSideOfLength2:120 image:photo]];
    [photoCropper dismissViewControllerAnimated:YES  completion:nil];

    NSData *data = [NSData dataWithData:UIImagePNGRepresentation(photo)];

    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];


    [data writeToFile:jpegFilePath atomically:YES];
    [ChosenMedicen setObject:jpegFilePath forKey:@"Image"];

}


- (void) photoCropperDidCancel:(SSPhotoCropperViewController *)photoCropper
{



    [photoCropper dismissViewControllerAnimated:YES  completion:nil];
}

-(void)getCameraPicture:(UIButton*)sender
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    photchoice =@"getCameraPicture";


    [appDelegate.toolbarDown setAlpha:0];




    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
#if (TARGET_IPHONE_SIMULATOR)
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
#endif
    [self presentViewController: picker animated:YES completion:NULL];
}


-(void)imagePickerController:(UIImagePickerController *)picker
      didFinishPickingImage : (UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{





    [headShot setImage:[self thumbWithSideOfLength2:120 image:image]];



    NSData *data = [NSData dataWithData:UIImagePNGRepresentation(image)];

    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];


    [data writeToFile:jpegFilePath atomically:YES];
    [ChosenMedicen setObject:jpegFilePath forKey:@"Image"];

    if ([photchoice isEqualToString:@"selectExitingPicture"]) {

    }
    else if ([photchoice isEqualToString:@"getCameraPicture"]) {

        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
    }


    [self dismissViewControllerAnimated:YES completion:NULL];


    [headShot setImage:image];



}


- (UIImage *)thumbWithSideOfLength2:(float)length image:(UIImage*)mainImage {


    UIImage *thumbnail;

    if (mainImage.size.width< length) {

        CGSize itemSiz1 = CGSizeMake(mainImage.size.width*(length/mainImage.size.width), mainImage.size.height*(length/mainImage.size.width));

        UIGraphicsBeginImageContextWithOptions(itemSiz1, NO, 0.0);

        CGRect imageRect2 = CGRectMake(0.0, 0.0, itemSiz1.width, itemSiz1.height);
        [mainImage drawInRect:imageRect2];

        mainImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }

    UIImageView *mainImageView = [[UIImageView alloc] initWithImage:mainImage];
    BOOL widthGreaterThanHeight = (mainImage.size.width > mainImage.size.height);
    float sideFull = (widthGreaterThanHeight) ? mainImage.size.height : mainImage.size.width;
    CGRect clippedRect = CGRectMake(0, 0, sideFull, sideFull);

    UIGraphicsBeginImageContext(CGSizeMake(length, length));
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextClipToRect( currentContext, clippedRect);
    CGFloat scaleFactor = length/sideFull;
    if (widthGreaterThanHeight) {

        CGContextTranslateCTM(currentContext, -((mainImage.size.width-sideFull)/2)*scaleFactor, 0);

    }
    else {
        CGContextTranslateCTM(currentContext, 0, -((mainImage.size.height - sideFull) / 2) * scaleFactor);
    }

    CGContextScaleCTM(currentContext, scaleFactor, scaleFactor);
    [mainImageView.layer renderInContext:currentContext];
    thumbnail = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();



    return thumbnail;

}


-(void)setItems:(NSMutableDictionary*)set

{

    //NSLog(@"%@", ChosenMedicen);

    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){


        ////////////////////////////////////////////////NSLog(@"setItems");


        chosenID = [set valueForKey:@"ID_Medicine"];

        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.strength_unit = NSLocalizedString([set valueForKey:@"Strength_unit"],nil);
        appDelegate.dose_unit = NSLocalizedString([set valueForKey:@"Dose_unit"],nil);

        appDelegate.TypeMed =[set valueForKey:@"Type"];


        NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];


        ////////////////////NSLog(@"%@", typit);





        ChosenMedicen =[[NSMutableDictionary alloc]initWithDictionary:set];


        if ([[typit valueForKey:@"Dose_name"] isEqualToString:@"-"]) {

            TekstIDLabel *Label = (TekstIDLabel *)[Dose viewWithTag:(4444)];
            TekstIDLabel *Label1 = (TekstIDLabel *)[Quantity viewWithTag:(4445)];
            TekstIDLabel *Label2 = (TekstIDLabel *)[Quantity viewWithTag:(4446)];
            TekstIDLabel *Label3 = (TekstIDLabel *)[Quantity viewWithTag:(4447)];


            [Label setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Quantity Not relevant:",nil)]];

            [Label1 setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Applications Remaining:",nil)]];


            [Label2 setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Applications Per Refill:",nil)]];
            [Label3 setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Applications\nFor Refilling:",nil)]];

            [self setlabelframe:Label];
            [self setlabelframe:Label1];
            [self setlabelframe:Label2];
            [self setlabelframe:Label3];



            //[Contacts setFrame:CGRectMake(10, 609, self.view.frame.size.width-20, 118)];

        }
        else
        {
            TekstIDLabel *Label = (TekstIDLabel *)[Dose viewWithTag:(4444)];
            TekstIDLabel *Label1 = (TekstIDLabel *)[Quantity viewWithTag:(4445)];
            TekstIDLabel *Label2 = (TekstIDLabel *)[Quantity viewWithTag:(4446)];
            TekstIDLabel *Label3 = (TekstIDLabel *)[Quantity viewWithTag:(4447)];

            //////////////////////////////////////////////////////////////////////////////NSLog(@"%@", [typit valueForKey:@"Drug_less"]);

            if ([[ChosenMedicen valueForKey:@"Quantity"] integerValue]>1)
            {


                NSString *uitzondering =[[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_less"] withString:[typit valueForKey:@"Drug_types"]];





                [Label setText:[NSString stringWithFormat:@"%@%@", [uitzondering stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"], NSLocalizedString(@":",nil)]];

                ////////////////////////////////////////////////NSLog(@"%@", Label.text);

            }

            else
            {
                [Label setText:[NSString stringWithFormat:@"%@%@", [[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"],NSLocalizedString(@":",nil)]];

            }


            if ([[ChosenMedicen valueForKey:@"Remaining"] integerValue]>1)
            {

                [Label1 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Remaining_name"],NSLocalizedString(@":",nil)]];



            }

            else
            {

                [Label1 setText:[NSString stringWithFormat:@"%@%@",[typit valueForKey:@"Remaining_name"],NSLocalizedString(@":",nil)]];

            }


            if ([[ChosenMedicen valueForKey:@"Filling"] integerValue]>1)
            {

                [RenewButton setAlpha:1];
                [Label2 setText:[NSString stringWithFormat:@"%@%@",  [typit valueForKey:@"Refill_name"],NSLocalizedString(@":",nil)]];

            }

            else
            {
                [RenewButton setAlpha:0];

                [Label2 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Refill_name"],NSLocalizedString(@":",nil)]];




            }
            if ([[ChosenMedicen valueForKey:@"Refilling"] integerValue]>1)
            {



                [Label3 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Minimume_name"],NSLocalizedString(@":",nil)]];

            }

            else
            {

                [Label3 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Minimume_name"], NSLocalizedString(@":",nil)]];





            }


            [self setlabelframe:Label];
            [self setlabelframe:Label1];
            [self setlabelframe:Label2];
            [self setlabelframe:Label3];







        }




        for (UIScrollView *scroll in self.view.subviews) {

            for (UIView *view in scroll.subviews) {

                for (UIButton *button in view.subviews) {


                    if ([button isKindOfClass:[UIButton class]]) {

                        [button setAlpha:1];


                        if ([[typit valueForKey:@"Dose_name"] isEqualToString:@"-"]) {

                            if ([button.titleLabel.text isEqualToString:@"Quantity"]) {
                                ////////////////////////////////////////////////////////////////////////////NSLog(@"button %@", button.titleLabel.text);
                                [button setAlpha:0];
                            }
                        }
                        else
                        {

                            if ([button.titleLabel.text isEqualToString:@"Quantity"]) {
                                ////////////////////////////////////////////////////////////////////////////NSLog(@"button %@", button.titleLabel.text);
                                [button setAlpha:1];
                            }
                        }

                    }
                }

                for (TekstIDLabel *label in view.subviews) {

                    if ([label isKindOfClass:[TekstIDLabel class]]) {

                        if (label.title) {

                            if ([label.title isEqualToString:@"Starting"]) {



                            }

                            else
                            {
                                [label setText:[set valueForKey:label.title]];
                            }

                            if ([label.title isEqualToString:@"Dates"]) {



                                NSArray *numbers2 = [[ChosenMedicen valueForKey:@"Dates"] componentsSeparatedByString:@"#"];




                                if ([numbers2 count]==1) {


                                }
                                else
                                {

                                    [label setText:[self getnumberOfday:[ChosenMedicen valueForKey:@"Day_number"]]];


                                }

                            }


                            else if ([label.title isEqualToString:@"Recording"]) {


                                if ([[ChosenMedicen valueForKey:@"Recording"] isEqualToString:@"444"]) {

                                    [label setText:@" "];
                                }
                                else
                                {



                                    [label setText:[[[GetData getRecording] valueForKey:@"title"]  objectAtIndex:[[[GetData getRecording] valueForKey:@"ID"] indexOfObject:[ChosenMedicen valueForKey:@"Recording"]]]];
                                }

                            }
                            else if ([label.title isEqualToString:@"Strength"]) {


                                if ([[ChosenMedicen valueForKey:@"Strength"] isEqualToString:@" "]) {


                                }
                                else
                                {

                                    [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Strength"], [ChosenMedicen valueForKey:@"Strength_unit"]]];
                                }
                            }
                            else if ([label.title isEqualToString:@"Remaining"]) {

                                if ([[ChosenMedicen valueForKey:@"Remaining"] isEqualToString:@" "]) {

                                }
                                else
                                {
                                    if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {


                                        //////////////////////NSLog(@"1");
                                        [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Remaining"]]];
                                    }
                                    else
                                    {

                                        //////////////////////NSLog(@"2");
                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Remaining"], [ChosenMedicen valueForKey:@"Dose_unit"]]];

                                    }
                                }
                            }
                            else if ([label.title isEqualToString:@"Quantity"]) {

                                if ([[typit valueForKey:@"Dose_name"] isEqualToString:@"-"]) {


                                    [label setAlpha:0];
                                }

                                else
                                {

                                    [label setAlpha:1];
                                    if ([[ChosenMedicen valueForKey:@"Quantity"] isEqualToString:@" "]) {

                                    }
                                    else
                                    {
                                        if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {

                                            [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Quantity"]]];

                                        }

                                        else
                                        {
                                            [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Quantity"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                        }
                                    }
                                }
                            }
                            else if ([label.title isEqualToString:@"Filling"]) {



                                if ([[ChosenMedicen valueForKey:@"Filling"] isEqualToString:@" "]) {

                                }
                                else
                                {
                                    if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {


                                        [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Filling"]]];
                                    }
                                    else
                                    {
                                        //100
                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Filling"], [ChosenMedicen valueForKey:@"Dose_unit"]]];

                                    }
                                }
                            }
                            else if ([label.title isEqualToString:@"Refilling"]) {



                                if ([[ChosenMedicen valueForKey:@"Refilling"] isEqualToString:@" "]) {

                                }
                                else
                                {
                                    if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {

                                        [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Refilling"]]];
                                    }
                                    else
                                    {
                                        //111
//                                        [label setBackgroundColor:[UIColor redColor]];
//                                        [label setFrame:CGRectMake(label.frame.origin.x-20, label.frame.origin.y, 120, 40)];
//                                        [label setTextColor:[UIColor blackColor]];
                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Refilling"], [ChosenMedicen valueForKey:@"Dose_unit"]]];

                                    }
                                }
                            }

                            else if ([label.title isEqualToString:@"Starting"]) {


                                NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                                [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                                [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];

                                //////////////////////NSLog(@"%@", ChosenMedicen);


                                [label setText:[dateConvert stringFromDate:[ChosenMedicen valueForKey:@"Starting"]]];

                            }

                            else if ([label.title isEqualToString:@"Times"]) {



                                NSArray *timesIt = [[ChosenMedicen valueForKey:@"Times"] componentsSeparatedByString:@","];


                                if ([timesIt count] ==1||[timesIt count] ==0) {

                                    [self DynamicCell:34+39+39+39];
                                }
                                else
                                {
                                    [self DynamicCell:(34+39+39+(39*[timesIt count]))];

                                }

                                [label setText:[[appDelegate convertList:[ChosenMedicen valueForKey:@"Times"]] stringByReplacingOccurrencesOfString:@"," withString:@"\n"]];

                                NSArray *numbers2 = [[ChosenMedicen valueForKey:@"Times"] componentsSeparatedByString:@","];




                                [label setNumberOfLines:[numbers2 count]];


                            }


                            else if ([label.title isEqualToString:@"Pharmacy"]) {




                                [label setText:[ChosenMedicen valueForKey:@"Pharmacy"]];



                            }
                            else if ([label.title isEqualToString:@"Type"]) {




                                [label setText:[ChosenMedicen valueForKey:@"Type"]];



                            }
                            else if ([label.title isEqualToString:@"Name"]) {




                                [label setText:[ChosenMedicen valueForKey:@"Name"]];



                            }



                            //prescription

                            else if ([label.title isEqualToString:@"Notes"]) {



                                [label setText:[ChosenMedicen valueForKey:@"Prescription"]];

                            }

                            else if ([label.title isEqualToString:@"Frequency_text"]) {


                                if ([selectedNotifictions count] ==0) {

                                    [label setText:@" "];
                                }
                                else
                                {
                                    [label setText:[ChosenMedicen valueForKey:@"Frequency_text"]];

                                }

                            }


                            else if ([label.title isEqualToString:@"Message"]) {



                                ////////////////////////////////////////////////NSLog(@"%@", label);
                                [label setText:[ChosenMedicen valueForKey:@"Message"]];


                            }

                            else if ([label.title isEqualToString:@"Frequency_number"]) {

                                if ([NSLocalizedString([ChosenMedicen valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {


                                    NSMutableArray *timesIt = [[[ChosenMedicen valueForKey:@"Frequency_text"] componentsSeparatedByString:@" "] mutableCopy];

                                    [timesIt replaceObjectAtIndex:1 withObject:[ChosenMedicen valueForKey:@"Frequency"]];


                                    [label setText:[NSString stringWithFormat:@"%@ %@ %@", [timesIt objectAtIndex:0],[timesIt objectAtIndex:1],[timesIt objectAtIndex:2]]];

                                    [Schdules.Frequent setText:@""];
                                }
                                else  if ([NSLocalizedString([ChosenMedicen valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Daily",nil)]) {

                                    [label setText:[ChosenMedicen valueForKey:@"Frequency_text"]];
                                    [Schdules.Dates setText:@""];
                                }
                                else  if ([NSLocalizedString([ChosenMedicen valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Weekly",nil)]) {

                                    [label setText:[ChosenMedicen valueForKey:@"Frequency_text"]];
                                    [Schdules.Dates setText:[ChosenMedicen valueForKey:@"Dates"]];

                                    [Schdules.Dates sizeToFit];

                                }

                            }




                        }

                    }

                }
            }
        }

        [headShot setImage:[UIImage imageNamed:@"Medhot.png"]];

        if ([UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"Image"]]) {

            [headShot setImage:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"Image"]]];
        }



        appDelegate.PerscriptionNotes = [set valueForKey:@"Notes"];
        appDelegate.Notes = [set valueForKey:@"Prescription"];



        [Schdules setfreguency:[ChosenMedicen valueForKey:@"ID_Medicine"]];


    });

}

-(void)setlabelframe:(TekstIDLabel*)sender
{

    [sender setFrame:CGRectMake(12,sender.center.y-(sender.frame.size.height/2), self.view.frame.size.width - 115, 39)];
//    [sender sizeToFit];

    [sender setFrame:CGRectMake(12, sender.center.y-(sender.frame.size.height/2),  sender.frame.size.width, sender.frame.size.height)];
    [sender setCenter:CGPointMake(sender.center.x, sender.centerit+20)];
}


-(NSString*)getnumberOfday:(NSString*)day

{

    NSArray *numbers2 = [day componentsSeparatedByString:@","];



    NSString *StringReturn = day;


    NSArray *names =[NSArray arrayWithObjects:@"Sun",@"Mon",@"Tue",@"Wen",@"",@"Fri",@"Sat", nil];
    NSArray *number =[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7", nil];


    for (NSInteger k=0 ; k<[numbers2 count]; k++) {

        for (NSInteger i=0 ; i<[names count]; i++) {

            if ([[numbers2 objectAtIndex:k] isEqualToString:NSLocalizedString([number objectAtIndex:i],nil)]) {


                StringReturn = [StringReturn stringByReplacingOccurrencesOfString:[numbers2 objectAtIndex:k]  withString:[names objectAtIndex:i]];

            }

        }


    }

    return StringReturn;

}


-(void)setItemsChangeID:(NSMutableDictionary*)set

{


    ChosenMedicen =[[NSMutableDictionary alloc]init];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];



    ChosenMedicen = set;


    chosenID = [appDelegate newUUID];

    [ChosenMedicen setObject:chosenID forKey:@"ID_Medicine"];
    [ChosenMedicen setObject:appDelegate.CurrentID forKey:@"ID"];
    [ChosenMedicen setObject:@" " forKey:@"Frequency"];


    for (UIScrollView *scroll in self.view.subviews) {

        for (UIView *view in scroll.subviews) {



            for (TekstIDLabel *label in view.subviews) {

                if ([label isKindOfClass:[TekstIDLabel class]]) {

                    if (label.title) {


                        if ([label.title isEqualToString:@"Recording"]) {

                            if ([[ChosenMedicen valueForKey:@"Recording"] isEqualToString:@"444"]) {

                                [label setText:@" "];
                            }
                            else
                            {



                                [label setText:[[[GetData getRecording] valueForKey:@"title"]  objectAtIndex:[[[GetData getRecording] valueForKey:@"ID"] indexOfObject:[ChosenMedicen valueForKey:@"Recording"]]]];
                            }

                        }

                        else
                        {

                            [label setText:[set valueForKey:label.title]];

                        }

                        if ([label.title isEqualToString:@"Type"]) {


                            appDelegate.TypeMed =[set valueForKey:label.title];
                        }


                    }


                }


            }


        }

    }


    [headShot setImage:[UIImage imageWithContentsOfFile:[ChosenMedicen valueForKey:@"Image"]]];

    appDelegate.PerscriptionNotes = [set valueForKey:@"Notes"];
    appDelegate.Notes = [set valueForKey:@"Prescription"];



}




- (void) viewWillAppear:(BOOL)animated {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    appDelegate.FromScreen=YES;
}


-(void)setNew:(NSString*)set;

{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    ChosenMedicen =[[NSMutableDictionary alloc]init];



    ChosenMedicen = [GetData getMedicinewithIDCopy:@"70f855d5-72cc-4092-acac-f31cf812a805"];

    [ChosenMedicen setObject:set forKey:@"Profile"];

    ////////////////////////////NSLog(@"%@", appDelegate.CurrentID);


    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, @"Prescription.png"];


    chosenID = [appDelegate newUUID];

    [ChosenMedicen setObject:chosenID forKey:@"ID_Medicine"];
    [ChosenMedicen setObject:appDelegate.CurrentID forKey:@"ID"];
    [ChosenMedicen setObject:jpegFilePath forKey:@"Image"];
    [ChosenMedicen setObject:jpegFilePath forKey:@"PerscriptionImage"];


    //NSLocalizedString(@"


    //////////////////////////////////////////////NSLog(@"%@", ChosenMedicen);



    [self setItems:ChosenMedicen];

}

-(void)setNew2:(NSString*)set;

{


    ChosenMedicen =[[NSMutableDictionary alloc]init];



    ChosenMedicen = [GetData getMedicinewithIDCopy:@"70f855d5-72cc-4092-acac-f31cf812a805"];

    [ChosenMedicen setObject:set forKey:@"Profile"];



    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, @"Prescription.png"];


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    chosenID = [appDelegate newUUID];

    [ChosenMedicen setObject:chosenID forKey:@"ID_Medicine"];
    [ChosenMedicen setObject:appDelegate.CurrentID forKey:@"ID"];
    [ChosenMedicen setObject:jpegFilePath forKey:@"Image"];
    [ChosenMedicen setObject:jpegFilePath forKey:@"PerscriptionImage"];



    //////////////////////////////////////////////NSLog(@"%@", ChosenMedicen);



    [self setItems:ChosenMedicen];



}



-(void)viewDidDisappear:(BOOL)animated

{


}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *) picker
{







    [picker dismissViewControllerAnimated:YES  completion:nil];



}


- (void) mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{





    [self dismissViewControllerAnimated:YES completion:NULL];
}




- (IBAction)showMediaPicker
{
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];

    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    [self presentViewController:mediaPicker animated:YES completion:NULL];
}


-(void) changeColorFridge
{
    LeveyPopColorView *pinview01 = (LeveyPopColorView *)[self.view  viewWithTag:3459148];
    if (pinview01) {

        [pinview01 removeFromSuperview];


    }
    else
    {


        color = [[LeveyPopColorView alloc] initWithTitle:NSLocalizedString(@"Share Photo to...",nil) options:NULL setIt:2];
        // lplv.isModal = NO;
        [color setTag:3459148];
        [color showInView:self.view animated:YES];
        [color getParent:self get:3];


    }






}

-(void)plaese:(NSString*)set

{

/* //27-06-2017 changes
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:[NSString stringWithFormat:NSLocalizedString(@"Fill in a %@ first to continue",nil), set]
                                                   delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
    [alert show];
*/
    
    UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"",nil) message:[NSString stringWithFormat:NSLocalizedString(@"Fill in a %@ first to continue",nil), set] preferredStyle:UIAlertControllerStyleAlert];
    [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
        
    });

    
}

#pragma mark - i  want

-(void) save:(UIButton*)sender
{
    
//    [self googleAnalyticsGroupping];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    //////////////////////////////////////////////////////////////////////////NSLog(@"%@", ChosenMedicen);

    chosenID = [ChosenMedicen valueForKey:@"ID_Medicine"];
    NSString *ID = [ChosenMedicen valueForKey:@"ID"];
    [ChosenMedicen setObject:@"YES" forKey:@"active"];


    ////////////////NSLog(@"%@", ChosenMedicen);


    appDelegate.Selected = [ChosenMedicen valueForKey:@"ID"];


    appDelegate.CurrentID = [ChosenMedicen valueForKey:@"ID"];

    //////////////////////////////NSLog(@"%@", appDelegate.CurrentID);




    if ([[ChosenMedicen valueForKey:@"ColorGreen"] isEqualToString:@"YES"]) {



        if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]||[NSLocalizedString([ChosenMedicen valueForKey:@"Frequency_text"],nil)isEqualToString:@" "]||[[ChosenMedicen valueForKey:@"Quantity"] isEqualToString:@" "]||[[ChosenMedicen valueForKey:@"Day_number"] isEqualToString:@" "]) {

/* //27-06-2017 changes
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"Please ensure you have filled the Name, Quantity Per Dose, the Frequency and Quantity Remaining, prior to saving.",nil)
                                                           delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
            [alert show];
*/
            UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"Please ensure you have filled the Name, Quantity Per Dose, the Frequency and Quantity Remaining, prior to saving.",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                
            });



        }

        else
        {


            appDelegate.Editing=NO;


            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm"];



            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {

                /* //27-06-2017 changes
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"Atleast fill in a name",nil)
                                                               delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                [alert show];
*/

                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"Atleast fill in a name",nil) preferredStyle:UIAlertControllerStyleAlert];
                [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                    
                });
                

                
                
            }
            else
            {




                NSArray *names =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit",@"Location",@"active",nil];

                [SaveCore insertMedecineSQL:ChosenMedicen setnames:names];



                if ([[ChosenMedicen valueForKey:@"Frequency"] isEqualToString:@" "]) {

                    //////////////////////////////////////////////////////////NSLog(@"3");
                    //[Notification RemoveNotification:chosenID];

                    chosenID = [ChosenMedicen valueForKey:@"ID_Medicine"];

                    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                    set  = ChosenMedicen;



                    [set setObject:chosenID forKey:@"ID_Medicine"];


                    for (NSInteger i=0; i<[names count]; i++) {

                        if ([[names objectAtIndex:i] isEqualToString:@"Image"])

                        {

                            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];
                            [set setObject:jpegFilePath  forKey:[names objectAtIndex:i]];

                        }
                        else  if ([[names objectAtIndex:i] isEqualToString:@"Remaining"])

                        {
                            [set setObject:[ChosenMedicen valueForKey:@"Filling"]  forKey:@"Filling"];
                        }
                        else if ([[names objectAtIndex:i] isEqualToString:@"PerscriptionImage"])

                        {
                            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Perscription_%@.png", chosenID]];
                            [set setObject:jpegFilePath  forKey:[names objectAtIndex:i]];
                        }
                        else

                        {

                            [set setObject:[ChosenMedicen valueForKey:[names objectAtIndex:i]] forKey:[names objectAtIndex:i]];

                        }
                    }


                    ////NSLog(@"a");
                    [Notification setDate:set];


                    for (NSInteger i=0 ; i<[[[self navigationController] viewControllers] count]; i++) {


                        if ([[[[self navigationController] viewControllers] objectAtIndex:i] isKindOfClass:[ShowMedecineController class]]) {

                            ShowMedecineController *controller2 =[[[self navigationController] viewControllers] objectAtIndex:i];


                            [controller2 setItemsBack:set];

                        }
                    }

                    [[self navigationController] popViewControllerAnimated:YES];




                }
                else
                {

                    //////////////////////////////////////////////////////////NSLog(@"5");
                    //[Notification RemoveNotification:chosenID];


                    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                    set  = ChosenMedicen;
                    [set setObject:chosenID forKey:@"ID_Medicine"];
                    [set setObject:ID forKey:@"ID"];


                    for (NSInteger i=0; i<[names count]; i++) {

                        if ([[names objectAtIndex:i] isEqualToString:@"Image"])

                        {

                            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];
                            [set setObject:jpegFilePath  forKey:[names objectAtIndex:i]];

                        }
                        else  if ([[names objectAtIndex:i] isEqualToString:@"Remaining"])

                        {
                            [set setObject:[ChosenMedicen valueForKey:@"Filling"]  forKey:@"Filling"];
                        }

                        else if ([[names objectAtIndex:i] isEqualToString:@"PerscriptionImage"])

                        {
                            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Perscription_%@.png", chosenID]];
                            [set setObject:jpegFilePath  forKey:[names objectAtIndex:i]];
                        }
                        else

                        {

                            [set setObject:[ChosenMedicen valueForKey:[names objectAtIndex:i]] forKey:[names objectAtIndex:i]];

                        }
                    }

                    ////NSLog(@"b");
                    [Notification setDate:set];

                    for (NSInteger i=0 ; i<[[[self navigationController] viewControllers] count]; i++) {


                        if ([[[[self navigationController] viewControllers] objectAtIndex:i] isKindOfClass:[ShowMedecineController class]]) {

                            ShowMedecineController *controller2 =[[[self navigationController] viewControllers] objectAtIndex:i];


                            [controller2 setItemsBack:set];

                        }
                    }

                    double delayInSeconds = 0.3;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        [[self navigationController] popViewControllerAnimated:YES];
                    });










                }





                appDelegate.Editing=NO;

            }

            [appDelegate.Profile removeAllObjects];

            NSArray *names =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit",@"Location",@"active",nil];
            [SaveCore insertMedecineSQL:ChosenMedicen setnames:names];


            for (NSInteger i=0 ; i<[names count]; i++) {


                [[NSUserDefaults standardUserDefaults] setObject:@" " forKey:[names objectAtIndex:i]];

            }


            //[Notification CheckNotification];

        }



    }

    else
    {


        if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]||[NSLocalizedString([ChosenMedicen valueForKey:@"Frequency_text"],nil) isEqualToString:@" "]||[[ChosenMedicen valueForKey:@"Day_number"] isEqualToString:@" "]||[[ChosenMedicen valueForKey:@"Quantity"] isEqualToString:@" "]) {

/*
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"Please ensure you have filled the Name, Quantity Per Dose, the Frequency and Quantity Remaining, prior to saving.",nil)
                                                           delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
            [alert show];
*/

            UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"Please ensure you have filled the Name, Quantity Per Dose, the Frequency and Quantity Remaining, prior to saving.",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                
            });

            
        }

        else
        {


            appDelegate.Editing=NO;


            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm"];



            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {

                /*
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"Atleast fill in a name",nil)
                                                               delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
                [alert show];
                */

                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"Atleast fill in a name",nil) preferredStyle:UIAlertControllerStyleAlert];
                [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                    
                });
                

                
            }
            else
            {





                NSArray *names =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit",@"Location",@"active",nil];

                [SaveCore insertMedecineSQL:ChosenMedicen setnames:names];





                if ([[ChosenMedicen valueForKey:@"Frequency"] isEqualToString:@" "]) {

                    //////////////////////////////////////////////////////////NSLog(@"1");
                    //[Notification RemoveNotification:chosenID];


                    chosenID = [ChosenMedicen valueForKey:@"ID_Medicine"];
                    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                    set  = ChosenMedicen;
                    [set setObject:[ChosenMedicen valueForKey:@"ID_Medicine"] forKey:@"ID_Medicine"];
                    [set setObject:ID forKey:@"ID"];


                    for (NSInteger i=0; i<[names count]; i++) {

                        if ([[names objectAtIndex:i] isEqualToString:@"Image"])

                        {

                            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];
                            [set setObject:jpegFilePath  forKey:[names objectAtIndex:i]];

                        }
                        else  if ([[names objectAtIndex:i] isEqualToString:@"Remaining"])

                        {
                            [set setObject:[ChosenMedicen valueForKey:@"Filling"]  forKey:@"Filling"];
                        }
                        else if ([[names objectAtIndex:i] isEqualToString:@"PerscriptionImage"])

                        {
                            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Perscription_%@.png", chosenID]];
                            [set setObject:jpegFilePath  forKey:[names objectAtIndex:i]];
                        }
                        else

                        {

                            [set setObject:[ChosenMedicen valueForKey:[names objectAtIndex:i]] forKey:[names objectAtIndex:i]];

                        }
                    }

                    ////NSLog(@"c");
                    [Notification setDate:set];


                    for (NSInteger i=0 ; i<[[[self navigationController] viewControllers] count]; i++) {


                        if ([[[[self navigationController] viewControllers] objectAtIndex:i] isKindOfClass:[ShowMedecineController class]]) {

                            ShowMedecineController *controller2 =[[[self navigationController] viewControllers] objectAtIndex:i];


                            [controller2 setItemsBack:set];

                        }
                    }

                    double delayInSeconds = 0.3;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                        [[self navigationController] popViewControllerAnimated:YES];
                    });



                }
                else
                {
                    //////////////////////////////////////////////////////////NSLog(@"4");
                    // [Notification RemoveNotification:chosenID];


                    NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
                    set  = ChosenMedicen;
                    [set setObject:chosenID forKey:@"ID_Medicine"];


                    for (NSInteger i=0; i<[names count]; i++) {

                        if ([[names objectAtIndex:i] isEqualToString:@"Image"])

                        {

                            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Image_%@.png", chosenID]];
                            [set setObject:jpegFilePath  forKey:[names objectAtIndex:i]];

                        }
                        else  if ([[names objectAtIndex:i] isEqualToString:@"Remaining"])

                        {
                            [set setObject:[ChosenMedicen valueForKey:@"Filling"]  forKey:@"Filling"];
                        }
                        else if ([[names objectAtIndex:i] isEqualToString:@"PerscriptionImage"])

                        {
                            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *jpegFilePath = [NSString stringWithFormat:@"%@/Caches/%@",docDir, [NSString stringWithFormat:@"Perscription_%@.png", chosenID]];
                            [set setObject:jpegFilePath  forKey:[names objectAtIndex:i]];
                        }
                        else

                        {

                            [set setObject:[ChosenMedicen valueForKey:[names objectAtIndex:i]] forKey:[names objectAtIndex:i]];

                        }
                    }

                    ////NSLog(@"d");
                    [Notification setDate:set];

                    for (NSInteger i=0 ; i<[[[self navigationController] viewControllers] count]; i++) {


                        if ([[[[self navigationController] viewControllers] objectAtIndex:i] isKindOfClass:[ShowMedecineController class]]) {

                            ShowMedecineController *controller2 =[[[self navigationController] viewControllers] objectAtIndex:i];


                            [controller2 setItemsBack:set];

                        }
                    }


                    double delayInSeconds = 0.3;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                        [[self navigationController] popViewControllerAnimated:YES];
                    });









                }





                appDelegate.Editing=NO;

            }

            [appDelegate.Profile removeAllObjects];

            NSArray *names =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit",@"Location",@"active",nil];

            [SaveCore insertMedecineSQL:ChosenMedicen setnames:names];


            for (NSInteger i=0 ; i<[names count]; i++) {


                [[NSUserDefaults standardUserDefaults] setObject:@" " forKey:[names objectAtIndex:i]];

            }


            //[Notification CheckNotification];

        }


    }




    [appDelegate insertMedicineNew:[[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"] with:ChosenMedicen];



}


-(void)getParant:(UIViewController*)parant

{
    parantIt = (StartViewController*)parant;
}


-(void) CreateAccount
{

}
-(void)Action:(ActionArrow*)sender
{

    NSLog(@"%@", sender.titleit);

    if ([sender.titleit isEqualToString:@"Color"]) {

        [self changeColorFridge];
    }
    if ([sender.titleit isEqualToString:@"Alert"]) {


        [self showMediaPicker];

    }
    if ([sender.titleit isEqualToString:@"Photo"]) {

        //[self getCameraPicture:sender];

        [self makeAchoice];
    }
    else
    {

        if ([sender.titleit isEqualToString:@"Name"]) {



            NameItemViewController *controller = [[NameItemViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {

                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {


                            if ([sender.titleit isEqualToString:label.title]) {
                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParant:self];
            [controller.navigationItem setTitle:NSLocalizedString(@"Name",nil)];

            NSString *deviceType = [UIDevice currentDevice].systemVersion;
            NSRange range = NSMakeRange (0, 1);
            //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {

                UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                label.backgroundColor = [UIColor clearColor];
                label.font = [UIFont boldSystemFontOfSize:20.0];
                label.textAlignment = NSTextAlignmentCenter;
                // ^-Use NSTextAlignmentCenter for older SDKs.
                label.textColor = [UIColor whiteColor]; // change this color
                controller.navigationItem.titleView = label;
                label.text = controller.navigationItem.title;
                [label sizeToFit];

            }

            [controller hideAndseek:sender.titleit];



        }

        //NSLocalizedString(@"Duration:", nil)



        else if ([sender.titleit isEqualToString:@"Duration:"]) {


            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {

                [self plaese:NSLocalizedString(@"name",nil)];


            }

            else
            {
                DurationViewController *controller = [[DurationViewController alloc]init];
                [self.navigationController pushViewController:controller animated:YES];


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {


                                if ([sender.titleit isEqualToString:label.title]) {
                                    [controller whatLabel:label];

                                }

                            }}}}

                [controller getParant:self];
                [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];

                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:20.0];
                    label.textAlignment = NSTextAlignmentCenter;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = controller.navigationItem.title;
                    [label sizeToFit];


                }

                [controller hideAndseek:sender.titleit];


            }


        }
        else if ([sender.titleit isEqualToString:@"Message"]) {

            //NSLocalizedString(@"Message:", nil)
            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {

                [self plaese:NSLocalizedString(@"name",nil)];


            }

            else
            {
                MessageViewController *controller = [[MessageViewController alloc]init];
                [self.navigationController pushViewController:controller animated:YES];


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {


                                if ([sender.titleit isEqualToString:label.title]) {
                                    [controller whatLabel:label];

                                }

                            }}}}

                [controller getParant:self];
                [controller.navigationItem setTitle:NSLocalizedString(@"Alert Message Text",nil)];

                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);


                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {



                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:15];
                    label.textAlignment = NSTextAlignmentCenter;
                    label.numberOfLines =2;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = controller.navigationItem.title;
                    [label sizeToFit];



                    ////NSLog(@"%@",[deviceType substringWithRange:range]);

                }


            }


        }
        else if ([sender.titleit isEqualToString:@"Instructions"]) {


            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {

                [self plaese:NSLocalizedString(@"name",nil)];


            }

            else
            {


                InstructionsViewController *controller = [[InstructionsViewController alloc]init];
                [self.navigationController pushViewController:controller animated:YES];


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {


                                if ([sender.titleit isEqualToString:label.title]) {
                                    [controller whatLabel:label];

                                }

                            }}}}

                [controller getParant:self];
                [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];


                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:20.0];
                    label.textAlignment = NSTextAlignmentCenter;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = controller.navigationItem.title;
                    [label sizeToFit];

                }

            }


        }


        //Minimum

        else if ([sender.titleit isEqualToString:@"Refilling"]) {

            //Refilling
            
            [self addActionForGoogleAnalytics:@"Refilling info"];

            if ([[ChosenMedicen valueForKey:@"Type"] isEqualToString:@" "]) {

                [self plaese:@"Medicine Type"];


            }

            else
            {


                MinimumViewController *controller = [[MinimumViewController alloc]init];
                [self presentViewController:controller animated:YES completion:Nil];
                [controller getParant:self];


                ////////////////////////////////NSLog(@"MinimumViewController");


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {


                                if ([sender.titleit isEqualToString:label.title]) {
                                    [controller whatValue:[ChosenMedicen valueForKey:@"Refilling"]];

                                }

                            }}}}

                //[controller getParant:self];
                [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];

                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:20.0];
                    label.textAlignment = NSTextAlignmentCenter;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = controller.navigationItem.title;
                    [label sizeToFit];


                }

                [controller hideAndseek:sender.titleit];


            }



        }


        else if ([sender.titleit isEqualToString:@"Filling"]) {

            //Filling
            
            [self addActionForGoogleAnalytics:@"Refilling info"];
            
            if ([[ChosenMedicen valueForKey:@"Type"] isEqualToString:@" "]) {

                [self plaese:@"Medicine Type"];


            }

            else
            {


                FillingViewController *controller = [[FillingViewController alloc]init];
                [self presentViewController:controller animated:YES completion:Nil];
                [controller getParant:self];

                ////////////////////////////////NSLog(@"FillingViewController");


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {


                                if ([sender.titleit isEqualToString:label.title]) {
                                    [controller whatValue:[ChosenMedicen valueForKey:@"Filling"]];

                                }

                            }}}}

                //[controller getParant:self];
                [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];

                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:20.0];
                    label.textAlignment = NSTextAlignmentCenter;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = controller.navigationItem.title;
                    [label sizeToFit];


                }

                [controller hideAndseek:sender.titleit];


            }



        }
        else if ([sender.titleit isEqualToString:@"Quantity"]) {


            if ([[ChosenMedicen valueForKey:@"Type"] isEqualToString:@" "]) {


                [self plaese:@"Medicine Type"];

            }

            else
            {

                DoseViewController *controller = [[DoseViewController alloc]init];
                [self.navigationController pushViewController:controller animated:YES];
                [controller getParant:self];


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {


                                if ([sender.titleit isEqualToString:label.title]) {


                                    [controller whatValue:[ChosenMedicen valueForKey:@"Quantity"]];


                                }

                            }}}}

                //[controller getParant:self];
                [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];

                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:20.0];
                    label.textAlignment = NSTextAlignmentCenter;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = NSLocalizedString(controller.navigationItem.title,nil);
                    [label sizeToFit];


                }

                [controller hideAndseek:sender.titleit];
            }



        }

        else if ([sender.titleit isEqualToString:@"Remaining"]) {

            //Remaining
            
            [self addActionForGoogleAnalytics:@"Refilling info"];
            
            if ([[ChosenMedicen valueForKey:@"Type"] isEqualToString:@" "]) {

                [self plaese:@"Medicine Type"];


            }

            else
            {

                RemainingViewController *controller = [[RemainingViewController alloc]init];
                [self presentViewController:controller animated:YES completion:Nil];
                [controller getParant:self];


                ////////////////////////////////NSLog(@"RemainingViewController");

                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {


                                if ([sender.titleit isEqualToString:label.title]) {
                                    [controller whatValue:[ChosenMedicen valueForKey:@"Remaining"]];

                                }

                            }}}}

                //[controller getParant:self];
                [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];

                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:20.0];
                    label.textAlignment = NSTextAlignmentCenter;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = controller.navigationItem.title;
                    [label sizeToFit];


                }

                [controller hideAndseek:sender.titleit];

            }




        }
        else if ([sender.titleit isEqualToString:@"Strength"]) {

            if ([[ChosenMedicen valueForKey:@"Type"] isEqualToString:@" "]) {

                [self plaese:@"Medicine Type"];


            }
            else
            {

                StrengthViewControllerScroll *controller = [[StrengthViewControllerScroll alloc]init];



                [controller hideAndseek:sender.titleit];

                [self.navigationController pushViewController:controller animated:YES];






            }

        }
        else if ([sender.titleit isEqualToString:@"Notes"]) {

            //Notes
            
            [self addActionForGoogleAnalytics:@"Prescription Notes"];
            
            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {

                [self plaese:NSLocalizedString(@"name",nil)];


            }

            else
            {
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

                PerscriptionViewController *controller = [[PerscriptionViewController alloc]init];

                appDelegate.CurrentID =chosenID;
                [self.navigationController pushViewController:controller animated:YES];

                controller.title =@"Prescription Notes";


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {

                                if ([sender.titleit isEqualToString:label.title]) {


                                    [controller whatLabel:label];

                                }

                            }}}}

                [controller getParant:self];

            }


        }

        else if ([sender.titleit isEqualToString:@"Type"]) {


            TypeViewController *controller = [[TypeViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];


            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleit isEqualToString:label.title]) {


                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParant:self];
            [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];

            NSString *deviceType = [UIDevice currentDevice].systemVersion;
            NSRange range = NSMakeRange (0, 1);
            //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                label.backgroundColor = [UIColor clearColor];
                label.font = [UIFont boldSystemFontOfSize:20.0];
                label.textAlignment = NSTextAlignmentCenter;
                // ^-Use NSTextAlignmentCenter for older SDKs.
                label.textColor = [UIColor whiteColor]; // change this color
                controller.navigationItem.titleView = label;
                label.text = controller.navigationItem.title;
                [label sizeToFit];


            }
            [controller hideAndseek:sender.titleit];
            [controller setChoice:sender.titleit];

        }


        else if ([sender.titleit isEqualToString:@"Profile"]) {


            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {

                [self plaese:NSLocalizedString(@"name",nil)];


            }

            else
            {
                ProfileViewController *controller = [[ProfileViewController alloc]init];
                [self.navigationController pushViewController:controller animated:YES];


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {

                                if ([sender.titleit isEqualToString:label.title]) {


                                    [controller whatLabel:label];

                                }

                            }}}}

                [controller getParant:self];
                [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];

                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:20.0];
                    label.textAlignment = NSTextAlignmentCenter;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = controller.navigationItem.title;
                    [label sizeToFit];

                }
                [controller hideAndseek:sender.titleit];
                [controller setChoice:sender.titleit];

            }
        }

        else if ([sender.titleit isEqualToString:@"Doctor"]) {

            //Doctor
            
            [self addActionForGoogleAnalytics:@"Doctor Info"];
            
            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {


                [self plaese:@"name"];


            }

            else
            {

                DocterViewController *controller = [[DocterViewController alloc]init];
                [self.navigationController pushViewController:controller animated:YES];


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {

                                if ([sender.titleit isEqualToString:label.title]) {


                                    [controller whatLabel:label];

                                }

                            }}}}

                [controller getParant:self];
                [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];

                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:20.0];
                    label.textAlignment = NSTextAlignmentCenter;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = controller.navigationItem.title;
                    [label sizeToFit];

                }
                [controller hideAndseek:sender.titleit];
                [controller setChoice:sender.titleit];

            }
        }
        else if ([sender.titleit isEqualToString:@"Pharmacy"]) {

            //Pharmacy
            
            [self addActionForGoogleAnalytics:@"Pharmacy Info"];
            
            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {

                [self plaese:NSLocalizedString(@"name",nil)];


            }

            else
            {

                PharmacyViewController *controller = [[PharmacyViewController alloc]init];
                [self.navigationController pushViewController:controller animated:YES];
                controller.title=@"Pharmacy";


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {



                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {

                                if ([sender.titleit isEqualToString:label.title]) {


                                    [controller whatLabel:label];

                                }

                            }}}}

                [controller getParant:self];
                [controller.navigationItem setTitle:NSLocalizedString(@"Pharmacy",nil)];

                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:20.0];
                    label.textAlignment = NSTextAlignmentCenter;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = controller.navigationItem.title;
                    [label sizeToFit];

                }
                [controller hideAndseek:sender.titleit];
                [controller setChoice:sender.titleit];



            }

        }
        else if ([sender.titleit isEqualToString:@"Frequency"]) {

            if ([[ChosenMedicen valueForKey:@"Name"] isEqualToString:@" "]) {

                [self plaese:NSLocalizedString(@"name",nil)];


            }

            else
            {

                FrequencyViewControllerNew *controller = [[FrequencyViewControllerNew alloc]init];
                [self.navigationController pushViewController:controller animated:YES];


                for (UIScrollView *scroll in self.view.subviews) {

                    for (UIView *view in scroll.subviews) {

                        for (TekstIDLabel *label in view.subviews) {

                            if ([label isKindOfClass:[TekstIDLabel class]]) {

                                if ([@"Frequency_text" isEqualToString:label.title]) {


                                    [controller whatLabel:label];

                                }
                                if ([@"Frequency_number" isEqualToString:label.title]) {



                                    [controller whatLabel:label];

                                }


                            }}}}


                [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];

                NSString *deviceType = [UIDevice currentDevice].systemVersion;
                NSRange range = NSMakeRange (0, 1);
                //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont boldSystemFontOfSize:20.0];
                    label.textAlignment = NSTextAlignmentCenter;
                    // ^-Use NSTextAlignmentCenter for older SDKs.
                    label.textColor = [UIColor whiteColor]; // change this color
                    controller.navigationItem.titleView = label;
                    label.text = controller.navigationItem.title;
                    [label sizeToFit];

                }

            }
        }

        else if ([sender.titleit isEqualToString:@"Recording"]) {


            RecordingViewController *controller = [[RecordingViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];

            for (UIScrollView *scroll in self.view.subviews) {

                for (UIView *view in scroll.subviews) {



                    for (TekstIDLabel *label in view.subviews) {

                        if ([label isKindOfClass:[TekstIDLabel class]]) {

                            if ([sender.titleit isEqualToString:label.title]) {


                                [controller whatLabel:label];

                            }

                        }}}}

            [controller getParant:self];
            [controller.navigationItem setTitle:NSLocalizedString(sender.titleit,nil)];

            NSString *deviceType = [UIDevice currentDevice].systemVersion;
            NSRange range = NSMakeRange (0, 1);
            //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);

            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {


                UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                label.backgroundColor = [UIColor clearColor];
                label.font = [UIFont boldSystemFontOfSize:20.0];
                label.textAlignment = NSTextAlignmentCenter;
                // ^-Use NSTextAlignmentCenter for older SDKs.
                label.textColor = [UIColor whiteColor]; // change this color
                controller.navigationItem.titleView = label;
                label.text = controller.navigationItem.title;
                [label sizeToFit];

            }
            [controller hideAndseek:sender.titleit];
            [controller setChoice:sender.titleit];



        }

    }





}


-(void)makeAchoice
{
    
    NSString *strLibrary = @"";
    
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"English"])
    {
        strLibrary = NSLocalizedString(@"Library", nil);
    }
    else
    {
        strLibrary = NSLocalizedString(@"Librar", nil);
    }

    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"",nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:NSLocalizedString(@"Cancel", nil)
                                             /* otherButtonTitles:NSLocalizedString(@"Library", nil)*/
                            otherButtonTitles:strLibrary, NSLocalizedString(@"Camera", nil),nil];



    sheet.delegate = self;
    sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;


    [sheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{


    if (buttonIndex ==1) {

        [self selectExitingPicture];
    }

    if (buttonIndex==2) {

        [self getCameraPicture:NULL];

    }


}

-(void)selectExitingPicture
{


    photchoice =@"selectExitingPicture";

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate.toolbarDown setAlpha:0];



    if([UIImagePickerController isSourceTypeAvailable:
        UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *picker= [[UIImagePickerController alloc]init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES  completion:NULL];

    }


}


- (void)setSelectedColor:(UIColor*)colorit{



    [ColorField setBackgroundColor:colorit];


}

#pragma mark - i  want

-(void)setitemsDictionary:(NSString*)set name:(NSString*)name
{

    //////////////////////NSLog(@"name name %@", set);

    [[NSUserDefaults standardUserDefaults] setObject:set forKey:name];

    if (set) {
        [ChosenMedicen setObject:set forKey:name];
    }

    UISwitch * swActive2 = (UISwitch *)[self.view viewWithTag:100];
    if ([name isEqualToString:@"Frequency"]) {

        [swActive2 setOn:TRUE animated:FALSE];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Schedule"];
        [ChosenMedicen setObject:@"YES" forKey:@"ColorGreen"];

    }


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    appDelegate.TypeMed = [ChosenMedicen valueForKey:@"Type"];


    NSArray *typit = [[GetData getDrugTypeStrength:appDelegate.TypeMed] objectAtIndex:0];


    ////////////////NSLog(@"%@", typit);

    if ([[typit valueForKey:@"Dose_name"] isEqualToString:@"-"]) {

        TekstIDLabel *Label = (TekstIDLabel *)[Dose viewWithTag:(4444)];
        TekstIDLabel *Label1 = (TekstIDLabel *)[Quantity viewWithTag:(4445)];
        TekstIDLabel *Label2 = (TekstIDLabel *)[Quantity viewWithTag:(4446)];
        TekstIDLabel *Label3 = (TekstIDLabel *)[Quantity viewWithTag:(4447)];


        [Label setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Quantity Not relevant:",nil)]];
        [Label1 setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Applications Remaining:",nil)]];
        [Label2 setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Applications Per Refill:",nil)]];
        [Label3 setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Minimum Applications\nFor Refilling:",nil)]];
        
        //done
        
        [self setlabelframe:Label];
        [self setlabelframe:Label1];
        [self setlabelframe:Label2];
        [self setlabelframe:Label3];



    }
    else
    {
        TekstIDLabel *Label = (TekstIDLabel *)[Dose viewWithTag:(4444)];
        TekstIDLabel *Label1 = (TekstIDLabel *)[Quantity viewWithTag:(4445)];
        TekstIDLabel *Label2 = (TekstIDLabel *)[Quantity viewWithTag:(4446)];
        TekstIDLabel *Label3 = (TekstIDLabel *)[Quantity viewWithTag:(4447)];



        if ([[ChosenMedicen valueForKey:@"Quantity"] integerValue]>1)
        {


            NSString *uitzondering =[[typit valueForKey:@"Dose_name"] stringByReplacingOccurrencesOfString:[typit valueForKey:@"Drug_less"] withString:[typit valueForKey:@"Drug_types"]];


            [Label setText:[NSString stringWithFormat:@"%@%@", [uitzondering stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"],NSLocalizedString(@":",nil)]];

            //13-11-2017
            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
            {
                if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
                {
                    Label.text = [Label.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mengde"];
                    Label.text = [Label.text stringByReplacingOccurrencesOfString:@"Spray" withString:@"Mengde"];

                }
            }
            
        }
        else
        {
            [Label setText:[NSString stringWithFormat:@"%@%@", [[typit valueForKey:@"Dose_name"]stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"], NSLocalizedString(@":",nil)]];

            //13-11-2017
            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
            {
                if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
                {
                    [Label setText:[NSString stringWithFormat:@"%@%@", [[typit valueForKey:@"Dose_name"]stringByReplacingOccurrencesOfString:@"Gouttess" withString:@"Gouttes"], NSLocalizedString(@":",nil)]];
                    
                    Label.text = [Label.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mengde"];
                    
                    Label.text = [Label.text stringByReplacingOccurrencesOfString:@"Spray" withString:@"Mengde"];

                }
            }
        }


        if ([[ChosenMedicen valueForKey:@"Remaining"] integerValue]>1)
        {
            //15-11
            [Label1 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Remaining_name"], NSLocalizedString(@":",nil)]];

            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
            {
                if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
                {
                    Label1.text = [Label1.text stringByReplacingOccurrencesOfString:@"tabletter" withString:@"mengde"];
                }
            }

        }

        else
        {
            //15-11
            
            [Label1 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Remaining_name"], NSLocalizedString(@":",nil)]];

            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
            {
                if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
                {
                    Label1.text = [Label1.text stringByReplacingOccurrencesOfString:@"tabletter" withString:@"mengde"];
                    Label1.text = [Label1.text stringByReplacingOccurrencesOfString:@"spray" withString:@"mengde"];

                }
            }
        }
        
        if ([[ChosenMedicen valueForKey:@"Filling"] integerValue]>1)
        {
            //15-11

            [RenewButton setAlpha:1];
            [Label2 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Refill_name"], NSLocalizedString(@":",nil)]];

            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
            {
                if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
                {
                    Label2.text = [Label2.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mengde"];
                    Label2.text = [Label2.text stringByReplacingOccurrencesOfString:@"Sprayer" withString:@"Mengde"];

                }
            }
        }
        else
        {
            //15-11
            [RenewButton setAlpha:0];
            [Label2 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Refill_name"], NSLocalizedString(@":",nil)]];

            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
            {
                if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
                {
                    Label2.text = [Label2.text stringByReplacingOccurrencesOfString:@"Tabletter" withString:@"Mengde"];
                    Label2.text = [Label2.text stringByReplacingOccurrencesOfString:@"Sprayer" withString:@"Mengde"];

                }
            }
        }
        
        if ([[ChosenMedicen valueForKey:@"Refilling"] integerValue]>1)
        {
            //15-11

            [Label3 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Minimume_name"], NSLocalizedString(@":",nil)]];

            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
            {
                if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
                {
                    Label3.text = [Label3.text stringByReplacingOccurrencesOfString:@"tabletter" withString:@"mengde"];
                    Label3.text = [Label3.text stringByReplacingOccurrencesOfString:@"spray" withString:@"mengde"];

                }
            }
        }

        else
        {

            //15-11
            [Label3 setText:[NSString stringWithFormat:@"%@%@", [typit valueForKey:@"Minimume_name"], NSLocalizedString(@":",nil)]];

            if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"])
            {
                if ([[SHARED_APPDELEGATE strMedtype]isEqualToString:@"Dråpe"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Væske"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Spray"])
                {
                    Label3.text = [Label3.text stringByReplacingOccurrencesOfString:@"tabletter" withString:@"mengde"];
                    Label3.text = [Label3.text stringByReplacingOccurrencesOfString:@"spray" withString:@"mengde"];

                }
            }
        }

        [self setlabelframe:Label];
        [self setlabelframe:Label1];
        [self setlabelframe:Label2];
        [self setlabelframe:Label3];



    }



    if ([[NSString stringWithFormat:@"%@%@", [ChosenMedicen valueForKey:@"Remaining"],[ChosenMedicen valueForKey:@"Filling"]] isEqualToString:@"  "]) {




    }
    else
    {
        if ([[ChosenMedicen valueForKey:@"Refilling"] intValue]>=1) {

            UISwitch * swActive2 = (UISwitch *)[self.view viewWithTag:103];
            [ChosenMedicen setObject:@"YES" forKey:@"ColorGreen"];
            [swActive2 setOn:TRUE animated:FALSE];

        }
        else
        {
            UISwitch * swActive2 = (UISwitch *)[self.view viewWithTag:103];
            [ChosenMedicen setObject:@"NO" forKey:@"ColorGreen"];
            [swActive2 setOn:FALSE animated:FALSE];
        }

    }

    for (UIScrollView *scroll in self.view.subviews) {

        for (UIView *view in scroll.subviews) {



            for (UIButton *button in view.subviews) {


                if ([button isKindOfClass:[UIButton class]]) {

                    [button setAlpha:1];


                    if ([[typit valueForKey:@"Dose_name"] isEqualToString:@"-"]) {

                        if ([button.titleLabel.text isEqualToString:@"Quantity"]) {
                            ////////////////////////////////////////////////////////////////////////////NSLog(@"button %@", button.titleLabel.text);
                            [button setAlpha:0];
                        }
                    }
                    else
                    {

                        if ([button.titleLabel.text isEqualToString:@"Quantity"]) {
                            ////////////////////////////////////////////////////////////////////////////NSLog(@"button %@", button.titleLabel.text);
                            [button setAlpha:1];
                        }
                    }

                }
            }


            for (TekstIDLabel *label in view.subviews) {

                if ([label isKindOfClass:[TekstIDLabel class]]) {

                    if (label.title) {

                        if ([label.title isEqualToString:@"Instructions"]) {


                            if ([[ChosenMedicen valueForKey:@"Instructions"] isEqualToString:@" "]) {

                                //////////////////////////////////////////////////////////////////////////////NSLog(@"1");
                            }
                            else
                            {
                                //////////////////////////////////////////////////////////////////////////////NSLog(@"2");


                                [label setText:[ChosenMedicen valueForKey:@"Instructions"]];
                            }
                        }

                        if ([label.title isEqualToString:@"Strength"]) {


                            if ([[ChosenMedicen valueForKey:@"Strength"] isEqualToString:@" "]) {

                                //////////////////////////////////////////////////////////////////////////////NSLog(@"1");
                            }
                            else
                            {
                                //////////////////////////////////////////////////////////////////////////////NSLog(@"2");


                                [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Strength"], [ChosenMedicen valueForKey:@"Strength_unit"]]];
                            }
                        }
                        else if ([label.title isEqualToString:@"Quantity"]) {

                            if ([[typit valueForKey:@"Dose_name"] isEqualToString:@"-"]) {


                                [label setAlpha:0];
                            }

                            else
                            {

                                [label setAlpha:1];

                                if ([[ChosenMedicen valueForKey:@"Quantity"] isEqualToString:@" "]) {

                                }
                                else
                                {
                                    if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {

                                        [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Quantity"]]];
                                    }
                                    else
                                        {
                                        //13-11-2017
                                        if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                                        {
//                                            [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Quantity"], [ChosenMedicen valueForKey:@"Dose_unit"]]]; //13-11-2017
                                            //stk
                                            [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Quantity"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                            
//                                            if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Dråber"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"])
//                                            {
//                                                label.text = [label.text stringByReplacingOccurrencesOfString:@"stk" withString:@"stykke"];
//                                            }
                                        }
                                        else
                                        {
                                            [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Quantity"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                        }
                                        
                                    }
                                }

                            }
                        }

                        else if ([label.title isEqualToString:@"Remaining"]) {

                            if ([[ChosenMedicen valueForKey:@"Remaining"] isEqualToString:@" "]) {

                            }
                            else
                            {
                                if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {

                                    [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Remaining"]]];
                                }
                                else
                                {
                                    
                                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                                    {
//                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Remaining"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                        //13-11-2017
                                        //stk
                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Remaining"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                        
//                                        if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Dråber"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"])
//                                        {
//                                            label.text = [label.text stringByReplacingOccurrencesOfString:@"stk" withString:@"stykke"];
//                                        }
                                    }
                                    else
                                    {
                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Remaining"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                    }
                                
                                }
                            }
                        }
                        else if ([label.title isEqualToString:@"Filling"]) {
                            if ([[ChosenMedicen valueForKey:@"Filling"] isEqualToString:@" "]) {

                            }
                            else
                            {
                                if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {


                                    [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Filling"]]];
                                }
                                else
                                {
                                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                                    {
//                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Filling"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                        //13-11-2017
                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Filling"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                        
//                                        if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Dråber"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"])
//                                        {
//                                            label.text = [label.text stringByReplacingOccurrencesOfString:@"stk" withString:@"stykke"];
//                                        }
                                    }
                                    else
                                    {
                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Filling"], [ChosenMedicen valueForKey:@"Dose_unit"]]];
                                    }

                                }
                            }
                        }
                        else if ([label.title isEqualToString:@"Refilling"]) {

                            if ([[ChosenMedicen valueForKey:@"Refilling"] isEqualToString:@" "]) {

                            }
                            else
                            {
                                if ([[ChosenMedicen valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {

                                    [label setText:[NSString stringWithFormat:@"%@",[ChosenMedicen valueForKey:@"Refilling"]]];
                                }
                                else
                                {
                                    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
                                    {
//                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Refilling"], [ChosenMedicen valueForKey:@"Dose_unit"]]]; //13-11-2017
                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Refilling"], [ChosenMedicen valueForKey:@"Dose_unit"]]];

//                                        if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Dråber"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Inhalator"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Salve"] || [[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Plaster"])
//                                        {
//                                            label.text = [label.text stringByReplacingOccurrencesOfString:@"stk" withString:@"stykke"];
//                                        }
                                        
                                    }
                                    else
                                    {
                                        [label setText:[NSString stringWithFormat:@"%@ %@",[ChosenMedicen valueForKey:@"Refilling"], [ChosenMedicen valueForKey:@"Dose_unit"]]];

                                    }

                                }
                            }
                        }

                    }

                }

            }

        }

    }

    //////////////////////////NSLog(@"%@", ChosenMedicen);

}

#pragma mark - i  want

- (IBAction)Cancel:(UIButton *)sender {



    [[self navigationController] popViewControllerAnimated:YES];


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    appDelegate.Editing=NO;



}


- (IBAction)Delete:(UIButton *)sender {

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    [appDelegate ShowAlert:NSLocalizedString(@"￼Warning!",nil) Messageit:NSLocalizedString(@"This medicine contains history data. All data will be lost! Are you sure, you want to delete this medicine?",nil) Custimize:NSLocalizedString(@"Delete",nil) parant:self];


}







-(void)getParantGo:(FrequencyViewControllerNew*)parant;

{

    parantGo =(FrequencyViewControllerNew*)parant;

}
- (IBAction)OK:(UIButton *)sender {




}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{


    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{

}
- (void)textViewDidEndEditing:(UITextView *)textView
{

}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{
    
}

//#pragma mark - Google Analytics
//
//-(void)googleAnalyticsGroupping
//{
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:2]value:@"Add Medicine"];
//}


#pragma mark - Google Analytics - Action

-(void)addActionForGoogleAnalytics:(NSString *)actionName
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"button_press"
                                                           label:actionName
                                                           value:nil] build]];
}


@end
