//
//  Notification.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 19-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject

+(void) postPone:(NSDictionary*)sender date:(NSDate*)time;
+(void) postTake:(NSDictionary*)sender date:(NSDate*)time;
+(void) postSkip:(NSDictionary*)sender;
+(void) postTakeEarly:(NSDictionary*)sender date:(NSDate*)time;
+(void)setDateSkip:(NSDictionary*)sender;
+(NSMutableArray*)calculateIntervalDays:(NSDate*)sender count:(NSInteger)interval;
+(void)setDate:(NSDictionary*)sender;
+(void)setDatepostpone:(NSDictionary*)sender;
+(void) postSkip2:(NSDictionary*)sender;

+(void)GoNotificationWith:(NSDate*)itemDate set:(NSDictionary*)sender andRepeatInterval:(NSCalendarUnit)CalUnit;
+(void)undowhatYoudid:(NSDictionary*)sender;
+(NSMutableArray*)GetNotification:(NSString*)sender;
+(NSDate*)setCalculatedate:(NSDictionary*)sender;
+(void)GoNotificationWeek:(NSDate*)itemDate set:(NSDictionary*)sender;
+(void)GoNotificationHour:(NSDate*)itemDate set:(NSDictionary*)sender;
+(void)GoNotificationHours:(NSDate*)itemDate set:(NSDictionary*)sender;
+(void)RenewNotification:(NSString*)sender info:(NSDictionary*)set;
+(void)CheckNotification;
+(void)RemoveNotification:(NSString*)sender;
+(void)CleareNotification;
+(void)GoNotificationDays:(NSDate*)itemDate set:(NSDictionary*)sender;
+(void)GoNotification:(NSDate*)itemDate set:(NSDictionary*)sender;
+(void)GoNotificationExtra:(NSDate*)itemDate set:(NSDictionary*)sender;


@end
