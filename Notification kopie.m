//
//  Notification.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 19-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "Notification.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "SaveData.h"
#import "PostponeNotification.h"

@implementation Notification

+(void) postPone:(NSDictionary*)sender date:(NSDate*)time

{
    
    
}

+(void) postPone2:(NSDictionary*)sender date:(NSDate*)time

{
    ////////////////NSLog(@"%@", sender);
   
    NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
      [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [setdata setDateFormat:@"dd/MM/yyyy hh:mm a"];
  
    
    NSDate *itemDate = [setdata dateFromString:[NSString stringWithFormat:@"%@ %@ %@",[sender valueForKey:@"CurrentDate"], [sender valueForKey:@"CurrentTime"], [sender valueForKey:@"Moment"]]];
    
    for (int i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
        
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        if ([[notif.userInfo valueForKey:@"ID_Medicine" ] isEqualToString:[sender valueForKey:@"ID_Medicine"]])
        {
           
            //////////////////////NSLog(@"notif %@ naar %@", notif.fireDate, time);
            
            
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notif];
            
            itemDate = notif.fireDate;
        
            [SaveData insertNotfication:data notificion:[sender valueForKey:@"ID_Medicine"] date:notif.fireDate changedate:time];
            
            [[UIApplication sharedApplication] cancelLocalNotification:notif];
            
            
        }
        else
        {
            ////////////////////////////////////////////////NSLog(@"leeg");
        }
        
    }
    

    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormat setDateFormat:@"hh:mm a"];
    
    
    NSDate *now = [[NSDate alloc] init];
    
    
    if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Hours"]) {
        
        
        ////////////////////////NSLog(@"let op");
        
        if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
            
            
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            itemDate  = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            
            
            
            
            
        }
        
        
        
    }
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Days"]) {
        
        //44 dagen
        
        
        if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue] ];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            
            itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
           
            
        }
        
        
        
        
        
    }
    
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Daily"]) {
        
        //verschillende tijden op een dag
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        
        
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
       
        [dateFormat2 setDateFormat:@"dd/MM/yyyy hh:mm a"];
        
        NSDate *date = [dateFormat2 dateFromString:[NSString  stringWithFormat:@"%@ %@ %@", [sender valueForKey:@"CurrentDate"],[sender valueForKey:@"Time"],[sender valueForKey:@"Moment"]]];
        
        NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:date];
        
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"intervalDays"] intValue]
             
             ];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            
            
        
    }
    
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Weekly"]) {
        
        if ([[sender valueForKey:@"Diverence"] intValue]>0 &&[[sender valueForKey:@"Diverence"] intValue]<=7) {
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"intervalDays"] intValue]
             
             ];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
           
        }
        
    }

    
    
    
    
     NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
 
    [format setDateFormat:@"dd/MM/yyyy"];
    
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:time];
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:time];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    [dateCompsEarly setDay:[dateComponents day]];
    [dateCompsEarly setMonth:[dateComponents month]];
    [dateCompsEarly setYear:[dateComponents year]];
    [dateCompsEarly setHour:[timeComponents hour]];
    [dateCompsEarly setMinute:timeComponents.minute];
    [dateCompsEarly setSecond:[timeComponents second]];
    NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
    
    
    [self GoNotificationExtra:itemNext set:sender];
    
    
    NSDateFormatter *dateFormat3 = [[NSDateFormatter alloc] init];
    [dateFormat3 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormat3 setDateFormat:@"hh:mm a"];
    
    //////////////NSLog(@"%@", [dateFormat3 stringFromDate:time]);
    
    NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
    [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
    [item  setObject:[format stringFromDate:now] forKey:@"Date"];
    [item  setObject:[dateFormat stringFromDate:itemNext] forKey:@"Time"];
    [item  setObject:[[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Name"] forKey:@"Name"];
    [item  setObject:[[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Strength"]   forKey:@"Dose"];
    [item  setObject:@"Postponed" forKey:@"Taken"];
    [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
    [item  setObject:@"3" forKey:@"Count"];
    [item  setObject:[dateFormat3 stringFromDate:time] forKey:@"Time_Action"];
    [item  setObject:@"5" forKey:@"Action"];
    [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
    [item  setObject:[sender valueForKey:@"Type"] forKey:@"Type"];
    
    [SaveData insertHistorySQL:item];
    
    
  [self CheckNotification];
}


+(void) postSkip:(NSDictionary*)sender{
    
    
}


+(void) postSkip2:(NSDictionary*)sender{
    
    

    //////////////////////NSLog(@"%@", sender);
    
    
    if ([sender valueForKey:@"ID"]) {
   
     
        
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
          [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [setdata setDateFormat:@"dd/MM/yyyy hh:mm a"];
        
     NSDate *itemDate = [setdata dateFromString:[NSString stringWithFormat:@"%@ %@ %@",[sender valueForKey:@"CurrentDate"], [sender valueForKey:@"CurrentTime"], [sender valueForKey:@"Moment"]]];
        
        
  
    for (int i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
        
        
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        if ([[notif.userInfo valueForKey:@"ID_Medicine"] isEqualToString:[sender valueForKey:@"ID_Medicine"]]) {
           
            if (notif.repeatInterval==NSDayCalendarUnit) {
          
            
            UIApplication *application;
            
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notif];
                
                itemDate = notif.fireDate;
                
                [SaveData insertNotfication:data notificion:[sender valueForKey:@"ID_Medicine"] date:notif.fireDate changedate:itemDate];
                
                
                
            application.applicationIconBadgeNumber = notif.applicationIconBadgeNumber-1;
            
              [[UIApplication sharedApplication] cancelLocalNotification:notif];
            
                
            }
        }
    }
    

    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
          
    [dateFormat2 setDateFormat:@"dd/MM/yyyy hh:mm a"];
    
       ////////////////////////////////NSLog(@"%@", [dateFormat2 stringFromDate:itemDate]);
    
    NSDate *now =itemDate;
   
    
    if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Hours"]) {
        
        
        if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
            
            
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            itemDate  = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            
            
            
            
            
        }
        
        
        
    }
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Days"]) {
        
        //44 dagen
        
        
        if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
          
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue] ];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            
            itemDate = [calendar dateFromComponents:dateComps];
             [self GoNotification:itemDate set:sender];
            
            
            
            
        }
        
        
        
        
        
    }
    
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Daily"]) {
        
        //verschillende tijden op een dag
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        
        
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            [dateFormat2 setDateFormat:@"dd/MM/yyyy hh:mm a"];
        
        NSDate *date = [dateFormat2 dateFromString:[NSString  stringWithFormat:@"%@ %@ %@", [sender valueForKey:@"CurrentDate"],[sender valueForKey:@"Time"],[sender valueForKey:@"Moment"]]];
        
        NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:date];
        
        
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"intervalDays"] intValue]
             
             ];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            itemDate = [calendar dateFromComponents:dateComps];
             [self GoNotification:itemDate set:sender];
            
            
            
            
        
        
    }
    
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Weekly"]) {
        
        if ([[sender valueForKey:@"Diverence"] intValue]>0 &&[[sender valueForKey:@"Diverence"] intValue]<=7) {
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
         
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"intervalDays"] intValue]
             
             ];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            itemDate = [calendar dateFromComponents:dateComps];
             [self GoNotificationWeek:itemDate set:sender];
            
            
            
            
        }
        
        
        
        
    }
    
    
    
    
        NSDateFormatter *dateFormat3 = [[NSDateFormatter alloc] init];
         [dateFormat3 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [dateFormat3 setDateFormat:@"hh:mm a"];
        
        
        NSDate *nu = [[NSDate alloc] init];
        
        
        
        
         NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
 
        [format setDateFormat:@"dd/MM/yyyy"];
        
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:nu] forKey:@"Date"];
        [item  setObject:[dateFormat3 stringFromDate:itemDate] forKey:@"Time"];
        [item  setObject:[[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Strength"]   forKey:@"Dose"];
        [item  setObject:@"Skipped" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:@"3" forKey:@"Count"];
        [item  setObject:[dateFormat3 stringFromDate:nu] forKey:@"Time_Action"];
        [item  setObject:@"5" forKey:@"Action"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        [item  setObject:[sender valueForKey:@"Type"] forKey:@"Type"];
        
        [SaveData insertHistorySQL:item];
        
        
        
        
      //[self CheckNotification];
    
    }
    
}


+(NSDate*)setCalculatedate:(NSDictionary*)sender
{
    
    NSDate *itemDate;
    
       ////////////////////////////////NSLog(@"%@", sender);
    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        
    [dateFormat2 setDateFormat:@"dd/MM/yyyy hh:mm:00"];
    
    NSString *dateit =[NSString stringWithFormat:@"%@ %@:00", [sender valueForKey:@"CurrentDate"], [sender valueForKey:@"CurrentTime"]];
    
         ////////////////////////////////NSLog(@"%@", dateit);
    NSDate *now = [dateFormat2 dateFromString:dateit];
 
    
    ////////////////////////////////NSLog(@"now %@", [dateFormat2 stringFromDate:now]);
    
    if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Hours"]) {
        
        
        if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
            
            
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
         
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            itemDate  = [calendar dateFromComponents:dateComps];
         
            
            
            
            
        }
        
        
        
    }
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Days"]) {
        
        //44 dagen
        
        
        if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
           
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            
            itemDate = [calendar dateFromComponents:dateComps];
            
         
            
            
        }
        
        
    }
    
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Daily"]) {
        
        //verschillende tijden op een dag
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
         
       [dateFormat2 setDateFormat:@"dd/MM/yyyy hh:mm a"];
        NSDate *date = [dateFormat2 dateFromString:[NSString  stringWithFormat:@"%@ %@ %@", [sender valueForKey:@"CurrentDate"],[sender valueForKey:@"Time"],[sender valueForKey:@"Moment"]]];
        
        NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:date];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"intervalDays"] intValue]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            itemDate = [calendar dateFromComponents:dateComps];
           
            
        
    }
    
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Weekly"]) {
        
        if ([[sender valueForKey:@"Diverence"] intValue]>0 &&[[sender valueForKey:@"Diverence"] intValue]<=7) {
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
          
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"intervalDays"] intValue]

];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            itemDate = [calendar dateFromComponents:dateComps];
            
        
            
            
        }
      
    }
    
    
    return itemDate;

    
    
    
}


+(void) postTake:(NSDictionary*)sender date:(NSDate*)time

{
    
}


+(void) postTake2:(NSDictionary*)sender date:(NSDate*)time;

{
    
  
    NSMutableDictionary *class = [[NSMutableDictionary alloc] init];
    
    
    
    for (id key in sender)
    {
         [class  setObject:[sender objectForKey:key] forKey:key];

    }
    if (([[class valueForKey:@"Filling"] intValue]-[[class valueForKey:@"Quantity"] intValue])<0) {
     
          [class  setObject:@"0"  forKey:@"Filling"];
    }
    else
        
    {
    [class  setObject:[NSString stringWithFormat:@"%i", [[class valueForKey:@"Filling"] intValue]-[[class valueForKey:@"Quantity"] intValue]] forKey:@"Filling"];
        
    }
    
    //////////////////NSLog(@"class %@", class);
    
 
    
    
    if ([sender valueForKey:@"ID"]) {
        
        
        
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
         [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [setdata setDateFormat:@"dd/MM/yyyy hh:mm a"];
        
        NSDate *itemDate = [setdata dateFromString:[NSString stringWithFormat:@"%@ %@ %@",[sender valueForKey:@"CurrentDate"], [sender valueForKey:@"CurrentTime"], [sender valueForKey:@"Moment"]]];
        
        
        for (int i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
            
            
            NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
            UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
            
            if ([[notif.userInfo valueForKey:@"ID_Medicine"] isEqualToString:[sender valueForKey:@"ID_Medicine"]]) {
                
             
                UIApplication *application;
              
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notif];
                
                itemDate = notif.fireDate;
                
                [SaveData insertNotfication:data notificion:[sender valueForKey:@"ID_Medicine"] date:notif.fireDate changedate:itemDate];
                
                
                
                application.applicationIconBadgeNumber = notif.applicationIconBadgeNumber-1;
                
                [[UIApplication sharedApplication] cancelLocalNotification:notif];
            }
        }
        
        
       
        
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
         
        [dateFormat2 setDateFormat:@"dd/MM/yyyy hh:mm:00"];
        
       
        NSDate *now =itemDate;
        
        
        if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Hours"]) {
            
            
            if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
                
                
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                
                NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]];
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                itemDate  = [calendar dateFromComponents:dateComps];
                [self GoNotification:itemDate set:class];
                
                
                
                
                
            }
            
            
            
        }
        else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Days"]) {
            
            //44 dagen
            
            
            if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                
                NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue] ];
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                
                
                itemDate = [calendar dateFromComponents:dateComps];
                [self GoNotification:itemDate set:class];
                
                
                
                
            }
            
        }
        
        else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Daily"]) {
            
            //verschillende tijden op een dag
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
              
            [dateFormat2 setDateFormat:@"dd/MM/yyyy hh:mm a"];
            
            NSDate *date = [dateFormat2 dateFromString:[NSString  stringWithFormat:@"%@ %@ %@", [sender valueForKey:@"CurrentDate"],[sender valueForKey:@"Time"],[sender valueForKey:@"Moment"]]];
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:date];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"intervalDays"] intValue]
                 
                 ];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                
                itemDate = [calendar dateFromComponents:dateComps];
                [self GoNotification:itemDate set:class];
                
                
                
                
                
            
        }
        
        else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Weekly"]) {
            
            if ([[sender valueForKey:@"Diverence"] intValue]>0 &&[[sender valueForKey:@"Diverence"] intValue]<=7) {
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                
                
                NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"intervalDays"] intValue]
                 
                 ];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                // Notification will fire in one minute
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                
                itemDate = [calendar dateFromComponents:dateComps];
                [self GoNotification:itemDate set:class];
                
                
                
                
            }
            
            
            
            
        }
        
        
        
        NSDateFormatter *dateFormat3 = [[NSDateFormatter alloc] init];
          [dateFormat3 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [dateFormat3 setDateFormat:@"hh:mm a"];
        
        
        NSDate *nu = [[NSDate alloc] init];
        
        
        
        
         NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
 
        [format setDateFormat:@"dd/MM/yyyy"];
        
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:nu] forKey:@"Date"];
        [item  setObject:[dateFormat3 stringFromDate:time] forKey:@"Time"];
        [item  setObject:[[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Strength"]   forKey:@"Dose"];
        [item  setObject:@"Taken" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:@"3" forKey:@"Count"];
        [item  setObject:[dateFormat3 stringFromDate:nu] forKey:@"Time_Action"];
        [item  setObject:@"5" forKey:@"Action"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        [item  setObject:[sender valueForKey:@"Type"] forKey:@"Type"];
        
        //////////////////////NSLog(@"%@", item);
        
        [SaveData insertHistorySQL:item];
        
        
        
        
      //[self CheckNotification];
        
    }
    
    
    
}
+(void) postTakeEarly:(NSDictionary*)sender date:(NSDate*)time

{
    
}

+(void) postTakeEarly2:(NSDictionary*)sender date:(NSDate*)time

{
    
    NSMutableDictionary *class = [[NSMutableDictionary alloc] init];
    
    
    
    for (id key in sender)
    {
        [class  setObject:[sender objectForKey:key] forKey:key];
        
    }
    
    
    [class  setObject:[NSString stringWithFormat:@"%i", [[class valueForKey:@"Filling"] intValue]-[[class valueForKey:@"Quantity"] intValue]] forKey:@"Filling"];
    
    //////////////////NSLog(@"class %@", class);
    
    
    
    
    if ([sender valueForKey:@"ID"]) {
        
        
        
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
        [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [setdata setDateFormat:@"dd/MM/yyyy hh:mm a"];
        
        NSDate *itemDate = [setdata dateFromString:[NSString stringWithFormat:@"%@ %@ %@",[sender valueForKey:@"CurrentDate"], [sender valueForKey:@"CurrentTime"], [sender valueForKey:@"Moment"]]];
        
        
        for (int i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
            
            
            NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
            UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
            
            if ([[notif.userInfo valueForKey:@"ID_Medicine"] isEqualToString:[sender valueForKey:@"ID_Medicine"]]) {
           
                
                UIApplication *application;
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notif];
                
                [SaveData insertNotfication:data notificion:[sender valueForKey:@"ID_Medicine"] date:notif.fireDate changedate:itemDate];
                
                
                
                application.applicationIconBadgeNumber = 0;
                
                [[UIApplication sharedApplication] cancelLocalNotification:notif];
            }
        }
        
        
        
        ////////////////////////////////NSLog(@"postSkip");
        
        
        
        
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
         
        [dateFormat2 setDateFormat:@"dd/MM/yyyy hh:mm: 00"];
        
        ////////////////////////////////NSLog(@"%@", [dateFormat2 stringFromDate:itemDate]);
        
        NSString *dateit =[NSString stringWithFormat:@"%@ %@:00", [sender valueForKey:@"CurrentDate"], [sender valueForKey:@"CurrentTime"]];
        
        NSDate *now =itemDate;
        
        
        if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Hours"]) {
            
            
            if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
                
                
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                
                NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]];
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                itemDate  = [calendar dateFromComponents:dateComps];
                [self GoNotification:itemDate set:class];
                
                
                
                
                
            }
            
            
            
        }
        else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Days"]) {
            
            //44 dagen
            
            
            if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                
                NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue] ];
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                
                
                itemDate = [calendar dateFromComponents:dateComps];
                [self GoNotification:itemDate set:class];
                
                
                
                
            }
            
            
            
            
            
        }
        
        else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Daily"]) {
            
            //verschillende tijden op een dag
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            
            
            NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            
            [dateFormat2 setDateFormat:@"dd/MM/yyyy hh:mm a"];
            NSDate *date = [dateFormat2 dateFromString:[NSString  stringWithFormat:@"%@ %@ %@", [sender valueForKey:@"CurrentDate"],[sender valueForKey:@"Time"],[sender valueForKey:@"Moment"]]];
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:date];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"intervalDays"] intValue]
                 
                 ];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                
                itemDate = [calendar dateFromComponents:dateComps];
                [self GoNotification:itemDate set:class];
                
                
                
                
            
        }
        
        else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Weekly"]) {
            
            if ([[sender valueForKey:@"Diverence"] intValue]>0 &&[[sender valueForKey:@"Diverence"] intValue]<=7) {
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                
                
                NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"intervalDays"] intValue]
                 
                 ];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                // Notification will fire in one minute
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                
                itemDate = [calendar dateFromComponents:dateComps];
                [self GoNotification:itemDate set:class];
                
                
                
                
            }
            
            
            
            
        }
        
       
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [dateFormat setDateFormat:@"hh:mm a"];
        
       
        
        
         NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
 
        [format setDateFormat:@"dd/MM/yyyy"];
        
        
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:time];
        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:time];
        
        NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        ////////////////////////////////////////////////NSLog(@"taken at %@", [dateFormat stringFromDate:itemNext]);
        
        
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:now] forKey:@"Date"];
        [item  setObject:[dateFormat stringFromDate:itemNext] forKey:@"Time"];
        [item  setObject:[[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Strength"]   forKey:@"Dose"];
        [item  setObject:@"Taken earlier" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:@"3" forKey:@"Count"];
        [item  setObject:[dateFormat stringFromDate:time] forKey:@"Time_Action"];
        [item  setObject:@"5" forKey:@"Action"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        [item  setObject:[sender valueForKey:@"Type"] forKey:@"Type"];
        
        [SaveData insertHistorySQL:item];
        
        
        
        
      //[self CheckNotification];
        
    }
    
    
 
    
    
 
    
}




+(void)setDate:(NSDictionary*)sender
{

    
    //NSLog(@"%@", sender);
    
    NSArray *Alertsit = [[sender valueForKey:@"Dates"] componentsSeparatedByString:@"#"];
    
    for (int i=0 ; i<[Alertsit count]; i++) {
        
        
        NSArray *timesIt = [[sender valueForKey:@"Times"] componentsSeparatedByString:@","];
        
        for (int k=0 ; k<[timesIt count]; k++) {
            
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            
            [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy hh:mm a"];
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            
            NSDate *now = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",[Alertsit objectAtIndex:i], [timesIt objectAtIndex:k]]];
            
            
            //NSLog(@"%@", [dateFormatter stringFromDate:now]);
            
            if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Weekly"]) {
                
                
                
                
                NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                
                [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                // Notification will fire in one minute
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                
                NSDate *itemDate = [calendar dateFromComponents:dateComps];
                [self GoNotificationWith:itemDate set:sender andRepeatInterval:NSWeekCalendarUnit];
                
                
            }
            
            if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Daily"]) {
                
               
           
                     //NSLog(@"%@", now);
                
                
                NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                // Notification will fire in one minute
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                
                NSDate *itemDate = [calendar dateFromComponents:dateComps];
                [self GoNotification:itemDate set:sender];
                
                
            }
            
            
            NSDate *today = [[NSDate alloc] init];
            
            
            if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Every X Hours"]) {
                
                
                
                
                if  ([today compare:now]==NSOrderedSame)
                    
                {
                 
                    
                    
                    
                    
                }
                else if ([today compare:now]==NSOrderedAscending)
                    
                    
                {
                    
                    
                    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                    
                    [dateComps setDay:[dateComponents day]];
                    [dateComps setMonth:[dateComponents month]];
                    [dateComps setYear:[dateComponents year]];
                    [dateComps setHour:[timeComponents hour]];
                    // Notification will fire in one minute
                    [dateComps setMinute:[timeComponents minute]];
                    [dateComps setSecond:[timeComponents second]];
                    
                    
                    NSDate *itemDate = [calendar dateFromComponents:dateComps];
                    [self GoNotification:itemDate set:sender];
                    
                    
                }
                else if ([today compare:now]==NSOrderedDescending)
                    
                    
                {
                    
                    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                    
                    [dateComps setDay:[dateComponents day]+7];
                    [dateComps setMonth:[dateComponents month]];
                    [dateComps setYear:[dateComponents year]];
                    [dateComps setHour:[timeComponents hour]];
                    // Notification will fire in one minute
                    [dateComps setMinute:[timeComponents minute]];
                    [dateComps setSecond:[timeComponents second]];
                    
                    
                    NSDate *itemDate = [calendar dateFromComponents:dateComps];
                    [self GoNotification:itemDate set:sender];
                  
                }

                
                
                /*
                //NSLog(@"%@", now);
                
             
                 
                 */
                
                
            }
            
            
            
            if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Every X Days"]) {
                
                
                //NSLog(@"%@", now);
               
                
                NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                [dateFormatter2 setDateFormat:@"EEEE dd, MMMM yyyy"];
                
                NSMutableArray *numberofDays =[self calculateIntervalDays:[dateFormatter2 dateFromString:[sender valueForKey:@"Starting"]] count:[[sender valueForKey:@"Frequency"] intValue]];
                
                
                   for (int i=0 ; i<10; i++) {
                       
                       
                       NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                       [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                       
                       [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy hh:mm a"];
                       
                   
                       
                       NSDate *now = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",[numberofDays objectAtIndex:i], [timesIt objectAtIndex:k]]];
                       
                       
                       //NSLog(@"dateFormatter %@", [dateFormatter stringFromDate:now]);
                       
                       
                       NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                       NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                       
                       [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]];
                       [dateComps setMonth:[dateComponents month]];
                       [dateComps setYear:[dateComponents year]];
                       [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"Frequency"] intValue]];
                       // Notification will fire in one minute
                       [dateComps setMinute:[timeComponents minute]];
                       [dateComps setSecond:[timeComponents second]];
                       
                       
                       NSDate *itemDate = [calendar dateFromComponents:dateComps];
                       [self GoNotificationDays:itemDate set:sender];
                   }
                
                /*
                //NSLog(@"%@", [sender valueForKey:@"Frequency"]);
                
                
                NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
                NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
                
                [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"Frequency"] intValue]];
                // Notification will fire in one minute
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                
                
                NSDate *itemDate = [calendar dateFromComponents:dateComps];
                [self GoNotificationDays:itemDate set:sender];
                 
                 */
                
                
                
                
                
            }
            
            
        
       
                
            }
            
        
        
    }



    /*
     
     NSEraCalendarUnit = kCFCalendarUnitEra,
     NSYearCalendarUnit = kCFCalendarUnitYear,
     NSMonthCalendarUnit = kCFCalendarUnitMonth,
     NSDayCalendarUnit = kCFCalendarUnitDay,
     NSHourCalendarUnit = kCFCalendarUnitHour,
     NSMinuteCalendarUnit = kCFCalendarUnitMinute,
     NSSecondCalendarUnit = kCFCalendarUnitSecond,
     NSWeekCalendarUnit = kCFCalendarUnitWeek,
     NSWeekdayCalendarUnit = kCFCalendarUnitWeekday,
     NSWeekdayOrdinalCalendarUnit = kCFCalendarUnitWeekdayOrdinal
     NSQuarterCalendarUnit = kCFCalendarUnitQuarter,
     
     */
    
    
}




+(NSMutableArray*)calculateIntervalDays:(NSDate*)sender count:(NSInteger)interval

{
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter2 setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    NSMutableArray *datesRow =[[NSMutableArray alloc]init];
    for (int i=0 ; i<10; i++) {
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:sender];
        
        
        NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
        [dateCompsEarly setDay:[dateComponents day]+((interval/24)*i)];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        //NSLog(@"%@", itemNext);
        
        
        [datesRow addObject:[dateFormatter2 stringFromDate:itemNext]];
        
        
        
    }
    
    
   
    
    return datesRow;
    
    
}


+(void) notify

{
    /*
    
    UILocalNotification *    localNotifEndCycle = [[UILocalNotification alloc] init];
    localNotifEndCycle.alertBody = @"Your Expected Date ";
    NSDate *now = [NSDate date];
    
    for( int i = 1; i <= 10;i++)
    {
        localNotifEndCycle.alertBody = @"Your Expected Date ";
        localNotifEndCycle.soundName=@"best_guitar_tone.mp3";
        localNotifEndCycle.fireDate = [NSDate dateWithTimeInterval:180*i sinceDate:now];
        [[UIApplication sharedApplication] scheduleLocalNotification: localNotifEndCycle];
    }
}


    UILocalNotification *locNot = [[UILocalNotification alloc] init];
    NSDate *now = [NSDate date];
    NSInterval interval;
    switch( freqFlag ) {     // Where freqFlag is NSHourCalendarUnit for example
        case NSHourCalendarUnit:
            interval = 60 * 60;  // One hour in seconds
            break;
        case NSDayCalendarUnit:
            interval = 24 * 60 * 60; // One day in seconds
            break;
    }
    if( every == 1 ) {
        locNot.fireDate = [NSDate dateWithTimeInterval: interval fromDate: now];
        locNot.repeatInterval = freqFlag;
        [[UIApplication sharedApplication] scheduleLocalNotification: locNot];
    } else {
        for( int i = 1; i <= repeatCountDesired; ++i ) {
            locNot.fireDate = [NSDate dateWithTimeInterval: interval*i fromDate: now];
            [[UIApplication sharedApplication] scheduleLocalNotification: locNot];
        }
    }
    [locNot release];
     
     */
}

/*

- (void)scheduleNotificationWithItem:(ToDoItem *)item interval:(int)minutesBefore {
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:item.day];
    [dateComps setMonth:item.month];
    [dateComps setYear:item.year];
    [dateComps setHour:item.hour];
    [dateComps setMinute:item.minute];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    [dateComps release];
    
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.fireDate = [itemDate addTimeInterval:-(minutesBefore*60)];
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    localNotif.alertBody = [NSString stringWithFormat:NSLocalizedString(@"%@ in %i minutes.", nil),
                            item.eventName, minutesBefore];
    localNotif.alertAction = NSLocalizedString(@"View Details", nil);
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:item.eventName forKey:ToDoItemKey];
    localNotif.userInfo = infoDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    [localNotif release];
}
 
 */


+(void)setDatepostpone:(NSDictionary*)sender{
    
}

+(void)setDatepostpone2:(NSDictionary*)sender{
    
  //////////////////////////////NSLog(@"setDatepostpone");
    
    
    
    
    
    if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Hours"]) {
        
        //44 uur intervalOurs
        
        if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
            
            
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            NSDate *now = [PostponeNotification setDatePostpone:sender];
            
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            
            NSDate *itemDate  = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            
            
            /*
             if ([[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0] intValue]>0) {
             
             
             NSDateComponents *dateCompsLate = [[NSDateComponents alloc] init];
             [dateCompsLate setDay:[dateComponents day]];
             [dateCompsLate setMonth:[dateComponents month]];
             [dateCompsLate setYear:[dateComponents year]];
             [dateCompsLate setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]];
             [dateCompsLate setMinute:dateComps.minute+[[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0] intValue]];
             [dateCompsLate setSecond:[timeComponents second]];
             
             
             NSDate *itemNext = [calendar dateFromComponents:dateCompsLate];
             [self GoNotification:itemNext set:sender];
             
             
             
             ////////////////////////////////////////////////////////////NSLog(@"Late");
             }
             
             */
            
            
            
            
            
        }
        
        
        
    }
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Days"]) {
        
        //44 dagen
        
        
        if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            NSDate *now = [PostponeNotification setDatePostpone:sender];
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            
            
            /*
             if ([[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0] intValue]>0) {
             
             
             NSDateComponents *dateCompsLate = [[NSDateComponents alloc] init];
             [dateCompsLate setDay:[dateComponents day]];
             [dateCompsLate setMonth:[dateComponents month]];
             [dateCompsLate setYear:[dateComponents year]];
             [dateCompsLate setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]];
             [dateCompsLate setMinute:dateComps.minute+[[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0] intValue]];
             [dateCompsLate setSecond:[timeComponents second]];
             
             
             NSDate *itemNext = [calendar dateFromComponents:dateCompsLate];
             [self GoNotification:itemNext set:sender];
             
             
             ////////////////////////////////////////////////////////////NSLog(@"Late");
             }
             
             */
            
            
        }
        
        
        
        
        
    }
    
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Daily"]) {
        
        //verschillende tijden op een dag
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDate *now = [PostponeNotification setDatePostpone:sender];
        
        
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
         
        [dateFormat2 setDateFormat:@"dd/MM/yyyy hh:mm a"];
        NSDate *date = [dateFormat2 dateFromString:[NSString  stringWithFormat:@"%@ %@ %@", [sender valueForKey:@"CurrentDate"],[sender valueForKey:@"Time"],[sender valueForKey:@"Moment"]]];
        
        NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:date];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            
            
            ////////////////////////////////////////////////NSLog(@"Reminder %@", [[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0]);
            
            
            /*
             if ([[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0] intValue]>0) {
             
             NSDateComponents *dateCompsLate = [[NSDateComponents alloc] init];
             [dateCompsLate setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]];
             [dateCompsLate setMonth:[dateComponents month]];
             [dateCompsLate setYear:[dateComponents year]];
             [dateCompsLate setHour:[timeComponents hour]];
             // Notification will fire in one minute
             [dateCompsLate setMinute:dateComps.minute+[[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0] intValue]];
             [dateCompsLate setSecond:[timeComponents second]];
             
             
             NSDate *itemDate = [calendar dateFromComponents:dateCompsLate];
             [self GoNotification:itemDate set:sender];
             
             ////////////////////////////////////////////////////////////NSLog(@"Late");
             }
             
             */
            
            
            
        
    }
    
    else if ([[sender valueForKey:@"Frequency_text"] isEqualToString:@"Weekly"]) {
        
        if ([[sender valueForKey:@"Diverence"] intValue]>0 &&[[sender valueForKey:@"Diverence"] intValue]<=7) {
            
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            
            NSDate *now = [PostponeNotification setDatePostpone:sender];
            
            
            
            NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
            NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now];
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            
            /*
             
             
             if ([[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0] intValue]>0) {
             
             
             
             NSDateComponents *dateCompsLate = [[NSDateComponents alloc] init];
             [dateCompsLate setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]];
             [dateCompsLate setMonth:[dateComponents month]];
             [dateCompsLate setYear:[dateComponents year]];
             [dateCompsLate setHour:[timeComponents hour]];
             // Notification will fire in one minute
             [dateCompsLate setMinute:dateComps.minute+[[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0] intValue]];
             
             [dateCompsLate setSecond:[timeComponents second]];
             
             
             NSDate *itemDate = [calendar dateFromComponents:dateCompsLate];
             [self GoNotification:itemDate set:sender];
             
             }
             
             */
            
            
        }
        
        
        
        
    }
    
    
    
  //[self CheckNotification];
    

    
    
}


+(void)undowhatYoudid:(NSDictionary*)sender

{
    
}


+(void)undowhatYoudid2:(NSDictionary*)sender

{
    
    
    
    for (int i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
        
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        if ([[notif.userInfo valueForKey:@"ID_Medicine" ] isEqualToString:sender])
        {
            ////////////////////////////////////////////////////NSLog(@"notif %@", notif);
            
            UIApplication *application;
            
            application.applicationIconBadgeNumber = notif.applicationIconBadgeNumber-1;
            
            [[UIApplication sharedApplication] cancelLocalNotification:notif];
            
        }
        else
        {
            ////////////////////////////////////////////////////NSLog(@"leeg");
        }
        
    }
    
    NSData *data = (NSData*)[[GetData GetNotification:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Notifications"];
    UILocalNotification *notif = (UILocalNotification*)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    

   
    ////////////////NSLog(@"notif %@", notif);
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
        UIBackgroundTaskIdentifier bgTask =0;
        UIApplication  *app = [UIApplication sharedApplication];
        bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:bgTask];
        }];
        
        
        [app scheduleLocalNotification:notif];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate reloadTableview];
        
    }
    
    
    
}

+(NSMutableArray*)GetNotification:(NSString*)sender;

{
    
    NSMutableArray *back = [[NSMutableArray alloc] init];
    
    for (int i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
        
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        if ([[notif.userInfo valueForKey:@"ID_Medicine" ] isEqualToString:sender])
        {
            
            ////////////////////////////NSLog(@"notif %@", notif);
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notif];
            
            
            [SaveData insertNotfication:data notificion:sender date:notif.fireDate changedate:notif.fireDate];
            
            
            [back addObject:notif.userInfo];
            
            
            
        }
        else
        {
            
        }
        
    }
    
    
    return back;
    
}





+(void)CheckNotification

{
    
    for (int i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
        
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
        [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [setdata setDateFormat:@"EEEE dd, MMMM yyyy HH:mm a"];
        
     //NSLog(@"check  %@ %@", [setdata stringFromDate:notif.fireDate], [notif.userInfo valueForKey:@"ID_Medicine"]);
    }
    
}


+(void)RemoveNotification:(NSString*)sender{
    
    
  ////////////////////////////////NSLog(@"RemoveNotification");
    
    for (int i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
        
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        if ([[notif.userInfo valueForKey:@"ID_Medicine" ] isEqualToString:sender])
        {
            ////////////////////////////////////////////////NSLog(@"notif %@", notif);
            
            UIApplication *application;
            
            application.applicationIconBadgeNumber = notif.applicationIconBadgeNumber-1;
            [[UIApplication sharedApplication] cancelLocalNotification:notif];
            
            
        }
        else
        {
            ////////////////////////////////////////////////NSLog(@"leeg");
        }
        
    }
    
}


+(void)RenewNotification:(NSString*)sender info:(NSDictionary*)set{
    
    
    ////////////////////////////////NSLog(@"RemoveNotification");
    
    for (int i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
        
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
        
        if ([[notif.userInfo valueForKey:@"ID_Medicine" ] isEqualToString:sender])
        {
            [notif setUserInfo:set];
            
          
            
           
               ////////////////////////////NSLog(@"total %@", [notif.userInfo valueForKey:@"ID_Medicine"]);
            
        }
        else
        {
          
        }
        
    }
    
}

+(void)CleareNotification{
    
    
   ////////////////////////////////NSLog(@"CleareNotification");
    

        for (int i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
            
            NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
            UILocalNotification *notif = [notificationArray objectAtIndex:i-1];
            
            UIApplication *application;
            
            application.applicationIconBadgeNumber = notif.applicationIconBadgeNumber-1;
            [[UIApplication sharedApplication] cancelLocalNotification:notif];
            
        }
        
}


+(void)GoNotificationHours:(NSDate*)itemDate set:(NSDictionary*)sender
{
    
   
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
        UIBackgroundTaskIdentifier bgTask =0;
        UIApplication  *app = [UIApplication sharedApplication];
        bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:bgTask];
        }];
        
        
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        [notif setAlertBody:[NSString stringWithFormat:@"Take your %@!", [[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Name"]]];
        notif.alertAction = @"Show?";
        notif.applicationIconBadgeNumber = 1;
        if (appDelegate.chosenMusic) {
            
            notif.soundName = [NSString stringWithFormat:@"%@.mp3",[[[GetData getMusic] valueForKey:@"title"]objectAtIndex:[[[GetData getMusic] valueForKey:@"ID"] indexOfObject:appDelegate.chosenMusic]]];
        }
        else
        {
            notif.soundName = @"twinkle.mp3";
        }
        notif.applicationIconBadgeNumber = 1;
        notif.hasAction = YES;
        
        
        notif.repeatInterval = NSHourCalendarUnit;
        
        
        ////////////////////////NSLog(@"%@ %@ ", sender, itemDate, notif);
        
        
        [app scheduleLocalNotification:notif];
        
        
        
        
        
    }
    
    
    //[self CheckNotification];
    
}


+(void)GoNotificationWith:(NSDate*)itemDate set:(NSDictionary*)sender andRepeatInterval:(NSCalendarUnit)CalUnit
{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
        UIBackgroundTaskIdentifier bgTask =0;
        UIApplication  *app = [UIApplication sharedApplication];
        bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:bgTask];
        }];
        
        
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        [notif setAlertBody:[NSString stringWithFormat:@"Take your %@!", [[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Name"]]];
        notif.alertAction = @"Show?";
        notif.applicationIconBadgeNumber = 1;
        if (appDelegate.chosenMusic) {
            
            notif.soundName = [NSString stringWithFormat:@"%@.mp3",[[[GetData getMusic] valueForKey:@"title"]objectAtIndex:[[[GetData getMusic] valueForKey:@"ID"] indexOfObject:appDelegate.chosenMusic]]];
        }
        else
        {
            notif.soundName = @"twinkle.mp3";
        }
        notif.applicationIconBadgeNumber = 1;
        notif.hasAction = YES;
        
        
        notif.repeatInterval = CalUnit;
        
        
        ////////////////////////NSLog(@"%@ %@ ", sender, itemDate, notif);
        
        
        [app scheduleLocalNotification:notif];
        
        
        
        //NSLog(@"%@", notif);
        
        
    }
   
}



+(void)GoNotificationWeek:(NSDate*)itemDate set:(NSDictionary*)sender
{
    
  
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
        UIBackgroundTaskIdentifier bgTask =0;
        UIApplication  *app = [UIApplication sharedApplication];
        bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:bgTask];
        }];
        
        
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        [notif setAlertBody:[NSString stringWithFormat:@"Take your %@!", [[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Name"]]];
        notif.alertAction = @"Show?";
        notif.applicationIconBadgeNumber = 1;
        if (appDelegate.chosenMusic) {
            
            notif.soundName = [NSString stringWithFormat:@"%@.mp3",[[[GetData getMusic] valueForKey:@"title"]objectAtIndex:[[[GetData getMusic] valueForKey:@"ID"] indexOfObject:appDelegate.chosenMusic]]];
        }
        else
        {
            notif.soundName = @"twinkle.mp3";
        }
        notif.applicationIconBadgeNumber = 1;
        notif.hasAction = YES;
        
        
        notif.repeatInterval = NSWeekCalendarUnit;
        
        
        ////////////////////////NSLog(@"%@ %@ ", sender, itemDate, notif);
        
        
        [app scheduleLocalNotification:notif];
        
        
        
        //NSLog(@"%@", notif);
        
        
    }
    
    
    //[self CheckNotification];
    
}


+(void)GoNotificationDays:(NSDate*)itemDate set:(NSDictionary*)sender
{
   
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
        UIBackgroundTaskIdentifier bgTask =0;
        UIApplication  *app = [UIApplication sharedApplication];
        bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:bgTask];
        }];
        
        
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        [notif setAlertBody:[NSString stringWithFormat:@"Take your %@!", [[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Name"]]];
        notif.alertAction = @"Show?";
        notif.applicationIconBadgeNumber = 1;
        if (appDelegate.chosenMusic) {
            
            notif.soundName = [NSString stringWithFormat:@"%@.mp3",[[[GetData getMusic] valueForKey:@"title"]objectAtIndex:[[[GetData getMusic] valueForKey:@"ID"] indexOfObject:appDelegate.chosenMusic]]];
        }
        else
        {
            notif.soundName = @"twinkle.mp3";
        }
        notif.applicationIconBadgeNumber = 1;
        notif.hasAction = YES;
        
        
        notif.repeatInterval = 0;
        
        
        
        ////////////////////////NSLog(@"%@ %@ ", sender, itemDate, notif);
        
        
        [app scheduleLocalNotification:notif];
        
        
        
        //NSLog(@"%@", notif);
        
        
    }
    
    
    //[self CheckNotification];
    
}


+(void)GoNotification:(NSDate*)itemDate set:(NSDictionary*)sender
{
   
    
    ////////////NSLog(@"GoNotification %@  %@", sender, itemDate);
    
    
    
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
        UIBackgroundTaskIdentifier bgTask =0;
        UIApplication  *app = [UIApplication sharedApplication];
        bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:bgTask];
        }];
        
        
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        [notif setAlertBody:[NSString stringWithFormat:@"Take your %@!", [[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Name"]]];
        notif.alertAction = @"Show?";
        notif.applicationIconBadgeNumber = 1;
        if (appDelegate.chosenMusic) {
            
            notif.soundName = [NSString stringWithFormat:@"%@.mp3",[[[GetData getMusic] valueForKey:@"title"]objectAtIndex:[[[GetData getMusic] valueForKey:@"ID"] indexOfObject:appDelegate.chosenMusic]]];
        }
        else
        {
            notif.soundName = @"twinkle.mp3";
        }
        notif.applicationIconBadgeNumber = 1;
        notif.hasAction = YES;
        
        
        notif.repeatInterval = NSDayCalendarUnit;
        
        
        ////////////////////////NSLog(@"%@ %@ ", sender, itemDate, notif);
        
        
        [app scheduleLocalNotification:notif];
        
        
        
        //NSLog(@"%@", notif);
                
        
    }
    
    
  //[self CheckNotification];
    
}

+(void)GoNotificationExtra:(NSDate*)itemDate set:(NSDictionary*)sender
{
    
    
    //////////////////NSLog(@"%@", sender);
    

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
        UIBackgroundTaskIdentifier bgTask =0;
        UIApplication  *app = [UIApplication sharedApplication];
        bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:bgTask];
        }];
        
        
        
        
        
        //////////////////////////////////////////////////NSLog(@"%@", sender);
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        //notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        [notif setAlertBody:[NSString stringWithFormat:@"Take your %@!", [[GetData getMedicinewithID:[sender valueForKey:@"ID_Medicine"]] valueForKey:@"Name"]]];
        notif.alertAction = @"Show?";
        notif.applicationIconBadgeNumber = 1;
        if (appDelegate.chosenMusic) {
            
            notif.soundName = [NSString stringWithFormat:@"%@.mp3",[[[GetData getMusic] valueForKey:@"title"]objectAtIndex:[[[GetData getMusic] valueForKey:@"ID"] indexOfObject:appDelegate.chosenMusic]]];
        }
        else
        {
            notif.soundName = @"twinkle.mp3";
        }
        notif.applicationIconBadgeNumber = 0;
        notif.hasAction = YES;
        
        
        notif.repeatInterval = NSMinuteCalendarUnit;
        
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notif];
        
        [SaveData insertPostponment:data notificion:[sender valueForKey:@"ID_Medicine"]];
        
        
        
        [app scheduleLocalNotification:notif];
        
        
    
        
        
        
        
    }
    
    
}




@end
