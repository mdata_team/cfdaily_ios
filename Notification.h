//
//  Notification.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 19-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject

+(void) postPone:(NSMutableDictionary*)sender date:(NSDate*)time andNote:(UILocalNotification*)curentChoise;;
+(void) postTake:(NSMutableDictionary*)sender date:(NSDate*)time andNote:(UILocalNotification*)curentChoise;;
+(void) postSkip:(NSMutableDictionary*)sender andNote:(UILocalNotification*)curentChoise;
+(void) postTakeEarly:(NSMutableDictionary*)sender date:(NSDate*)time andNote:(UILocalNotification*)curentChoise;;
+(NSMutableArray*)calculateIntervalDays:(NSDate*)sender count:(NSInteger)interval;

+(void)setDate:(NSMutableDictionary*)sender;
+(void)GoNotificationWith:(NSDate*)itemDate set:(NSMutableDictionary*)sender andRepeatInterval:(NSCalendarUnit)CalUnit;
+(void)undowhatYoudid:(NSMutableDictionary*)sender;
+(void)GoNotificationWeek:(NSDate*)itemDate set:(NSMutableDictionary*)sender;
+(void)GoNotificationHours:(NSDate*)itemDate set:(NSMutableDictionary*)sender;
+(void)RenewNotification:(NSString*)sender info:(NSMutableDictionary*)set;
+(void)CheckNotification;
+(void)RemoveNotification:(NSString*)sender;
+(void)CleareNotification;
+(NSDate*)calculatedateLate:(NSDate*)date late:(NSInteger)minute;
+(void)SilenceNotification:(NSString*)sender;
+(void)GoNotification:(NSDate*)itemDate set:(NSMutableDictionary*)sender ;
+(void)GoNotificationExtra:(NSDate*)itemDate set:(NSMutableDictionary*)sender andNote:(UILocalNotification*)curentChoise;;
+(NSString*)SetNumber:(NSString*)type count:(NSString*)dose;
+(void)FireAtwill:(NSMutableDictionary*)sender setDate:(NSDate*)date;
+(NSDate*)CalculateDateOnDay:(NSString*)sender StartingDate:(NSString*)starting;
+(NSString*)CalculateNumberleft:(NSString*)set onID:(NSString*)IDit;
+(NSString*) Remaining:(NSString*)left Quantity:(NSString*)less;
+ (NSString *) floatToString:(NSString*) val;
+(NSInteger)CheckNumbers:(NSDate*)sender;
+(NSInteger)CheckNumbers:(NSDate*)sender withID:(NSString*)ID;
+(void)changeDictonary:(NSMutableDictionary*)set and:(UILocalNotification*)notid;
+(void)changeDictonary:(NSMutableDictionary*)set;
+(void)ActivateNotification:(NSString*)set active:(NSString*)doornot;


+(NSDate*)whatNotificationConvert:(UILocalNotification*)sender;
+(void)insertforundofunction:(NSMutableDictionary*)sender;

+(void)insertforundoExtra:(NSMutableDictionary*)sender;
@end

