//
//  Notification.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 19-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "Notification.h"
#import "AppDelegate.h"
#import "GetData.h"
#import "SaveData.h"
#import "GetData.h"
#import "PostponeNotification.h"
#import "GetData.h"
#import "SaveCore.h"
#import <AudioToolbox/AudioServices.h>

@implementation Notification


+(NSMutableArray*)calculateIntervalDays:(NSDate*)sender count:(NSInteger)interval

{
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    NSMutableArray *datesRow =[[NSMutableArray alloc]init];
    for (NSInteger i=0 ; i<10; i++) {
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:sender];
        
        
        NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
        [dateCompsEarly setDay:[dateComponents day]+((interval/24)*i)];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        [datesRow addObject:[dateFormatter2 stringFromDate:itemNext]];
        
        
        
    }
    
    
    
    
    return datesRow;
    
    
}

+(NSDateFormatter*) setFormatCompete:(NSDateFormatter*) set

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [set setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [set setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
    return set;
}

+(NSDate*)CalculateDateOnDay:(NSString*)sender StartingDate:(NSString*)starting

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
    
    
    NSDate *returndate;
    
    
    for (NSInteger i=1 ; i<8; i++) {
        
        
        unsigned units = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorian components:units fromDate:[dateFormatter2 dateFromString:sender]];
        NSDateComponents *timeComponents = [gregorian components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:sender]];
        
        
        [components setDay:[components day]+(i-1)];
        [components setHour:[timeComponents hour]];
        // Notification will fire in one minute
        [components setMinute:[timeComponents minute]];
        [components setSecond:0];
        
        NSDate *thisDate = [gregorian dateFromComponents:components];
        
        NSCalendar* cal = [NSCalendar currentCalendar];
        NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:thisDate];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatter2 setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
        
        if ([comp weekday] ==[starting intValue]) {
            
            returndate= thisDate;
            
        }
        
        if ([starting intValue]==8) {
            
            if ([comp weekday] ==1) {
                
                returndate= thisDate;
                
            }
        }
        
        
        
    }
    
    
    return returndate;
    
    
    
    
}



+(void)setDate:(NSMutableDictionary*)sender
{
 
    
    [self RemoveNotification:[sender valueForKey:@"ID_Medicine"]];
    
    
 
    
    //NSLog(@"Frequency_text %@", [sender valueForKey:@"Frequency_text"]);
    
    [self insertforundoExtra:sender];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSDate *now = [[NSDate alloc] init];
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    
    
    NSDateComponents *comp2 = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:now];
    
    NSInteger numberOfDays = [comp2 day];
    
    
        
        NSArray *timesIt = [[sender valueForKey:@"Times"] componentsSeparatedByString:@","];
        
        for (NSInteger i=0 ; i<[timesIt count]; i++) {
         
            NSString *time=[timesIt objectAtIndex:i];
            
            
            NSDateFormatter *format24 = [[NSDateFormatter alloc] init];
            [format24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [format24 setDateFormat:@"HH:mm"];
            
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [format setDateFormat:@"hh:mm a"];
            
            
            
            if ([format24 dateFromString:time]) {
                
                
                BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
                
                if (!strsetting)
                {
                    
                    time= [format stringFromDate:[format24 dateFromString:time]];
                }
                
                else
                {
                  
                }
                
                
            }
            else
                
            {
                BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
                
                if (!strsetting)
                {
                    
                    
                    
                    //////////////NSLog(@"YES format %@", time);
                }
                
                else
                {
                    
                    //////////////NSLog(@"NO format %@", [format24 stringFromDate:[format dateFromString:time]]);
                    // time = [format stringFromDate:[format24 dateFromString:time]];
                    
                    time= [format24 stringFromDate:[format dateFromString:time]];
                    
                    
                    // time = [format stringFromDate:[format24 dateFromString:time]];
                }
                
                
                
                
            }
            
            
            
            
            if ([NSLocalizedString([sender valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Hours",nil)]) {
                
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                
                
                NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
                
                
                
                AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                NSDateComponents *dateComponents;
                NSDateComponents *timeComponents;
                
                if  ([now compare:[sender valueForKey:@"Starting"]]==NSOrderedSame)
                    
                {
                    
                    //////NSLog(@"1");
                    //[Name setText:[dateConvert stringFromDate:now]];
                    
                    dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                    timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                    
                    
                }
                else if  ([now compare:[sender valueForKey:@"Starting"]]==NSOrderedDescending)
                    
                    
                {
                    
                    
                    ////NSLog(@"2");
                    
                    //[Name setText:[dateConvert stringFromDate:now]];
                    
                    dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                    timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                    
                }
                else {
                    //////NSLog(@"3");
                    
                    //[Name setText:[dateConvert stringFromDate:[sender valueForKey:@"Starting"]]];
                    
                    dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:[sender valueForKey:@"Starting"]],[myApp changesDone:time]]]];
                    timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:[sender valueForKey:@"Starting"]],[myApp changesDone:time]]]];
                    
                    
                }
                
                
              
                
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]
                 ];
                // Notification will fire in one minute
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:0];
                
                
                NSDate *itemDate = [calendar dateFromComponents:dateComps];
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                
                
                NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateFormatter2 setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
                
                
                
                
                if  ([now compare:itemDate]==NSOrderedSame)
                    
                {
                    
                    
                }
                
                else if  ([now compare:itemDate]==NSOrderedAscending)
                    
                {
                    NSDateComponents* comp = [calendar components:NSCalendarUnitWeekday fromDate:itemDate];
                    
                    ////////////////////////////////////////////////NSLog(@"%@", [dateFormatter2 stringFromDate:itemDate]);
                    
                    [self FireAtwill:sender setDate:[self CalculateDateOnDay:[dateFormatter2 stringFromDate:itemDate] StartingDate:[NSString stringWithFormat:@"%ld", (long)[comp weekday]]]];
                    
                    
                }
                else if  ([now compare:itemDate]==NSOrderedDescending)
                    
                {
                    NSDateComponents* comp = [calendar components:NSCalendarUnitWeekday fromDate:itemDate];
                    
                    
                    
                    if ([comp weekday]+1==8) {
                        [self FireAtwill:sender setDate:[self CalculateDateOnDay:[dateFormatter2 stringFromDate:itemDate] StartingDate:[NSString stringWithFormat:@"%i", 1]]];
                    }
                    else
                    {
                        [self FireAtwill:sender setDate:[self CalculateDateOnDay:[dateFormatter2 stringFromDate:itemDate] StartingDate:[NSString stringWithFormat:@"%ld", (long)[comp weekday]+1]]];
                        
                    }
                }
                
               
            }
            else if ([NSLocalizedString([sender valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
                
                
                AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                
                for (int s=0 ; s<4; s++) {
                    
                    
                    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                    
                    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                    
                    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                    [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
                    
                    NSDateComponents *dateComponents;
                    NSDateComponents *timeComponents;
                    
                    if  ([now compare:[sender valueForKey:@"Starting"]]==NSOrderedSame)
                        
                    {
                        
                        //////NSLog(@"1");
                        //[Name setText:[dateConvert stringFromDate:now]];
                        
                        dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                        timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                        
                        
                    }
                    else if  ([now compare:[sender valueForKey:@"Starting"]]==NSOrderedDescending)
                        
                        
                    {
                        
                        
                        dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                        timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                        
                    }
                    else {
                    
                        
                        dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:[sender valueForKey:@"Starting"]],[myApp changesDone:time]]]];
                        timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:[sender valueForKey:@"Starting"]],[myApp changesDone:time]]]];
                        
                        
                    }

                    
                    [dateComps setDay:[dateComponents day]+([[sender valueForKey:@"Frequency"] intValue]*s)];
                    [dateComps setMonth:[dateComponents month]];
                    [dateComps setYear:[dateComponents year]];
                    [dateComps setHour:[timeComponents hour]];
                    // Notification will fire in one minute
                    [dateComps setMinute:[timeComponents minute]];
                    [dateComps setSecond:0];
                    
                    
                    NSDate *itemDate = [calendar dateFromComponents:dateComps];
                    
                    
                    
                    
                    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                    [dateFormatter2 setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
                    
                    
                    NSDateComponents* comp = [calendar components:NSCalendarUnitWeekday fromDate:itemDate];
                    
                    [self FireAtwill:sender setDate:[self CalculateDateOnDay:[dateFormatter2 stringFromDate:itemDate] StartingDate:[NSString stringWithFormat:@"%ld", (long)[comp weekday]]]];
                    
                    
                }
                
                
            }
            
            else if ([NSLocalizedString([sender valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Weekly",nil)]) {
               
                
                
                NSArray *Days = [[sender valueForKey:@"Day_number"] componentsSeparatedByString:@","];
                
                for (int k=0 ; k<[Days count]; k++) {
                    
                AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                
                NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
                
                
                    NSDateComponents *dateComponents;
                    NSDateComponents *timeComponents;
                    
                    if  ([now compare:[sender valueForKey:@"Starting"]]==NSOrderedSame)
                        
                    {
                        
                        //////NSLog(@"1");
                        //[Name setText:[dateConvert stringFromDate:now]];
                        
                        dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                        timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                        
                        
                    }
                    else if  ([now compare:[sender valueForKey:@"Starting"]]==NSOrderedDescending)
                        
                        
                    {
                        
                        
                        ////NSLog(@"2");
                        
                        //[Name setText:[dateConvert stringFromDate:now]];
                        
                        dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                        timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                        
                    }
                    else {
                        //////NSLog(@"3");
                        
                        //[Name setText:[dateConvert stringFromDate:[sender valueForKey:@"Starting"]]];
                        
                        dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:[sender valueForKey:@"Starting"]],[myApp changesDone:time]]]];
                        timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:[sender valueForKey:@"Starting"]],[myApp changesDone:time]]]];
                        
                        
                    }

                
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                // Notification will fire in one minute
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:0];
                
                
                NSDate *itemDate = [calendar dateFromComponents:dateComps];
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                
                
                
                NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateFormatter2 setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
                
                
                
                [self FireAtwill:sender setDate:[self CalculateDateOnDay:[dateFormatter2 stringFromDate:itemDate] StartingDate:[NSString stringWithFormat:@"%i", [[Days objectAtIndex:k]intValue]]]];
                    
                }
                
                
                
                
                
            }
            
            else if ([NSLocalizedString([sender valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Daily",nil)])
            {
                
                
                AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                
                NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
                [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
                [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];
                
                
                
                NSDateComponents *dateComponents;
                NSDateComponents *timeComponents;
                
                if  ([now compare:[sender valueForKey:@"Starting"]]==NSOrderedSame)
                    
                {
              
                    dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                    timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                    
                    
                }
                else if  ([now compare:[sender valueForKey:@"Starting"]]==NSOrderedDescending)
                    
                    
                {
                    
                    
                    
                    dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                    timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:now],[myApp changesDone:time]]]];
                    
                }
                else {
                  
                    
                    dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:[sender valueForKey:@"Starting"]],[myApp changesDone:time]]]];
                    timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [dateConvert stringFromDate:[sender valueForKey:@"Starting"]],[myApp changesDone:time]]]];
                    
                    
                }

                
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                // Notification will fire in one minute
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:0];
                
                NSDate *itemDate = [calendar dateFromComponents:dateComps];
                
                if  ([now compare:itemDate]==NSOrderedSame)
                    
                {
                    
                }

                else if  ([now compare:itemDate]==NSOrderedAscending)
                    
                {
                    NSDateComponents* comp = [calendar components:NSCalendarUnitWeekday fromDate:itemDate];
                    
                    [self FireAtwill:sender setDate:[self CalculateDateOnDay:[dateFormatter2 stringFromDate:itemDate] StartingDate:[NSString stringWithFormat:@"%ld", (long)[comp weekday]]]];
                    
                }
                
                else if  ([now compare:itemDate]==NSOrderedDescending)
                    
                {
                    
                    
                    [dateComps setDay:numberOfDays];
                    [dateComps setMonth:[dateComponents month]];
                    [dateComps setYear:[dateComponents year]];
                    [dateComps setHour:[timeComponents hour]
                     ];
                    // Notification will fire in one minute
                    [dateComps setMinute:[timeComponents minute]];
                    [dateComps setSecond:0];
                    
                    itemDate = [calendar dateFromComponents:dateComps];
                    
                    
                    NSDateComponents* comp = [calendar components:NSCalendarUnitWeekday fromDate:itemDate];
                    
                    [self FireAtwill:sender setDate:[self CalculateDateOnDay:[dateFormatter2 stringFromDate:itemDate] StartingDate:[NSString stringWithFormat:@"%ld", (long)[comp weekday]]]];
                    
                    
                }
                
                
                
            }
        }
    
    [self CheckNotificationWithID:[sender valueForKey:@"ID_Medicine"]];
  
    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotification" afterDelay:0];
    
}

+(NSDate*)calculatedateLate:(NSDate*)date late:(NSInteger)minute

{
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:date];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:date];
    
    
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
    [dateComps setMinute:[timeComponents minute]-minute];
    
    [dateComps setSecond:0];
    
    
    
    return [calendar dateFromComponents:dateComps];
    
    
}

+(NSDate*)calculatedateEarly:(NSDate*)date late:(NSInteger)minute

{
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:date];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:date];
    
    
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    
    [dateComps setHour:[timeComponents hour]];
    [dateComps setMinute:[timeComponents minute]+minute];
    [dateComps setSecond:0];
    
    
    return [calendar dateFromComponents:dateComps];
}

+(void)FireAtwill:(NSMutableDictionary*)sender setDate:(NSDate*)date
{
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:date];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:date];
    
    
    if ([NSLocalizedString([sender valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Weekly",nil)]) {
        
        
        NSDate *now = [[NSDate alloc] init];
        
        if ([now compare:date]==NSOrderedAscending)
            
        {
            
            
            
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:0];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            
            
            [self GoNotificationWeek:itemDate set:sender];
            [self GoNotificationNext:itemDate set:sender];
            
        }
        else if ([now compare:date]==NSOrderedDescending)
            
        {
            
            
            
            [dateComps setDay:[dateComponents day]+7];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            
            
            [self GoNotificationWeek:itemDate set:sender];
            [self GoNotificationNext:itemDate set:sender];
        }
        else if ([now compare:date]==NSOrderedSame)
            
        {
            
            
            
        }
        
        
        
        
        
        
    }
    
    
    else if ([NSLocalizedString([sender valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Daily",nil)]) {
        
        
        NSDate *now = [[NSDate alloc] init];
        
        if ([now compare:date]==NSOrderedAscending)
            
        {
            
            
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            [self GoNotificationNext:itemDate set:sender];
            
            
        }
        else if ([now compare:date]==NSOrderedDescending)
            
        {
            
            [dateComps setDay:[dateComponents day]+1];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            [self GoNotificationNext:itemDate set:sender];
            
            
        }
        else if ([now compare:date]==NSOrderedSame)
            
        {
            
            
            
        }
        
        
        
    }
    
    else if ([NSLocalizedString([sender valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
        
        
        NSDate *now = [[NSDate alloc] init];
        
        if ([now compare:date]==NSOrderedAscending)
            
        {
            
            
            
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            [self GoNotificationNext:itemDate set:sender];
            
            
            
        }
        else if ([now compare:date]==NSOrderedDescending)
            
        {
            
            
            
            [dateComps setDay:[dateComponents day]+([[sender valueForKey:@"Frequency"] intValue]*4)];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            
            
            [self GoNotification:itemDate set:sender];
            [self GoNotificationNext:itemDate set:sender];
            
            
        }
        else if ([now compare:date]==NSOrderedSame)
            
        {
            
            
            
        }
        
        
        
    }
    
    else if ([NSLocalizedString([sender valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Hours",nil)]) {
        
        
        
        
        if ([[sender valueForKey:@"Frequency"] isEqualToString:@"1"]) {
            
            
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotificationHours:itemDate set:sender];
            [self GoNotificationNext:itemDate set:sender];
        }
        else
        {
            
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:[timeComponents second]];
            
            
            NSDate *itemDate = [calendar dateFromComponents:dateComps];
            [self GoNotification:itemDate set:sender];
            [self GoNotificationNext:itemDate set:sender];
            
            
        }
        
        
    }
    
    
    
}

#pragma Remaining

+(NSString*) Remaining:(NSString*)left Quantity:(NSString*)less

{
    NSString *calculated;

    NSLog(@"%@", left, less);
    
    calculated =[NSString stringWithFormat:@"%.0f", [left floatValue] -[less floatValue]];
    
    NSLog(@"floatToString %@", [self  floatToString:calculated]);
    return [self  floatToString:calculated];
}

+ (NSString *) floatToString:(NSString*) val {
    
    
    NSArray *numbers2 = [val componentsSeparatedByString:@"."];
    
    if ([numbers2 count]==2)
        
    {
        NSString *ret = [NSString stringWithFormat:@"%@", [numbers2 objectAtIndex:1]];
        NSInteger index = (int)[ret length] - 1;
        BOOL trim = FALSE;
        while (
               ([ret characterAtIndex:index] == '0' ||
                [ret characterAtIndex:index] == '.')
               &&
               index > 0)
        {
            index--;
            trim = TRUE;
        }
        if (trim)
            ret = [ret substringToIndex: index +1];
        
        if ([ret intValue]>0)
            
            
            return [NSString stringWithFormat:@"%i.%@",[[numbers2 objectAtIndex:0] intValue],ret];
        
        else
            
            return [NSString stringWithFormat:@"%i",[[numbers2 objectAtIndex:0] intValue]];
        
    }
    
    else
    {
        
        NSString *ret = [NSString stringWithFormat:@"%@", val];
        return [NSString stringWithFormat:@"%i",[ret intValue]];
    }
}


#pragma Remaining not




+(void) postTake:(NSMutableDictionary*)sender date:(NSDate*)time andNote:(UILocalNotification*)curentChoise;
{

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
   
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[UIApplication sharedApplication].applicationIconBadgeNumber-1];
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(fireDate == %@) and (userInfo.ID_Medicine =%@)", curentChoise.fireDate, [curentChoise.userInfo valueForKey:@"ID_Medicine"]];
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate2];
    
    for (int k =0; k < [notificationArray count]; k++) {
  
        
              UILocalNotification *change = [notificationArray objectAtIndex:k];
        
        
           //NSLog(@"%@", change.fireDate);
        
           [[UIApplication sharedApplication] cancelLocalNotification:change];
        
    }
    
 
    
    
    NSMutableDictionary *copyDictonary = [[NSMutableDictionary alloc] initWithDictionary:curentChoise.userInfo];
    
    
    
    [self insertforundofunction:sender];
    
    [self insertforundoExtra:sender];
    
    
    NSArray *names =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit",@"Location",@"active",nil];
    
    // [delegate loadingviewOn];
    
    [appDelegate GetProfilesWithID:[sender valueForKey:@"ID"]];
    
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:curentChoise];
    [appDelegate insertNotifications:sender withdata:data];
    
    
    NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
    [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [DaysFormat setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    
    NSDateFormatter *Time = [[NSDateFormatter alloc] init];
    [Time setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [Time setDateFormat:[appDelegate convertString:@"hh:mm a"]];
    
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
    
    
    
    
    
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [format setDateFormat:@"dd/MM/yyyy"];
    
    
    
    
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:curentChoise.fireDate];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:curentChoise.fireDate];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    
    
    NSDate *itemNext;
    
    if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Weekly",nil)]) {
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        
        itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:((24*7)*60*60) sinceDate:itemNext];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
        
    }
    else  if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:((24*([[curentChoise.userInfo valueForKey:@"Frequency"] intValue]*4))*60*60) sinceDate:itemNext];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
        
        
    }
    else  if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Hours",nil)]) {
        NSDateComponents *timeComponents2 = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:curentChoise.fireDate];
        
        NSDateComponents *dateCompsEarly2 = [[NSDateComponents alloc] init];
        
        
        if ([[curentChoise.userInfo  valueForKey:@"Frequency"] isEqualToString:@"1"]) {
            
            
            
            [dateCompsEarly2 setDay:[dateComponents day]];
            [dateCompsEarly2 setMonth:[dateComponents month]];
            [dateCompsEarly2 setYear:[dateComponents year]];
            [dateCompsEarly2 setHour:[timeComponents2 hour]+1];
            [dateCompsEarly2 setMinute:timeComponents2.minute];
            [dateCompsEarly2 setSecond:[timeComponents2 second]];
            
            itemNext = [calendar dateFromComponents:dateCompsEarly2];
            
            NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
            [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [DaysFormat setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
            
            
            
            
            
            itemNext =[dateFormatter2 dateFromString:[DaysFormat stringFromDate:itemNext]];
        }
        else
        {
            [dateCompsEarly setDay:[dateComponents day]];
            [dateCompsEarly setMonth:[dateComponents month]];
            [dateCompsEarly setYear:[dateComponents year]];
            [dateCompsEarly setHour:[timeComponents hour]];
            [dateCompsEarly setMinute:timeComponents.minute];
            [dateCompsEarly setSecond:[timeComponents second]];
            
            itemNext = [calendar dateFromComponents:dateCompsEarly];
            
            
            
            NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:itemNext];
            
            
            itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
            
        }
    }
    else  if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Daily",nil)]) {
        
        
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:itemNext];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
    }
    
    
    
    NSDate *now = [[NSDate alloc] init];
    
    
    
    
    NSMutableDictionary *item2 = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *itemMed = [[NSMutableDictionary alloc] init];
    
    for (id key in curentChoise.userInfo)
    {
        [item2 setObject:[curentChoise.userInfo valueForKey:key]  forKey:key];
        [itemMed setObject:[curentChoise.userInfo valueForKey:key]  forKey:key];
        
        
    }
    
    UIBackgroundTaskIdentifier bgTask =0;
    UIApplication  *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    
    if ([[curentChoise.userInfo valueForKey:@"Remaining"]intValue] -[[curentChoise.userInfo valueForKey:@"Quantity"]intValue]<=0) {
        
        [item2 setObject: [NSString stringWithFormat:@"%i", 0] forKey:@"Remaining"];
        [itemMed setObject: [NSString stringWithFormat:@"%i", 0] forKey:@"Remaining"];
        [copyDictonary setObject: [NSString stringWithFormat:@"%i", 0] forKey:@"Remaining"];
    }
    else
    {
        [item2 setObject:[self Remaining:[curentChoise.userInfo valueForKey:@"Remaining"] Quantity:[curentChoise.userInfo valueForKey:@"Quantity"]] forKey:@"Remaining"];
        
        
        [itemMed setObject: [self Remaining:[curentChoise.userInfo valueForKey:@"Remaining"] Quantity:[curentChoise.userInfo valueForKey:@"Quantity"]] forKey:@"Remaining"];
        
        [copyDictonary setObject: [self Remaining:[curentChoise.userInfo valueForKey:@"Remaining"] Quantity:[curentChoise.userInfo valueForKey:@"Quantity"]] forKey:@"Remaining"];
        
    }
    
    
    
    if  ([now compare:curentChoise.fireDate]==NSOrderedSame)
        
    {
        
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
        [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:curentChoise.fireDate] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
            
        }
        else
        {
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
            
        }
        
        
        
        
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Taken" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:time] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            
            for (id key in sender)
            {
                
                
                if ([key isEqualToString:@"Coloralpha"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    
                }
                if ([key isEqualToString:@"ColorRed"]) {
                    
                    [item2 setObject:@" "  forKey:@"ColorRed"];
                    [itemMed setObject:@" "  forKey:@"ColorRed"];
                    [itemMed setObject:@"Was postponed"  forKey:@"Aktion"];
                    
                }
                
                
            }
            
            
      
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
            
        }
        else
        {
            
            curentChoise.userInfo =item2;
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            
            [curentChoise setFireDate:itemNext];
            
            
            curentChoise.applicationIconBadgeNumber = [self CheckNumbers:curentChoise.fireDate];
            
            [curentChoise setUserInfo:copyDictonary];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            
            
            [self changeDictonary:copyDictonary and:curentChoise];
         
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            
     
        
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
            
            [appDelegate seeifiRunOut:item2];
            
        }
        
        

        
        

        
        
        
        
        
        
        
    }
    else if ([now compare:curentChoise.fireDate]==NSOrderedAscending)
        
        
    {
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
        [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:curentChoise.fireDate] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
        }
        else
        {
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
        }
        
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Taken earlier" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:time] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            
            for (id key in sender)
            {
                
                
                if ([key isEqualToString:@"Coloralpha"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:@"Aktion"];
                    
                }
                if ([key isEqualToString:@"ColorRed"]) {
                    
                    [item2 setObject:@" "  forKey:@"ColorRed"];
                    [itemMed setObject:@" "  forKey:@"ColorRed"];
                    [itemMed setObject:@"Was postponed"  forKey:@"Aktion"];
                }
                
                
            }
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            
            [SaveCore insertHistorySQL:item];
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
            
            
        }
        else
        {
            
            curentChoise.userInfo =item2;
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:curentChoise.fireDate withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            
            
            
            [curentChoise setUserInfo:copyDictonary];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            
            
            [self changeDictonary:copyDictonary and:curentChoise];
            
    
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            
            [SaveCore insertHistorySQL:item];
            
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
            
            [appDelegate seeifiRunOut:item2];
            
        }
        
        

        

        
        
        
    }
    else if ([now compare:curentChoise.fireDate]==NSOrderedDescending)
        
        
    {
        
        //////NSLog(@"NSOrderedDescending");
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
        [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:curentChoise.fireDate] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
        }
        else
        {
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
        }
        
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Taken late" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:time] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            
            for (id key in sender)
            {
                
                
                if ([key isEqualToString:@"Coloralpha"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:@"Aktion"];
                    
                }
                if ([key isEqualToString:@"ColorRed"]) {
                    
                    [item2 setObject:@" "  forKey:@"ColorRed"];
                    [itemMed setObject:@" "  forKey:@"ColorRed"];
                }
                
                
            }
            
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:[dateFormatLevel dateFromString:[curentChoise.userInfo valueForKey:@"Coloralpha"]]]];
            [curentChoise setFireDate:[dateFormatLevel dateFromString:[curentChoise.userInfo valueForKey:@"Coloralpha"]]];
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
        }
        else
        {
            
            
            curentChoise.userInfo =item2;
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            
            
            
            
            
            [curentChoise setUserInfo:copyDictonary];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            
            
            [self changeDictonary:copyDictonary and:curentChoise];
            
         
           
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            
         
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
                    [appDelegate seeifiRunOut:item2];
            
        }
        
        
   

        
        
        
        
        
    }
    
    
    
  
    
    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotification" afterDelay:0];
    
    [self CheckNotificationWithID:[sender valueForKey:@"ID_Medicine"]];
    
    
    
}

+(void) postSkip:(NSMutableDictionary*)sender andNote:(UILocalNotification*)curentChoise
{
    
    
     //NSLog(@"postSkip %@", curentChoise);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[UIApplication sharedApplication].applicationIconBadgeNumber-1];
    
   
      NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(fireDate == %@) and (userInfo.ID_Medicine =%@)", curentChoise.fireDate, [curentChoise.userInfo valueForKey:@"ID_Medicine"]];
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate2];
    
    for (int k =0; k < [notificationArray count]; k++) {
        
        
        UILocalNotification *change =[notificationArray objectAtIndex:k];
        
        //NSLog(@"%@", change.fireDate);
        
        [[UIApplication sharedApplication] cancelLocalNotification:change];
        
    }
    
    
    
       [self insertforundofunction:sender];
    
       [self insertforundoExtra:sender];
    
    
    NSArray *names =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit",@"Location",@"active",nil];
    
    // [delegate loadingviewOn];
    
    [appDelegate GetProfilesWithID:[sender valueForKey:@"ID"]];
 
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:curentChoise];
    [appDelegate insertNotifications:sender withdata:data];
    
    
    NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
    [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [DaysFormat setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    
    NSDateFormatter *Time = [[NSDateFormatter alloc] init];
    [Time setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [Time setDateFormat:[appDelegate convertString:@"hh:mm a"]];
    
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
    
    
    
    
    
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [format setDateFormat:@"dd/MM/yyyy"];
    
    
    
    
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:curentChoise.fireDate];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:curentChoise.fireDate];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    
    
    NSDate *itemNext;
    
    if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Weekly",nil)]) {
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        
        itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:((24*7)*60*60) sinceDate:itemNext];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
        
    }
    else  if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:((24*([[curentChoise.userInfo valueForKey:@"Frequency"] intValue]*4))*60*60) sinceDate:itemNext];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
        
        
    }
    else  if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Hours",nil)]) {
        NSDateComponents *timeComponents2 = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:curentChoise.fireDate];
        
        NSDateComponents *dateCompsEarly2 = [[NSDateComponents alloc] init];
        
        
        if ([[curentChoise.userInfo  valueForKey:@"Frequency"] isEqualToString:@"1"]) {
            
            
            
            [dateCompsEarly2 setDay:[dateComponents day]];
            [dateCompsEarly2 setMonth:[dateComponents month]];
            [dateCompsEarly2 setYear:[dateComponents year]];
            [dateCompsEarly2 setHour:[timeComponents2 hour]+1];
            [dateCompsEarly2 setMinute:timeComponents2.minute];
            [dateCompsEarly2 setSecond:[timeComponents2 second]];
            
            itemNext = [calendar dateFromComponents:dateCompsEarly2];
            
            NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
            [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [DaysFormat setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
            
            
            
            
            
            itemNext =[dateFormatter2 dateFromString:[DaysFormat stringFromDate:itemNext]];
        }
        else
        {
            [dateCompsEarly setDay:[dateComponents day]];
            [dateCompsEarly setMonth:[dateComponents month]];
            [dateCompsEarly setYear:[dateComponents year]];
            [dateCompsEarly setHour:[timeComponents hour]];
            [dateCompsEarly setMinute:timeComponents.minute];
            [dateCompsEarly setSecond:[timeComponents second]];
            
            itemNext = [calendar dateFromComponents:dateCompsEarly];
            
            
            
            NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:itemNext];
            
            
            itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
            
        }
    }
    else  if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Daily",nil)]) {
        
        
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:itemNext];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
    }
    
    
    
    NSDate *now = [[NSDate alloc] init];
    
    
    
    
    NSMutableDictionary *item2 = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *itemMed = [[NSMutableDictionary alloc] init];
    
    for (id key in curentChoise.userInfo)
    {
        [item2 setObject:[curentChoise.userInfo valueForKey:key]  forKey:key];
        [itemMed setObject:[curentChoise.userInfo valueForKey:key]  forKey:key];
        
        
    }
    
    UIBackgroundTaskIdentifier bgTask =0;
    UIApplication  *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    
    
    

    
    
    if  ([now compare:curentChoise.fireDate]==NSOrderedSame)
        
    {
        
        
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
        [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:now] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
        }
        else
        {
            
            
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
        }
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Skipped" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:now] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            
            for (id key in sender)
            {
                
                if ([key isEqualToString:@"Coloralpha"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:@"Aktion"];
                    
                }
                if ([key isEqualToString:@"ColorRed"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    
                }
                
            }
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
        }
        else
        {
            
            
            
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
            
        }
        
        
        
        
        
        
        
        
        
    }
    else if ([now compare:curentChoise.fireDate]==NSOrderedAscending)
        
        
    {
        
        
        
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
        [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:now] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
        }
        else
        {
            
            
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
        }
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Skipped" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:now] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            
            for (id key in sender)
            {
                
                
                if ([key isEqualToString:@"Coloralpha"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:@"Aktion"];
                    
                }
                if ([key isEqualToString:@"ColorRed"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    
                }
                
            }
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
            
            
        }
        else
        {
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            
            [SaveCore insertHistorySQL:item];
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
        }
        
        
        
    }
    else if ([now compare:curentChoise.fireDate]==NSOrderedDescending)
        
        
    {
        
        
        
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
        [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:now] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
        }
        else
        {
            
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
        }
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Skipped" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:now] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            
            for (id key in sender)
            {
                
                
                if ([key isEqualToString:@"Coloralpha"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:@"Aktion"];
                    
                }
                if ([key isEqualToString:@"ColorRed"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    
                }
                
            }
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
        }
        else
        {
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
        }
        
        
        
    }
    
    

    
    
     [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotification" afterDelay:0];
    
    [self CheckNotificationWithID:[sender valueForKey:@"ID_Medicine"]];
    
    
    
}



+(void) postTakeEarly:(NSMutableDictionary*)sender date:(NSDate*)time andNote:(UILocalNotification*)curentChoise;
{
    
    
     //NSLog(@"postTakeEarly %@", curentChoise);
 
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
     NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(fireDate == %@) and (userInfo.ID_Medicine =%@)", curentChoise.fireDate, [curentChoise.userInfo valueForKey:@"ID_Medicine"]];
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate2];
    
    for (int k =0; k < [notificationArray count]; k++) {
        
        
        UILocalNotification *change =[notificationArray objectAtIndex:k];
        
            //NSLog(@"%@", change.fireDate);
        
        [[UIApplication sharedApplication] cancelLocalNotification:change];
        
    }
    
    
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[UIApplication sharedApplication].applicationIconBadgeNumber-1];
    
    
    
    [[UIApplication sharedApplication] cancelLocalNotification:curentChoise];
    
    
    NSMutableDictionary *copyDictonary = [[NSMutableDictionary alloc] initWithDictionary:curentChoise.userInfo];
    
    
    
    [self insertforundofunction:sender];
    
    [self insertforundoExtra:sender];
    
    
    NSArray *names =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit",@"Location",@"active",nil];
    
    // [delegate loadingviewOn];
    
    [appDelegate GetProfilesWithID:[sender valueForKey:@"ID"]];
    

    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:curentChoise];
    [appDelegate insertNotifications:sender withdata:data];
    
    
    NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
    [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [DaysFormat setDateFormat:@"EEEE dd, MMMM yyyy"];
    
    
    
    
    NSDateFormatter *Time = [[NSDateFormatter alloc] init];
    [Time setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [Time setDateFormat:[appDelegate convertString:@"hh:mm a"]];
    
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
    
    
    
    
    
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [format setDateFormat:@"dd/MM/yyyy"];
    
    
    
    
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:curentChoise.fireDate];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:curentChoise.fireDate];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    
    
    NSDate *itemNext;
    
    if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Weekly",nil)]) {
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        
        itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:((24*7)*60*60) sinceDate:itemNext];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
        
    }
    else  if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:((24*([[curentChoise.userInfo valueForKey:@"Frequency"] intValue]*4))*60*60) sinceDate:itemNext];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
        
        
    }
    else  if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Hours",nil)]) {
        NSDateComponents *timeComponents2 = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:curentChoise.fireDate];
        
        NSDateComponents *dateCompsEarly2 = [[NSDateComponents alloc] init];
        
        
        if ([[curentChoise.userInfo  valueForKey:@"Frequency"] isEqualToString:@"1"]) {
            
            
            
            [dateCompsEarly2 setDay:[dateComponents day]];
            [dateCompsEarly2 setMonth:[dateComponents month]];
            [dateCompsEarly2 setYear:[dateComponents year]];
            [dateCompsEarly2 setHour:[timeComponents2 hour]+1];
            [dateCompsEarly2 setMinute:timeComponents2.minute];
            [dateCompsEarly2 setSecond:[timeComponents2 second]];
            
            itemNext = [calendar dateFromComponents:dateCompsEarly2];
            
            NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
            [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [DaysFormat setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
            
            
            
            
            
            itemNext =[dateFormatter2 dateFromString:[DaysFormat stringFromDate:itemNext]];
        }
        else
        {
            [dateCompsEarly setDay:[dateComponents day]];
            [dateCompsEarly setMonth:[dateComponents month]];
            [dateCompsEarly setYear:[dateComponents year]];
            [dateCompsEarly setHour:[timeComponents hour]];
            [dateCompsEarly setMinute:timeComponents.minute];
            [dateCompsEarly setSecond:[timeComponents second]];
            
            itemNext = [calendar dateFromComponents:dateCompsEarly];
            
            
            
            NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:itemNext];
            
            
            itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
            
        }
    }
    else  if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Daily",nil)]) {
        
        
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        itemNext = [calendar dateFromComponents:dateCompsEarly];
        
        
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:itemNext];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
    }
    
    
    
    
    
    NSMutableDictionary *item2 = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *itemMed = [[NSMutableDictionary alloc] init];
    
    for (id key in curentChoise.userInfo)
    {
        [item2 setObject:[curentChoise.userInfo valueForKey:key]  forKey:key];
        [itemMed setObject:[curentChoise.userInfo valueForKey:key]  forKey:key];
        
        
    }
    
    UIBackgroundTaskIdentifier bgTask =0;
    UIApplication  *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    
    
    if ([[curentChoise.userInfo valueForKey:@"Remaining"]intValue] -[[curentChoise.userInfo valueForKey:@"Quantity"]intValue]<=0) {
        
        [item2 setObject: [NSString stringWithFormat:@"%i", 0] forKey:@"Remaining"];
        [itemMed setObject: [NSString stringWithFormat:@"%i", 0] forKey:@"Remaining"];
        [copyDictonary setObject: [NSString stringWithFormat:@"%i", 0] forKey:@"Remaining"];
    }
    else
    {
        [item2 setObject:[self Remaining:[curentChoise.userInfo valueForKey:@"Remaining"] Quantity:[curentChoise.userInfo valueForKey:@"Quantity"]] forKey:@"Remaining"];
        
        
        [itemMed setObject: [self Remaining:[curentChoise.userInfo valueForKey:@"Remaining"] Quantity:[curentChoise.userInfo valueForKey:@"Quantity"]] forKey:@"Remaining"];
        
        [copyDictonary setObject: [self Remaining:[curentChoise.userInfo valueForKey:@"Remaining"] Quantity:[curentChoise.userInfo valueForKey:@"Quantity"]] forKey:@"Remaining"];
        
    }
    
    
    
    
    if  ([time compare:curentChoise.fireDate]==NSOrderedSame)
        
    {
        
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
        [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:curentChoise.fireDate] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
            
        }
        else
        {
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
            
        }
        
        
        
        
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Taken" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:time] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            
            for (id key in sender)
            {
                
                
                if ([key isEqualToString:@"Coloralpha"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    
                }
                if ([key isEqualToString:@"ColorRed"]) {
                    
                    [item2 setObject:@" "  forKey:@"ColorRed"];
                    [itemMed setObject:@" "  forKey:@"ColorRed"];
                    [itemMed setObject:@"Was postponed"  forKey:@"Aktion"];
                    
                }
                
                
            }
            
            
            
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            
            
            [SaveCore insertHistorySQL:item];
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
            
        }
        else
        {
            
            curentChoise.userInfo =item2;
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            
            [curentChoise setFireDate:itemNext];
            
            
            curentChoise.applicationIconBadgeNumber = [self CheckNumbers:curentChoise.fireDate];
            
            [curentChoise setUserInfo:copyDictonary];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            
            
            [self changeDictonary:copyDictonary and:curentChoise];
            
              [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            
    
            
            
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
                    [appDelegate seeifiRunOut:item2];
            
        }
        
        
        
      
        
        
        
        
        
        
        
    }
    else if ([time compare:curentChoise.fireDate]==NSOrderedAscending)
        
        
    {
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
        [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:curentChoise.fireDate] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
        }
        else
        {
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
        }
        
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Taken earlier" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:time] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            
            for (id key in sender)
            {
                
                
                if ([key isEqualToString:@"Coloralpha"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:@"Aktion"];
                    
                }
                if ([key isEqualToString:@"ColorRed"]) {
                    
                    [item2 setObject:@" "  forKey:@"ColorRed"];
                    [itemMed setObject:@" "  forKey:@"ColorRed"];
                    [itemMed setObject:@"Was postponed"  forKey:@"Aktion"];
                }
                
                
            }
            
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
            
            
        }
        else
        {
            
            curentChoise.userInfo =item2;
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:curentChoise.fireDate withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            
            
            
            [curentChoise setUserInfo:copyDictonary];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            
            
            [self changeDictonary:copyDictonary and:curentChoise];
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
         
            [SaveCore insertHistorySQL:item];
            
     
            
            
            
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
                   [appDelegate seeifiRunOut:item2];
            
        }
        
        
    
      
       
    }
    else if ([time compare:curentChoise.fireDate]==NSOrderedDescending)
        
        
    {
        
        NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
        [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
        [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:curentChoise.fireDate] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
        }
        else
        {
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
        }
        
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Taken late" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:time] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            
            for (id key in sender)
            {
                
                
                if ([key isEqualToString:@"Coloralpha"]) {
                    
                    [item2 setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:key];
                    [itemMed setObject:@" "  forKey:@"Aktion"];
                    
                }
                if ([key isEqualToString:@"ColorRed"]) {
                    
                    [item2 setObject:@" "  forKey:@"ColorRed"];
                    [itemMed setObject:@" "  forKey:@"ColorRed"];
                }
                
                
            }
            
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:[dateFormatLevel dateFromString:[curentChoise.userInfo valueForKey:@"Coloralpha"]]]];
            [curentChoise setFireDate:[dateFormatLevel dateFromString:[curentChoise.userInfo valueForKey:@"Coloralpha"]]];
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
        }
        else
        {
            
            
            curentChoise.userInfo =item2;
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            
            
            
            
            
            [curentChoise setUserInfo:copyDictonary];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            
            
            [self changeDictonary:copyDictonary and:curentChoise];
           
            
               [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
            
            [SaveCore insertHistorySQL:item];
            
          
            
            
        
            
            [SaveCore insertMedecineSQL:item2 setnames:names];
            
            
              [appDelegate seeifiRunOut:item2];
            
        }
        
        
      
        
        
        
    }
    
    
 
    
    
    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotification" afterDelay:0];
    
    [self CheckNotificationWithID:[sender valueForKey:@"ID_Medicine"]];
    
    
    
}



+(void)postPone:(NSMutableDictionary*)sender date:(NSDate*)time andNote:(UILocalNotification*)curentChoise;

{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[UIApplication sharedApplication].applicationIconBadgeNumber-1];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    

    [self insertforundofunction:sender];
    
    [self insertforundoExtra:sender];
    
 
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:curentChoise];
    [appDelegate insertNotifications:sender withdata:data];
    
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatter2 setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
    
    
    
    
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [format setDateFormat:@"dd/MM/yyyy"];
    
    
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:time];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:time];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    
    
    NSDate *itemNext;
    
    if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Weekly",nil)]) {
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        
        NSDate *Next = [calendar dateFromComponents:dateCompsEarly];
        
        NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
        [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [DaysFormat setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        
        
        
        NSDateFormatter *Time = [[NSDateFormatter alloc] init];
        [Time setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [Time setDateFormat:[appDelegate convertString:@"hh:mm a"]];
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:((24*7)*60*60) sinceDate:Next];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
        
        
        
    }
    else  if ([NSLocalizedString([curentChoise.userInfo valueForKey:@"Frequency_text"],nil) isEqualToString:NSLocalizedString(@"Every X Days",nil)]) {
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:[timeComponents second]];
        
        NSDate *Next = [calendar dateFromComponents:dateCompsEarly];
        
        NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
        [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [DaysFormat setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        
        NSDateFormatter *Time = [[NSDateFormatter alloc] init];
        [Time setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [Time setDateFormat:[appDelegate convertString:@"hh:mm a"]];
        
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:((24*([[curentChoise.userInfo valueForKey:@"Frequency"] intValue]*4))*60*60) sinceDate:Next];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
    }
    else  if ([[curentChoise.userInfo valueForKey:@"Frequency_text"] isEqualToString:NSLocalizedString(@"Every X Hours",nil)]) {
        
        NSDateComponents *timeComponents2 = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:curentChoise.fireDate];
        
        NSDateComponents *dateCompsEarly2 = [[NSDateComponents alloc] init];
        
        
        if ([[curentChoise.userInfo  valueForKey:@"Frequency"] isEqualToString:@"1"]) {
            
            
            
            [dateCompsEarly2 setDay:[dateComponents day]];
            [dateCompsEarly2 setMonth:[dateComponents month]];
            [dateCompsEarly2 setYear:[dateComponents year]];
            [dateCompsEarly2 setHour:[timeComponents2 hour]+1];
            [dateCompsEarly2 setMinute:timeComponents2.minute];
            [dateCompsEarly2 setSecond:[timeComponents2 second]];
            
            itemNext = [calendar dateFromComponents:dateCompsEarly2];
            
            NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
            [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [DaysFormat setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
            
            itemNext =[dateFormatter2 dateFromString:[DaysFormat stringFromDate:itemNext]];
        }
        else
        {
            [dateCompsEarly setDay:[dateComponents day]];
            [dateCompsEarly setMonth:[dateComponents month]];
            [dateCompsEarly setYear:[dateComponents year]];
            [dateCompsEarly setHour:[timeComponents hour]];
            [dateCompsEarly setMinute:timeComponents.minute];
            [dateCompsEarly setSecond:0];
            
            itemNext = [calendar dateFromComponents:dateCompsEarly];
            
            NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
            [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [DaysFormat setDateFormat:@"EEEE dd, MMMM yyyy"];
            
            
            
            
            NSDateFormatter *Time = [[NSDateFormatter alloc] init];
            [Time setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
            [Time setDateFormat:[appDelegate convertString:@"hh:mm a"]];
            
            
            NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:itemNext];
            
            
            itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
            
        }
    }
    else  if ([[curentChoise.userInfo valueForKey:@"Frequency_text"] isEqualToString:NSLocalizedString(@"Daily",nil)]) {
        
        
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute];
        [dateCompsEarly setSecond:0];
        
        NSDate *Next = [calendar dateFromComponents:dateCompsEarly];
        
        NSDateFormatter *DaysFormat = [[NSDateFormatter alloc] init];
        [DaysFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [DaysFormat setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        
        
        
        NSDateFormatter *Time = [[NSDateFormatter alloc] init];
        [Time setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [Time setDateFormat:[appDelegate convertString:@"hh:mm a"]];
        
        
        NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:Next];
        
        
        itemNext =[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@", [DaysFormat stringFromDate:tomorrow], [Time stringFromDate:curentChoise.fireDate]]];
    }
    
    
    
    NSDate *now = [[NSDate alloc] init];
    
    
    NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
    [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
    
    
    NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
    [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
    
    
    NSMutableDictionary *item2 = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *itemMed = [[NSMutableDictionary alloc] init];
    
    for (id key in sender)
    {
        
        
        if ([key isEqualToString:@"Coloralpha"]) {
            
            [item2 setObject:@"Postponed"  forKey:key];
            [itemMed setObject:@"Postponed"  forKey:key];
            
        }
        else if ([key isEqualToString:@"ColorRed"]) {
            
            if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
                
                [item2 setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate]  forKey:key];
                [itemMed setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate]   forKey:key];
            }
            else
            {
                [item2 setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:key];
                [itemMed setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]   forKey:key];
                
                
                
            }
            
        }
        
        else
        {
            
            [item2 setObject:[sender valueForKey:key]  forKey:key];
            [itemMed setObject:[sender valueForKey:key]  forKey:key];
            
        }
        
        
    }
    
    UIBackgroundTaskIdentifier bgTask =0;
    UIApplication  *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    
    
    [[UIApplication sharedApplication] cancelLocalNotification:curentChoise];
    
    
    if  ([now compare:curentChoise.fireDate]==NSOrderedSame)
        
    {
        
        
        
        
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            [self GoNotificationExtra:time set:itemMed andNote:curentChoise];
        }
        
        
        else
        {
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            [self GoNotificationExtra:time set:itemMed andNote:curentChoise];
        }
        
        NSDateFormatter *dateFormatLevel24 = [[NSDateFormatter alloc] init];
        [dateFormatLevel24 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [dateFormatLevel24 setDateFormat:[appDelegate convertString:@"dd/MM/yyyy hh:mm a"]];
        
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:time] forKey:@"Date"];
        
        
        
        
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
        }
        else
        {
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
        }
        
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Postponed" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:time] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
           [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
        
        [SaveCore insertHistorySQL:item];
        
        
    }
    else if ([now compare:curentChoise.fireDate]==NSOrderedAscending)
        
        
    {
        
        
        
        
        
        
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:time] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
        }
        else
        {
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
        }
        
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Postponed" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:time] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            [self GoNotificationExtra:time set:itemMed andNote:curentChoise];
        }
        else
        {
            
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            [self GoNotificationExtra:time set:itemMed andNote:curentChoise];
        }
        
           [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
        
        [SaveCore insertHistorySQL:item];
        
        
    }
    else if ([now compare:curentChoise.fireDate]==NSOrderedDescending)
        
        
    {
        
        
        
        
        
        
        
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[sender valueForKey:@"ID"] forKey: @"ID"];
        [item  setObject:[format stringFromDate:time] forKey:@"Date"];
        if ([[curentChoise.userInfo valueForKey:@"ColorRed"] length]<4) {
            
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Time"];
            [item  setObject:[dateFormatLevel stringFromDate:curentChoise.fireDate] forKey:@"Original_Time"];
        }
        else
        {
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"]  forKey:@"Time"];
            [item  setObject:[curentChoise.userInfo valueForKey:@"ColorRed"] forKey:@"Original_Time"];
        }
        
        [item  setObject:[sender valueForKey:@"Name"] forKey:@"Name"];
        [item  setObject:[NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"Strength"],[sender valueForKey:@"Strength_unit"]] forKey:@"Dose"];
        [item  setObject:@"Postponed" forKey:@"Taken"];
        [item  setObject:[sender valueForKey:@"ID_Medicine"] forKey:@"ID_medicine"];
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Count"];
        [item  setObject:[dateFormatLevel stringFromDate:time] forKey:@"Time_Action"];
        [item  setObject:[dateFormatLevel24 stringFromDate:curentChoise.fireDate] forKey:@"Time_Action_24"];
        if ([[sender valueForKey:@"Location"] isEqualToString:@" "]) {
            
            [item  setObject:[sender valueForKey:@"Instructions"] forKey:@"Action"];
        }
        else
        {
            [item  setObject:[NSString stringWithFormat:@"%@, %@",[sender valueForKey:@"Location"],[sender valueForKey:@"Instructions"]] forKey:@"Action"];
        }
        [item  setObject:[sender valueForKey:@"Quantity"] forKey:@"Quantity"];
        
        if ([[sender valueForKey:@"Dose_unit"] isEqualToString:@"piece"]) {
            
            [item  setObject:[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]] forKey:@"Type"];
            
        }
        else
            
            
        {
            [item  setObject:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"Dose_unit"],[Notification SetNumber:[sender valueForKey:@"Type"] count:[sender valueForKey:@"Quantity"]]] forKey:@"Type"];
        }
        
        
        
        if ([[curentChoise.userInfo valueForKey:@"Coloralpha"] isEqualToString:@"Postponed"]) {
            
            
            [item  setObject:@"Postponed" forKey: @"Extra"];
            
            
            
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            [self GoNotificationExtra:time set:itemMed andNote:curentChoise];
        }
        else
        {
            
            [curentChoise setApplicationIconBadgeNumber:[self CheckNumbers:itemNext withID:[sender valueForKey:@"ID_Medicine"]]];
            [curentChoise setFireDate:itemNext];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:curentChoise];
            [self GoNotificationExtra:time set:itemMed andNote:curentChoise];
        }
        
           [item  setObject:[appDelegate newUUID] forKey:@"sendid"];
        
        [SaveCore insertHistorySQL:item];
        
        
    }
    
    
    
    
    
    
    
    
    //[appDelegate reloadTableview:@"GoNotification"];
    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotification" afterDelay:0.1];
    
    [self CheckNotificationWithID:[sender valueForKey:@"ID_Medicine"]];
    
}






+(NSString*)CalculateNumberleft:(NSString*)set onID:(NSString*)IDit

{
    
    NSInteger Quantity = [[[NSUserDefaults standardUserDefaults] valueForKey:IDit] intValue];
    NSInteger Dose = [set intValue];
    
    
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%li", Quantity-Dose] forKey:IDit];
    
    
    return [[NSUserDefaults standardUserDefaults] valueForKey:IDit];
    
}

+(void)changeDictonary:(NSMutableDictionary*)set and:(UILocalNotification*)notid

{
   ////NSLog(@"%@", notid.userInfo);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", [set valueForKey:@"ID_Medicine"]];
    
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        
        
       
        
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        
       
            
            [[UIApplication sharedApplication] cancelLocalNotification:notif];
            
           
            [notif setUserInfo:set];
            
           
            
            [[UIApplication sharedApplication] scheduleLocalNotification:notif];
            
            
           ////NSLog(@"changeDictonary %@",  notif.userInfo);
       
        
    }
    
    
}


+(void)changeDictonary:(NSMutableDictionary*)set

{
    
        ////NSLog(@"changeDictonary");
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", [set valueForKey:@"ID_Medicine"]];
    
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        [[UIApplication sharedApplication] cancelLocalNotification:notif];
      
        
        [notif setUserInfo:set];
       
        
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
        
        
    }
    
    
}





+(NSString*)SetNumber:(NSString*)type count:(NSString*)dose
{
    
    NSString *backstring;
    
    
    if ([[[[GetData getDrugTypeStrength:type] valueForKey:@"Drug_less"] objectAtIndex:0] isEqualToString:@"Other"])
    {
        
        
        
        NSString *Drug_type =type;
        
        NSString *Drug_types =type;
        
        
        if ([dose intValue]>1) {
            
            backstring =Drug_types;
            
            
        }
        else
        {
            backstring =Drug_type;
        }
        
    }
    
    else
    {
        NSString *Drug_type = [[[GetData getDrugTypeStrength:type] valueForKey:@"Drug_less"] objectAtIndex:0];
        
        NSString *Drug_types = [[[GetData getDrugTypeStrength:type] valueForKey:@"Drug_types"] objectAtIndex:0];
        
        
        if ([dose intValue]>1) {
            
            backstring =Drug_types;
            
            
        }
        else
        {
            backstring =Drug_type;
        }
    }
    
    
    
    
    
    return backstring;
    
}



+(void)undowhatYoudid:(NSMutableDictionary*)sender

{
    
}



+(void)CheckNotificationWithID:(NSString*)set

{
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", set];
    
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
        [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [setdata setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
        
        
        
       //NSLog(@"%@", [setdata stringFromDate:notif.fireDate]);
      
        
    }
    
    
    
}

+(NSInteger)CheckNumbers:(NSDate*)sender withID:(NSString*)ID

{
   ////NSLog(@"CheckNumbers %@ %@", sender, ID);
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(fireDate == %@)", sender];
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate2];
    
      ////NSLog(@"%lu", [notificationArray count]+1);
    
    return [notificationArray count]+1;
    
}


+(NSInteger)CheckNumbers:(NSDate*)sender

{
    
   
    NSArray *notificationArray= [[UIApplication sharedApplication] scheduledLocalNotifications];
    
   ////NSLog(@"%lu", [notificationArray count]+1);
    
    return [notificationArray count]+1;
    
}



+(void)CheckNotification

{
    
    ////////////////////////////////////////////////////NSLog(@"CheckNotification");
    
    for (NSInteger i =[[[UIApplication sharedApplication] scheduledLocalNotifications] count]; i > 0; i--) {
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
        [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [setdata setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
        
        
        
        
    }
    
}


+(void)RemoveNotification:(NSString*)sender{
    
    
    
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", sender];
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    
    for (NSInteger i =[notificationArray count]-1; i >= 0; i--) {
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        
        [[UIApplication sharedApplication] cancelLocalNotification:notif];
       ////NSLog(@"weg %@", notif.fireDate);
        
        
    }
    
    
    
    //[self CheckNotificationWithID:sender];
}

+(void)SleepNotification:(NSString*)sender{
    
    
    
    
}

+(void)SilenceNotification:(NSString*)sender{
    
    
    ////////////////////////////NSLog(@"%@", sender);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Profiles *setcours = [appDelegate GetProfilesWithID:sender];
    
    
    ////////////////////////////NSLog(@"%@", setcours);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID = %@", sender];
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        
        ////////////////////////////NSLog(@"%@", [notif.userInfo valueForKey:@"ID"]);
        
        if ([setcours.silence isEqualToString:@"YES"]) {
            
            notif.soundName = @"Leeg.mp3";
            notif.applicationIconBadgeNumber =  [self CheckNumbers:notif.fireDate withID:sender];
            
            
            
            ////////////////////////////NSLog(@"%@", notif.soundName);
        }
        else if ([setcours.silence isEqualToString:@"NO"])
        {
            
            notif.soundName = [NSString stringWithFormat:@"%@.mp3",setcours.audio];
            notif.applicationIconBadgeNumber =  [self CheckNumbers:notif.fireDate withID:sender];
            
            
            ////////////////////////////NSLog(@"%@", notif.soundName);
        }
        
        
    }
    
}


+(void)ActivateNotification:(NSString*)set active:(NSString*)doornot{
    
    
   ////NSLog(@"ActivateNotification %@", set);
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id_medicine = %@", set];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Medicine" inManagedObjectContext:[appDelegate managedObjectContext]]];
    [fetchRequest setPredicate:predicate];
    
    
    
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    ////////////////////////////////////////////NSLog(@"fetchedObjects %@", fetchedObjects);
    
    for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        Medicine *newCourse = [fetchedObjects objectAtIndex:i];
        
        if ([newCourse.active isEqualToString:@"NO"]) {
            
        }
        else
            
        {
            newCourse.active =doornot;
            
            ////NSLog(@"newCourse %@", newCourse);
            
        }
        
        
        
    }
    
    
}



+(void)RenewNotification:(NSString*)sender info:(NSMutableDictionary*)set{
    
    ////////////////////////////////////////////////////NSLog(@"RenewNotification");
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", [set valueForKey:@"ID_Medicine"]];
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        
        
        [notif setUserInfo:set];
        
    }
    
}

+(void)CleareNotification{
    
    //////////////////NSLog(@"CleareNotification 2");
    
    
    UIApplication *app = [UIApplication sharedApplication];
    
    for (UILocalNotification *localNotification in app.scheduledLocalNotifications)
    {
        [app cancelLocalNotification:localNotification];
        [UIApplication sharedApplication].applicationIconBadgeNumber=0;
        
       ////NSLog(@"CleareNotification %@", localNotification);
    }
    
}

  


+(void)GoNotificationHours:(NSDate*)itemDate set:(NSMutableDictionary*)sender
{
    
    
       ////NSLog(@"GoNotificationHours");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Profiles *setcours = [appDelegate GetProfilesWithID:[sender valueForKey:@"ID"]];
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
      
        
        
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        if ([[sender valueForKey:@"Message"] isEqualToString:@" "]) {
            [notif setAlertBody:[NSString stringWithFormat:NSLocalizedString(@"Take your %@!",nil), [sender valueForKey:@"Name"]]];
        }
        else
        {
            [notif setAlertBody:[sender valueForKey:@"Message"]];
            
        }
        notif.alertAction = @"Show?";
        
        if (setcours.audio) {
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = [NSString stringWithFormat:@"%@.mp3",setcours.audio];
            }
        }
        else
        {
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = @"twinkle.mp3";
            }
        }
        
        notif.hasAction = YES;
        
        
        notif.repeatInterval = NSCalendarUnitHour;
        notif.applicationIconBadgeNumber =  [self CheckNumbers:notif.fireDate withID:[sender valueForKey:@"ID_Medicine"]];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
        
        
        
        
        
    }

    
    //[appDelegate reloadTableview:@"GoNotificationHours"];
    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotificationHours" afterDelay:0];
    
    
    
}


+(void)GoNotificationWith:(NSDate*)itemDate set:(NSMutableDictionary*)sender andRepeatInterval:(NSCalendarUnit)CalUnit
{
    
    
        ////NSLog(@"GoNotificationWith");
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(userInfo.ID_Medicine = %@) and fireDate =", [sender valueForKey:@"ID_Medicine"], itemDate];
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        
       
        [[UIApplication sharedApplication] cancelLocalNotification:notif];
        
    }
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Profiles *setcours = [appDelegate GetProfilesWithID:[sender valueForKey:@"ID"]];
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
       
        
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        
        if ([[sender valueForKey:@"Message"] isEqualToString:@" "]) {
            [notif setAlertBody:[NSString stringWithFormat:NSLocalizedString(@"Take your %@!",nil), [sender valueForKey:@"Name"]]];
        }
        else
        {
            [notif setAlertBody:[sender valueForKey:@"Message"]];
            
        }
        notif.alertAction = @"Show?";
        
        if (setcours.audio) {
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = [NSString stringWithFormat:@"%@.mp3",setcours.audio];
            }
        }
        else
        {
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = @"twinkle.mp3";
            }
        }
        notif.hasAction = YES;
        
        
        notif.repeatInterval = CalUnit;
        
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
        
        
    }
    

    

    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotificationWith" afterDelay:0];
    
    
}


-(void)seeifPassed:(NSDate*)set wit:(NSMutableDictionary*)sender

{
    
    
    if([NSLocalizedString([sender valueForKey:@"Duration"],nil) isEqualToString:NSLocalizedString(@"Never",nil)])
        
    {
    }
    else
    {
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
        [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [setdata setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComps = [[NSDateComponents alloc] init];
        
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:[setdata dateFromString:[sender valueForKey:@"Duration"]]];
        
        
        [dateComps setDay:[dateComponents day]];
        [dateComps setMonth:[dateComponents month]];
        [dateComps setYear:[dateComponents year]];
        [dateComps setHour:23];
        // Notification will fire in one minute
        [dateComps setMinute:59];
        [dateComps setSecond:59];
        
        
        NSDate *itemDate = [calendar dateFromComponents:dateComps];
        
        if  ([itemDate compare:set]==NSOrderedSame)
            
        {
            
            
            
        }
        else if  ([itemDate compare:set]==NSOrderedDescending)
            
            
        {
            
        }
        else if  ([itemDate compare:set]==NSOrderedAscending)
            
            
        {
            
            
            
            
        }
        
    }
}



+(void)GoNotificationWeek:(NSDate*)itemDate set:(NSMutableDictionary*)sender
{
    
    
       ////NSLog(@"GoNotificationWeek");
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Profiles *setcours = [appDelegate GetProfilesWithID:[sender valueForKey:@"ID"]];
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
     
        
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        
        if ([[sender valueForKey:@"Message"] isEqualToString:@" "]) {
            [notif setAlertBody:[NSString stringWithFormat:NSLocalizedString(@"Take your %@!",nil),[sender valueForKey:@"Name"]]];
        }
        else
        {
            [notif setAlertBody:[sender valueForKey:@"Message"]];
            
        }
        notif.alertAction = @"Show?";
        
        if (setcours.audio) {
            
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = [NSString stringWithFormat:@"%@.mp3",setcours.audio];
            }
        }
        else
        {
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = @"twinkle.mp3";
            }
        }
        
        notif.hasAction = YES;
        
        
        notif.repeatInterval = NSWeekCalendarUnit;
        notif.applicationIconBadgeNumber =  [self CheckNumbers:notif.fireDate withID:[sender valueForKey:@"ID_Medicine"]];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
        
        
        
        
    }


    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotificationWeek" afterDelay:0];
    
    
    
    
}


+(void)GoNotificationDays:(NSDate*)itemDate set:(NSMutableDictionary*)sender
{
    
    
   ////NSLog(@"GoNotificationDays");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Profiles *setcours = [appDelegate GetProfilesWithID:[sender valueForKey:@"ID"]];
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
      
        
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        
        if ([[sender valueForKey:@"Message"] isEqualToString:@" "]) {
            [notif setAlertBody:[NSString stringWithFormat:NSLocalizedString(@"Take your %@!",nil), [sender valueForKey:@"Name"]]];
        }
        else
        {
            [notif setAlertBody:[sender valueForKey:@"Message"]];
            
        }
        notif.alertAction = @"Show?";
        
        if (setcours.audio) {
            
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = [NSString stringWithFormat:@"%@.mp3",setcours.audio];
                
            }
        }
        else
        {
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = @"twinkle.mp3";
            }
        }
        notif.hasAction = YES;
        
        
        notif.repeatInterval = 0;
        notif.applicationIconBadgeNumber =  [self CheckNumbers:notif.fireDate withID:[sender valueForKey:@"ID_Medicine"]];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
        
        
      
        
        
        
    }

    
    
    //[appDelegate reloadTableview:@"GoNotificationDays"];
    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotificationDays" afterDelay:0];
    
}


+(void)GoNotification:(NSDate*)itemDate set:(NSMutableDictionary*)sender
{
    
    
    
 ////NSLog(@"GoNotification");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Profiles *setcours = [appDelegate GetProfilesWithID:[sender valueForKey:@"ID"]];
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
       
        
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.timeZone = [NSTimeZone localTimeZone];
        notif.userInfo = sender;
        
        if ([[sender valueForKey:@"Message"] isEqualToString:@" "]) {
            [notif setAlertBody:[NSString stringWithFormat:NSLocalizedString(@"Take your %@!",nil), [sender valueForKey:@"Name"]]];
        }
        else
        {
            [notif setAlertBody:[sender valueForKey:@"Message"]];
            
        }
        notif.alertAction = @"Show?";
        
        if (setcours.audio) {
            
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = [NSString stringWithFormat:@"%@.mp3",setcours.audio];
            }
        }
        else
        {
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = @"twinkle.mp3";
            }
        }
        
        notif.hasAction = YES;
        
        
        notif.repeatInterval = NSCalendarUnitDay;
        notif.applicationIconBadgeNumber =  [self CheckNumbers:notif.fireDate withID:[sender valueForKey:@"ID_Medicine"]];
        
        
        
        
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
        
        
        
        NSDateFormatter *setdata = [[NSDateFormatter alloc] init];
        [setdata setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [setdata setDateFormat:[appDelegate convertString:@"EEEE dd, MMMM yyyy hh:mm a"]];
        
        
        
        NSDateFormatter *Hours = [[NSDateFormatter alloc] init];
        [Hours setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [Hours setDateFormat:[appDelegate convertString:@"hh:mm a"]];
        
        
        
        NSDateFormatter *day = [[NSDateFormatter alloc] init];
        [day setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        [day setDateFormat:@"dd/MM/yyyy"];
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item  setObject:@"Normal" forKey: @"Extra"];
        [item  setObject:[notif.userInfo valueForKey:@"ID_Medicine"] forKey: @"ID_Medicine"];
        [item  setObject:[notif.userInfo valueForKey:@"Name"] forKey: @"Name"];
        [item  setObject:[setdata stringFromDate:notif.fireDate] forKey: @"Firetime"];
        [item  setObject:[day stringFromDate:notif.fireDate] forKey: @"Date"];
        [item  setObject:[Hours stringFromDate:notif.fireDate] forKey: @"Time"];
        [SaveData insertAktiveMeds:item];
        
        
        
    }
    
    

    
}


+(void)GoNotificationNext:(NSDate*)itemDate set:(NSMutableDictionary*)sender
{
  
    
    
    
    if ([[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0] intValue]==0) {
        
    }
    else
    {
            ////NSLog(@"GoNotificationNext");
        
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComps = [[NSDateComponents alloc] init];
        
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:itemDate];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:itemDate];
        
        
        [dateComps setDay:[dateComponents day]];
        [dateComps setMonth:[dateComponents month]];
        [dateComps setYear:[dateComponents year]];
        
        [dateComps setHour:[timeComponents hour]];
        [dateComps setMinute:[timeComponents minute]+[[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0] intValue]];
        [dateComps setSecond:0];
        
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        Profiles *setcours = [appDelegate GetProfilesWithID:[sender valueForKey:@"ID"]];
        
        
        Class cls = NSClassFromString(@"UILocalNotification");
        if (cls != nil) {
            
            
            UIBackgroundTaskIdentifier bgTask =0;
            UIApplication  *app = [UIApplication sharedApplication];
            bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
                [app endBackgroundTask:bgTask];
            }];
            
            
            
            UILocalNotification *notif = [[cls alloc] init];
            notif.fireDate = [calendar dateFromComponents:dateComps];
            notif.timeZone = [NSTimeZone localTimeZone];
            notif.userInfo = sender;
            
            if ([[sender valueForKey:@"Message"] isEqualToString:@" "]) {
                [notif setAlertBody:[NSString stringWithFormat:NSLocalizedString(@"Take your %@!",nil),[sender valueForKey:@"Name"]]];
            }
            else
            {
                [notif setAlertBody:[sender valueForKey:@"Message"]];
                
            }
            notif.alertAction = @"Show?";
            
            if (setcours.audio) {
                
                if ([setcours.silence isEqualToString:@"YES"]) {
                    notif.soundName = @"Leeg.mp3";
                }
                else
                    
                {
                    notif.soundName = [NSString stringWithFormat:@"%@.mp3",setcours.audio];
                }
            }
            else
            {
                if ([setcours.silence isEqualToString:@"YES"]) {
                    notif.soundName = @"Leeg.mp3";
                }
                else
                    
                {
                    notif.soundName = @"twinkle.mp3";
                    
                }
            }
            
            notif.hasAction = YES;
            
            
            notif.repeatInterval = 0;
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notif];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:[NSString  stringWithFormat:@"Extra_%@",[sender valueForKey:@"ID_Medicine"]]];
            notif.applicationIconBadgeNumber =  [self CheckNumbers:notif.fireDate withID:[sender valueForKey:@"ID_Medicine"]];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:notif];
            
            
            
            
            
        }
        
     
        
  
        [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotificationNext" afterDelay:0];
        
        
        
        
    }
    
}

+(void)GoNotificationExtra:(NSDate*)itemDate set:(NSMutableDictionary*)sender andNote:(UILocalNotification*)curentChoise;
{
    
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Profiles *setcours = [appDelegate GetProfilesWithID:[sender valueForKey:@"ID"]];

    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:curentChoise];
    [appDelegate insertNotifications:sender withdata:data];
    
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        
     
        
        UILocalNotification *notif = [[cls alloc] init];
        notif.fireDate = itemDate;
        notif.userInfo = sender;
        
        if ([[sender valueForKey:@"Message"] isEqualToString:@" "]) {
            [notif setAlertBody:[NSString stringWithFormat:NSLocalizedString(@"Take your %@!",nil),[sender valueForKey:@"Name"]]];
        }
        else
        {
            [notif setAlertBody:[sender valueForKey:@"Message"]];
            
        }
        notif.alertAction = @"Show?";
        
        
        if (setcours.silence) {
            
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = [NSString stringWithFormat:@"%@.mp3",setcours.audio];
            }
        }
        else
        {
            if ([setcours.silence isEqualToString:@"YES"]) {
                notif.soundName = @"Leeg.mp3";
            }
            else
                
            {
                notif.soundName = @"twinkle.mp3";
            }
        }
        notif.hasAction = YES;
        
        
        notif.repeatInterval = NSCalendarUnitDay;
        notif.applicationIconBadgeNumber =  [self CheckNumbers:notif.fireDate withID:[sender valueForKey:@"ID_Medicine"]];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
        
        
        
        
    }

    
    

    [appDelegate performSelector:@selector(reloadTableview:) withObject:@"GoNotificationExtra" afterDelay:0];
    
    
    
    
}




+(NSDate*)whatNotificationConvert:(UILocalNotification*)sender
{

       

        
        NSArray *itemsUp = [NSArray arrayWithObjects: @"EEEE, MMMM dd, yyyy hh:mm a", @"EEEE dd MMMM yyyy HH:mm", @"EEEE, dd. MMMM yyyy hh:mm", @"EEEE dd MMMM yyyy HH:mm",nil];
    
       
               for (NSInteger i=0 ; i<[itemsUp count]; i++) {
                   
                   NSDateFormatter *format = [[NSDateFormatter alloc] init];
                   
                   [format setDateFormat:[itemsUp objectAtIndex:i]];
                   
                   
                   
                   if ([format dateFromString:[self getfiredate:[NSString stringWithFormat:@"%@", sender]]]) {
                       
      
                       
                       return [format dateFromString:[self getfiredate:[NSString stringWithFormat:@"%@", sender]]];
                       
                       
                       break;
                       
                   }
                 
               }
    
          return sender.fireDate;
}


+(NSString*)getfiredate:(NSString*)yourString


{

    
    
    yourString = [yourString stringByReplacingOccurrencesOfString:
            [NSString stringWithFormat: @"a.m."]
                                           withString: @""];
    
    
    yourString = [yourString stringByReplacingOccurrencesOfString:
                  [NSString stringWithFormat: @"p.m."]
                                                       withString: @""];
  

    
    NSScanner *theScanner;
    NSString *text = @"-";
    
    theScanner = [NSScanner scannerWithString: yourString];
    
    while ([theScanner isAtEnd] == NO) {
        // find start of tag
        [theScanner scanUpToString: @"fire date = " intoString: NULL];
        
        
        
        
        if ([yourString rangeOfString:@"00 AM"].location==NSNotFound)
        {
            
            // find end of tag
            [theScanner scanUpToString: @":00 " intoString: &text];
            
            
        }
        else
            
        {
            
            
            if ([yourString rangeOfString:@"00 AM"].location != NSNotFound) {
                
                
                
            }
            else
                
            {
                [theScanner scanUpToString: @"00 PM" intoString: &text];
                text = [NSString stringWithFormat:@"%@ %@", text, @"PM"];
            }
            
            if ([yourString rangeOfString:@"00 PM"].location != NSNotFound) {
                
                
                
            }
            else
                
            {
                [theScanner scanUpToString: @"00 AM" intoString: &text];
                
                
                text = [NSString stringWithFormat:@"%@ %@", text, @"AM"];
                
                
            }
            
            
            
            
        }
        
        
        text = [text stringByReplacingOccurrencesOfString:
                [NSString stringWithFormat: @"fire date = "]
                                               withString: @""];
        
        text = [text stringByReplacingOccurrencesOfString:
                [NSString stringWithFormat: @": AM AM"]
                                               withString: @" AM"];
        
        text = [text stringByReplacingOccurrencesOfString:
                [NSString stringWithFormat: @": PM PM"]
                                               withString: @" PM"];
        
        text = [text stringByReplacingOccurrencesOfString:
                [NSString stringWithFormat: @" at "]
                                               withString: @" "];
        
        
    } // while //
 
    return text;
    
    
}

+(NSString*)getdate:(NSString*)yourString
{

 
    
    NSScanner *theScanner;
    NSString *text = @"-";
    
    theScanner = [NSScanner scannerWithString: yourString];
    
    while ([theScanner isAtEnd] == NO) {
        // find start of tag
        [theScanner scanUpToString: @"next fire date = " intoString: NULL];
        
      
  
        
           if ([yourString rangeOfString:@"00 AM"].location==NSNotFound)
           {
           
            // find end of tag
            [theScanner scanUpToString: @":00 " intoString: &text];
            
            
          }
        else
            
        {
          
            
            if ([yourString rangeOfString:@"00 AM"].location != NSNotFound) {
                
                
               
            }
            else
                
            {
                  [theScanner scanUpToString: @"00 PM" intoString: &text];
                   text = [NSString stringWithFormat:@"%@ %@", text, @"PM"];
            }
            
            if ([yourString rangeOfString:@"00 PM"].location != NSNotFound) {
                
                
                
            }
            else
                
            {
                [theScanner scanUpToString: @"00 AM" intoString: &text];
                
                
                text = [NSString stringWithFormat:@"%@ %@", text, @"AM"];
                
                
            }
            
            
           
    
        }
      
        
        text = [text stringByReplacingOccurrencesOfString:
                [NSString stringWithFormat: @"next fire date = "]
                                               withString: @""];
        
        text = [text stringByReplacingOccurrencesOfString:
                [NSString stringWithFormat: @": AM AM"]
                                               withString: @" AM"];
        
        text = [text stringByReplacingOccurrencesOfString:
                [NSString stringWithFormat: @": PM PM"]
                                               withString: @" PM"];
        
        text = [text stringByReplacingOccurrencesOfString:
                [NSString stringWithFormat: @" at "]
                                               withString: @" "];
        

    } // while //

    return text;
    
    
}


+(void)insertforundofunction:(NSMutableDictionary*)sender
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSData *dataPast;
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userInfo.ID_Medicine = %@", [sender valueForKey:@"ID_Medicine"]];
    
    NSArray *notificationArray= [[[UIApplication sharedApplication] scheduledLocalNotifications] filteredArrayUsingPredicate:predicate];
    
    for (NSInteger i =0; i < ([notificationArray count]); i++) {
        
        UILocalNotification *notif = [notificationArray objectAtIndex:i];
        
        
        if (i<1) {
            
            
            dataPast = [NSKeyedArchiver archivedDataWithRootObject:notif];
            
            [appDelegate insertNotifications:sender withdata:dataPast];
           
            
        }
        
    }
    


}


+(void)insertforundoExtra:(NSMutableDictionary*)sender

{

    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",[sender valueForKey:@"ID_Medicine"]]]) {
        
        
        NSData *dataExtra= [[NSUserDefaults standardUserDefaults] objectForKey:[NSString  stringWithFormat:@"Extra_%@",[sender valueForKey:@"ID_Medicine"]]];
        
        UILocalNotification *localNotif = [NSKeyedUnarchiver unarchiveObjectWithData:dataExtra];
        
        [[UIApplication sharedApplication] cancelLocalNotification:localNotif];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString  stringWithFormat:@"Extra_%@",[sender valueForKey:@"ID_Medicine"]]];
        
    }
    
    
    
    
}



@end
