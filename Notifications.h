//
//  Notifications.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 27-08-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Notifications : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * firedate;
@property (nonatomic, retain) NSData * notifications;
@property (nonatomic, retain) NSString * changeDate;

@end
