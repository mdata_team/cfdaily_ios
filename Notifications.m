//
//  Notifications.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 27-08-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "Notifications.h"


@implementation Notifications

@dynamic id;
@dynamic firedate;
@dynamic notifications;
@dynamic changeDate;

@end
