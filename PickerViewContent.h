//
//  PickerViewContent.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-09-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrengthViewControllerScroll.h"

@class IntegerPicker, CustumPickerStrengt, CustumPicker, CustumPickerRefill;
@interface PickerViewContent : UIView
{
    
    IntegerPicker *integerPicker;
    CustumPicker *custumpicker;
    CustumPickerStrengt *custumpickerStrength;
    CustumPickerRefill *custumPickerRefill;
}
@property (nonatomic, strong) IntegerPicker *integerPicker;
@property (nonatomic, strong) CustumPicker *custumpicker;
@property (nonatomic, strong) CustumPickerStrengt *custumpickerStrength;

-(void)rollTo:(NSString*)set;
-(void)rollitem:(NSString*)set;
-(void)Decimal:(NSString*)CompaireDecimal pernt:(UIViewController*)set;
-(void)Decimalstrength:(NSString*)CompaireDecimal pernt:(StrengthViewControllerScroll*)set;
@end
