//
//  PickerViewContent.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-09-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "PickerViewContent.h"
#import "CustumPicker.h"
#import "IntegerPicker.h"
#import "CustumPickerRefill.h"
#import "CustumPickerStrengt.h"
#import "AppDelegate.h"


@implementation PickerViewContent
@synthesize integerPicker;
@synthesize custumpicker;
@synthesize custumpickerStrength;



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            
//            [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"balk"]]]; //29-06-2017 changes
            [self setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];

           
            
        }
        
        else
        {
//            [self setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //29-06-2017
              [self setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];

            
        }
        
     
        
    }
    return self;
}


-(void)Decimalstrength:(NSString*)CompaireDecimal pernt:(StrengthViewControllerScroll*)set

{
    
    NSLog(@"Decimalstrength %@ %@", set, CompaireDecimal);
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  
    if ([CompaireDecimal isEqualToString:@"piece"]) {
        
        NSArray *numbers2 = [CompaireDecimal componentsSeparatedByString:@","];
        
        integerPicker = [[IntegerPicker alloc] initWithFrame:CGRectMake(0, 0, 320, 180)];
        [integerPicker geloadContenst:[[numbers2 objectAtIndex:0] length] numbers:3 getparant:set];
        [integerPicker setParantit:set];
        integerPicker.delegate = integerPicker;
        integerPicker.dataSource = integerPicker;
        [self addSubview:integerPicker];
    }
    else
    {
        custumpicker = [[CustumPicker alloc] initWithFrame:CGRectMake(0, 0, 240, 180)];
        [custumpicker geloadContenst:4 numbers:8 getparant:set];
        [custumpicker setParantit:set];
        custumpicker.delegate = custumpicker;
        custumpicker.dataSource = custumpicker;
        [self addSubview:custumpicker];
        
        custumpickerStrength = [[CustumPickerStrengt alloc] initWithFrame:CGRectMake(240, 0, 80, 180)];
        [custumpickerStrength geloadContenst:1 numbers:8 getparant:set];
        [custumpickerStrength setParantit:set];
        custumpickerStrength.delegate = custumpickerStrength;
        custumpickerStrength.dataSource = custumpickerStrength;
        [self addSubview:custumpickerStrength];
        
        
        NSLog(@"%@", appDelegate.strength_unit);
        
       // [custumpickerStrength selectRow:appDelegate.strength_unit inComponent:0 animated:YES];
        
    }

}


-(void)Decimal:(NSString*)CompaireDecimal pernt:(UIViewController*)set
{
    
    NSLog(@"Decimal %@", NSLocalizedString(CompaireDecimal,nil));
    
    NSLog(@"Log : %@",[SHARED_APPDELEGATE strMedtype]);
    
    if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Danish"])
    {
        if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injektion"])
        {
            custumpicker = [[CustumPicker alloc] initWithFrame:CGRectMake(0, 0, 240, 180)];
            [custumpicker geloadContenst:4 numbers:8 getparant:set];
            [custumpicker setParantit:(StrengthViewControllerScroll*)set];
            custumpicker.delegate = custumpicker;
            custumpicker.dataSource = custumpicker;
            [self addSubview:custumpicker];
            
            custumPickerRefill = [[CustumPickerRefill alloc] initWithFrame:CGRectMake(240, 0, 80, 180)];
            [custumPickerRefill geloadContenst:1 numbers:8 getparant:set];
            [custumPickerRefill setParantit:(StrengthViewControllerScroll*)set];
            custumPickerRefill.delegate = custumPickerRefill;
            custumPickerRefill.dataSource = custumPickerRefill;
            [self addSubview:custumPickerRefill];
            return;
        }
        else
        {
            NSArray *numbers2 = [CompaireDecimal componentsSeparatedByString:@","];
            
            integerPicker = [[IntegerPicker alloc] initWithFrame:CGRectMake(0, 0, 320, 180)];
            [integerPicker geloadContenst:[[numbers2 objectAtIndex:0] length] numbers:3 getparant:set];
            [integerPicker setParantit:(StrengthViewControllerScroll*)set];
            integerPicker.delegate = integerPicker;
            integerPicker.dataSource = integerPicker;
            [self addSubview:integerPicker];
            return;
        }
    }
    else if ([NSLocalizedString(@"Taal",nil) isEqualToString:@"Norwegian"]) //9-11-2017
    {
        if ([[SHARED_APPDELEGATE strMedtype] isEqualToString:@"Injeksjon"])
        {
            custumpicker = [[CustumPicker alloc] initWithFrame:CGRectMake(0, 0, 240, 180)];
            [custumpicker geloadContenst:4 numbers:8 getparant:set];
            [custumpicker setParantit:(StrengthViewControllerScroll*)set];
            custumpicker.delegate = custumpicker;
            custumpicker.dataSource = custumpicker;
            [self addSubview:custumpicker];
            
            custumPickerRefill = [[CustumPickerRefill alloc] initWithFrame:CGRectMake(240, 0, 80, 180)];
            [custumPickerRefill geloadContenst:1 numbers:8 getparant:set];
            [custumPickerRefill setParantit:(StrengthViewControllerScroll*)set];
            custumPickerRefill.delegate = custumPickerRefill;
            custumPickerRefill.dataSource = custumPickerRefill;
            [self addSubview:custumPickerRefill];
            return;
        }
        else
        {
            NSArray *numbers2 = [CompaireDecimal componentsSeparatedByString:@","];
            
            integerPicker = [[IntegerPicker alloc] initWithFrame:CGRectMake(0, 0, 320, 180)];
            [integerPicker geloadContenst:[[numbers2 objectAtIndex:0] length] numbers:3 getparant:set];
            [integerPicker setParantit:(StrengthViewControllerScroll*)set];
            integerPicker.delegate = integerPicker;
            integerPicker.dataSource = integerPicker;
            [self addSubview:integerPicker];
            return;
        }
    }
    
    if ([CompaireDecimal isEqualToString:@"piece"])
    {
        NSArray *numbers2 = [CompaireDecimal componentsSeparatedByString:@","];
    
       integerPicker = [[IntegerPicker alloc] initWithFrame:CGRectMake(0, 0, 320, 180)];
        [integerPicker geloadContenst:[[numbers2 objectAtIndex:0] length] numbers:3 getparant:set];
        [integerPicker setParantit:(StrengthViewControllerScroll*)set];
        integerPicker.delegate = integerPicker;
        integerPicker.dataSource = integerPicker;
        [self addSubview:integerPicker];
    }
    else
    {
        custumpicker = [[CustumPicker alloc] initWithFrame:CGRectMake(0, 0, 240, 180)];
        [custumpicker geloadContenst:4 numbers:8 getparant:set];
        [custumpicker setParantit:(StrengthViewControllerScroll*)set];
        custumpicker.delegate = custumpicker;
        custumpicker.dataSource = custumpicker;
        [self addSubview:custumpicker];
        
        custumPickerRefill = [[CustumPickerRefill alloc] initWithFrame:CGRectMake(240, 0, 80, 180)];
        [custumPickerRefill geloadContenst:1 numbers:8 getparant:set];
        [custumPickerRefill setParantit:(StrengthViewControllerScroll*)set];
        custumPickerRefill.delegate = custumPickerRefill;
        custumPickerRefill.dataSource = custumPickerRefill;
        [self addSubview:custumPickerRefill];
        
    }
    
    

    
    
}

-(void)rollitem:(NSString*)set

{
    
  //[custumPickerRefill
    
    
}


-(void)rollTo:(NSString*)set

{
    
  
    
    NSString *increments = @"";
    
    NSArray *numbers = [set componentsSeparatedByString:@"."];
 
    if ([numbers count]==2) {
   
    for (NSInteger i=0 ; i<4-[[numbers objectAtIndex:0] length]; i++) {
       
         increments = [increments stringByAppendingString:@"0"];
        
         
     }
    

    
    NSString *tekst = [NSString stringWithFormat:@"%@%.03f", increments,[set floatValue]];
    
    
   for (NSInteger i=0 ; i<[tekst length]; i++) {
        
        NSRange range = NSMakeRange (i, 1);
        
        if ([[tekst substringWithRange:range] isEqualToString:@"."]) {
           
            
        }
        else
            
        {
           [custumpicker selectRow:[[tekst substringWithRange:range] intValue] inComponent:i animated:YES];
         
        }
        
        
    }
    
   
    }
   
  
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
