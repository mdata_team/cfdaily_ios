//
//  myPickerViewTaal.m
//  MSD
//
//  Created by Jeffrey Snijder on 30-01-12.
//  Copyright (c) 2012 Livecast.nl. All rights reserved.
//

#import "PickerViewFrequentie.h"
//#import "SavFileTo.h"
#import "AppDelegate.h"
//#import "SetMedicijnenViewController.h"
#import "HoursViewController.h"

@implementation PickerViewFrequentie
@synthesize parantit;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.delegate = self;
        self.dataSource = self;
        
     
        placeArray = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"6",@"8",@"12",@"24",nil];
    }
    return self;
}

-(void) geloadContenst
{
     self.delegate = nil;
   
}

- (void)reloadAllComponents
{
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
  
  
    [parantit.TitleText setText:[placeArray objectAtIndex:row]];
    
    
    [parantit.TitleText setFrame:CGRectMake(13*[[NSString stringWithFormat:@"%lu", (unsigned long)[NSLocalizedString(@"Every",nil) length]] intValue], 28, parantit.view.frame.size.width-40, 35)];
    
    if ([parantit.title isEqualToString:NSLocalizedString(@"Every X Hours",nil)]) {
        
        if ([parantit.TitleText.text length]<4) {
            
            
            if ([parantit.TitleText.text length] ==0) {
                
                [parantit.underTitleText setText:[NSString stringWithFormat:@"%@   %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
            }
            if ([parantit.TitleText.text length] ==1) {
                
                
                [parantit.underTitleText setText:[NSString stringWithFormat:@"%@     %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
            }
            if ([parantit.TitleText.text length] ==2) {
                
                [parantit.underTitleText setText:[NSString stringWithFormat:@"%@      %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
            }
            if ([parantit.TitleText.text length] ==3) {
                
                [parantit.underTitleText setText:[NSString stringWithFormat:@"%@        %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"DHoursays",nil)]];
            }
            
            
        }
        else
        {
            //[parantit.TitleText setText:@""];
            [parantit.underTitleText setText:[NSString stringWithFormat:@"%@   %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Hours",nil)]];
        }
    }
    
    
    
    
    else if ([parantit.title isEqualToString:NSLocalizedString(@"Every X Days",nil)])
        
    {
        if ([parantit.TitleText.text length]<4) {
            
            
            if ([parantit.TitleText.text length] ==0) {
                
                [parantit.underTitleText setText:[NSString stringWithFormat:@"%@   %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
            }
            if ([parantit.TitleText.text length] ==1) {
                
                
                [parantit.underTitleText setText:[NSString stringWithFormat:@"%@     %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
            }
            if ([parantit.TitleText.text length] ==2) {
                
                [parantit.underTitleText setText:[NSString stringWithFormat:@"%@      %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
            }
            if ([parantit.TitleText.text length] ==3) {
                
                [parantit.underTitleText setText:[NSString stringWithFormat:@"%@        %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
            }
            
            
        }
        else
        {
            [parantit.TitleText setText:@""];
            
            [parantit.underTitleText setText:[NSString stringWithFormat:@"%@  %@", NSLocalizedString(@"Every",nil), NSLocalizedString(@"Days",nil)]];
        }
    }
    
    



}

-(void)setparant:(HoursViewController*)parant

{
    parantit =parant;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
	NSString *returnStr;
	returnStr = [placeArray objectAtIndex:row];
	return returnStr;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
	UILabel *retval = (id)view;
	if (!retval) {
		retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
	}
	retval.text = [placeArray objectAtIndex:row];
   
	retval.font = [UIFont boldSystemFontOfSize:15];
	retval.textAlignment = NSTextAlignmentCenter;
	retval.backgroundColor = [UIColor clearColor];
    
	
	return retval;
}



- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	CGFloat componentWidth;
	componentWidth = 40;	// first column size is wider to hold names
	return componentWidth;
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 40.0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [placeArray count];
	
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
	
	
}



@end
