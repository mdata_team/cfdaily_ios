    //
    //  PlayCell.h
    //  CFMedcare
    //
    //  Created by Jeffrey Snijder on 25-02-13.
    //  Copyright (c) 2013 Menopur. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import "ActionArrow.h"
@class MusicViewController;
@interface PlayCell : UITableViewCell

@property (nonatomic, retain) IBOutlet  MusicViewController *lostView;
@property (nonatomic, retain) IBOutlet ActionArrow *info;
@property (nonatomic, retain) IBOutlet UILabel *title;
@property (nonatomic, retain) IBOutlet ActionArrow *Extra;
@property (nonatomic, retain) IBOutlet UIImageView *background;

-(void) FillAllItems:(NSString*)name index:(NSInteger)set;
-(void)getparent:(MusicViewController*)set;
@end
