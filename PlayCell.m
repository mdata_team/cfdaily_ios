    //
    //  PlayCell.m
    //  CFMedcare
    //
    //  Created by Jeffrey Snijder on 25-02-13.
    //  Copyright (c) 2013 Menopur. All rights reserved.
    //

#import "PlayCell.h"
#import "AppDelegate.h"

@implementation PlayCell
@synthesize lostView;
@synthesize info;
@synthesize title;
@synthesize Extra;
@synthesize background;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
            // Initialization code


        Extra = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [Extra setBackgroundColor:[UIColor clearColor]];
        [Extra setTag:140];
        [self addSubview:Extra];


        title = [[UILabel alloc]  initWithFrame:CGRectMake(10, 10, 200, 72/2)];
        [title setTag:10];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setTextColor:[UIColor blackColor]];
        [title setFont:[UIFont boldSystemFontOfSize:16]];
        [self addSubview:title];

        info = [[ActionArrow alloc]  initWithFrame:CGRectMake(285, 10, 60/2, 72/2)];
        [info setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
        [info setTag:140];
        [self addSubview:info];


        background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Vink.png"]];
        [background setFrame:CGRectMake(self.frame.size.width-80,  20, 20, 20)];
        [background setBackgroundColor:[UIColor clearColor]];
        [self addSubview:background];
       
        
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
            
            //NSLog(@"left");
            
            self.transform = CGAffineTransformMakeScale(-1, 1);
            
            
            for (UIView *content in self.subviews) {
                
                
                for (UILabel *label in content.subviews) {
                    
                    
                    if ([label isKindOfClass:[UILabel class]]) {
                        
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                
                for (UIButton *label in content.subviews) {
                    
                    
                    if ([label isKindOfClass:[UIButton class]]) {
                        
                        label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                        [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                
                
                
                
            }
            
            
        }
        
        else
        {
            
        }
        

    }
    return self;
}

-(void)getparent:(MusicViewController*)set
{

  lostView =set;
    [info addTarget:lostView action:@selector(PresentView:) forControlEvents:UIControlEventTouchUpInside];
    [Extra addTarget:lostView action:@selector(select:) forControlEvents:UIControlEventTouchUpInside];
}


-(void) FillAllItems:(NSString*)name index:(NSInteger)set;
{

    [info setTitleit:name];

    [Extra setTitleit:name];
    

    
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([appDelegate.currentProfile.audio isEqualToString:name]) {
        
        [background setAlpha:1];
        
    }
    else
    {
        
        [background setAlpha:0];
    }


}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
        // Configure the view for the selected state
}

@end
