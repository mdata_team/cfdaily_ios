//
//  PostponeNotification.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 17-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostponeNotification : NSObject

+(NSDate*)setDatePostpone:(NSDictionary*)sender;
@end
