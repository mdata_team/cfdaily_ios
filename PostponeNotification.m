//
//  PostponeNotification.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 17-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "PostponeNotification.h"

@implementation PostponeNotification



+(NSDate*)setDatePostpone:(NSDictionary*)sender
{
    
  
    NSDate *itemDate  = [[NSDate alloc] init];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDate *now = [[NSDate alloc] init];
    
//    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now]; //26-06-2017 change
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:now];

//    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:now]; //26-06-2017 change
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:now];
    
    
    
    
    if ([[sender valueForKey:@"choice"] isEqualToString:@"Hours"]) {
        
        
        
        if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
            
            
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]+[[sender valueForKey:@"intervalOurs"] intValue]];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:0];
            
            
            itemDate = [calendar dateFromComponents:dateComps];
            
       
            
        }
        
        
        
    }
    else if ([[sender valueForKey:@"choice"] isEqualToString:@"Days"]) {
        
        //44 dagen
        
        
        if ([[sender valueForKey:@"intervalOurs"] intValue]>0) {
            
            
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+[[sender valueForKey:@"intervalOurs"] intValue]+[[sender valueForKey:@"intervalOurs"] intValue]];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:0];
            
            
            itemDate = [calendar dateFromComponents:dateComps];
            
        }
        
        
    }
    
    else if ([[sender valueForKey:@"choice"] isEqualToString:NSLocalizedString(@"Daily",nil)]) {
        
        
        
        if ([[sender valueForKey:@"Moment"] isEqualToString:NSLocalizedString(@"AM",nil)]) {
            
            
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]+[[sender valueForKey:@"Diverence"] intValue]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:0];
            
            itemDate = [calendar dateFromComponents:dateComps];
            
          
        }
        else
            
        {
            
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]+[[sender valueForKey:@"Diverence"] intValue]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]+12+12];
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:0];
            
            
            itemDate = [calendar dateFromComponents:dateComps];
    
            
        }
        
        
    }
    
    else if ([[sender valueForKey:@"choice"] isEqualToString:NSLocalizedString(@"Weekly",nil)]) {
        
        if ([[sender valueForKey:@"Diverence"] intValue]>0 &&[[sender valueForKey:@"Diverence"] intValue]<7) {
            
            
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            [dateComps setDay:[dateComponents day]+[[sender valueForKey:@"Diverence"] intValue]+[[sender valueForKey:@"Diverence"] intValue]];
            [dateComps setMonth:[dateComponents month]];
            [dateComps setYear:[dateComponents year]];
            [dateComps setHour:[timeComponents hour]];
            // Notification will fire in one minute
            [dateComps setMinute:[timeComponents minute]];
            [dateComps setSecond:0];
            
            itemDate = [calendar dateFromComponents:dateComps];
            
        
            
        }
        
    }
    
    
    return itemDate;
    
}






@end
