//
//  ProcesIt.h
//  Vertex Dental
//
//  Created by Jeffrey Snijder on 01-07-13.
//  Copyright (c) 2013 Houdah Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProcesIt : UIView
@property (nonatomic, strong) IBOutlet UILabel *progressBar;
@property (nonatomic, assign) NSInteger less;

-(void)Changeit:(NSString*)set;
-(void)Setwhole:(NSString*)set;
@end
