//
//  ProcesIt.m
//  Vertex Dental
//
//  Created by Jeffrey Snijder on 01-07-13.
//  Copyright (c) 2013 Houdah Software. All rights reserved.
//

#import "ProcesIt.h"
#import <QuartzCore/QuartzCore.h>

@implementation ProcesIt
@synthesize progressBar;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        progressBar = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 5, 15)];
        [progressBar setBackgroundColor:[UIColor colorWithRed:0.510 green:0.741 blue:0.757 alpha:1.000]];
        [self addSubview:progressBar];
        [progressBar setAlpha:1];
        
       
    }
    return self;
}

-(void)Changeit:(NSString*)set


{
    
    if (self.frame.size.height ==1000) {
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.8];
        
        [progressBar setFrame:CGRectMake(0, 0, (self.frame.size.height/11)*[set intValue], 15)];
        
        [UIView commitAnimations];
        
    }
    
    else
    {
 
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.8];
    
    [progressBar setFrame:CGRectMake(0, 0, (self.frame.size.width/11)*[set intValue], 15)];
    
    [UIView commitAnimations];
        
    }
    
}


-(void)Setwhole:(NSString*)set


{
    
    if (self.frame.size.height ==1000) {
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.8];
        
        [progressBar setFrame:CGRectMake(0, 0, (self.frame.size.height/11)*[set intValue], 15)];
        
        [UIView commitAnimations];
        
    }
    
    else
    {

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.8];
    
    [progressBar setFrame:CGRectMake(0, 0, (self.frame.size.width/11)*[set intValue], 15)];
    
    [UIView commitAnimations];
        
    }
    
}

-(void)animateLabel


{
        [progressBar setBackgroundColor:[UIColor whiteColor]];
}


@end
