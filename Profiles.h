//
//  Course.h
//  CDCourses
//
//  Created by Jeffrey Snijder on 26-07-13.
//  Copyright (c) 2013 Jeffrey Snijder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Profiles : NSManagedObject


@property (nonatomic, retain) NSString * audio;
@property (nonatomic, retain) NSString * coloralpha;
@property (nonatomic, retain) NSString * colorblue;
@property (nonatomic, retain) NSString * colorRed;
@property (nonatomic, retain) NSString * colorGreen;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *silence;
@property (nonatomic, retain) NSString *vibrate;

@property (nonatomic, retain) NSString *send;
@property (nonatomic, retain) NSString *id;




@end
