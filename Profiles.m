//
//  Course.m
//  CDCourses
//
//  Created by Jeffrey Snijder on 26-07-13.
//  Copyright (c) 2013 Jeffrey Snijder. All rights reserved.
//

#import "Profiles.h"


@implementation Profiles


@dynamic audio;
@dynamic coloralpha;
@dynamic colorblue;
@dynamic colorRed;
@dynamic colorGreen;
@dynamic image;
@dynamic name;
@dynamic silence;
@dynamic vibrate;
@dynamic send;
@dynamic id;


-(void)awakeFromInsert
{
    
    [super awakeFromInsert];

    
    
}
@end
