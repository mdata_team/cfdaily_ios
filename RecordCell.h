    //
    //  PlayCell.h
    //  CFMedcare
    //
    //  Created by Jeffrey Snijder on 25-02-13.
    //  Copyright (c) 2013 Menopur. All rights reserved.
    //

#import <UIKit/UIKit.h>

@class RecordingViewController;
@interface RecordCell : UITableViewCell

@property (nonatomic, retain) IBOutlet  RecordingViewController *lostView;
@property (nonatomic, retain) IBOutlet UIButton *info;
@property (nonatomic, retain) IBOutlet UILabel *title;
@property (nonatomic, retain) IBOutlet UIButton *Extra;
@property (nonatomic, retain) IBOutlet UIImageView *background;

-(void) FillAllItems:(NSString*)name index:(NSInteger)set;
-(void)getparent:(RecordingViewController*)set;
@end
