//
//  RecordingAct.h
//  Tell
//
//  Created by Jeffrey Snijder on 27-01-13.
//  Copyright (c) 2013 Livecast. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordingAct : NSObject
+(void) startRecordingCopy:(NSString*)title;
+(void) startRecording:(NSString*)title parant:(UIButton*)sender;
+(void) stopRecording:(NSString*)set;
+(void) playRecording:(NSURL*)title;
+(void)PlayingStop;



@end
