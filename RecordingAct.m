//
//  RecordingAct.m
//  Tell
//
//  Created by Jeffrey Snijder on 27-01-13.
//  Copyright (c) 2013 Livecast. All rights reserved.
//

#import "RecordingAct.h"
#import "AppDelegate.h"
#import "SaveData.h"
#import "GetData.h"

@implementation RecordingAct



+(void) startRecordingCopy:(NSString*)title
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];



    appDelegate.recoringNow = YES;


    appDelegate.audioRecorder = nil;

        // Init audio with record capability
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryRecord error:nil];



    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] initWithCapacity:10];
    if(appDelegate.recordEncoding == ENC_PCM)
        {
        [recordSettings setObject:[NSNumber numberWithInt: kAudioFormatLinearPCM] forKey: AVFormatIDKey];
        [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
        [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSettings setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
        [recordSettings setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
        }
    else
        {
        NSNumber *formatObject;

        switch (appDelegate.recordEncoding) {
            case (ENC_AAC):
                formatObject = [NSNumber numberWithInt: kAudioFormatMPEG4AAC];
                break;
            case (ENC_ALAC):
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleLossless];
                break;
            case (ENC_IMA4):
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleIMA4];
                break;
            case (ENC_ILBC):
                formatObject = [NSNumber numberWithInt: kAudioFormatiLBC];
                break;
            case (ENC_ULAW):
                formatObject = [NSNumber numberWithInt: kAudioFormatULaw];
                break;
            default:
                formatObject = [NSNumber numberWithInt: kAudioFormatMPEG4AAC];
        }

        [recordSettings setObject:formatObject forKey: AVFormatIDKey];
        [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
        [recordSettings setObject:[NSNumber numberWithInt:12800] forKey:AVEncoderBitRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSettings setObject:[NSNumber numberWithInt: AVAudioQualityHigh] forKey: AVEncoderAudioQualityKey];
        }




    
    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);

    NSString *documentsDir = [paths2 objectAtIndex:0];


    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Caches/Copy.pcm", documentsDir]];

    [[NSUserDefaults standardUserDefaults] setObject:title forKey:@"IDit"];


    NSDate *today1 = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm"];
    appDelegate.DateString = [dateFormat stringFromDate:today1];


    NSError *error = nil;
    appDelegate.audioRecorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSettings error:&error];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:)
												 name:AVPlayerItemDidPlayToEndTimeNotification
											   object:appDelegate.audioRecorder];

    if ([appDelegate.audioRecorder prepareToRecord] == YES){
        [appDelegate.audioRecorder setDelegate:appDelegate];
        [appDelegate.audioRecorder record];
        appDelegate.recoringNow = YES;
    }else {
           
    }
    ;
}


+(void) startRecording:(NSString*)title parant:(UIButton*)sender
{
    
    
    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDir = [paths2 objectAtIndex:0];
    
    BOOL success;
    
    
    //FileManager - je maakt dus toegang tot je files in je app met Filemanager.
    NSFileManager *FileManager = [NSFileManager defaultManager];
    
    success = [FileManager fileExistsAtPath:[NSString stringWithFormat:@"%@/Caches/%@.pcm", documentsDir, title]];
    
    
    
    if (success) {
        
        //26-06-2017 change
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:[NSString stringWithFormat:@"This name has already been used!"]
//                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
//        [alert show];
        
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"",nil) message:[NSString stringWithFormat:@"This name has already been used!"] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [[SHARED_APPDELEGATE window].rootViewController presentViewController:alert animated:YES completion:nil];
            
        });
        
    }
    
    else
    {
 
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    
    appDelegate.recoringNow = YES;


    appDelegate.audioRecorder = nil;

   AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryRecord error:nil];



    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] initWithCapacity:10];
    if(appDelegate.recordEncoding == ENC_PCM)
        {
        [recordSettings setObject:[NSNumber numberWithInt: kAudioFormatLinearPCM] forKey: AVFormatIDKey];
        [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
        [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSettings setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
        [recordSettings setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
        }
    else
        {
        NSNumber *formatObject;

        switch (appDelegate.recordEncoding) {
            case (ENC_AAC):
                formatObject = [NSNumber numberWithInt: kAudioFormatMPEG4AAC];
                break;
            case (ENC_ALAC):
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleLossless];
                break;
            case (ENC_IMA4):
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleIMA4];
                break;
            case (ENC_ILBC):
                formatObject = [NSNumber numberWithInt: kAudioFormatiLBC];
                break;
            case (ENC_ULAW):
                formatObject = [NSNumber numberWithInt: kAudioFormatULaw];
                break;
            default:
                formatObject = [NSNumber numberWithInt: kAudioFormatMPEG4AAC];
        }

        [recordSettings setObject:formatObject forKey: AVFormatIDKey];
        [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
        [recordSettings setObject:[NSNumber numberWithInt:12800] forKey:AVEncoderBitRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSettings setObject:[NSNumber numberWithInt: AVAudioQualityHigh] forKey: AVEncoderAudioQualityKey];
        }

    
    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDir = [paths2 objectAtIndex:0];

    
     NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Caches/%@.pcm", documentsDir, title]];
 

   
     [[NSUserDefaults standardUserDefaults] setObject:title forKey:@"IDit"];


    NSDate *today1 = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm"];
    appDelegate.DateString = [dateFormat stringFromDate:today1];

    
    NSError *error = nil;
    appDelegate.audioRecorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSettings error:&error];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:)
												 name:AVPlayerItemDidPlayToEndTimeNotification
											   object:appDelegate.audioRecorder];

    if ([appDelegate.audioRecorder prepareToRecord] == YES){
        [appDelegate.audioRecorder setDelegate:appDelegate];
        [appDelegate.audioRecorder record];
        appDelegate.recoringNow = YES;
    }else {
       
    }
     
    double delayInSeconds = 10;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [sender setBackgroundColor:[UIColor whiteColor]];
        [self stopRecording:title];
    });
        
    }

}


+(void) stopRecording:(NSString*)set
{
  
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    if ([appDelegate.audioRecorder isRecording]) {


        [appDelegate.audioRecorder stop];
        appDelegate.recoringNow = NO;

    }
    


    [SaveData insertAudiointoSQL:set];

     

    
    
}

+(void) playRecording:(NSURL*)title
{


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    appDelegate.audioPlayer=NULL;

    
    appDelegate.playingNow = YES;

    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];

             
    NSError *error;

  
            // appDelegate.audioPlayer = [[AVAudioPlayer alloc] initWithData:audio error:&error];
    appDelegate.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:title error:&error];
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self                          selector:@selector(handle_PlaybackStateChanged:)
                               name:AVPlayerItemDidPlayToEndTimeNotification object:appDelegate.audioPlayer];

    [appDelegate.audioPlayer prepareToPlay];
    [appDelegate.audioPlayer setDelegate:appDelegate];
    
    
    [appDelegate.audioPlayer setVolume:1];
    
    
    [appDelegate.audioPlayer play];




         
    
}


+(void)PlayingStop
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
       [appDelegate.audioPlayer stop];
}

+(void) Record

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    if ([appDelegate.onTimer isValid]) {
        [appDelegate.onTimer invalidate];
        appDelegate.onTimer = NULL;

        
    }


   

    if ([appDelegate.audioRecorder isRecording]) {

    }
    else
        {

            //[onTimer invalidate];
            //onTimer = NULL;

        }



}








@end
