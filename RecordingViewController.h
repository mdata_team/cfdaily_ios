//
//  RecordingViewController.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 06-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class TekstIDLabel, NewMedecineController;
@interface RecordingViewController : UIViewController <UITextViewDelegate, UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate, MPMediaPickerControllerDelegate>
{
    
    UIView *Combi;
    UITableView *table;
    NSMutableArray *Vraagantwoord;
    
    UITextView *TitleTextchose;
    TekstIDLabel *extra;
}

@property (nonatomic, retain) NewMedecineController *parantIt;
@property (nonatomic, retain) TekstIDLabel *parantLabel;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;

-(void) setTextview: (NSString *) text;
-(void)getParant:(UIViewController*)parant;
-(void)whatLabel:(UILabel*)set;
-(void)hideAndseek:(NSString*)set;
-(void)setChoice:(NSString*)gothere;

@end
