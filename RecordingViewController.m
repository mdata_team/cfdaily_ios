//
//  RecordingViewController.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 06-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "RecordingViewController.h"
#import "NewMedecineController.h"
#import <QuartzCore/QuartzCore.h>
#import "RecordCell.h"
#import "GetData.h"
#import "AppDelegate.h"
#import "RecordingAct.h"
#import "SaveData.h"
#import "TekstIDLabel.h"
#import "ActionArrow.h"

@interface RecordingViewController ()

@end

@implementation RecordingViewController
@synthesize parantIt;

@synthesize TitleText;
@synthesize parantLabel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
         Vraagantwoord =[[NSMutableArray alloc]init];
           Vraagantwoord =[GetData getRecording];
        
        
        //
        
        
//        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
        [a1 setTag:44];
//        [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
        [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
         NSString *deviceType = [UIDevice currentDevice].systemVersion;
        NSRange range = NSMakeRange (0, 1);
        //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
           [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        else
        {
             [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
            a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = random;
        
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
        
        
        Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 79)];
        [Combi setBackgroundColor:[UIColor whiteColor]];
        [Combi.layer setCornerRadius:10];
        [Combi.layer setBorderWidth:1.5];
        [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
        [Combi.layer setMasksToBounds:YES];
        Combi.layer.shadowOffset = CGSizeMake(3, 0);
        Combi.layer.shadowOpacity = 2;
        Combi.layer.shadowRadius = 2.0;
        [self.view addSubview:Combi];
        
        NSArray *Cell1 =[NSArray arrayWithObjects:@"Play#88",@"Record#82",@"Stop#76",nil];
        
       for (NSInteger i=0 ; i<[Cell1 count]; i++) {
            
            
        NSArray *numbers = [[Cell1 objectAtIndex:i] componentsSeparatedByString:@"#"];

        
        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(((self.view.frame.size.width-20)/3)*i, 0,  (self.view.frame.size.width-20)/3, 40)];
        [NameButton setTitle:[numbers objectAtIndex:0] forState:UIControlStateNormal];
            [NameButton setTitleColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000] forState:UIControlStateNormal];
            [NameButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
        [NameButton setTag:140+i];
            [NameButton.layer setBorderWidth:1.5];
            [NameButton.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Combi addSubview:NameButton];
            
        
            
        }
        
        
        UIButton *NameButton = [[UIButton alloc]  initWithFrame:CGRectMake(((self.view.frame.size.width-20)/3)*2, 39,  (self.view.frame.size.width-20)/3, 40)];
        [NameButton setTitle:@"Save" forState:UIControlStateNormal];
        [NameButton setTitleColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000] forState:UIControlStateNormal];
        [NameButton setTag:140+3];
        [NameButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
        [NameButton.layer setBorderWidth:1.5];
        [NameButton.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Combi addSubview:NameButton];
        
        
        TitleTextchose = [[UITextView alloc] initWithFrame:CGRectMake(0, 39, ((self.view.frame.size.width-20)/3)*2, 40)];
        [TitleTextchose setBackgroundColor:[UIColor whiteColor]];
        [TitleTextchose setTextAlignment:NSTextAlignmentCenter];
        [TitleTextchose setText:@"Fill in a name"];
        [TitleTextchose setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        [TitleTextchose setTag:123+1];
        [TitleTextchose.layer setBorderWidth:1.5];
        [TitleTextchose.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
        TitleTextchose.selectedRange = NSMakeRange(0, 0);
        [TitleTextchose setDelegate:self];
        [TitleTextchose setFont:[UIFont boldSystemFontOfSize:15]];
        [Combi addSubview:TitleTextchose];
        
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 110, 320, self.view.bounds.size.height-204)];
        table.separatorColor = [UIColor grayColor];
        table.backgroundColor = [UIColor clearColor];
        table.rowHeight =60;
        
        table.delegate = self;
        table.dataSource = self;
        
        [table setEditing:YES animated:YES];
        [table setEditing:YES];
        
        [table setEditing:NO];
    [self.view addSubview:table];
        
    }
    return self;
}
-(void) setTextview: (NSString *) text

{
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView setText:@""];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtPath:[[Vraagantwoord valueForKey:@"url"] objectAtIndex:indexPath.row] error:NULL];
        
        
        [SaveData RemovetRecording:[[Vraagantwoord valueForKey:@"ID"] objectAtIndex:indexPath.row]];
        
        [Vraagantwoord removeObjectAtIndex:indexPath.row];
        
        
        [table reloadData];
        
        
        
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        
        
        
    }
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [Vraagantwoord count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(RecordCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
 
    
    [cell FillAllItems:[[Vraagantwoord valueForKey:@"ID"] objectAtIndex:indexPath.row] index:indexPath.row];
    
    [cell.title setText:[[Vraagantwoord valueForKey:@"title"] objectAtIndex:indexPath.row]];
    
    [cell getparent:self];
    
    
    
    //play.png
    
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    return 60;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    
    
    
    
    
}

-(void)hideAndseek:(NSString*)set
{
    
}


-(void)whatLabel:(UILabel*)set;

{
    
    extra =(TekstIDLabel*)set;
    
   
  
    
}


- (RecordCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    NSString *identifier = @"CellIdentifier";
    RecordCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
	if (cell == nil) {
        
        cell = [[RecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    else
        
    {
        
    }
  
    
    return cell;
    
    
}



-(void) Action:(UIButton*)sender

{
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
   
   for (NSInteger i=0 ; i<=4; i++) {
        
            UIButton *setimage2 = (UIButton *)[Combi viewWithTag:140+i];
              [setimage2 setBackgroundColor:[UIColor whiteColor]];
        
    }
    
    if ([sender.titleLabel.text isEqualToString:@"Save"]) {
        
        
        
        NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDir = [paths2 objectAtIndex:0];
        
        BOOL success;
        
        
        //FileManager - je maakt dus toegang tot je files in je app met Filemanager.
        NSFileManager *FileManager = [NSFileManager defaultManager];
        
        success = [FileManager fileExistsAtPath:[NSString stringWithFormat:@"%@/Caches/%@.pcm", documentsDir, TitleTextchose.text]];
        
        
        
        if (success) {
        
        
        [sender setBackgroundColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:0.4]];
           [TitleTextchose resignFirstResponder];
        
        
        NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDir = [paths2 objectAtIndex:0];
        
     
        NSMutableDictionary *set = [[NSMutableDictionary alloc] init];
        [set setObject:TitleTextchose.text forKey:@"title"];
        [set setObject: [appDelegate newUUID] forKey:@"ID"];
        [set setObject: [NSString stringWithFormat:@"%@/Caches/%@.pcm", documentsDir, TitleTextchose.text] forKey:@"url"];
        
        [SaveData insertRecoring:set];
        [Vraagantwoord addObject:set];
            
        }
        else
        {
            /* //27-06-2017 changes
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:[NSString stringWithFormat:@"Please record something first!"]
                                                           delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
            [alert show];
            */
            
            UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"Please record something first!",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                
            });

            
        }
        
        
        
        double delayInSeconds = 2.0;
dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
      [sender setBackgroundColor:[UIColor whiteColor]];
    
      
    
    [table reloadData];
});
        
    }
    
    
    if ([sender.titleLabel.text isEqualToString:@"Play"]) {
        
        NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDir = [paths2 objectAtIndex:0];
        
        
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Caches/%@.pcm", documentsDir, TitleTextchose.text]];
        
        
        [sender setBackgroundColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:0.4]];
        
         [RecordingAct playRecording:url];
        
    }
    if ([sender.titleLabel.text isEqualToString:@"Record"]) {
        
        
        if ([TitleTextchose.text isEqualToString:@"Fill in a name"]) {
           
            /* //27-06-2017 changes
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:[NSString stringWithFormat:@"fill in a name first!"]
                                                           delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
            [alert show];
            */
             
            UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"fill in a name first!",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alertcontroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alertcontroller animated:YES completion:nil];
                
            });
            

            
            double delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [sender setBackgroundColor:[UIColor whiteColor]];
            });
        }
        else
        {
          [sender setBackgroundColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:0.4]];
        
        if ([TitleTextchose.text length]>0) {
        
             [RecordingAct startRecording:TitleTextchose.text parant:sender];
        }
        else
        {
           
        }
            
        }
    }
    if ([sender.titleLabel.text isEqualToString:@"Stop"]) {
       
          [sender setBackgroundColor:[UIColor colorWithRed:0.282 green:0.580 blue:0.561 alpha:0.4]];
        
        [RecordingAct stopRecording:TitleTextchose.text];
        [RecordingAct PlayingStop];
        
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [sender setBackgroundColor:[UIColor whiteColor]];
        });
        
    }
    
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)select:(ActionArrow*)sender
{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [[NSUserDefaults standardUserDefaults] setObject:sender.titleLabel.text forKey: @"recordingMusic"];
    [[NSUserDefaults standardUserDefaults] setObject:[[Vraagantwoord valueForKey:@"url"]objectAtIndex:[[Vraagantwoord valueForKey:@"ID"] indexOfObject:sender.titleit]] forKey: @"recordingURL"];
    
    appDelegate.recordingURL =[[NSUserDefaults standardUserDefaults]  objectForKey:@"recordingURL"];
    appDelegate.recordingMusic =[[NSUserDefaults standardUserDefaults]  objectForKey:@"recordingMusic"];
    
    
    [extra setText:[[Vraagantwoord valueForKey:@"title"]objectAtIndex:[[Vraagantwoord valueForKey:@"ID"] indexOfObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"recordingMusic"]]]];
    
   
    
    [table reloadData];
    
    
}

-(void)setChoice:(NSString*)gothere

{
    
}


-(void)PresentView:(ActionArrow*)sender

{
    
    
    
    NSURL *url = [[NSURL alloc] initWithString:[[[Vraagantwoord valueForKey:@"url"]objectAtIndex:[[Vraagantwoord valueForKey:@"ID"] indexOfObject:sender.titleit]]stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
     
    [RecordingAct playRecording:url];
    
    
}

-(void)getParant:(UIViewController*)parant

{
    parantIt = (NewMedecineController*)parant;
}


-(void) backAction
{
    
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [parantLabel setText:TitleText.text];
        
        if ([[NSUserDefaults standardUserDefaults]  objectForKey:@"recordingMusic"]) {
            [parantIt.ChosenMedicen setObject:[[NSUserDefaults standardUserDefaults]  objectForKey:@"recordingMusic"] forKey:@"Recording"];
            
        }
        else
        {
            [parantIt.ChosenMedicen setObject:@"444" forKey:@"Recording"];
            
        }
       
        
        
        
        
    });
    
  
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
