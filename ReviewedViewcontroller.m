//
//  ReviewedViewcontroller.m
//  CFMedcare
//
//  Created by Livecast02 on 09-10-14.
//  Copyright (c) 2014 Menopur. All rights reserved.
//

#import "ReviewedViewcontroller.h"
#define MIN_CELL_HEIGHT 20

@interface ReviewedViewcontroller ()

@end

@implementation ReviewedViewcontroller

- (void)viewDidLoad {

         [self.view.layer setMasksToBounds:YES];
    
    [self setTitle:NSLocalizedString(@"Endorsed by",nil)];
    
    
    // this will appear as the title in the navigation bar
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    // ^-Use NSTextAlignmentCenter for older SDKs.
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
    [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
    label.text = self.title;
    [label sizeToFit];
    
  
//    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *a1 = [[UIButton alloc]initWithFrame:CGRectMake(2, 2, 30, 30)];
    [a1 setTag:44];
//    [a1 setFrame:CGRectMake(2, 2, 104/1.9, 72/1.9)];
    [a1 addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    NSString *deviceType = [UIDevice currentDevice].systemVersion;
    NSRange range = NSMakeRange (0, 1);
    //////////////////////////////////////////////////////////////////////////////////////////NSLog((@"%@",[deviceType substringWithRange:range]);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    else
    {
        [a1 setImage:[UIImage imageNamed:@"Arrow7.png"] forState:UIControlStateNormal];
        a1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = random;
    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]]];
  
    
    
    UIScrollView *scrollViewSpread = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
    [scrollViewSpread setCanCancelContentTouches:NO];
    scrollViewSpread.showsHorizontalScrollIndicator = YES;
//    [scrollViewSpread setBackgroundColor:[UIColor colorWithRed:0.886 green:0.937 blue:0.957 alpha:1.000]]; //06-07-2017 changes
    //250, 241, 235)
    [scrollViewSpread setBackgroundColor:[UIColor colorWithRed:250.0f/255.0f green:241.0f/255.0f blue:235.0f/255.0f alpha:1.000]];
    
    scrollViewSpread.delaysContentTouches=YES;
    scrollViewSpread.clipsToBounds = YES;
    scrollViewSpread.scrollEnabled = YES;
    scrollViewSpread.pagingEnabled = YES;
    [scrollViewSpread setDelegate:self];
    scrollViewSpread.showsHorizontalScrollIndicator=YES;
    [scrollViewSpread flashScrollIndicators];
    scrollViewSpread.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.view  addSubview:scrollViewSpread];
    
    
    
    
    UILabel *roundone = [[UILabel alloc] initWithFrame:CGRectMake(16, 18, 6, 6)];
    [roundone setBackgroundColor:[UIColor colorWithRed:0.337 green:0.357 blue:0.369 alpha:1.000]];
    [roundone.layer setCornerRadius:3];
    [roundone.layer setMasksToBounds:YES];
    
    [scrollViewSpread addSubview:roundone];
    
    
    
    UITextView *DiscaimerText = [[UITextView alloc] initWithFrame:CGRectMake(24, 6, self.view.frame.size.width-40, 50)];
    [DiscaimerText setBackgroundColor:[UIColor clearColor]];
    [DiscaimerText setTextColor:[UIColor colorWithRed:0.337 green:0.357 blue:0.369 alpha:1.000]];
    [DiscaimerText setTag:123+1];
    [DiscaimerText setText:NSLocalizedString(@"CFDaily has been reviewed and approved by the European Cystic Fibrosis Society (ECFS).", nil)];
    [DiscaimerText setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [DiscaimerText setUserInteractionEnabled:YES];
    [DiscaimerText setEditable:NO];
    [DiscaimerText setScrollEnabled:YES];
    [scrollViewSpread addSubview:DiscaimerText];
    
    
    UIImageView *background =[[UIImageView alloc] initWithImage:[UIImage imageNamed:NSLocalizedString(@"ecfs_reviewed.png",nil)]];
    [background setFrame:CGRectMake((self.view.frame.size.width-100)/2,  60, 100, 100)];
    
    [scrollViewSpread addSubview:background];
  
    
    
    UIButton *Extra = [[UIButton alloc]  initWithFrame:CGRectMake((self.view.frame.size.width-100)/2,  50, 100, 100)];
    [Extra setBackgroundColor:[UIColor clearColor]];
    //[Extra addTarget:self action:@selector(url) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:Extra];
    

    
    UITextView *DiscaimerText2 = [[UITextView alloc] initWithFrame:CGRectMake(24, 155, self.view.frame.size.width-40, self.view.frame.size.height-66)];
    [DiscaimerText2 setBackgroundColor:[UIColor clearColor]];
    [DiscaimerText2 setTextColor:[UIColor colorWithRed:0.337 green:0.357 blue:0.369 alpha:1.000]];
    [DiscaimerText2 setTag:123+1];
    [DiscaimerText2 setText:NSLocalizedString(@"CFDaily has been reviewed by the UK's National Health Service (NHS) and is now included in the NHS Choices Health Apps Library. The library is a service from the NHS that helps patients find safe and trusted Apps to help manage their health. The NHS reviews all Apps accepted into the library to ensure they are clinically safe, contain accurate information and are relevant to patients.", nil)];
    [DiscaimerText2 setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [DiscaimerText2 setUserInteractionEnabled:YES];
    [DiscaimerText2 setEditable:NO];
    [DiscaimerText2 setScrollEnabled:YES];
    [scrollViewSpread addSubview:DiscaimerText2];
    
    
    float newheigt = [self calculateTextHeighTitle:DiscaimerText2.text width:self.view.frame.size.width-40];
    

    
   
     
     UILabel *roundtwo = [[UILabel alloc] initWithFrame:CGRectMake(16, 167, 6, 6)];
     [roundtwo setBackgroundColor:[UIColor colorWithRed:0.337 green:0.357 blue:0.369 alpha:1.000]];
     [roundtwo.layer setCornerRadius:3];
    [roundtwo.layer setMasksToBounds:YES];
     [scrollViewSpread addSubview:roundtwo];
    

    
    //500 × 139 pixels
        
    UIImageView *logo =[[UIImageView alloc] initWithImage:[UIImage imageNamed:NSLocalizedString(@"nhs.png",nil)]];
    //11-07-2017
   // [logo setFrame:CGRectMake((self.view.frame.size.width-(500/2))/2,  155+newheigt, 500/2, 139/2)];
    [logo setFrame:CGRectMake((self.view.frame.size.width-(500/2))/2,  155+newheigt, 500/2, 170/2)];
    [scrollViewSpread addSubview:logo];
    
    
    //11-07-2017
    //UIButton *Extranext = [[UIButton alloc]  initWithFrame:CGRectMake((self.view.frame.size.width-(500/2))/2,  155+newheigt, 500/2, 139/2)];
    UIButton *Extranext = [[UIButton alloc]  initWithFrame:CGRectMake((self.view.frame.size.width-(500/2))/2,  155+newheigt, 500/2, 170/2)];
    [Extranext setBackgroundColor:[UIColor clearColor]];
    [Extranext addTarget:self action:@selector(url) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:Extranext];
    
    
   // UITextView *DiscaimerText3 = [[UITextView alloc] initWithFrame:CGRectMake(24, 155+newheigt+(139/2), self.view.frame.size.width-40, 80)]; //11-07-2017
    UITextView *DiscaimerText3 = [[UITextView alloc] initWithFrame:CGRectMake(24, 155+newheigt+(170/2), self.view.frame.size.width-40, 80)];
    [DiscaimerText3 setBackgroundColor:[UIColor clearColor]];
    [DiscaimerText3 setTextColor:[UIColor colorWithRed:0.337 green:0.357 blue:0.369 alpha:1.000]];
    [DiscaimerText3 setTag:123+1];
    [DiscaimerText3 setText:NSLocalizedString(@"CFDaily has been tried and tested by myhealthapps.net's patients panel and is now available in the myealthapps library.  Myhealthapps.net brings together the world's favourite healthcare apps.", nil)];
    [DiscaimerText3 setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [DiscaimerText3 setUserInteractionEnabled:YES];
    [DiscaimerText3 setEditable:NO];
    [DiscaimerText3 setScrollEnabled:YES];
    [scrollViewSpread addSubview:DiscaimerText3];
    
    //11-07-2017
    //UILabel *roundtree = [[UILabel alloc] initWithFrame:CGRectMake(16, 154+newheigt+(139/2)+13, 6, 6)];
    UILabel *roundtree = [[UILabel alloc] initWithFrame:CGRectMake(16, 154+newheigt+(170/2)+13, 6, 6)];
    [roundtree setBackgroundColor:[UIColor colorWithRed:0.337 green:0.357 blue:0.369 alpha:1.000]];
    [roundtree.layer setCornerRadius:3];
    [roundtree.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:roundtree];
    
    //198 × 184 pixels
    UIImageView *logo3 =[[UIImageView alloc] initWithImage:[UIImage imageNamed:NSLocalizedString(@"MYHEALTH_ENGLISH.png",nil)]];
    //[logo3 setFrame:CGRectMake((self.view.frame.size.width-(198/2))/2,  185+newheigt+(139/2)+50, 198/2, 184/2)]; //11-07-2017
    [logo3 setFrame:CGRectMake((self.view.frame.size.width-(198/2))/2,  185+newheigt+(170/2)+50, 198/2, 184/2)];
    [scrollViewSpread addSubview:logo3];
    
    
    
   // UIButton *Extranext3 = [[UIButton alloc]  initWithFrame:CGRectMake((self.view.frame.size.width-(198/2))/2,  185+newheigt+(139/2)+60, 198/2, 184/2)]; //11-07-2017
    UIButton *Extranext3 = [[UIButton alloc]  initWithFrame:CGRectMake((self.view.frame.size.width-(198/2))/2,  185+newheigt+(170/2)+60, 198/2, 184/2)];
    [Extranext3 setBackgroundColor:[UIColor clearColor]];
    [Extranext3 addTarget:self action:@selector(urlhealth) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewSpread addSubview:Extranext3];
     [scrollViewSpread setContentSize:CGSizeMake(320, 630)];
    
    //  http://apps.nhs.uk/
    

    [scrollViewSpread performSelector:@selector(flashScrollIndicators) withObject:nil afterDelay:0.2];

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (CGFloat)calculateTextHeighTitle:(NSString*)text width:(CGFloat)nWidth
{
    
    
    CGSize constraint = CGSizeMake(nWidth, 20000.0f);
    CGRect textRect = [text boundingRectWithSize:constraint
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:12]}
                                         context:nil];
    
    CGSize size = textRect.size;
    CGFloat height = MAX(size.height, MIN_CELL_HEIGHT);
    
    
    return height+30;
}


-(void)url

{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://apps.nhs.uk/"]]; //26-06-2017 change
    
   //[[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://apps.nhs.uk/"] options:@{} completionHandler:nil];
    
    
}

-(void)urlhealth

{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://myhealthapps.net"]]; //26-06-2017 change
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://myhealthapps.net"] options:@{} completionHandler:nil];
}


-(void) backAction
{
    
    
    [[self navigationController] popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
