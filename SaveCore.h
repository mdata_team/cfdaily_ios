//
//  SaveOrGetData.h
//  Eric_Coolen
//
//  Created by Glenn on 18-12-12.
//  Copyright (c) 2012 Glenn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StartViewController.h"

@interface SaveCore : NSObject



//We willen de data halen uit de database en deze in een NSMutableArray zetten

///de functie moet dus een Array tereug geven

+(void)insertMedecineSQLDouble:(NSMutableDictionary*)items setnames:(NSArray*)names;
+(NSString*)seeIflate:(NSMutableDictionary*)items;
+(void)insertHistorySQL:(NSMutableDictionary*)items;

+(void)insertNotfication:(NSData*)items notificion:(NSString*)ID date:(NSDate*)set changedate:(NSDate*)time;
+(void)RemovetMedice:(NSString*)items;
+(NSDate*)getdate:(NSDate*) set with:(NSString*)difference what:(NSString*)late;
+(void)insertMedecineSQL:(NSMutableDictionary*)items setnames:(NSArray*)names;
+(NSMutableArray*) getSetting;
+(NSMutableDictionary*) getMedicinewithID:(NSString*)ID;
+(NSMutableArray*) getProfiles:(NSArray*)itemsnamen;
+(NSMutableArray*) getMedicinewithProfileID:(NSString*)ID;




@end
