//
//  SaveOrGetData.m
//  Eric_Coolen
//
//  Created by Glenn on 18-12-12.
//  Copyright (c) 2012 Glenn. All rights reserved.
//

#import "SaveCore.h"
#import "sqlite3.h"
#import "AppDelegate.h"
#import "DownloadImages.h"
#import "StartViewController.h"
#import "GetData.h"
#import "InsertPatientSyncManager.h"
#import "MedsProfileViewController.h"
#import "InsertMedSyncManager.h"
#import "Notifications.h"
#import "History.h"


@implementation SaveCore


#pragma mark - Core Data stack


+(void)insertMedecineSQLDouble:(NSMutableDictionary*)items setnames:(NSArray*)names
{
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    BOOL exists = NO;
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "SELECT ID_Medicine from Medicine WHERE (ID_Medicine) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [[items objectForKey:@"ID_Medicine"] UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            exists = YES;
         
        }
    }
    sqlite3_finalize(syncStatementCheck);
    
    
    if (exists) {
        
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
 
        
        
        NSArray *names2 =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",@"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"ID_Medicine", nil];
        
        
        sql = "UPDATE Medicine SET ID=?, Name=?, Strength=?, Type=?, Profile=?, Schedule=?, Frequency=?, Remaining=?, Filling=?, Refilling=?, Doctor=?, Prescription=?, Notes=?, Image=?, ColorRed=?, ColorGreen=?, Colorblue=?, Coloralpha=?, Audio=?, Quantity=?, Duration=?, Instructions=?, PerscriptionImage=?, Starting=?, Recording=?, Frequency_text=?, Times=?, Dates=?, Message=? Day_number=? WHERE (ID_Medicine) = (?)";
        
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
       
           for (NSInteger i =0; i < [names2 count]; i++) {
             
                
                sqlite3_bind_text(statement, i+1, [[items valueForKey:[names2 objectAtIndex:i]]  UTF8String], [[items valueForKey:[names2 objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                
                
            }
            
            if(sqlite3_step(statement)==SQLITE_DONE)
            {
                
                
            }
            else
            {
                
                
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
        
        
    }
    else
    {
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        
        
        sql = "INSERT INTO Medicine (ID, Name, Strength, Type, Profile, Schedule, Frequency, Remaining, Filling, Refilling, Doctor, Prescription, Notes, Image, ID_Medicine, ColorRed, ColorGreen, Colorblue, Coloralpha, Audio, Quantity,Duration,Instructions, PerscriptionImage, Starting, Recording, Frequency_text, Times, Dates, Message, Day_number) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
           for (NSInteger i =0; i < [names count]; i++) {
                
              
                
                sqlite3_bind_text(statement, i+1, [[items valueForKey:[names objectAtIndex:i]]  UTF8String], [[items valueForKey:[names objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                
                
            }
        }
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            
            
        }
        else
        {
            
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    
    
    
}



+(NSString*)seeIflate:(NSMutableDictionary*)items

{
    

    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    
    NSDateFormatter *dateFormatLevel = [[NSDateFormatter alloc] init];
    [dateFormatLevel setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormatLevel setDateFormat:[delegate convertString:@"dd/MM/yyyy hh:mm a"]];
    
    
  
    
    NSDate *lateDate =[self getdate:[dateFormatLevel dateFromString:[items valueForKey:@"Time_Action"]] with:[[[GetData getSetting] valueForKey:@"Early"] objectAtIndex:0] what:@"Early"];
    
    NSDate *earlyDate =[self getdate:[dateFormatLevel dateFromString:[items valueForKey:@"Time_Action"]] with:[[[GetData getSetting] valueForKey:@"Late"] objectAtIndex:0] what:@"Late"];
    
      //////////////////////////////////////////////////////NSLog(@"%@ %@ %@",lateDate, [dateFormatLevel dateFromString: [items valueForKey:@"Original_Time"]], earlyDate);

    
    NSString *Taken =@"Taken";
    
    
    
      //////////////////////////////////////////////////////NSLog(@"%d %d", NSOrderedAscending, NSOrderedDescending);

    //////////////////////////////////////////////////////NSLog(@"%d", [earlyDate compare:[dateFormatLevel dateFromString: [items valueForKey:@"Original_Time"]]]);
    
     //////////////////////////////////////////////////////NSLog(@"%d", [lateDate compare:[dateFormatLevel dateFromString: [items valueForKey:@"Original_Time"]]]);
    
    if  ([earlyDate compare:[dateFormatLevel dateFromString: [items valueForKey:@"Original_Time"]]]==NSOrderedAscending && [lateDate compare:[dateFormatLevel dateFromString:[items valueForKey:@"Original_Time"]]]==NSOrderedDescending)
        
    {
        Taken =@"Taken";
        
    }
    else if  ([lateDate compare:[dateFormatLevel dateFromString: [items valueForKey:@"Original_Time"]]]==NSOrderedAscending && [earlyDate compare:[dateFormatLevel dateFromString: [items valueForKey:@"Original_Time"]]]==NSOrderedAscending)
        
    {
        
      
        Taken =@"Taken earlier";
        
        
    }
    else if  ([lateDate compare:[dateFormatLevel dateFromString: [items valueForKey:@"Original_Time"]]]==NSOrderedDescending && [earlyDate compare:[dateFormatLevel dateFromString: [items valueForKey:@"Original_Time"]]]==NSOrderedDescending)
        
    {
        
        Taken =@"Taken late";
        
    }
    
    
    

    return Taken;
}



+(void)insertHistorySQL:(NSMutableDictionary*)items
{
    
   
   ////NSLog(@"%@", items);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    
    if ([[items valueForKey:@"Taken"] isEqualToString:@"Skipped"]) {
        
    }
    else if ([[items valueForKey:@"Taken"] isEqualToString:@"Postponed"]) {
        
    }
    else
    {
        
        [items  setObject:[self seeIflate:items] forKey:@"Taken"];
        
    }
    NSDate *now = [[NSDate alloc] init];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [format setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
    [items  setObject:[format stringFromDate:now] forKey:@"Action_Date"];
    
    
    
    [appDelegate insertHistory:items];
    
}


+(void)insertNotfication:(NSData*)items notificion:(NSString*)ID date:(NSDate*)set changedate:(NSDate*)time;
{
    
   
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
  
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:[delegate convertString:@"dd-MM-yyyy hh:mm a"]];
    
    
    NSMutableDictionary *total = [[NSMutableDictionary alloc] init];
    [total setObject:ID forKey:@"ID"];
    [total setObject:[dateFormat stringFromDate:time] forKey:@"Firedate"];
    [total setObject:[dateFormat stringFromDate:set] forKey:@"ChangeDate"];
    
    
  
     
   
    
}




+(NSMutableArray*) getMedicinewithProfileID:(NSString*)ID


{
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSError *error = nil;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Medicine"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
   for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        Medicine *set = [fetchedObjects objectAtIndex:i];
        
        if ([ID isEqualToString:set.id]) {
            
            
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            [item setObject:set.id forKey:@"ID"];
            [item setObject:set.name forKey:@"Name"];
            [item setObject:set.strength forKey:@"Strength"];
            [item setObject:set.type forKey:@"Type"];
            [item setObject:set.profile forKey:@"Profile"];
            [item setObject:set.schedule forKey:@"Schedule"];
            [item setObject:set.frequency forKey:@"Frequency"];
            [item setObject:set.remaining forKey:@"Remaining"];
            [item setObject:set.filling forKey:@"Filling"];
            [item setObject:set.refilling forKey:@"Refilling"];
            [item setObject:set.doctor forKey:@"Doctor"];
            [item setObject:set.prescription forKey:@"Prescription"];
            [item setObject:set.notes forKey:@"Notes"];
            [item setObject:set.image forKey:@"Image"];
            [item setObject:set.id_medicine forKey:@"ID_Medicine"];
            [item setObject:set.colorRed forKey:@"ColorRed"];
            [item setObject:set.colorGreen forKey:@"ColorGreen"];
            [item setObject:set.colorblue forKey:@"Colorblue"];
            [item setObject:set.coloralpha forKey:@"Coloralpha"];
            [item setObject:set.audio forKey:@"Audio"];
            [item setObject:set.quantity forKey:@"Quantity"];
            [item setObject:set.duration forKey:@"Duration"];
            [item setObject:set.instructions forKey:@"Instructions"];
            [item setObject:set.perscriptionImage forKey:@"PerscriptionImage"];
            
            if (set.starting1) {
               [item setObject:set.starting1 forKey:@"Starting"];
            }
            else
            {
                NSDate *now = [[NSDate alloc] init];
                [item setObject:now forKey:@"Starting"];
            }
           
            [item setObject:set.recording forKey:@"Recording"];
            [item setObject:set.frequency_text forKey:@"Frequency_text"];
            [item setObject:set.times forKey:@"Times"];
            [item setObject:set.dates forKey:@"Dates"];
            [item setObject:set.message forKey:@"Message"];
            [item setObject:set.day_number forKey:@"Day_number"];
            [item setObject:set.phone_doctor forKey:@"Phone_Doctor"];
            [item setObject:set.phone_pharmacy forKey:@"Phone_Pharmacy"];
            [item setObject:set.email_doctor forKey:@"eMail_Doctor"];
            [item setObject:set.email_pharmacy forKey:@"eMail_Pharmacy"];
            [item setObject:set.pharmacy forKey:@"Pharmacy"];
            [item setObject:set.strength_unit forKey:@"Strength_unit"];
             [item setObject:set.dose_unit forKey:@"Dose_unit"];
             [item setObject:set.location forKey:@"Location"];
            
            
            
            
       
            
            [Take addObject:item];
            
            
            
        }
        
    }
 
    return Take;
    
}

+(NSMutableArray*) getProfiles:(NSArray*)itemsnamen


{
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSError *error = nil;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Profiles"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
   for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        
        Profiles *newCourse = [fetchedObjects objectAtIndex:i];
        
        [item setObject:newCourse.name forKey:@"Name"];
        [item setObject:newCourse.id forKey:@"ID"];
        [item setObject:newCourse.colorRed forKey:@"ColorRed"];
        [item setObject:newCourse.colorGreen forKey:@"ColorGreen"];
        [item setObject:newCourse.coloralpha forKey:@"Coloralpha"];
        [item setObject:newCourse.colorblue forKey:@"Colorblue"];
        [item setObject:newCourse.audio forKey:@"Audio"];
        [item setObject:newCourse.silence forKey:@"Silence"];
        [item setObject:newCourse.vibrate forKey:@"Vibrate"];
        [item setObject:[UIImage imageWithData:newCourse.image] forKey:@"Image"];
        
        [Take addObject:item];
        
        
    }
    
    
  
    
    return Take;
    
    
}


+(NSMutableDictionary*) getMedicinewithID:(NSString*)ID


{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Medicine"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
    
   for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        Medicine *set = [fetchedObjects objectAtIndex:i];
        
        if ([set.id_medicine isEqualToString:ID]) {
            
            [item setObject:set.id forKey:@"ID"];
            [item setObject:set.name forKey:@"Name"];
            [item setObject:set.strength forKey:@"Strength"];
            [item setObject:set.type forKey:@"Type"];
            [item setObject:set.profile forKey:@"Profile"];
            [item setObject:set.schedule forKey:@"Schedule"];
            [item setObject:set.frequency forKey:@"Frequency"];
            [item setObject:set.remaining forKey:@"Remaining"];
            [item setObject:set.filling forKey:@"Filling"];
            [item setObject:set.refilling forKey:@"Refilling"];
            [item setObject:set.doctor forKey:@"Doctor"];
            [item setObject:set.prescription forKey:@"Prescription"];
            [item setObject:set.notes forKey:@"Notes"];
            [item setObject:set.image forKey:@"Image"];
            [item setObject:set.id_medicine forKey:@"ID_Medicine"];
            [item setObject:set.colorRed forKey:@"ColorRed"];
            [item setObject:set.colorGreen forKey:@"ColorGreen"];
            [item setObject:set.colorblue forKey:@"Colorblue"];
            [item setObject:set.coloralpha forKey:@"Coloralpha"];
            [item setObject:set.audio forKey:@"Audio"];
            [item setObject:set.quantity forKey:@"Quantity"];
            [item setObject:set.duration forKey:@"Duration"];
            [item setObject:set.instructions forKey:@"Instructions"];
            [item setObject:set.perscriptionImage forKey:@"PerscriptionImage"];
            if (set.starting1) {
                [item setObject:set.starting1 forKey:@"Starting"];
            }
            else
            {
                NSDate *now = [[NSDate alloc] init];
                [item setObject:now forKey:@"Starting"];
            }
            [item setObject:set.recording forKey:@"Recording"];
            [item setObject:set.frequency_text forKey:@"Frequency_text"];
            [item setObject:set.times forKey:@"Times"];
            [item setObject:set.dates forKey:@"Dates"];
            [item setObject:set.message forKey:@"Message"];
            [item setObject:set.day_number forKey:@"Day_number"];
            [item setObject:set.phone_doctor forKey:@"Phone_Doctor"];
            [item setObject:set.phone_pharmacy forKey:@"Phone_Pharmacy"];
            [item setObject:set.email_doctor forKey:@"eMail_Doctor"];
            [item setObject:set.email_pharmacy forKey:@"eMail_Pharmacy"];
            [item setObject:set.pharmacy forKey:@"Pharmacy"];
            [item setObject:set.strength_unit forKey:@"Strength_unit"];
            [item setObject:set.dose_unit forKey:@"Dose_unit"];
            [item setObject:set.location forKey:@"Location"];
            
            
            break;
        }
        
    }
    
    
    return item;
    
}


+(NSMutableArray*) getHistoryTotal


{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
 
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    NSError *error = nil;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"History"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
   for (NSInteger i =0; i < [fetchedObjects count]; i++) {
        
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        
        History *set = [fetchedObjects objectAtIndex:i];
        
        [item setObject:set.id forKey:@"ID"];
        [item setObject:set.date forKey:@"Date"];
        [item setObject:set.time forKey:@"Time"];
        [item setObject:set.name forKey:@"Name"];
        [item setObject:set.dose forKey:@"Dose"];
        [item setObject:set.taken forKey:@"Taken"];
        [item setObject:set.idmedicine forKey:@"ID_medicine"];
        [item setObject:set.count forKey:@"Count"];
    
        
        
        }
        
        
  
    
    return Take;
    
}



+(void)RemovetMedice:(NSString*)items;
{
    
    AppDelegate *myApp =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [myApp removeHistory:items];
    [myApp removeNotification:items];
    [myApp removeMedicine:items];
   
    
}

+(NSDate*)getdate:(NSDate*) set with:(NSString*)difference what:(NSString*)late;


{
    
    
    NSDate *Timeless;
    
    if ([late isEqualToString:@"Late"]) {
        
       
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];

        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:set];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:set];
//26-06-2017 change
//        NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:set];
//        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:set];
        
        NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
        //[dateCompsEarly setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute-[difference intValue]];
        
        [dateCompsEarly setSecond:0];
        Timeless = [calendar dateFromComponents:dateCompsEarly];
    }
    
    else
    {
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:set];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:set];

        //26-06-2017 change
//        NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:set];
//        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:set];
        
        NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute+[difference intValue]];
        
        [dateCompsEarly setSecond:0];
        Timeless = [calendar dateFromComponents:dateCompsEarly];
        
    }
    
    return Timeless;
}



+(void)insertMedecineSQL:(NSMutableDictionary*)items setnames:(NSArray*)names;


{
    
    ////////////////////////////////////////////////////////////NSLog(@"%@", items);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate insertMedicine:items];
 
    
    
    
    
}

+(NSMutableArray*) getSetting


{
    
  
    
    NSArray *names =[NSArray arrayWithObjects:@"Second_alert",@"Disturb_in",@"Disturb_out",@"Late",@"Early",@"History",@"ID",@"Disturb_out_ad",@"Disturb_in_ad", nil];
    
    
    NSMutableArray *Take = [[NSMutableArray alloc] init];
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *selectStatement;
    
    
    NSString* searchQuery =@"SELECT Second_alert,Disturb_in,Disturb_out,Late,Early,History,ID,Disturb_out_ad, Disturb_in_ad from Settings";
    
    const char *sql =   [searchQuery UTF8String];
    
    //en beginnen een aktie
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            
            
           for (NSInteger i =0; i < [names count]; i++) {
                
                
                
                [item  setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, i)] forKey:[names objectAtIndex:i]];
                
                
                
            }
            
            
            [Take addObject:item];
            
            
            
            
        }
        
    }
    
   //05-09-2017
    sqlite3_finalize(selectStatement);
    sqlite3_close(database);
    
    return Take;
  
    
}



@end


