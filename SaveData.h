//
//  SaveOrGetData.h
//  Eric_Coolen
//
//  Created by Glenn on 18-12-12.
//  Copyright (c) 2012 Glenn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StartViewController.h"

@interface SaveData : NSObject



//We willen de data halen uit de database en deze in een NSMutableArray zetten

///de functie moet dus een Array tereug geven

//plus vergeten nu kan ik hem wel ophalen in Fristviewcontroller
+(void)insertProfileSQLDouble:(NSMutableDictionary*)items setnames:(NSArray*)names new:(StartViewController*) sender;


+(void)insertMusic:(NSMutableDictionary*)items;
+(void)insertSetting:(NSString*)item setnames:(NSString*)Data;
+(void)insertAudiointoSQL:(NSString*)ID;
+(void)RemovetMedice:(NSString*)items;
+(void)RemovetRecording:(NSString*)ID;
+(void)insertRecoring:(NSMutableDictionary*)items;
+(void)RemovetMusic:(NSString*)ID;
+ (UIImage *)thumbWithSideOfLength:(float)length image:(UIImage*)mainImage;
+(void)RemoveNotifications:(NSString*)ID;
+(NSString *) change: (NSString *) dumb;

+(void)DeleteHistory:(NSString*)set;
+(NSString *)stringCleaner:(NSString *)yourString;
+(void)RemovetHistroy:(NSString*)ID;
+(void)insertNumbers:(NSMutableArray*)items;
+(void)insertRemanining:(NSString*)set WithID:(NSString*)theID;
+(BOOL)saveinstellingen:(NSString*)choice;
+(void)insertAktiveMeds:(NSMutableDictionary*)items;
+(void) setUpDatabase:(NSString*)Columns onTable:(NSString*)table;
+(NSDate*)getdate:(NSDate*) set with:(NSString*)difference what:(NSString*)late;

//ophalen



@end
