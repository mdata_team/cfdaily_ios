//
//  SaveOrGetData.m
//  Eric_Coolen
//
//  Created by Glenn on 18-12-12.
//  Copyright (c) 2012 Glenn. All rights reserved.
//

#import "SaveData.h"
#import "sqlite3.h"
#import "AppDelegate.h"
#import "DownloadImages.h"
#import "StartViewController.h"
#import "GetData.h"
#import "InsertPatientSyncManager.h"
#import "MedsProfileViewController.h"
#import "InsertMedSyncManager.h"


@implementation SaveData


#pragma mark - Core Data stack


+(void) setUpDatabase:(NSString*)Columns onTable:(NSString*)table
{
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm:00"];
    
    sqlite3_stmt *syncStatementCheck;
    
    
    //
    //  const char *sql = "SELECT * from History WHERE (ID_medicine) = (?)";
    
    //SELECT c FROM information_schema.columns WHERE table_name = '<table_name>'
    
    
    NSString *deleteStatementNS2 = [NSString stringWithFormat:
                                    @"SELECT %@ from %@", Columns,table];
    

    
    const char *sql =   [deleteStatementNS2 UTF8String];
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
    }
    else
        
    {
        char *errorMsg;
        
        
        NSString *createQuestionTable = [NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ TEXT DEFAULT ' ' NOT NULL", table, Columns];
        
        if(sqlite3_exec(database, [createQuestionTable UTF8String], NULL, NULL,&errorMsg) != SQLITE_OK)
        {
            sqlite3_close(database);
            NSAssert1(0, @"Error creating table: %s", errorMsg);
            sqlite3_free(errorMsg);
        }
        
        
    
        
    }
    
    
    
    
    sqlite3_finalize(syncStatementCheck);
    sqlite3_close(database);
    
    
    
}



//CREATE TABLE "Music" ("ID" , "title" , "url" )



+(void)insertSetting:(NSString*)item setnames:(NSString*)Data
{
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    
    sqlite3_stmt *statement;
    
    
    
    NSString *StatementString = [NSString stringWithFormat:
                                 @"UPDATE Settings SET %@=? WHERE (ID) = (1)", item];
    
    const char *sql =   [StatementString UTF8String];
    
    if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(statement, 1, [Data UTF8String], [Data length], SQLITE_TRANSIENT);
        
    }
    
    if(sqlite3_step(statement)==SQLITE_DONE)
    {
        
        
    }
    else
    {
        
        
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    
    
}








+(void)RemoveNotifications:(NSString*)ID
{
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    BOOL exists = NO;
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "DELETE Firedate from Notifications WHERE (Firedate) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            exists = YES;
        }
    }
    sqlite3_finalize(syncStatementCheck);
    sqlite3_close(database);
    
    
}



+(void)RemovetMusic:(NSString*)ID
{
    
    
    
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    BOOL exists = NO;
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "DELETE from Music WHERE (ID) = (?)";
    
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            exists = YES;
        }
    }
    sqlite3_finalize(syncStatementCheck);
    sqlite3_close(database);
    
    
}



+(void)insertMusic:(NSMutableDictionary*)items;
{
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    BOOL exists = NO;
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "DELETE from Music WHERE (title) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [[items valueForKey:@"title"] UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            exists = YES;
        }
    }
    sqlite3_finalize(syncStatementCheck);
    sqlite3_close(database);
    
    
    
    if (exists) {
        
        
        
    }
    else
        
    {
        
        NSArray *names2 =[NSArray arrayWithObjects:@"ID",@"title",@"url", nil];
        
        
        sqlite3 *database = [AppDelegate getNewDBconnection];
        
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        
        
        sql = "INSERT INTO Music (ID,title,url) VALUES (?,?,?)";
        
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
           for (NSInteger i =0; i < [names2 count]; i++) {
                
                
                
                sqlite3_bind_text(statement, i+1, [[items valueForKey:[names2 objectAtIndex:i]]  UTF8String], [[items valueForKey:[names2 objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                
                
                
            }
        }
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            
            
            
        }
        else
        {
            
            
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    
    
    
}



+(void)RemovetHistroy:(NSString*)ID
{
    
  
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    BOOL exists = NO;
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "DELETE from History WHERE (ID) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck,1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            exists = YES;
        }
    }
    
  
    
    const char *sql2 = "DELETE from Notifications WHERE (ID) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql2, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            exists = YES;
        }
    }
    sqlite3_finalize(syncStatementCheck);
    sqlite3_close(database);

    
    
    sqlite3_finalize(syncStatementCheck);
    sqlite3_close(database);
    
    
    
    
    
}



+(void)RemovetRecording:(NSString*)ID
{
    
    
    
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    BOOL exists = NO;
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "DELETE from Recordings WHERE (ID) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            exists = YES;
        }
    }
    sqlite3_finalize(syncStatementCheck);
    sqlite3_close(database);
    
    
}


+(void)DeleteHistory:(NSString*)set

{
    
  
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "DELETE from History WHERE (Original_Time) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [set UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            
        }
    }
    
    
    const char *sql2 = "DELETE from Notifications WHERE (Firedate) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql2, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [set UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
         
        }
    }
    sqlite3_finalize(syncStatementCheck);
    sqlite3_close(database);
    
    
    
}





+(void)insertRecoring:(NSMutableDictionary*)items;
{
    
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    BOOL exists = NO;
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "DELETE from Recordings WHERE (title) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [[items valueForKey:@"title"] UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            exists = YES;
        }
    }
    sqlite3_finalize(syncStatementCheck);
    sqlite3_close(database);
    
    
    
    if (exists) {
        
        
        
    }
    else
        
    {
        
        NSArray *names2 =[NSArray arrayWithObjects:@"ID",@"title",@"url", nil];
        
        
        sqlite3 *database = [AppDelegate getNewDBconnection];
        
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        
        
        sql = "INSERT INTO Recordings (ID,title,url) VALUES (?,?,?)";
        
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
           for (NSInteger i =0; i < [names2 count]; i++) {
                
                
                
                sqlite3_bind_text(statement, i+1, [[items valueForKey:[names2 objectAtIndex:i]]  UTF8String], [[items valueForKey:[names2 objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                
                
                
            }
        }
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            
            
            
        }
        else
        {
            
            
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    
    
    
}





+(void)RemovetMedice:(NSString*)items

{
    
}


+(void)insertMedecineSQL2:(NSMutableDictionary*)items setnames:(NSArray*)names
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate insertMedicine:items];
   
    

    
    
    NSString *ID_copy;
    NSString *ID_copy2;
    NSString *ID_Medicine_copy;
    
  
    NSArray *names2 =[NSArray arrayWithObjects:@"ID",@"Name",@"Strength",@"Type",@"Profile",@"Schedule",@"Frequency",@"Remaining",@"Filling",@"Refilling",@"Doctor",@"Prescription",@"Notes",@"Image",@"ID_Medicine",@"ColorRed",@"ColorGreen",@"Colorblue",@"Coloralpha",@"Audio",@"Quantity",@"Duration",@"Instructions",@"PerscriptionImage",@"Starting",@"Recording",  @"Frequency_text",@"Times",@"Dates",@"Message",@"Day_number",@"Phone_Doctor",@"Phone_Pharmacy",@"eMail_Doctor",@"eMail_Pharmacy",@"Pharmacy",@"Dose_unit",@"Strength_unit",@"Location",@"active",nil];
    
    
    
    //@"Doctor_phone",@"Doctor_mail",
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    BOOL exists = NO;
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "SELECT ID_Medicine from Medicine WHERE (ID_Medicine) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [[items objectForKey:@"ID_Medicine"] UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
           for (NSInteger i =0; i < [names2 count]; i++) {
                
                if ([[names objectAtIndex:i] isEqualToString:@"ID_Medicine"]) {
                    
                    ID_Medicine_copy= [items valueForKey:[names objectAtIndex:i]];
                }
                
                else if ([[names objectAtIndex:i] isEqualToString:@"ID"]) {
                    
                    ID_copy= [items valueForKey:[names objectAtIndex:i]];
                }
                
                
                
            }
            exists = YES;
            
           
        }
    }
    sqlite3_finalize(syncStatementCheck);
    
    
    sqlite3_stmt *selectStatement;
    
    
    
    const char *sql2 = "SELECT ID FROM Medicine WHERE (ID_Medicine) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql2, -1, &selectStatement, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(selectStatement, 1, [[items objectForKey:@"ID_Medicine"] UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            ID_copy2= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatement, 0)];
            
            
        }
        
    }
    
    
    
    if (exists) {
        
        if ([ID_copy isEqualToString:ID_copy2])
            
        {
            
          
            
            sqlite3_stmt *statement;
            const char *sql = nil;
            
            
            
            sql = "UPDATE Medicine SET ID=?, Name=?, Strength=?, Type=?, Profile=?, Schedule=?, Frequency=?, Remaining=?, Filling=?, Refilling=?, Doctor=?, Prescription=?, Notes=?, Image=?, ColorRed=?, ColorGreen=?, Colorblue=?, Coloralpha=?, Audio=?, Quantity=?, Duration=?, Instructions=?, PerscriptionImage=?, Starting=?, Recording=?, Frequency_text=?,Times=? ,Dates=?, Message=?, Day_number=?, Phone_Doctor=?,Phone_Pharmacy=?,eMail_Doctor=?,eMail_Pharmacy=?,Pharmacy=? WHERE (ID_Medicine) = (?)";
            
            
            
            if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
            {
                
                
                
               for (NSInteger i =0; i < [names2 count]; i++) {
                    
                    
                    
                    sqlite3_bind_text(statement, i+1, [[items valueForKey:[names2 objectAtIndex:i]]  UTF8String], [[items valueForKey:[names2 objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                    
                    
                }
                
                if(sqlite3_step(statement)==SQLITE_DONE)
                {
                    
                   
                }
                else
                {
                    
                    
                }
                sqlite3_finalize(statement);
                sqlite3_close(database);
            }
            
            
        }
        
        else
        {
            
            sqlite3_stmt *statement;
            const char *sql = nil;
            
            
            
            sql = "INSERT INTO Medicine (ID, Name, Strength, Type, Profile, Schedule, Frequency, Remaining, Filling, Refilling, Doctor, Prescription, Notes, Image, ID_Medicine, ColorRed, ColorGreen, Colorblue, Coloralpha, Audio, Quantity,Duration,Instructions, PerscriptionImage, Starting, Recording, Frequency_text, Times, Dates, Message, Day_number,Phone_Doctor,Phone_Pharmacy,eMail_Doctor,eMail_Pharmacy,Pharmacy) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            
            
            
            if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
            {
               for (NSInteger i =0; i < [names count]; i++) {
                    
                    if ([[names objectAtIndex:i] isEqualToString:@"ID_Medicine"])
                    {
                        
                        
                        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        
                        NSString *NewID =[appDelegate newUUID];
                        
                        sqlite3_bind_text(statement, i+1, [NewID UTF8String], [NewID length], SQLITE_TRANSIENT);
                    }
                    
                    else if ([[names objectAtIndex:i] isEqualToString:@"ID"]||[[names objectAtIndex:i] isEqualToString:@"Name"]||[[names objectAtIndex:i] isEqualToString:@"Strength"]||[[names objectAtIndex:i] isEqualToString:@"Profile"]||[[names objectAtIndex:i] isEqualToString:@"Type"]||[[names objectAtIndex:i] isEqualToString:@"PerscriptionImage"]||[[names objectAtIndex:i] isEqualToString:@"Image"])
                    {
                        
                        
                        sqlite3_bind_text(statement, i+1, [[items valueForKey:[names objectAtIndex:i]]  UTF8String], [[items valueForKey:[names objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                        
                        
                    }
                    
                    
                    else
                    {
                      
                        sqlite3_bind_text(statement, i+1, [[items valueForKey:[names objectAtIndex:i]]  UTF8String], [[items valueForKey:[names objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                    }
                    
                
                    
                }
            }
            
            if(sqlite3_step(statement)==SQLITE_DONE)
            {
               
                
            }
            else
            {
                
              
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
            
            //26-06-2017 change
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Medicine",nil) message:NSLocalizedString(@"had been copied in an other account!",nil)
//                                                           delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
//            [alert show];
            
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Medicine",nil) message:NSLocalizedString(@"had been copied in an other account!",nil) preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [[SHARED_APPDELEGATE window].rootViewController presentViewController:alert animated:YES completion:nil];
                
            });
            
            
     
        }
        
        
        
    }
    else
    {
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        
        
        sql = "INSERT INTO Medicine (ID, Name, Strength, Type, Profile, Schedule, Frequency, Remaining, Filling, Refilling, Doctor, Prescription, Notes, Image, ID_Medicine, ColorRed, ColorGreen, Colorblue, Coloralpha, Audio, Quantity,Duration,Instructions, PerscriptionImage, Starting, Recording, Frequency_text, Times, Dates, Message, Day_number,Phone_Doctor,Phone_Pharmacy,eMail_Doctor,eMail_Pharmacy,Pharmacy) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
           for (NSInteger i =0; i < [names count]; i++) {
                
              sqlite3_bind_text(statement, i+1, [[items valueForKey:[names objectAtIndex:i]]  UTF8String], [[items valueForKey:[names objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                
                
            }
        }
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            
            
        }
        else
        {
            
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    
    
    
    
}


+(void)insertNumbers:(NSMutableArray*)items

{
    
    if (1==2) {
        
        
        NSArray *names2 =[NSArray arrayWithObjects:@"Remaining",@"Filling",@"Refilling", nil];
        
        
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        sqlite3 *database = [AppDelegate getNewDBconnection];
        
        
        
        sql = "UPDATE Medicine SET Remaining=?, Filling=?, Refilling=? WHERE (ID_Medicine) = (?)";
        
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
            
            
            
           for (NSInteger i =0; i < [names2 count]; i++) {
                
                
                
                sqlite3_bind_text(statement, i+1, [[items valueForKey:[names2 objectAtIndex:i]]  UTF8String], [[items valueForKey:[names2 objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                
                
            }
            
            if(sqlite3_step(statement)==SQLITE_DONE)
            {
                
                
            }
            else
            {
                
                
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
        
        
    }
    
    
    
    
}



+(void)insertRemanining:(NSString*)set WithID:(NSString*)theID
{
    
  
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *statement = NULL;
    
    
    const char *sql = "INSERT INTO History (Refiilling) VALUES (?) WHERE (ID_Medicine) = (?)";
    
    sqlite3_bind_text(statement, 1, [theID UTF8String], -1, SQLITE_TRANSIENT);
    

    if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
    {
        
     
        
        
        sqlite3_bind_text(statement, 2, [set  UTF8String], [set  length], SQLITE_TRANSIENT);
        
        
    }
    
}





+(NSDate*)getdate:(NSDate*) set with:(NSString*)difference what:(NSString*)late;


{
    
    NSDate *Timeless;
    
    if ([late isEqualToString:@"Late"]) {
       
       
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:set];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:set];
        
        NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
        //[dateCompsEarly setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateCompsEarly setDay:[dateComponents day]];
        [dateCompsEarly setMonth:[dateComponents month]];
        [dateCompsEarly setYear:[dateComponents year]];
        [dateCompsEarly setHour:[timeComponents hour]];
        [dateCompsEarly setMinute:timeComponents.minute-[difference intValue]-1];
        
        [dateCompsEarly setSecond:0];
        Timeless = [calendar dateFromComponents:dateCompsEarly];
    }
    
    else
    {
    
   
 
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:set];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:set];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    //[dateCompsEarly setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateCompsEarly setDay:[dateComponents day]];
    [dateCompsEarly setMonth:[dateComponents month]];
    [dateCompsEarly setYear:[dateComponents year]];
    [dateCompsEarly setHour:[timeComponents hour]];
    [dateCompsEarly setMinute:timeComponents.minute+[difference intValue]-1];
    [dateCompsEarly setSecond:0];
    Timeless = [calendar dateFromComponents:dateCompsEarly];
        
    }
    
    return Timeless;
}


+(void)insertAktiveMeds:(NSMutableDictionary*)items
{
    
    
    
    NSArray *names =[NSArray arrayWithObjects:@"ID_Medicine",@"Name",@"Firetime",@"Date",@"Time",nil];
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    BOOL exists = NO;
    
    
    sqlite3_stmt *syncStatementCheck;
    
    const char *sql = "SELECT * from AtiveMeds WHERE (ID_Medicine) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [[items objectForKey:@"ID_Medicine"] UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            exists = YES;
        }
        
    }
    sqlite3_finalize(syncStatementCheck);
    
    
    if (exists) {
        
        
        NSArray *names =[NSArray arrayWithObjects: @"Name",@"Firetime",@"Date",@"Time",@"ID_Medicine",nil];
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        
        sql = "UPDATE AtiveMeds SET Name=?,Firetime=?,Date=?,Time=? WHERE (ID_Medicine) = (?)";
        
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
            
           for (NSInteger i =0; i < [names count]; i++) {
                
                sqlite3_bind_text(statement, i+1, [[items valueForKey:[names objectAtIndex:i]]  UTF8String], [[items valueForKey:[names objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                
                
            }
            
            if(sqlite3_step(statement)==SQLITE_DONE)
            {
                
            }
            else
            {
                
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
        
        
    }
    else
    {
        
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        
        
        sql = "INSERT INTO AtiveMeds (ID_Medicine,Name,Firetime,Date,Time) VALUES (?,?,?,?,?)";
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
           for (NSInteger i =0; i < [names count]; i++) {
                
                sqlite3_bind_text(statement, i+1, [[items valueForKey:[names objectAtIndex:i]]  UTF8String], [[items valueForKey:[names objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                
            }
        }
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            
            
        }
        else
        {
            
            
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    
    
    
}







+(void)insertProfileSQLDouble:(NSMutableDictionary*)items setnames:(NSArray*)names new:(StartViewController*) sender;
{
    
    
    //CREATE TABLE "Profile" ("Name" TEXT,"ID" TEXT,"Image" BLOB,"ColorRed" TEXT,"ColorGreen" TEXT,"Colorblue" TEXT,"Coloralpha" TEXT,"Audio" TEXT, "Silence" TEXT, "Vibrate" TEXT)
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    
    sqlite3_stmt *statement;
    const char *sql = nil;
    
    
    sql = "INSERT INTO Profile (ID, Name,Image,ColorRed,ColorGreen,Colorblue,Coloralpha,Audio,Silence,Vibrate) VALUES (?,?,?,?,?,?,?,?,?,?)";
    
    
    
    NSData *data = [NSData dataWithData:UIImagePNGRepresentation([items valueForKey:@"Image"])];
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
    {
       for (NSInteger i =0; i < [names count]; i++) {
            
           
            if ([[names objectAtIndex:i] isEqualToString:@"Image"]) {
                
                sqlite3_bind_blob(statement, i+1, [data bytes], [data length], SQLITE_TRANSIENT);
                
            }
            else
            {
                
                sqlite3_bind_text(statement, i+1, [[items valueForKey:[names objectAtIndex:i]]  UTF8String], [[items valueForKey:[names objectAtIndex:i]]  length], SQLITE_TRANSIENT);
                
            }
            
            
        }
    }
    
    if(sqlite3_step(statement)==SQLITE_DONE)
    {
        
        
    }
    else
    {
        
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
}





+(void)insertAudiointoSQL:(NSString*)ID;
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSArray *names =[NSArray arrayWithObjects:@"Audio_file",@"ID", nil];
    
    
    
    
    sqlite3 *database = [AppDelegate getNewDBconnectionWith:appDelegate.TitleID];
    
    BOOL exists = NO;
    
    
    sqlite3_stmt *syncStatementCheck;
    
    
    
    
    const char *sql = "SELECT ID FROM Audio WHERE (ID) = (?)";
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
   
            
            exists = YES;
        }
    }
    sqlite3_finalize(syncStatementCheck);
    
    
    if (exists) {
        
        
        
        
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        
        
        NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDir = [paths2 objectAtIndex:0];
        
        
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Caches/Copy.pcm", documentsDir]];
        
        
        NSData *audioData = [NSData dataWithContentsOfURL:url];
        
        
        
        
        sql = "UPDATE Audio SET Audio_file=? where ID=?";
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
          
           for (NSInteger i =0; i < [names count]; i++) {
                
        
                
                if ([[names objectAtIndex:i] isEqualToString:@"Audio_file"]) {
                    
                    
                    sqlite3_bind_blob(statement, i+1, [audioData bytes], [audioData length], SQLITE_TRANSIENT);
                    
                    
                }
                
                else
                {
                 
                    
                    sqlite3_bind_text(statement, i+1, [ID  UTF8String], [ID  length], SQLITE_TRANSIENT);
                    
                    
                    
                }
                
                
            }
            
            if(sqlite3_step(statement)==SQLITE_DONE)
            {
                
                
                
                
            }
            else
            {
                
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
        
        
    }
    else
    {
      
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        
        //je vraagt het eerst pad aan in je array.
        NSString *documentsDirectory = [NSString stringWithFormat:@"%@/Caches/",[paths objectAtIndex:0]];
        
        //en bout het pad op
        NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:@"Copy.pcm"];
        
        
        
        
        NSData *audioData = [NSData dataWithContentsOfFile:databasePath];
      
        sql = "INSERT INTO Audio (Audio_file, ID) VALUES (?,?)";
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
           for (NSInteger i =0; i < [names count]; i++) {
                
                
                if ([[names objectAtIndex:i] isEqualToString:@"Audio_file"]) {
                    
                    sqlite3_bind_blob(statement, i+1, [audioData bytes], [audioData length], SQLITE_TRANSIENT);
                    
                }
                else
                {
                    
                    sqlite3_bind_text(statement, i+1, [ID UTF8String], [ID  length], SQLITE_TRANSIENT);
                    
                }
                
                
            }
        }
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            
            
            
            
        }
        else
        {
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    
    
}





+ (UIImage *)thumbWithSideOfLength:(float)length image:(UIImage*)mainImage {
    
    UIImage *thumbnail;
    
    
 
    if (mainImage.size.width< length) {
        
        CGSize itemSiz1 = CGSizeMake(mainImage.size.width*(length/mainImage.size.width), mainImage.size.height*(length/mainImage.size.width));
        
        UIGraphicsBeginImageContextWithOptions(itemSiz1, NO, 0.0);
        
        CGRect imageRect2 = CGRectMake(0.0, 0.0, itemSiz1.width, itemSiz1.height);
        [mainImage drawInRect:imageRect2];
        
        mainImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    UIImageView *mainImageView = [[UIImageView alloc] initWithImage:mainImage];
    BOOL widthGreaterThanHeight = (mainImage.size.width > mainImage.size.height);
    float sideFull = (widthGreaterThanHeight) ? mainImage.size.height : mainImage.size.width;
    CGRect clippedRect = CGRectMake(0, 0, sideFull, sideFull);
    
    UIGraphicsBeginImageContext(CGSizeMake(length, length));
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextClipToRect( currentContext, clippedRect);
    CGFloat scaleFactor = length/sideFull;
    if (widthGreaterThanHeight) {
        
        
        CGContextTranslateCTM(currentContext, -((mainImage.size.width-sideFull)/2)*scaleFactor, 0);
        
    }
    else {
        CGContextTranslateCTM(currentContext, 0, -((mainImage.size.height - sideFull) / 2) * scaleFactor);
    }
    
    CGContextScaleCTM(currentContext, scaleFactor, scaleFactor);
    [mainImageView.layer renderInContext:currentContext];
    thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    return thumbnail;
    
}






+(BOOL)saveinstellingen:(NSString*)choice

{
    
    
    //ik maak hier dus een BOOL van om de knop te kunnen afvangen
    
    sqlite3 *database = [AppDelegate getNewDBconnection];
    sqlite3_stmt *syncStatementCheck;
    
    
    BOOL exists = NO;
    
    
    //oke we willen eigelijk allen maar weten of hij er in zit
    //als dat zo is moet hij alleen ja of nee terug geven
    //de lijst die onstaat kunnen we dan gebruiken straks om dingen af te vangen!
    
    
    
    
    
    const char *sql = "SELECT Soort FROM Instelling WHERE LOWER(Soort) = LOWER(?)";
    
    if(sqlite3_prepare_v2(database, sql, -1, &syncStatementCheck, NULL) == SQLITE_OK)
    {
        
        sqlite3_bind_text(syncStatementCheck, 1, [choice UTF8String], -1, SQLITE_TRANSIENT);
        
        
        while(sqlite3_step(syncStatementCheck) == SQLITE_ROW)
        {
            
            
            
            exists = YES;
            
            
            
        }
    }
    
    sqlite3_finalize(syncStatementCheck);
    
    
    if (exists) {
        
        
        
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        //gaat niet goed met deze stelling
        sql = "DELETE FROM Instelling WHERE (Soort) = (?)";
        
        
        //jaah
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
            
            sqlite3_bind_text(statement, 1, [choice UTF8String], [choice  length], SQLITE_TRANSIENT);
            
            exists =NO;
            
        }
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            
            
          
            
        }
        else
        {
           
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
        
        
        
    }
    else
    {
      
        
        sqlite3_stmt *statement;
        const char *sql = nil;
        
        sql = "INSERT INTO Instelling (Soort) VALUES (?)";
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK)
        {
            
            sqlite3_bind_text(statement, 1, [choice UTF8String], [choice  length], SQLITE_TRANSIENT);
            
            
            exists =YES;
        }
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            
            
          
            
        }
        else
        {
           
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    
    
    
    //sorry ik zat een andere stelling te vernueken!
    return exists;
    
}






+ (NSString *)stringCleaner:(NSString *)yourString {
    
    NSScanner *theScanner;
    NSString *text = nil;
    
    theScanner = [NSScanner scannerWithString:yourString];
    
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString:@"<iframe src=" intoString:NULL] ;
        
        [theScanner scanUpToString:@"</iframe>" intoString:&text] ;
        
        yourString = [yourString stringByReplacingOccurrencesOfString:
                      [NSString stringWithFormat:@"%@</iframe>", text]
                                                           withString:@""];
        
    }
    
    return yourString;
    
}

+(NSString *) change: (NSString *) dumb
{
    if ([dumb length] ==0) {
        
        dumb=@"-";
        
        return dumb;
        
    }
    
    else
    {
        
        return dumb;
        
    }
    
 
    
}

@end
