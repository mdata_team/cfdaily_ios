//
//  SchedularView.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NewMedecineController,TimersTableView,TekstIDLabel;
@interface SchedularView : UIView
{
    
}
-(void) setShedulare:(NewMedecineController*)parant;
@property (nonatomic, strong) TimersTableView *related;
@property (nonatomic, strong) IBOutlet UIView *ContentView;
@property (nonatomic, strong) IBOutlet TekstIDLabel *strating;
@property (nonatomic, strong) IBOutlet TekstIDLabel *Frequent;
@property (nonatomic, strong) IBOutlet TekstIDLabel *ending;
@property (nonatomic, strong) IBOutlet TekstIDLabel *times;
@property (nonatomic, strong) IBOutlet TekstIDLabel *Dates;
@property (nonatomic, strong) IBOutlet TekstIDLabel *FrequentNumber;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) NSInteger numberit;
@property (nonatomic, strong) NSMutableArray *totalitmes;
-(void)setTableviewExtra:(NSMutableDictionary*)set;
-(void)setTableview:(NSMutableArray*)set;
-(void)setfreguency:(NSString*)ID;
@end
