//
//  SchedularView.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "SchedularView.h"
#import "TekstIDLabel.h"
#import "TimersTableView.h"
#import <QuartzCore/QuartzCore.h>
#import "NewMedecineController.h"
#import "TimersTableView.h"
#import "ActionArrow.h"

@implementation SchedularView
@synthesize related;
@synthesize strating;
@synthesize Frequent;
@synthesize ending;
@synthesize ContentView;
@synthesize count;
@synthesize numberit;
@synthesize times;
@synthesize Dates;
@synthesize FrequentNumber;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        
    }
    return self;
}


-(void) setShedulare:(NewMedecineController*)parant
{
    
    
    
    NSArray *Frequency =[NSArray arrayWithObjects:NSLocalizedString(@"Frequency:",nil),NSLocalizedString(@"Starting:",nil),NSLocalizedString(@"Ending:",nil),NSLocalizedString(@"Time(s) per Day:",nil),nil];
    
    NSArray *names3 =[NSArray arrayWithObjects:@"   Frequency:", nil];
    NSArray *Cell3 =[NSArray arrayWithObjects:@"Frequency#100", nil];
    
    for (NSInteger i=0 ; i<[names3 count]; i++) {
        
        NSArray *numbers = [[Cell3 objectAtIndex:i] componentsSeparatedByString:@"#"];
        
        
        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(265, 70, 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [NameButton setTitleit:[numbers objectAtIndex:0]];
        [NameButton setTag:140];
        [NameButton addTarget:parant action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:NameButton];
        
        
        ActionArrow *Extra = [[ActionArrow alloc]  initWithFrame:CGRectMake(0, 0, 300, 300)];
        [Extra setBackgroundColor:[UIColor clearColor]];
        [Extra setTitleit:[numbers objectAtIndex:0]];
        [Extra setTag:140];
        [Extra addTarget:parant action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:Extra];
        
        
    }
    
    for (NSInteger i=0 ; i<[Frequency count]; i++) {
        
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, (39/2)+(39*i), self.frame.size.width, 40)];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
        [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:14]];
        [MedName setText:[NSString stringWithFormat:@"%@", NSLocalizedString([Frequency objectAtIndex:i], nil)]];
        [MedName setTag:120];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [self addSubview:MedName];
        
        [MedName sizeToFit];
        
        
        if (i==0) {
            
            Frequent = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+10, MedName.frame.origin.y, self.frame.size.width-(MedName.frame.size.width+50), MedName.frame.size.height)];
            [Frequent setTextColor:[UIColor blackColor]];
            [Frequent setFont:[UIFont systemFontOfSize:14]];
            [Frequent setTextAlignment:NSTextAlignmentRight];
            [Frequent setText:@""];
            [Frequent setTitle:@"Frequency_text"];
            [Frequent setTag:120];
            [Frequent setBackgroundColor:[UIColor clearColor]];
            [Frequent setNumberOfLines:3];
            [self addSubview:Frequent];
            
            
         
            
            
            FrequentNumber = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+10, MedName.frame.origin.y, self.frame.size.width-(MedName.frame.size.width+50), MedName.frame.size.height)];
            [FrequentNumber setTextColor:[UIColor blackColor]];
            [FrequentNumber setFont:[UIFont systemFontOfSize:14]];
            [FrequentNumber setTextAlignment:NSTextAlignmentRight];
            [FrequentNumber setText:@""];
            [FrequentNumber setTitle:@"Frequency_number"];
            [FrequentNumber setTag:120];
            [FrequentNumber setBackgroundColor:[UIColor clearColor]];
            [FrequentNumber setNumberOfLines:3];
            [self addSubview:FrequentNumber];
            
            
        }
        if (i==1) {
            
            
            Dates = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+10, MedName.frame.origin.y, self.frame.size.width-(MedName.frame.size.width+50), MedName.frame.size.height)];
            [Dates setTextColor:[UIColor clearColor]];
            [Dates setFont:[UIFont systemFontOfSize:14]];
            [Dates setTextAlignment:NSTextAlignmentRight];
            [Dates setText:@""];
            [Dates setTitle:@"Dates"];
            [Dates setTag:120];
            [Dates setBackgroundColor:[UIColor clearColor]];
            [Dates setNumberOfLines:3];
            [self addSubview:Dates];
            [Dates setAlpha:0];
          
            
            strating = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+10, MedName.frame.origin.y, self.frame.size.width-(MedName.frame.size.width+50), 40)];
            [strating setTextColor:[UIColor blackColor]];
            [strating setFont:[UIFont systemFontOfSize:14]];
            [strating setText:@""];
            [strating setTextAlignment:NSTextAlignmentRight];
            [strating setTitle:@"Starting"];
            [strating setTag:120];
            [strating setBackgroundColor:[UIColor clearColor]];
            [strating setNumberOfLines:2];
            [self addSubview:strating];
            
            
        
            
            
            
        }
        if (i==2) {
            
            
            
            ending = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+10, MedName.frame.origin.y, self.frame.size.width-(MedName.frame.size.width+50), MedName.frame.size.height)];
            [ending setTextColor:[UIColor blackColor]];
            [ending setFont:[UIFont systemFontOfSize:14]];
            [ending setText:@""];
            [ending setTextAlignment:NSTextAlignmentRight];
            [ending setTitle:@"Duration"];
            [ending setTag:120];
            [ending setBackgroundColor:[UIColor clearColor]];
            [ending setNumberOfLines:2];
            [self addSubview:ending];
            
            
        }
        
        if (i==3) {
            
            
            
            times = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+10, MedName.frame.origin.y, self.frame.size.width-(MedName.frame.size.width+50), MedName.frame.size.height)];
            [times setTextColor:[UIColor blackColor]];
            [times setFont:[UIFont systemFontOfSize:14]];
            [times setTextAlignment:NSTextAlignmentRight];
            [times setText:@""];
            [times setTitle:@"Times"];
            [times setTag:120];
            [times setBackgroundColor:[UIColor clearColor]];
            [times setNumberOfLines:3];
            [self addSubview:times];
            
            
        }
        
    }
    
 
     
  
     
     
     
     

    
    
}




-(void)setTableviewExtra:(NSMutableDictionary*)set

{
    
    
    }



-(void)setTableview:(NSMutableArray*)set

{
    
    
    
    numberit =[set count];
    
    
    for (NSInteger i=0 ; i<[set count]; i++) {
        
        UIView  *ContentView2 = (UIView *)[self viewWithTag:160+i];
        [ContentView2 removeFromSuperview];
        
    }
    
    
    for (NSInteger i=0 ; i<[set count]; i++) {
        
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        [dateFormatter setDateFormat:@"EEEE dd, MMMM yyyy"];
        
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
        
        
        [dateFormatter2 setDateFormat:@"EEEE"];
        
   
        
        related.listofitemsCopyed =[[NSArray alloc]init];
        
        related.listofitemsCopyed = [[[set valueForKey:@"Times" ] objectAtIndex:i] componentsSeparatedByString:@""];
        
        
        [related reloadData];
        
        if (numberit==1) {
            
        }
        else
        {
            UIButton *Start = [[UIButton alloc]  initWithFrame:CGRectMake(135, 26, 60/2, 72/2)];
            [Start setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
            [Start addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
            [Start setTag:(i+260)];
            [ContentView addSubview:Start];
            
        }
        
        
        
        [related reloadData];
        
        
    }
    
}


-(NSString*)getnumberOfday:(NSString*)day

{
    NSString *StringReturn;
    
    
    NSArray *names =[NSArray arrayWithObjects:@"Sun",@"Mon",@"Tue",@"Wen",@"",@"Fri",@"Sat", nil];
    NSArray *number =[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7", nil];
    
    
    for (NSInteger i=0 ; i<[names count]; i++) {
        
        if ([day isEqualToString:NSLocalizedString([names objectAtIndex:i],nil)]) {
            
            StringReturn =[number objectAtIndex:i];
        }
        
        
    }
    
    return StringReturn;
    
}

-(void)change:(UIButton*)sender

{
    
    
    
    UIView  *ContentView2 = (UIView *)[self viewWithTag:160+count];
    [self insertSubview:ContentView2 atIndex:[self.subviews count]];
    
    
    
    count++;
    
    if (count==numberit) {
        
        count=0;
        
    }
    
    
    
    
}
-(void)setfreguency:(NSString*)ID


{
    
    
    
    [related setfreguency:ID];
    
}


@end
