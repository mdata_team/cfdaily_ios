//
//  SchedulareCell.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TekstIDLabel, TimeSchedulare;
@interface SchedulareCell : UITableViewCell
@property (nonatomic, strong) TekstIDLabel *Name;
@property (nonatomic, strong) UIButton *add_button;
@property (nonatomic, assign) NSInteger counit;
@property (nonatomic, strong) IBOutlet UIView *content;
@property (nonatomic, strong) IBOutlet TimeSchedulare *Parantit;
@property (nonatomic, strong) IBOutlet UIButton *Take;
-(void)makeCell:(NSMutableDictionary*)sender rowedit:(NSInteger)count;
-(void)gotoScreen:(TekstIDLabel*)sender count:(NSInteger)rowit;
@end
