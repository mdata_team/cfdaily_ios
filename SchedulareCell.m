//
//  SchedulareCell.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "SchedulareCell.h"
#import <QuartzCore/QuartzCore.h>
#import "TekstIDLabel.h"
#import "TimeSchedulare.h"

@implementation SchedulareCell
@synthesize Name;
@synthesize content;
@synthesize add_button;
@synthesize counit;
@synthesize Parantit;
@synthesize Take;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        [self setBackgroundColor:[UIColor clearColor]];
  
        content = [[UIView alloc] initWithFrame:CGRectMake(50, 5, 250, 40)];
        [content setBackgroundColor:[UIColor whiteColor]];
        [content.layer setCornerRadius:10];
        [content.layer setBorderWidth:1.5];
//        [content.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
        [content.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];
        [content.layer setMasksToBounds:YES];
        [self addSubview:content];
        
        Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(10, 0, content.frame.size.width, 40)];
        [Name setTextColor:[UIColor blackColor]];
        [Name setFont:[UIFont systemFontOfSize:14]];
        [Name setText:@"Starting"];
        [Name setTextAlignment:NSTextAlignmentLeft];
        [Name setTitle:@"Starting"];
        [Name setTag:120];
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [content addSubview:Name];
        
        
        add_button = [[UIButton alloc]  initWithFrame:CGRectMake(5, 57, 35, 35)];
        [add_button setImage:[UIImage imageNamed:@"Edit_table.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(removeAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:add_button];
        
        
        
        UIButton *Start = [[UIButton alloc]  initWithFrame:CGRectMake(216, 3, 60/2, 72/2)];
        [Start setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [Start addTarget:self action:@selector(gotoviewcontroller) forControlEvents:UIControlEventTouchUpInside];
        [Start setTag:172];
        [content addSubview:Start];
        
        

        
        
        Take = [[UIButton alloc] initWithFrame:CGRectMake(170,  5 ,75, 30)];
        //(252, 101, 101)
//        [Take setBackgroundColor:[UIColor colorWithRed:0.698 green:0.149 blue:0.224 alpha:1.000]]; //01-07-2017 changes
        [Take setBackgroundColor:[UIColor colorWithRed:0.998 green:0.396 blue:0.396 alpha:1.000]];
        [Take setShowsTouchWhenHighlighted:NO];
        [Take setTitle:NSLocalizedString(@"Remove",nil) forState:UIControlStateNormal];
        [Take setAutoresizesSubviews:YES];
        [Take setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [Take.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
        [Take addTarget:self action:@selector(remove) forControlEvents:UIControlEventTouchUpInside];
        [Take setShowsTouchWhenHighlighted:YES];
        [Take setTag:31];
        [content addSubview:[self setColor:Take]];
        Take.alpha =0;
        
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
            
            
            //NSLog(@"left");
            
            self.transform = CGAffineTransformMakeScale(-1, 1);
            
            
            for (UIView *content1 in self.subviews) {
                
                
                for (UILabel *label in content1.subviews) {
                    
                    
                    if ([label isKindOfClass:[UILabel class]]) {
                        
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                        [label setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                
                for (UIButton *label in content1.subviews) {
                    
                    
                    if ([label isKindOfClass:[UIButton class]]) {
                        
                        label.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
                        [label.titleLabel setTextAlignment:NSTextAlignmentNatural];
                        
                    }
                    
                }
                
                
                
                
            }
            
            
        }
        
        else
        {
            
        }
        
  
    }
    return self;
}


-(UIButton*)setColor:(UIButton*)sender

{
    
    CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
    shineLayer2.frame = sender.layer.bounds;
    shineLayer2.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          nil];
    shineLayer2.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];
    
    
    
    [sender.layer addSublayer:shineLayer2];
    [sender.layer setMasksToBounds:YES];
    
    
    sender.titleLabel.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    sender.titleLabel.layer.shadowOpacity = 1;
    sender.titleLabel.layer.shadowRadius = 1.0;
    [sender.layer setCornerRadius:6];
    [sender.layer setBorderColor:sender.backgroundColor.CGColor];
    [sender.layer setBorderWidth:0.8];
    
    return sender;
    
}

-(void)makeCell:(NSMutableDictionary*)sender rowedit:(NSInteger)count


{
    
    counit=count;

    
    if (count ==0) {
        
        [add_button setAlpha:1];
        //self.navigationItem.rightBarButtonItem = self.myTableViewController.editButtonItem;
        [content setFrame:CGRectMake(50, 55, 250, 40)];
         [add_button setFrame:CGRectMake(5, 57, 35, 35)];
        [self setEditing:NO];
      
    }
    else
    {
        
          [add_button setAlpha:1];
        [add_button setFrame:CGRectMake(5, 5, 35, 35)];
        [self setEditing:NO];
        [content setFrame:CGRectMake(50, 5, 250, 40)];
   
        
    }
  
 
      
    
    [Name setText:[sender valueForKey:@"Time"]];
    
}

-(void)removeAction

{
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
   
    
    
    if (Take.alpha ==1) {
        
        [Take setAlpha:0];
        add_button.transform = CGAffineTransformMakeRotation(0);
        
        
    }
    else
    {
         [Take setAlpha:1];
        add_button.transform = CGAffineTransformMakeRotation(-3.141592653589793/2);
        
        
    }
  
    
    [UIView commitAnimations];
    
    
}

-(void)gotoviewcontroller

{
   
    [Parantit gotoScreen:Name count:counit];
}
-(void)remove

{
    
    //NSLog(@"remove");
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    
  
    
    [Parantit RemoveItem:counit];
       [Take setAlpha:0];
    
        add_button.transform = CGAffineTransformMakeRotation(0);
    [UIView commitAnimations];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
