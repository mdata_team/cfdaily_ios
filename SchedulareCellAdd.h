//
//  SchedulareCell.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TekstIDLabel;
@interface SchedulareCellAdd : UITableViewCell
@property (nonatomic, strong) TekstIDLabel *Name;
-(void)makeCell:(NSMutableDictionary*)sender;
@end
