//
//  SchedulareCell.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "SchedulareCellAdd.h"
#import <QuartzCore/QuartzCore.h>
#import "TekstIDLabel.h"

@implementation SchedulareCellAdd
@synthesize Name;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
     
    }
    return self;
}

-(void)makeCell:(NSMutableDictionary*)sender


{


    NSDateFormatter *dateConvert = [[NSDateFormatter alloc]init];
    [dateConvert setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateConvert setDateFormat:@"EEEE dd, MMMM yyyy"];

     [Name setText:[dateConvert stringFromDate:[sender valueForKey:@"Starting"]]];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

@end
