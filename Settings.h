//
//  Settings.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-08-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Settings : NSManagedObject

@property (nonatomic, retain) NSString * second_alert;
@property (nonatomic, retain) NSString * disturb_in;
@property (nonatomic, retain) NSString * disturb_out;
@property (nonatomic, retain) NSString * late;
@property (nonatomic, retain) NSString * early;
@property (nonatomic, retain) NSString * history;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * disturb_out_ad;
@property (nonatomic, retain) NSString * disturb_in_ad;

@end
