//
//  Settings.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-08-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "Settings.h"


@implementation Settings

@dynamic second_alert;
@dynamic disturb_in;
@dynamic disturb_out;
@dynamic late;
@dynamic early;
@dynamic history;
@dynamic id;
@dynamic disturb_out_ad;
@dynamic disturb_in_ad;

@end
