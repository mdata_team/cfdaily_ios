//
//  SettingsViewcontrollerViewController.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeveyPopColorView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "BufferedNavigationController.h"
#import "NHMailActivity.h"

#import "GAIDictionaryBuilder.h"

@class StartViewController;
@interface SettingsViewcontroller : UIViewController<UITextViewDelegate, UIAlertViewDelegate, MPMediaPickerControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NHCalendarActivityDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, retain) StartViewController *parantIt;
@property (nonatomic, retain) UIImageView *headShot;
@property (nonatomic, retain) MPMusicPlayerController *musicPlayer;
@property (nonatomic, retain) UIButton *button;
@property (nonatomic, retain) LeveyPopColorView *color;
@property (nonatomic, retain) UILabel *Nameit;
@property (nonatomic, retain) UILabel *Sound;
@property (nonatomic, retain) UILabel *ColorField;
@property (nonatomic, retain) NSString *chosenID;
@property (nonatomic, retain) NSMutableArray *Settings;
- (IBAction)OK:(UIButton *)sender;
- (IBAction)Cancel:(UIButton *)sender;
-(void) turnbuttons:(NSString*) setter;
-(void)setItems:(NSMutableDictionary*)set;
-(void)setNew;
-(void)getParant:(id)parant;
- (void)setSelectedColor:(UIColor*)colorit;
-(void)popview;
@end
