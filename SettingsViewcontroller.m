//
//  SettingsViewcontrollerViewController.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "SettingsViewcontroller.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "SaveData.h"
#import "GetData.h"
#import "NameViewController.h"
#import "MedsViewController.h"
#import "StartViewController.h"
#import "HistViewController.h"
#import "InfoViewController.h"
#import "EarlyDurationViewController.h"
#import "LateDurationViewController.h"
#import "AlertViewController.h"
#import "NextAlertViewController.h"
#import "FromToViewController.h"
#import "ShowHistoryViewController.h"
#import "GetData.h"
#import "Notification.h"
#import "SaveData.h"
#import "TekstIDLabel.h"
#import "MultipleViewController.h"
#import "AktionViewController.h"
#import "ActionArrow.h"

@interface SettingsViewcontroller ()

@end

@implementation SettingsViewcontroller

@synthesize color;



@synthesize musicPlayer;
@synthesize headShot;
@synthesize Nameit;
@synthesize Sound;
@synthesize ColorField;
@synthesize chosenID;
@synthesize parantIt;
@synthesize button;


- (void)viewDidLoad
{
    
    //Settings main screen

//    [self googleAnalyticsGroupping];
    
    //********** Google Analytics ****************
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"S_Settings (main screen) _iOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //********** Google Analytics ****************
    
    if ([SHARED_APPDELEGATE isNetworkReachable])
    {
        [SHARED_APPDELEGATE sendHitsInBackground];
    }

    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    //     [self.view.layer setMasksToBounds:YES];
    
    
    self.navigationItem.title =NSLocalizedString(@"Settings",nil);
    // this will appear as the title in the navigation bar
     UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    // ^-Use NSTextAlignmentCenter for older SDKs.
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
      [self.navigationItem.titleView setBackgroundColor:[UIColor clearColor]];
    label.text = self.navigationItem.title;
    [label sizeToFit];
    


    int64_t delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){


        [appDelegate.Navigaioncopy removeAllObjects];
        [appDelegate.Navigaioncopy addObject:self];

        [self.navigationController setViewControllers:appDelegate.Navigaioncopy animated:NO];
        
        
        
    });


    UIScrollView *scrollViewSpread = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
    [scrollViewSpread setCanCancelContentTouches:NO];
    scrollViewSpread.showsHorizontalScrollIndicator = YES;
  [scrollViewSpread setBackgroundColor:[UIColor colorWithRed:250.0f/255.0f green:241.0f/255.0f blue:235.0f/255.0f alpha:1.000]];
 

    scrollViewSpread.delaysContentTouches=YES;
    scrollViewSpread.clipsToBounds = YES;
    scrollViewSpread.scrollEnabled = YES;
    scrollViewSpread.pagingEnabled = YES;
    [scrollViewSpread setDelegate:self];
    scrollViewSpread.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.view  addSubview:scrollViewSpread];


    UIView *Combi = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 78)];
    [Combi setBackgroundColor:[UIColor whiteColor]];
    [Combi.layer setCornerRadius:10];
    [Combi.layer setBorderWidth:1.5];
//    [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Combi.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];

    [Combi.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Combi];



   NSArray *names1 =[NSArray arrayWithObjects:[NSString stringWithFormat:@"   %@#CFDaily %@", NSLocalizedString(@"Version:", nil), [NSString stringWithFormat:@"%@.0",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]]], [NSString stringWithFormat:@"   %@:#%@", NSLocalizedString(@"Language", nil), NSLocalizedString(@"Taals", nil)], nil];

   for (NSInteger i=0 ; i<[names1 count]; i++) {

          NSArray *numbers2 = [[names1 objectAtIndex:i] componentsSeparatedByString:@"#"];
       
        TekstIDLabel *MedName = [[TekstIDLabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 40)];

       //   [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 chanegs
       [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];

        [MedName setFont:[UIFont boldSystemFontOfSize:15]];
        [MedName setText:[numbers2 objectAtIndex:0]];
        [MedName setTag:120];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Combi addSubview:MedName];

       [MedName sizeToFit];
       [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];

       UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];

       //  [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
       [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
       [Combi addSubview:line];


        TekstIDLabel *Name = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+5, 39*i, self.view.frame.size.width-(MedName.frame.size.width+50), 40)];
//        [Name setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
       [Name setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [Name setFont:[UIFont systemFontOfSize:14]];
       //*********** Add New Code for Get Version in only two digit like "1.0" if condition
       NSString *strVersion = [numbers2 objectAtIndex:1];

       NSArray *aryVersion = [strVersion componentsSeparatedByString:@"."];

       if ([aryVersion count] > 2)
       {
           NSLog(@"%@",[NSString stringWithFormat:@"%@.%@",[aryVersion objectAtIndex:0],[aryVersion objectAtIndex:1]]);

           [Name setText:[NSString stringWithFormat:@"%@.%@",[aryVersion objectAtIndex:0],[aryVersion objectAtIndex:1]]];
           [Name setText:@"CFDaily 1.1"]; //30-10-2017
       }
       else
        
       if (i == 0)
       {
          [Name setText:@"CFDaily 1.1"]; //30-10-2017
       }
       else
       {
           [Name setText:[numbers2 objectAtIndex:1]];
       }
        [Name setTag:120];
       if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
           [Name setTextAlignment:NSTextAlignmentLeft];
       }
       
       else
           
       {
           [Name setTextAlignment:NSTextAlignmentRight];
       }
        [Name setBackgroundColor:[UIColor clearColor]];
        [Name setNumberOfLines:3];
        [Combi addSubview:Name];
       
       [Name setCenter:CGPointMake(Name.center.x, MedName.center.y)];
       
  
    }
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        //NSLog(@"left");
           Combi.transform = CGAffineTransformMakeScale(-1, 1);
        
     

                for (TekstIDLabel *label in Combi.subviews) {
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        label.transform = CGAffineTransformMakeScale(-1, 1);
                       
                        
                    }
                    
                }
       
        
        
    } else {
  
    }



    UIView *Assign = [[UIView alloc] initWithFrame:CGRectMake(10, 100, self.view.frame.size.width-20,156)];
    [Assign setBackgroundColor:[UIColor whiteColor]];
    [Assign.layer setCornerRadius:10];
    [Assign.layer setBorderWidth:1];
//    [Assign.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor]; //28-06-2017 changes
    [Assign.layer setBorderColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000].CGColor];

    [Assign.layer setMasksToBounds:YES];
    [scrollViewSpread addSubview:Assign];



    
    NSArray *names2 =[NSArray arrayWithObjects: [NSString stringWithFormat:@"%@:#0#2", NSLocalizedString(@"Reminder Alert", nil)],[NSString stringWithFormat:@"%@:#2#2", NSLocalizedString(@"Set Late Duration", nil)],  [NSString stringWithFormat:@"%@:#4#2", NSLocalizedString(@"Set Early Duration", nil)], [NSString stringWithFormat:@"%@:#6#2", NSLocalizedString(@"Alert Actions", nil)],  [NSString stringWithFormat:@"%@:#8#3", NSLocalizedString(@"Do Not Disturb Alerts", nil)], nil];

      NSArray *names3 =[NSArray arrayWithObjects:@"Reminder",@"Duration",@"Early",@"Action",@"Disturb", nil];

   
   for (NSInteger i=0 ; i<[names2 count]; i++) {


        NSArray *numbers = [[names2 objectAtIndex:i] componentsSeparatedByString:@"#"];

        UILabel *MedName = [[UILabel alloc] initWithFrame:CGRectMake(0, 18.5*[[numbers objectAtIndex:1] intValue], self.view.frame.size.width-20, 18.5*[[numbers objectAtIndex:2] intValue])];
//        [MedName setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
       [MedName setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedName setFont:[UIFont boldSystemFontOfSize:15]];
        [MedName setText:[NSString stringWithFormat:@"   %@", NSLocalizedString([numbers objectAtIndex:0],nil)]];
        [MedName setTag:120];
        [MedName setBackgroundColor:[UIColor clearColor]];
        [MedName setNumberOfLines:3];
        [Assign addSubview:MedName];
       
       [MedName sizeToFit];
       [MedName setCenter:CGPointMake(MedName.center.x, (39*i)+20)];
       
       
       UILabel *line =[[UILabel alloc] initWithFrame:CGRectMake(0, 39*i, self.view.frame.size.width-20, 1)];
//       [line setBackgroundColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 chanegs
       [line setBackgroundColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
       [Assign addSubview:line];
        
        
        
        ActionArrow *NameButton = [[ActionArrow alloc]  initWithFrame:CGRectMake(Assign.frame.size.width-35, (39*i), 60/2, 72/2)];
        [NameButton setImage:[UIImage imageNamed:@"action"] forState:UIControlStateNormal];
        [NameButton setTitleit:[names3 objectAtIndex:i]];
        [NameButton setTag:140];
        [NameButton addTarget:self action:@selector(Action:) forControlEvents:UIControlEventTouchUpInside];
        [Assign addSubview:NameButton];
        
        [NameButton setCenter:CGPointMake(NameButton.center.x, MedName.center.y)];



        TekstIDLabel *tekstContent = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+5, 18.5*[[numbers objectAtIndex:1] intValue], self.view.frame.size.width-(MedName.frame.size.width+80), 18.5*[[numbers objectAtIndex:2] intValue])];
        [tekstContent setTextColor:[UIColor blackColor]];
        [tekstContent setFont:[UIFont systemFontOfSize:14]];
            //[tekstContent setText:[numbers objectAtIndex:0]];
        [tekstContent setTag:489+i];
       if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
           [tekstContent setTextAlignment:NSTextAlignmentLeft];
       }
       
       else
           
       {
           [tekstContent setTextAlignment:NSTextAlignmentRight];
       }
        [tekstContent setBackgroundColor:[UIColor clearColor]];
        [tekstContent setNumberOfLines:10];
        [Assign addSubview:tekstContent];
       
     


        if ([@"Disturb" isEqualToString:[names3 objectAtIndex:i]]) {

        TekstIDLabel *MedNameinfo = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+5, 19.5*[[numbers objectAtIndex:1] intValue], self.view.frame.size.width-(MedName.frame.size.width+80), 36)];
//        [MedNameinfo setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [MedNameinfo setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [MedNameinfo setFont:[UIFont systemFontOfSize:14]];
        [MedNameinfo setTag:120];
        [MedNameinfo setText:@""];
        MedNameinfo.title =[names3 objectAtIndex:i];
            if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                [MedNameinfo setTextAlignment:NSTextAlignmentLeft];
            }
            
            else
                
            {
                [MedNameinfo setTextAlignment:NSTextAlignmentRight];
            }
        [MedNameinfo setBackgroundColor:[UIColor clearColor]];
        [MedNameinfo setNumberOfLines:3];
        [Assign addSubview:MedNameinfo];
            
            
            
            [MedNameinfo setCenter:CGPointMake(MedNameinfo.center.x, MedName.center.y)];
            
        }
        
        else
        {
            TekstIDLabel *MedNameinfo = [[TekstIDLabel alloc] initWithFrame:CGRectMake(MedName.frame.size.width+5, 18.5*[[numbers objectAtIndex:1] intValue], self.view.frame.size.width-(MedName.frame.size.width+80), 36)];
//            [MedNameinfo setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]]; //28-06-2017 changes
            [MedNameinfo setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
            [MedNameinfo setFont:[UIFont systemFontOfSize:14]];
            [MedNameinfo setTag:120];
            [MedNameinfo setText:@""];
            MedNameinfo.title =[names3 objectAtIndex:i];
            if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
                [MedNameinfo setTextAlignment:NSTextAlignmentLeft];
            }
            
            else
                
            {
                [MedNameinfo setTextAlignment:NSTextAlignmentRight];
            }
            [MedNameinfo setBackgroundColor:[UIColor clearColor]];
            [MedNameinfo setNumberOfLines:3];
            [Assign addSubview:MedNameinfo];
            
            [MedNameinfo setCenter:CGPointMake(MedNameinfo.center.x, MedName.center.y)];
            
        }
            //184 × 36 pixels

        UIButton *Cancel = [[UIButton alloc] initWithFrame:CGRectMake(125, self.view.frame.size.height-180, 184,  36)];
//        [Cancel setBackgroundColor:[UIColor colorWithRed:0.553 green:0.000 blue:0.047 alpha:1.000]]; //01-07-2017 changes
       [Cancel setBackgroundColor:[UIColor colorWithRed:0.998 green:0.396 blue:0.396 alpha:1.000]];
        [Cancel setTitle:NSLocalizedString(@"Delete",nil) forState:UIControlStateNormal];
        Cancel.titleLabel.layer.shadowOffset = CGSizeMake(1, 1);
        Cancel.titleLabel.layer.shadowOpacity = 1;
        Cancel.titleLabel.layer.shadowRadius = 1.0;
        [Cancel.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
        [Cancel.titleLabel setTextColor:[UIColor whiteColor]];
        [Cancel setShowsTouchWhenHighlighted:NO];
        [Cancel setAutoresizesSubviews:YES];
        [Cancel addTarget:self action:@selector(Deleteall:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:[self setbutton:Cancel]];

    
        
        
    }
    
    
    if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        
        //NSLog(@"left");
        Assign.transform = CGAffineTransformMakeScale(-1, 1);
        
        
        
        for (UILabel *label in Assign.subviews) {
            
            
            if ([label isKindOfClass:[UILabel class]]) {
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
    ;
                
            }
            
        }
        
        
     
        
        for (TekstIDLabel *label in Assign.subviews) {
            
            
            if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                label.transform = CGAffineTransformMakeScale(-1, 1);
             
                
            }
            
        }
        
        
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSLeftTextAlignment];
        
        
    } else {
        
        //NSLog(@"right");
        //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
    }
    



    [scrollViewSpread setContentSize:CGSizeMake(320, 400)];
    

    
  

    if ([MFMailComposeViewController canSendMail])
        button.enabled = YES;



        // Do any additional setup after loading the view, typically from a nib.
}


-(UIButton*)setbutton:(UIButton*)sender

{
    
    CAGradientLayer *shineLayer2 = [CAGradientLayer layer];
    shineLayer2.frame = sender.layer.bounds;
    shineLayer2.colors = [NSArray arrayWithObjects:
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                          (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                          nil];
    shineLayer2.locations = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.5f],
                             [NSNumber numberWithFloat:0.8f],
                             [NSNumber numberWithFloat:1.0f],
                             nil];
    
    
    
    [sender.layer addSublayer:shineLayer2];
    [sender.layer setMasksToBounds:YES];
    [sender.layer setCornerRadius:4];
    [sender.layer setBorderWidth:1.5];
    [sender.layer setBorderColor:sender.backgroundColor.CGColor];
    
    return sender;
}


-(void)Deleteall:(UIButton*)set
{

    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:NSLocalizedString(@"Are you sure you want to delete all notifications?", nil)];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464750];
    [alert addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [alert addButtonWithTitle:NSLocalizedString(@"Yes",nil)];
    [alert show];
    
  
    
    
    
}



-(void)Reset:(UIButton*)set

{
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:NSLocalizedString(@"What exactly is to be reset??",nil)];
    [alert setMessage:@""];
    [alert setDelegate:self];
    [alert setTag:464749];
    [alert addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [alert addButtonWithTitle:NSLocalizedString(@"Yes",nil)];
    [alert show];
    
    
    
    
    
}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)index{
    
    
    if (alertView.tag ==464750) {
        if (index==0) {
            


    
        }
        
        else
        {
            [Notification CleareNotification];
            [UIApplication sharedApplication].applicationIconBadgeNumber=0;


        }
    
    }
    
    
    
  else if (alertView.tag ==4500) {
      if (index==0) {
          
          
          
      }
      
      else if (index==1) {
          
      double delayInSeconds = 0.3;
          dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
          dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=WIFI"]];
          });
   
          
      }
      
  }
    
    
   
    else if (alertView.tag ==464749) {
  
    if (index==0) {
        
        
       
    }
    
    else
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        
        [SaveData insertSetting:@"Disturb_in" setnames:@"12:00 AM"];
        [SaveData insertSetting:@"Disturb_out" setnames:@"12:00 AM"];
        [SaveData insertSetting:@"History" setnames:@"0"];
        [SaveData insertSetting:@"Disturb_in_ad" setnames:@"AM"];
        [SaveData insertSetting:@"Disturb_out_ad" setnames:@"AM"];
        [SaveData insertSetting:@"Late" setnames:@"0"];
        [SaveData insertSetting:@"Second_alert" setnames:@"0"];
        [SaveData insertSetting:@"Early" setnames:@"0"];
        
        
        NSArray *names3 =[NSArray arrayWithObjects:@"Alert",@"Reminder",@"Disturb",@"History",@"Duration",@"Early", nil];
        
       for (NSInteger i=0 ; i<[names3 count]; i++) {
            
            UILabel *setimage = (UILabel *)[self.view viewWithTag:489+i];
            
            if ([appDelegate.chosenMusic length] >3) {
                
                if (i == 0) {
                    
                    
                 [setimage setText:appDelegate.chosenMusic];
                    
                }
                
                if (i == 1) {
                    
                    [setimage setText: [NSString stringWithFormat:@"%@ %@", [[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0], @"min"]];
                }
                
                if (i == 2) {
                    
                    //Disturb_in
                    
                    [setimage setText: [NSString stringWithFormat:@"%@\n%@", [[[GetData getSetting] valueForKey:@"Disturb_in"] objectAtIndex:0], [[[GetData getSetting] valueForKey:@"Disturb_out"] objectAtIndex:0]]];
                }
                if (i == 3) {
                    
                    [setimage setText: [NSString stringWithFormat:@"%@ %@", [[[GetData getSetting] valueForKey:@"History"] objectAtIndex:0], @"days"]];
                }
                
                if (i == 4) {
                    
                    [setimage setText: [NSString stringWithFormat:@"%@ %@", [[[GetData getSetting] valueForKey:@"Late"] objectAtIndex:0], @"min"]];
                }
                
                if (i == 5) {
                    
                    [setimage setText: [NSString stringWithFormat:@"%@ %@", [[[GetData getSetting] valueForKey:@"Early"] objectAtIndex:0], @"min"]];
                    
                }
                
            }
            
        }
        
        
    
        
        
        
    }
        
    }
    
    else  if (alertView.tag ==464749)
    {
        if (index==0) {
          
            
                        

            
        }
        
        else
        {
        
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            
            
            [SaveData insertSetting:@"Disturb_in" setnames:@"12:00 AM"];
            [SaveData insertSetting:@"Disturb_out" setnames:@"12:00 AM"];
            [SaveData insertSetting:@"History" setnames:@"22"];
            [SaveData insertSetting:@"Disturb_in_ad" setnames:@"AM"];
            [SaveData insertSetting:@"Disturb_out_ad" setnames:@"AM"];
            [SaveData insertSetting:@"Late" setnames:@"10"];
            [SaveData insertSetting:@"Second_alert" setnames:@"10"];
            [SaveData insertSetting:@"Early" setnames:@"10"];
            
            
            NSArray *names3 =[NSArray arrayWithObjects:@"Alert",@"Reminder",@"Disturb",@"History",@"Duration",@"Early", nil];
            
           for (NSInteger i=0 ; i<[names3 count]; i++) {
                
                UILabel *setimage = (UILabel *)[self.view viewWithTag:489+i];
                
                if ([appDelegate.chosenMusic length] >3) {
                    
                    if (i == 0) {
                        
                         [setimage setText:appDelegate.chosenMusic];
                        
                    }
                    
                    if (i == 1) {
                        
                        [setimage setText: [NSString stringWithFormat:@"%@ %@", [[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0], @"min"]];
                    }
                    
                    if (i == 2) {
                        
                        //Disturb_in
                        [setimage setText: [NSString stringWithFormat:@"%@\n%@", [[[GetData getSetting] valueForKey:@"Disturb_in"] objectAtIndex:0], [[[GetData getSetting] valueForKey:@"Disturb_out"] objectAtIndex:0]]];
                    }
                    if (i == 3) {
                        
                        [setimage setText: [NSString stringWithFormat:@"%@ %@", [[[GetData getSetting] valueForKey:@"History"] objectAtIndex:0], @"days"]];
                    }
                    
                    if (i == 4) {
                        
                        [setimage setText: [NSString stringWithFormat:@"%@ %@", [[[GetData getSetting] valueForKey:@"Late"] objectAtIndex:0], @"min"]];
                    }
                    
                    if (i == 5) {
                        
                        [setimage setText: [NSString stringWithFormat:@"%@ %@", [[[GetData getSetting] valueForKey:@"Early"] objectAtIndex:0], @"min"]];
                        
                    }
                    
                }
                
            }
            

                        
        }
        
        
    }
}




-(void)switchValueReminder:(UISwitch*)sender

{

}

- (IBAction)showMediaPicker:(id)sender
{
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];

    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    [self presentViewController:mediaPicker animated:YES completion:NULL];

}

-(void)viewDidAppear:(BOOL)animated
{

//////////////////////     [self.view.layer setMasksToBounds:YES];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
        [appDelegate.toolbarDown setAlpha:1];
    
    for (UIScrollView *view1 in self.view.subviews) {
        
        
        for (UIView *view2 in view1.subviews) {
            
            
            for (TekstIDLabel *label in view2.subviews) {
                
                
                
                if ([label isKindOfClass:[TekstIDLabel class]]) {
                    
                    
                 
                    if ([label.title isEqualToString:@"Duration"]) {
                        
                        
                       [label setText:[[[GetData getSetting] valueForKey:@"Late"] objectAtIndex:0]];
                    }
                    
                    if ([label.title isEqualToString:@"Action"]) {
                        
                        
                        [label setText:[[[GetData getSetting] valueForKey:@"History"] objectAtIndex:0]];
                    }
                    
                    if ([label.title isEqualToString:@"Early"]) {
                        
                        
                        
                        [label setText:[[[GetData getSetting] valueForKey:@"Early"] objectAtIndex:0]];
                    }
                    
                    if ([label.title isEqualToString:@"Reminder"]) {
                        
                        
                        
                        [label setText:[[[GetData getSetting] valueForKey:@"Second_alert"] objectAtIndex:0]];
                    }
                    if ([label.title isEqualToString:@"Disturb"]) {
                        
                        
                        //Disturb_in
                        [label setText: [NSString stringWithFormat:@"%@\n%@", [[[GetData getSetting] valueForKey:@"Disturb_in"] objectAtIndex:0], [[[GetData getSetting] valueForKey:@"Disturb_out"] objectAtIndex:0]]];
                    }
                    
                    
                    
                    
                }
                
            }
            
        }
    }
    
    
    }

- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{

    MPMediaItem *anItem = (MPMediaItem *)[mediaItemCollection.items objectAtIndex:0];

    NSURL *assetURL = [anItem valueForProperty: MPMediaItemPropertyAssetURL];


    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);

    NSString *documentsDir = [NSString stringWithFormat:@"%@/Caches/",[paths2 objectAtIndex:0]];





    NSData *data2 = [NSData dataWithContentsOfURL:assetURL];

    [data2 writeToFile:[NSString stringWithFormat:@"%@/Copy.mp3", documentsDir] atomically:YES];


    [self dismissViewControllerAnimated:YES completion:NULL];


}



-(void)getCameraPicture:(UIButton*)sender
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate.toolbarDown setAlpha:0];


	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.allowsEditing = YES;
#if (TARGET_IPHONE_SIMULATOR)
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
#endif
	[self presentViewController: picker animated:YES completion:NULL];
}


-(void)imagePickerController:(UIImagePickerController *)picker
      didFinishPickingImage : (UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{



    [headShot setImage:[self thumbWithSideOfLength2:120 image:image]];
    
       UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);

    [self dismissViewControllerAnimated:YES completion:NULL];

}


- (UIImage *)thumbWithSideOfLength2:(float)length image:(UIImage*)mainImage {

    UIImage *thumbnail;

    if (mainImage.size.width< length) {

        CGSize itemSiz1 = CGSizeMake(mainImage.size.width*(length/mainImage.size.width), mainImage.size.height*(length/mainImage.size.width));

        UIGraphicsBeginImageContextWithOptions(itemSiz1, NO, 0.0);

        CGRect imageRect2 = CGRectMake(0.0, 0.0, itemSiz1.width, itemSiz1.height);
        [mainImage drawInRect:imageRect2];

        mainImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }

    UIImageView *mainImageView = [[UIImageView alloc] initWithImage:mainImage];
    BOOL widthGreaterThanHeight = (mainImage.size.width > mainImage.size.height);
    float sideFull = (widthGreaterThanHeight) ? mainImage.size.height : mainImage.size.width;
    CGRect clippedRect = CGRectMake(0, 0, sideFull, sideFull);

    UIGraphicsBeginImageContext(CGSizeMake(length, length));
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextClipToRect( currentContext, clippedRect);
    CGFloat scaleFactor = length/sideFull;
    if (widthGreaterThanHeight) {

        CGContextTranslateCTM(currentContext, -((mainImage.size.width-sideFull)/2)*scaleFactor, 0);

    }
    else {
        CGContextTranslateCTM(currentContext, 0, -((mainImage.size.height - sideFull) / 2) * scaleFactor);
    }

    CGContextScaleCTM(currentContext, scaleFactor, scaleFactor);
    [mainImageView.layer renderInContext:currentContext];
    thumbnail = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return thumbnail;

}


-(void)setItems:(NSMutableDictionary*)set

{



    chosenID = [set valueForKey:@"ID"];

    UIColor *colorit = [UIColor colorWithRed:[[set valueForKey:@"ColorRed"] floatValue] green:[[set valueForKey:@"ColorGreen"]  floatValue] blue:[[set valueForKey:@"Colorblue"]  floatValue] alpha:[[set valueForKey:@"Coloralpha"]  floatValue]];


    [ColorField setBackgroundColor:colorit];
    [Nameit setText:[set valueForKey:@"Name"]];
    [Sound setText:[set valueForKey:@"Audio"]];
    [headShot setImage:[set valueForKey:@"Image"]];


}


- (void) viewWillAppear:(BOOL)animated {

    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
          [appDelegate seeIfNotification];
    appDelegate.FromScreen=NO;
    
    
    [appDelegate seeIfNotification];
    
}


-(void)setNew

{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    chosenID = [appDelegate newUUID];


}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *) picker
{
 


    
	[picker dismissViewControllerAnimated:YES  completion:nil];


}


- (void) mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}




- (IBAction)showMediaPicker
{


 

    
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];

    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.prompt = @"Select songs to play";
    [mediaPicker.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    [self presentViewController:mediaPicker animated:YES completion:NULL];
}


-(void) changeColorFridge
{
    LeveyPopColorView *pinview01 = (LeveyPopColorView *)[self.view  viewWithTag:3459148];
    if (pinview01) {

        [pinview01 removeFromSuperview];


    }
    else
        {


        color = [[LeveyPopColorView alloc] initWithTitle:NSLocalizedString(@"Share Photo to...",nil) options:NULL setIt:2];
            // lplv.isModal = NO;
        [color setTag:3459148];
        [color showInView:self.view animated:YES];
        [color getParent:self get:3];


        }






}


-(void) save:(UIButton*)sender
{



}

-(void) backAction
{


    [[self navigationController] popViewControllerAnimated:YES];

}


-(void) CreateAccount

{

}

-(void)Next:(UIButton*)sender

{


    ////////////////////////////////////////////////////NSLog(@"%@", sender);

    if (sender.tag ==31) {


        StartViewController *controller4 = [[StartViewController alloc]init];
        [self.navigationController pushViewController:controller4 animated:NO];

    }
    if (sender.tag ==32) {



        HistViewController *controller2 = [[HistViewController alloc]init];
        [self.navigationController pushViewController:controller2 animated:NO];


    }
    if (sender.tag ==33) {



        MedsViewController *controller3 = [[MedsViewController alloc]init];
        [self.navigationController pushViewController:controller3 animated:NO];


      
    }
    if (sender.tag ==34) {

        [self Offerte];

    }
    if (sender.tag ==35) {


            //MedicineViewController.h
    }

    if (sender.tag ==36) {

       
    }
    
    
}



-(void)popview

{
    
     
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];

    InfoViewController *controller = [[InfoViewController alloc]init];
    [self presentViewController:controller animated:YES completion:Nil];
}

- (void)Offerte

{

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.toolbarDown setAlpha:0];



}




-(void)Action:(ActionArrow*)sender

{
   
    
    
    if ([sender.titleit isEqualToString:@"Alert"]) {


        AlertViewController *controller = [[AlertViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        
        


    }
    if ([sender.titleit isEqualToString:@"Reminder"]) {


        NextAlertViewController *controller = [[NextAlertViewController alloc]init];
        [self presentViewController:controller animated:YES completion:Nil];


        
        for (UIScrollView *view1 in self.view.subviews) {
            
            
            for (UIView *view2 in view1.subviews) {
                
                
                for (TekstIDLabel *label in view2.subviews) {
                    
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        
                        
                        if ([label.title isEqualToString:@"Reminder"]) {
                            
                            
                            
                            
                            [controller whatLabel:label];
                        }
                        
                    }
                    
                }
                
            }
            
            
        }
        

       
    }

    if ([sender.titleit isEqualToString:@"Action"]) {
        
      



        AktionViewController *controller = [[AktionViewController alloc]init];
        [self presentViewController:controller animated:YES completion:Nil];
        
        
        
        for (UIScrollView *view1 in self.view.subviews) {
            
            
            for (UIView *view2 in view1.subviews) {
                
                
                for (TekstIDLabel *label in view2.subviews) {
                    
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        
                        
                        if ([label.title isEqualToString:@"Action"]) {
                            
                            [controller whatLabel:label];
                        }
                        
                    }
                    
                }
                
            }
            
            
        }
        
 

    }
    if ([sender.titleit isEqualToString:@"History"]) {

        ShowHistoryViewController *controller = [[ShowHistoryViewController alloc]init];
        [self presentViewController:controller animated:YES completion:Nil];


            //ShowHistoryViewController
        
        for (TekstIDLabel *label in self.view.subviews) {
            
            if ([label isKindOfClass:[TekstIDLabel class]]) {
                
                //
                
                if ([label.title isEqualToString:@"Duration"]) {
                    
                    
                    
                    [controller whatLabel:label];
                }
                
            }
            
            
            
            
        }
        


    }
    if ([sender.titleit isEqualToString:@"Duration"]) {

        
        
        LateDurationViewController *controller = [[LateDurationViewController alloc]init];
        [self presentViewController:controller animated:YES completion:Nil];
          for (UIScrollView *view1 in self.view.subviews) {
              
              
              for (UIView *view2 in view1.subviews) {
   
         
            for (TekstIDLabel *label in view2.subviews) {
         
              
                
            if ([label isKindOfClass:[TekstIDLabel class]]) {
                
            
                
                if ([label.title isEqualToString:@"Duration"]) {
                   
                    
                 
                        [controller whatLabel:label];
                }
                
            }
                
        }
            
        }
   }
        

    }
    if ([sender.titleit isEqualToString:@"Early"]) {

        EarlyDurationViewController *controller = [[EarlyDurationViewController alloc]init];
        [self presentViewController:controller animated:YES completion:Nil];
        for (UIScrollView *view1 in self.view.subviews) {
            
            
            for (UIView *view2 in view1.subviews) {
                
                
                for (TekstIDLabel *label in view2.subviews) {
                    
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        
                        
                        if ([label.title isEqualToString:@"Early"]) {
                            
                          
                            
                            [controller whatLabel:label];
                        }
                        
                    }
                    
                }
                
            }
            
        }

    }
    if ([sender.titleit isEqualToString:@"Disturb"]) {
        
        FromToViewController *controller = [[FromToViewController alloc]init];
        [self presentViewController:controller animated:YES completion:Nil];
        for (UIScrollView *view1 in self.view.subviews) {
            
            
            for (UIView *view2 in view1.subviews) {
                
                
                for (TekstIDLabel *label in view2.subviews) {
                    
                    
                    
                    if ([label isKindOfClass:[TekstIDLabel class]]) {
                        
                        
                        
                        if ([label.title isEqualToString:@"Early"]) {
                            
                            
                            
                            [controller whatLabel:label];
                        }
                        
                    }
                    
                }
                
            }
            
        }
    }
    else
        {



       
      
        }

}
-(void)getParant:(id)parant
{
    
}


- (void)setSelectedColor:(UIColor*)colorit{
    

    
    
    [ColorField setBackgroundColor:colorit];
    
    
}

- (IBAction)Cancel:(UIButton *)sender {
    
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}

- (IBAction)OK:(UIButton *)sender {
    
    
    
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

-(void) turnbuttons:(NSString*) setter

{
   
}


//#pragma mark - Google Analytics
//
//-(void)googleAnalyticsGroupping
//{
//    id tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTIC_TRACKING_ID];
//    [tracker set:[GAIFields contentGroupForIndex:5]value:@"Settings"];
//}

@end
