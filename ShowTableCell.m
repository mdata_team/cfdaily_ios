//
//  ShowTableCell.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 09-07-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "ShowTableCell.h"

@implementation ShowTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
