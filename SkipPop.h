//
//  AlertPop.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkipPop : UIView


@property (nonatomic, retain) IBOutlet UIImageView *headShot;
@property (nonatomic, retain) IBOutlet UILabel *Date;
@property (nonatomic, retain) IBOutlet UILabel *Name;
@property (nonatomic, retain) IBOutlet UITextView *Taken;
@property (nonatomic, retain) IBOutlet UILabel *title;



-(void)Choice:(UIButton*)sender;
-(void)changeData:(NSMutableDictionary*)sender date:(NSDate*)fireDate;
-(void)setMultiple;
-(void)setSingle;

@end
