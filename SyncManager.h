#import <Foundation/Foundation.h>

@interface SyncManager : NSObject <NSXMLParserDelegate>{
	NSMutableString *currentItemValue;
	NSMutableDictionary *currentItem;
	NSMutableArray *itemList;
	
	NSMutableData *webData;
	NSXMLParser *xmlParser;
}

@property(nonatomic, retain) NSMutableData *webData;
@property(nonatomic, retain) NSXMLParser *xmlParser;

@property(nonatomic, retain) NSMutableDictionary *currentItem;
@property(nonatomic, retain) NSMutableString *currentItemValue;
@property(nonatomic, retain) NSMutableArray *itemList;

@end
