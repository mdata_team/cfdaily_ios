//
//  AlertPop.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 13-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "TakeOrTookPop.h"
#import <QuartzCore/QuartzCore.h>
#import "GetData.h"
#import "AppDelegate.h"

@implementation TakeOrTookPop
@synthesize headShot;
@synthesize Date;
@synthesize Name;
@synthesize Taken;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
       
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0.0f, 280, 180)];
        
//       [title setBackgroundColor:[UIColor colorWithRed:0.882 green:0.937 blue:0.957 alpha:1.000]];
        [title setTag:233];
//        [title setBackgroundColor:[UIColor colorWithRed:0.460 green:0.631 blue:0.957 alpha:1.000]]; //30-06-2017 changes
        [title setBackgroundColor:[UIColor colorWithRed:0.396 green:0.722 blue:0.533 alpha:1.000]];
        
        [title.layer setCornerRadius:10];
          [title.layer setMasksToBounds:YES];
        [self addSubview:title];
        
        
        CAGradientLayer *shineLayer1 = [CAGradientLayer layer];
        shineLayer1.frame = title.layer.bounds;
        shineLayer1.colors = [NSArray arrayWithObjects:
                              (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                              (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                              (id)[UIColor colorWithWhite:0.75f alpha:0.4f].CGColor,
                              (id)[UIColor colorWithWhite:0.4f alpha:0.4f].CGColor,
                              (id)[UIColor colorWithWhite:0.2f alpha:0.4f].CGColor,
                              nil];
        shineLayer1.locations = [NSArray arrayWithObjects:
                                 [NSNumber numberWithFloat:1.0f],
                                 [NSNumber numberWithFloat:0.8f],
                                 [NSNumber numberWithFloat:0.6f],
                                 [NSNumber numberWithFloat:0.4f],
                                 [NSNumber numberWithFloat:0.2f],
                                 nil];
        [title.layer addSublayer:shineLayer1];
        
        
        
           NSArray *profileItem =[NSArray arrayWithObjects:NSLocalizedString(@"Take Now",nil),NSLocalizedString(@"I Already Took",nil),NSLocalizedString(@"More...",nil), nil];
        
        
       for (NSInteger i=0 ; i<[profileItem count]; i++) {

        
        UIButton *Stelvraag = [[UIButton alloc] initWithFrame:CGRectMake(30, 15+(45*i), 240, 40)];
        
        
        [Stelvraag setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        [Stelvraag setTitle:[profileItem objectAtIndex:i] forState:UIControlStateNormal];
        [Stelvraag.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
//            [Stelvraag setTitleColor:[UIColor colorWithRed:0.141 green:0.365 blue:0.471 alpha:1.000] forState:UIControlStateNormal]; //01-07-2017 chanegs
           [Stelvraag setTitleColor:[UIColor colorWithRed:0.557 green:0.608 blue:0.808 alpha:1.000] forState:UIControlStateNormal];
            [Stelvraag setShowsTouchWhenHighlighted:YES];
             [Stelvraag setTag:200+i];
        [Stelvraag.layer setCornerRadius:8]; 
            [Stelvraag.layer setBorderWidth:1];
//            [Stelvraag.layer setBorderColor:[UIColor colorWithRed:0.216 green:0.627 blue:0.651 alpha:1.000].CGColor]; //01-017-2017 changes
           [Stelvraag.layer setBorderColor:[UIColor clearColor].CGColor];
            [Stelvraag addTarget:appDelegate action:@selector(TakenOrtook:) forControlEvents:UIControlEventTouchUpInside];
            if (i ==0) {
                
                
//                 [Stelvraag setBackgroundColor:[UIColor colorWithRed:0.859 green:0.918 blue:0.949 alpha:1.000]];//01-07-2017 changes
                [Stelvraag setBackgroundColor:[UIColor whiteColor]];
                
            }
            if (i ==1) {
                
//                [Stelvraag setBackgroundColor:[UIColor colorWithRed:0.859 green:0.918 blue:0.949 alpha:1.000]]; //01-07-2017 changes
                [Stelvraag setBackgroundColor:[UIColor whiteColor]];
            }
            if (i ==2) {
                
//             [Stelvraag setBackgroundColor:[UIColor colorWithRed:0.859 green:0.918 blue:0.949 alpha:1.000]]; //01-07-2017 changes
                [Stelvraag setBackgroundColor:[UIColor whiteColor]];
               
            }

        [self addSubview:Stelvraag];
      
 
            
            
        }
        
        
        [self setMultiple];
    
        
        
    }
    return self;
}

-(void)setMultiple

{
    
    UILabel *Label = (UILabel *)[self viewWithTag:(233)];
    [Label setFrame:CGRectMake(Label.frame.origin.x, Label.frame.origin.y, 280, 160)];
    
    
    NSArray *profileItem =[NSArray arrayWithObjects:NSLocalizedString(@"Take All Now",nil),NSLocalizedString(@"I Already Took All",nil),NSLocalizedString(@"More...",nil), nil];
    
   for (NSInteger i=0 ; i<[profileItem count]; i++) {
        
        
        UIButton *button = (UIButton *)[self viewWithTag:(200+i)];
        
        [button setAlpha:1];
        
        
       [button setTitle:[profileItem objectAtIndex:i] forState:UIControlStateNormal];
        
     
        
    }
    
}
-(void)setSingle
    
    {
        
        UILabel *Label = (UILabel *)[self viewWithTag:(233)];
        [Label setFrame:CGRectMake(Label.frame.origin.x, Label.frame.origin.y, 280, 120)];
        
        NSArray *profileItem =[NSArray arrayWithObjects:NSLocalizedString(@"Take Now",nil),NSLocalizedString(@"I Already Took",nil),NSLocalizedString(@"More...",nil), nil];
        
       for (NSInteger i=0 ; i<[profileItem count]; i++) {
            
            
            UIButton *button = (UIButton *)[self viewWithTag:(200+i)];
            
            
            
            [button setTitle:[profileItem objectAtIndex:i] forState:UIControlStateNormal];
            
            if (i==2) {
                [button setAlpha:0];
            }
            
            
        }
        
        
    }

-(void)Choice:(UIButton*)sender
  
  {
      
      [UIView beginAnimations:nil context:NULL];
      [UIView setAnimationBeginsFromCurrentState:YES];
      
      
      
      
      [UIView setAnimationDuration:0.5f];
      
    
      
      if (self.frame.origin.y == 100) {
          
        [self setFrame:CGRectMake(10, 480, 300, 300)];
      }
      else
      {
          
          [self setFrame:CGRectMake(10, 100, 300, 300)];
          
      }
      
      
      
      [UIView commitAnimations];
      
    
      
  }


-(void)changeData:(NSMutableDictionary*)sender date:(NSDate*)fireDate
{

    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat2 setDateFormat:@"hh:mm"];
    
    
    
     NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
 
    [format setDateFormat:@"dd/MM/yyyy"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
     [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)]];
    [dateFormat setDateFormat:[appDelegate  convertString:@"hh:mm a"]];
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:fireDate];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:fireDate];
    
    NSDateComponents *dateCompsEarly = [[NSDateComponents alloc] init];
    [dateCompsEarly setDay:[dateComponents day]];
    [dateCompsEarly setMonth:[dateComponents month]];
    [dateCompsEarly setYear:[dateComponents year]];
    [dateCompsEarly setHour:[timeComponents hour]];
    [dateCompsEarly setMinute:timeComponents.minute+[[[[GetData getSetting] valueForKey:@"Early"] objectAtIndex:0] intValue]];
    [dateCompsEarly setSecond:0];
    NSDate *itemNext = [calendar dateFromComponents:dateCompsEarly];
    
    
    [Date setText:[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:itemNext]]];
        
    
    [headShot setImage:[sender valueForKey:@"Image"]];
  
  
    [Name setText:[sender valueForKey:@"Name"]];
    
  
}



@end
