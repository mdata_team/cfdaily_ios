//
//  TekstIDLabel.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 18-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TekstIDLabel : UILabel

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *realText;
@property (nonatomic, assign) float centerit;

@end
