//
//  TextSubit.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 21-08-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol MyTextViewDelegate <NSObject>
@optional
- (void)textFieldDidDelete;
@end

@interface MyTextField : UITextView<UIKeyInput>

//create "myDelegate"
@property (nonatomic, assign) id<MyTextViewDelegate> myDelegate;
@end
