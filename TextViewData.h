//
//  TextViewData.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 21-09-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextViewData : UIView <UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic, retain) UILabel *Content;
@property (nonatomic, retain) UILabel *Value;
-(void)changeValue;
- (CGFloat)calculateTextHeight:(NSString*)text width:(CGFloat)nWidth fontit:(UIFont*)fontitnow;
@end
