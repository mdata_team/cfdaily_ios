//
//  TextViewData.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 21-09-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "TextViewData.h"
#define MIN_CELL_HEIGHT 20.0f

@implementation TextViewData
@synthesize Content;
@synthesize Value;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor =[UIColor clearColor];
        
        Content = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 140, 35)];
        [Content setBackgroundColor:[UIColor clearColor]];
        [Content setTextAlignment:NSTextAlignmentRight];
        //        [Content setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]]; //28-06-2017 changes
        [Content setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [Content setBackgroundColor:[UIColor clearColor]];
        [Content setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [self addSubview:Content];
        
        
        Value = [[UILabel alloc] initWithFrame:CGRectMake(145, 0, 140, 35)];
        [Value setBackgroundColor:[UIColor clearColor]];
//        [Value setTextColor:[UIColor colorWithRed:0.000 green:0.173 blue:0.396 alpha:1]]; //28-06-2017 changes
        [Value setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]];
        [Value setTag:123+1];
        [Value setTextAlignment:NSTextAlignmentLeft];
        [Value setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [self addSubview:Value];
        
        
        if ([[UIApplication sharedApplication] userInterfaceLayoutDirection] == UIUserInterfaceLayoutDirectionRightToLeft) {
           
             [Content setTextAlignment:NSTextAlignmentLeft];
       
            Content.transform = CGAffineTransformMakeScale(-1, 1);
           
            
        } else {
            
            //NSLog(@"right");
            //[[(NSMutableParagraphStyle *)paraStyle setAlignment:NSRightTextAlignment];
        }
    }
    return self;
}


-(void)changeValue

{
    
    ////////////////////////////////////////////////////////////////////////////////NSLog(@"changeValue%@", Value.text);
    
    if ( [Value.text isEqualToString:@"piece"]) {
      
        
        Content.text  =[Content.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        Value.text  =[Value.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        [Value setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]]; //28-06-2017 add new
        [Value setAlpha:0];
        
        [Content setFrame:CGRectMake(45, 0, [self calculateTextHeight:Content.text width:200 fontit:[UIFont fontWithName:@"Helvetica" size:16]], 35)];
        
        [Value setFrame:CGRectMake(Content.frame.size.width, 0, [self calculateTextHeight:Value.text width:200 fontit:[UIFont fontWithName:@"Helvetica" size:16]], 35)];
        
        
        [self setFrame:CGRectMake(300-(Content.frame.size.width+Value.frame.size.width), self.frame.origin.y, Content.frame.size.width+Value.frame.size.width, 35)];
        
        
    }
    else
    {

   
    Content.text  =[Content.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    Value.text  =[Value.text stringByReplacingOccurrencesOfString:@" " withString:@""];

        [Value setTextColor:[UIColor colorWithRed:0.506 green:0.565 blue:0.780 alpha:1.000]]; //28-06-2017 add new

    [Content setFrame:CGRectMake(-3, 0, [self calculateTextHeight:Content.text width:200 fontit:[UIFont fontWithName:@"Helvetica" size:16]], 35)];
    
      [Value setFrame:CGRectMake(Content.frame.size.width, 0, [self calculateTextHeight:Value.text width:200 fontit:[UIFont fontWithName:@"Helvetica" size:16]], 35)];
    
    
     [self setFrame:CGRectMake(300-(Content.frame.size.width+Value.frame.size.width), self.frame.origin.y, Content.frame.size.width+Value.frame.size.width, 35)];
    
        
    }
    
    
    
}

- (CGFloat)calculateTextHeight:(NSString*)text width:(CGFloat)nWidth fontit:(UIFont*)fontitnow
{
    CGSize constraint = CGSizeMake(20000.0f, nWidth);
	CGSize size = [text sizeWithFont:fontitnow constrainedToSize:constraint lineBreakMode:NSLineBreakByTruncatingTail];
	CGFloat height = MAX(MIN_CELL_HEIGHT, size.width);
    
    height =[[NSString stringWithFormat:@"%0.f", height] floatValue]+18;

	return height;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
