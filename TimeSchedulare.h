//
//  TimeScedulare.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FrequencyViewControllerNew, TekstIDLabel;

@interface TimeSchedulare : UITableView <UITextViewDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *TimesSet;
-(void)getitReload:(NSMutableDictionary*)set;
-(void)RemoveItem:(NSInteger)count;
@property (nonatomic, strong) IBOutlet FrequencyViewControllerNew *Parantit;
-(void)getparant:(UIViewController*)sender;
-(void)gotoScreen:(TekstIDLabel*)sender count:(NSInteger)rowit;


@end
