//
//  TimeScedulare.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "TimeSchedulare.h"
#import "SchedulareCell.h"
#import "SchedulareCellAdd.h"
#import <QuartzCore/QuartzCore.h>
#import "FrequencyViewControllerNew.h"

@implementation TimeSchedulare
@synthesize TimesSet;
@synthesize Parantit;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
       TimesSet =[[NSMutableArray alloc]init];
      
        
        self.delegate = self;
        self.dataSource = self;
       [self setEditing:YES];

        [self setContentSize:CGSizeMake(self.frame.size.width,400)];
        
    
    }
    return self;
}

-(void)getparant:(UIViewController*)sender

{
    
    Parantit =(FrequencyViewControllerNew*)sender;
    
    
    
    
}

-(void)RemoveItem:(NSInteger)count


{
    
    //NSLog(@"%@ %i", TimesSet, count);
    
    
    [TimesSet removeObjectAtIndex:count];
    [self reloadData];
    
    
    BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
    if (strsetting) {
        
        //NSLog(@"YES");
    }
    else
        
    {
        //NSLog(@"NO");
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
        [self.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
        
        NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"am", @"a.m.", @"vorm.", @"μ.μ.", @"AM"];
        
        NSArray *notificationArray= [self.TimesSet filteredArrayUsingPredicate:predicate];
        
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"pm", @"p.m.", @"nachm.", @"π.μ.", @"PM"];
        
        NSArray *notificationArray2= [self.TimesSet filteredArrayUsingPredicate:predicate2];
        
        [self.TimesSet removeAllObjects];
        
        [self.TimesSet addObjectsFromArray:notificationArray];
        [self.TimesSet addObjectsFromArray:notificationArray2];
        
        
        
        NSArray *copy = [self.TimesSet copy];
        NSInteger index = [copy count] - 1;
        for (id object in [copy reverseObjectEnumerator]) {
            if ([self.TimesSet indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                [self.TimesSet removeObjectAtIndex:index];
            }
            index--;
        }
        
        
        //NSLog(@"%@", self.TimesSet);
    }
    
    [self reloadData];

    
    
    
}
-(void)RemoveItem3:(NSInteger)count


{
    
    //NSLog(@"%@ %i", TimesSet, count);
    
    
    [TimesSet removeObjectAtIndex:count];
    [self reloadData];
    
   for (NSInteger i=0 ; i<[Parantit.DayinaRow count]; i++) {
        
        NSMutableDictionary *set = [[NSMutableDictionary alloc]init];
        
        for (id key in [Parantit.DayinaRow objectAtIndex:i]) {
            
            
            
            if ([key isEqualToString:@"Times"]) {
                
                NSString *uploadString = [[Parantit.table.TimesSet valueForKey:@"Time"] componentsJoinedByString:@","];
                
                [set setObject:uploadString forKey:@"Times"];
            }
            
            else
            {
                
                [set setObject:[[Parantit.DayinaRow objectAtIndex:i] objectForKey:key] forKey:key];
                
            }
            
        }
        
        [Parantit.DayinaRow replaceObjectAtIndex:i withObject:set];
        
    }
    
    
    
    
    BOOL strsetting=[[[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_TIME"] boolValue];
    
       if (!strsetting) {
        
        //NSLog(@"YES"); 
        
    }
    else
        
    {
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Date" ascending:TRUE];
        [self.TimesSet sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
        
        NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"am", @"a.m.", @"vorm.", @"μ.μ.", @"AM"];
        
      
        
        NSArray *notificationArray= [self.TimesSet filteredArrayUsingPredicate:predicate];
        
        
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@) or (Date contains %@)", @"pm", @"p.m.", @"nachm.", @"π.μ.",  @"PM"];
        
        
        NSArray *notificationArray2= [self.TimesSet filteredArrayUsingPredicate:predicate2];
        
        [self.TimesSet removeAllObjects];
        
        [self.TimesSet addObjectsFromArray:notificationArray];
        [self.TimesSet addObjectsFromArray:notificationArray2];
        
        
        NSArray *copy = [self.TimesSet copy];
        NSInteger index = [copy count] - 1;
        for (id object in [copy reverseObjectEnumerator]) {
            if ([self.TimesSet indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                [self.TimesSet removeObjectAtIndex:index];
            }
            index--;
        }
   
    
    }
    

    
    
}

-(void)gotoScreen:(TekstIDLabel*)sender count:(NSInteger)rowit;

{
    

    [Parantit gotoScreen:sender count:rowit];
    
    
    

    
}
-(void)getitReload:(NSMutableDictionary*)set


{
    
 
  
}

-(void)getDates:(NSMutableArray*)set

{
    
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
     
        
       [TimesSet removeObjectAtIndex:indexPath.row];
        [self reloadData];
        
        
        
        
        
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        
        
        
    }
    
    if (editingStyle == UITableViewCellEditingStyleInsert) {
     
      
    }
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [TimesSet count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(SchedulareCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row ==0) {
        
        [cell.contentView setFrame:CGRectMake(0, 50, cell.contentView.frame.size.width, 50)];
    }
    else
    {
        
    }
    

    cell.Parantit =self;
    [cell makeCell:[TimesSet objectAtIndex:indexPath.row] rowedit:indexPath.row];
 
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
  
    if (indexPath.row==0)
    return 100;
    else
    return 50;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
  
    
}


- (SchedulareCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    
    
    NSString *identifier = @"CellIdentifier";
    
    SchedulareCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
   
	if (cell == nil) {
     
        cell = [[SchedulareCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
      
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
       
    }
    
    else
        
    {
        
    }
  
    
         return cell;
    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
