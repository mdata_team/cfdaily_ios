//
//  SearchTableView.h
//  Vertex_Dental
//
//  Created by Jeffrey Snijder on 18-04-13.
//  Copyright (c) 2013 Houdah Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimersTableView : UITableView < UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, retain) NSArray *listofitemsCopyed;
@property (nonatomic, retain) NSMutableArray *listoftings;
@property (nonatomic, retain) NSMutableArray *SectionList;
-(void)setfreguency:(NSString*)ID;

@end
