//
//  SearchTableView.m
//  Vertex_Dental
//
//  Created by Jeffrey Snijder on 18-04-13.
//  Copyright (c) 2013 Houdah Software. All rights reserved.
//

#import "TimersTableView.h"
#import "AlertCell.h"

@implementation TimersTableView
@synthesize listofitemsCopyed;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)setfreguency:(NSString*)ID


{
        
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    return [listofitemsCopyed count];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
   
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 20;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    NSString *identifier = @"CellIdentifier";
    AlertCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    
	if (cell == nil) {
        
        cell = [[AlertCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
        
        
    }
    
    else
        
    {
        
    }
    
    cell.textLabel.text = [listofitemsCopyed objectAtIndex:indexPath.row];
    [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
    [cell.textLabel setFont:[UIFont systemFontOfSize:12]];
    
    return cell;
    
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
