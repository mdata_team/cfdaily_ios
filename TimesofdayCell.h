//
//  TimesofdayCell.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 21-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimesofDayViewController.h"

@interface TimesofdayCell : UITableViewCell <UITextViewDelegate>

@property (nonatomic, retain) IBOutlet UIImageView *Postpone;
@property (nonatomic, retain) IBOutlet  TimesofDayViewController *lostView;
@property (nonatomic, retain) IBOutlet UITextView *TitleText;
@property (nonatomic, retain) IBOutlet NSString *timeframe;
@property (nonatomic, retain) IBOutlet NSString *ID;
-(void)getparent:(TimesofDayViewController*)set;
-(void) FillAllItems:(NSMutableDictionary*) sender index:(NSInteger)set;

@end
