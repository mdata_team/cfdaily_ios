//
//  TimesofdayCell.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 21-02-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "TimesofdayCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation TimesofdayCell
@synthesize lostView;
@synthesize TitleText;
@synthesize timeframe;
@synthesize Postpone;
@synthesize ID;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code


     

            //211 × 89 pixels
     
        timeframe= @"AM";


        UIView *Combi = [[UIView alloc] initWithFrame:CGRectMake(40, 2, 270, 44)];
        [Combi setBackgroundColor:[UIColor whiteColor]];
        [Combi.layer setCornerRadius:10];
        [Combi.layer setBorderWidth:1.5];
        [Combi.layer setBorderColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000].CGColor];
        [Combi.layer setMasksToBounds:YES];
        [self addSubview:Combi];




        TitleText = [[UITextView alloc] initWithFrame:CGRectMake(55, 4, 240, 35)];
        [TitleText setBackgroundColor:[UIColor whiteColor]];
            //[TitleText setDelegate:self];
        [TitleText setText:NSLocalizedString(@"",nil)];
        [TitleText setTextColor:[UIColor colorWithRed:0.157 green:0.412 blue:0.678 alpha:1.000]];
        [TitleText setTag:123+1];
        [TitleText setEditable:NO];
        [TitleText setEditable:NO];
        [TitleText becomeFirstResponder];

         TitleText.selectedRange = NSMakeRange([TitleText.text length]-1, 0);
        
        [TitleText setFont:[UIFont boldSystemFontOfSize:15]];
        [self addSubview:TitleText];


    }
    
    return self;
}
-(void) FillAllItems:(NSMutableDictionary*) sender index:(NSInteger)set

{

    ID  = [sender valueForKey:@"ID"];
    [TitleText setText:[NSString stringWithFormat:@"%@ %@", [sender valueForKey:@"CurrentTime"], [sender valueForKey:@"Moment"]]];



}

-(void) GreenIconButtonClicked:(id) sender
{

    [lostView addItem:[NSString stringWithFormat:@"%@ %@", TitleText.text, timeframe]];
    
}


-(void)getparent:(TimesofDayViewController*)set
{
    lostView =set;



    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
