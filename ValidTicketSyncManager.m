//
//  MedSyncManager.m
//  CFMedcare
//
//  Created by Jeffrey Snijder on 20-04-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import "ValidTicketSyncManager.h"
#import "AppDelegate.h"

@implementation ValidTicketSyncManager 
@synthesize Setcontent;
@synthesize setNames;
@synthesize citys;
@synthesize Result;
@synthesize theConnection;
@synthesize currentElement;
@synthesize item;
@synthesize count;
@synthesize setName;
@synthesize term;
@synthesize webData;
@synthesize xmlParser;


-(ValidTicketSyncManager*)init{
	self = [super init];
	if(self){
        
        
        
       [self GetID:@""];
        
    }
    return self;
}

-(void)GetID:(NSString*)termit

{
    
    /*
     
     <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
     <soapenv:Header/>
     <soapenv:Body>
     <tem:ValidTicket>
     <!--Optional:-->
     <tem:Ticket>?</tem:Ticket>
     </tem:ValidTicket>
     </soapenv:Body>
     </soapenv:Envelope>
     
     */
    
   
    Setcontent = [[NSMutableArray alloc] init];
    citys = [[NSMutableArray alloc] init];
    setName=@"ValidTicketResponse";
    
    setNames = [[NSMutableArray alloc] initWithObjects:@"ValidTicketResult", nil];

    
    //http://mdmedcare.com/dosingws/dosingws.svc?wsdl
    
    //03-08-2017
    
    
    NSString *soapMessage = [NSString stringWithFormat:
                             
                             @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
                             "<soapenv:Header/>"
                             "<soapenv:Body>"
                             "<tem:ValidTicket>"
                             "<tem:Ticket>%@</tem:Ticket>"
                             "</tem:ValidTicket>"
                             "</soapenv:Body>"
                             "</soapenv:Envelope>", [[NSUserDefaults standardUserDefaults] valueForKey:@"AuthenticateUserResult"]];
    
    //To suppress the leak in NSXMLParser
    NSURL *url = [NSURL URLWithString:@"http://mdmedcare.com/DosingWS/DosingWS.svc/soap"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://tempuri.org/IDosingWS/ValidTicket" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:
     [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    webData = [[NSMutableData data] retain]; 
     
     
}


-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	
    [webData setLength: 0];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[webData appendData:data];
    
    unsigned char byteBuffer[[webData length]];
    [webData getBytes:byteBuffer];
    
    
	
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  
    xmlParser = [[NSXMLParser alloc] initWithData: webData];
    [xmlParser setDelegate: self];
    [xmlParser setShouldResolveExternalEntities: YES];
    [xmlParser parse];//start parsing
 
   
    
    
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
}

-(void) parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
{
    NSString *temp = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];

    
   for (NSInteger i =0; i < [setNames count]; i++) {
        
        if ([currentElement isEqualToString:[setNames objectAtIndex:i]]) {
            
            
            [[Setcontent objectAtIndex:i] appendString:temp];
            
            
        }
    }
    
    
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    
    currentElement = [elementName copy];
    if ([elementName isEqualToString:setName]) {
        
        item = [[NSMutableDictionary alloc] init];
        [Setcontent removeAllObjects];
        
       for (NSInteger i =0; i < [setNames count]; i++) {
            
            NSMutableString *Setit= [[NSMutableString alloc] init];
            [Setcontent addObject:Setit];
            
        }
        
    }
    
    
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
	
    
    
    if ([elementName isEqualToString:setName]) {
        
        
       for (NSInteger i =0; i < [setNames count]; i++) {
            
            [item setObject:[Setcontent objectAtIndex:i] forKey:[setNames objectAtIndex:i]];
          
        }
        
        [citys addObject:item];
        
        
       ////////////NSLog(@"%@", citys);
        
        
    
        
        [item release];
		
    }
	
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    
    
    string = [string stringByReplacingOccurrencesOfString: @"\"" withString:@""];
    string = [string stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    string = [string stringByReplacingOccurrencesOfString: @"\t" withString:@""];
    
    
    
   for (NSInteger i =0; i < [setNames count]; i++) {
        
        if ([currentElement isEqualToString:[setNames objectAtIndex:i]]) {
            
            [[Setcontent objectAtIndex:i] appendString:string];
            
            
        }
        
        
        
        
    }
    
}

-(void)parser:(NSXMLParser*)myParser {
    
    NSAutoreleasePool *schedulePool = [[NSAutoreleasePool alloc] init];
    
    BOOL success = [myParser parse];
    
    if(success) {
        
     
    } else {
        
   
        
    }
    
    [schedulePool drain];
    
    [myParser release];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    

    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
   
    
    
        if ([citys count] >0) {
            
            if ([[[citys valueForKey:@"ValidTicketResult"]objectAtIndex:0] isEqualToString:@"Success"]) {
                
         
                
                
            }
            else
                
                
            {
                [delegate getticket];
            }
        }
    
    

    
}




-(NSString *) change: (NSString *) dumb
{
    if ([dumb isEqualToString:@""]) {
        
        dumb=@"-";
        
        return dumb;
        
    }
    
    else
    {
        
        return dumb;
        
    }
    
}





@end
