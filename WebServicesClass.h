//
//  WebServicesClass.h
//  GymTymer
//
//  Created by TechnoMac-7 on 12/08/16.
//  Copyright © 2016 TechnoMac-10. All rights reserved.

#define successCode 200     // API response success code
#define APIMessageParam @"message"
#define APIStatusParam @"code"

#import <Foundation/Foundation.h>
//#import "AppDelegate.h"
//#import "MBProgressHUD.h"

@interface WebServicesClass : NSObject<NSURLSessionTaskDelegate>

//@property (nonatomic, strong) MBProgressHUD *HUD;

+ (WebServicesClass *) sharedWebServiceClass;

// API calling POST method
-(void)JsonCall:(NSString *)strparam url:(NSString *)strUrl WitCompilation:(void (^)(NSString *strResponse,NSError *error))completion;
-(void)JsonCallGET:(NSString *)urlString WitCompilation:(void (^)(NSMutableDictionary *Dictionary))completion;
// API calling GET method
//-(void)JsonCallGET:(NSString *)urlString WitCompilation:(void (^)(NSMutableDictionary *Dictionary))completion;
//
//// API calling Image uplaoding "FormData" request POST
//-(void)JsonCallWithImage:(NSData *)imageData withfieldName:(NSString *)strfieldName ClassURL:(NSString *)urlClass WitCompilation:(void (^)(NSMutableDictionary *Dictionary,NSError *error))completion;
//
//// Indicator
//-(void)HideProgressHUD;
//-(void)ShowProgressHUD;
@end
