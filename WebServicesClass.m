//
//  WebServicesClass.m
//  GymTymer
//
//  Created by TechnoMac-7 on 12/08/16.
//  Copyright © 2016 TechnoMac-10. All rights reserved.
//

#import "WebServicesClass.h"
//#import "AppDelegate.h"

@implementation WebServicesClass

static WebServicesClass* _sharedWebServiceCom = nil;

+ (WebServicesClass *) sharedWebServiceClass
{
    @synchronized([WebServicesClass class])
    {
        if (!_sharedWebServiceCom)
            _sharedWebServiceCom = [[self alloc] init];
        
        return _sharedWebServiceCom;
    }
    return nil;
}
- (id)init {
    self = [super init];
    if (self != nil) {
        
        // initialize stuff here
//        _HUD = [[MBProgressHUD alloc]initWithWindow:[SHARED_APPDELEGATE window]];
//        [[SHARED_APPDELEGATE window] addSubview:_HUD];
//        [_HUD setLabelText:@"Please wait"];
    }
    return self;
}

#pragma mark - POST method

-(void)JsonCall:(NSString *)strparam url:(NSString *)strUrl WitCompilation:(void (^)(NSString *strResponse,NSError *error))completion
{
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",strUrl]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
        
//    if ([userDefaults objectForKey:userdefaultAuthHeader]) {
//        [request setValue:[userDefaults objectForKey:userdefaultAuthHeader] forHTTPHeaderField:@"Authorization"];
//    }
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
//    NSDictionary *mapData = [[NSDictionary alloc] initWithDictionary:dicData];
    
//    NSString  *strParam = @"id=e6922c1f310bf76b&device=Android&version=1.0.1";
    NSData *postData = [NSData dataWithBytes:strparam.UTF8String length:strparam.length];
    [request setHTTPBody:postData];

//    [_HUD setLabelText:@"Please wait"];
//    [self ShowProgressHUD];      // Show MBProgressHUD
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                         {
                                             if (!error)
                                             {
//                                                 [self HideProgressHUD];  // Hide MBProgressHUD
                                                 
                                                 NSString *strResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

                                                 NSLog(@"strResponse %@",strResponse);

                                                 //                                                 NSMutableDictionary *dicjson = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
//                                                 NSLog(@"%@",dicjson);
                                                 
                                                 if (completion)
                                                     completion(strResponse,error);
                                             }
                                             else
                                             {
                                                 if (completion)
                                                     completion(nil,error);
//                                                 [CommonMethodsModel ShowAlertwithTitle:@"Server Error" Message:error.description];
                                             }
                                         }];
    [postDataTask resume];
}
#pragma mark - GET method

-(void)JsonCallGET:(NSString *)urlString WitCompilation:(void (^)(NSMutableDictionary *Dictionary))completion
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];

    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];


[request setHTTPMethod:@"GET"];

    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if (!error)
                                              {
                                                  NSMutableDictionary *dicjson = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                  NSLog(@"%@",dicjson);

                                                  if (completion)
                                                      completion(dicjson);
                                              }
                                              else
                                              {
                                                  NSMutableDictionary *dicjson = [[NSMutableDictionary alloc]init];
                                                  if (completion)
                                                      completion(dicjson);
                                              }
                                          }];
    [postDataTask resume];
}



@end
