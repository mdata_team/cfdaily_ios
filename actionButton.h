//
//  actionButton.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 30-08-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Profiles.h"

@interface actionButton : UIButton


@property (nonatomic, strong) Profiles *chosenCourse;

@end
