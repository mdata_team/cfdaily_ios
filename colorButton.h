//
//  colorButton.h
//  CFMedcare
//
//  Created by Jeffrey Snijder on 15-03-13.
//  Copyright (c) 2013 Menopur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Medicine.h"

@interface colorButton : UIButton

@property (nonatomic, retain) UILocalNotification *currentNotification;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, strong) Medicine *chosenCourse;

@end
